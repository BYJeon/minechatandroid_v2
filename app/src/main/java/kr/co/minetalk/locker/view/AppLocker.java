package kr.co.minetalk.locker.view;

import android.app.Application;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.yalantis.ucrop.UCropActivity;

import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.activity.AccountChangeActivity;
import kr.co.minetalk.activity.AccountWithdrawActivity;
import kr.co.minetalk.activity.AddQnaActivity;
import kr.co.minetalk.activity.AdsMessageDetailActivity;
import kr.co.minetalk.activity.BuyCouponActivity;
import kr.co.minetalk.activity.BuyHistoryActivity;
import kr.co.minetalk.activity.CameraTranslactionActivity;
import kr.co.minetalk.activity.ChargeActivity;
import kr.co.minetalk.activity.ChatBackgroundSettingActivity;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.activity.ChatNormalSetActivity;
import kr.co.minetalk.activity.ChatOptionActivity;
import kr.co.minetalk.activity.ChatRoomFriendActivity;
import kr.co.minetalk.activity.ChatSearchActivity;
import kr.co.minetalk.activity.CreateMarketingChatActivity;
import kr.co.minetalk.activity.EditAccountActivity;
import kr.co.minetalk.activity.EditChatNameActivity;
import kr.co.minetalk.activity.EditCustomNumActivity;
import kr.co.minetalk.activity.EditGenderActivity;
import kr.co.minetalk.activity.EditIDActivity;
import kr.co.minetalk.activity.EditJobActivity;
import kr.co.minetalk.activity.EditNameActivity;
import kr.co.minetalk.activity.EditNickNameActivity;
import kr.co.minetalk.activity.EditPhoneActivity;
import kr.co.minetalk.activity.EditProfileActivity;
import kr.co.minetalk.activity.EditRecommendActivity;
import kr.co.minetalk.activity.EditStateMessageActivity;
import kr.co.minetalk.activity.EditUserAddressActivity;
import kr.co.minetalk.activity.EditUserBirthDayActivity;
import kr.co.minetalk.activity.EditUserEmailActivity;
import kr.co.minetalk.activity.EditUserPassActivity;
import kr.co.minetalk.activity.EmailCertiActivity;
import kr.co.minetalk.activity.EventActivity;
import kr.co.minetalk.activity.ExchangeActivity;
import kr.co.minetalk.activity.FAQActivity;
import kr.co.minetalk.activity.FacebookLoginActivity;
import kr.co.minetalk.activity.FindAddrActivity;
import kr.co.minetalk.activity.FindIdActivity;
import kr.co.minetalk.activity.FindPasswordActivity;
import kr.co.minetalk.activity.FindPasswordWithIdActivity;
import kr.co.minetalk.activity.FriendBlockListActivity;
import kr.co.minetalk.activity.FriendManageActivity;
import kr.co.minetalk.activity.FriendProfileActivity;
import kr.co.minetalk.activity.FriendSearchActivity;
import kr.co.minetalk.activity.LimitCheckActivity;
import kr.co.minetalk.activity.LoginActivity;
import kr.co.minetalk.activity.MainActivity;
import kr.co.minetalk.activity.MessageTargetActivity;
import kr.co.minetalk.activity.MyLanguageActivity;
import kr.co.minetalk.activity.MyQrActivity;
import kr.co.minetalk.activity.NextGearActivity;
import kr.co.minetalk.activity.NoticeActivity;
import kr.co.minetalk.activity.PayActivity;
import kr.co.minetalk.activity.PhoneCertiActivity;
import kr.co.minetalk.activity.PhotoViewerActivity;
import kr.co.minetalk.activity.PointAmountHistoryActivity;
import kr.co.minetalk.activity.PointCashGainActivity;
import kr.co.minetalk.activity.PointExchangeActivity;
import kr.co.minetalk.activity.PointGameActivity;
import kr.co.minetalk.activity.PointGamePlayActivity;
import kr.co.minetalk.activity.PointHistoryActivity;
import kr.co.minetalk.activity.PointReportActivity;
import kr.co.minetalk.activity.PointReportMoreActivity;
import kr.co.minetalk.activity.PointSaveActivity;
import kr.co.minetalk.activity.PointSendActivity;
import kr.co.minetalk.activity.PointTokenGainActivity;
import kr.co.minetalk.activity.PointWithDrawActivity;
import kr.co.minetalk.activity.QRFriendActivity;
import kr.co.minetalk.activity.QnAActivity;
import kr.co.minetalk.activity.RecommendActivity;
import kr.co.minetalk.activity.RegistActivity;
import kr.co.minetalk.activity.RegistNormalActivity;
import kr.co.minetalk.activity.RegistSnsActivity;
import kr.co.minetalk.activity.SaveHistoryActivity;
import kr.co.minetalk.activity.SelectBackgroundActivity;
import kr.co.minetalk.activity.SelectFriendActivity;
import kr.co.minetalk.activity.SelectLanguageActivity;
import kr.co.minetalk.activity.SelectNormalOptionActivity;
import kr.co.minetalk.activity.SelectSecurityFriendActivity;
import kr.co.minetalk.activity.SendPointUserActivity;
import kr.co.minetalk.activity.SnsGoogleLoginActivity;
import kr.co.minetalk.activity.SnsKakaoLoginActivity;
import kr.co.minetalk.activity.SplashActivity;
import kr.co.minetalk.activity.TermsActivity;
import kr.co.minetalk.activity.TranslationResultActivity;
import kr.co.minetalk.activity.TransmissionActivity;
import kr.co.minetalk.activity.UpdateActivity;
import kr.co.minetalk.activity.VoiceTranslationActivity;
import kr.co.minetalk.activity.WalletActivity;
import kr.co.minetalk.activity.WebViewActivity;
import kr.co.minetalk.locker.AppLockImpl;
import kr.co.minetalk.locker.utils.Locker;

public class AppLocker {

	private volatile static AppLocker instance;
	private Locker curAppLocker;
	private String TAG = "AppLocker";

	public static AppLocker getInstance() {
		synchronized (AppLocker.class) {
			if (instance == null) {
				instance = new AppLocker();
			}
		}
		return instance;
	}

	public void enableAppLock(boolean enable) {
		if(enable)
			curAppLocker.enable();
		else
			curAppLocker.disable();
	}


	public void enableAppLock(Application app, boolean enable) {
		if (curAppLocker == null) {
			curAppLocker = new AppLockImpl(app);
		}

		if(enable) {
			curAppLocker.enable();
			enableAppActivity(enable);
		}else
			curAppLocker.disable();
	}

	public void enableAppLockFromSwitch(Application app, boolean enable) {
		if (curAppLocker == null) {
			curAppLocker = new AppLockImpl(app);
		}

		if(enable) {
			curAppLocker.enable();
			enableAppBySwitchActivity(enable);
		}else
			curAppLocker.disable();
	}

	public void clearIgnoredActivity(){
		if(curAppLocker == null) return;
		curAppLocker.clearAllClass();
	}

	public void addIgnoredActivity(Class<?> clazz){
		if(curAppLocker == null) return;
		curAppLocker.addIgnoredActivity(clazz);
	}

	public void removeIgnoredActivity(Class<?> clazz){
		if(curAppLocker == null) return;
		curAppLocker.removeIgnoredActivity(clazz);
	}


	public void enableAppBySwitchActivity(boolean enable) {
		curAppLocker.clearAllClass();

		//curAppLocker.addIgnoredActivity(SplashActivity.class);
		curAppLocker.addIgnoredActivity(MainActivity.class);
		curAppLocker.addIgnoredActivity(LoginActivity.class);
		curAppLocker.addIgnoredActivity(RegistActivity.class);
		curAppLocker.addIgnoredActivity(RegistNormalActivity.class);
		curAppLocker.addIgnoredActivity(AdsMessageDetailActivity.class);

		curAppLocker.addIgnoredActivity(RegistSnsActivity.class);
		curAppLocker.addIgnoredActivity(FindPasswordActivity.class);
		curAppLocker.addIgnoredActivity(FindPasswordWithIdActivity.class);
		curAppLocker.addIgnoredActivity(FindIdActivity.class);
		curAppLocker.addIgnoredActivity(EditProfileActivity.class);
		curAppLocker.addIgnoredActivity(EditAccountActivity.class);
		curAppLocker.addIgnoredActivity(NextGearActivity.class);
		curAppLocker.addIgnoredActivity(NextGearActivity.class);

		curAppLocker.addIgnoredActivity(EditNameActivity.class);
		curAppLocker.addIgnoredActivity(EditPhoneActivity.class);
		curAppLocker.addIgnoredActivity(EditUserPassActivity.class);
		curAppLocker.addIgnoredActivity(EditUserBirthDayActivity.class);
		curAppLocker.addIgnoredActivity(EditUserEmailActivity.class);
		curAppLocker.addIgnoredActivity(EditUserAddressActivity.class);

		curAppLocker.addIgnoredActivity(EditStateMessageActivity.class);
		curAppLocker.addIgnoredActivity(NoticeActivity.class);
		curAppLocker.addIgnoredActivity(PointSendActivity.class);
		curAppLocker.addIgnoredActivity(PointReportActivity.class);
		curAppLocker.addIgnoredActivity(MyQrActivity.class);
		curAppLocker.addIgnoredActivity(WebViewActivity.class);

		curAppLocker.addIgnoredActivity(FriendSearchActivity.class);
		curAppLocker.addIgnoredActivity(TranslationResultActivity.class);
		curAppLocker.addIgnoredActivity(FriendManageActivity.class);
		curAppLocker.addIgnoredActivity(MyLanguageActivity.class);
		curAppLocker.addIgnoredActivity(FriendBlockListActivity.class);
		curAppLocker.addIgnoredActivity(VoiceTranslationActivity.class);

		curAppLocker.addIgnoredActivity(ChatDetailActivity.class);
		curAppLocker.addIgnoredActivity(ChargeActivity.class);
		curAppLocker.addIgnoredActivity(ChatOptionActivity.class);
		curAppLocker.addIgnoredActivity(CreateMarketingChatActivity.class);
		curAppLocker.addIgnoredActivity(MessageTargetActivity.class);
		curAppLocker.addIgnoredActivity(EditChatNameActivity.class);

		curAppLocker.addIgnoredActivity(EditJobActivity.class);
		curAppLocker.addIgnoredActivity(EditGenderActivity.class);
		curAppLocker.addIgnoredActivity(PayActivity.class);
		curAppLocker.addIgnoredActivity(SelectLanguageActivity.class);
		curAppLocker.addIgnoredActivity(ChatBackgroundSettingActivity.class);
		curAppLocker.addIgnoredActivity(SelectBackgroundActivity.class);

		curAppLocker.addIgnoredActivity(SendPointUserActivity.class);
		curAppLocker.addIgnoredActivity(SelectFriendActivity.class);
		curAppLocker.addIgnoredActivity(SelectSecurityFriendActivity.class);
		curAppLocker.addIgnoredActivity(PhotoViewerActivity.class);
		curAppLocker.addIgnoredActivity(CameraTranslactionActivity.class);
		curAppLocker.addIgnoredActivity(SnsKakaoLoginActivity.class);

		curAppLocker.addIgnoredActivity(FacebookLoginActivity.class);
		curAppLocker.addIgnoredActivity(SnsGoogleLoginActivity.class);
		curAppLocker.addIgnoredActivity(UCropActivity.class);
		curAppLocker.addIgnoredActivity(FAQActivity.class);
		curAppLocker.addIgnoredActivity(QnAActivity.class);
		curAppLocker.addIgnoredActivity(AddQnaActivity.class);


		curAppLocker.addIgnoredActivity(TransmissionActivity.class);
		curAppLocker.addIgnoredActivity(RecommendActivity.class);
		curAppLocker.addIgnoredActivity(TermsActivity.class);
		curAppLocker.addIgnoredActivity(ExchangeActivity.class);
		curAppLocker.addIgnoredActivity(FriendProfileActivity.class);
		curAppLocker.addIgnoredActivity(ChatSearchActivity.class);

		curAppLocker.addIgnoredActivity(QRFriendActivity.class);
		curAppLocker.addIgnoredActivity(PointReportMoreActivity.class);
		curAppLocker.addIgnoredActivity(PointHistoryActivity.class);
		curAppLocker.addIgnoredActivity(EventActivity.class);
		curAppLocker.addIgnoredActivity(UpdateActivity.class);
		curAppLocker.addIgnoredActivity(ChatRoomFriendActivity.class);

		curAppLocker.addIgnoredActivity(PointCashGainActivity.class);
		curAppLocker.addIgnoredActivity(PointTokenGainActivity.class);
		curAppLocker.addIgnoredActivity(ChatNormalSetActivity.class);
		curAppLocker.addIgnoredActivity(EditCustomNumActivity.class);
		curAppLocker.addIgnoredActivity(EditIDActivity.class);
		curAppLocker.addIgnoredActivity(PointExchangeActivity.class);

		curAppLocker.addIgnoredActivity(WalletActivity.class);
		curAppLocker.addIgnoredActivity(PointAmountHistoryActivity.class);
		curAppLocker.addIgnoredActivity(PointWithDrawActivity.class);
		curAppLocker.addIgnoredActivity(EditNickNameActivity.class);
		curAppLocker.addIgnoredActivity(EditRecommendActivity.class);
		curAppLocker.addIgnoredActivity(PhoneCertiActivity.class);

		curAppLocker.addIgnoredActivity(EmailCertiActivity.class);
		curAppLocker.addIgnoredActivity(AccountWithdrawActivity.class);
		curAppLocker.addIgnoredActivity(AccountChangeActivity.class);
		curAppLocker.addIgnoredActivity(PointSaveActivity.class);
		curAppLocker.addIgnoredActivity(PointGameActivity.class);
		curAppLocker.addIgnoredActivity(PointGamePlayActivity.class);
		curAppLocker.addIgnoredActivity(BuyCouponActivity.class);

		curAppLocker.addIgnoredActivity(LimitCheckActivity.class);
		curAppLocker.addIgnoredActivity(SaveHistoryActivity.class);
		curAppLocker.addIgnoredActivity(BuyHistoryActivity.class);
		curAppLocker.addIgnoredActivity(SelectNormalOptionActivity.class);
	}

	public void enableAppActivity(boolean enable){
		curAppLocker.clearAllClass();

//		PackageManager pManager = MineTalkApp.getAppContext().getPackageManager();
//		String packageName = MineTalkApp.getAppContext().getApplicationContext().getPackageName();
//
//		try {
//			ActivityInfo[] list = pManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES).activities;
//			for (ActivityInfo activityInfo : list) {
//				Log.d(TAG, "ActivityInfo = " + activityInfo.name);
//				Log.d(TAG, "ActivityInfo = " + activityInfo.packageName);
//				if(enable){
//					if(activityInfo.getClass().getName().toLowerCase().contains("mainactivity")) continue;
//					curAppLocker.addIgnoredActivity("class " + activityInfo.name);
//				}else{
//					curAppLocker.removeIgnoredActivity("class " + activityInfo.name);
//				}
//			}
//		} catch (PackageManager.NameNotFoundException e) {
//			e.printStackTrace();
//		}

		//curAppLocker.addIgnoredActivity(SplashActivity.class);
		//curAppLocker.addIgnoredActivity(MainActivity.class);
		curAppLocker.addIgnoredActivity(LoginActivity.class);
		curAppLocker.addIgnoredActivity(RegistActivity.class);
		curAppLocker.addIgnoredActivity(RegistNormalActivity.class);
		curAppLocker.addIgnoredActivity(AdsMessageDetailActivity.class);

		curAppLocker.addIgnoredActivity(RegistSnsActivity.class);
		curAppLocker.addIgnoredActivity(FindPasswordActivity.class);
		curAppLocker.addIgnoredActivity(FindPasswordWithIdActivity.class);
		curAppLocker.addIgnoredActivity(FindIdActivity.class);
		curAppLocker.addIgnoredActivity(EditProfileActivity.class);
		curAppLocker.addIgnoredActivity(EditAccountActivity.class);
		curAppLocker.addIgnoredActivity(NextGearActivity.class);
		curAppLocker.addIgnoredActivity(FindAddrActivity.class);

		curAppLocker.addIgnoredActivity(EditNameActivity.class);
		curAppLocker.addIgnoredActivity(EditPhoneActivity.class);
		curAppLocker.addIgnoredActivity(EditUserPassActivity.class);
		curAppLocker.addIgnoredActivity(EditUserBirthDayActivity.class);
		curAppLocker.addIgnoredActivity(EditUserEmailActivity.class);
		curAppLocker.addIgnoredActivity(EditUserAddressActivity.class);

		curAppLocker.addIgnoredActivity(EditStateMessageActivity.class);
		curAppLocker.addIgnoredActivity(NoticeActivity.class);
		curAppLocker.addIgnoredActivity(PointSendActivity.class);
		curAppLocker.addIgnoredActivity(PointReportActivity.class);
		curAppLocker.addIgnoredActivity(MyQrActivity.class);
		curAppLocker.addIgnoredActivity(WebViewActivity.class);

		curAppLocker.addIgnoredActivity(FriendSearchActivity.class);
		curAppLocker.addIgnoredActivity(TranslationResultActivity.class);
		curAppLocker.addIgnoredActivity(FriendManageActivity.class);
		curAppLocker.addIgnoredActivity(MyLanguageActivity.class);
		curAppLocker.addIgnoredActivity(FriendBlockListActivity.class);
		curAppLocker.addIgnoredActivity(VoiceTranslationActivity.class);

		curAppLocker.addIgnoredActivity(ChatDetailActivity.class);
		curAppLocker.addIgnoredActivity(ChargeActivity.class);
		curAppLocker.addIgnoredActivity(ChatOptionActivity.class);
		curAppLocker.addIgnoredActivity(CreateMarketingChatActivity.class);
		curAppLocker.addIgnoredActivity(MessageTargetActivity.class);
		curAppLocker.addIgnoredActivity(EditChatNameActivity.class);

		curAppLocker.addIgnoredActivity(EditJobActivity.class);
		curAppLocker.addIgnoredActivity(EditGenderActivity.class);
		curAppLocker.addIgnoredActivity(PayActivity.class);
		curAppLocker.addIgnoredActivity(SelectLanguageActivity.class);
		curAppLocker.addIgnoredActivity(ChatBackgroundSettingActivity.class);
		curAppLocker.addIgnoredActivity(SelectBackgroundActivity.class);

		curAppLocker.addIgnoredActivity(SendPointUserActivity.class);
		curAppLocker.addIgnoredActivity(SelectFriendActivity.class);
		curAppLocker.addIgnoredActivity(SelectSecurityFriendActivity.class);
		curAppLocker.addIgnoredActivity(PhotoViewerActivity.class);
		curAppLocker.addIgnoredActivity(CameraTranslactionActivity.class);
		curAppLocker.addIgnoredActivity(SnsKakaoLoginActivity.class);

		curAppLocker.addIgnoredActivity(FacebookLoginActivity.class);
		curAppLocker.addIgnoredActivity(SnsGoogleLoginActivity.class);
		curAppLocker.addIgnoredActivity(UCropActivity.class);
		curAppLocker.addIgnoredActivity(FAQActivity.class);
		curAppLocker.addIgnoredActivity(QnAActivity.class);
		curAppLocker.addIgnoredActivity(AddQnaActivity.class);


		curAppLocker.addIgnoredActivity(TransmissionActivity.class);
		curAppLocker.addIgnoredActivity(RecommendActivity.class);
		curAppLocker.addIgnoredActivity(TermsActivity.class);
		curAppLocker.addIgnoredActivity(ExchangeActivity.class);
		curAppLocker.addIgnoredActivity(FriendProfileActivity.class);
		curAppLocker.addIgnoredActivity(ChatSearchActivity.class);

		curAppLocker.addIgnoredActivity(QRFriendActivity.class);
		curAppLocker.addIgnoredActivity(PointReportMoreActivity.class);
		curAppLocker.addIgnoredActivity(PointHistoryActivity.class);
		curAppLocker.addIgnoredActivity(EventActivity.class);
		curAppLocker.addIgnoredActivity(UpdateActivity.class);
		curAppLocker.addIgnoredActivity(ChatRoomFriendActivity.class);

		curAppLocker.addIgnoredActivity(PointCashGainActivity.class);
		curAppLocker.addIgnoredActivity(PointTokenGainActivity.class);
		curAppLocker.addIgnoredActivity(ChatNormalSetActivity.class);
		curAppLocker.addIgnoredActivity(EditCustomNumActivity.class);
		curAppLocker.addIgnoredActivity(EditIDActivity.class);
		curAppLocker.addIgnoredActivity(PointExchangeActivity.class);

		curAppLocker.addIgnoredActivity(WalletActivity.class);
		curAppLocker.addIgnoredActivity(PointAmountHistoryActivity.class);
		curAppLocker.addIgnoredActivity(PointWithDrawActivity.class);
		curAppLocker.addIgnoredActivity(EditNickNameActivity.class);
		curAppLocker.addIgnoredActivity(EditRecommendActivity.class);
		curAppLocker.addIgnoredActivity(PhoneCertiActivity.class);

		curAppLocker.addIgnoredActivity(EmailCertiActivity.class);
		curAppLocker.addIgnoredActivity(AccountWithdrawActivity.class);
		curAppLocker.addIgnoredActivity(AccountChangeActivity.class);
		curAppLocker.addIgnoredActivity(PointSaveActivity.class);
		curAppLocker.addIgnoredActivity(PointGameActivity.class);
		curAppLocker.addIgnoredActivity(PointGamePlayActivity.class);
		curAppLocker.addIgnoredActivity(BuyCouponActivity.class);

		curAppLocker.addIgnoredActivity(LimitCheckActivity.class);
		curAppLocker.addIgnoredActivity(SaveHistoryActivity.class);
		curAppLocker.addIgnoredActivity(BuyHistoryActivity.class);
		curAppLocker.addIgnoredActivity(SelectNormalOptionActivity.class);

//		if(!enable){
//			//curAppLocker.removeIgnoredActivity(SplashActivity.class);
//			//curAppLocker.removeIgnoredActivity(MainActivity.class);
//			curAppLocker.removeIgnoredActivity(LoginActivity.class);
//			curAppLocker.removeIgnoredActivity(RegistActivity.class);
//			curAppLocker.removeIgnoredActivity(RegistNormalActivity.class);
//			curAppLocker.removeIgnoredActivity(AdsMessageDetailActivity.class);
//
//			curAppLocker.removeIgnoredActivity(RegistSnsActivity.class);
//			curAppLocker.removeIgnoredActivity(FindPasswordActivity.class);
//			curAppLocker.removeIgnoredActivity(FindPasswordWithIdActivity.class);
//			curAppLocker.removeIgnoredActivity(FindIdActivity.class);
//			curAppLocker.removeIgnoredActivity(EditProfileActivity.class);
//			curAppLocker.removeIgnoredActivity(EditAccountActivity.class);
//
//			curAppLocker.removeIgnoredActivity(EditNameActivity.class);
//			curAppLocker.removeIgnoredActivity(EditPhoneActivity.class);
//			curAppLocker.removeIgnoredActivity(EditUserPassActivity.class);
//			curAppLocker.removeIgnoredActivity(EditUserBirthDayActivity.class);
//			curAppLocker.removeIgnoredActivity(EditUserEmailActivity.class);
//			curAppLocker.removeIgnoredActivity(EditUserAddressActivity.class);
//
//			curAppLocker.removeIgnoredActivity(EditStateMessageActivity.class);
//			curAppLocker.removeIgnoredActivity(NoticeActivity.class);
//			curAppLocker.removeIgnoredActivity(PointSendActivity.class);
//			curAppLocker.removeIgnoredActivity(PointReportActivity.class);
//			curAppLocker.removeIgnoredActivity(MyQrActivity.class);
//			curAppLocker.removeIgnoredActivity(WebViewActivity.class);
//
//			curAppLocker.removeIgnoredActivity(FriendSearchActivity.class);
//			curAppLocker.removeIgnoredActivity(TranslationResultActivity.class);
//			curAppLocker.removeIgnoredActivity(FriendManageActivity.class);
//			curAppLocker.removeIgnoredActivity(MyLanguageActivity.class);
//			curAppLocker.removeIgnoredActivity(FriendBlockListActivity.class);
//			curAppLocker.removeIgnoredActivity(VoiceTranslationActivity.class);
//
//			curAppLocker.removeIgnoredActivity(ChatDetailActivity.class);
//			curAppLocker.removeIgnoredActivity(ChargeActivity.class);
//			curAppLocker.removeIgnoredActivity(ChatOptionActivity.class);
//			curAppLocker.removeIgnoredActivity(CreateMarketingChatActivity.class);
//			curAppLocker.removeIgnoredActivity(MessageTargetActivity.class);
//			curAppLocker.removeIgnoredActivity(EditChatNameActivity.class);
//
//			curAppLocker.removeIgnoredActivity(EditJobActivity.class);
//			curAppLocker.removeIgnoredActivity(EditGenderActivity.class);
//			curAppLocker.removeIgnoredActivity(PayActivity.class);
//			curAppLocker.removeIgnoredActivity(SelectLanguageActivity.class);
//			curAppLocker.removeIgnoredActivity(ChatBackgroundSettingActivity.class);
//			curAppLocker.removeIgnoredActivity(SelectBackgroundActivity.class);
//
//			curAppLocker.removeIgnoredActivity(SendPointUserActivity.class);
//			curAppLocker.removeIgnoredActivity(SelectFriendActivity.class);
//			curAppLocker.removeIgnoredActivity(SelectSecurityFriendActivity.class);
//			curAppLocker.removeIgnoredActivity(PhotoViewerActivity.class);
//			curAppLocker.removeIgnoredActivity(CameraTranslactionActivity.class);
//			curAppLocker.removeIgnoredActivity(SnsKakaoLoginActivity.class);
//
//			curAppLocker.removeIgnoredActivity(FacebookLoginActivity.class);
//			curAppLocker.removeIgnoredActivity(SnsGoogleLoginActivity.class);
//			curAppLocker.removeIgnoredActivity(UCropActivity.class);
//			curAppLocker.removeIgnoredActivity(FAQActivity.class);
//			curAppLocker.removeIgnoredActivity(QnAActivity.class);
//			curAppLocker.removeIgnoredActivity(AddQnaActivity.class);
//
//
//			curAppLocker.removeIgnoredActivity(TransmissionActivity.class);
//			curAppLocker.removeIgnoredActivity(RecommendActivity.class);
//			curAppLocker.removeIgnoredActivity(TermsActivity.class);
//			curAppLocker.removeIgnoredActivity(ExchangeActivity.class);
//			curAppLocker.removeIgnoredActivity(FriendProfileActivity.class);
//			curAppLocker.removeIgnoredActivity(ChatSearchActivity.class);
//
//			curAppLocker.removeIgnoredActivity(QRFriendActivity.class);
//			curAppLocker.removeIgnoredActivity(PointReportMoreActivity.class);
//			curAppLocker.removeIgnoredActivity(PointHistoryActivity.class);
//			curAppLocker.removeIgnoredActivity(EventActivity.class);
//			curAppLocker.removeIgnoredActivity(UpdateActivity.class);
//			curAppLocker.removeIgnoredActivity(ChatRoomFriendActivity.class);
//
//			curAppLocker.removeIgnoredActivity(PointCashGainActivity.class);
//			curAppLocker.removeIgnoredActivity(PointTokenGainActivity.class);
//			curAppLocker.removeIgnoredActivity(ChatNormalSetActivity.class);
//			curAppLocker.removeIgnoredActivity(EditCustomNumActivity.class);
//			curAppLocker.removeIgnoredActivity(EditIDActivity.class);
//			curAppLocker.removeIgnoredActivity(PointExchangeActivity.class);
//
//			curAppLocker.removeIgnoredActivity(WalletActivity.class);
//			curAppLocker.removeIgnoredActivity(PointAmountHistoryActivity.class);
//			curAppLocker.removeIgnoredActivity(PointWithDrawActivity.class);
//			curAppLocker.removeIgnoredActivity(EditNickNameActivity.class);
//			curAppLocker.removeIgnoredActivity(EditRecommendActivity.class);
//			curAppLocker.removeIgnoredActivity(PhoneCertiActivity.class);
//
//			curAppLocker.removeIgnoredActivity(EmailCertiActivity.class);
//			curAppLocker.removeIgnoredActivity(AccountWithdrawActivity.class);
//			curAppLocker.removeIgnoredActivity(PointSaveActivity.class);
//			curAppLocker.removeIgnoredActivity(PointGameActivity.class);
//			curAppLocker.removeIgnoredActivity(PointGamePlayActivity.class);
//			curAppLocker.removeIgnoredActivity(BuyCouponActivity.class);
//
//			curAppLocker.removeIgnoredActivity(LimitCheckActivity.class);
//			curAppLocker.removeIgnoredActivity(SaveHistoryActivity.class);
//			curAppLocker.removeIgnoredActivity(BuyHistoryActivity.class);
//			curAppLocker.removeIgnoredActivity(SelectNormalOptionActivity.class);
//		}else{
//			//curAppLocker.addIgnoredActivity(SplashActivity.class);
//			//curAppLocker.addIgnoredActivity(MainActivity.class);
//			curAppLocker.addIgnoredActivity(LoginActivity.class);
//			curAppLocker.addIgnoredActivity(RegistActivity.class);
//			curAppLocker.addIgnoredActivity(RegistNormalActivity.class);
//			curAppLocker.addIgnoredActivity(AdsMessageDetailActivity.class);
//
//			curAppLocker.addIgnoredActivity(RegistSnsActivity.class);
//			curAppLocker.addIgnoredActivity(FindPasswordActivity.class);
//			curAppLocker.addIgnoredActivity(FindPasswordWithIdActivity.class);
//			curAppLocker.addIgnoredActivity(FindIdActivity.class);
//			curAppLocker.addIgnoredActivity(EditProfileActivity.class);
//			curAppLocker.addIgnoredActivity(EditAccountActivity.class);
//
//			curAppLocker.addIgnoredActivity(EditNameActivity.class);
//			curAppLocker.addIgnoredActivity(EditPhoneActivity.class);
//			curAppLocker.addIgnoredActivity(EditUserPassActivity.class);
//			curAppLocker.addIgnoredActivity(EditUserBirthDayActivity.class);
//			curAppLocker.addIgnoredActivity(EditUserEmailActivity.class);
//			curAppLocker.addIgnoredActivity(EditUserAddressActivity.class);
//
//			curAppLocker.addIgnoredActivity(EditStateMessageActivity.class);
//			curAppLocker.addIgnoredActivity(NoticeActivity.class);
//			curAppLocker.addIgnoredActivity(PointSendActivity.class);
//			curAppLocker.addIgnoredActivity(PointReportActivity.class);
//			curAppLocker.addIgnoredActivity(MyQrActivity.class);
//			curAppLocker.addIgnoredActivity(WebViewActivity.class);
//
//			curAppLocker.addIgnoredActivity(FriendSearchActivity.class);
//			curAppLocker.addIgnoredActivity(TranslationResultActivity.class);
//			curAppLocker.addIgnoredActivity(FriendManageActivity.class);
//			curAppLocker.addIgnoredActivity(MyLanguageActivity.class);
//			curAppLocker.addIgnoredActivity(FriendBlockListActivity.class);
//			curAppLocker.addIgnoredActivity(VoiceTranslationActivity.class);
//
//			curAppLocker.addIgnoredActivity(ChatDetailActivity.class);
//			curAppLocker.addIgnoredActivity(ChargeActivity.class);
//			curAppLocker.addIgnoredActivity(ChatOptionActivity.class);
//			curAppLocker.addIgnoredActivity(CreateMarketingChatActivity.class);
//			curAppLocker.addIgnoredActivity(MessageTargetActivity.class);
//			curAppLocker.addIgnoredActivity(EditChatNameActivity.class);
//
//			curAppLocker.addIgnoredActivity(EditJobActivity.class);
//			curAppLocker.addIgnoredActivity(EditGenderActivity.class);
//			curAppLocker.addIgnoredActivity(PayActivity.class);
//			curAppLocker.addIgnoredActivity(SelectLanguageActivity.class);
//			curAppLocker.addIgnoredActivity(ChatBackgroundSettingActivity.class);
//			curAppLocker.addIgnoredActivity(SelectBackgroundActivity.class);
//
//			curAppLocker.addIgnoredActivity(SendPointUserActivity.class);
//			curAppLocker.addIgnoredActivity(SelectFriendActivity.class);
//			curAppLocker.addIgnoredActivity(SelectSecurityFriendActivity.class);
//			curAppLocker.addIgnoredActivity(PhotoViewerActivity.class);
//			curAppLocker.addIgnoredActivity(CameraTranslactionActivity.class);
//			curAppLocker.addIgnoredActivity(SnsKakaoLoginActivity.class);
//
//			curAppLocker.addIgnoredActivity(FacebookLoginActivity.class);
//			curAppLocker.addIgnoredActivity(SnsGoogleLoginActivity.class);
//			curAppLocker.addIgnoredActivity(UCropActivity.class);
//			curAppLocker.addIgnoredActivity(FAQActivity.class);
//			curAppLocker.addIgnoredActivity(QnAActivity.class);
//			curAppLocker.addIgnoredActivity(AddQnaActivity.class);
//
//
//			curAppLocker.addIgnoredActivity(TransmissionActivity.class);
//			curAppLocker.addIgnoredActivity(RecommendActivity.class);
//			curAppLocker.addIgnoredActivity(TermsActivity.class);
//			curAppLocker.addIgnoredActivity(ExchangeActivity.class);
//			curAppLocker.addIgnoredActivity(FriendProfileActivity.class);
//			curAppLocker.addIgnoredActivity(ChatSearchActivity.class);
//
//			curAppLocker.addIgnoredActivity(QRFriendActivity.class);
//			curAppLocker.addIgnoredActivity(PointReportMoreActivity.class);
//			curAppLocker.addIgnoredActivity(PointHistoryActivity.class);
//			curAppLocker.addIgnoredActivity(EventActivity.class);
//			curAppLocker.addIgnoredActivity(UpdateActivity.class);
//			curAppLocker.addIgnoredActivity(ChatRoomFriendActivity.class);
//
//			curAppLocker.addIgnoredActivity(PointCashGainActivity.class);
//			curAppLocker.addIgnoredActivity(PointTokenGainActivity.class);
//			curAppLocker.addIgnoredActivity(ChatNormalSetActivity.class);
//			curAppLocker.addIgnoredActivity(EditCustomNumActivity.class);
//			curAppLocker.addIgnoredActivity(EditIDActivity.class);
//			curAppLocker.addIgnoredActivity(PointExchangeActivity.class);
//
//			curAppLocker.addIgnoredActivity(WalletActivity.class);
//			curAppLocker.addIgnoredActivity(PointAmountHistoryActivity.class);
//			curAppLocker.addIgnoredActivity(PointWithDrawActivity.class);
//			curAppLocker.addIgnoredActivity(EditNickNameActivity.class);
//			curAppLocker.addIgnoredActivity(EditRecommendActivity.class);
//			curAppLocker.addIgnoredActivity(PhoneCertiActivity.class);
//
//			curAppLocker.addIgnoredActivity(EmailCertiActivity.class);
//			curAppLocker.addIgnoredActivity(AccountWithdrawActivity.class);
//			curAppLocker.addIgnoredActivity(PointSaveActivity.class);
//			curAppLocker.addIgnoredActivity(PointGameActivity.class);
//			curAppLocker.addIgnoredActivity(PointGamePlayActivity.class);
//			curAppLocker.addIgnoredActivity(BuyCouponActivity.class);
//
//			curAppLocker.addIgnoredActivity(LimitCheckActivity.class);
//			curAppLocker.addIgnoredActivity(SaveHistoryActivity.class);
//			curAppLocker.addIgnoredActivity(BuyHistoryActivity.class);
//			curAppLocker.addIgnoredActivity(SelectNormalOptionActivity.class);
//		}
	}
	public boolean isAppLockEnabled() {
		if (curAppLocker == null) {
			return false;
		} else {
			return true;
		}
	}

	public void setAppLock(Locker appLocker) {
		if (curAppLocker != null) {
			curAppLocker.disable();
		}
		curAppLocker = appLocker;
	}

	public Locker getAppLock() {
		return curAppLocker;
	}
}
