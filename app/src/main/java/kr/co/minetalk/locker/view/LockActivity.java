package kr.co.minetalk.locker.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.MainActivity;
import kr.co.minetalk.fingerprint.FingerprintHandler;
import kr.co.minetalk.locker.AppLockerActivity;
import kr.co.minetalk.locker.utils.Encryptor;
import kr.co.minetalk.locker.utils.Locker;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.Preferences;

public class LockActivity extends AppLockerActivity {
	public static final String TAG = "LockActivity";

	private int type = -1;
	private boolean isFingerType = Preferences.getFingerScreenLock();
	private String oldPasscode = null;

	protected EditText codeField1 = null;
	protected EditText codeField2 = null;
	protected EditText codeField3 = null;
	protected EditText codeField4 = null;
	protected InputFilter[] filters = null;
	protected TextView tvMessage = null;
	protected AppCompatActivity mCompatActivity;

	//지문 인증
	private FingerprintHandler fingerprintHandler = null;
	private FingerprintManager fingerprintManager;
	private KeyguardManager keyguardManager;
	private KeyStore keyStore;
	private KeyGenerator keyGenerator;
	private Cipher cipher;
	private FingerprintManager.CryptoObject cryptoObject;
	private static final String KEY_NAME = "minechat_key";

	private ImageView mFingerImg = null;
	private boolean isHeaderExist = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.page_passcode);
		mCompatActivity =this;

		findViewById(R.id.ll_applock).setBackgroundResource(MineTalk.isBlackTheme ? R.color.bk_theme_bg : R.color.white_color);
		findViewById(R.id.layout_finger_root).setBackgroundResource(MineTalk.isBlackTheme ? R.color.bk_theme_bg : R.color.white_color);

		tvMessage = (TextView) findViewById(R.id.tv_message);
		tvMessage.setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String message = extras.getString(Locker.MESSAGE);
			if (message != null) {
				tvMessage.setText(message);
			}

			type = extras.getInt(Locker.TYPE, -1);

			isHeaderExist = extras.getBoolean(Locker.HEAD_TYPE, false);
		}

		initBroadCastReceiver();

		findViewById(R.id.layout_finger_root).setVisibility(isFingerType ? View.VISIBLE : View.GONE);
		mFingerImg = (ImageView)findViewById(R.id.iv_finger);
		mFingerImg.setImageResource(MineTalk.isBlackTheme ? R.drawable.ic_fingerprint_pre_bk : R.drawable.ic_fingerprint_pre);
		//지문 인증일 경우
		if (isFingerType) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
				keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

				if (!fingerprintManager.isHardwareDetected()) {//Manifest에 Fingerprint 퍼미션을 추가해 워야 사용가능
					//tv_message.setText("지문을 사용할 수 없는 디바이스 입니다.");
				} else if (ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
					//tv_message.setText("지문사용을 허용해 주세요.");
					/*잠금화면 상태를 체크한다.*/
				} else if (!keyguardManager.isKeyguardSecure()) {
					//tv_message.setText("잠금화면을 설정해 주세요.");
				} else if (!fingerprintManager.hasEnrolledFingerprints()) {
					//tv_message.setText("등록된 지문이 없습니다.");
				} else {//모든 관문을 성공적으로 통과(지문인식을 지원하고 지문 사용이 허용되어 있고 잠금화면이 설정되었고 지문이 등록되어 있을때)
					//tv_message.setText("손가락을 홈버튼에 대 주세요.");

					generateKey();
					if (cipherInit()) {
						cryptoObject = new FingerprintManager.CryptoObject(cipher);
						//핸들러실행
						fingerprintHandler = new FingerprintHandler(this);
						fingerprintHandler.startAutho(fingerprintManager, cryptoObject);
					}
				}
			}
		}

		filters = new InputFilter[2];
		filters[0] = new InputFilter.LengthFilter(1);
		filters[1] = numberFilter;

		codeField1 = (EditText) findViewById(R.id.passcode_1);
		setupEditText(codeField1);

		codeField2 = (EditText) findViewById(R.id.passcode_2);
		setupEditText(codeField2);

		codeField3 = (EditText) findViewById(R.id.passcode_3);
		setupEditText(codeField3);

		codeField4 = (EditText) findViewById(R.id.passcode_4);
		setupEditText(codeField4);

		((ImageView) findViewById(R.id.iv_lock)).setImageResource(MineTalk.isBlackTheme ? R.drawable.ic_lock_bk : R.drawable.ic_lock);

		// setup the keyboard
		((TextView) findViewById(R.id.button0)).setOnClickListener(btnListener);
		((TextView) findViewById(R.id.button0)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
		((Button) findViewById(R.id.button1)).setOnClickListener(btnListener);
		((Button) findViewById(R.id.button1)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
		((Button) findViewById(R.id.button2)).setOnClickListener(btnListener);
		((Button) findViewById(R.id.button2)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
		((Button) findViewById(R.id.button3)).setOnClickListener(btnListener);
		((Button) findViewById(R.id.button3)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
		((Button) findViewById(R.id.button4)).setOnClickListener(btnListener);
		((Button) findViewById(R.id.button4)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
		((Button) findViewById(R.id.button5)).setOnClickListener(btnListener);
		((Button) findViewById(R.id.button5)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
		((Button) findViewById(R.id.button6)).setOnClickListener(btnListener);
		((Button) findViewById(R.id.button6)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
		((Button) findViewById(R.id.button7)).setOnClickListener(btnListener);
		((Button) findViewById(R.id.button7)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
		((Button) findViewById(R.id.button8)).setOnClickListener(btnListener);
		((Button) findViewById(R.id.button8)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
		((Button) findViewById(R.id.button9)).setOnClickListener(btnListener);
		((Button) findViewById(R.id.button9)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

		codeField1.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		codeField2.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		codeField3.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		codeField4.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);

		((TextView) findViewById(R.id.button_clear))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						clearFields();
					}
				});

		((TextView) findViewById(R.id.button_erase))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						onDeleteKey();
					}
				});

		overridePendingTransition(R.anim.slide_up, R.anim.zero);

		switch (type) {

		case Locker.DISABLE_PASSLOCK:
			this.setTitle("Disable Pin");
			break;
		case Locker.ENABLE_PASSLOCK:
			this.setTitle("Enable Pin");
			tvMessage.setText(getResources().getString(R.string.enter_passcode));
			break;
		case Locker.CHANGE_PASSWORD:
			this.setTitle("Change Pin");
			break;
		case Locker.UNLOCK_PASSWORD:
			this.setTitle("Unlock Pin");
			tvMessage.setText(getResources().getString(R.string.txtEnterPin));
			break;
		}

		if(isHeaderExist) {
			((ImageView) findViewById(R.id.btn_back)).setImageResource(MineTalk.isBlackTheme ? R.drawable.btn_top_before_wh : R.drawable.btn_top_before);
			((TextView) findViewById(R.id.tv_title)).setTextColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
			((RelativeLayout) findViewById(R.id.layout_header)).setBackgroundColor(getResources().getColor(MineTalk.isBlackTheme ? R.color.bk_theme_status : R.color.white_color));
			((TextView) findViewById(R.id.tv_warning)).setVisibility(View.VISIBLE);

			((ImageView) findViewById(R.id.btn_back)).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					setResult(RESULT_CANCELED);
					finish();
				}
			});
		}else{
			((TextView) findViewById(R.id.tv_warning)).setVisibility(View.GONE);
			findViewById(R.id.layout_header).setVisibility(View.GONE);
		}
	}

	private void initBroadCastReceiver() {
		IntentFilter newChatFilter = new IntentFilter();
		newChatFilter.addAction(MineTalk.BROAD_CAST_FINGER_MESSAGE);
		registerReceiver(mFingerLockReceiver, newChatFilter);
	}

	private BroadcastReceiver mFingerLockReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			boolean result = bundle.getBoolean(MineTalk.FINGER_LOCK_RESULT, false);
			String message = bundle.getString(MineTalk.FINGER_LOCK_MESSAGE, "");
			boolean restart = bundle.getBoolean(MineTalk.FINGER_LOCK_RESTART, false);

			if(restart){
				//System.exit(0);
			}

			if(result) { //인증 성공
				mFingerImg.setImageResource(MineTalk.isBlackTheme ? R.drawable.ic_fingerprint_ok_bk : R.drawable.ic_fingerprint_ok);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						MineTalkApp.getCurrentActivity().setResult(RESULT_OK);
						finish();
					}
				}, 500);

			}else{ //인증 실패
				mFingerImg.setImageResource(R.drawable.ic_fingerprint_wrong);
			}
		}
	};

	public int getType() {
		return type;
	}

	protected void onPasscodeInputed() {
		String passLock = codeField1.getText().toString()
				+ codeField2.getText().toString()
				+ codeField3.getText().toString() + codeField4.getText();

		switch (type) {

		case Locker.DISABLE_PASSLOCK:
			if (AppLocker.getInstance().getAppLock().checkPasscode(passLock)) {
				setResult(RESULT_OK);
				AppLocker.getInstance().getAppLock().setPasscode(null);
				finish();
			} else {
				onPasscodeError();
			}
			break;

		case Locker.ENABLE_PASSLOCK:
			if (oldPasscode == null) {
				tvMessage.setText(R.string.reenter_passcode);
				oldPasscode = passLock;

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						clearFields();
					}
				}, 300);

			} else {
				if (passLock.equals(oldPasscode)) {

					showMessageAlert(getResources().getString(R.string.pin_num_warning),
							getResources().getString(R.string.common_ok),
							new PopupListenerFactory.SimplePopupListener() {
								@Override
								public void onClick(DialogInterface f, int state) {
									if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
										setResult(RESULT_OK);
										AppLocker.getInstance().getAppLock()
												.setPasscode(passLock);
										finish();
									}
								}
							});

				} else {
					oldPasscode = null;
					tvMessage.setText(R.string.enter_passcode);
					onPasscodeError();
				}
			}
			break;

		case Locker.CHANGE_PASSWORD:
			if (AppLocker.getInstance().getAppLock().checkPasscode(passLock)) {
				tvMessage.setText(R.string.enter_passcode);
				type = Locker.ENABLE_PASSLOCK;
			} else {
				onPasscodeError();
			}
			break;

		case Locker.UNLOCK_PASSWORD:
			if (AppLocker.getInstance().getAppLock().checkPasscode(passLock)) {
				Intent intent = new Intent(MineTalk.BROAD_CAST_LOCK_SCREEN_PAUSE_MESSAGE);
				intent.putExtra("visible", false);
				MineTalkApp.getCurrentActivity().sendBroadcast(intent);

				AppLocker.getInstance().enableAppBySwitchActivity(true);
				setResult(RESULT_OK);
				finish();
			} else {
				onPasscodeError();
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		if(isHeaderExist) {
			setResult(RESULT_CANCELED);
			finish();
		}
	}

	protected void setupEditText(EditText editText) {
		editText.setInputType(InputType.TYPE_NULL);
		editText.setFilters(filters);
		editText.setOnTouchListener(touchListener);
		editText.setTransformationMethod(PasswordTransformationMethod
				.getInstance());
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_DEL) {
			onDeleteKey();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private void onDeleteKey() {
		if (codeField1.isFocused()) {
		} else if (codeField2.isFocused()) {
			codeField1.requestFocus();
			codeField1.setText("");
			codeField1.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		} else if (codeField3.isFocused()) {
			codeField2.requestFocus();
			codeField2.setText("");
			codeField2.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		} else if (codeField4.isFocused()) {
			codeField3.requestFocus();
			codeField3.setText("");
			codeField3.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		}
	}

	private OnClickListener btnListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			int currentValue = -1;
			int id = view.getId();
			if (id == R.id.button0) {
				currentValue = 0;
			} else if (id == R.id.button1) {
				currentValue = 1;
			} else if (id == R.id.button2) {
				currentValue = 2;
			} else if (id == R.id.button3) {
				currentValue = 3;
			} else if (id == R.id.button4) {
				currentValue = 4;
			} else if (id == R.id.button5) {
				currentValue = 5;
			} else if (id == R.id.button6) {
				currentValue = 6;
			} else if (id == R.id.button7) {
				currentValue = 7;
			} else if (id == R.id.button8) {
				currentValue = 8;
			} else if (id == R.id.button9) {
				currentValue = 9;
			} else {
			}

			// set the value and move the focus
			String currentValueString = String.valueOf(currentValue);
			if (codeField1.isFocused()) {
				codeField1.setText(currentValueString);
				codeField1.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_pressed_bk : R.drawable.edittext_pressed);
				codeField2.requestFocus();
				codeField2.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
			} else if (codeField2.isFocused()) {
				codeField2.setText(currentValueString);
				codeField2.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_pressed_bk : R.drawable.edittext_pressed);
				codeField3.requestFocus();
				codeField3.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
			} else if (codeField3.isFocused()) {
				codeField3.setText(currentValueString);
				codeField3.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_pressed_bk : R.drawable.edittext_pressed);
				codeField4.requestFocus();
				codeField4.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
			} else if (codeField4.isFocused()) {
				codeField4.setText(currentValueString);
				codeField3.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_pressed_bk : R.drawable.edittext_pressed);
			}

			if (codeField4.getText().toString().length() > 0
					&& codeField3.getText().toString().length() > 0
					&& codeField2.getText().toString().length() > 0
					&& codeField1.getText().toString().length() > 0) {
				codeField4.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_pressed_bk : R.drawable.edittext_pressed);
				onPasscodeInputed();
			}
		}
	};

	protected void onPasscodeError() {
		Encryptor.snackPeak(mCompatActivity,getString(R.string.passcode_wrong));

		Thread thread = new Thread() {
			public void run() {
				Animation animation = AnimationUtils.loadAnimation(
						LockActivity.this, R.anim.shake);
				findViewById(R.id.ll_applock).startAnimation(animation);
				clearFields();
			}
		};
		runOnUiThread(thread);
	}

	private InputFilter numberFilter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {

			if (source.length() > 1) {
				return "";
			}

			if (source.length() == 0) // erase
			{
				return null;
			}

			try {
				int number = Integer.parseInt(source.toString());
				if ((number >= 0) && (number <= 9))
					return String.valueOf(number);
				else
					return "";
			} catch (NumberFormatException e) {
				return "";
			}
		}
	};

	private OnTouchListener touchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			v.performClick();
			clearFields();
			return false;
		}
	};

	private void clearFields() {
		codeField1.setText("");
		codeField2.setText("");
		codeField3.setText("");
		codeField4.setText("");

		codeField1.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		codeField2.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		codeField3.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		codeField4.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.edittext_normal_bk : R.drawable.edittext_normal);
		
		codeField1.postDelayed(new Runnable() {

			@Override
			public void run() {
				codeField1.requestFocus();
			}
		}, 200);
	}

	//Cipher Init()
	@TargetApi(Build.VERSION_CODES.M)
	public boolean cipherInit(){
		try {
			cipher = Cipher.getInstance(
					KeyProperties.KEY_ALGORITHM_AES + "/"
							+ KeyProperties.BLOCK_MODE_CBC + "/"
							+ KeyProperties.ENCRYPTION_PADDING_PKCS7);
		} catch (NoSuchAlgorithmException |
				NoSuchPaddingException e) {
			throw new RuntimeException("Failed to get Cipher", e);
		}
		try {
			keyStore.load(null);
			SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
					null);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return true;
		} catch (KeyPermanentlyInvalidatedException e) {
			return false;
		} catch (KeyStoreException | CertificateException
				| UnrecoverableKeyException | IOException
				| NoSuchAlgorithmException | InvalidKeyException e) {
			throw new RuntimeException("Failed to init Cipher", e);
		}
	}

	//Key Generator
	@TargetApi(Build.VERSION_CODES.M)
	protected void generateKey() {
		try {
			keyStore = KeyStore.getInstance("AndroidKeyStore");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			throw new RuntimeException("Failed to get KeyGenerator instance", e);
		}

		try {
			keyStore.load(null);
			keyGenerator.init(new KeyGenParameterSpec.Builder(KEY_NAME,
					KeyProperties.PURPOSE_ENCRYPT |
							KeyProperties.PURPOSE_DECRYPT)
					.setBlockModes(KeyProperties.BLOCK_MODE_CBC)
					.setUserAuthenticationRequired(true)
					.setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
					.build());
			keyGenerator.generateKey();
		} catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | CertificateException | IOException e){
			throw new RuntimeException(e);
		}
	}
}