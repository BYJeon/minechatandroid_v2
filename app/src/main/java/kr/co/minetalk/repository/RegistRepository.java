package kr.co.minetalk.repository;

public class RegistRepository {
    private volatile static RegistRepository mInstance = null;

    public static RegistRepository getInstance() {
        if(mInstance == null) {
            synchronized (RegistRepository.class) {
                if (mInstance == null) {
                    mInstance = new RegistRepository();
                }
            }
        }
        return mInstance;
    }


    private String registType   = "";
    private String providerType = "";
    private String providerId   = "";
    private String userLoginId  = "";
    private String userName     = "";
    private String userEmail    = "";
    private String userProfileImageUrl = "";
    private String userPassword = "";

    public void initialize() {
        this.registType   = "";
        this.providerType = "";
        this.providerId   = "";
        this.userName     = "";
        this.userEmail    = "";
        this.userLoginId  = "";

        this.userProfileImageUrl = "";
    }

    public String getRegistType() {
        return registType;
    }

    public void setRegistType(String registType) {
        this.registType = registType;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLodginId) {
        this.userLoginId = userLodginId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserProfileImageUrl() {
        return userProfileImageUrl;
    }

    public void setUserProfileImageUrl(String userProfileImageUrl) {
        this.userProfileImageUrl = userProfileImageUrl;
    }

    public static RegistRepository getmInstance() {
        return mInstance;
    }

    public static void setmInstance(RegistRepository mInstance) {
        RegistRepository.mInstance = mInstance;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
