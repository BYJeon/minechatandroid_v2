package kr.co.minetalk.repository;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.utils.Preferences;

public class CountryRepository {

    private volatile static CountryRepository mInstance = null;

    public static CountryRepository getInstance() {
        if(mInstance == null) {
            synchronized (CountryRepository.class) {
                if (mInstance == null) {
                    mInstance = new CountryRepository();
                    mInstance.initialize();
                }
            }
        }
        return mInstance;
    }

    private ArrayList<CountryInfoModel> mCountryList = new ArrayList<>();

    /**
     * 초기화
     */
    public void initialize() {
        this.mCountryList.clear();
        this.mCountryList.add(new CountryInfoModel("Korean", "한국어", "flag_kor", "ko", "ko-KR"));
        this.mCountryList.add(new CountryInfoModel("ENG", "영어", "flag_eng","en", "en-US"));
        this.mCountryList.add(new CountryInfoModel("Chinese", "중국어", "flag_chn","zh-CN", "cmn-Hans-CN"));
        this.mCountryList.add(new CountryInfoModel("Japanese", "일본어", "flag_jpn", "ja", "ja-JP"));
        this.mCountryList.add(new CountryInfoModel("Malay", "말레이어(말레이시아)", "flag_mys", "ms", "ms-MY"));  //
        this.mCountryList.add(new CountryInfoModel("Tiếng Việt", "베트남어", "flag_vnm","vi", "vi-VN"));
        this.mCountryList.add(new CountryInfoModel("Thai", "타이어", "flag_tha", "th", "th-TH"));
        this.mCountryList.add(new CountryInfoModel("Mongolian", "몽골어", "flag_mon", "mn", ""));   // 없음

        this.mCountryList.add(new CountryInfoModel("Galician", "갈리시아어", "flag_gal", "gl", "gl-ES"));
        this.mCountryList.add(new CountryInfoModel("Gujarati", "구자라트어", "flag_guj", "gu", "gu-IN"));
        this.mCountryList.add(new CountryInfoModel("Greek", "그리스어", "flag_grc", "el", "el-GR"));
        this.mCountryList.add(new CountryInfoModel("Dutch", "네덜란드어", "flag_nld", "nl", "nl-NL"));
        this.mCountryList.add(new CountryInfoModel("Nepali", "네팔어", "flag_nep", "ne", "ne-NP"));
        this.mCountryList.add(new CountryInfoModel("Norwegian", "노르웨이어", "flag_nor", "no", "nb-NO"));
        this.mCountryList.add(new CountryInfoModel("Danish", "덴마크어", "flag_dnk", "da", "da-DK"));
        this.mCountryList.add(new CountryInfoModel("German", "독일어", "flag_deu", "de", "de-DE"));
        this.mCountryList.add(new CountryInfoModel("Lao", "라오어(라오스)", "flag_lao", "lo", "lo-LA"));
        this.mCountryList.add(new CountryInfoModel("Latvian", "라트비아어", "flag_lva", "lv", "lv-LV"));
        this.mCountryList.add(new CountryInfoModel("Latin", "라틴어", "flag_lat", "la", "")); // 없음
        this.mCountryList.add(new CountryInfoModel("Russian", "러시아어", "flag_rus", "ru", "ru-RU"));
        this.mCountryList.add(new CountryInfoModel("Romanian", "루마니아어", "flag_rou", "ro", "ro-RO"));
        this.mCountryList.add(new CountryInfoModel("Lithuanian", "리투아니아어", "flag_ltu", "lt", "lt-LT"));
        this.mCountryList.add(new CountryInfoModel("Marathi", "마라티어", "flag_mar", "mr", "mr-IN"));
        this.mCountryList.add(new CountryInfoModel("Maori", "마오리어", "flag_mao", "mi", ""));  // 없음
        this.mCountryList.add(new CountryInfoModel("Macedonian", "마케도니아어", "flag_mkd", "mk", ""));   // 없음

        this.mCountryList.add(new CountryInfoModel("Maltese", "몰타어", "flag_mal", "ml", "ml-IN")); //  ml-IN 확인요청
        this.mCountryList.add(new CountryInfoModel("Hmong", "몽어", "flag_hmo", "hmn", ""));
        this.mCountryList.add(new CountryInfoModel("Myanmar", "미얀마", "flag_mya", "my", ""));
        this.mCountryList.add(new CountryInfoModel("Basque","바스크어","flag_bas", "eu", "eu-ES"));

        this.mCountryList.add(new CountryInfoModel("Belarusian",  "벨라루스어","flag_bel", "be", ""));   // 없음
        this.mCountryList.add(new CountryInfoModel("Bengali","벵골어(방글라데시아)","flag_ben", "bn", "bn-BD"));
        this.mCountryList.add(new CountryInfoModel("Bosnian","보스니아어","flag_bos", "bs", "")); // 없음
        this.mCountryList.add(new CountryInfoModel("Bulgarian","불가리아어","flag_bgr", "bg", "bg-BG"));
        this.mCountryList.add(new CountryInfoModel("Serbian", "세르비아어", "flag_seb", "sr", "sr-RS"));
        this.mCountryList.add(new CountryInfoModel("Cebuano","세부어","flag_ceb", "ceb", "")); // 없음
        this.mCountryList.add(new CountryInfoModel("Somali", "소말리어", "flag_som", "so", "")); // 없음
        this.mCountryList.add(new CountryInfoModel("Swahili", "스와힐리어(탄자니아)", "flag_swa", "sw", "sw-TZ"));
        this.mCountryList.add(new CountryInfoModel("Swedish", "스웨덴어", "flag_swe", "sv", "sv-SE"));
        this.mCountryList.add(new CountryInfoModel("Spanish", "스페인어", "flag_esp", "es", "es-ES"));
        this.mCountryList.add(new CountryInfoModel("Slovak", "슬로바키아어", "flag_svk", "sk", "sk-SK"));
        this.mCountryList.add(new CountryInfoModel("Slovenian", "슬로베니아어", "flag_slo", "sl", "sl-SI"));
        this.mCountryList.add(new CountryInfoModel("Arabic","아랍어","flag_ara", "ar", ""));  // 종류가 많음
        this.mCountryList.add(new CountryInfoModel("Armenian",    "아르메니아어",  "flag_arm", "hy", "hy-AM"));
        this.mCountryList.add(new CountryInfoModel("Icelandic", "아이슬란드어", "flag_ice", "is", "is-IS"));
        this.mCountryList.add(new CountryInfoModel("Irish", "아일랜드어", "flag_irl", "ga", "")); // 없음
        this.mCountryList.add(new CountryInfoModel("Azerbaijani", "아제르바이잔어", "flag_aze", "az", "az-AZ"));
        this.mCountryList.add(new CountryInfoModel("Afrikaans",   "아프리칸스어(남아프리카공화국)",   "flag_afr","af", "af-ZA"));
        this.mCountryList.add(new CountryInfoModel("Albanian","알바니아어","flag_alb", "sq", ""));  // 없음
        this.mCountryList.add(new CountryInfoModel("Estonian", "에스토니아어", "flag_est", "et" , ""));    // 없음
        this.mCountryList.add(new CountryInfoModel("Esperanto", "에스페란토", "flag_epo", "eo", "")); // 없음

        this.mCountryList.add(new CountryInfoModel("Yoruba", "요루바어", "flag_yor", "yo", ""));     // 없음
        this.mCountryList.add(new CountryInfoModel("Urdu", "우르두어(파키스탄)", "flag_urd", "ur", "ur-PK"));
        this.mCountryList.add(new CountryInfoModel("Ukrainian", "우크라이나어", "flag_ukr", "uk", "uk-UA"));
        this.mCountryList.add(new CountryInfoModel("Welsh", "웨일스어", "flag_wel", "cy", ""));  // 없음
        this.mCountryList.add(new CountryInfoModel("Igbo", "이그보어", "flag_igb", "ig", ""));   // 없음
        this.mCountryList.add(new CountryInfoModel("Yiddish", "이디시어", "flag_yid", "yi", ""));    // 없음
        this.mCountryList.add(new CountryInfoModel("Italian", "이탈리아어", "flag_ita", "it", "it-IT"));
        this.mCountryList.add(new CountryInfoModel("bahasa Indonesia", "인도네시아어", "flag_idn","in", "id-ID"));

        this.mCountryList.add(new CountryInfoModel("Javanese", "자바어", "flag_jav", "jw", "jv-ID"));
        this.mCountryList.add(new CountryInfoModel("Georgian", "조지아어", "flag_geo", "ka", "ka-GE"));
        this.mCountryList.add(new CountryInfoModel("Zulu", "줄루어", "flag_zul", "zu", "zu-ZA"));

        this.mCountryList.add(new CountryInfoModel("Czech", "체코어", "flag_cze", "cs", "cs-CZ"));
        this.mCountryList.add(new CountryInfoModel("Catalan","카탈루냐어","flag_cat", "ca", "ca-ES"));
        this.mCountryList.add(new CountryInfoModel("Kannada", "칸나다어(인도카르나타카주)", "flag_kan", "kn", "kn-IN"));
        this.mCountryList.add(new CountryInfoModel("Croatian", "크로아티아어", "flag_hrv", "hr", "hr-HR"));
        this.mCountryList.add(new CountryInfoModel("Khmer", "크메르어(캄보디아)", "flag_khm", "km", "km-KH"));
        this.mCountryList.add(new CountryInfoModel("Tamil", "타밀어(드라비다어족)", "flag_tam", "ta", "")); // 종류가 많음. 정확한 지역 확인요청

        this.mCountryList.add(new CountryInfoModel("Turkish", "터키어", "flag_tur", "tr", "tr-TR"));
        this.mCountryList.add(new CountryInfoModel("Telugu", "텔루구어", "flag_tel", "te", "te-IN"));
        this.mCountryList.add(new CountryInfoModel("Punjabi", "펀자브어", "flag_pun", "pa", "es-PA"));
        this.mCountryList.add(new CountryInfoModel("Persian", "페르시아어", "flag_per", "fa", "fa-IR"));
        this.mCountryList.add(new CountryInfoModel("Portuguese", "포르투갈어", "flag_prt", "pt", "pt-PT"));
        this.mCountryList.add(new CountryInfoModel("Polish", "폴란드어", "flag_pol", "pl", "pl-PL"));
        this.mCountryList.add(new CountryInfoModel("French", "프랑스어", "flag_fra", "fr", "fr-FR"));
        this.mCountryList.add(new CountryInfoModel("Finnish", "핀란드어", "flag_fin", "fi", "fi-FI"));
        this.mCountryList.add(new CountryInfoModel("Hausa", "하우사어", "flag_hau", "ha", ""));  // 없음

        this.mCountryList.add(new CountryInfoModel("Hungarian", "헝가리어", "flag_hun", "hu", "hu-HU"));
        this.mCountryList.add(new CountryInfoModel("Hebrew", "히브리어(이스라엘)", "flag_heb", "iw", "he-IL"));
        this.mCountryList.add(new CountryInfoModel("Hindi", "힌디어(인도)", "flag_hin", "hi", "hi-IN"));
        this.mCountryList.add(new CountryInfoModel("Haitian", "Creole 이티 크리올어", "flag_hai", "ht", "")); // 없음

        CountryInfoModel defaultLang = getCountryModelWithRemove(Locale.getDefault().getLanguage());
        this.mCountryList.add(0, defaultLang);
    }


    public ArrayList<CountryInfoModel> getCountryList() {
        return this.mCountryList;
    }


    /**
     *
     * @param languageCode 언어코드
     * @return
     */
    public CountryInfoModel getCountryModel(String languageCode) {
        for(int i = 0 ; i < mCountryList.size() ; i++) {
            if(mCountryList.get(i).getCode().equals(languageCode)) {
                return mCountryList.get(i);
            }
        }
        return mCountryList.get(0);
    }

    public CountryInfoModel getCountryModelWithRemove(String languageCode) {
        CountryInfoModel model = null;
        for(int i = 0 ; i < mCountryList.size() ; i++) {
            if(mCountryList.get(i).getCode().equals(languageCode)) {
                model = mCountryList.get(i);
                mCountryList.remove(i);
                break;
            }
        }
        return model;
    }

    public CountryInfoModel getItem(int index) {
        return this.mCountryList.get(index);
    }

    public ArrayList<CountryInfoModel> getCountryListAll() {
        return this.mCountryList;
    }


    public ArrayList<CountryInfoModel> getOtherCountry(int type) {
        ArrayList<CountryInfoModel> tempCountryList = new ArrayList<>();

        String myLanguageCode = "";
        if(type == 0) {
            myLanguageCode = Preferences.getMyChatLanguage();
        } else {
            myLanguageCode = Preferences.getChatTranslationLanguageCode();
        }
        for(int i = 0 ; i < this.mCountryList.size() ; i++) {

            CountryInfoModel item = new CountryInfoModel(mCountryList.get(i).getCountryNameOther(), mCountryList.get(i).getCountryName(),
                    mCountryList.get(i).getFlag_image(), mCountryList.get(i).getCode(), mCountryList.get(i).getVoice_code());

            if(myLanguageCode != null && myLanguageCode.equals(mCountryList.get(i).getCode())) {
                item.setSelect(true);
            } else {
                item.setSelect(false);
            }

            tempCountryList.add(item);
        }

        return tempCountryList;
    }

}
