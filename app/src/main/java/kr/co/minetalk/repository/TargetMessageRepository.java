package kr.co.minetalk.repository;

import java.util.HashMap;

public class TargetMessageRepository {

    private volatile static TargetMessageRepository mInstance = null;

    public static TargetMessageRepository getInstance() {
        if(mInstance == null) {
            synchronized (TargetMessageRepository.class) {
                if (mInstance == null) {
                    mInstance = new TargetMessageRepository();
                    mInstance.initialize();
                }
            }
        }
        return mInstance;
    }

    private HashMap<String, String> mGenderMap = new HashMap<>();
    private HashMap<String, String> mAddressMap = new HashMap<>();
    private HashMap<String, String> mAgeMap = new HashMap<>();
    private HashMap<String, String> mJobMap = new HashMap<>();

    public void initialize() {
        mGenderMap.clear();
        mAddressMap.clear();
        mAgeMap.clear();
        mJobMap.clear();
    }

    public void addGender(String key, String value) {
        mGenderMap.put(key, value);
    }

    public void removeGender(String key) {
        mGenderMap.remove(key);
    }

    public boolean containGender(String key) {
        return mGenderMap.containsKey(key);
    }

    public void addAddress(String key, String value) {
        mAddressMap.put(key, value);
    }

    public void removeAddress(String key) {
        mAddressMap.remove(key);
    }

    public boolean containAddress(String key) {
        return mAddressMap.containsKey(key);
    }

    public void addAge(String key, String value) {
        mAgeMap.put(key, value);
    }

    public void removeAge(String key) {
        mAgeMap.remove(key);
    }

    public boolean containAge(String key) {
        return mAgeMap.containsKey(key);
    }

    public void addJob(String key, String value) {
        mJobMap.put(key, value);
    }

    public void removeJob(String key) {
        mJobMap.remove(key);
    }

    public boolean containJob(String key) {
        return mJobMap.containsKey(key);
    }




    public HashMap<String, String> getGenderMap() {
        return mGenderMap;
    }

    public HashMap<String, String> getAddressMap() {
        return mAddressMap;
    }

    public HashMap<String, String> getAgeMap() {
        return mAgeMap;
    }

    public HashMap<String, String> getJobMap() {
        return mJobMap;
    }
}
