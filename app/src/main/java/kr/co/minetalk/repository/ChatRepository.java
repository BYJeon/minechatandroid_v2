package kr.co.minetalk.repository;

import java.util.HashMap;

public class ChatRepository {

    private volatile static ChatRepository mInstance = null;

    public static ChatRepository getInstance() {
        if(mInstance == null) {
            synchronized (ChatRepository.class) {
                if (mInstance == null) {
                    mInstance = new ChatRepository();
                    mInstance.initialize();
                }
            }
        }
        return mInstance;
    }

    private HashMap<String, String> mFriendXidInfoMap = new HashMap<>();
    private String mThreadKey = "";

    public void initialize() {
        mFriendXidInfoMap.clear();
    }


    public void addUserInfo(String xid, String userName) {
        mFriendXidInfoMap.put(xid, userName);
    }

    public boolean containUser(String xid) {
        return mFriendXidInfoMap.containsKey(xid);
    }

    public int chattingRoomMemberCount() {
        return mFriendXidInfoMap.size();
    }

    public void setThreadKey(String threadKey) {
        this.mThreadKey = threadKey;
    }

    public String getThreadKey() {
        return mThreadKey;
    }

    public HashMap<String, String> getmFriendXidInfoMap() {
        return mFriendXidInfoMap;
    }
}
