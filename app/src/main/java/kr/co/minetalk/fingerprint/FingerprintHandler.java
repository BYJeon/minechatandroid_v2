package kr.co.minetalk.fingerprint;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.util.Log;
import android.widget.Toast;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;

@TargetApi(Build.VERSION_CODES.M)//이녀석은 또 처음 보는군.
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback{
    CancellationSignal cancellationSignal;
    private Context context;

    public FingerprintHandler(Context context){
        this.context = context;
    }

    //메소드들 정의
    public void startAutho(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject){
        cancellationSignal = new CancellationSignal();
        fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        Intent intent = new Intent(MineTalk.BROAD_CAST_FINGER_MESSAGE);
        intent.putExtra(MineTalk.FINGER_LOCK_RESTART, true);
        MineTalkApp.getCurrentActivity().sendBroadcast(intent);

        this.update(MineTalkApp.getCurrentActivity().getResources().getString(R.string.finger_lock_auth_error) + errString, false);
    }

    @Override
    public void onAuthenticationFailed() {
        this.update(MineTalkApp.getCurrentActivity().getResources().getString(R.string.finger_lock_auth_fail), false);
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        this.update(helpString.toString(), false);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update(MineTalkApp.getCurrentActivity().getResources().getString(R.string.finger_lock_auth_sucess), true);
    }

    public void stopFingerAuth(){
        if(cancellationSignal != null && !cancellationSignal.isCanceled()){
            cancellationSignal.cancel();
        }
    }

    private void update(String s, boolean b) {
        //Log.d("WW", s);

        Toast.makeText(MineTalkApp.getAppContext(), s, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MineTalk.BROAD_CAST_FINGER_MESSAGE);
        intent.putExtra(MineTalk.FINGER_LOCK_RESULT, b);
        intent.putExtra(MineTalk.FINGER_LOCK_MESSAGE, s);
        MineTalkApp.getCurrentActivity().sendBroadcast(intent);

//        final TextView tv_message = (TextView) ((A
//        ctivity)context).findViewById(R.id.tv_message);
//        final ImageView iv_fingerprint = (ImageView) ((Activity)context).findViewById(R.id.iv_fingerprint);
//        final LinearLayout linearLayout = (LinearLayout) ((Activity)context).findViewById(R.id.ll_secure);
//
//        //안내 메세지 출력
//        tv_message.setText(s);
//
//        if(b == false){
//            tv_message.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
//        } else {//지문인증 성공
//            tv_message.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
//            iv_fingerprint.setImageResource(R.mipmap.ic_done);
//            linearLayout.setVisibility(LinearLayout.VISIBLE);
//
//            //sound effect
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Ringtone r = RingtoneManager.getRingtone((Activity)context, notification);
//            r.play();
//        }
    }
}
