package kr.co.minetalk.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;


import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.MainActivity;
import kr.co.minetalk.activity.SplashActivity;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.ThreadListApi;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.utils.Preferences;


/**
 * Created by episode on 2018. 10. 8..
 */

public class FireBaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
//        Timber.d("From: " + remoteMessage.getFrom());


        // Check if message contains a data payload.
        Map<String, String> data = remoteMessage.getData();
        if (data.size() > 0) {
            Log.e("@@@TAG", "message data payload : " + data.toString());

            String category_type = data.get("category_type");
            if(category_type.equals("1")) {
                String push_info = data.get("push_info");
                try {
                    JSONObject pushObject = new JSONObject(push_info);
                    String title = pushObject.getString("title");
                    String body = pushObject.getString("body");
                    String image = pushObject.getString("image");
                    String ad_idx = pushObject.getString("ad_idx");

                    sendNotification(title, StringEscapeUtils.unescapeJava(body), image, "", ad_idx, "");


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if(category_type.equals("2")) {
                String push_info = data.get("push_info");
                try {
                    JSONObject pushObject = new JSONObject(push_info);
                    String title = getString(R.string.app_name);
                    String body = pushObject.getString("body");
                    String userName = pushObject.getString("user_name");
                    String threadKey = pushObject.getString("thread_key");
                    String securityThreadKey = pushObject.getString("is_security");

                    //MineTalk.PUSH_SECURITY_THREAD_KEY = securityThreadKey;

                    MessageJSonModel messageJSonModel = new MessageJSonModel();
                    messageJSonModel = MessageJSonModel.parse(new JSONObject(body));

                    String message = messageJSonModel.getText();
//                    sendNotification(title, userName + " : " + StringEscapeUtils.unescapeJava(message), null, threadKey, "");


                    sendNotification(title, userName + " : " + message, null, threadKey, "", securityThreadKey);
                    sendBroadcast(new Intent(MineTalk.BROAD_CAST_NEW_MESSAGE));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if(category_type.equals("3")) {
                String push_info = data.get("push_info");
                try {
                    JSONObject pushObject = new JSONObject(push_info);
                    String title = pushObject.getString("title");
                    String body = pushObject.getString("body");
                    String image = pushObject.getString("image");

                    sendNotification(title, StringEscapeUtils.escapeJava(body), image, "", "", "");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if(category_type.equals("4")) {
                String push_info = data.get("push_info");
                try {
                    JSONObject pushObject = new JSONObject(push_info);
                    String title = pushObject.getString("title");
                    String body = pushObject.getString("body");

                    sendNotification(title, body, "", "", "", "");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if(category_type.equals("5")) { //메세지 삭제 푸쉬
                String push_info = data.get("push_info");
                try {
                    JSONObject pushObject = new JSONObject(push_info);
                    String title = getString(R.string.app_name);
                    String body = pushObject.getString("body");
                    String userName = pushObject.getString("user_name");
                    String threadKey = pushObject.getString("thread_key");
                    String seq = pushObject.getString("seq");

                    MineTalk.PUSH_DELETE_SEQ = seq;

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
                sendBroadcast(new Intent(MineTalk.BROAD_CAST_DELETE_MESSAGE));
                return;
            }else if(category_type.equals("6")) { //보안채팅 방나가기 푸쉬
                String push_info = data.get("push_info");
                try {
                    JSONObject pushObject = new JSONObject(push_info);
                    String threadKey = pushObject.getString("thread_key");

                    MineTalk.PUSH_THREAD_KEY = "";
                    MineTalk.PUSH_DELETE_THREAD_KEY = threadKey;

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(MineTalk.PUSH_DELETE_THREAD_KEY, 0);

                sendBroadcast(new Intent(MineTalk.BROAD_CAST_SECURITY_CHAT_OUT_MESSAGE));
                return;
            }


        }

        try {
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();

            sendNotification(title, body, null, "", "", "");
        } catch (Exception e) {
            e.printStackTrace();
        }



        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            sendNotification(remoteMessage.getNotification());
//        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void sendNotification(String title, String body, String image, String threadKey, String adIdx, String securityMode) {

        Intent intent = new Intent(this, MainActivity.class);

        MineTalk.PUSH_THREAD_KEY = threadKey;

        if(!threadKey.equals("")) {
            Log.d("Security threadKey : ", threadKey);
            intent.putExtra("threadKey", threadKey);
        }

        if(!securityMode.equals("")) {
            Log.d("Security Mode : ", securityMode);
            intent.putExtra("securityMode", securityMode);
        }

        if(!adIdx.equals("")) {
            MineTalk.AD_IDX = adIdx;
            MineTalk.AD_TITLE = title;

            intent.putExtra("adIdx", adIdx);
            intent.putExtra("adTitle", title);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestID /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        if (!TextUtils.isEmpty(title)) {
            notificationBuilder.setContentTitle(title);
        }
        if (!TextUtils.isEmpty(body)) {
            notificationBuilder.setContentText(body);
        }


        if(image == null || image.equals("")) {
            NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle(notificationBuilder);
            style.bigText(body).setBigContentTitle(title);
        } else {
            Bitmap bigPicture = null;
            try {
                URL url = new URL(image);
                bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (Exception e) {
                e.printStackTrace();
                bigPicture = null;
            }

            if(bigPicture != null) {
                NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle(notificationBuilder);
                bigPictureStyle.bigPicture(bigPicture).setBigContentTitle(title).setSummaryText(body);
            } else {
                NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle(notificationBuilder);
                style.bigText(body).setBigContentTitle(title);
            }
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }


        //long seed = System.currentTimeMillis();
        //Random rand = new Random(seed);


        if(!threadKey.equals("")) {
            notificationManager.notify(threadKey, 0 /* ID of notification */, notificationBuilder.build());
        } else {
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }
}
