package kr.co.minetalk.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.utils.CommonUtils;

public class FriendDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "minetalk_setting";

    private static final int DATABASE_VERSION = 3;

    private static final String TABLE_FRIEND_LIST_NAME = "FRIEND_LIST_TABLE";
    private static final String TABLE_FRIEND_LIST_NICK_NAME = "FRIEND_LIST_NICK_TABLE";

    private static final String TABLE_SAVE_TRANS_LIST = "SAVE_TRANS_TABLE";

    private static final String KEY_FRIEND_LIST_USER_XID = "USER_XID";
    private static final String KEY_FRIEND_LIST_USER_NAME = "USER_NAME";
    private static final String KEY_FRIEND_LIST_USER_HP = "USER_HP";
    private static final String KEY_FRIEND_LIST_USER_PROFILE_IMAGE = "USER_PROFILE_IMAGE";
    private static final String KEY_FRIEND_LIST_USER_STATE_MESSAGE = "USER_STATE_MESSAGE";
    private static final String KEY_FRIEND_LiST_USER_FAVORITE_YN = "USER_FAVORITE_YN";
    private static final String KEY_FRIEND_LiST_USER_NATION_CODE = "USER_NATION_CODE";

    public FriendDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String T_FRIEND_LIST_CREATE = "CREATE TABLE IF NOT EXISTS "+ TABLE_FRIEND_LIST_NAME +" (\n" +
                "  USER_XID char(32) PRIMARY KEY UNIQUE NOT NULL,\n" +
                "  USER_NAME char(50),\n" +
                "  USER_HP text,\n" +
                "  USER_PROFILE_IMAGE text,\n" +
                "  USER_STATE_MESSAGE text,\n" +
                "  USER_FAVORITE_YN text,\n" +
                "  USER_NATION_CODE text\n" +
                ");";

        db.execSQL(T_FRIEND_LIST_CREATE);

        String T_FRIEND_NICK_LIST_CREATE = "CREATE TABLE IF NOT EXISTS "+ TABLE_FRIEND_LIST_NICK_NAME +" (\n" +
                "  USER_XID char(32) PRIMARY KEY UNIQUE NOT NULL,\n" +
                "  USER_NAME char(50),\n" +
                "  USER_HP text,\n" +
                "  USER_PROFILE_IMAGE text,\n" +
                "  USER_STATE_MESSAGE text,\n" +
                "  USER_FAVORITE_YN text,\n" +
                "  USER_NATION_CODE text\n" +
                ");";

        db.execSQL(T_FRIEND_NICK_LIST_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE IF EXISTS "+ TABLE_FRIEND_LIST_NAME);
        //db.execSQL("DROP TABLE IF EXISTS "+ TABLE_FRIEND_LIST_NICK_NAME);
        onCreate(db);
    }

    public void addFriendList(ArrayList<FriendListModel> friendListModels) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        for (int i = 0 ; i < friendListModels.size() ; i++) {
            insertFriend(db, friendListModels.get(i));
            insertFriendNickName(db, friendListModels.get(i));
        }

        db.setTransactionSuccessful();
        db.endTransaction();

    }

    public ArrayList<FriendListModel> loadFriendList() {
        SQLiteDatabase db = this.getReadableDatabase();

        return selectFriendList(db);
    }

    public HashMap<String, String> loadFriendMap() {
        SQLiteDatabase db = this.getReadableDatabase();

        return  selectFriendMap(db);
    }

    public HashMap<String, String> loadFriendNickNameMap() {
        SQLiteDatabase db = this.getReadableDatabase();

        return  selectFriendNickNameMap(db);
    }

    public HashMap<String, String> loadFriendXidToNameMap() {
        SQLiteDatabase db = this.getReadableDatabase();

        return  selectFriendXidMap(db);
    }

    public ArrayList<FriendListModel> loadFavoriteList() {
        SQLiteDatabase db = this.getReadableDatabase();

        return selectFovoriteList(db, "Y");
    }


    public ArrayList<FriendListModel> selectFriendList(SQLiteDatabase db) {

        ArrayList<FriendListModel> friendList = new ArrayList<>();

        Cursor cursor = db.query(TABLE_FRIEND_LIST_NAME, new String[] {
                KEY_FRIEND_LIST_USER_XID,
                KEY_FRIEND_LIST_USER_NAME,
                KEY_FRIEND_LIST_USER_HP,
                KEY_FRIEND_LIST_USER_PROFILE_IMAGE,
                KEY_FRIEND_LIST_USER_STATE_MESSAGE,
                KEY_FRIEND_LiST_USER_FAVORITE_YN,
                KEY_FRIEND_LiST_USER_NATION_CODE,
//        }, null, null, null, null, null, null);
        }, null, null, null, null, "CASE WHEN substr(USER_NAME ,0, 1) BETWEEN 'ㄱ' AND '힣' THEN 1"+
                " WHEN substr(USER_NAME, 0, 1) BETWEEN 'A' AND 'Z' THEN 2" +
                " WHEN substr(USER_NAME, 0, 1) BETWEEN 'a' AND 'z' THEN 2" +
                " ELSE 3 " +
                " END," +
                " USER_NAME COLLATE LOCALIZED ASC");

        if(cursor != null) {
            if(cursor.getCount() > 0 ) {
                if(cursor.moveToFirst()) {
                    do {
                        FriendListModel threadMember = new FriendListModel();

                        threadMember.setUser_xid(cursor.getString(0));
                        threadMember.setUser_name(cursor.getString(1));
                        threadMember.setUser_hp(cursor.getString(2));
                        threadMember.setUser_profile_image(cursor.getString(3));
                        threadMember.setUser_state_message(cursor.getString(4));
                        threadMember.setFavorite_yn(cursor.getString(5));
                        threadMember.setUser_nation_code(cursor.getString(6));

                        friendList.add(threadMember);
                    } while (cursor.moveToNext());
                }
            }

            cursor.close();
        }

        return friendList;
    }

    public HashMap<String, String> selectFriendMap(SQLiteDatabase db) {
        HashMap<String, String> friendMap = new HashMap<>();

        Cursor cursor = db.query(TABLE_FRIEND_LIST_NAME, new String[] {
                KEY_FRIEND_LIST_USER_XID,
                KEY_FRIEND_LIST_USER_NAME,
                KEY_FRIEND_LIST_USER_HP,
                KEY_FRIEND_LIST_USER_PROFILE_IMAGE,
                KEY_FRIEND_LIST_USER_STATE_MESSAGE,
                KEY_FRIEND_LiST_USER_FAVORITE_YN,
                KEY_FRIEND_LiST_USER_NATION_CODE,
        }, null, null, null, null, null, null);

        if(cursor != null) {
            if(cursor.getCount() > 0 ) {
                if(cursor.moveToFirst()) {
                    do {
                        FriendListModel threadMember = new FriendListModel();

                        threadMember.setUser_xid(cursor.getString(0));
                        threadMember.setUser_name(cursor.getString(1));
                        threadMember.setUser_hp(cursor.getString(2));
                        threadMember.setUser_profile_image(cursor.getString(3));
                        threadMember.setUser_state_message(cursor.getString(4));
                        threadMember.setFavorite_yn(cursor.getString(5));
                        threadMember.setUser_nation_code(cursor.getString(6));

                        friendMap.put(threadMember.getUser_hp(), threadMember.getUser_name());
                    } while (cursor.moveToNext());
                }
            }

            cursor.close();
        }

        return friendMap;
    }

    public HashMap<String, String> selectFriendNickNameMap(SQLiteDatabase db) {
        HashMap<String, String> friendMap = new HashMap<>();

        Cursor cursor = db.query(TABLE_FRIEND_LIST_NICK_NAME, new String[] {
                KEY_FRIEND_LIST_USER_XID,
                KEY_FRIEND_LIST_USER_NAME,
                KEY_FRIEND_LIST_USER_HP,
                KEY_FRIEND_LIST_USER_PROFILE_IMAGE,
                KEY_FRIEND_LIST_USER_STATE_MESSAGE,
                KEY_FRIEND_LiST_USER_FAVORITE_YN,
                KEY_FRIEND_LiST_USER_NATION_CODE,
        }, null, null, null, null, null, null);

        if(cursor != null) {
            if(cursor.getCount() > 0 ) {
                if(cursor.moveToFirst()) {
                    do {
                        FriendListModel threadMember = new FriendListModel();

                        threadMember.setUser_xid(cursor.getString(0));
                        threadMember.setUser_name(cursor.getString(1));
                        threadMember.setUser_hp(cursor.getString(2));
                        threadMember.setUser_profile_image(cursor.getString(3));
                        threadMember.setUser_state_message(cursor.getString(4));
                        threadMember.setFavorite_yn(cursor.getString(5));
                        threadMember.setUser_nation_code(cursor.getString(6));

                        friendMap.put(threadMember.getUser_hp(), threadMember.getUser_name());
                    } while (cursor.moveToNext());
                }
            }

            cursor.close();
        }

        return friendMap;
    }

    public HashMap<String, String> selectFriendXidMap(SQLiteDatabase db) {
        HashMap<String, String> friendMap = new HashMap<>();

        try {
            Cursor cursor = db.query(TABLE_FRIEND_LIST_NAME, new String[]{
                    KEY_FRIEND_LIST_USER_XID,
                    KEY_FRIEND_LIST_USER_NAME,
                    KEY_FRIEND_LIST_USER_HP,
                    KEY_FRIEND_LIST_USER_PROFILE_IMAGE,
                    KEY_FRIEND_LIST_USER_STATE_MESSAGE,
                    KEY_FRIEND_LiST_USER_FAVORITE_YN,
                    KEY_FRIEND_LiST_USER_NATION_CODE,
            }, null, null, null, null, null, null);

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    if (cursor.moveToFirst()) {
                        do {
                            FriendListModel threadMember = new FriendListModel();

                            threadMember.setUser_xid(cursor.getString(0));
                            threadMember.setUser_name(cursor.getString(1));
                            threadMember.setUser_hp(cursor.getString(2));
                            threadMember.setUser_profile_image(cursor.getString(3));
                            threadMember.setUser_state_message(cursor.getString(4));
                            threadMember.setFavorite_yn(cursor.getString(5));
                            threadMember.setUser_nation_code(cursor.getString(6));

                            friendMap.put(threadMember.getUser_xid(), threadMember.getUser_name());
                        } while (cursor.moveToNext());
                    }
                }

                cursor.close();
            }
        }catch (SQLiteException e){
            e.printStackTrace();
        }
        return friendMap;
    }


    public ArrayList<FriendListModel> selectFovoriteList(SQLiteDatabase db, String favorite_yn) {

        ArrayList<FriendListModel> friendList = new ArrayList<>();

        Cursor cursor = db.query(TABLE_FRIEND_LIST_NAME, new String[] {
                KEY_FRIEND_LIST_USER_XID,
                KEY_FRIEND_LIST_USER_NAME,
                KEY_FRIEND_LIST_USER_HP,
                KEY_FRIEND_LIST_USER_PROFILE_IMAGE,
                KEY_FRIEND_LIST_USER_STATE_MESSAGE,
                KEY_FRIEND_LiST_USER_FAVORITE_YN,
                KEY_FRIEND_LiST_USER_NATION_CODE,
        }, KEY_FRIEND_LiST_USER_FAVORITE_YN + "= ?", new String[] {
                favorite_yn
        }, null, null, null, null);

        if(cursor != null) {
            if(cursor.getCount() > 0 ) {
                if(cursor.moveToFirst()) {
                    do {
                        FriendListModel threadMember = new FriendListModel();

                        threadMember.setUser_xid(cursor.getString(0));
                        threadMember.setUser_name(cursor.getString(1));
                        threadMember.setUser_hp(cursor.getString(2));
                        threadMember.setUser_profile_image(cursor.getString(3));
                        threadMember.setUser_state_message(cursor.getString(4));
                        threadMember.setFavorite_yn(cursor.getString(5));
                        threadMember.setUser_nation_code(cursor.getString(6));

                        friendList.add(threadMember);
                    } while (cursor.moveToNext());
                }
            }

            cursor.close();
        }

        return friendList;
    }


    public void removeFriend(String user_xid) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        db.delete(TABLE_FRIEND_LIST_NAME, KEY_FRIEND_LIST_USER_XID + "= ?", new String[] {
                user_xid
        });

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void removeFriendAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        db.delete(TABLE_FRIEND_LIST_NAME, null, null);

        db.setTransactionSuccessful();
        db.endTransaction();
    }


    public long insertFriend(SQLiteDatabase db, FriendListModel friendListModel) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_FRIEND_LIST_USER_XID, friendListModel.getUser_xid());

        String contactUserName = MineTalkApp.getContactUserName(friendListModel.getUser_hp());
        if(contactUserName == null || contactUserName.equals("")) {
            contentValues.put(KEY_FRIEND_LIST_USER_NAME, friendListModel.getUser_name());
        } else {
            contentValues.put(KEY_FRIEND_LIST_USER_NAME, contactUserName);
        }

        contentValues.put(KEY_FRIEND_LIST_USER_HP, friendListModel.getUser_hp());
        contentValues.put(KEY_FRIEND_LIST_USER_PROFILE_IMAGE, friendListModel.getUser_profile_image());
        contentValues.put(KEY_FRIEND_LIST_USER_STATE_MESSAGE, friendListModel.getUser_state_message());
        contentValues.put(KEY_FRIEND_LiST_USER_FAVORITE_YN, friendListModel.getFavorite_yn());
        contentValues.put(KEY_FRIEND_LiST_USER_NATION_CODE, friendListModel.getUser_nation_code());

        return db.insert(TABLE_FRIEND_LIST_NAME, null, contentValues);
    }

    public long insertFriendNickName(SQLiteDatabase db, FriendListModel friendListModel) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_FRIEND_LIST_USER_XID, friendListModel.getUser_xid());

        String contactUserName = MineTalkApp.getContactUserName(friendListModel.getUser_hp());
        if(contactUserName == null || contactUserName.equals("")) {
            contentValues.put(KEY_FRIEND_LIST_USER_NAME, friendListModel.getUser_name());
        } else {
            contentValues.put(KEY_FRIEND_LIST_USER_NAME, contactUserName);
        }

        contentValues.put(KEY_FRIEND_LIST_USER_HP, friendListModel.getUser_hp());
        contentValues.put(KEY_FRIEND_LIST_USER_PROFILE_IMAGE, friendListModel.getUser_profile_image());
        contentValues.put(KEY_FRIEND_LIST_USER_STATE_MESSAGE, friendListModel.getUser_state_message());
        contentValues.put(KEY_FRIEND_LiST_USER_FAVORITE_YN, friendListModel.getFavorite_yn());
        contentValues.put(KEY_FRIEND_LiST_USER_NATION_CODE, friendListModel.getUser_nation_code());

        return db.insert(TABLE_FRIEND_LIST_NICK_NAME, null, contentValues);
    }

    public void updateFavoriteYN(String user_xid, String yn) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_FRIEND_LiST_USER_FAVORITE_YN, yn);

        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_FRIEND_LIST_NAME, contentValues, KEY_FRIEND_LIST_USER_XID + "= ?", new String[] {
                user_xid
        });
    }

    public void updateUserName(String phoneName, String name) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_FRIEND_LIST_USER_NAME, name);

        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_FRIEND_LIST_NAME, contentValues, KEY_FRIEND_LIST_USER_HP + "= ?", new String[] {
                phoneName
        });
    }

    public void updateUserNickName(String phoneName, String nickName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_FRIEND_LIST_USER_NAME, nickName);

        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_FRIEND_LIST_NICK_NAME, contentValues, KEY_FRIEND_LIST_USER_HP + "= ?", new String[] {
                phoneName
        });
    }
}
