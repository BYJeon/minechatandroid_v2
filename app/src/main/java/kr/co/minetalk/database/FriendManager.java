package kr.co.minetalk.database;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;

import kr.co.minetalk.api.model.FriendListModel;

public class FriendManager {
    private static volatile FriendManager singletonInstance = null;

    public static FriendManager getInstance() {
        if(singletonInstance == null) {
            throw new NullPointerException("manager init error");
        }

        return singletonInstance;
    }

    public static FriendManager init(Context context) {
        if(singletonInstance==null) {
            synchronized (FriendManager.class) {
                if (singletonInstance == null) {
                    context = context.getApplicationContext();
                    singletonInstance = new FriendManager(context);
                }
            }
        }

        return singletonInstance;
    }

    private FriendDbHelper friendDbHelper;

    public FriendManager(Context context) {
        friendDbHelper = new FriendDbHelper(context);
    }

    public void release() {
        friendDbHelper.close();
    }


    public void saveFriend(ArrayList<FriendListModel> friendListModels) {
        friendDbHelper.addFriendList(friendListModels);
    }

    public void removeFriend(String user_xid) {
        friendDbHelper.removeFriend(user_xid);
    }

    public void removeFriendAll() {
        friendDbHelper.removeFriendAll();
    }

    public ArrayList<FriendListModel> selectFriend() {
        return friendDbHelper.loadFriendList();
    }

    public HashMap<String, String> selectFriendMap() {
        return friendDbHelper.loadFriendMap();
    }

    public HashMap<String, String> selectFriendNickNameMap() {
        return friendDbHelper.loadFriendNickNameMap();
    }

    public HashMap<String, String> selectFriendXidToNameMap() {
        return friendDbHelper.loadFriendXidToNameMap();
    }

    public ArrayList<FriendListModel> selectFavorite() {
        return friendDbHelper.loadFavoriteList();
    }

    public void updateFavorite(String user_xid, String yn) {
        friendDbHelper.updateFavoriteYN(user_xid, yn);
    }

    public void updateUserName(String phoneNumber, String userName) {
        friendDbHelper.updateUserName(phoneNumber, userName);
    }

    public void updateUserNickName(String phoneNumber, String userNickName) {
        friendDbHelper.updateUserNickName(phoneNumber, userNickName);
    }
}
