package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.UserAccumulateListApi;
import kr.co.minetalk.api.UserLimitListApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CouponModel;
import kr.co.minetalk.api.model.MiningModel;
import kr.co.minetalk.api.model.UserAccumulateListResponse;
import kr.co.minetalk.api.model.UserLimitListResponse;
import kr.co.minetalk.databinding.ActivityLimitCheckBinding;
import kr.co.minetalk.databinding.ActivitySaveHistoryBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.CouponItemView;
import kr.co.minetalk.view.MiningItemView;

public class SaveHistoryActivity extends BindActivity<ActivitySaveHistoryBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SaveHistoryActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flag) {
        Intent intent = new Intent(context, SaveHistoryActivity.class);
        intent.addFlags(flag);
        context.startActivity(intent);
    }

    private UserAccumulateListApi mUserAccumalateListApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_save_history;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUserAccumalateListApi.execute();
    }

    private void initApi() {
        mUserAccumalateListApi = new UserAccumulateListApi(SaveHistoryActivity.this, new ApiBase.ApiCallBack<UserAccumulateListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(SaveHistoryActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserAccumulateListResponse response) {
                ProgressUtil.hideProgress(SaveHistoryActivity.this);
                if(response != null) {

                    String accumulateAmount = CommonUtils.comma_won(response.getTotal_accumulate_amount());
                    mBinding.tvTotalAmount.setText(accumulateAmount);
                    //mBinding.tvTotalBalanceAmount.setText(limitAmount);
                    //mBinding.tvBalanceAmount.setText( CommonUtils.comma_won(Long.toString(balenceAmount)));
                    setupData(response);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(SaveHistoryActivity.this);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

    }

    private void setupData(UserAccumulateListResponse response) {
        mBinding.layoutListContainer.removeAllViews();

        ArrayList<MiningModel> arrs = response.getMiningList();
//        arrs.add(new CouponModel("1일차 (Sample)", "일일접속", "2019-11-26 16:07:09","500"));
//        arrs.add(new CouponModel("마인테트리스 (Sample)", "게임", "2019-11-26 16:07:09","1000"));
//        arrs.add(new CouponModel("마인몰 (Sample)", "상품구매", "2019-11-26 16:07:09","20000"));
//        arrs.add(new CouponModel("1일차 (Sample)", "일일접속", "2019-11-26 16:07:09","500"));

        for(int i = 0; i < arrs.size() ; i++) {
            MiningItemView itemView = new MiningItemView(this);
            itemView.setData(arrs.get(i));
            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }
    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //params.topMargin = getResources().getDimensionPixelOffset(R.dimen.height_24);

        return params;
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvSavePointHistoryInfoTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvSavePointTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTotalAmount.setTextColor(getResources().getColor(R.color.white_color));

        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvSavePointHistoryInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvSavePointTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvTotalAmount.setTextColor(getResources().getColor(R.color.main_text_color));
        }
    }
}
