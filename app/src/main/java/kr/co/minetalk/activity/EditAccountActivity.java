package kr.co.minetalk.activity;

import android.Manifest;
import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.LogoutApi;
import kr.co.minetalk.api.ModifyUserGenderApi;
import kr.co.minetalk.api.ModifyUserJobApi;
import kr.co.minetalk.api.ModifyUserNameApi;
import kr.co.minetalk.api.ModifyUserStateMessageApi;
import kr.co.minetalk.api.RequestEmailCertificationApi;
import kr.co.minetalk.api.UploadImageApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.NumberCodeData;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditAccountBinding;
import kr.co.minetalk.databinding.ActivityEditProfileBinding;
import kr.co.minetalk.message.manager.MessageThreadManager;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.listener.OnMediaPopupListener;
import kr.co.minetalk.ui.popup.BottomMediaPopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class EditAccountActivity extends BindActivity<ActivityEditAccountBinding> {

    private final int PERMISSON = 1;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditAccountActivity.class);
        context.startActivity(intent);
    }

    private LogoutApi mLogoutApi;
    private UploadImageApi mUploadImageApi;

    private ArrayAdapter<String> mGenderAdapter;
    private ArrayList<String>    mGenderList = new ArrayList<>();
    private ArrayAdapter<String> mJobAdapter;
    private ArrayList<String>    mJobList = new ArrayList<>();

    private ModifyUserNameApi mModifyUserNameApi;
    private ModifyUserStateMessageApi mModifyUserStateMessageApi;
    private ModifyUserGenderApi mModifyUserGenderApi;

    private RequestEmailCertificationApi mEmailCertiApi;


    private String mSelectGender = "M";

    private ModifyUserJobApi mModifyUserJobApi;
    private String mSelectJob = "J001";

    private String mNationCode = "82";
    private String mphoneNum   = "";

    private int mNationCodePositionIndex = 0;
    final Calendar mCalendar = Calendar.getInstance();

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setupUserInfo();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_account;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        /**************************
         * Nation Code initialize
         **************************/
        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            //mBinding.tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            //mBinding.tvNationCode.setText("한국 " + "+82");
            mNationCode = "82";
        }
        mNationCodePositionIndex = 0;
        /**************************/

        setUserPhoneNum();
        userInfoRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            setupUserInfo();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initApi() {
        mLogoutApi = new LogoutApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditAccountActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditAccountActivity.this);
                if(baseModel != null) {

                    MessageThreadManager.getInstance().removeThreadAll();

                    MineTalkApp.saveLoginInfoClear();
                    LoginActivity.startActivity(EditAccountActivity.this);
                    finish();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditAccountActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
        mUploadImageApi = new UploadImageApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditAccountActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditAccountActivity.this);
                if(baseModel != null) {
                    userInfoRequest();
                }

                if(!mCameraphotonameTemp.equals("")) {
                    try {
                        File tempImageFile = new File(mCameraphotonameTemp);
                        tempImageFile.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mCameraphotonameTemp = "";
                }


            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditAccountActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mModifyUserNameApi = new ModifyUserNameApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditAccountActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
//                ProgressUtil.hideProgress(EditProfileActivity.this);
//                if(baseModel != null) {
//                    userInfoRequest();
//                }

                editStateMessageRequest(mBinding.etStateMessage.getText().toString());
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditAccountActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mModifyUserStateMessageApi = new ModifyUserStateMessageApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                //ProgressUtil.showProgress(EditProfileActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
//                ProgressUtil.hideProgress(EditProfileActivity.this);
//                if(baseModel != null) {
//                    userInfoRequest();
//                }

                editUserGenderRequest();
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditAccountActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mModifyUserGenderApi = new ModifyUserGenderApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                //ProgressUtil.showProgress(EditProfileActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
//                ProgressUtil.hideProgress(EditProfileActivity.this);
//                if(baseModel != null) {
//                    userInfoRequest();
//                }

                editJobRequest();
            }

            @Override
            public void onFailure(int request_code, String message) {
                //ProgressUtil.hideProgress(EditProfileActivity.this);
                //showMessageAlert(message);
                editJobRequest();
            }

            @Override
            public void onCancellation() {

            }
        });

        mModifyUserJobApi = new ModifyUserJobApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                //ProgressUtil.showProgress(EditProfileActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditAccountActivity.this);
                //Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.profile_modified), Toast.LENGTH_SHORT).show();
                if(baseModel != null) {
                    userInfoRequest();
                }

                showMessageAlert(getResources().getString(R.string.profile_save_complete));
            }

            @Override
            public void onFailure(int request_code, String message) {
                //ProgressUtil.hideProgress(EditProfileActivity.this);
                //showMessageAlert(message);

                ProgressUtil.hideProgress(EditAccountActivity.this);
                Toast.makeText(EditAccountActivity.this, getResources().getString(R.string.profile_modified), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.layoutUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //EditNameActivity.startActivity(EditProfileActivity.this);
            }
        });


        mBinding.btnUserPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditUserPassActivity.startActivity(EditAccountActivity.this);
            }
        });

        mBinding.btnChangeBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditUserBirthDayActivity.startActivity(EditAccountActivity.this);

//                DatePickerDialog dialog = new DatePickerDialog(EditProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker datePicker, int year, int month, int date) {
//
//                        String msg = String.format("%d 년 %d 월 %d 일", year, month+1, date);
//                        Toast.makeText(EditProfileActivity.this, msg, Toast.LENGTH_SHORT).show();
//                    }
//                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DATE));
//
//                dialog.getDatePicker().setMaxDate(new Date().getTime());    //입력한 날짜 이후로 클릭 안되게 옵션
//                dialog.show();
            }
        });

        mBinding.btnChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditUserAddressActivity.startActivity(EditAccountActivity.this);
            }
        });

//        mBinding.layoutUserEmail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                EditUserEmailActivity.startActivity(EditProfileActivity.this);
//            }
//        });

        mBinding.layoutStateMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //EditStateMessageActivity.startActivity(EditProfileActivity.this);
            }
        });


        //추천인 등록
        mBinding.btnChangeRecommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditRecommendActivity.startActivity(EditAccountActivity.this);
            }
        });


        //성별 변경
        mBinding.snGenderType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TextView textView = (TextView)parent.getChildAt(0);
                //textView.setTextSize(CommonUtils.convertDpToPixel(5.1f, getApplicationContext()));
                if(textView != null) {
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textSize_40pt));
                    textView.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.main_text_color));
                }
                changeSpinnerArrowColor(MineTalk.isBlackTheme ? R.color.main_text_color : R.color.main_text_color);

                mSelectGender = position == 0 ? "M" : "F";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //직업 변경
        mBinding.snJobType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                //textView.setTextSize(CommonUtils.convertDpToPixel(5.1f, getApplicationContext()));
                if(textView != null) {
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textSize_40pt));
                    textView.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.main_text_color));
                }
                //changeSpinnerArrowColor(MineTalk.isBlackTheme ? R.color.main_text_color : R.color.main_text_color);

                if(position == 0)
                    mSelectJob = "J001";
                else if(position == 1)
                    mSelectJob = "J002";
                else if(position == 2)
                    mSelectJob = "J003";
                else if(position == 3)
                    mSelectJob = "J004";
                else if(position == 4)
                    mSelectJob = "J005";
                else if(position == 5)
                    mSelectJob = "J999";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //통관번호 찾기
        mBinding.btnChangeCustomNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditCustomNumActivity.startActivity(EditAccountActivity.this);
                //Intent intentWeb = new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.customs.go.kr/pms/html/mos/extr/MOS0101053S.do"));
                //startActivity(intentWeb);
            }
        });

        mBinding.btnPhoneAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneCertiActivity.startActivity(EditAccountActivity.this);
            }
        });


        mBinding.btnEmailAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailCertiActivity.startActivity(EditAccountActivity.this);
            }
        });

        mBinding.btnEditId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditIDActivity.startActivity(EditAccountActivity.this);
            }
        });

        //탈퇴하기
        mBinding.btnUserWithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountWithdrawActivity.startActivity(EditAccountActivity.this);
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void changeSpinnerArrowColor(int color){
        mBinding.snGenderType.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
        mBinding.snJobType.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
    }

    private void logoutRequest() {
        mLogoutApi.execute();
    }

    private void setUserPhoneNum(){
        try {
            mphoneNum = MineTalkApp.getUserInfoModel().getUser_hp();
            mNationCode = MineTalkApp.getUserInfoModel().getUser_nation_code().replace("+", "");

            if(mphoneNum == null) return;
            try {
                ArrayList<NumberCodeData> array = NumberCode.getIntance().getDataList();
                for( NumberCodeData data : array ) {
                    if(mphoneNum.contains(data.getNumCode())) {
                        mphoneNum = mphoneNum.replace(data.getNumCode(), "0");
                        break;
                    }
                }

            } catch (Exception e) {
                mphoneNum = "";
            }

        }catch (SecurityException e){
            e.printStackTrace();
            return;
        }


        if(mphoneNum != null && !mphoneNum.equals("")) {
            mBinding.etPhoneNumber.setText( "(+" + mNationCode + ") " + mphoneNum);
            //mBinding.etPhoneNumber.setBackgroundResource(R.drawable.button_bg_dcdcdc);
            mBinding.etPhoneNumber.setEnabled(false);

            //mBinding.layoutNationCode.setBackgroundResource(R.drawable.button_bg_dcdcdc);
            //mBinding.layoutNationCode.setEnabled(false);
        }
    }

    private void setupUserInfo() {
        boolean isSNSLogin = true;
        String userName = MineTalkApp.getUserInfoModel().getUser_name();
        String userPhoneNumber = MineTalkApp.getUserInfoModel().getUser_hp();
        String userProviderType = MineTalkApp.getUserInfoModel().getUser_provider_type();
        String userBirthDay = MineTalkApp.getUserInfoModel().getUser_birthday();
        if(userBirthDay.length() == 8){
            userBirthDay = String.format("%s-%s-%s", userBirthDay.substring(0,4), userBirthDay.substring(4,6), userBirthDay.substring(6,8));
        }

        String userAddreess = MineTalkApp.getUserInfoModel().getUser_addr_si() +" "+ MineTalkApp.getUserInfoModel().getUser_addr_gu()+" "+ MineTalkApp.getUserInfoModel().getUser_addr_detail();
        String userEmail = MineTalkApp.getUserInfoModel().getUser_email();
        String stateMessage = StringEscapeUtils.unescapeJava(MineTalkApp.getUserInfoModel().getUser_state_message());

        String userId = MineTalkApp.getUserInfoModel().getUser_login_id();
        String userProvideId = MineTalkApp.getUserInfoModel().getUser_provider_id();



        mBinding.tvUserId.setText(userProvideId);
        if(userProviderType.equals(MineTalk.LOGIN_TYPE_FACEBOOK)){
            mBinding.tvSnsId.setText(getString(R.string.sns_facebook));
        }else if(userProviderType.equals(MineTalk.LOGIN_TYPE_KAKAO)){
            mBinding.tvSnsId.setText(getString(R.string.sns_kakao));
        }else if(userProviderType.equals(MineTalk.LOGIN_TYPE_GOOGLE)){
            mBinding.tvSnsId.setText(getString(R.string.sns_google));
        }else{
            mBinding.tvUserId.setText(userId);
            isSNSLogin = false;
            if(userId.equals("")){
                if(!userPhoneNumber.equals(""))
                    mBinding.tvUserId.setText(userPhoneNumber);

                mBinding.btnEditId.setVisibility(View.VISIBLE);
            }else{
                //ID 변경 가능한 사용자
                if(userPhoneNumber.equals(userId)){
                    mBinding.btnEditId.setVisibility(View.VISIBLE);
                }else{
                    mBinding.btnEditId.setVisibility(View.INVISIBLE);
                }
            }

            mBinding.tvSnsId.setText(getString(R.string.normal_regist_title));
        }


        mBinding.tvUserPassword.setText(Preferences.getUserPassword());
        mBinding.etUserName.setText(userName);
        //mBinding.tvUserPhone.setText(userPhoneNumber);
        if(userProviderType.equals("")) {
            mBinding.layoutUserPassword.setVisibility(View.VISIBLE);
        } else {
            mBinding.layoutUserPassword.setVisibility(View.GONE);
        }

        mBinding.tvUserBirthDay.setText(userBirthDay);
        mBinding.tvUserAddress.setText(userAddreess);
        mBinding.etEmail.setText(userEmail);
        mBinding.etStateMessage.setText(stateMessage);


        mSelectGender = MineTalkApp.getUserInfoModel().getUser_gender();

        if(mSelectGender.equals(""))
            mSelectGender = "M";

        mGenderList.clear();
        mGenderList.add(getString(R.string.gender_man));
        mGenderList.add(getString(R.string.gender_women));

        mGenderAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, mGenderList);
        mGenderAdapter.setDropDownViewResource(R.layout.spinner_text_color);
        mBinding.snGenderType.setAdapter(mGenderAdapter);

        if(mSelectGender.equals("M")) {
            mBinding.snGenderType.setSelection(0);
            //mBinding.etUserGender(getString(R.string.gender_man));
        } else if(mSelectGender.equals("F")) {
            //mBinding.etUserGender.setText(getString(R.string.gender_women));
            mBinding.snGenderType.setSelection(1);
        }

        mJobList.clear();
        mJobList.add(getString(R.string.msg_target_btn_job_001));
        mJobList.add(getString(R.string.msg_target_btn_job_002));
        mJobList.add(getString(R.string.msg_target_btn_job_003));
        mJobList.add(getString(R.string.msg_target_btn_job_004));
        mJobList.add(getString(R.string.msg_target_btn_job_005));
        mJobList.add(getString(R.string.msg_target_btn_job_999));

        mJobAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, mJobList);
        mJobAdapter.setDropDownViewResource(R.layout.spinner_text_color);
        mBinding.snJobType.setAdapter(mJobAdapter);

        mSelectJob = MineTalkApp.getUserInfoModel().getUser_job();
        if(mSelectJob.equals(""))
            mSelectJob = "J001";
        if(mSelectJob.equals("J001")) {
            mBinding.snJobType.setSelection(0);
        } else if(mSelectJob.equals("J002")) {
            mBinding.snJobType.setSelection(1);
        } else if(mSelectJob.equals("J003")) {
            mBinding.snJobType.setSelection(2);
        } else if(mSelectJob.equals("J004")) {
            mBinding.snJobType.setSelection(3);
        } else if(mSelectJob.equals("J005")) {
            mBinding.snJobType.setSelection(4);
        } else if(mSelectJob.equals("J999")) {
            mBinding.snJobType.setSelection(5);
        }

        String recommendUserName = MineTalkApp.getUserInfoModel().getRecommend_name();
        String recommendUserHp   = MineTalkApp.getUserInfoModel().getRecommend_hp();
        String recommendUserNationCode = MineTalkApp.getUserInfoModel().getRecommend_nation_code();

//        String recommendUserName = "Jerome";
//        String recommendUserHp   = "01026888022";

        if(recommendUserName.equals("")) {
            mBinding.tvRecommendUser.setText("-");
            if(isSNSLogin)
                mBinding.tvRecommendUser.setText(getString(R.string.sns_regist_type_user));

            mBinding.btnChangeRecommend.setText(getResources().getString(R.string.btn_add_regist_title));
            mBinding.btnChangeRecommend.setVisibility(View.VISIBLE);
        }else if(MineTalkApp.getUserInfoModel().getRecommend_change_count().equals("0") && !recommendUserName.equals("")){ //추천인 변경하기
            if(isSNSLogin)
                mBinding.tvRecommendUser.setText(getString(R.string.sns_regist_type_user));

            mBinding.btnChangeRecommend.setText(getResources().getString(R.string.bottom_button_confirm));
            mBinding.btnChangeRecommend.setVisibility(View.VISIBLE);

            if(recommendUserNationCode != null && recommendUserHp.length() > 0)
                mBinding.tvRecommendUser.setText(recommendUserName + "(+"+ recommendUserNationCode + " " + recommendUserHp + ")");
            else
                mBinding.tvRecommendUser.setText(recommendUserName);
        }else {
            mBinding.btnChangeRecommend.setVisibility(View.GONE);
            if(recommendUserNationCode != null && recommendUserHp.length() > 0)
                mBinding.tvRecommendUser.setText(recommendUserName + "(+"+ recommendUserNationCode + " " + recommendUserHp + ")");
            else
                mBinding.tvRecommendUser.setText(recommendUserName);
        }

        String customClearanceNum = Preferences.getCustomNumber(MineTalkApp.getUserInfoModel().getUser_xid());
        if(customClearanceNum.equals(""))
            mBinding.btnChangeCustomNum.setText(getResources().getString(R.string.btn_add_regist_title));
        else
            mBinding.btnChangeCustomNum.setText(getResources().getString(R.string.bottom_button_confirm));

         mBinding.tvCustomNum.setText(customClearanceNum);

        //인증 체크 이미지
        Drawable checkImage = getResources().getDrawable( R.drawable.icon_mypage_check );
        int h = checkImage.getIntrinsicHeight();
        int w = checkImage.getIntrinsicWidth();
        checkImage.setBounds( 0, 0, w, h );

        if( MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("") || MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("N") ){
            mBinding.btnPhoneAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
            mBinding.btnPhoneAuth.setEnabled(true);
            mBinding.tvPhoneAuthInfoTitle.setCompoundDrawables( null, null, null, null );

            mBinding.btnPhoneAuth.setText(getString(R.string.regist_button_auth));
        }else{
            //인증이 되어 있는 경우 재인증 할 수 있음.
            mBinding.btnPhoneAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
            mBinding.btnPhoneAuth.setEnabled(true);
            mBinding.tvPhoneAuthInfoTitle.setCompoundDrawables( null, null, checkImage, null );

            mBinding.btnPhoneAuth.setText(getString(R.string.reset_auth_title));
        }

        if( MineTalkApp.getUserInfoModel().getUser_email().equals("") || MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("N") ){
            mBinding.btnEmailAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
            mBinding.btnEmailAuth.setEnabled(true);

            mBinding.tvEmailAuthInfoTitle.setCompoundDrawables( null, null, null, null );

            mBinding.btnEmailAuth.setText(getString(R.string.regist_button_auth));
        }else{
            mBinding.btnEmailAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
            mBinding.btnEmailAuth.setEnabled(true);

            mBinding.tvEmailAuthInfoTitle.setCompoundDrawables( null, null, checkImage, null );
            mBinding.btnEmailAuth.setText(getString(R.string.reset_auth_title));
        }

        setUserPhoneNum();
    }

    private void onChooseMedia() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,null);
        galleryIntent.setType("image/*");
        galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.select_work));

//        Intent[] intentArray =  {cameraIntent};
//        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);


        startActivityForResult(chooser,MineTalk.REQUEST_CODE_MEDIA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    onChooseMedia();
                    showMediaBottomPopup();
                } else {
                    return;
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MineTalk.REQUEST_CODE_MEDIA) {
            if(resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                if (uri != null) {
                    String contentType = CommonUtils.getMediaType(EditAccountActivity.this, uri);
                    String contentPath = CommonUtils.getImageMediaPath(EditAccountActivity.this, uri);

                    Log.e("@@@TAG", "file path : " + contentPath);

                    try {
                        ExifInterface exif = new ExifInterface(contentPath);
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                        Bitmap originalImage = BitmapFactory.decodeFile(contentPath);
                        Bitmap rotateImage = CommonUtils.rotateBitmap(originalImage, orientation);

                        String saveTempBmImage = Environment.getExternalStorageDirectory() + "/MineChat/" + "tmp_profile_" + System.currentTimeMillis() + ".jpg";

                        try {
                            if (saveBitmapToFileCache(rotateImage, saveTempBmImage)) {
                                mCameraphotonameTemp = saveTempBmImage;

                                uploadImageRequest(new File(saveTempBmImage));
                            }
                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        } else if(requestCode == MineTalk.REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {


            File image = new File(Environment.getExternalStorageDirectory() + "/MineChat/" + mCameraPhotoName);
            Bitmap originalImage = BitmapFactory.decodeFile(image.getPath());

            if(originalImage != null) {
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(image.getPath());

                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Bitmap rotateImage = CommonUtils.rotateBitmap(originalImage, orientation);


                    String saveTempBmImage = Environment.getExternalStorageDirectory() + "/MineChat/" + "tmp_profile_" + mCameraPhotoName;

                    if(saveBitmapToFileCache(rotateImage, saveTempBmImage)) {
                        mCameraphotonameTemp  = saveTempBmImage;

                        uploadImageRequest(new File(saveTempBmImage));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void uploadImageRequest(File image) {
        mUploadImageApi.execute(this, image);
    }


    private Uri mCameraPhotoUri;
    private String mCameraPhotoName;
    private String mCameraphotonameTemp = "";
    public void requestTakePhotoRequest(int request_code) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); //사진을 찍기 위하여 설정합니다.
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException e) {
            Toast.makeText(this, getResources().getString(R.string.image_process_failed_msg), Toast.LENGTH_SHORT).show();
        }
        if (photoFile != null) {
            mCameraPhotoUri = FileProvider.getUriForFile(this,
                    "kr.co.minetalk.provider", photoFile); //FileProvider의 경우 이전 포스트를 참고하세요.
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mCameraPhotoUri); //사진을 찍어 해당 Content uri를 photoUri에 적용시키기 위함
            startActivityForResult(intent, request_code);
        }
    }

    // Android M에서는 Uri.fromFile 함수를 사용하였으나 7.0부터는 이 함수를 사용할 시 FileUriExposedException이
    // 발생하므로 아래와 같이 함수를 작성합니다. 이전 포스트에 참고한 영문 사이트를 들어가시면 자세한 설명을 볼 수 있습니다.
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        String imageFileName = "PP" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/MineChat/"); //test라는 경로에 이미지를 저장하기 위함
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCameraPhotoName = image.getName();

        return image;
    }


    private boolean saveBitmapToFileCache(Bitmap bitmap, String strFilePath) {

        File fileCacheItem = new File(strFilePath);
        OutputStream out = null;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    //이름 변경 요청
    private void editUserNameRequest(String userName) {
        if(userName == null || userName.equals("")) {
            ProgressUtil.hideProgress(EditAccountActivity.this);
            showMessageAlert(getResources().getString(R.string.edit_name_hint_name));
            return;
        }

        mModifyUserNameApi.execute(userName);
    }

    //상태 메시지 변경 요청
    private void editStateMessageRequest(String inputMessage) {
        if(inputMessage == null || inputMessage.equals("")) {
            ProgressUtil.hideProgress(EditAccountActivity.this);
            showMessageAlert(getResources().getString(R.string.edit_state_message_hint));
            return;
        }

        mModifyUserStateMessageApi.execute(inputMessage);
    }

    //성별 변경 요청
    private void editUserGenderRequest() {
        mModifyUserGenderApi.execute(mSelectGender);
    }

    //직업 변경 요청
    private void editJobRequest() {
        mModifyUserJobApi.execute(mSelectJob);
    }

    private void showMediaBottomPopup() {
        BottomMediaPopup mediaPopup = new BottomMediaPopup(this);
        mediaPopup.setEventListener(new OnMediaPopupListener() {
            @Override
            public void onCamera() {
                requestTakePhotoRequest(MineTalk.REQUEST_CODE_CAMERA);
            }

            @Override
            public void onGallery() {
                onChooseMedia();
            }
        });
        mediaPopup.show();
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvIdTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserId.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvPasswordTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvSnsTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvSnsId.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvRecommendTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvRecommendUser.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvAddInfoTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvNameTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.etUserName.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvStatusTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.etStateMessage.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvBirthTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserBirthDay.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvGenderTitle.setTextColor(getResources().getColor(R.color.white_color));
            //mBinding.tvUserGender.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvJobTitle.setTextColor(getResources().getColor(R.color.white_color));
            //mBinding.tvUserJob.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvAddrTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserAddress.setTextColor(getResources().getColor(R.color.white_color));


            mBinding.tvCustomTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvCustomNum.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvNormalInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvAddInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvExtraInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvWithdrawInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));

            mBinding.layoutButtonRegType.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.viewRegType.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.layoutButtonId.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.viewId.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.layoutUserPassword.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnUserPassword.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnUserPassword.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnChangeRecommend.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnChangeRecommend.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnEditId.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnEditId.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.viewPassword.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.layoutRecommend.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.layoutUserName.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.viewName.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.layoutStateMessage.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.viewStatusMessage.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.layoutUserBirthDay.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnChangeBirth.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnChangeBirth.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.viewBirth.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.layoutUserGender.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.viewGender.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.layoutUserAddress.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnChangeAddress.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnChangeAddress.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.viewAddress.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.layoutUserJob.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.layoutCustoms.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnChangeCustomNum.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnChangeCustomNum.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvAccountTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAccountStatus.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutAccount.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnUserWithdraw.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnUserWithdraw.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutPhoneAuthRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutEmailAuthRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.btnPhoneAuth.setBackgroundResource(R.drawable.button_bg_4dcdb60c);
            mBinding.btnPhoneAuth.setTextColor(getResources().getColor(R.color.main_text_color));

            if( MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("") || MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("N") ){
                mBinding.btnPhoneAuth.setBackgroundResource(R.drawable.button_bg_cdb60c);
                mBinding.btnPhoneAuth.setEnabled(true);
            }

            mBinding.btnEmailAuth.setBackgroundResource(R.drawable.button_bg_4dcdb60c);
            mBinding.btnEmailAuth.setTextColor(getResources().getColor(R.color.main_text_color));

            if( MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("") || MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("N") ){
                mBinding.btnEmailAuth.setBackgroundResource(R.drawable.button_bg_cdb60c);
                mBinding.btnEmailAuth.setEnabled(true);
            }

            mBinding.tvPhoneTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.etPhoneNumber.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.etPhoneNumber.setTextColor(getResources().getColor(R.color.main_text_color));

            //mBinding.etEmail.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.etEmail.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvRecommedInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvPhoneAuthInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvEmailAuthInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));

            mBinding.tvCustomNum.setTextColor(getResources().getColor(R.color.main_text_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));


            mBinding.tvIdTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUserId.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvPasswordTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvSnsTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvSnsId.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvRecommendTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvRecommendUser.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvAddInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvNameTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.etUserName.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvStatusTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.etStateMessage.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvBirthTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUserBirthDay.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvGenderTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.tvUserGender.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvJobTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.tvUserJob.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvAddrTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUserAddress.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvCustomTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvCustomNum.setTextColor(getResources().getColor(R.color.main_text_color));

            //추가 색상
            mBinding.tvNormalInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAddInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvExtraInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutButtonRegType.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.viewRegType.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.layoutButtonId.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.viewId.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.layoutUserPassword.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnUserPassword.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnUserPassword.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.btnChangeRecommend.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnChangeRecommend.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.btnEditId.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnEditId.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.viewPassword.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.layoutRecommend.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.layoutUserName.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.viewName.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.layoutStateMessage.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.viewStatusMessage.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.layoutUserBirthDay.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnChangeBirth.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnChangeBirth.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.viewBirth.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.layoutUserGender.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.viewGender.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.layoutUserAddress.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnChangeAddress.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnChangeAddress.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.viewAddress.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.layoutUserJob.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.layoutCustoms.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnChangeCustomNum.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnChangeCustomNum.setTextColor(getResources().getColor(R.color.white_color));

            //탈퇴하기
            mBinding.tvWithdrawInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAccountTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAccountStatus.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.layoutAccount.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnUserWithdraw.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnUserWithdraw.setTextColor(getResources().getColor(R.color.white_color));
            
            mBinding.layoutPhoneAuthRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutEmailAuthRoot.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.btnPhoneAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);
            mBinding.btnPhoneAuth.setTextColor(getResources().getColor(R.color.white_color));

            if( MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("") || MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("N") ){
                mBinding.btnPhoneAuth.setBackgroundResource(R.drawable.button_bg_16c066);
                mBinding.btnPhoneAuth.setEnabled(true);
            }

            mBinding.btnEmailAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);
            mBinding.btnEmailAuth.setTextColor(getResources().getColor(R.color.white_color));
            if( MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("") || MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("N") ){
                mBinding.btnEmailAuth.setBackgroundResource(R.drawable.button_bg_16c066);
                mBinding.btnEmailAuth.setEnabled(true);
            }

            mBinding.tvPhoneTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.etPhoneNumber.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.etPhoneNumber.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.etEmail.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.etEmail.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvRecommedInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvPhoneAuthInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvEmailAuthInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
        }
    }
}
