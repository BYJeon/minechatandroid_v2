package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityMyQrBinding;

public class MyQrActivity extends BindActivity<ActivityMyQrBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MyQrActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_qr;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        setUIEventListener();

        String userInfoFormatStr = "";
        String userName = "";
        String userProfileUrl = "";

        try {
            userInfoFormatStr = getResources().getString(R.string.my_qr_activity_user_info);
            userName = MineTalkApp.getUserInfoModel().getUser_name();
            userProfileUrl = MineTalkApp.getUserInfoModel().getUser_profile_image();
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        mBinding.tvUserNameInfo.setText(String.format(userInfoFormatStr, userName));

        if(userProfileUrl == null || userProfileUrl.equals("")) {
            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
        } else {
            Glide.with(MineTalkApp.getCurrentActivity()).load(userProfileUrl).into(mBinding.ivUserImage);
        }

        setupQR();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnQrDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    saveViewtoJpeg(mBinding.ivQr, "MineChat", "my_qr_" + MineTalkApp.getUserInfoModel().getUser_hp());
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });
    }


    private void setupQR() {
        int bWidth = (int) 500;
        int bHeight = (int) 500;
        try {
            String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
            Bitmap barcodeImage = encodeAsBitmap(userXid, BarcodeFormat.QR_CODE, bWidth, bHeight);
            if(barcodeImage != null) {
                Glide.with(this)
                        .load(barcodeImage)
                        .into(mBinding.ivQr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // 배경 색상
    private static final int WHITE = 0xFFFFFFFF;
    // 바코드 색상
    private static final int BLACK = 0xFF000000;

    private Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;

        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();

        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }
    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

    public void saveViewtoJpeg(View view,String folder, String name) {

        File ex_storage = Environment.getExternalStorageDirectory();
        String foler_name = "/"+folder+"/";
        String file_name = name + System.currentTimeMillis() + ".jpg";
        String string_path = ex_storage.getPath() + foler_name;
        File file_path;
        try {
            file_path = new File(string_path);
            if(!file_path.isDirectory()){
                file_path.mkdirs();
            }

            view.buildDrawingCache();


            Bitmap bitmap = view.getDrawingCache();

            FileOutputStream out = new FileOutputStream(string_path + file_name);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); out.close();
            out.close();

            Toast.makeText(this, getResources().getString(R.string.qr_save_msg), Toast.LENGTH_SHORT).show();
            sendBroadcast(new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(string_path+file_name))) );


        } catch (FileNotFoundException exception) {
            Log.e("FileNotFoundException", exception.getMessage());
        } catch(IOException exception) {
            Log.e("IOException", exception.getMessage());
        }
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvUserNameInfo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvUserNameInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }
}
