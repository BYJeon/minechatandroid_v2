package kr.co.minetalk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.SelectFriendGridAdapter;
import kr.co.minetalk.adapter.SelectFriendListAdapter;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.ActivityRoomMemberBinding;
import kr.co.minetalk.databinding.ActivitySelectFriendBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.CreateGroupThreadApi;
import kr.co.minetalk.message.api.CreateThreadApi;
import kr.co.minetalk.message.api.InviteGroupThreadApi;
import kr.co.minetalk.message.api.KickThreadApi;
import kr.co.minetalk.message.api.ThreadChannelApi;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.message.model.ThreadWithChannel;
import kr.co.minetalk.repository.ChatRepository;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class ChatRoomFriendActivity extends BindActivity<ActivityRoomMemberBinding> {

    private static final String COLUMN_THREAD_KEY = "column_thread_key";
    private static final String COLUMN_DEFAULT_CHAT_NAME = "column_default_chat_name";
    private static final String COLUMN_TYPE = "type";

    public static final String TYPE_KICK = "type_kick";

    private String mCurrentType = "";

    public static void startActivity(Context context, String threadKey, String chatName, int requestCode) {
        Intent intent = new Intent(context, ChatRoomFriendActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, threadKey);
        intent.putExtra(COLUMN_DEFAULT_CHAT_NAME, chatName);
        ((Activity)context).startActivityForResult(intent, requestCode);
    }

    public static void startActivity(Context context, String threadKey, String chatName, String type, int requestCode) {
        Intent intent = new Intent(context, ChatRoomFriendActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, threadKey);
        intent.putExtra(COLUMN_DEFAULT_CHAT_NAME, chatName);
        intent.putExtra(COLUMN_TYPE, type);

        ((Activity)context).startActivityForResult(intent, requestCode);
    }

    private GridLayoutManager mGridLayoutManager;
    private SelectFriendListAdapter mListAdapter;
    private SelectFriendGridAdapter mGridAdapter;
    private ArrayList<FriendListModel> mFriendList = new ArrayList<>();
    private String mActivityTitle;
    private String mCurrentThreadKey;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }


    private void loadFriend() {
        String userXidMe = MineTalkApp.getUserInfoModel().getUser_xid();

        for (int i = 0; i < mFriendList.size(); i++) {
            FriendListModel data = mFriendList.get(i);
            FriendListModel item = new FriendListModel();
            item.setUser_xid(data.getUser_xid());
            item.setUser_hp(data.getUser_hp());
            item.setUser_name(data.getUser_name());
            if(userXidMe.equals(item.getUser_xid()))
                mFriendList.get(i).setUser_name(MineTalkApp.getUserInfoModel().getUser_name());

            item.setUser_state_message(data.getUser_state_message());
            item.setUser_profile_image(data.getUser_profile_image());
        }

        setupData();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_room_member;
    }

    @Override
    protected void initView() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            //mActivityTitle = getIntent().getExtras().getString(COLUMN_DEFAULT_CHAT_NAME, MineTalk.POINT_TYPE_CASH);

            mActivityTitle = getString(R.string.chat_root_member);
            mCurrentThreadKey = getIntent().getExtras().getString(COLUMN_THREAD_KEY, "");
            mCurrentType = getIntent().getExtras().getString(COLUMN_TYPE, "");
        }

        //친구 강퇴하기
        if(mCurrentType.equals(TYPE_KICK)){
            mActivityTitle = getString(R.string.chat_kick_friend_title);
            mBinding.btnConfirm.setVisibility(View.VISIBLE);
        }

        mBinding.setHandlers(this);

        if(mCurrentThreadKey.length() != 0){
            getThreadInfo(mCurrentThreadKey);
        }

        if (mActivityTitle == null || mActivityTitle.equals("")) {
            mActivityTitle = getString(R.string.select_friend_title);
        }

        mBinding.tvActivityTitle.setText(mActivityTitle);

        mListAdapter = new SelectFriendListAdapter();
        mGridAdapter = new SelectFriendGridAdapter();

        mGridLayoutManager = new GridLayoutManager(this, 1);
        mBinding.recyclerView.setLayoutManager(mGridLayoutManager);
        mBinding.recyclerView.setAdapter(mListAdapter);

        String countMessage = String.format("친구 %d명", 0);
        mBinding.tvSelectCount.setText(countMessage);

        setUIEventListener();
        updateTheme();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.ibSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                mBinding.ibSortList.setText(bDescOrder ? "가↑" : "가↓");
                setupData();
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.viewGradient.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);

                mBinding.viewGradient.setVisibility(View.INVISIBLE);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Preferences.getSelectFriendListType()){ // Grid Type
                    mBinding.recyclerView.scrollToPosition(mGridAdapter.getItemCount()-1);
                }else{
                    mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                }

                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.ivListType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setSelectFriendListType(!Preferences.getSelectFriendListType());
                boolean listType = Preferences.getSelectFriendListType();

                if(MineTalk.isBlackTheme) { //GridType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
                } else { //ListType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
                }

                if (listType) { //GridType
                    mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                    mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            return position == 0 ? BaseFragment.mRowNum : 1;
                        }
                    });
                    mBinding.recyclerView.setAdapter(mGridAdapter);
                } else {
                    mGridLayoutManager.setSpanCount(1);
                    mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            return 1;
                        }
                    });
                    mBinding.recyclerView.setAdapter(mListAdapter);
                }
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean listType = Preferences.getSelectFriendListType();
                int selectCount = listType ? mGridAdapter.getSelectData().size() : mListAdapter.getSelectData().size();
                ArrayList<FriendListModel> selData = listType ? mGridAdapter.getSelectData() : mListAdapter.getSelectData();

                if (mCurrentType.equals(TYPE_KICK)) {
                    if (selectCount > 0) {
                        String user_xid = MineTalkApp.getUserInfoModel().getUser_xid();
                        if (selectCount == 1) {
                            kickThreadRequest(user_xid, selData.get(0).getUser_xid());
                        }
                    }
                }
            }
        });
    }

    private void setupData() {
//        for(int i = 0; i < 100; ++ i){
//            mFriendList.add(mFriendList.get(0));
//        }

        //정렬
        Collections.sort(mFriendList, bDescOrder ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);

        mListAdapter.setData(mFriendList, mCurrentType.equals(TYPE_KICK));
        mListAdapter.setAdapterListener(mFriendListViewOrderListener);
        mGridAdapter.setData(mFriendList, mCurrentType.equals(TYPE_KICK));
        mGridAdapter.setAdapterListener(mFriendListViewOrderListener);

        boolean listType = Preferences.getSelectFriendListType();
        if (listType) { //GridType
            mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            mGridLayoutManager.setSpanCount(1);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }

        String countMessage = String.format("친구 %d명", mFriendList.size());
        mBinding.tvSelectCount.setText(countMessage);
    }

    private OnFriendListViewOrderListener mFriendListViewOrderListener = new OnFriendListViewOrderListener() {
        @Override
        public void refreshListType(boolean bGridType) {
            boolean listType = Preferences.getSelectFriendListType();

            if(MineTalk.isBlackTheme) { //GridType
                mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
            } else { //ListType
                mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
            }

            if (listType) { //GridType
                mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return position == 0 ? BaseFragment.mRowNum : 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mGridAdapter);
            } else {
                mGridLayoutManager.setSpanCount(1);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mListAdapter);
            }
        }

        @Override
        public void refreshOrder(boolean bOrder) {
            bDescOrder = bOrder;
            setupData();
        }
    };

    private void kickThreadRequest(String user_xid, String friend_xids) {
        ProgressUtil.showProgress(this);
        KickThreadApi kickThreadApi = new KickThreadApi(this, mCurrentThreadKey, user_xid, friend_xids);
        kickThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatRoomFriendActivity.this);
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                Log.d("kickThreadRequest", error.toString());
            }
        });
    }

    private void getThreadInfo(String threadKey) {
        ProgressUtil.showProgress(this);
        ThreadChannelApi threadApi = new ThreadChannelApi(this, threadKey);
        threadApi.request(new BaseHttpRequest.APICallbackListener<ThreadWithChannel>() {
            @Override
            public void onSuccess(ThreadWithChannel res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatRoomFriendActivity.this);
                        mFriendList.clear();
                        ArrayList<ThreadMember> members = res.getMembers();
                        String userXidMe = MineTalkApp.getUserInfoModel().getUser_xid();

                        for( ThreadMember member : members ){
                            FriendListModel item = new FriendListModel();
                            item.setUser_xid(member.getXid());
                            item.setUser_hp(member.getUserHp());
                            item.setUser_name(MineTalkApp.getUserNameByPhoneNum(member.getNickName(), member.getUserHp()));
                            if(userXidMe.equals(item.getUser_xid()))
                                item.setUser_name(MineTalkApp.getUserInfoModel().getUser_name());

                            if(mCurrentType.equals(TYPE_KICK) && userXidMe.equals(item.getUser_xid())) continue;

                            item.setUser_state_message("");
                            item.setUser_profile_image(member.getProfileImageUrl());

                            mFriendList.add(item);
                        }

                        setupData();
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatRoomFriendActivity.this);
                    }
                });
            }
        });
    }

    public void updateTheme() {
        super.updateTheme();

        boolean listType = Preferences.getSelectFriendListType();

        if (MineTalk.isBlackTheme) {
            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.bk_point_color));

        } else {
            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.app_point_color));

        }


    }
}
