package kr.co.minetalk.activity.listener;

public interface OnFilterListener {
    public void onFilterChanged(String filter);
}
