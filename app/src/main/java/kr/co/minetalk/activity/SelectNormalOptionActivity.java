package kr.co.minetalk.activity;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;

import java.util.Locale;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.databinding.ActivitySelectLanguageBinding;
import kr.co.minetalk.databinding.ActivitySelectNormalOptionBinding;
import kr.co.minetalk.fingerprint.FingerprintHandler;
import kr.co.minetalk.locker.utils.Locker;
import kr.co.minetalk.locker.view.AppLocker;
import kr.co.minetalk.locker.view.LockActivity;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.ui.popup.SimpleListPopup;
import kr.co.minetalk.ui.popup.SimpleThemePopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;

public class SelectNormalOptionActivity extends BindActivity<ActivitySelectNormalOptionBinding> {
    static enum TARGET_TYPE{
        TARGET_ON2ON,
        TARGET_CAMERA
    }
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SelectNormalOptionActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_select_normal_option;
    }

    @Override
    protected void initView() {

        //mBinding.swFingerLock.setSelected(Preferences.getFingerScreenLock());
        mBinding.swPinLock.setSelected(Preferences.getPinScreenLock());
        setUIEventListener();

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
//            KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
//
//            if (!fingerprintManager.isHardwareDetected()) {//Manifest에 Fingerprint 퍼미션을 추가해 워야 사용가능
//                //tv_message.setText("지문을 사용할 수 없는 디바이스 입니다.");
//                mBinding.swFingerLock.setEnabled(false);
//            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//                //tv_message.setText("지문사용을 허용해 주세요.");
//                /*잠금화면 상태를 체크한다.*/
//                mBinding.swFingerLock.setEnabled(false);
//            } else if (!keyguardManager.isKeyguardSecure()) {
//                //tv_message.setText("잠금화면을 설정해 주세요.");
//                mBinding.swFingerLock.setEnabled(false);
//            } else if (!fingerprintManager.hasEnrolledFingerprints()) {
//                //tv_message.setText("등록된 지문이 없습니다.");
//                mBinding.swFingerLock.setEnabled(false);
//            } else {//모든 관문을 성공적으로 통과(지문인식을 지원하고 지문 사용이 허용되어 있고 잠금화면이 설정되었고 지문이 등록되어 있을때)
//                //tv_message.setText("손가락을 홈버튼에 대 주세요.");
//                mBinding.swFingerLock.setEnabled(true);
//            }
//        }
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        // 테마 선택
        mBinding.tvButtonTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupTopLanuge(TARGET_TYPE.TARGET_ON2ON);
            }
        });

        // 지문 잠금
//        mBinding.swFingerLock.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean isSelect = !v.isSelected();
//                v.setSelected(isSelect);
//                Preferences.setFingerScreenLock(isSelect);
//                AppLocker.getInstance().enableAppLock(getApplication(), isSelect || mBinding.swPinLock.isSelected());
//                AppLocker.getInstance().disableMainActivityAppLock(getApplication());
//            }
//        });

        // 화면 pin 잠금
        mBinding.swPinLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                Preferences.setPinScreenLock(isSelect);
                AppLocker.getInstance().enableAppLockFromSwitch(getApplication(), isSelect);

                if(isSelect) {
                    //핀 번호 비밀번호 설정이 안되어 있을 경우 비번 설정
                    if(!AppLocker.getInstance().getAppLock().isPasscodeSet()){
                        Intent intent = new Intent(getApplicationContext(), LockActivity.class);
                        intent.putExtra(Locker.TYPE, Locker.ENABLE_PASSLOCK);
                        intent.putExtra(Locker.LOCK_TYPE, true);
                        intent.putExtra(Locker.HEAD_TYPE, true);
                        startActivityForResult(intent, Locker.ENABLE_PASSLOCK);
                    }
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_CANCELED){
            mBinding.swPinLock.setSelected(false);
        }
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        mBinding.tvButtonTheme.setText(MineTalk.isBlackTheme ? "블랙↓" : "기본↓");
        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvPinLock.setTextColor(getResources().getColor(R.color.white_color));
            //mBinding.tvFingerLock.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvDpTheme.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvFontScale.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBgColor.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvPinLock.setTextColor(getResources().getColor(R.color.white_color));

            //mBinding.swFingerLock.setImageResource(R.drawable.selector_switch_bk);
            mBinding.swPinLock.setImageResource(R.drawable.selector_switch_bk);
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvPinLock.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.tvFingerLock.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvDpTheme.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvFontScale.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBgColor.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvPinLock.setTextColor(getResources().getColor(R.color.main_text_color));

            //mBinding.swFingerLock.setImageResource(R.drawable.selector_switch);
            mBinding.swPinLock.setImageResource(R.drawable.selector_switch);
        }
    }

    private void popupTopLanuge(TARGET_TYPE type) {
        SimpleThemePopup simpleListPopup = new SimpleThemePopup();
        simpleListPopup.setOnItemIndexListener(new SimpleThemePopup.OnItemIndexListener() {
            @Override
            public void onItemClick(int index) {
                MineTalk.isBlackTheme = index == 0 ? false : true;
                Preferences.setThemeType(MineTalk.isBlackTheme);
                updateTheme();

                ((MainActivity)MineTalkApp.getmMainContext()).getOnFragmentEventListner().onChangeTheme();
            }
        });

        simpleListPopup.show(getSupportFragmentManager(), "popup_top_lang");
    }

    private void  popupBottomLanuge(TARGET_TYPE type) {
        SimpleListPopup simpleListPopup = new SimpleListPopup();
        simpleListPopup.setOnItemIndexListener(new SimpleListPopup.OnItemIndexListener() {
            @Override
            public void onItemClick(int index) {

            }
        });
        simpleListPopup.show(getSupportFragmentManager(), "popup_bot_lang");
    }




}
