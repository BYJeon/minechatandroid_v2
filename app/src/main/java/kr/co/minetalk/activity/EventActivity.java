package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.EventResponse;
import kr.co.minetalk.api.ListEventApi;
import kr.co.minetalk.databinding.ActivityEventBinding;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.EventItemView;
import kr.co.minetalk.view.FAQItemView;

public class EventActivity extends BindActivity<ActivityEventBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EventActivity.class);
        context.startActivity(intent);
    }

    private ListEventApi mListEventApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_event;
    }

    @Override
    protected void onResume() {
        super.onResume();

        eventListRequest();
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
    }

    private void initApi() {
        mListEventApi = new ListEventApi(this, new ApiBase.ApiCallBack<EventResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EventActivity.this);
            }

            @Override
            public void onSuccess(int request_code, EventResponse eventResponse) {
                ProgressUtil.hideProgress(EventActivity.this);
                if(eventResponse != null) {
                    setupData(eventResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EventActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void eventListRequest() {
        mListEventApi.execute();
    }

    private void setupData(EventResponse response) {
        mBinding.layoutListContainer.removeAllViews();
        for(int i = 0 ; i < response.getData().size() ; i++) {
            EventItemView itemView = new EventItemView(this, (i%2) == 0 ? FAQItemView.ItemType.ITEM_TYPE_EVEN : FAQItemView.ItemType.ITEM_TYPE_ODD);
            itemView.setData(response.getData().get(i));

            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }
    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //params.topMargin = getResources().getDimensionPixelOffset(R.dimen.height_24);

        return params;
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
        }else{

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }
}
