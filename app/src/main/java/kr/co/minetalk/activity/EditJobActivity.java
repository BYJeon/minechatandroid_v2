package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ModifyUserEmailApi;
import kr.co.minetalk.api.ModifyUserJobApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditUserEmailBinding;
import kr.co.minetalk.databinding.ActivityEditUserJobBinding;
import kr.co.minetalk.utils.ProgressUtil;

public class EditJobActivity extends BindActivity<ActivityEditUserJobBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditJobActivity.class);
        context.startActivity(intent);
    }

    private ModifyUserJobApi mModifyUserJobApi;
    private String mSelectJob;
    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_user_job;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initViewValue();
        setDefaultValue();
        initApi();
        setUIEventListener();


    }

    private void initApi() {
        mModifyUserJobApi = new ModifyUserJobApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditJobActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditJobActivity.this);
                if(baseModel != null) {
                    userInfoRequest();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditJobActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSelectJob != null && !mSelectJob.equals("")) {
                    editJobRequest();
                }
            }
        });

        mBinding.checkJob001.setOnClickListener(mCheckViewListener);
        mBinding.checkJob002.setOnClickListener(mCheckViewListener);
        mBinding.checkJob003.setOnClickListener(mCheckViewListener);
        mBinding.checkJob004.setOnClickListener(mCheckViewListener);
        mBinding.checkJob005.setOnClickListener(mCheckViewListener);
        mBinding.checkJob999.setOnClickListener(mCheckViewListener);
    }

    private void editJobRequest() {
        mModifyUserJobApi.execute(mSelectJob);
    }

    private void initViewValue() {
        mBinding.checkJob001.setData(getString(R.string.msg_target_btn_job_001), "J001");
        mBinding.checkJob002.setData(getString(R.string.msg_target_btn_job_002), "J002");
        mBinding.checkJob003.setData(getString(R.string.msg_target_btn_job_003), "J003");
        mBinding.checkJob004.setData(getString(R.string.msg_target_btn_job_004), "J004");
        mBinding.checkJob005.setData(getString(R.string.msg_target_btn_job_005), "J005");
        mBinding.checkJob999.setData(getString(R.string.msg_target_btn_job_999), "J999");
    }

    private void setDefaultValue() {
        mSelectJob = MineTalkApp.getUserInfoModel().getUser_job();
        if(mSelectJob.equals("J001")) {
            mBinding.checkJob001.setSelected(true);
            mBinding.checkJob002.setSelected(false);
            mBinding.checkJob003.setSelected(false);
            mBinding.checkJob004.setSelected(false);
            mBinding.checkJob005.setSelected(false);
            mBinding.checkJob999.setSelected(false);
        } else if(mSelectJob.equals("J002")) {
            mBinding.checkJob001.setSelected(false);
            mBinding.checkJob002.setSelected(true);
            mBinding.checkJob003.setSelected(false);
            mBinding.checkJob004.setSelected(false);
            mBinding.checkJob005.setSelected(false);
            mBinding.checkJob999.setSelected(false);
        } else if(mSelectJob.equals("J003")) {
            mBinding.checkJob001.setSelected(false);
            mBinding.checkJob002.setSelected(false);
            mBinding.checkJob003.setSelected(true);
            mBinding.checkJob004.setSelected(false);
            mBinding.checkJob005.setSelected(false);
            mBinding.checkJob999.setSelected(false);
        } else if(mSelectJob.equals("J004")) {
            mBinding.checkJob001.setSelected(false);
            mBinding.checkJob002.setSelected(false);
            mBinding.checkJob003.setSelected(false);
            mBinding.checkJob004.setSelected(true);
            mBinding.checkJob005.setSelected(false);
            mBinding.checkJob999.setSelected(false);
        } else if(mSelectJob.equals("J005")) {
            mBinding.checkJob001.setSelected(false);
            mBinding.checkJob002.setSelected(false);
            mBinding.checkJob003.setSelected(false);
            mBinding.checkJob004.setSelected(false);
            mBinding.checkJob005.setSelected(true);
            mBinding.checkJob999.setSelected(false);
        } else if(mSelectJob.equals("J999")) {
            mBinding.checkJob001.setSelected(false);
            mBinding.checkJob002.setSelected(false);
            mBinding.checkJob003.setSelected(false);
            mBinding.checkJob004.setSelected(false);
            mBinding.checkJob005.setSelected(false);
            mBinding.checkJob999.setSelected(true);
        }
    }

    private View.OnClickListener mCheckViewListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.check_job_001 :
                    mSelectJob = "J001";
                    mBinding.checkJob001.setSelected(true);
                    mBinding.checkJob002.setSelected(false);
                    mBinding.checkJob003.setSelected(false);
                    mBinding.checkJob004.setSelected(false);
                    mBinding.checkJob005.setSelected(false);
                    mBinding.checkJob999.setSelected(false);
                    break;
                case R.id.check_job_002 :
                    mSelectJob = "J002";
                    mBinding.checkJob001.setSelected(false);
                    mBinding.checkJob002.setSelected(true);
                    mBinding.checkJob003.setSelected(false);
                    mBinding.checkJob004.setSelected(false);
                    mBinding.checkJob005.setSelected(false);
                    mBinding.checkJob999.setSelected(false);
                    break;
                case R.id.check_job_003 :
                    mSelectJob = "J003";
                    mBinding.checkJob001.setSelected(false);
                    mBinding.checkJob002.setSelected(false);
                    mBinding.checkJob003.setSelected(true);
                    mBinding.checkJob004.setSelected(false);
                    mBinding.checkJob005.setSelected(false);
                    mBinding.checkJob999.setSelected(false);
                    break;
                case R.id.check_job_004 :
                    mSelectJob = "J004";
                    mBinding.checkJob001.setSelected(false);
                    mBinding.checkJob002.setSelected(false);
                    mBinding.checkJob003.setSelected(false);
                    mBinding.checkJob004.setSelected(true);
                    mBinding.checkJob005.setSelected(false);
                    mBinding.checkJob999.setSelected(false);
                    break;
                case R.id.check_job_005 :
                    mSelectJob = "J005";
                    mBinding.checkJob001.setSelected(false);
                    mBinding.checkJob002.setSelected(false);
                    mBinding.checkJob003.setSelected(false);
                    mBinding.checkJob004.setSelected(false);
                    mBinding.checkJob005.setSelected(true);
                    mBinding.checkJob999.setSelected(false);
                    break;
                case R.id.check_job_999 :
                    mSelectJob = "J999";
                    mBinding.checkJob001.setSelected(false);
                    mBinding.checkJob002.setSelected(false);
                    mBinding.checkJob003.setSelected(false);
                    mBinding.checkJob004.setSelected(false);
                    mBinding.checkJob005.setSelected(false);
                    mBinding.checkJob999.setSelected(true);
                    break;
            }
        }
    };
}
