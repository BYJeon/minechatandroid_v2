package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityEditPhoneBinding;

public class EditPhoneActivity extends BindActivity<ActivityEditPhoneBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditPhoneActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_phone;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });

        // 인증번호 요청 버튼
        mBinding.btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        // 인증번호 확인 버튼
        mBinding.btnAuthConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


}
