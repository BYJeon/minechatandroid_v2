package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ModifyUserEmailApi;
import kr.co.minetalk.api.ModifyUserGenderApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditUserEmailBinding;
import kr.co.minetalk.databinding.ActivityEditUserGenderBinding;
import kr.co.minetalk.utils.ProgressUtil;

public class EditGenderActivity extends BindActivity<ActivityEditUserGenderBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditGenderActivity.class);
        context.startActivity(intent);
    }

    private ModifyUserGenderApi mModifyUserGenderApi;
    private String mSelectGender;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_user_gender;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initViewValue();
        setDefaultValue();
        initApi();
        setUIEventListener();

    }

    private void initApi() {
        mModifyUserGenderApi = new ModifyUserGenderApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditGenderActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditGenderActivity.this);
                if(baseModel != null) {
                    userInfoRequest();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditGenderActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSelectGender != null && !mSelectGender.equals("")) {
                    editUserGenderRequest();
                }
            }
        });

        mBinding.checkGenderWomen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectGender = "F";
                mBinding.checkGenderWomen.setSelected(true);
                mBinding.checkGenderMan.setSelected(false);
            }
        });

        mBinding.checkGenderMan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectGender = "M";
                mBinding.checkGenderMan.setSelected(true);
                mBinding.checkGenderWomen.setSelected(false);
            }
        });
    }

    private void editUserGenderRequest() {
        mModifyUserGenderApi.execute(mSelectGender);
    }

    private void initViewValue() {
        mBinding.checkGenderMan.setData(getString(R.string.gender_man), "M");
        mBinding.checkGenderWomen.setData(getString(R.string.gender_women), "F");
    }

    private void setDefaultValue() {
        mSelectGender = MineTalkApp.getUserInfoModel().getUser_gender();
        if(mSelectGender.equals("M")) {
            mBinding.checkGenderMan.setSelected(true);
            mBinding.checkGenderWomen.setSelected(false);
        } else if(mSelectGender.equals("F")) {
            mBinding.checkGenderWomen.setSelected(true);
            mBinding.checkGenderMan.setSelected(false);
        }
    }


}
