package kr.co.minetalk.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsMessage;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.PwinquiryApi;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.PwInquiryModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityFindPasswordBinding;
import kr.co.minetalk.databinding.ActivityFindPasswordOrgBinding;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class FindPasswordActivity extends BindActivity<ActivityFindPasswordOrgBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, FindPasswordActivity.class);
        context.startActivity(intent);
    }

    private RequestSmsApi mRequestSmsApi;
    private PwinquiryApi mPwinquiryApi;

    private String mNationCode = "82";
    private int mNationCodePositionIndex = 0;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
        //syncFriendList();
        //MainActivity.startActivity(LoginActivity.this);
        //finish();
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        Preferences.setLoginAuthorization("");
        Preferences.setAuthorization("");
        Preferences.setLoginType("");

        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_find_password_org;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            mBinding.tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            mBinding.tvNationCode.setText("한국 " + "+82");
            mNationCode = "82";
        }

        mNationCodePositionIndex = 0;

//        if(Build.VERSION.SDK_INT >= 23) {
//            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
//                    ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
//                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.RECEIVE_SMS,
//                        Manifest.permission.READ_SMS}, PERMISSON);
//            } else {
//                initSMSCatchReceiver();
//            }
//        }
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.layoutNationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String[] array = NumberCode.getIntance().getmTitleArray();
                    showBottomWheelPopup(array, mNationCodePositionIndex, mNationCodePickerListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mBinding.btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempPassword = mBinding.tvFindPassword.getText().toString();
                if(tempPassword == null || tempPassword.equals("")) {
                    return;
                }

                ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clipData = ClipData.newPlainText("TempPassword", tempPassword);
                clipboardManager.setPrimaryClip(clipData);

                Toast.makeText(FindPasswordActivity.this,getResources().getString(R.string.find_password_copy_message),Toast.LENGTH_SHORT).show();
            }
        });

        mBinding.btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPasswordRequest();
            }
        });

        mBinding.btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smsRequest();
            }
        });
    }

    private void initApi() {
        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FindPasswordActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(FindPasswordActivity.this);
                if(baseModel != null) {
                    showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FindPasswordActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mPwinquiryApi = new PwinquiryApi(this, new ApiBase.ApiCallBack<PwInquiryModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FindPasswordActivity.this);
            }

            @Override
            public void onSuccess(int request_code, PwInquiryModel pwInquiryModel) {
                ProgressUtil.hideProgress(FindPasswordActivity.this);
                if(pwInquiryModel != null) {
                    mBinding.tvFindPassword.setText(pwInquiryModel.getTemp_password());
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FindPasswordActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    // 전화번호 국가 코드 선택 Listener
    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
        @Override
        public void onClick(int state, int num) {
            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                mNationCodePositionIndex = num;
                try {
                    String viewName = NumberCode.getIntance().getDataList().get(num).getName();
                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");

                    mBinding.tvNationCode.setText(viewName);
                } catch (Exception e) {
                    e.printStackTrace();
                    mNationCode = "82";
                }
            }
        }

        @Override
        public void onClick(int state, int num, int position) {

        }
    };


    private void smsRequest() {
        String phoneNumber = mBinding.etPhoneNumber.getText().toString();
        if(phoneNumber == null || phoneNumber.replace(" ", "").equals("")) {
            showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
            return;
        }
        mRequestSmsApi.execute(MineTalk.SMS_TYPE_PWINQUIRY, mNationCode, phoneNumber);
    }

    private void findPasswordRequest() {
        String phoneNumber = mBinding.etPhoneNumber.getText().toString();
        if(phoneNumber == null || phoneNumber.replace(" ", "").equals("")) {
            showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
            return;
        }

        String authNumber = mBinding.etAuthNumber.getText().toString();
        if(authNumber == null || authNumber.replace(" ", "").equals("")) {
            showMessageAlert(getResources().getString(R.string.find_password_validation_auth_number));
            return;
        }

        mPwinquiryApi.execute(mNationCode, phoneNumber, authNumber);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mAuthSMSBroadCastReceiver != null) {
            unregisterReceiver(mAuthSMSBroadCastReceiver);
        }
    }

    private static final String ACTION_SMS = "android.provider.Telephony.SMS_RECEIVED";
    private BroadcastReceiver mAuthSMSBroadCastReceiver;
    private void initSMSCatchReceiver() {

        IntentFilter filter = new IntentFilter();
        filter.setPriority(999);
        filter.addAction(ACTION_SMS);


        mAuthSMSBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent.getAction().equals(ACTION_SMS)) {

                    Bundle bundle = intent.getExtras();
                    if(bundle == null)
                        return;

                    Object[] pdusObj = (Object[]) bundle.get("pdus");
                    if(pdusObj == null)
                        return;

                    SmsMessage[] smsMessages = new SmsMessage[pdusObj.length];

                    for (int i = 0; i < pdusObj.length; i++) {

                        smsMessages[i] = SmsMessage.createFromPdu((byte[])pdusObj[i]);

                        final String address = smsMessages[i].getOriginatingAddress();
                        final String message = smsMessages[i].getDisplayMessageBody();

                        String smsPhone = Preferences.getSmsPhone();
                        if( smsPhone.equals(address.replace("-","")) ) {
                            abstrackNumber(message);
                        }
                    }

                    this.abortBroadcast();
                }
            }

            private void abstrackNumber(String message) {
                Pattern p = Pattern.compile("([^\\d])");
                Matcher matcher = p.matcher(message);
                StringBuffer destStringBuffer = new StringBuffer();

                while (matcher.find()){
                    matcher.appendReplacement(destStringBuffer, "");
                }

                matcher.appendTail(destStringBuffer);
                mBinding.etAuthNumber.setText(destStringBuffer.toString());

            }

        };
        registerReceiver(mAuthSMSBroadCastReceiver, filter);
    }

    private final int PERMISSON = 1;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initSMSCatchReceiver();
                } else {
                    return;
                }
        }
    }

}
