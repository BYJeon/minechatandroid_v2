package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ModifyUserEmailApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditUserEmailBinding;
import kr.co.minetalk.utils.ProgressUtil;

public class EditUserEmailActivity extends BindActivity<ActivityEditUserEmailBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditUserEmailActivity.class);
        context.startActivity(intent);
    }

    private ModifyUserEmailApi mModifyUserEmailApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_user_email;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        String defaultValue = MineTalkApp.getUserInfoModel().getUser_email();
        mBinding.etEmail.setText(defaultValue);

    }

    private void initApi() {
        mModifyUserEmailApi = new ModifyUserEmailApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditUserEmailActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditUserEmailActivity.this);
                if(baseModel != null) {
                    userInfoRequest();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditUserEmailActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editEmailRequest(mBinding.etEmail.getText().toString());
            }
        });
    }

    private void editEmailRequest(String user_email) {
        if(user_email == null || user_email.equals("")) {
            showMessageAlert("");
            return;
        }
        mModifyUserEmailApi.execute(user_email);
    }

    @Override
    public void updateTheme() {
        super.updateTheme();
    }
}
