package kr.co.minetalk.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.MutableInt;
import android.view.View;
import android.widget.Toast;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.api.SnsCheckApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.SnsCheckModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityLoginBinding;
import kr.co.minetalk.repository.RegistRepository;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class LoginActivity extends BindActivity<ActivityLoginBinding> {

    private final int PERMISSON = 1;
    private final int PERMISSON2 = 2;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    private SnsCheckApi mSnsCheckApi;

    private RegistRepository mRegistRepository;

    private String mNationCode = "82";
    private int mNationCodePositionIndex = 0;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
        syncFriendList();
        //MainActivity.startActivity(LoginActivity.this);
        //finish();
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        Preferences.setLoginAuthorization("");
        Preferences.setAuthorization("");
        Preferences.setLoginType("");

        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        mRegistRepository = RegistRepository.getInstance();

        MineTalkApp.addActivity(this);

        initApi();
        setUIEventListener();

        mSyncListener = mFriendSyncListner;

        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            //mBinding.tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            //mBinding.tvNationCode.setText("한국 " + "+82");
            mNationCode = "82";
        }

        mNationCodePositionIndex = 0;
    }

    private OnSyncListener mFriendSyncListner = new OnSyncListener() {
        @Override
        public void onSyncComplete() {
            MainActivity.startActivity(LoginActivity.this);
            finish();
        }
    };

    private void initApi() {
        mSnsCheckApi = new SnsCheckApi(this, new ApiBase.ApiCallBack<SnsCheckModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(LoginActivity.this);
            }

            @Override
            public void onSuccess(int request_code, SnsCheckModel snsCheckModel) {
                ProgressUtil.hideProgress(LoginActivity.this);
                if(snsCheckModel != null) {
                    if(snsCheckModel.getRegist_yn().toUpperCase().equals("Y")) {
                        // 로그인
                        loginRequest("", mRegistRepository.getProviderId(), "", mRegistRepository.getProviderType());
                    } else {

                        String snsType = "";
                        if(mRegistRepository.getRegistType().equals(MineTalk.LOGIN_TYPE_GOOGLE)){
                            snsType = getString(R.string.sns_google);
                        }else if(mRegistRepository.getRegistType().equals(MineTalk.LOGIN_TYPE_FACEBOOK)){
                            snsType = getString(R.string.sns_facebook);
                        }else{
                            snsType = getString(R.string.sns_kakao);
                        }


                        showMessageAlert(String.format(getString(R.string.sns_regist_account), snsType, snsType),
                                getResources().getString(R.string.common_ok),
                                getResources().getString(R.string.common_cancel),
                                new PopupListenerFactory.SimplePopupListener() {
                                    @Override
                                    public void onClick(DialogInterface f, int state) {
                                        if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                            // 회원가입(SNS) 이동(추가정보를 받기 위해 이동)
                                            RegistSnsActivity.startActivity(LoginActivity.this, MineTalk.REQUEST_CODE_REGIST);
                                        }
                                    }
                                });

                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(LoginActivity.this);
                showMessageAlert(message);
                mRegistRepository.initialize();
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if(Build.VERSION.SDK_INT >= 23) {
//                    if(ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
//                            ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
//                        ActivityCompat.requestPermissions(LoginActivity.this, new String[] {Manifest.permission.RECEIVE_SMS,
//                                Manifest.permission.READ_SMS}, PERMISSON);
//                    } else {
//                        mRegistRepository.initialize();
//                        mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_H);
//                        RegistActivity.startActivity(LoginActivity.this, MineTalk.REQUEST_CODE_REGIST);
//                    }
//                }
                mRegistRepository.initialize();
                mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_I);
                RegistNormalActivity.startActivity(LoginActivity.this, MineTalk.REQUEST_CODE_REGIST);
                //RegistSnsActivity.startActivity(LoginActivity.this, MineTalk.REQUEST_CODE_REGIST);

            }
        });

        mBinding.btnFindPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if(Build.VERSION.SDK_INT >= 23) {
//                    if(ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
//                            ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
//                        ActivityCompat.requestPermissions(LoginActivity.this, new String[] {Manifest.permission.RECEIVE_SMS,
//                                Manifest.permission.READ_SMS}, PERMISSON2);
//                    } else {
//                        FindPasswordActivity.startActivity(LoginActivity.this);
//                    }
//                }
                FindPasswordWithIdActivity.startActivity(LoginActivity.this);
            }
        });

        mBinding.btnFindId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FindIdActivity.startActivity(LoginActivity.this);
            }
        });

        //일반 계정 전환
        mBinding.btnChangeAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountChangeActivity.startActivity(LoginActivity.this);
            }
        });

        mBinding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginValidationCheck();
            }
        });

//        mBinding.layoutNationCode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    String[] array = NumberCode.getIntance().getmTitleArray();
//                    showBottomWheelPopup(array, mNationCodePositionIndex, mNationCodePickerListener);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });

        mBinding.layoutBtnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SnsGoogleLoginActivity.startActivity(LoginActivity.this, MineTalk.REQUEST_CODE_GOOGLE_LOGIN);
            }
        });

        mBinding.layoutBtnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FacebookLoginActivity.startActivity(LoginActivity.this, MineTalk.REQUEST_CODE_FACEBOOK_LOGIN);
            }
        });

        mBinding.layoutBtnKakao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SnsKakaoLoginActivity.startActvity(LoginActivity.this, MineTalk.REQUEST_CODE_KAKAO_LOGIN);
            }
        });

    }

    private void loginValidationCheck() {
        String userPhone = mBinding.etPhoneNumber.getText().toString();
        String password  = mBinding.etPassword.getText().toString();

        if(userPhone == null || userPhone.equals("")) {
            showMessageAlert(getString(R.string.find_password_hint_id));
            return;
        }

        if(password == null || password.equals("")) {
            showMessageAlert(getString(R.string.password_input_hint));
            return;
        }

        Preferences.setUserPassword(password);
        loginRequest(mNationCode, userPhone, password, MineTalk.LOGIN_TYPE_IP);
    }


    // 전화번호 국가 코드 선택 Listener
    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
        @Override
        public void onClick(int state, int num) {
            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                mNationCodePositionIndex = num;
                try {
                    String viewName = NumberCode.getIntance().getDataList().get(num).getName();
                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");

                    //mBinding.tvNationCode.setText(viewName);
                } catch (Exception e) {
                    e.printStackTrace();
                    mNationCode = "82";
                }
            }
        }

        @Override
        public void onClick(int state, int num, int posimSnsCheckApition) {

        }
    };

    private void snsCheckRequest(String provider_type, String provider_id) {
        mSnsCheckApi.execute(provider_type, provider_id);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MineTalk.REQUEST_CODE_FACEBOOK_LOGIN) {
            if(resultCode == RESULT_OK) {
                if(data != null && data.getExtras() != null) {
                    mRegistRepository.initialize();
                    mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_S);
                    mRegistRepository.setProviderType(MineTalk.LOGIN_TYPE_FACEBOOK);
                    mRegistRepository.setRegistType(MineTalk.LOGIN_TYPE_FACEBOOK);
                    mRegistRepository.setUserName(data.getExtras().getString(MineTalk.ARG_SNS_USER_NAME));
                    mRegistRepository.setUserEmail(data.getExtras().getString(MineTalk.ARG_SNS_USER_EMAIL));
                    mRegistRepository.setProviderId(data.getExtras().getString(MineTalk.ARG_SNS_ID));
                    mRegistRepository.setUserProfileImageUrl(data.getExtras().getString(MineTalk.ARG_SNS_USER_PROFILE_IMG));

                    snsCheckRequest(mRegistRepository.getProviderType(), mRegistRepository.getProviderId());
                }
            }
        } else if(requestCode == MineTalk.REQUEST_CODE_KAKAO_LOGIN) {
            if(resultCode == RESULT_OK) {
                mRegistRepository.initialize();
                mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_S);
                mRegistRepository.setProviderType(MineTalk.LOGIN_TYPE_KAKAO);
                mRegistRepository.setRegistType(MineTalk.LOGIN_TYPE_KAKAO);
                mRegistRepository.setUserName(data.getExtras().getString(MineTalk.ARG_SNS_USER_NAME));
                mRegistRepository.setUserEmail(data.getExtras().getString(MineTalk.ARG_SNS_USER_EMAIL));
                mRegistRepository.setProviderId(data.getExtras().getString(MineTalk.ARG_SNS_ID));
                mRegistRepository.setUserProfileImageUrl(data.getExtras().getString(MineTalk.ARG_SNS_USER_PROFILE_IMG));

                snsCheckRequest(mRegistRepository.getProviderType(), mRegistRepository.getProviderId());
            }
        }else if(requestCode == MineTalk.REQUEST_CODE_GOOGLE_LOGIN){
            if(resultCode == RESULT_OK) {
                mRegistRepository.initialize();
                mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_S);
                mRegistRepository.setProviderType(MineTalk.LOGIN_TYPE_GOOGLE);
                mRegistRepository.setRegistType(MineTalk.LOGIN_TYPE_GOOGLE);
                mRegistRepository.setUserName(data.getExtras().getString(MineTalk.ARG_SNS_USER_NAME));
                mRegistRepository.setUserEmail(data.getExtras().getString(MineTalk.ARG_SNS_USER_EMAIL));
                mRegistRepository.setProviderId(data.getExtras().getString(MineTalk.ARG_SNS_ID));
                mRegistRepository.setUserProfileImageUrl(data.getExtras().getString(MineTalk.ARG_SNS_USER_PROFILE_IMG));

                snsCheckRequest(mRegistRepository.getProviderType(), mRegistRepository.getProviderId());
            }
        }else if(requestCode == MineTalk.REQUEST_CODE_REGIST) {
            try {
                mRegistRepository.initialize();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mRegistRepository.initialize();
                    mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_H);
                    //RegistActivity.startActivity(LoginActivity.this, MineTalk.REQUEST_CODE_REGIST);
                    RegistNormalActivity.startActivity(LoginActivity.this, MineTalk.REQUEST_CODE_REGIST);
                } else {
                    return;
                }
                break;
            case PERMISSON2:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    FindPasswordActivity.startActivity(LoginActivity.this);
                } else {
                    return;
                }
                break;
        }
    }
}
