package kr.co.minetalk.activity.listener;

public interface OnPermissionListener {
    public void onPermissionCheckComplete();
}
