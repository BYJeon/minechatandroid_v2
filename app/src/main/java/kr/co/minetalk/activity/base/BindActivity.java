package kr.co.minetalk.activity.base;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ContentResolver;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ActionMenuView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;

import kr.co.minetalk.activity.listener.OnDataObserverListener;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.api.AddFriendApi;
import kr.co.minetalk.api.ListFriendApi;
import kr.co.minetalk.api.LoginApi;
import kr.co.minetalk.api.SetRejectFriendApi;
import kr.co.minetalk.api.UploadContactApi;
import kr.co.minetalk.api.UserInfoApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.FriendListResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.listener.OnProfileListener;
import kr.co.minetalk.ui.popup.BottomProfilePopup;
import kr.co.minetalk.ui.popup.BottomWheelPopup;
import kr.co.minetalk.ui.popup.SimplePopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;


public abstract class BindActivity<B extends ViewDataBinding> extends BaseActivity {
    protected B mBinding;

    protected abstract int getLayoutId();

    protected abstract void initView();

    protected abstract void onDefaultApiSuccess(BaseModel baseModel);

    protected abstract void onDefaultApiFailure(int request_code, String message);

    protected OnSyncListener mSyncListener;
    protected OnDataObserverListener mDataObserver;

    protected LoginApi mLoginApi;
    protected UserInfoApi mUserInfoApi;
    protected UploadContactApi mUploadContactApi;
    protected ListFriendApi mListFriendApi;
    protected AddFriendApi mAddFriendApi;
    private SetRejectFriendApi mBlockUserApi;

    protected BottomWheelPopup mBottomWheelPicker;

    protected ContentResolver mContentResolver;


    protected boolean mIsShowProgress = true;

    protected boolean mIsShowDialogs = false;

    protected boolean bDescOrder = false;
    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        mBinding = DataBindingUtil.setContentView(this, getLayoutId());

        mContentResolver = getApplication().getContentResolver();
        initDefaultApi();
        initView();

        updateTheme();
    }

    public void updateTheme() {
        MineTalk.isBlackTheme = Preferences.getThemeType();

        if(MineTalk.isBlackTheme){
            setTheme(R.style.AppBlackTheme);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.bk_theme_bg));
                View view = getWindow().getDecorView();
                view.setSystemUiVisibility(view.getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
        }else{
            setTheme(R.style.AppTheme);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.white_color));
                View view = getWindow().getDecorView();
                view.setSystemUiVisibility(view.getSystemUiVisibility() | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
        }

    }

    private void initDefaultApi() {
        mLoginApi = new LoginApi(this, new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                if(mIsShowProgress) {
                    ProgressUtil.showProgress(BindActivity.this);
                }
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(BindActivity.this);
                if(userInfoBaseModel != null) {
                    Preferences.setRecommandUserCode("");
                    onDefaultApiSuccess(userInfoBaseModel);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                Preferences.setUserPassword("");
                ProgressUtil.hideProgress(BindActivity.this);
                onDefaultApiFailure(request_code, message);
            }

            @Override
            public void onCancellation() {
                Preferences.setUserPassword("");
            }
        });

        mUserInfoApi = new UserInfoApi(this, new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                if(mIsShowProgress) {
                    ProgressUtil.showProgress(BindActivity.this);
                }
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(BindActivity.this);
                if(userInfoBaseModel != null) {
                    onDefaultApiSuccess(userInfoBaseModel);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(BindActivity.this);
                onDefaultApiFailure(request_code, message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mUploadContactApi = new UploadContactApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                if(mIsShowProgress) {
                    ProgressUtil.showProgress(BindActivity.this);
                }

            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(BindActivity.this);
                if(baseModel != null) {
                    mListFriendApi.execute("friend");
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(BindActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mListFriendApi = new ListFriendApi(this, new ApiBase.ApiCallBack<FriendListResponse>() {
            @Override
            public void onPreparation() {
                if(mIsShowProgress) {
                    ProgressUtil.showProgress(BindActivity.this);
                }
            }

            @Override
            public void onSuccess(int request_code, FriendListResponse response) {
                ProgressUtil.hideProgress(BindActivity.this);
                if(response != null) {
                    MineTalkApp.setRecommendList(response.getRecommend_list());
                    saveLocalDB(response.getFriend_list());
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(BindActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mAddFriendApi = new AddFriendApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                if(mIsShowProgress) {
                    ProgressUtil.showProgress(BindActivity.this);
                }
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(BindActivity.this);
                if(baseModel != null) {
                    loadFriendListToServer();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(BindActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mBlockUserApi = new SetRejectFriendApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                if(mIsShowProgress) {
                    //ProgressUtil.showProgress(BindActivity.this);
                }
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                //ProgressUtil.hideProgress(BindActivity.this);
                if(baseModel != null) {

                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(BindActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    protected void userBlockRequest(String user_xid) {
        this.mBlockUserApi.execute(user_xid, "Y");
    }

    // 로그인 요청
    protected void loginRequest(String nation_code, String id, String password, String login_type) {
        String auth = "";

        if(login_type.equals(MineTalk.LOGIN_TYPE_APP)) {
            auth = CommonUtils.makeAuthorization(nation_code + "|" + id, password);
        } else if(login_type.equals(MineTalk.LOGIN_TYPE_IP)) {
            auth = CommonUtils.makeAuthorization(id, password);
        } else {
            auth = CommonUtils.makeAuthorization(id, password);
        }

        Preferences.setLoginAuthorization(auth);
        Preferences.setAuthorization(auth);
        Preferences.setLoginType(login_type);

        mLoginApi.execute(MineTalk.getUserPlatform());
    }

    /**
     * 자동 로그인
     */
    protected void loginRequest() {
        mLoginApi.execute(MineTalk.getUserPlatform());
    }

    protected void userInfoRequest() {

        if(mUserInfoApi != null) {
            mUserInfoApi.onDestroy();
        }
        mUserInfoApi.execute();

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void showMessageAlert(String message) {
        SimplePopup simplePopup = new SimplePopup(message, null, getString(R.string.common_ok));
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageAlert(String message, String ok) {
        SimplePopup simplePopup = new SimplePopup(message, null, ok);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageAlert(String message, String ok, PopupListenerFactory.SimplePopupListener listener) {
        SimplePopup simplePopup = new SimplePopup(message, null, ok);
        simplePopup.setDialogEventListener(listener);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageAlert(String message, PopupListenerFactory.SimplePopupListener listener) {
        SimplePopup simplePopup = new SimplePopup(message, null, getString(R.string.common_ok));
        simplePopup.setDialogEventListener(listener);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageAlert(boolean isCancelable, String message, PopupListenerFactory.SimplePopupListener listener) {
        SimplePopup simplePopup = new SimplePopup(message, null, getString(R.string.common_ok));
        simplePopup.setCancelable(isCancelable);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageAlert(boolean isCancelable, String message, String buttonName, PopupListenerFactory.SimplePopupListener listener) {
        SimplePopup simplePopup = new SimplePopup(message, null, getString(R.string.common_ok));
        simplePopup.setCancelable(isCancelable);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageAlert(String message, String ok, String cancel, PopupListenerFactory.SimplePopupListener listener) {
        SimplePopup simplePopup = new SimplePopup(message, null, ok, cancel);
        simplePopup.setDialogEventListener(listener);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageWithWarningAlert(String message, String warning, String ok, String cancel, PopupListenerFactory.SimplePopupListener listener) {
        SimplePopup simplePopup = new SimplePopup(message, null, warning, ok, cancel);
        simplePopup.setDialogEventListener(listener);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageWithWarningAlert(String message, String warning, String ok, PopupListenerFactory.SimplePopupListener listener) {
        SimplePopup simplePopup = new SimplePopup(message, warning, ok, "", "", "");
        simplePopup.setDialogEventListener(listener);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageWithWarningAlertReverse(String message, String warning, String ok, String cancel, PopupListenerFactory.SimplePopupListener listener) {
        SimplePopup simplePopup = new SimplePopup(message, null, warning, ok, cancel, true);
        simplePopup.setDialogEventListener(listener);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showMessageAlert(boolean cancelalbl,String message, String detailMsg, String ok, String cancel, PopupListenerFactory.SimplePopupListener listener) {
        SimplePopup simplePopup = new SimplePopup(message, null, ok, cancel);
        simplePopup.setDialogEventListener(listener);
        simplePopup.show(getSupportFragmentManager(), "alert_message");
    }

    protected void showBottomWheelPopup(String[] array, int defaultPosition, PopupListenerFactory.BaseInputWheelListener listener) {
        mBottomWheelPicker = new BottomWheelPopup(this);
        mBottomWheelPicker.setData(array, defaultPosition);
        mBottomWheelPicker.setDialogEventListener(listener);
        mBottomWheelPicker.show();
    }

    protected void showBottomWheelMoneyPopup(String[] array, int defaultPosition, PopupListenerFactory.BaseInputWheelListener listener) {
        mBottomWheelPicker = new BottomWheelPopup(this);
        mBottomWheelPicker.setData(array, defaultPosition);
        mBottomWheelPicker.setDialogEventListener(listener);
        mBottomWheelPicker.show();
    }


    protected void showProfilePopup(BottomProfilePopup.ViewType viewType, FriendListModel data, OnProfileListener listener) {
        BottomProfilePopup profilePopup = new BottomProfilePopup(this);
        profilePopup.setViewType(viewType);
        profilePopup.setData(data);
        profilePopup.setProfileListener(listener);
        profilePopup.show();
    }

    protected void showMyProfilePopup(BottomProfilePopup.ViewType viewType, UserInfoBaseModel data, OnProfileListener listener) {
        BottomProfilePopup profilePopup = new BottomProfilePopup(this);
        profilePopup.setViewType(viewType);
        profilePopup.setUserInfoData(data);
        profilePopup.setProfileListener(listener);
        profilePopup.show();
    }

    public ArrayList<ContactModel> getContactList() {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        };
        String[] selectionArgs = null;
        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor cursor = mContentResolver.query(uri, projection, null, selectionArgs, sortOrder);
        ArrayList<ContactModel> contactList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                String phoneNumber = cursor.getString(0);
                String name = cursor.getString(1);
                if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
                    continue;
                }
                phoneNumber = phoneNumber.replaceAll("-", "");
                contactList.add(new ContactModel(name, phoneNumber));
            } while (cursor.moveToNext());
        }
        return contactList;
    }

    protected JSONArray makeContactParams(ArrayList<ContactModel> arrayList) throws JSONException {
        JSONArray jsonArray = new JSONArray();

        for(int i = 0 ; i < arrayList.size(); i++) {
            String userName  = arrayList.get(i).getUser_name();
            String userPhone = arrayList.get(i).getUser_phone();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("phone_number", userPhone);
            jsonObject.put("contact_name", userName);

            jsonArray.put(jsonObject);
        }

        return jsonArray;
    }

    protected void syncFriendList() {
        JSONArray contactParams = new JSONArray();
        mUploadContactApi.execute(contactParams);
    }

    protected void syncDoFriendList() {
        if(!MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("Y"))return;
        try {
            JSONArray contactParams = makeContactParams(getContactList());
            mUploadContactApi.execute(contactParams);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void saveLocalDB(ArrayList<FriendListModel> arrayList) {
        FriendManager.getInstance().saveFriend(arrayList);
        MineTalkApp.setFriendList(arrayList);

        if(mSyncListener != null) {
            mSyncListener.onSyncComplete();
        }
    }

    /**
     * 서버로 부터 친구리스트를 받아온다.
     * 받아온 친구 리스트를 로컬 디비에 저장
     */
    protected void loadFriendListToServer() {
        mListFriendApi.execute("friend");
    }


    /**
     * 친구 추가 요청
     * @param user_xid
     */
    protected void addFriendRequest(String user_xid) {
        mAddFriendApi.execute(user_xid);
    }




}
