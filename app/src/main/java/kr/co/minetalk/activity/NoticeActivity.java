package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.NoticeListAdapter;
import kr.co.minetalk.api.ListNoticeApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.NoticeResponse;
import kr.co.minetalk.databinding.ActivityNoticeBinding;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.FAQItemView;
import kr.co.minetalk.view.NoticeItemView;

public class NoticeActivity extends BindActivity<ActivityNoticeBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, NoticeActivity.class);
        context.startActivity(intent);
    }

    private ListNoticeApi mListNoticeApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_notice;
    }

    @Override
    protected void onResume() {
        super.onResume();

        noticeListRequest();
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
    }

    private void initApi() {
        mListNoticeApi = new ListNoticeApi(this, new ApiBase.ApiCallBack<NoticeResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(NoticeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, NoticeResponse noticeResponse) {
                ProgressUtil.hideProgress(NoticeActivity.this);
                if(noticeResponse != null) {
                    setupData(noticeResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(NoticeActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void noticeListRequest() {
        mListNoticeApi.execute();
    }

    private void setupData(NoticeResponse response) {
        mBinding.layoutListContainer.removeAllViews();
        for(int i = 0 ; i < response.getData().size() ; i++) {
            NoticeItemView itemView = new NoticeItemView(this, (i%2) == 0 ? FAQItemView.ItemType.ITEM_TYPE_EVEN : FAQItemView.ItemType.ITEM_TYPE_ODD);
            itemView.setData(response.getData().get(i));

            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }
    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //params.topMargin = getResources().getDimensionPixelOffset(R.dimen.height_24);

        return params;
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
        }else{

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }
}
