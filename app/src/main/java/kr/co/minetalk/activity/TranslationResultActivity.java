package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.GoogleTranslateApi;
import kr.co.minetalk.api.model.BaseModel;

import kr.co.minetalk.api.model.TranslateModel;
import kr.co.minetalk.databinding.ActivityTranslationResultsBinding;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class TranslationResultActivity extends BindActivity<ActivityTranslationResultsBinding> {

    public static final String COLUMN_ORIGIN_CODE = "o_lang_code";
    public static final String COLUMN_ORIGIN_TEXT = "o_text";
    public static final String COLUMN_TARGET_CODE = "t_lang_code";
    public static final String COLUMN_TARGET_TEXT = "t_text";


    public static void startActivity(Context context, String originalLangCode, String originalText, String targetLangCode, String targetText) {
        Intent intent = new Intent(context, TranslationResultActivity.class);
        intent.putExtra(COLUMN_TARGET_CODE, targetLangCode);
        intent.putExtra(COLUMN_TARGET_TEXT, targetText);

        intent.putExtra(COLUMN_ORIGIN_CODE, originalLangCode);
        intent.putExtra(COLUMN_ORIGIN_TEXT, originalText);

        context.startActivity(intent);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    private GoogleTranslateApi mGoogleTranslateApi;

    private String mTopLanguageCode = "";
    private String mTopVoiceCode    = "";
    private String mTopLanguageName = "";
    private int    mTopFlagResource = -1;

    private String mBottomLanguageCode = "";
    private String mBottomVoiceCode    = "";
    private String mBottomLanguageName = "";
    private int    mBottomFlagResource = -1;

    private CountryRepository mCountryRepository;

    private String mTargetText;
    private String mTargetLangCode;

    private String mOriginText;
    private String mOriginLangCode;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_translation_results;
    }

    @Override
    protected void initView() {
        if(getIntent() != null && getIntent().getExtras() != null) {
            mTargetLangCode = getIntent().getExtras().getString(COLUMN_TARGET_CODE);
            mTargetText = getIntent().getExtras().getString(COLUMN_TARGET_TEXT);

            mOriginLangCode = getIntent().getExtras().getString(COLUMN_ORIGIN_CODE);
            mOriginText = getIntent().getExtras().getString(COLUMN_ORIGIN_TEXT);
        }


        mCountryRepository = CountryRepository.getInstance();
        mBinding.setHandlers(this);

        initApi();
        initSetting();
        setUIEventListener();

        if(mTargetLangCode.equals("")) {
            mTargetLangCode = Preferences.getCameraTargetCode();
        }


        mBinding.tvOriginalText.setText(mOriginText);
        transRequest();

    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutLanHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.layoutButtonSource.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.layoutButtonTarget.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.layoutTransBg.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
        }else{

            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutLanHeader.setBackgroundColor(getResources().getColor(R.color.main_theme_sub_bg));

            mBinding.layoutButtonSource.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.layoutButtonTarget.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.layoutTransBg.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
        }
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void transRequest() {
        mGoogleTranslateApi.execute(mOriginText, mOriginLangCode, mTargetLangCode);
    }

    private void initApi() {
        mGoogleTranslateApi = new GoogleTranslateApi(this, new ApiBase.ApiCallBack<TranslateModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(TranslationResultActivity.this);
            }

            @Override
            public void onSuccess(int request_code, TranslateModel translateModel) {
                ProgressUtil.hideProgress(TranslationResultActivity.this);
                if(translateModel != null) {

                    mBinding.tvTransText.setText(translateModel.getText());

                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(TranslationResultActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void initSetting() {
        String topLanguage = Preferences.getCameraSourceCode();
        if(topLanguage.equals("")) {
            mTopLanguageCode = mCountryRepository.getCountryModel("ko").getCode();
            mTopVoiceCode    = mCountryRepository.getCountryModel("ko").getVoice_code();
            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                mTopLanguageName = mCountryRepository.getCountryModel("ko").getCountryName();
            } else {
                mTopLanguageName = mCountryRepository.getCountryModel("ko").getCountryNameOther();
            }
            mTopFlagResource = CommonUtils.getResourceImage(this, mCountryRepository.getCountryModel("ko").getFlag_image());

        } else {
            mTopLanguageCode = mCountryRepository.getCountryModel(topLanguage).getCode();
            mTopVoiceCode    = mCountryRepository.getCountryModel(topLanguage).getVoice_code();

            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                mTopLanguageName = mCountryRepository.getCountryModel(topLanguage).getCountryName();
            } else {
                mTopLanguageName = mCountryRepository.getCountryModel(topLanguage).getCountryNameOther();
            }
            mTopFlagResource = CommonUtils.getResourceImage(this, mCountryRepository.getCountryModel(topLanguage).getFlag_image());
        }


        String bottomLanguage = Preferences.getCameraTargetCode();
        if(bottomLanguage.equals("")) {
            mBottomLanguageCode = mCountryRepository.getCountryModel("en").getCode();
            mBottomVoiceCode    = mCountryRepository.getCountryModel("en").getVoice_code();
            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                mBottomLanguageName = mCountryRepository.getCountryModel("en").getCountryName();
            } else {
                mBottomLanguageName = mCountryRepository.getCountryModel("en").getCountryNameOther();
            }
            mBottomFlagResource = CommonUtils.getResourceImage(this, mCountryRepository.getCountryModel("en").getFlag_image());
        } else {
            mBottomLanguageCode = mCountryRepository.getCountryModel(bottomLanguage).getCode();
            mBottomVoiceCode    = mCountryRepository.getCountryModel(bottomLanguage).getVoice_code();
            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                mBottomLanguageName = mCountryRepository.getCountryModel(bottomLanguage).getCountryName();
            } else {
                mBottomLanguageName = mCountryRepository.getCountryModel(bottomLanguage).getCountryNameOther();
            }
            mBottomFlagResource = CommonUtils.getResourceImage(this, mCountryRepository.getCountryModel(bottomLanguage).getFlag_image());
        }

//        mBinding.ivSourceFlag.setImageResource(getCountryImageResource(mTopLanguageCode));
        mBinding.ivSourceFlag.setImageResource(mTopFlagResource);
        mBinding.tvSourceName.setText(mTopLanguageName);
//        mBinding.ivTargetFlag.setImageResource(getCountryImageResource(mBottomLanguageCode));
        mBinding.ivTargetFlag.setImageResource(mBottomFlagResource);
        mBinding.tvTargetName.setText(mBottomLanguageName);
    }

    private int getCountryImageResource(String languageCode) {
        if(languageCode.equals("ko")) {
            return R.drawable.btn_option_korean;
        } else if(languageCode.equals("zh-CN")) {
            return R.drawable.btn_option_china;
        } else if(languageCode.equals("en")) {
            return R.drawable.btn_option_english;
        } else if(languageCode.equals("ja")) {
            return R.drawable.btn_option_japan;
        } else if(languageCode.equals("vi")) {
            return R.drawable.btn_option_vietnam;
        } else  {
            return R.drawable.btn_option_korean;
        }
    }

}
