package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.FriendBlockGridAdapter;
import kr.co.minetalk.adapter.FriendBlockListAdapter;
import kr.co.minetalk.adapter.listener.OnFriendBlockAdapterListener;
import kr.co.minetalk.api.ListFriendApi;
import kr.co.minetalk.api.SetRejectFriendApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.FriendListResponse;
import kr.co.minetalk.databinding.ActivityFriendBlockBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class FriendBlockListActivity extends BindActivity<ActivityFriendBlockBinding> {
    private GridLayoutManager   mGridLayoutManager;
    private boolean bDescOrder = false;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, FriendBlockListActivity.class);
        context.startActivity(intent);
    }

    private ListFriendApi mListFriendApi;
    private SetRejectFriendApi mSetRejectFriendApi;

    private FriendBlockListAdapter mListAdapter;
    private FriendBlockGridAdapter mGridAdapter;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_friend_block;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();

        mListAdapter = new FriendBlockListAdapter();
        mListAdapter.setAdapterListener(mFriendBlockAdapterListener, mFriendListViewOrderListener);

        mGridAdapter = new FriendBlockGridAdapter();
        mGridAdapter.setAdapterListener(mFriendBlockAdapterListener, mFriendListViewOrderListener);

        mGridLayoutManager = new GridLayoutManager(this, 1);
        mBinding.recyclerView.setLayoutManager(mGridLayoutManager);
        mBinding.recyclerView.setAdapter(mListAdapter);

        setUIEventListener();
        blockListRequest();

        updateTheme();
    }

    private void initApi() {
        mListFriendApi = new ListFriendApi(this, new ApiBase.ApiCallBack<FriendListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FriendBlockListActivity.this);
            }

            @Override
            public void onSuccess(int request_code, FriendListResponse friendListResponse) {
                ProgressUtil.hideProgress(FriendBlockListActivity.this);
                if(friendListResponse != null) {
                    setupData(friendListResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FriendBlockListActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mSetRejectFriendApi = new SetRejectFriendApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FriendBlockListActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(FriendBlockListActivity.this);
                if(baseModel != null) {
                    blockListRequest();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FriendBlockListActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.ivListType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setBlockFriendListType(!Preferences.getBlockFriendListType());
                boolean listType = Preferences.getBlockFriendListType();

                if (listType) { //GridType
                    mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                    mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            return position == 0 ? BaseFragment.mRowNum : 1;
                        }
                    });
                    mBinding.recyclerView.setAdapter(mGridAdapter);
                } else {
                    mGridLayoutManager.setSpanCount(1);
                    mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            return 1;
                        }
                    });
                    mBinding.recyclerView.setAdapter(mListAdapter);
                }

                if(MineTalk.isBlackTheme) { //GridType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
                } else { //ListType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
                }

            }
        });

        mBinding.ibSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                mBinding.ibSortList.setText(bDescOrder ? "가↑" : "가↓");
                setDataByOrder();
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.viewGradient.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);

                mBinding.viewGradient.setVisibility(View.INVISIBLE);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Preferences.getSelectFriendListType()){ // Grid Type
                    mBinding.recyclerView.scrollToPosition(mGridAdapter.getItemCount()-1);
                }else{
                    mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                }

                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void blockListRequest() {
        mListFriendApi.execute("reject");
    }

    private void blockLiftRequest(String user_xid, String reject_yn) {
        mSetRejectFriendApi.execute(user_xid, reject_yn);
    }

    private void setDataByOrder(){

        ArrayList<FriendListModel> data = mListAdapter.getItem();
        if(data == null || data.size() == 0) return;

        Collections.sort(data, bDescOrder ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);
        mListAdapter.setItem(data);
        mGridAdapter.setItem(data);

        mListAdapter.notifyDataSetChanged();
        mGridAdapter.notifyDataSetChanged();
    }

    private void setupData(FriendListResponse response) {

        String countMessage = String.format("차단구 %d명", 0);
        mBinding.tvBlockCount.setText(countMessage);

        if(response != null) {
            boolean listType = Preferences.getBlockFriendListType();

            ArrayList<FriendListModel> data = response.getFriend_list();

//            for(int i = 0; i < 100; ++i)
//                data.add(data.get(0));

            mListAdapter.setItem(data);
            mGridAdapter.setItem(data);

            if (listType) { //GridType
                mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return position == 0 ? BaseFragment.mRowNum : 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mGridAdapter);
            } else {
                mGridLayoutManager.setSpanCount(1);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mListAdapter);
            }

            if(MineTalk.isBlackTheme) { //GridType
                mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
            } else { //ListType
                mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
            }

            countMessage = String.format("차단친구 %d명", data.size());
            mBinding.tvBlockCount.setText(countMessage);
        }
    }

    private OnFriendBlockAdapterListener mFriendBlockAdapterListener = new OnFriendBlockAdapterListener() {
        @Override
        public void onBlockLift(String user_xid) {
            blockLiftRequest(user_xid, "N");
        }

        @Override
        public void onClickUser(String user_xid) {

        }
    };

    private OnFriendListViewOrderListener mFriendListViewOrderListener = new OnFriendListViewOrderListener() {
        @Override
        public void refreshListType(boolean bGridType) {
            boolean listType = Preferences.getBlockFriendListType();

            if(MineTalk.isBlackTheme) { //GridType
                mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
            } else { //ListType
                mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
            }

            if (listType) { //GridType
                mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return position == 0 ? BaseFragment.mRowNum : 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mGridAdapter);
            } else {
                mGridLayoutManager.setSpanCount(1);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mListAdapter);
            }
        }

        @Override
        public void refreshOrder(boolean bOrder) {
            bDescOrder = bOrder;
            setDataByOrder();
        }
    };

    @Override
    public void updateTheme() {
        super.updateTheme();

        boolean listType = Preferences.getFriendListType();

        if(MineTalk.isBlackTheme){
            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
        }else{
            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }
}
