package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityEditChatNameBinding;
import kr.co.minetalk.repository.ChatRepository;
import kr.co.minetalk.utils.Preferences;

public class EditChatNameActivity extends BindActivity<ActivityEditChatNameBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditChatNameActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String actvityTitle) {
        Intent intent = new Intent(context, EditChatNameActivity.class);
        intent.putExtra(COLUMN_DEFAULT_CHAT_NAME, actvityTitle);
        context.startActivity(intent);
    }

    private static final String COLUMN_DEFAULT_CHAT_NAME = "column_default_chat_name";

    private String mDefaultChatName;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_chat_name;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        if(getIntent() != null && getIntent().getExtras() != null) {
            mDefaultChatName = getIntent().getExtras().getString(COLUMN_DEFAULT_CHAT_NAME);
        }

        setUIEventListener();

        String chatName = Preferences.getChattingName(ChatRepository.getInstance().getThreadKey());
        if(chatName != null && !chatName.equals("")) {
            mBinding.etName.setText(chatName);
            mBinding.etName.setSelection(mBinding.etName.length());
            mBinding.tvCurChatName.setText(String.format("현재 채팅방명 : %s", chatName));
        } else {
            mBinding.etName.setText(mDefaultChatName);
            mBinding.etName.setSelection(mBinding.etName.length());
            mBinding.tvCurChatName.setText(String.format("현재 채팅방명 : %s", mDefaultChatName));
        }

    }


    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = mBinding.etName.getText().toString();
                if(!name.equals("")) {
                    String threadKey = ChatRepository.getInstance().getThreadKey();
                    Preferences.setChattingName(threadKey, name);

                    finish();
                }
            }
        });
    }


}
