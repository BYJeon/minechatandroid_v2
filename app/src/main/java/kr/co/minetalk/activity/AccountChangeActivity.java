package kr.co.minetalk.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.SmsMessage;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ChangeJoinTypeApi;
import kr.co.minetalk.api.CheckSmsApi;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityAccountChangeBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class AccountChangeActivity extends BindActivity<ActivityAccountChangeBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, AccountChangeActivity.class);
        context.startActivity(intent);
    }

    private RequestSmsApi mRequestSmsApi;
    private CheckSmsApi mCheckSmsApi;
    private ChangeJoinTypeApi mChangeJoinTypeApi;

    private String mNationCode = "82";
    private String mPhoneNum = "";
    private int mNationCodePositionIndex = 0;

    private boolean isPasswordAccord = false;
    private boolean isSNSRegistUser = false;

    private int mRemainTime = 180;
    private Timer mTimer = null;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        //MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
        //syncFriendList();
        //MainActivity.startActivity(LoginActivity.this);
        //finish();
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        Preferences.setLoginAuthorization("");
        Preferences.setAuthorization("");
        Preferences.setLoginType("");

        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_account_change;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getNumCode();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            mBinding.tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            mBinding.tvNationCode.setText("+82");
            mNationCode = "82";
        }

        mNationCodePositionIndex = 0;

        mBinding.btnSendAuth.setVisibility(View.VISIBLE);

        mBinding.btnPhoneAuth.setBackgroundResource(R.drawable.button_bg_16c066);
        mBinding.btnPhoneAuth.setEnabled(true);

        mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);
        mBinding.btnSendAuth.setEnabled(false);
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.layoutNationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String[] array = NumberCode.getIntance().getmTitleArray();
                    showBottomWheelPopup(array, mNationCodePositionIndex, mNationCodePickerListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //인증번호 발급 받기
        mBinding.btnPhoneAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNum = mBinding.etPhoneNumber.getText().toString();
                if(phoneNum.equals("")){
                    showMessageAlert(getResources().getString(R.string.login_validation_phone_number));
                    return;
                }
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                isPasswordAccord = false;
                mBinding.btnConfirmChange.setBackgroundResource(R.drawable.button_bg_4d16c066);
                mBinding.btnConfirmChange.setEnabled(false);

                mPhoneNum = phoneNum;
                mRequestSmsApi.execute(MineTalk.SMS_TYPE_JOINTYPE, mNationCode, phoneNum);
            }
        });

        //인증 요청
        mBinding.btnSendAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String authNum = mBinding.etAuthNumber.getText().toString().trim();

                if (authNum.equals("")) {
                    showMessageAlert(getString(R.string.phone_empty_auth_num));
                    return;
                }

                mCheckSmsApi.execute(MineTalk.SMS_TYPE_JOINTYPE, mNationCode, mPhoneNum, authNum);
            }
        });

        //전환하기 버튼
        mBinding.btnConfirmChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showMessageAlert(getResources().getString(R.string.change_account_last_warning),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    mChangeJoinTypeApi.execute(mNationCode, mPhoneNum);
                                }
                            }
                        });

            }
        });
    }

    // 전화번호 국가 코드 선택 Listener
    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
        @Override
        public void onClick(int state, int num) {
            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                mNationCodePositionIndex = num;
                try {
                    String viewName = NumberCode.getIntance().getDataList().get(num).getNumCode();
                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");

                    mBinding.tvNationCode.setText(viewName);
                } catch (Exception e) {
                    e.printStackTrace();
                    mNationCode = "82";
                }
            }
        }

        @Override
        public void onClick(int state, int num, int position) {

        }
    };


    private void initApi() {
        mChangeJoinTypeApi = new ChangeJoinTypeApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(AccountChangeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(AccountChangeActivity.this);
                if (baseModel != null) {

                    mPhoneNum = "";
                    showMessageAlert(getResources().getString(R.string.change_account_complete),
                            new PopupListenerFactory.SimplePopupListener() {
                                @Override
                                public void onClick(DialogInterface f, int state) {
                                    if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                        finish();
                                    }
                                }
                            });

                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(AccountChangeActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(AccountChangeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(AccountChangeActivity.this);
                if (baseModel != null) {
                    mBinding.btnSendAuth.setEnabled(true);
                    mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_16c066);

                    mBinding.tvInputAuthNumDesc.setVisibility(View.VISIBLE);
                    isPasswordAccord = true;
                    //인증 번호 발송 성공
                    showMessageAlert(getResources().getString(R.string.phone_send_auth_success));
                    mBinding.tvRemainTime.setVisibility(View.VISIBLE);
                    TimerStop();

                    mTimer = new Timer();
                    mTimer.schedule(new CustomTimer(), 1000, 1000);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.btnSendAuth.setEnabled(false);
                mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);

                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                isPasswordAccord = false;
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                TimerStop();
                ProgressUtil.hideProgress(AccountChangeActivity.this);
                showMessageAlert(message);

                mBinding.btnConfirmChange.setBackgroundColor(Color.parseColor("#4d16c066"));
                mBinding.btnConfirmChange.setEnabled(false);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckSmsApi = new CheckSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(AccountChangeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(AccountChangeActivity.this);
                if (baseModel != null) {

                    //인증 번호 발송 성공
                    showMessageAlert(getResources().getString(R.string.change_account_auth_match_desc));
                    mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                    TimerStop();


                    mBinding.btnConfirmChange.setBackgroundColor(Color.parseColor("#16c066"));
                    mBinding.btnConfirmChange.setEnabled(true);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                isPasswordAccord = false;
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                TimerStop();
                ProgressUtil.hideProgress(AccountChangeActivity.this);
                showMessageAlert(message);

                mBinding.btnConfirmChange.setBackgroundColor(Color.parseColor("#4d16c066"));
                mBinding.btnConfirmChange.setEnabled(false);
            }

            @Override
            public void onCancellation() {

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAuthSMSBroadCastReceiver != null) {
            unregisterReceiver(mAuthSMSBroadCastReceiver);
        }

        TimerStop();
    }

    private void TimerStop() {
        if (mTimer != null)
            mTimer.cancel();

        mRemainTime = 180;
    }

    private static final String ACTION_SMS = "android.provider.Telephony.SMS_RECEIVED";
    private BroadcastReceiver mAuthSMSBroadCastReceiver;

    private void initSMSCatchReceiver() {

        IntentFilter filter = new IntentFilter();
        filter.setPriority(999);
        filter.addAction(ACTION_SMS);


        mAuthSMSBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(ACTION_SMS)) {

                    Bundle bundle = intent.getExtras();
                    if (bundle == null)
                        return;

                    Object[] pdusObj = (Object[]) bundle.get("pdus");
                    if (pdusObj == null)
                        return;

                    SmsMessage[] smsMessages = new SmsMessage[pdusObj.length];

                    for (int i = 0; i < pdusObj.length; i++) {

                        smsMessages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);

                        final String address = smsMessages[i].getOriginatingAddress();
                        final String message = smsMessages[i].getDisplayMessageBody();

                        String smsPhone = Preferences.getSmsPhone();
                        if (smsPhone.equals(address.replace("-", ""))) {
                            abstrackNumber(message);
                        }
                    }

                    this.abortBroadcast();
                }
            }

            private void abstrackNumber(String message) {
                Pattern p = Pattern.compile("([^\\d])");
                Matcher matcher = p.matcher(message);
                StringBuffer destStringBuffer = new StringBuffer();

                while (matcher.find()) {
                    matcher.appendReplacement(destStringBuffer, "");
                }

                matcher.appendTail(destStringBuffer);
                mBinding.etAuthNumber.setText(destStringBuffer.toString());

            }

        };
        registerReceiver(mAuthSMSBroadCastReceiver, filter);
    }

    private final int PERMISSON = 1;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initSMSCatchReceiver();
                } else {
                    return;
                }
        }
    }


    // 첫 번째 TimerTask 를 이용한 방법
    class CustomTimer extends TimerTask {
        @Override
        public void run() {
            mRemainTime -= 1;

            if (mRemainTime == 0) {
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                showMessageAlert(getResources().getString(R.string.phone_time_over_auth_num));
                TimerStop();

                mBinding.btnSendAuth.setEnabled(false);
                mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);

                return;
            }
            mBinding.tvRemainTime.setText(String.format("%02d:%02d", mRemainTime / 60, mRemainTime % 60));
        }
    }

}
