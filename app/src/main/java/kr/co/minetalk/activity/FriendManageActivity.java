package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.CompoundButton;
import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.api.DeleteDataApi;
import kr.co.minetalk.api.GetFriendConfigApi;
import kr.co.minetalk.api.SetFriendConfigApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendConfigModel;
import kr.co.minetalk.databinding.ActivityFriendManageBinding;
import kr.co.minetalk.message.manager.MessageThreadManager;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class FriendManageActivity extends BindActivity<ActivityFriendManageBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, FriendManageActivity.class);
        context.startActivity(intent);
    }

    private GetFriendConfigApi mGetFriendConfigApi;
    private SetFriendConfigApi mSetFriendConfigApi;

    private DeleteDataApi mDeleteDataApi;
    private boolean mFriendOrder;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_friend_manage;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        mSyncListener = mFriendSyncListner;
        mFriendOrder = Preferences.getFriendSettingOrder();
        mBinding.tvFriendOrder.setText(mFriendOrder ?
                 getResources().getString(R.string.text_up_title) : getResources().getString(R.string.text_down_title));

        initApi();
        setUIEventListener();
        getConfigRequest();

        if (!Preferences.getContactUseNotice()) {
            showMessageAlert(getResources().getString(R.string.friend_manage_contact_use_desc),
                    new PopupListenerFactory.SimplePopupListener() {
                        @Override
                        public void onClick(DialogInterface f, int state) {
                            if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                Preferences.setContactUseNotice(true);
                            }
                        }
                    });
        }
    }

    private void initApi() {
        mGetFriendConfigApi = new GetFriendConfigApi(this, new ApiBase.ApiCallBack<FriendConfigModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FriendManageActivity.this);
            }

            @Override
            public void onSuccess(int request_code, FriendConfigModel friendConfigModel) {
                ProgressUtil.hideProgress(FriendManageActivity.this);
                if(friendConfigModel != null) {
                    setupUIData(friendConfigModel);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FriendManageActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mSetFriendConfigApi = new SetFriendConfigApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FriendManageActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(FriendManageActivity.this);
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FriendManageActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mDeleteDataApi = new DeleteDataApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FriendManageActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(FriendManageActivity.this);
                if(baseModel != null) {
                    MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MessageThreadManager.getInstance().removeThreadAll();
                        }
                    });
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FriendManageActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.cbAutoSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!compoundButton.isPressed()) {
                    return;
                }
                setConfigRequest();
            }
        });

        mBinding.cbFriendAddConfirm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!compoundButton.isPressed()) {
                    return;
                }
                setConfigRequest();
            }
        });

        mBinding.tvFriendOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFriendOrder = !mFriendOrder;
                mBinding.tvFriendOrder.setText(mFriendOrder ?
                        getResources().getString(R.string.text_up_title) :  getResources().getString(R.string.text_down_title));
                Preferences.setFriendSettingOrder(mFriendOrder);
            }
        });

//        mBinding.tvAutoSync.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean isSelect = !v.isSelected();
//                v.setSelected(isSelect);
//                setConfigRequest();
//            }
//        });
//
//        mBinding.tvFriendAddConfirm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean isSelect = !v.isSelected();
//                v.setSelected(isSelect);
//                setConfigRequest();
//            }
//        });

        mBinding.tvButtonContactSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncDoFriendList();
            }
        });

        mBinding.layoutButtonBlockFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendBlockListActivity.startActivity(FriendManageActivity.this);
            }
        });

        mBinding.tvButtonMessageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteDataApi.execute(MineTalkApp.getUserInfoModel().getUser_xid(), "message");
            }
        });
    }

    private void getConfigRequest() {
        mGetFriendConfigApi.execute();
    }

    private void setConfigRequest() {
        boolean autoSyncSelect = mBinding.cbAutoSync.isChecked();
        boolean addFriendConfirmSelect = mBinding.cbFriendAddConfirm.isChecked();

        String auto_sync  = autoSyncSelect ? "Y" : "N";
        String allow_sync = addFriendConfirmSelect ? "Y" : "N";

        mSetFriendConfigApi.execute(auto_sync, allow_sync);
    }

    private void setupUIData(FriendConfigModel data) {
        if(data != null) {
            boolean autoSync = data.getAuto_sync().equals("Y") ? true : false;
            mBinding.cbAutoSync.setChecked(autoSync);

            boolean allowYN = data.getAllow_yn().equals("Y") ? true : false;
            mBinding.cbFriendAddConfirm.setChecked(allowYN);

            String sync_date = String.format(getResources().getString(R.string.friend_manage_text_update_date), data.getSync_date());
            mBinding.tvUpdateDate.setText(sync_date);
        }
    }

    private OnSyncListener mFriendSyncListner = new OnSyncListener() {
        @Override
        public void onSyncComplete() {
            getConfigRequest();

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            ProgressUtil.showProgress(FriendManageActivity.this);
//                            ArrayList<ContactModel> contactModels = CommonUtils.getContactList(FriendManageActivity.this);
//                            if(contactModels != null) {
//                                for(int i = 0 ; i < contactModels.size() ; i ++) {
//                                    FriendManager.getInstance().updateUserName(contactModels.get(i).getUser_phone(), contactModels.get(i).getUser_name());
//                                }
//                            }
//                            ProgressUtil.hideProgress(FriendManageActivity.this);
                        }
                    });
                }
            });

        }
    };

    @Override
    public void updateTheme(){
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvAutoSync.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAutoSyncSub.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserSync.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvSyncInfo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvFriendAddConfirm.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvFriendAddInfo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserOtherSet.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserOtherInfo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBlockFriend.setTextColor(getResources().getColor(R.color.white_color));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBinding.cbAutoSync.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.bk_point_color)));
                mBinding.cbFriendAddConfirm.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.bk_point_color)));
            }

            mBinding.tvButtonContactSync.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvButtonContactSync.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.tvFriendOrder.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvFriendOrder.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.btnBlockFriend.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBlockFriend.setBackgroundResource(R.drawable.button_bg_cdb60c);

        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvAutoSync.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAutoSyncSub.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUserSync.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvSyncInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvFriendAddConfirm.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvFriendAddInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUserOtherSet.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUserOtherInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBlockFriend.setTextColor(getResources().getColor(R.color.main_text_color));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBinding.cbAutoSync.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.app_point_color)));
                mBinding.cbFriendAddConfirm.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.app_point_color)));
            }

            mBinding.tvButtonContactSync.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvButtonContactSync.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.tvFriendOrder.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvFriendOrder.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.btnBlockFriend.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBlockFriend.setBackgroundResource(R.drawable.button_bg_16c066);
        }

    }

}
