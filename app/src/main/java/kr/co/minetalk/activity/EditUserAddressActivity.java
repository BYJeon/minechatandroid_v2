package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ModifyUserAddressApi;
import kr.co.minetalk.api.RequestAddressApi;
import kr.co.minetalk.api.model.AddressListModel;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.TokenListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditAddressBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;

public class EditUserAddressActivity extends BindActivity<ActivityEditAddressBinding> {
    private ArrayAdapter<String> mSidapter;
    private ArrayAdapter<String> mGudapter;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditUserAddressActivity.class);
        context.startActivity(intent);
    }

    public enum RequestType {
        REQUEST_WAIT,
        REQUEST_SI,
        REQUEST_GU
    }

    private RequestAddressApi mRequestAddressApi;
    private RequestAddressApi mRequestGuAddressApi;
    private ModifyUserAddressApi mModifyUserAddressApi;

    private ArrayList<String> mAddressSiList = new ArrayList<>();
    private ArrayList<String> mAddressGuList = new ArrayList<>();
    private RequestType mRequestType = RequestType.REQUEST_SI;

    private String mSelectSi = "";
    private String mSelectGu = "";

    private boolean mIsDefaultGuData = false;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
            //setResult(RESULT_OK);
            //finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_address;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        initApi();
        setUIEventListener();

        setupDefaultValue();

        if(mSelectSi != null && !mSelectSi.equals("")) {
            mIsDefaultGuData = true;
        }else{
            mSelectSi = "서울특별시";
            mIsDefaultGuData = true;
        }


        siDataRequest();

//        if(mSelectSi != null && !mSelectSi.equals("")) {
//            guDataRequest();
//        }
    }

    private void setupDefaultValue() {

        try {
            String default_si = MineTalkApp.getUserInfoModel().getUser_addr_si();
            String default_gu = MineTalkApp.getUserInfoModel().getUser_addr_gu();
            String default_detail = MineTalkApp.getUserInfoModel().getUser_addr_detail();

            if(default_si != null && !default_si.equals("")) {
                mSelectSi = default_si;
                mBinding.tvSi.setText(default_si);
                mBinding.layoutCurAddr.setVisibility(View.VISIBLE);
                mBinding.tvChangeAddrTitle.setText(getResources().getString(R.string.change_origin_title));

                mBinding.tvCurAddrSi.setText(mSelectSi);

            }else{
                mBinding.layoutCurAddr.setVisibility(View.GONE);
                mBinding.tvChangeAddrTitle.setText(getResources().getString(R.string.new_title));
            }

            if(default_gu != null && !default_gu.equals("")) {
                mSelectGu = default_gu;
                mBinding.tvGu.setText(default_gu);

                mBinding.tvCurAddrDo.setText(mSelectGu);
            }

            if(default_detail != null && !default_detail.equals("")) {
                mBinding.etDetailAddress.setText(default_detail);
                mBinding.tvCurAddrDetail.setText(default_detail);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initApi() {
        mRequestAddressApi = new RequestAddressApi(this, new ApiBase.ApiCallBack<AddressListModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditUserAddressActivity.this);
            }

            @Override
            public void onSuccess(int request_code, AddressListModel addressListModel) {
                ProgressUtil.hideProgress(EditUserAddressActivity.this);
                if(addressListModel != null) {
                    if(mRequestType == RequestType.REQUEST_SI) {
                        mAddressSiList.addAll(addressListModel.getAddr_list());
                        mRequestType = RequestType.REQUEST_WAIT;

                        mSidapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, mAddressSiList);
                        mSidapter.setDropDownViewResource(R.layout.spinner_text_color);
                        mBinding.snSiType.setAdapter(mSidapter);

                    } else if(mRequestType == RequestType.REQUEST_GU){
                        mAddressGuList.clear();
                        mAddressGuList.addAll(addressListModel.getAddr_list());
                        mRequestType = RequestType.REQUEST_WAIT;

                        mGudapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, mAddressGuList);
                        mGudapter.setDropDownViewResource(R.layout.spinner_text_color);
                        mBinding.snGuType.setAdapter(mGudapter);
                    }


                    if(mIsDefaultGuData) {
                        guDefaultDataRequest();
                    }
                }

            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditUserAddressActivity.this);
                showMessageAlert(message);

                mIsDefaultGuData = false;

            }

            @Override
            public void onCancellation() {

            }
        });

        mModifyUserAddressApi = new ModifyUserAddressApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditUserAddressActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditUserAddressActivity.this);
                if(baseModel != null) {
                    userInfoRequest();

                    showMessageAlert(getResources().getString(R.string.addr_save_complete),
                            new PopupListenerFactory.SimplePopupListener() {
                                @Override
                                public void onClick(DialogInterface f, int state) {
                                    if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                        setResult(RESULT_OK);
                                        finish();
                                    }
                                }
                            });
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditUserAddressActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRequestGuAddressApi = new RequestAddressApi(this, new ApiBase.ApiCallBack<AddressListModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditUserAddressActivity.this);
            }

            @Override
            public void onSuccess(int request_code, AddressListModel addressListModel) {
                ProgressUtil.hideProgress(EditUserAddressActivity.this);
                if(addressListModel != null) {
                    mAddressGuList.clear();
                    mAddressGuList.addAll(addressListModel.getAddr_list());

                    mGudapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, mAddressGuList);
                    mGudapter.setDropDownViewResource(R.layout.spinner_text_color);
                    mBinding.snGuType.setAdapter(mGudapter);
                }

                mIsDefaultGuData = false;
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditUserAddressActivity.this);
                showMessageAlert(message);

                mIsDefaultGuData = false;
            }

            @Override
            public void onCancellation() {

            }
        });


    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyAddressRequest();
            }
        });

        mBinding.layoutButtonSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomWheelSiPopup();
            }
        });

        mBinding.layoutButtonGu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomWheelGuPopup();
            }
        });

        mBinding.snSiType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                if( textView != null )
                    textView.setTextSize(CommonUtils.convertDpToPixel(5.0f, getApplicationContext()));

                mSelectSi = mSidapter.getItem(position);

                mRequestType = RequestType.REQUEST_GU;
                guDataRequest();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.snGuType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                if(textView != null)
                    textView.setTextSize(CommonUtils.convertDpToPixel(5.0f, getApplicationContext()));

                mSelectGu = mGudapter.getItem(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void siDataRequest() {
        mRequestAddressApi.execute("si");
    }

    private void guDataRequest() {
        mRequestAddressApi.execute("gu", mSelectSi);
    }

    private void guDefaultDataRequest() {
        mRequestGuAddressApi.execute("gu", mSelectSi);
    }


    private void bottomWheelSiPopup() {
        if(mAddressSiList.size() > 0) {
            String[] array = new String[mAddressSiList.size()];
            for(int i = 0 ; i < mAddressSiList.size() ; i++) {
                array[i] = mAddressSiList.get(i);
            }
            showBottomWheelPopup(array, 0, new PopupListenerFactory.BaseInputWheelListener() {
                @Override
                public void onClick(int state, int num) {
                    if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                        mSelectSi = mAddressSiList.get(num);
                        mBinding.tvSi.setText(mAddressSiList.get(num));
                        mBinding.tvGu.setText("");
                        mSelectGu = "";

                        mRequestType = RequestType.REQUEST_GU;
                        guDataRequest();

                    }

                }

                @Override
                public void onClick(int state, int num, int position) {

                }
            });
        }
    }


    private void bottomWheelGuPopup() {
        if(mAddressGuList.size() > 0) {
            String[] array = new String[mAddressGuList.size()];
            for(int i = 0 ; i < mAddressGuList.size() ; i++) {
                array[i] = mAddressGuList.get(i);
            }
            showBottomWheelPopup(array, 0, new PopupListenerFactory.BaseInputWheelListener() {
                @Override
                public void onClick(int state, int num) {
                    if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                        mSelectGu = mAddressGuList.get(num);
                        mBinding.tvGu.setText(mAddressGuList.get(num));

                    }

                }

                @Override
                public void onClick(int state, int num, int position) {

                }
            });
        }
    }

    private void modifyAddressRequest() {


        if(mSelectSi == null ||  mSelectSi.equals("")) {
            showMessageAlert(getResources().getString(R.string.addr_empty_warning));
            return;
        }

        if(mSelectGu == null || mSelectGu.equals("")) {
            showMessageAlert(getResources().getString(R.string.addr_empty_warning));
            return;
        }

//        if(mBinding.etDetailAddress.getText().toString().equals("")) {
//            showMessageAlert(getResources().getString(R.string.addr_empty_warning));
//            return;
//        }

        showMessageAlert(getResources().getString(R.string.addr_save_warning),
                getResources().getString(R.string.common_ok),
                getResources().getString(R.string.common_cancel),
                new PopupListenerFactory.SimplePopupListener() {
                    @Override
                    public void onClick(DialogInterface f, int state) {
                        if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                            mModifyUserAddressApi.execute(mSelectSi, mSelectGu, "detail removed");
                        }
                    }
                });

    }




}
