package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.SelectFriendGridAdapter;
import kr.co.minetalk.adapter.SelectFriendListAdapter;
import kr.co.minetalk.api.LogoutApi;
import kr.co.minetalk.api.OutSecurityThreadApi;
import kr.co.minetalk.api.TransferTokenApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.ActivityChatOptionBinding;
import kr.co.minetalk.message.api.AlarmThreadApi;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.CreateGroupThreadApi;
import kr.co.minetalk.message.api.InviteGroupThreadApi;
import kr.co.minetalk.message.api.ThreadChannelApi;
import kr.co.minetalk.message.manager.MessageThreadManager;
import kr.co.minetalk.message.manager.ThreadController;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.message.model.ThreadWithChannel;
import kr.co.minetalk.repository.ChatRepository;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.FriendListView;
import kr.co.minetalk.view.UserIconView;

public class ChatOptionActivity extends BindActivity<ActivityChatOptionBinding> {
    private static final String COLUMN_THREAD_KEY = "column_thread_key";
    private static final String COLUMN_DEFAULT_CHAT_NAME = "column_default_chat_name";
    private static final String COLUMN_SECURITY_CHAT     = "column_security_chat";
    private static final String COLUMN_MEMBER_COUNT = "column_member_count";

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ChatOptionActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String threadKey) {
        Intent intent = new Intent(context, ChatOptionActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, threadKey);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String threadKey, String chatName, int requestCode) {
        Intent intent = new Intent(context, ChatOptionActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, threadKey);
        intent.putExtra(COLUMN_DEFAULT_CHAT_NAME, chatName);
        ((Activity)context).startActivityForResult(intent, requestCode);
    }

    public static void startActivity(Context context, String threadKey, String chatName, boolean securityChat, int memberCount, int requestCode) {
        Intent intent = new Intent(context, ChatOptionActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, threadKey);
        intent.putExtra(COLUMN_DEFAULT_CHAT_NAME, chatName);
        intent.putExtra(COLUMN_SECURITY_CHAT, securityChat);
        intent.putExtra(COLUMN_MEMBER_COUNT, memberCount);
        ((Activity)context).startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    private String mCurrentThreadKey;
    private boolean mIsAlarm = false;
    private String mDefaultChatName;
    private boolean mIsDirect = false;
    private OutSecurityThreadApi mOutSecurityThreadApi;
    private boolean mIsSecureChat = false;
    //방장 여부
    private boolean mIsOwner = false;
    private int mMemberCount = 0;


    //현재 참여자 정보
    private ArrayList<FriendListModel> mRoomFriendList = new ArrayList<>();
    //친구 초대 리스트
    private ArrayList<FriendListModel> mInviteFriendList = new ArrayList<>();

    private SelectFriendListAdapter mListAdapter = new SelectFriendListAdapter();
    private GridLayoutManager mGridLayoutManager;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_chat_option;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        if(getIntent() != null && getIntent().getExtras() != null) {
            mCurrentThreadKey = getIntent().getExtras().getString(COLUMN_THREAD_KEY, "");
            mDefaultChatName = getIntent().getExtras().getString(COLUMN_DEFAULT_CHAT_NAME, "");
            mIsSecureChat = getIntent().getExtras().getBoolean(COLUMN_SECURITY_CHAT, false);
            mMemberCount  = getIntent().getExtras().getInt(COLUMN_MEMBER_COUNT);

            mBinding.cbAutoTrans.setChecked(Preferences.getChatAutoTransEnable(mCurrentThreadKey));
        }

        initApi();
        setUIEventListener();

        setDefaultValue();

        updateTheme();
        if(mCurrentThreadKey != null && !mCurrentThreadKey.equals("")) {
            getThreadInfo(mCurrentThreadKey);
        }

        mListAdapter = new SelectFriendListAdapter();
        mGridLayoutManager = new GridLayoutManager(this, 1);
        mBinding.recyclerView.setLayoutManager(mGridLayoutManager);
        mBinding.recyclerView.setAdapter(mListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setDefaultValue();
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));


            mBinding.tvRoomMemberTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvTranslateLan.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAutoTrans.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAutoTransInfo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAutoAlert.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAutoAlertInfo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvSecurity.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvSecurityInfo.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvInviteFriendListTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvInviteFriendSubInfo.setTextColor(getResources().getColor(R.color.white_color));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBinding.cbAutoTrans.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.bk_point_color)));
                mBinding.cbAutoAlert.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.bk_point_color)));
            }

            mBinding.tvButtonChatName.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvButtonSelFriend.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvButtonFriendInvite.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvButtonTransLanguage.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvChatNameTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvChatNameSubTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvButtonChatName.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.tvButtonChatName.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvRoomMemberSubTitle.setTextColor(getResources().getColor(R.color.white_color));
            //mBinding.tvButtonFriendInvite.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.tvButtonFriendInvite.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvButtonSelFriend.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.tvButtonSelFriend.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvTranslateLanInfo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvButtonTransLanguage.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.tvButtonTransLanguage.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutButtonChatExit.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.layoutButtonChatExit.setTextColor(getResources().getColor(R.color.main_text_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvRoomMemberTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvTranslateLan.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAutoTrans.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAutoTransInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAutoAlert.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAutoAlertInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvSecurity.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvSecurityInfo.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvInviteFriendListTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvInviteFriendSubInfo.setTextColor(getResources().getColor(R.color.main_text_color));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBinding.cbAutoTrans.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.app_point_color)));
                mBinding.cbAutoAlert.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.app_point_color)));
            }

            mBinding.tvButtonSelFriend.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvButtonFriendInvite.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvButtonTransLanguage.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvChatNameTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvChatNameSubTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvButtonChatName.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.tvButtonChatName.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvRoomMemberSubTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            //mBinding.tvButtonFriendInvite.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.tvButtonFriendInvite.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvButtonSelFriend.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.tvButtonSelFriend.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvTranslateLanInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvButtonTransLanguage.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.tvButtonTransLanguage.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonChatExit.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.layoutButtonChatExit.setTextColor(getResources().getColor(R.color.white_color));
        }
    }

    private void initApi() {
        mOutSecurityThreadApi = new OutSecurityThreadApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChatOptionActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(ChatOptionActivity.this);
                if(baseModel != null) {
                    Intent intent = new Intent();

                    if(mIsDirect) {
                        intent.putExtra("result", "exit_out");
                    } else {
                        intent.putExtra("result", "group_exit");
                    }

                    setResult(RESULT_OK, intent);
                    finish();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(ChatOptionActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {

        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.tvButtonChatName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditChatNameActivity.startActivity(ChatOptionActivity.this, mDefaultChatName);
            }
        });


        mBinding.cbAutoAlert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!compoundButton.isPressed()) {
                    return;
                }
                alarmRequest(b);
            }
        });

        mBinding.cbAutoTrans.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!compoundButton.isPressed()) {
                    return;
                }
                Preferences.setChatAutoTransEnable(mCurrentThreadKey, b);
            }
        });

        mBinding.tvButtonSelFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //SelectFriendActivity.startActivity(ChatOptionActivity.this, getString(R.string.select_friend_title), SelectFriendActivity.TYPE_INVITE, MineTalk.REQUEST_CODE_SELECT_FRIEND);
                ChatRoomFriendActivity.startActivity(ChatOptionActivity.this, mCurrentThreadKey, mDefaultChatName, MineTalk.REQUEST_CODE_SELECT_ROOM_FRIEND);
            }
        });

        mBinding.tvButtonTransLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyLanguageActivity.startActivity(ChatOptionActivity.this, MyLanguageActivity.TYPE_TRANSLATION_LANGUAGE);
            }
        });


        //하단 친구 초대 하기
        mBinding.tvButtonFriendInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectFriendActivity.startActivity(ChatOptionActivity.this, getString(R.string.select_friend_title), SelectFriendActivity.TYPE_INVITE, MineTalk.REQUEST_CODE_SELECT_FRIEND);
//                int selectCount = mListAdapter.getSelectData().size();
//                ArrayList<FriendListModel> selData = mListAdapter.getSelectData();
//
//                int chattingRoomCount = ChatRepository.getInstance().chattingRoomMemberCount();
//                if (chattingRoomCount <= 2) {
//                    if (selectCount > 0) {
//                        ArrayList<String> member = new ArrayList<>();
//                        String user_xid = MineTalkApp.getUserInfoModel().getUser_xid();
//
//                        Set<String> keyset = ChatRepository.getInstance().getmFriendXidInfoMap().keySet();
//                        Iterator<String> iterator = keyset.iterator();
//
//                        int inviteCount = 0;
//                        while (iterator.hasNext()) {
//                            String memberXid = iterator.next();
//                            if (!memberXid.equals(user_xid)) {
//                                member.add(memberXid);
//                                inviteCount = inviteCount + 1;
//                            }
//                        }
//
//                        String[] xids = new String[selectCount + inviteCount];
//                        for (int i = 0; i < selectCount; i++) {
//                            xids[i] = selData.get(i).getUser_xid();
//                        }
//
//                        int xidsIndex = xids.length - 1;
//                        for (int j = 0; j < member.size(); j++) {
//                            xids[xidsIndex + j] = member.get(j);
//                        }
//
//                        newThreadGroupRequest(user_xid, xids);
//                    }
//
//
//                } else {
//                    if (selectCount > 0) {
//                        String user_xid = MineTalkApp.getUserInfoModel().getUser_xid();
//                        String[] xids = new String[selectCount];
//                        for (int i = 0; i < selectCount; i++) {
//                            xids[i] = selData.get(i).getUser_xid();
//                        }
//                        initThreadMemeberRequest(ChatRepository.getInstance().getThreadKey(), user_xid, xids);
//                    }
//                }
            }
        });

        //친구 퇴출 하기
        mBinding.tvButtonFriendKick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatRoomFriendActivity.startActivity(ChatOptionActivity.this, mCurrentThreadKey, mDefaultChatName, ChatRoomFriendActivity.TYPE_KICK,MineTalk.REQUEST_CODE_SELECT_FRIEND);
            }
        });

//        mBinding.switchMessageAlert.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean isSelect = !v.isSelected();
//                v.setSelected(isSelect);
//
//                alarmRequest(isSelect);
//            }
//        });

//        mBinding.switchMessageTranslation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean isSelect = !v.isSelected();
//                v.setSelected(isSelect);
//
//                Preferences.setChatTranslationUse(isSelect);
//            }
//        });

//        mBinding.layoutButtonChatBackground.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ChatBackgroundSettingActivity.startActivity(ChatOptionActivity.this);
//            }
//        });
//
//        mBinding.layoutButtonTransLanguage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MyLanguageActivity.startActivity(ChatOptionActivity.this, MyLanguageActivity.TYPE_TRANSLATION_LANGUAGE);
//            }
//
//        });

//        // 친구 초대
//        mBinding.ivButtonUserAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SelectFriendActivity.startActivity(ChatOptionActivity.this, getString(R.string.select_friend_title), SelectFriendActivity.TYPE_INVITE, MineTalk.REQUEST_CODE_SELECT_FRIEND);
//            }
//        });

        mBinding.layoutButtonChatExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showMessageAlert(getResources().getString(R.string.chat_room_exit_message),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
//                                    if(mIsSecureChat){
//                                        mOutSecurityThreadApi.execute(mCurrentThreadKey);
//                                    }else{
//                                        Intent intent = new Intent();
//                                        if(mIsDirect) {
//                                            intent.putExtra("result", "exit_out");
//                                        } else {
//                                            intent.putExtra("result", "group_exit");
//                                        }
//
//                                        setResult(RESULT_OK, intent);
//                                        finish();
//                                    }
                                    Intent intent = new Intent();
                                    if(mIsSecureChat){
                                        intent.putExtra("result", "security_exit_out");
                                    } else if(mIsDirect) {
                                        intent.putExtra("result", "exit_out");
                                    } else {
                                        intent.putExtra("result", "group_exit");
                                    }

                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            }
                        });

            }
        });
    }



    private void setDefaultValue() {
        //boolean isTranslation = Preferences.getChatTranslationUse();
        //mBinding.switchMessageTranslation.setSelected(isTranslation);
        mBinding.cbAutoTrans.setChecked(Preferences.getChatAutoTransEnable(mCurrentThreadKey));

        String myLanguage = Preferences.getChatTranslationLanguageCode();
        String mSelectLanguage = "";


        if(myLanguage.equals("ko")) {
            mSelectLanguage = getString(R.string.lang_ko);
        } else if(myLanguage.equals("zh-CN")) {
            mSelectLanguage = getString(R.string.lang_cn);
        } else if(myLanguage.equals("en")) {
            mSelectLanguage = getString(R.string.lang_en);
        } else if(myLanguage.equals("id")) {
            mSelectLanguage = "bahasa Indonesia";
        } else if(myLanguage.equals("vi")) {
            mSelectLanguage = getString(R.string.lang_vi);
        }else{
            CountryRepository mCountryRepository = CountryRepository.getInstance();
            mSelectLanguage = mCountryRepository.getCountryModel(myLanguage.length() == 0 ? "en" : myLanguage).getCountryName();

        }

        mBinding.tvButtonTransLanguage.setText(mSelectLanguage + "↓");
        //ArrayList<CountryInfoModel> countryData = CountryRepository.getInstance().getOtherCountry(MyLanguageActivity.TYPE_TRANSLATION_LANGUAGE);
    }

    private void newThreadGroupRequest(String user_xid, String[] friend_xid) {
        ProgressUtil.showProgress(this);
        CreateGroupThreadApi createGroupThreadApi = new CreateGroupThreadApi(this, user_xid, friend_xid);
        createGroupThreadApi.request(new BaseHttpRequest.APICallbackListener<ThreadData>() {
            @Override
            public void onSuccess(ThreadData res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatOptionActivity.this);
                        mListAdapter.notifyDataSetChanged();

                        String threadName = "";
                        if (res.getMembers().size() > 0) {
                            String members = "";
                            int count = 0;
                            for (ThreadMember tmember : res.getMembers()) {
                                //members = members + tmember.getNickName() + ",";

                                String contactName = MineTalkApp.getContactUserName(tmember.getUserHp());
                                if(contactName == null || contactName.equals("")) {
                                    members = members + tmember.getNickName();
                                } else {
                                    members = members + contactName;
                                }

                                if(count != (res.getMembers().size() - 1)) {
                                    members = members + ",";
                                }
                                count++;
                            }
                            threadName = members;
                        }

                        ChatDetailActivity.startActivity(ChatOptionActivity.this, res.getThreadKey(), res.getJoinMemberCount(), threadName);
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatOptionActivity.this);
                        mListAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private void initThreadMemeberRequest(String threadKey, String user_xid, String[] friend_xid) {

        ProgressUtil.showProgress(this);
        InviteGroupThreadApi inviteGroupThreadApi = new InviteGroupThreadApi(this, user_xid, friend_xid, threadKey);
        inviteGroupThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatOptionActivity.this);

                        Intent intent = new Intent();
                        intent.putExtra("result", "invite_complete");

                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatOptionActivity.this);
                    }
                });
            }
        });
    }

    private void getThreadInfo(String threadKey) {
        ProgressUtil.showProgress(this);
        ThreadChannelApi threadApi = new ThreadChannelApi(this, threadKey);
        threadApi.request(new BaseHttpRequest.APICallbackListener<ThreadWithChannel>() {
            @Override
            public void onSuccess(ThreadWithChannel res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatOptionActivity.this);
                        setChatMember(res.getMembers());

                        Log.e("@@@TAG", "thread info : " + res.getIsDirect());

                        mIsDirect = (res.getIsDirect().equals("Y")) ? true : false;
                        //mBinding.switchMessageAlert.setSelected(mIsAlarm);
                        mBinding.cbAutoAlert.setChecked(mIsAlarm);

                    }
                });
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatOptionActivity.this);
                    }
                });
            }
        });
    }


    //현재 참여 친구 리스트
    private void loadRoomFriendList(){
        mBinding.layoutFriendMemberContainer.removeAllViews();
        for(int i = 0 ; i < mRoomFriendList.size() ; i++) {
            FriendListView itemView = new FriendListView(MineTalkApp.getCurrentActivity());
            itemView.setViewType(FriendListView.ViewType.NORMAL);
            itemView.setData(mRoomFriendList.get(i));
            mBinding.layoutFriendMemberContainer.addView(itemView);
        }
    }


    //초대할 친구 리스트
    private void loadInviteFriend() {

        mInviteFriendList.clear();

        for (int i = 0; i < MineTalkApp.getFriendList().size(); i++) {
            FriendListModel data = MineTalkApp.getFriendList().get(i);

            FriendListModel item = new FriendListModel();
            item.setUser_xid(data.getUser_xid());
            item.setUser_hp(data.getUser_hp());
            item.setUser_name(data.getUser_name());
            item.setUser_state_message(data.getUser_state_message());
            item.setUser_profile_image(data.getUser_profile_image());

            if (!ChatRepository.getInstance().containUser(item.getUser_xid())) {
                mInviteFriendList.add(item);
            }
        }

        mListAdapter.setData(mInviteFriendList);

//        mBinding.layoutFriendListContainer.removeAllViews();
//        for(int i = 0 ; i < mInviteFriendList.size() ; i++) {
//            FriendListView itemView = new FriendListView(MineTalkApp.getCurrentActivity());
//            itemView.setViewType(FriendListView.ViewType.NORMAL);
//            itemView.setData(mInviteFriendList.get(i));
//            mBinding.layoutFriendListContainer.addView(itemView);
//        }
    }

    private void alarmRequest(boolean enable) {
        ProgressUtil.showProgress(this);

        String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
        AlarmThreadApi alarmThreadApi = new AlarmThreadApi(this, userXid, mCurrentThreadKey, enable);
        alarmThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                ProgressUtil.hideProgress(ChatOptionActivity.this);
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                ProgressUtil.hideProgress(ChatOptionActivity.this);
                Log.e("@@@TAG", error.getMessage());
            }
        });
    }


    private void setChatMember(ArrayList<ThreadMember> members) {

        ChatRepository.getInstance().initialize();
        ChatRepository.getInstance().setThreadKey(mCurrentThreadKey);
        //mBinding.layoutFriendContainer.removeAllViews();
        ArrayList<FriendListModel> friendListModels = new ArrayList<>();

        String userXidMe = MineTalkApp.getUserInfoModel().getUser_xid();

        if(members.size() > 0) {
            for(int i = 0 ; i < members.size() ; i++) {

                if(userXidMe.equals(members.get(i).getXid())) {
                    //방장여부 체크
                    mIsOwner = members.get(i).getIsOwner().toUpperCase().equals("Y");
                    mIsAlarm = members.get(i).getAlarm().toUpperCase().equals("Y") ? true : false;
                }

                FriendListModel itemModel = new FriendListModel();
                itemModel.setUser_xid(members.get(i).getXid());

                itemModel.setUser_name(members.get(i).getNickName());

                itemModel.setUser_profile_image(members.get(i).getProfileImageUrl());

                friendListModels.add(itemModel);

                ChatRepository.getInstance().addUserInfo(itemModel.getUser_xid(), itemModel.getUser_name());
            }
        }

//        for(int i = 0 ; i < friendListModels.size() ; i++) {
//            UserIconView iconView = new UserIconView(this);
//            iconView.setData(friendListModels.get(i));
//
//            mBinding.layoutFriendContainer.addView(iconView);
//        }

        mRoomFriendList.clear();
        //ArrayList<ThreadMember> memberArr = members.getMembers();

        for( ThreadMember member : members ){
            FriendListModel item = new FriendListModel();
            item.setUser_xid(member.getXid());
            item.setUser_hp(member.getUserHp());
            item.setUser_name(member.getNickName());
            item.setUser_state_message("");
            item.setUser_profile_image(member.getProfileImageUrl());

            mRoomFriendList.add(item);
        }


        //loadRoomFriendList();
        //loadInviteFriend();

        //방장일 경우 초대/퇴출 가능
//        if(mIsOwner){
//            mBinding.tvButtonFriendInvite.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
//            mBinding.tvButtonFriendInvite.setEnabled(true);
//
//            mBinding.tvButtonFriendKick.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
//            mBinding.tvButtonFriendKick.setEnabled(true);
//
//        }else{
//            mBinding.tvButtonFriendInvite.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);
//            mBinding.tvButtonFriendInvite.setEnabled(false);
//
//            mBinding.tvButtonFriendKick.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);
//            mBinding.tvButtonFriendKick.setEnabled(false);
//        }

        mBinding.tvButtonFriendInvite.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
        mBinding.tvButtonFriendInvite.setEnabled(true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MineTalk.REQUEST_CODE_SELECT_FRIEND && resultCode == RESULT_OK) {
            if(data != null && data.getExtras() != null) {
                String result = data.getExtras().getString("result");
                ChatRepository.getInstance().initialize();
                if(result.equals("invite_complete")) {
                    setResult(RESULT_OK);
                    finish();
                }
            }
        }
    }
}
