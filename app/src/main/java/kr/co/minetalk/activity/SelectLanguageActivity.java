package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;

import java.util.Locale;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.databinding.ActivitySelectLanguageBinding;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.ui.popup.SimpleListPopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;

public class SelectLanguageActivity extends BindActivity<ActivitySelectLanguageBinding> {
    static enum TARGET_TYPE{
        TARGET_ON2ON,
        TARGET_CAMERA
    }
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SelectLanguageActivity.class);
        context.startActivity(intent);
    }


    private CountryRepository mCountryRepository;

    private String mTopLanguageCode = "";
    private String mTopVoiceCode    = "";
    private String mTopLanguageName = "";
    private int    mTopFlagResource = -1;

    private String mBottomLanguageCode = "";
    private String mBottomVoiceCode    = "";
    private String mBottomLanguageName = "";
    private int    mBottomFlagResource = -1;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_select_language;
    }

    @Override
    protected void initView() {
        initViewValue();
        setDefaultValue();
        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.tvButtonChangeLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int radioButtonID = mBinding.rgRoot.getCheckedRadioButtonId();
                View radioButton = mBinding.rgRoot.findViewById(radioButtonID);
                String lanCo = radioButton.getTag().toString();
                Preferences.setChatTranslationLanguageCode(lanCo);
            }
        });


        // 1:1 음성 번역어 지정
        mBinding.tvButtonSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupTopLanuge(TARGET_TYPE.TARGET_ON2ON);
            }
        });

        mBinding.tvButtonTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupBottomLanuge(TARGET_TYPE.TARGET_ON2ON);
            }
        });

        // 이미지(카메라) 번역어 지정
        mBinding.tvButtonCameraSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupTopLanuge(TARGET_TYPE.TARGET_CAMERA);
            }
        });

        mBinding.tvButtonCameraTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupBottomLanuge(TARGET_TYPE.TARGET_CAMERA);
            }
        });

        //mBinding.checkKo.setOnClickListener(mCheckLanguageViewListener);
        //mBinding.checkCn.setOnClickListener(mCheckLanguageViewListener);
        //mBinding.checkJa.setOnClickListener(mCheckLanguageViewListener);
        //mBinding.checkEn.setOnClickListener(mCheckLanguageViewListener);
        //mBinding.checkVi.setOnClickListener(mCheckLanguageViewListener);
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvSystemLan.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTransLan.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvMainTransLan.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTargetLan.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvSourceLan.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvOriginLan.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTranslationLan.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvButtonChangeLang.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.checkKo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.checkEn.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.checkCn.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.checkJa.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.checkVi.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.checkKo.setButtonDrawable(R.drawable.radiobutton_selector_bk);
            mBinding.checkEn.setButtonDrawable(R.drawable.radiobutton_selector_bk);
            mBinding.checkCn.setButtonDrawable(R.drawable.radiobutton_selector_bk);
            mBinding.checkJa.setButtonDrawable(R.drawable.radiobutton_selector_bk);
            mBinding.checkVi.setButtonDrawable(R.drawable.radiobutton_selector_bk);


        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvSystemLan.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTransLan.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvMainTransLan.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvSourceLan.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTransLan.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvOriginLan.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTranslationLan.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvButtonChangeLang.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.checkKo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.checkEn.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.checkCn.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.checkJa.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.checkVi.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.checkKo.setButtonDrawable(R.drawable.radiobutton_selector);
            mBinding.checkEn.setButtonDrawable(R.drawable.radiobutton_selector);
            mBinding.checkCn.setButtonDrawable(R.drawable.radiobutton_selector);
            mBinding.checkJa.setButtonDrawable(R.drawable.radiobutton_selector);
            mBinding.checkVi.setButtonDrawable(R.drawable.radiobutton_selector);

        }
    }

    private void initViewValue() {
        mCountryRepository = CountryRepository.getInstance();

        CountryInfoModel info = CountryRepository.getInstance().getCountryModel(Locale.getDefault().getLanguage());

        mBinding.tvButtonSystemLanguage.setText(info.getCountryNameOther());
        mBinding.checkKo.setText(getString(R.string.lang_ko));
        mBinding.checkKo.setTag("ko");
        mBinding.checkCn.setText(getString(R.string.lang_cn));
        mBinding.checkCn.setTag("zh-CN");
        mBinding.checkEn.setText(getString(R.string.lang_en));
        mBinding.checkEn.setTag("en");
        mBinding.checkJa.setText(getString(R.string.lang_ja));
        mBinding.checkJa.setTag("ja");
        mBinding.checkVi.setText(getString(R.string.lang_vi));
        mBinding.checkVi.setTag("vi");


        String topLanguage = Preferences.getCameraSourceCode();
        mBinding.tvButtonCameraSource.setText(getCountryName(topLanguage, "ko"));

        String bottomLanguage = Preferences.getCameraTargetCode();
        mBinding.tvButtonCameraTarget.setText(getCountryName(bottomLanguage, "en"));

        topLanguage = Preferences.getVoiceBottomLanguage();
        mBinding.tvButtonSource.setText(getCountryName(topLanguage, "ko"));

        bottomLanguage = Preferences.getVoiceTopLanguage();
        mBinding.tvButtonTarget.setText(getCountryName(bottomLanguage, "en"));

    }

    private String getCountryName(final String lanCode, final String defaultLan){
        String languageName = "";

        if(lanCode.equals("")) {
            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                languageName = mCountryRepository.getCountryModel(defaultLan).getCountryName();
            } else {
                languageName = mCountryRepository.getCountryModel(defaultLan).getCountryNameOther();
            }
        } else {
            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                languageName = mCountryRepository.getCountryModel(lanCode).getCountryName();
            } else {
                languageName = mCountryRepository.getCountryModel(lanCode).getCountryNameOther();
            }
        }

        return languageName+"↓";
    }

    private void setDefaultValue() {
        String code = Preferences.getChatTranslationLanguageCode();
        if(code.equals("ko")) {
            mBinding.checkKo.setChecked(true);
            mBinding.checkCn.setChecked(false);
            mBinding.checkEn.setChecked(false);
            mBinding.checkJa.setChecked(false);
            mBinding.checkVi.setChecked(false);
        } else if(code.equals("zh-CN")) {
            mBinding.checkKo.setChecked(false);
            mBinding.checkCn.setChecked(true);
            mBinding.checkEn.setChecked(false);
            mBinding.checkJa.setChecked(false);
            mBinding.checkVi.setChecked(false);
        } else if(code.equals("ja")) {
            mBinding.checkKo.setChecked(false);
            mBinding.checkCn.setChecked(false);
            mBinding.checkEn.setChecked(false);
            mBinding.checkJa.setChecked(true);
            mBinding.checkVi.setChecked(false);
        } else if(code.equals("en")) {
            mBinding.checkKo.setChecked(false);
            mBinding.checkCn.setChecked(false);
            mBinding.checkEn.setChecked(true);
            mBinding.checkJa.setChecked(false);
            mBinding.checkVi.setChecked(false);
        } else if(code.equals("vi")) {
            mBinding.checkKo.setChecked(false);
            mBinding.checkCn.setChecked(false);
            mBinding.checkEn.setChecked(false);
            mBinding.checkJa.setChecked(false);
            mBinding.checkVi.setChecked(true);
        }
    }

    private void popupTopLanuge(TARGET_TYPE type) {
        SimpleListPopup simpleListPopup = new SimpleListPopup();
        simpleListPopup.setOnItemIndexListener(new SimpleListPopup.OnItemIndexListener() {
            @Override
            public void onItemClick(int index) {
                mTopLanguageCode = mCountryRepository.getItem(index).getCode();
                mTopVoiceCode    = mCountryRepository.getItem(index).getVoice_code();
                mTopLanguageName = mCountryRepository.getItem(index).getCountryName();

                if(type == TARGET_TYPE.TARGET_CAMERA) {
                    mBinding.tvButtonCameraSource.setText(getCountryName(mTopLanguageCode, "ko"));
                    Preferences.setCameraSourceCode(mTopLanguageCode);
                }
                else {
                    mBinding.tvButtonSource.setText(getCountryName(mTopLanguageCode, "en"));
                    Preferences.setVoiceBottomLanguage(mTopLanguageCode);
                }

            }
        });
        simpleListPopup.show(getSupportFragmentManager(), "popup_top_lang");
    }

    private void  popupBottomLanuge(TARGET_TYPE type) {
        SimpleListPopup simpleListPopup = new SimpleListPopup();
        simpleListPopup.setOnItemIndexListener(new SimpleListPopup.OnItemIndexListener() {
            @Override
            public void onItemClick(int index) {
                mBottomLanguageCode = mCountryRepository.getItem(index).getCode();
                mBottomVoiceCode    = mCountryRepository.getItem(index).getVoice_code();
                mBottomFlagResource = CommonUtils.getResourceImage(SelectLanguageActivity.this, mCountryRepository.getItem(index).getFlag_image());

                if(type == TARGET_TYPE.TARGET_CAMERA) {
                    mBinding.tvButtonCameraTarget.setText(getCountryName(mBottomLanguageCode, "en"));
                    Preferences.setCameraTargetCode(mBottomLanguageCode);
                }
                else {
                    mBinding.tvButtonTarget.setText(getCountryName(mBottomLanguageCode, "ko"));
                    Preferences.setVoiceTopLanguage(mBottomLanguageCode);
                }

            }
        });
        simpleListPopup.show(getSupportFragmentManager(), "popup_bot_lang");
    }


//    private View.OnClickListener mCheckLanguageViewListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            switch (v.getId()) {
//                case R.id.check_ko:
//                    Preferences.setChatTranslationLanguageCode(CountryRepository.getInstance().getItem(0).getCode());
//                    mBinding.checkKo.setSelected(true);
//                    mBinding.checkCn.setSelected(false);
//                    mBinding.checkEn.setSelected(false);
//                    mBinding.checkJa.setSelected(false);
//                    mBinding.checkVi.setSelected(false);
//
//                    break;
//                case R.id.check_cn:
//                    Preferences.setChatTranslationLanguageCode(CountryRepository.getInstance().getItem(1).getCode());
//                    mBinding.checkKo.setSelected(false);
//                    mBinding.checkCn.setSelected(true);
//                    mBinding.checkEn.setSelected(false);
//                    mBinding.checkJa.setSelected(false);
//                    mBinding.checkVi.setSelected(false);
//                    break;
//                case R.id.check_ja:
//                    Preferences.setChatTranslationLanguageCode(CountryRepository.getInstance().getItem(3).getCode());
//                    mBinding.checkKo.setSelected(false);
//                    mBinding.checkCn.setSelected(false);
//                    mBinding.checkEn.setSelected(false);
//                    mBinding.checkJa.setSelected(true);
//                    mBinding.checkVi.setSelected(false);
//                    break;
//                case R.id.check_en:
//                    Preferences.setChatTranslationLanguageCode(CountryRepository.getInstance().getItem(2).getCode());
//                    mBinding.checkKo.setSelected(false);
//                    mBinding.checkCn.setSelected(false);
//                    mBinding.checkEn.setSelected(true);
//                    mBinding.checkJa.setSelected(false);
//                    mBinding.checkVi.setSelected(false);
//                    break;
//                case R.id.check_vi:
//                    Preferences.setChatTranslationLanguageCode(CountryRepository.getInstance().getItem(4).getCode());
//                    mBinding.checkKo.setSelected(false);
//                    mBinding.checkCn.setSelected(false);
//                    mBinding.checkEn.setSelected(false);
//                    mBinding.checkJa.setSelected(false);
//                    mBinding.checkVi.setSelected(true);
//                    break;
//            }
//        }
//    };


}
