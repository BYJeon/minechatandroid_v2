package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.ReportListAdapter;
import kr.co.minetalk.api.ListPointApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.PointHistoryResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityPointHistoryBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;

public class PointHistoryActivity extends BindActivity<ActivityPointHistoryBinding> {

    private static final String COLUMN_POINT_TYPE = "point_type";

    public static void startActivity(Context context, String point_type) {
        Intent intent = new Intent(context, PointReportActivity.class);
        intent.putExtra(COLUMN_POINT_TYPE, point_type);
        context.startActivity(intent);
    }

    private ListPointApi mListPointApi;
    private ReportListAdapter mAdapter;

    private String mPointType = MineTalk.POINT_TYPE_CASH;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            pointHistoryRequest(mPointType);
        }

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_point_history;
    }

    @Override
    protected void initView() {
        if(getIntent() != null && getIntent().getExtras() != null) {
            mPointType = getIntent().getExtras().getString(COLUMN_POINT_TYPE, MineTalk.POINT_TYPE_CASH);
        }
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        mAdapter = new ReportListAdapter();
        mBinding.listView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        setupCheckBox();
        pointHistoryRequest(mPointType);
    }

    private void initApi() {
        mListPointApi = new ListPointApi(this, new ApiBase.ApiCallBack<PointHistoryResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointHistoryActivity.this);
            }

            @Override
            public void onSuccess(int request_code, PointHistoryResponse pointHistoryResponse) {
                ProgressUtil.hideProgress(PointHistoryActivity.this);
                if(pointHistoryResponse != null) {
                    setupData(pointHistoryResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointHistoryActivity.this);
                showMessageAlert(message);

            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.tvCheckCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPointType = MineTalk.POINT_TYPE_CASH;
                setupCheckBox();

                pointHistoryRequest(mPointType);
            }
        });

        mBinding.tvCheckToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPointType = MineTalk.POINT_TYPE_TOKEN;
                setupCheckBox();

                pointHistoryRequest(mPointType);
            }
        });

        mBinding.ivRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userInfoRequest();
            }
        });
    }

    private void setupCheckBox() {
        if(mPointType.equals(MineTalk.POINT_TYPE_CASH)) {
            mBinding.tvCheckCash.setSelected(true);
            mBinding.tvCheckToken.setSelected(false);

            mBinding.layoutTokenInfo.setVisibility(View.GONE);
            mBinding.tvTotalPoint.setVisibility(View.VISIBLE);

        } else {
            mBinding.tvCheckCash.setSelected(false);
            mBinding.tvCheckToken.setSelected(true);

            setupTokenInfoData();

            mBinding.tvTotalPoint.setVisibility(View.INVISIBLE);
            mBinding.layoutTokenInfo.setVisibility(View.VISIBLE);
        }

    }

    private void pointHistoryRequest(String point_type) {
        mListPointApi.execute(point_type);
    }

    private void setupData(PointHistoryResponse response) {
        if(mPointType.equals(MineTalk.POINT_TYPE_CASH)) {
            mBinding.tvTotalPoint.setText(String.format(getString(R.string.point_report_total_cash), CommonUtils.comma_won(response.getTotal_point())));
        } else {
            mBinding.tvTotalPoint.setText(String.format(getString(R.string.point_report_total_token), CommonUtils.comma_won(response.getTotal_point())));
        }
        mAdapter.setData(response.getPoint_history());
    }


    private void setupTokenInfoData() {
        String totalToken = "";
        String nomalToken = "";
        String lockToken = "";

        try {
            totalToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getTotal_mine_token());
            nomalToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getUser_mine_token());
            lockToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getLock_mine_token());
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        String unit = getResources().getString(R.string.more_fragment_text_unit_token);

        mBinding.tvTotal.setText("합산 : " + totalToken + unit);
        mBinding.tvNormal.setText("일반 : " + nomalToken + unit);
        mBinding.tvDelay.setText("유예 : " + lockToken + unit);

    }


}
