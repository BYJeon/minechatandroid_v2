package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import org.apache.commons.lang3.StringEscapeUtils;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ModifyUserStateMessageApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditStateMessageBinding;
import kr.co.minetalk.utils.ProgressUtil;

public class EditStateMessageActivity extends BindActivity<ActivityEditStateMessageBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditStateMessageActivity.class);
        context.startActivity(intent);
    }

    private ModifyUserStateMessageApi mModifyUserStateMessageApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_state_message;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        String defaultValue = MineTalkApp.getUserInfoModel().getUser_state_message();
        mBinding.etStateMessage.setText(defaultValue);

    }

    private void initApi() {
        mModifyUserStateMessageApi = new ModifyUserStateMessageApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditStateMessageActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditStateMessageActivity.this);
                if(baseModel != null) {
                    userInfoRequest();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditStateMessageActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editStateMessageRequest(StringEscapeUtils.unescapeJava(mBinding.etStateMessage.getText().toString()));
            }
        });
    }

    private void editStateMessageRequest(String inputMessage) {
        if(inputMessage == null || inputMessage.equals("")) {
            showMessageAlert("");
            return;
        }

        mModifyUserStateMessageApi.execute(inputMessage);
    }
}
