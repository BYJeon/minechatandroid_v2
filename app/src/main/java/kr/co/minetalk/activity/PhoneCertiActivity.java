package kr.co.minetalk.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.SmsMessage;
import android.view.View;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.CheckAddCertificationApi;
import kr.co.minetalk.api.CheckUserHPApi;
import kr.co.minetalk.api.PwinquiryApi;
import kr.co.minetalk.api.RequestPhoneCertificationApi;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.PwInquiryModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityPhoneCertiBinding;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class PhoneCertiActivity extends BindActivity<ActivityPhoneCertiBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PhoneCertiActivity.class);
        context.startActivity(intent);
    }

    private RequestSmsApi mRequestSmsApi;
    private PwinquiryApi mPwinquiryApi;

    private String mNationCode = "82";
    private int mNationCodePositionIndex = 0;
    private boolean isPhoneDuplicateCheck = false;

    private RequestPhoneCertificationApi mPhoneCertiApi;
    private CheckUserHPApi mCheckUserHpApi;
    private CheckAddCertificationApi mCheckAddCertiApi;

    private int mRemainTime = 180;
    private Timer mTimer = null;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
        //syncFriendList();
        //MainActivity.startActivity(LoginActivity.this);
        //finish();
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        Preferences.setLoginAuthorization("");
        Preferences.setAuthorization("");
        Preferences.setLoginType("");

        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_phone_certi;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            mBinding.tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            mBinding.tvNationCode.setText("한국 " + "+82");
            mNationCode = "82";
        }

        mNationCodePositionIndex = 0;

        if(MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("Y") && !MineTalkApp.getUserInfoModel().getUser_hp().equals("")){ //인증이 되어있는 경우
            mBinding.layoutOrgRoot.setVisibility(View.VISIBLE);
            mBinding.tvOrgPhoneNum.setText(String.format("(+%s) %s", mNationCode, MineTalkApp.getUserInfoModel().getUser_hp()));
            mBinding.tvPhoneAuthInfoTitle.setText(getString(R.string.change_phone_title));
        }else{
            mBinding.tvPhoneAuthInfoTitle.setText(getString(R.string.new_phone_title));
            mBinding.layoutOrgRoot.setVisibility(View.GONE);
        }

        if(MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("Y") && !MineTalkApp.getUserInfoModel().getUser_email().equals("")){ //이메일 인증이 되어있는 경우
            mBinding.btnEmailCerti.setVisibility(View.GONE);
        }else{
            mBinding.btnEmailCerti.setVisibility(View.VISIBLE);
        }

//        if(Build.VERSION.SDK_INT >= 23) {
//            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
//                    ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
//                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.RECEIVE_SMS,
//                        Manifest.permission.READ_SMS}, PERMISSON);
//            } else {
//                initSMSCatchReceiver();
//            }
//        }
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.layoutNationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String[] array = NumberCode.getIntance().getmTitleArray();
                    showBottomWheelPopup(array, mNationCodePositionIndex, mNationCodePickerListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //중복확인
        mBinding.btnCheckDuplicate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                String phone = mBinding.etPhoneNumber.getText().toString();

                mBinding.layoutSendEmail.setVisibility(View.INVISIBLE);
                mBinding.layoutWarningEmail.setVisibility(View.INVISIBLE);

                if(phone.equals("")){
                    showMessageAlert(getString(R.string.phone_input_empty_warning));
                    return;
                }

                mCheckUserHpApi.execute(mNationCode, phone);
            }
        });

        //인증번호 발송
        mBinding.btnSendAuthNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = mBinding.etPhoneNumber.getText().toString();
                mPhoneCertiApi.execute(mNationCode, phone);
            }
        });


        //인증 요청
        mBinding.btnSendAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String authNum = mBinding.etAuthNumber.getText().toString().trim();

                if(authNum.equals("")){
                    showMessageAlert(getString(R.string.phone_empty_auth_num));
                    return;
                }

                if(!isPhoneDuplicateCheck){
                    showMessageAlert(getString(R.string.phone_do_not_auth_num));
                    return;
                }

                mCheckAddCertiApi.execute("phone", authNum);
            }
        });

        mBinding.btnEmailCerti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailCertiActivity.startActivity(PhoneCertiActivity.this);
            }
        });

//        mBinding.btnCopy.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String tempPassword = mBinding.tvFindPassword.getText().toString();
//                if(tempPassword == null || tempPassword.equals("")) {
//                    return;
//                }
//
//                ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//                ClipData clipData = ClipData.newPlainText("TempPassword", tempPassword);
//                clipboardManager.setPrimaryClip(clipData);
//
//                Toast.makeText(PhoneCertiActivity.this,getResources().getString(R.string.find_password_copy_message),Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        mBinding.btnFind.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                findPasswordRequest();
//            }
//        });
//
//        mBinding.btnAuth.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                smsRequest();
//            }
//        });
    }

    private void initApi() {
        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PhoneCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                if(baseModel != null) {
                    showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mPhoneCertiApi = new RequestPhoneCertificationApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PhoneCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                if(baseModel != null && baseModel.getCode().equals("0000")) {

                    mBinding.btnSendAuth.setEnabled(true);
                    mBinding.btnSendAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
                    //인증 번호 발송 성공
                    showMessageAlert(getResources().getString(R.string.phone_send_auth_success));
                    mBinding.tvRemainTime.setVisibility(View.VISIBLE);
                    TimerStop();

                    mTimer = new Timer();
                    mTimer.schedule(new CustomTimer(), 1000, 1000);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                TimerStop();
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckUserHpApi = new CheckUserHPApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PhoneCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                if(baseModel != null && baseModel.getCode().equals("0000")) {
                    mBinding.tvInputAuthNumDesc.setVisibility(View.VISIBLE);
                    //중복
                    //showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));

                    mBinding.layoutSendEmail.setVisibility(View.VISIBLE);
                    mBinding.layoutWarningEmail.setVisibility(View.INVISIBLE);
                    isPhoneDuplicateCheck = true;

                    //중복이 없을 시 인증 번호 발송 요청을 한다.
                    String phone = mBinding.etPhoneNumber.getText().toString();
                    mPhoneCertiApi.execute(mNationCode, phone);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                showMessageAlert(message);
                mBinding.layoutSendEmail.setVisibility(View.INVISIBLE);
                mBinding.layoutWarningEmail.setVisibility(View.VISIBLE);
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                isPhoneDuplicateCheck = false;
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckAddCertiApi = new CheckAddCertificationApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PhoneCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                if(baseModel != null && baseModel.getCode().equals("0000")) {
                    MineTalkApp.getUserInfoModel().setUser_hp_confirm("Y");
                    MineTalkApp.getUserInfoModel().setUser_hp(mBinding.etPhoneNumber.getText().toString());
                    mBinding.etAuthNumber.setText("");
                    mBinding.etPhoneNumber.setText("");
                    TimerStop();
                    mBinding.tvRemainTime.setVisibility(View.INVISIBLE);

                    mBinding.btnSendAuth.setEnabled(false);
                    mBinding.btnSendAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);

                    //인증완료
                    showMessageAlert(getResources().getString(R.string.phone_auth_success));
                }else if(baseModel != null && baseModel.getCode().equals("0001")){
                    MineTalkApp.getUserInfoModel().setUser_hp_confirm("Y");
                    MineTalkApp.getUserInfoModel().setUser_hp(mBinding.etPhoneNumber.getText().toString());
                    mBinding.etAuthNumber.setText("");
                    mBinding.etPhoneNumber.setText("");
                    TimerStop();
                    mBinding.tvRemainTime.setVisibility(View.INVISIBLE);

                    mBinding.btnSendAuth.setEnabled(false);
                    mBinding.btnSendAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);

                    //탈퇴자 인증완료
                    showMessageAlert(baseModel.getMessage());
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                TimerStop();
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                showMessageAlert(message);
                mBinding.layoutSendEmail.setVisibility(View.INVISIBLE);
                mBinding.layoutWarningEmail.setVisibility(View.INVISIBLE);
                //isPhoneDuplicateCheck = false;
            }

            @Override
            public void onCancellation() {

            }
        });

        mPwinquiryApi = new PwinquiryApi(this, new ApiBase.ApiCallBack<PwInquiryModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PhoneCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, PwInquiryModel pwInquiryModel) {
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                if(pwInquiryModel != null) {
                    //mBinding.tvFindPassword.setText(pwInquiryModel.getTemp_password());
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PhoneCertiActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    // 전화번호 국가 코드 선택 Listener
    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
        @Override
        public void onClick(int state, int num) {
            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                mNationCodePositionIndex = num;
                try {
                    String viewName = NumberCode.getIntance().getDataList().get(num).getName();
                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");

                    mBinding.tvNationCode.setText(viewName);
                } catch (Exception e) {
                    e.printStackTrace();
                    mNationCode = "82";
                }
            }
        }

        @Override
        public void onClick(int state, int num, int position) {

        }
    };


    private void smsRequest() {
        String phoneNumber = mBinding.etPhoneNumber.getText().toString();
        if(phoneNumber == null || phoneNumber.replace(" ", "").equals("")) {
            showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
            return;
        }
        mRequestSmsApi.execute(MineTalk.SMS_TYPE_PWINQUIRY, mNationCode, phoneNumber);
    }

    private void findPasswordRequest() {
        String phoneNumber = mBinding.etPhoneNumber.getText().toString();
        if(phoneNumber == null || phoneNumber.replace(" ", "").equals("")) {
            showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
            return;
        }

        String authNumber = mBinding.etAuthNumber.getText().toString();
        if(authNumber == null || authNumber.replace(" ", "").equals("")) {
            showMessageAlert(getResources().getString(R.string.find_password_validation_auth_number));
            return;
        }

        mPwinquiryApi.execute(mNationCode, phoneNumber, authNumber);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mAuthSMSBroadCastReceiver != null) {
            unregisterReceiver(mAuthSMSBroadCastReceiver);
        }

        TimerStop();
    }

    private void TimerStop(){
        if(mTimer != null)
            mTimer.cancel();

        mRemainTime = 180;
    }

    private static final String ACTION_SMS = "android.provider.Telephony.SMS_RECEIVED";
    private BroadcastReceiver mAuthSMSBroadCastReceiver;
    private void initSMSCatchReceiver() {

        IntentFilter filter = new IntentFilter();
        filter.setPriority(999);
        filter.addAction(ACTION_SMS);


        mAuthSMSBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent.getAction().equals(ACTION_SMS)) {

                    Bundle bundle = intent.getExtras();
                    if(bundle == null)
                        return;

                    Object[] pdusObj = (Object[]) bundle.get("pdus");
                    if(pdusObj == null)
                        return;

                    SmsMessage[] smsMessages = new SmsMessage[pdusObj.length];

                    for (int i = 0; i < pdusObj.length; i++) {

                        smsMessages[i] = SmsMessage.createFromPdu((byte[])pdusObj[i]);

                        final String address = smsMessages[i].getOriginatingAddress();
                        final String message = smsMessages[i].getDisplayMessageBody();

                        String smsPhone = Preferences.getSmsPhone();
                        if( smsPhone.equals(address.replace("-","")) ) {
                            abstrackNumber(message);
                        }
                    }

                    this.abortBroadcast();
                }
            }

            private void abstrackNumber(String message) {
                Pattern p = Pattern.compile("([^\\d])");
                Matcher matcher = p.matcher(message);
                StringBuffer destStringBuffer = new StringBuffer();

                while (matcher.find()){
                    matcher.appendReplacement(destStringBuffer, "");
                }

                matcher.appendTail(destStringBuffer);
                mBinding.etAuthNumber.setText(destStringBuffer.toString());

            }

        };
        registerReceiver(mAuthSMSBroadCastReceiver, filter);
    }

    private final int PERMISSON = 1;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initSMSCatchReceiver();
                } else {
                    return;
                }
        }
    }


    // 첫 번째 TimerTask 를 이용한 방법
    class CustomTimer extends TimerTask {
        @Override
        public void run() {
            mRemainTime -= 1;

            if(mRemainTime == 0){
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                showMessageAlert(getResources().getString(R.string.phone_time_over_auth_num));
                TimerStop();
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBinding.tvRemainTime.setText(String.format("%02d:%02d", mRemainTime / 60 , mRemainTime % 60) );
                }
            });
        }
    }

}
