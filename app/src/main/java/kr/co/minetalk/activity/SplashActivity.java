package kr.co.minetalk.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import java.util.Random;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.BuildConfig;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnPermissionListener;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.api.VersionApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.api.model.VersionModel;
import kr.co.minetalk.databinding.ActivitySplashBinding;
import kr.co.minetalk.locker.view.AppLocker;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.ServerLiveApi;
import kr.co.minetalk.message.model.ServerLiveData;
import kr.co.minetalk.ui.PermissionNotiDialog;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class SplashActivity extends BindActivity<ActivitySplashBinding> implements OnPermissionListener {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }


    private VersionApi mVersionApi;

    private int [] bgImageArrKo = { R.drawable.img_bg_splash_a, R.drawable.img_bg_splash_b };

    //private int [] bgImageArr = { R.drawable.img_bg_splash_d, R.drawable.img_bg_splash_e };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {
        //크리스마스 배경을 random 하게 출력
        long seed = System.currentTimeMillis();
        Random rand = new Random(seed);
        int bgIdx = rand.nextInt(2);

//        String locale = Locale.getDefault().getLanguage();
//        if(locale.equals("ko")) //한국
//            mBinding.layoutRoot.setBackgroundResource(bgImageArrKo[bgIdx]);
//        else
//            mBinding.layoutRoot.setBackgroundResource(bgImageArr[bgIdx]);

        mBinding.layoutRoot.setBackgroundResource(bgImageArrKo[bgIdx]);

        if(getIntent() != null && getIntent().getExtras() != null) {
            MineTalk.PUSH_THREAD_KEY = getIntent().getExtras().getString("threadKey");
            MineTalk.AD_IDX = getIntent().getExtras().getString("adIdx", "");
            MineTalk.AD_TITLE = getIntent().getExtras().getString("adTitle", "");
        }

        initApi();
        mBinding.setHandlers(this);
        mSyncListener = mFriendSyncListner;

        this.mIsShowProgress = false;

        if (!Preferences.getAuthorityNotice()) {
            PermissionNotiDialog dialog = new PermissionNotiDialog(this, this);
            dialog.setCancelable(false);
            dialog.show();
        }else{
            onPermissionCheckComplete();
        }

        //서버정검 시간 확인버튼
        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);

            if( MineTalk.isDebug ){
                Preferences.setLoginType(MineTalk.X_LOGIN_TYPE);
                String xid = MineTalkApp.getUserInfoModel().getUser_xid();
                String token =  MineTalkApp.getUserInfoModel().getAuth_token();
                String tokenAuth = CommonUtils.makeAuthorization(xid, token);
                Preferences.setAuthorization(tokenAuth);
            }

            syncFriendList();
            AppLocker.getInstance().enableAppLock(getApplication(), Preferences.getPinScreenLock());
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
//        if(request_code == 401) {

            try {
                showMessageAlert(message, new PopupListenerFactory.SimplePopupListener() {
                    @Override
                    public void onClick(DialogInterface f, int state) {
                        if (state == PopupListenerFactory.SimplePopupListener.STATE_OK) {
                            Preferences.setLoginType("");
                            Preferences.setNationCode("");
                            Preferences.setLoginType("");
                            Preferences.setLoginAuthorization("");
                            Preferences.setAuthorization("");
                            Preferences.setUserPassword("");

                            mIsShowProgress = true;

                            LoginActivity.startActivity(SplashActivity.this);
                            finish();
                        }
                    }
                });
            }catch (IllegalStateException e){
                e.printStackTrace();
                LoginActivity.startActivity(SplashActivity.this);
                finish();
            }
//        }


    }

    @Override
    public void onPermissionCheckComplete() {
        ServerLiveApi serverLiveApi = new ServerLiveApi(this);
        serverLiveApi.request(new BaseHttpRequest.APICallbackListener<ServerLiveData>() {
            @Override
            public void onSuccess(ServerLiveData res) {
                //ProgressUtil.hideProgress(SplashActivity.this);
                //서버가 살아있는지 여부 판단.
                String serverActive = res.getServer_active();
                if(serverActive.equals("Y")){
                    checkPermission();
                }else{
                    mBinding.tvTime.setText(res.getCheck_time());
                    mBinding.layoutServerDisabled.setVisibility(View.VISIBLE);
                    //showMessageAlert(res.getCheck_time());
                }
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                //ProgressUtil.hideProgress(SplashActivity.this);
                checkPermission();
                Log.e("@@@TAG", error.getMessage());
            }
        });
    }

    private void initApi() {
        mVersionApi = new VersionApi(this, new ApiBase.ApiCallBack<VersionModel>() {
            @Override
            public void onPreparation() {

            }

            @Override
            public void onSuccess(int request_code, VersionModel versionModel) {
                ProgressUtil.hideProgress(SplashActivity.this);
                if(versionModel != null) {
                    Preferences.setSmsPhone(versionModel.getSms_phone());

                    String localVersion  = BuildConfig.VERSION_NAME;
                    String serverVersion = versionModel.getVersion();

                    int lVersion = Integer.parseInt(localVersion.replace(".", ""));
                    int sVersion = Integer.parseInt(serverVersion.replace(".", ""));

                    MineTalkApp.NOTICE_UPDATE_YN = versionModel.getForce_yn();
                    MineTalkApp.NOITICE_YN = versionModel.getNotice_yn();
                    MineTalkApp.NOITICE_TITLE = versionModel.getNotice_title();
                    MineTalkApp.NOITICE_CONTENTS = versionModel.getNotice_contents();

                    if(lVersion >= sVersion) {
                        checkAutoLogin();
                    } else {
                        if(MineTalkApp.NOTICE_UPDATE_YN.equals("Y")) {//강제 업데이트
                            showVersionAlert();
                        }else if(MineTalkApp.NOTICE_UPDATE_YN.equals("S")) { //선택 업데이트
                            showMessageAlert(false,
                                    getString(R.string.app_update_message),
                                    "",
                                    getString(R.string.common_ok),
                                    getString(R.string.common_cancel),
                                    new PopupListenerFactory.SimplePopupListener() {
                                        @Override
                                        public void onClick(DialogInterface f, int state) {
                                            if (state == PopupListenerFactory.SimplePopupListener.STATE_OK) {
                                                // 마켓 이동
                                                moveGooglePlayStore();
                                                finishApp();
                                            } else{
                                                checkAutoLogin();
                                            }
                                        }
                                    });
                        }else {
                            checkAutoLogin();
                        }
                    }
                }

            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(SplashActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }


    private void moveMainPage() {
        mIsShowProgress = true;
        MainActivity.startActivity(SplashActivity.this);
        finish();
    }

    private void checkAutoLogin() {
        String authoriztion = Preferences.getLoginAuthorization();
        if(authoriztion != null && !authoriztion.equals("")) {
            loginRequest();
        } else {
            mIsShowProgress = true;

            LoginActivity.startActivity(this);
            finishApp();
        }
    }

    private void showVersionAlert() {

        try {
            showMessageAlert(false,
                    getString(R.string.app_update_message),
                    "",
                    getString(R.string.common_ok),
                    getString(R.string.common_cancel),
                    new PopupListenerFactory.SimplePopupListener() {
                        @Override
                        public void onClick(DialogInterface f, int state) {
                            if (state == PopupListenerFactory.SimplePopupListener.STATE_OK) {
                                // 마켓 이동
                                moveGooglePlayStore();
                                finishApp();
                            } else if (state == PopupListenerFactory.SimplePopupListener.STATE_CANCEL) {
                                finishApp();
                            }
                        }
                    });
        }catch (IllegalStateException e){
            e.printStackTrace();
        }
    }

    private void finishApp() {
        finish();
    }

    private void moveGooglePlayStore() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + getPackageName()));
        startActivity(intent);
    }

    private void versionRequest() {

        MineTalkApp.initializeContactMap();
        if(!checkRegistration()) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    String registrationId = FirebaseInstanceId.getInstance().getToken();
                    Preferences.setPushToken(registrationId);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mVersionApi.execute(SplashActivity.this, MineTalk.getUserPlatform());
                        }
                    });
                }
            });
        } else {
            mVersionApi.execute(this, MineTalk.getUserPlatform());
        }
    }

    private boolean checkRegistration() {
        String registrationId = Preferences.getPushToken();
        if(registrationId == null || registrationId.equals("")) {
            return false;
        }
        return true;
    }



    private OnSyncListener mFriendSyncListner = new OnSyncListener() {
        @Override
        public void onSyncComplete() {
            moveMainPage();
        }
    };


    private final int PERMISSION = 1;
    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CONTACTS, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION);
            } else {
                versionRequest();
            }
        } else {
            versionRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // GRANTED
                    versionRequest();
                    return;
                } else {
                    // DENIED
//                    BeautyWeatherApp.setIsLocationPermission(false);
//                    nextPage();
                    return;
                }
        }
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutServerDisabled.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvDisabledTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTimeTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvTime.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnConfirm.setBackgroundResource(R.drawable.button_bg_cdb60c);
        }else{
            mBinding.layoutServerDisabled.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvDisabledTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTimeTitle.setTextColor(getResources().getColor(R.color.app_point_color));
            mBinding.tvTime.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnConfirm.setBackgroundResource(R.drawable.button_bg_16c066);
        }
    }
}
