package kr.co.minetalk.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.CheckSmsApi;
import kr.co.minetalk.api.PwinquiryApi;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.SecessionUserApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.PwInquiryModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityAccountWithdrawBinding;
import kr.co.minetalk.message.manager.MessageThreadManager;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.Sha256Util;

public class AccountWithdrawActivity extends BindActivity<ActivityAccountWithdrawBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, AccountWithdrawActivity.class);
        context.startActivity(intent);
    }

    private RequestSmsApi mRequestSmsApi;
    private CheckSmsApi mCheckSmsApi;

    private SecessionUserApi mSecessionUserApi;
    private PwinquiryApi mPwinquiryApi;

    private String mNationCode = "82";
    private int mNationCodePositionIndex = 0;

    private boolean isPasswordAccord = false;
    private boolean isSNSRegistUser = false;

    private int mRemainTime = 180;
    private Timer mTimer = null;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
        //syncFriendList();
        //MainActivity.startActivity(LoginActivity.this);
        //finish();
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        Preferences.setLoginAuthorization("");
        Preferences.setAuthorization("");
        Preferences.setLoginType("");

        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_account_withdraw;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");
        } catch (Exception e) {
            e.printStackTrace();
            mNationCode = "82";
        }

        mNationCodePositionIndex = 0;

        //인증 체크 이미지
        Drawable checkImage = getResources().getDrawable(R.drawable.icon_mypage_check);
        int h = checkImage.getIntrinsicHeight();
        int w = checkImage.getIntrinsicWidth();
        checkImage.setBounds(0, 0, w, h);


        if (MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("Y")) { //인증이 되어있는 경우
            mBinding.btnSendAuth.setVisibility(View.VISIBLE);
            mBinding.layoutOrgRoot.setVisibility(View.GONE);
            mBinding.tvPhoneAuthInfoTitle.setCompoundDrawables(null, null, checkImage, null);

            mBinding.etUserPassword.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.etUserPassword.setHint("");
            mBinding.etUserPassword.setFocusable(false);
            mBinding.etUserPassword.setEnabled(false);

            mBinding.btnUserPassword.setBackgroundResource(R.drawable.button_bg_4d16c066);
            mBinding.btnUserPassword.setEnabled(false);

            mBinding.tvUserPhone.setText(String.format("(+%s)%s", mNationCode, MineTalkApp.getUserInfoModel().getUser_hp()));

            mBinding.btnPhoneAuth.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnPhoneAuth.setEnabled(true);

            mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);
            mBinding.btnSendAuth.setEnabled(false);

        } else {

            String userProviderType = MineTalkApp.getUserInfoModel().getUser_provider_type();

            mBinding.btnSendAuth.setVisibility(View.GONE);
            mBinding.tvPhoneAuthInfoTitle.setCompoundDrawables(null, null, null, null);

            mBinding.etAuthNumber.setBackgroundColor(Color.parseColor("#f2f2f2"));
            mBinding.etAuthNumber.setHint("");
            mBinding.etAuthNumber.setEnabled(false);


            mBinding.btnPhoneAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);
            mBinding.btnPhoneAuth.setEnabled(false);

            mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);
            mBinding.btnSendAuth.setEnabled(false);

            //SNS 계정일 경우 바로 탈퇴하기
            if (userProviderType.equals(MineTalk.LOGIN_TYPE_FACEBOOK) || userProviderType.equals(MineTalk.LOGIN_TYPE_KAKAO)
                    || userProviderType.equals(MineTalk.LOGIN_TYPE_GOOGLE)) {
//            if(true){
                isSNSRegistUser = true;
                mBinding.btnConfirmWithdraw.setEnabled(true);
                mBinding.btnConfirmWithdraw.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_point_color) :
                        getResources().getColor(R.color.app_point_color));

                mBinding.etUserPassword.setBackgroundColor(getResources().getColor(R.color.white_color));
                mBinding.etUserPassword.setHint("");
                mBinding.etUserPassword.setFocusable(false);
                mBinding.etUserPassword.setEnabled(false);

                mBinding.btnUserPassword.setBackgroundResource(R.drawable.button_bg_4d16c066);
                mBinding.btnUserPassword.setEnabled(false);
            }
        }

        String userName = MineTalkApp.getUserInfoModel().getUser_name();
        String userId = MineTalkApp.getUserInfoModel().getUser_login_id();
        String userProvideId = MineTalkApp.getUserInfoModel().getUser_provider_id();
        String userProviderType = MineTalkApp.getUserInfoModel().getUser_provider_type();

        mBinding.tvNickname.setText(userName);
        mBinding.tvUserId.setText(userName);

        mBinding.tvUserId.setText(userProvideId);
        if (userProviderType.equals(MineTalk.LOGIN_TYPE_FACEBOOK)) {
        } else if (userProviderType.equals(MineTalk.LOGIN_TYPE_KAKAO)) {
        } else if (userProviderType.equals(MineTalk.LOGIN_TYPE_GOOGLE)) {
        } else {
            mBinding.tvUserId.setText(userId);

        }

    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //인증번호 발급 받기
        mBinding.btnPhoneAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                isPasswordAccord = false;
                mBinding.btnConfirmWithdraw.setBackgroundResource(R.drawable.button_bg_4d16c066);
                mBinding.btnConfirmWithdraw.setEnabled(false);

                mRequestSmsApi.execute(MineTalk.SMS_TYPE_SECESSION, mNationCode, MineTalkApp.getUserInfoModel().getUser_hp());
            }
        });

        //인증 요청
        mBinding.btnSendAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String authNum = mBinding.etAuthNumber.getText().toString().trim();

                if (authNum.equals("")) {
                    showMessageAlert(getString(R.string.phone_empty_auth_num));
                    return;
                }

                mCheckSmsApi.execute(MineTalk.SMS_TYPE_SECESSION, mNationCode, MineTalkApp.getUserInfoModel().getUser_hp(), authNum);
            }
        });


        //비빌번호 탈퇴 인증
        mBinding.btnUserPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pwd = Preferences.getUserPassword();
                if (mBinding.etUserPassword.getText().toString().equals(pwd)) {
                    mBinding.btnConfirmWithdraw.setBackgroundResource(R.drawable.button_bg_16c066);
                    mBinding.btnConfirmWithdraw.setEnabled(true);
                    showMessageAlert(getString(R.string.pwd_match_withdraw_warning));
                } else {
                    showMessageAlert(getString(R.string.pwd_match_wrong_warning));
                }
            }
        });

        mBinding.etUserPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mBinding.btnConfirmWithdraw.setBackgroundResource(R.drawable.button_bg_4d16c066);
                mBinding.btnConfirmWithdraw.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //탈퇴하기 버튼
        mBinding.btnConfirmWithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String hp_confirm = MineTalkApp.getUserInfoModel().getUser_hp_confirm();
                if (hp_confirm.equals("Y")) { //인증이 되어있는 경우
                    String authNum = mBinding.etAuthNumber.getText().toString().trim();

                    if (authNum.equals("")) {
                        showMessageAlert(getString(R.string.phone_empty_auth_num));
                        return;
                    }
                }

                showMessageWithWarningAlert(String.format(getString(R.string.withdraw_last_warning), MineTalkApp.getUserInfoModel().getUser_name()),
                        getResources().getString(R.string.withdraw_retore_warning),
                        getResources().getString(R.string.common_cancel),
                        getResources().getString(R.string.recommend_reg_n),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_CANCEL) {
                                    if (hp_confirm.equals("Y")) { //인증이 되어 있을 경우
                                        String authNum = mBinding.etAuthNumber.getText().toString().trim();
                                        mSecessionUserApi.execute(authNum);
                                    } else {
                                        String userProviderType = MineTalkApp.getUserInfoModel().getUser_provider_type();
                                        //SNS 계정일 경우 바로 탈퇴하기
                                        if (userProviderType.equals(MineTalk.LOGIN_TYPE_FACEBOOK) || userProviderType.equals(MineTalk.LOGIN_TYPE_KAKAO)
                                                || userProviderType.equals(MineTalk.LOGIN_TYPE_GOOGLE)) {
                                            try {
                                                mSecessionUserApi.executeBySNS("");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }else {
                                            try {
                                                String sha256Pass = Sha256Util.encoding(Preferences.getUserPassword());
                                                mSecessionUserApi.executeBySNS(sha256Pass);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            }
                        });
            }
        });
    }

    private void initApi() {
        mSecessionUserApi = new SecessionUserApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(AccountWithdrawActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(AccountWithdrawActivity.this);
                if (baseModel != null) {

                    showMessageAlert(getResources().getString(R.string.withdraw_complete_warning),
                            new PopupListenerFactory.SimplePopupListener() {
                                @Override
                                public void onClick(DialogInterface f, int state) {
                                    if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                        MessageThreadManager.getInstance().removeThreadAll();
                                        MineTalkApp.saveLoginInfoClear();
                                        LoginActivity.startActivity(AccountWithdrawActivity.this);
                                        finish();
                                    }
                                }
                            });

                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(AccountWithdrawActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(AccountWithdrawActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(AccountWithdrawActivity.this);
                if (baseModel != null) {
                    mBinding.btnSendAuth.setEnabled(true);
                    mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_16c066);

                    mBinding.tvInputAuthNumDesc.setVisibility(View.VISIBLE);
                    isPasswordAccord = true;
                    //인증 번호 발송 성공
                    showMessageAlert(getResources().getString(R.string.phone_send_auth_success));
                    mBinding.tvRemainTime.setVisibility(View.VISIBLE);
                    TimerStop();

                    mTimer = new Timer();
                    mTimer.schedule(new CustomTimer(), 1000, 1000);

                    //mBinding.btnConfirmWithdraw.setBackgroundColor(Color.parseColor("#16c066"));
                    //mBinding.btnConfirmWithdraw.setEnabled(true);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.btnSendAuth.setEnabled(false);
                mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);

                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                isPasswordAccord = false;
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                TimerStop();
                ProgressUtil.hideProgress(AccountWithdrawActivity.this);
                showMessageAlert(message);

                mBinding.btnConfirmWithdraw.setBackgroundColor(Color.parseColor("#4d16c066"));
                mBinding.btnConfirmWithdraw.setEnabled(false);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckSmsApi = new CheckSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(AccountWithdrawActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(AccountWithdrawActivity.this);
                if (baseModel != null) {

                    //인증 번호 발송 성공
                    showMessageAlert(getResources().getString(R.string.withdraw_auth_match_desc));
                    mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                    TimerStop();


                    mBinding.btnConfirmWithdraw.setBackgroundColor(Color.parseColor("#16c066"));
                    mBinding.btnConfirmWithdraw.setEnabled(true);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                isPasswordAccord = false;
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                TimerStop();
                ProgressUtil.hideProgress(AccountWithdrawActivity.this);
                showMessageAlert(message);

                mBinding.btnConfirmWithdraw.setBackgroundColor(Color.parseColor("#4d16c066"));
                mBinding.btnConfirmWithdraw.setEnabled(false);
            }

            @Override
            public void onCancellation() {

            }
        });

        mPwinquiryApi = new PwinquiryApi(this, new ApiBase.ApiCallBack<PwInquiryModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(AccountWithdrawActivity.this);
            }

            @Override
            public void onSuccess(int request_code, PwInquiryModel pwInquiryModel) {
                ProgressUtil.hideProgress(AccountWithdrawActivity.this);
                if (pwInquiryModel != null) {
                    //mBinding.tvFindPassword.setText(pwInquiryModel.getTemp_password());
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(AccountWithdrawActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAuthSMSBroadCastReceiver != null) {
            unregisterReceiver(mAuthSMSBroadCastReceiver);
        }

        TimerStop();
    }

    private void TimerStop() {
        if (mTimer != null)
            mTimer.cancel();

        mRemainTime = 180;
    }

    private static final String ACTION_SMS = "android.provider.Telephony.SMS_RECEIVED";
    private BroadcastReceiver mAuthSMSBroadCastReceiver;

    private void initSMSCatchReceiver() {

        IntentFilter filter = new IntentFilter();
        filter.setPriority(999);
        filter.addAction(ACTION_SMS);


        mAuthSMSBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(ACTION_SMS)) {

                    Bundle bundle = intent.getExtras();
                    if (bundle == null)
                        return;

                    Object[] pdusObj = (Object[]) bundle.get("pdus");
                    if (pdusObj == null)
                        return;

                    SmsMessage[] smsMessages = new SmsMessage[pdusObj.length];

                    for (int i = 0; i < pdusObj.length; i++) {

                        smsMessages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);

                        final String address = smsMessages[i].getOriginatingAddress();
                        final String message = smsMessages[i].getDisplayMessageBody();

                        String smsPhone = Preferences.getSmsPhone();
                        if (smsPhone.equals(address.replace("-", ""))) {
                            abstrackNumber(message);
                        }
                    }

                    this.abortBroadcast();
                }
            }

            private void abstrackNumber(String message) {
                Pattern p = Pattern.compile("([^\\d])");
                Matcher matcher = p.matcher(message);
                StringBuffer destStringBuffer = new StringBuffer();

                while (matcher.find()) {
                    matcher.appendReplacement(destStringBuffer, "");
                }

                matcher.appendTail(destStringBuffer);
                mBinding.etAuthNumber.setText(destStringBuffer.toString());

            }

        };
        registerReceiver(mAuthSMSBroadCastReceiver, filter);
    }

    private final int PERMISSON = 1;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initSMSCatchReceiver();
                } else {
                    return;
                }
        }
    }


    // 첫 번째 TimerTask 를 이용한 방법
    class CustomTimer extends TimerTask {
        @Override
        public void run() {
            mRemainTime -= 1;

            if (mRemainTime == 0) {
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                showMessageAlert(getResources().getString(R.string.phone_time_over_auth_num));
                TimerStop();

                mBinding.btnSendAuth.setEnabled(false);
                mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);

                return;
            }
            mBinding.tvRemainTime.setText(String.format("%02d:%02d", mRemainTime / 60, mRemainTime % 60));
        }
    }

}
