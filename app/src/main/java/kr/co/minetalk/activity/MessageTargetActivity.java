package kr.co.minetalk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ModifyUserAddressApi;
import kr.co.minetalk.api.RequestAddressApi;
import kr.co.minetalk.api.model.AddressListModel;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityMessageTargetBinding;
import kr.co.minetalk.repository.TargetMessageRepository;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.AddressListItemView;
import kr.co.minetalk.view.listener.OnCheckBarEventListener;
import kr.co.minetalk.view.listener.OnDeleteAddressListener;

public class MessageTargetActivity extends BindActivity<ActivityMessageTargetBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent( context, MessageTargetActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int requestCode) {
        Intent intent = new Intent( context, MessageTargetActivity.class);
        ((Activity)context).startActivityForResult(intent, requestCode);

    }

    public enum RequestType {
        REQUEST_WAIT,
        REQUEST_SI,
        REQUEST_GU
    }

    private final String TYPE_GENDER = "GENDER";
    private final String TYPE_AGE = "AGE";
    private final String TYPE_JOB = "JOB";

    private RequestAddressApi mRequestAddressApi;

    private ArrayList<String> mAddressSiList = new ArrayList<>();
    private ArrayList<String> mAddressGuList = new ArrayList<>();
    private RequestType mRequestType = RequestType.REQUEST_SI;

    private String mSelectSi = "";
    private String mSelectGu = "";

    private TargetMessageRepository mTargetMessageRepository;

    private HashMap<String, String> mAddressHistory = new HashMap<String, String>();

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_message_target;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        mTargetMessageRepository = TargetMessageRepository.getInstance();

        initApi();

        initButton();
        setUIEventListener();

        setDefaultData();
        siDataRequest();
    }

    private void initButton() {
        mBinding.checkBarGenderMan.setType(TYPE_GENDER);
        mBinding.checkBarGenderMan.setData(getString(R.string.msg_target_button_gender_man), "M");
        mBinding.checkBarGenderWomen.setType(TYPE_GENDER);
        mBinding.checkBarGenderWomen.setData(getString(R.string.msg_target_button_gender_women), "F");

//        mBinding.checkBarGenderMan.setCheckBarEventListener(mGenderListener);
//        mBinding.checkBarGenderWomen.setCheckBarEventListener(mGenderListener);

        mBinding.checkBar1020.setData(getString(R.string.msg_target_btn_1020), "1");
        mBinding.checkBar2030.setData(getString(R.string.msg_target_btn_2030), "2");
        mBinding.checkBar4050.setData(getString(R.string.msg_target_btn_4050), "3");
        mBinding.checkBar6070.setData(getString(R.string.msg_target_btn_6070), "4");
        mBinding.checkBar80over.setData(getString(R.string.msg_target_btn_80_over), "5");

        mBinding.checkBar1020.setType(TYPE_AGE);
        mBinding.checkBar2030.setType(TYPE_AGE);
        mBinding.checkBar4050.setType(TYPE_AGE);
        mBinding.checkBar6070.setType(TYPE_AGE);
        mBinding.checkBar80over.setType(TYPE_AGE);

//        mBinding.checkBar1020.setCheckBarEventListener(mAgeListener);
//        mBinding.checkBar2030.setCheckBarEventListener(mAgeListener);
//        mBinding.checkBar4050.setCheckBarEventListener(mAgeListener);
//        mBinding.checkBar6070.setCheckBarEventListener(mAgeListener);
//        mBinding.checkBar80over.setCheckBarEventListener(mAgeListener);


        mBinding.checkBarUser1.setData(getString(R.string.msg_target_btn_job_001), "J001");
        mBinding.checkBarUser2.setData(getString(R.string.msg_target_btn_job_002), "J002");
        mBinding.checkBarUser3.setData(getString(R.string.msg_target_btn_job_003), "J003");
        mBinding.checkBarUser4.setData(getString(R.string.msg_target_btn_job_004), "J004");
        mBinding.checkBarUser5.setData(getString(R.string.msg_target_btn_job_005), "J005");
        mBinding.checkBarUser6.setData(getString(R.string.msg_target_btn_job_999), "J999");

        mBinding.checkBarUser1.setType(TYPE_JOB);
        mBinding.checkBarUser2.setType(TYPE_JOB);
        mBinding.checkBarUser3.setType(TYPE_JOB);
        mBinding.checkBarUser4.setType(TYPE_JOB);
        mBinding.checkBarUser5.setType(TYPE_JOB);
        mBinding.checkBarUser6.setType(TYPE_JOB);

//        mBinding.checkBarUser1.setCheckBarEventListener(mJobListener);
//        mBinding.checkBarUser2.setCheckBarEventListener(mJobListener);
//        mBinding.checkBarUser3.setCheckBarEventListener(mJobListener);
//        mBinding.checkBarUser4.setCheckBarEventListener(mJobListener);
//        mBinding.checkBarUser5.setCheckBarEventListener(mJobListener);
//        mBinding.checkBarUser6.setCheckBarEventListener(mJobListener);
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.layoutButtonSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomWheelSiPopup();
            }
        });

        mBinding.layoutButtonGu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomWheelGuPopup();
            }
        });

        mBinding.tvButtonAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mSelectSi.equals("")) {
                    showMessageAlert(getString(R.string.chat_target_validate_area_sido));
                    return;
                }

                if(mSelectGu.equals("")) {
                    showMessageAlert(getString(R.string.chat_target_validate_area_gugun));
                    return;
                }

                AddressListItemView itemView = new AddressListItemView(MessageTargetActivity.this);
                itemView.setData(mSelectSi + " " + mSelectGu);
                itemView.setOnDeleteListener(mAddressDeleteListener);

                if(!mAddressHistory.containsKey(mSelectSi + " " + mSelectGu)) {
                    if(mBinding.layoutAddressContainer.getChildCount() == 0) {
                        mBinding.layoutAddressContainer.addView(itemView);
                    } else {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        layoutParams.topMargin = (int) getResources().getDimensionPixelOffset(R.dimen.height_25);

                        mBinding.layoutAddressContainer.addView(itemView, layoutParams);
                    }
                }

                mSelectSi = "";
                mSelectGu = "";

                mBinding.tvSi.setText("");
                mBinding.tvGu.setText("");


            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingData();
            }
        });
    }

    private void initApi() {
        mRequestAddressApi = new RequestAddressApi(this, new ApiBase.ApiCallBack<AddressListModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MessageTargetActivity.this);
            }

            @Override
            public void onSuccess(int request_code, AddressListModel addressListModel) {
                ProgressUtil.hideProgress(MessageTargetActivity.this);
                if(addressListModel != null) {
                    if(mRequestType == RequestType.REQUEST_SI) {
                        mAddressSiList.addAll(addressListModel.getAddr_list());
                        mRequestType = RequestType.REQUEST_WAIT;
                    } else if(mRequestType == RequestType.REQUEST_GU){
                        mAddressGuList.clear();
                        mAddressGuList.addAll(addressListModel.getAddr_list());
                        mRequestType = RequestType.REQUEST_WAIT;
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MessageTargetActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private OnDeleteAddressListener mAddressDeleteListener = new OnDeleteAddressListener() {
        @Override
        public void onAddressDelete(String idx) {
            mAddressHistory.remove(idx);
            for(int i = 0 ; i < mBinding.layoutAddressContainer.getChildCount() ; i++) {
                AddressListItemView itemView = (AddressListItemView)mBinding.layoutAddressContainer.getChildAt(i);
                String value = itemView.getValue();

                if(value.equals(idx)) {
                    mBinding.layoutAddressContainer.removeView(itemView);
                }

            }
        }
    };

    private void setDefaultData() {

        mBinding.checkBarGenderMan.setSelected(mTargetMessageRepository.containGender("M"));
        mBinding.checkBarGenderWomen.setSelected(mTargetMessageRepository.containGender("F"));

        mBinding.checkBar1020.setSelected(mTargetMessageRepository.containAge("1"));
        mBinding.checkBar2030.setSelected(mTargetMessageRepository.containAge("2"));
        mBinding.checkBar4050.setSelected(mTargetMessageRepository.containAge("3"));
        mBinding.checkBar6070.setSelected(mTargetMessageRepository.containAge("4"));
        mBinding.checkBar80over.setSelected(mTargetMessageRepository.containAge("5"));

        mBinding.checkBarUser1.setSelected(mTargetMessageRepository.containJob("J001"));
        mBinding.checkBarUser2.setSelected(mTargetMessageRepository.containJob("J002"));
        mBinding.checkBarUser3.setSelected(mTargetMessageRepository.containJob("J003"));
        mBinding.checkBarUser4.setSelected(mTargetMessageRepository.containJob("J004"));
        mBinding.checkBarUser5.setSelected(mTargetMessageRepository.containJob("J005"));
        mBinding.checkBarUser6.setSelected(mTargetMessageRepository.containJob("J999"));

        mBinding.layoutAddressContainer.removeAllViews();
        mAddressHistory.clear();

        if(mTargetMessageRepository.getAddressMap().size() > 0) {
            Iterator<String> keys = mTargetMessageRepository.getAddressMap().keySet().iterator();
            while( keys.hasNext() ){
                String key = keys.next();

                mAddressHistory.put(key, "");

                AddressListItemView itemView = new AddressListItemView(MessageTargetActivity.this);
                itemView.setData(key);
                itemView.setOnDeleteListener(mAddressDeleteListener);

                if(mBinding.layoutAddressContainer.getChildCount() == 0) {
                    mBinding.layoutAddressContainer.addView(itemView);
                } else {
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.topMargin = (int) getResources().getDimensionPixelOffset(R.dimen.height_25);

                    mBinding.layoutAddressContainer.addView(itemView, layoutParams);
                }
            }
        }


    }


    private void siDataRequest() {
        mRequestAddressApi.execute("si");
    }

    private void guDataRequest() {
        mRequestAddressApi.execute("gu", mSelectSi);
    }


    private void bottomWheelSiPopup() {
        if(mAddressSiList.size() > 0) {
            String[] array = new String[mAddressSiList.size()];
            for(int i = 0 ; i < mAddressSiList.size() ; i++) {
                array[i] = mAddressSiList.get(i);
            }
            showBottomWheelPopup(array, 0, new PopupListenerFactory.BaseInputWheelListener() {
                @Override
                public void onClick(int state, int num) {
                    if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {

                        mSelectGu = "";
                        mBinding.tvGu.setText("");

                        mSelectSi = mAddressSiList.get(num);
                        mBinding.tvSi.setText(mAddressSiList.get(num));

                        mRequestType = RequestType.REQUEST_GU;
                        guDataRequest();

                    }

                }

                @Override
                public void onClick(int state, int num, int position) {

                }
            });
        }
    }


    private void bottomWheelGuPopup() {
        if(mAddressGuList.size() > 0) {
            String[] array = new String[mAddressGuList.size()];
            for(int i = 0 ; i < mAddressGuList.size() ; i++) {
                array[i] = mAddressGuList.get(i);
            }
            showBottomWheelPopup(array, 0, new PopupListenerFactory.BaseInputWheelListener() {
                @Override
                public void onClick(int state, int num) {
                    if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                        mSelectGu = mAddressGuList.get(num);
                        mBinding.tvGu.setText(mAddressGuList.get(num));


                        AddressListItemView itemView = new AddressListItemView(MessageTargetActivity.this);
                        itemView.setData(mSelectSi + " " + mSelectGu);
                        itemView.setOnDeleteListener(mAddressDeleteListener);

                        if(!mAddressHistory.containsKey(mSelectSi + " " + mSelectGu)) {
                            if(mBinding.layoutAddressContainer.getChildCount() == 0) {
                                mBinding.layoutAddressContainer.addView(itemView);
                            } else {
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                layoutParams.topMargin = (int) getResources().getDimensionPixelOffset(R.dimen.height_25);

                                mBinding.layoutAddressContainer.addView(itemView, layoutParams);
                            }
                        }

                        mSelectSi = "";
                        mSelectGu = "";

                        mBinding.tvSi.setText("");
                        mBinding.tvGu.setText("");


                    }
                }

                @Override
                public void onClick(int state, int num, int position) {

                }
            });
        }
    }

    private OnCheckBarEventListener mGenderListener = new OnCheckBarEventListener() {
        @Override
        public void onChecked(boolean isCheck, String value, String type) {
            if(isCheck) {
                mTargetMessageRepository.addGender(value, value);
            } else {
                mTargetMessageRepository.removeGender(value);
            }
        }
    };

    private OnCheckBarEventListener mAgeListener = new OnCheckBarEventListener() {
        @Override
        public void onChecked(boolean isCheck, String value, String type) {
            if(isCheck) {
                mTargetMessageRepository.addAge(value, value);
            } else {
                mTargetMessageRepository.removeAge(value);
            }
        }
    };

    private OnCheckBarEventListener mJobListener = new OnCheckBarEventListener() {
        @Override
        public void onChecked(boolean isCheck, String value, String type) {
            if(isCheck) {
                mTargetMessageRepository.addJob(value,value);
            } else {
                mTargetMessageRepository.removeJob(value);
            }
        }
    };

    private void settingData() {

        if(!mBinding.checkBarGenderWomen.isSelect() && !mBinding.checkBarGenderMan.isSelect()) {
            showMessageAlert(getResources().getString(R.string.validation_gender));
            return;
        }

        mTargetMessageRepository.initialize();

        // GENDER
        if(mBinding.checkBarGenderMan.isSelect()) {
            String value = mBinding.checkBarGenderMan.getValue();
            mTargetMessageRepository.addGender(value, value);
        }

        if(mBinding.checkBarGenderWomen.isSelect()) {
            String value = mBinding.checkBarGenderWomen.getValue();
            mTargetMessageRepository.addGender(value, value);
        }


        // AGE
        if(mBinding.checkBar1020.isSelect()) {
            String value = mBinding.checkBar1020.getValue();
            mTargetMessageRepository.addAge(value, value);
        }

        if(mBinding.checkBar2030.isSelect()) {
            String value = mBinding.checkBar2030.getValue();
            mTargetMessageRepository.addAge(value, value);
        }

        if(mBinding.checkBar4050.isSelect()) {
            String value = mBinding.checkBar4050.getValue();
            mTargetMessageRepository.addAge(value, value);
        }

        if(mBinding.checkBar6070.isSelect()) {
            String value = mBinding.checkBar6070.getValue();
            mTargetMessageRepository.addAge(value, value);
        }

        if(mBinding.checkBar80over.isSelect()) {
            String value = mBinding.checkBar80over.getValue();
            mTargetMessageRepository.addAge(value, value);
        }

        // JOB
        if(mBinding.checkBarUser1.isSelect()) {
            String value = mBinding.checkBarUser1.getValue();
            mTargetMessageRepository.addJob(value, value);
        }

        if(mBinding.checkBarUser2.isSelect()) {
            String value = mBinding.checkBarUser2.getValue();
            mTargetMessageRepository.addJob(value, value);
        }

        if(mBinding.checkBarUser3.isSelect()) {
            String value = mBinding.checkBarUser3.getValue();
            mTargetMessageRepository.addJob(value, value);
        }

        if(mBinding.checkBarUser4.isSelect()) {
            String value = mBinding.checkBarUser4.getValue();
            mTargetMessageRepository.addJob(value, value);
        }

        if(mBinding.checkBarUser5.isSelect()) {
            String value = mBinding.checkBarUser5.getValue();
            mTargetMessageRepository.addJob(value, value);
        }
        if(mBinding.checkBarUser6.isSelect()) {
            String value = mBinding.checkBarUser6.getValue();
            mTargetMessageRepository.addJob(value, value);
        }


        int addressCount = mBinding.layoutAddressContainer.getChildCount();
        if(addressCount > 0) {
            for(int i = 0 ; i < addressCount ; i++) {
                AddressListItemView itemView = (AddressListItemView)mBinding.layoutAddressContainer.getChildAt(i);
                String value = itemView.getValue();
                mTargetMessageRepository.addAddress(value, value);
            }
        }

        setResult(RESULT_OK);
        finish();

    }


}
