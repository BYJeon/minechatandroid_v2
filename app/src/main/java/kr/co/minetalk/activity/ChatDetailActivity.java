package kr.co.minetalk.activity;

import android.Manifest;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnKeyboardVisibilityListener;
import kr.co.minetalk.adapter.ChatDetailAdapter;
import kr.co.minetalk.adapter.EmoticonAdapter;
import kr.co.minetalk.adapter.data.EmoticonDataModel;
import kr.co.minetalk.adapter.listener.OnChatMessageListener;
import kr.co.minetalk.adapter.listener.RecyclerItemClickListener;
import kr.co.minetalk.api.FriendInfoApi;
import kr.co.minetalk.api.GoogleTranslateApi;
import kr.co.minetalk.api.MessageImageApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.api.model.FileInfoModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.api.model.TranslateModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.ActivityChattingDetailBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.locker.view.AppLocker;
import kr.co.minetalk.message.api.AlarmThreadApi;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.NewThreadMessageApi;
import kr.co.minetalk.message.api.OutSecurityThreadApi;
import kr.co.minetalk.message.api.OutThreadApi;
import kr.co.minetalk.message.api.ThreadChannelApi;
import kr.co.minetalk.message.manager.MessageDecode;
import kr.co.minetalk.message.manager.MessageThreadManager;
import kr.co.minetalk.message.manager.ThreadController;
import kr.co.minetalk.message.model.MessageData;
import kr.co.minetalk.message.model.SessionData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.message.model.ThreadWithChannel;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.listener.OnProfileListener;
import kr.co.minetalk.ui.popup.SimpleChatWorkPopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.FileUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.RealPathUtil;

public class ChatDetailActivity extends BindActivity<ActivityChattingDetailBinding> implements OnKeyboardVisibilityListener {
    public final int PERMISSION = 1;

    private static final String COLUMN_THREAD_KEY = "column_thread_key";
    private static final String COLUMN_TRREAD_NAME = "column_thread_name";
    private static final String COLUMN_LAST_SEQ = "column_last_seq";
    private static final String COLUMN_MEMBER_COUNT = "column_member_count";
    private static final String COLUMN_SECURITY_MODE = "column_security_mode";

    private static final String EMOTICON_TYPE_MUSTELA = "mustela";
    private static final String EMOTICON_TYPE_PANDAGIRL = "pandagirl";
    private static final String EMOTICON_TYPE_SEAL = "seal";

    private static final int INPUT_MODE_STT = 0;
    private static final int INPUT_MODE_TEXT = 1;

    private static final int MAX_FILE_SIZE = 10485760;

    private SimpleDateFormat mDtFormat = new SimpleDateFormat("yyyy.MM.dd E");

    private String mFileName = "";

    public static void startActivity(Context context, String thread_key) {
        Intent intent = new Intent(context, ChatDetailActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, thread_key);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String thread_key, boolean securityMode) {
        Intent intent = new Intent(context, ChatDetailActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, thread_key);
        intent.putExtra(COLUMN_SECURITY_MODE, securityMode);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String thread_key, String thread_name) {
        Intent intent = new Intent(context, ChatDetailActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, thread_key);
        intent.putExtra(COLUMN_TRREAD_NAME, thread_name);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String thread_key, int thread_count, String thread_name) {
        Intent intent = new Intent(context, ChatDetailActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, thread_key);
        intent.putExtra(COLUMN_TRREAD_NAME, thread_name);
        intent.putExtra(COLUMN_MEMBER_COUNT, thread_count);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }


    public static void startActivity(Context context, String thread_key, int thread_count, String thread_name, boolean securityMode) {
        Intent intent = new Intent(context, ChatDetailActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, thread_key);
        intent.putExtra(COLUMN_TRREAD_NAME, thread_name);
        intent.putExtra(COLUMN_MEMBER_COUNT, thread_count);
        intent.putExtra(COLUMN_SECURITY_MODE, securityMode);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String thread_key, String thread_name, int lastSeq) {
        Intent intent = new Intent(context, ChatDetailActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, thread_key);
        intent.putExtra(COLUMN_TRREAD_NAME, thread_name);
        intent.putExtra(COLUMN_LAST_SEQ, lastSeq);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }

    private String mThreadKey;
    private String mThreadName;
    private boolean isSecurityMode = false;

    private ThreadController threadController;
    private boolean isConnected = false;
    private boolean isLoaded = false;
    private boolean isTransRequest = false;

    private SessionData sessionData;

    private ChatDetailAdapter mAdapter;

    private boolean mIsScrollLock = false;
    private boolean mIsMoveLast = true;
    private int mLastSeq;

    private GoogleTranslateApi mGoogleTranslateApi;
    private MessageImageApi mMessageImageApi;
    private FriendInfoApi mFriendInfoApi;

    private String mCurrentMessageType = MineTalk.MSG_TYPE_TEXT;


    private boolean mIsFinish = false;
    private boolean mIsOptionOpen = false;

    private EmoticonAdapter mEmoticonAdapter;
    private GridLayoutManager mGridLayoutManager;

    private String mImageWidth = "";
    private String mImageHeight = "";
    private String mImagePath = "";

    private String mSelectEmoticonName = "";
    private ArrayList<EmoticonDataModel> mEmoticonResource = new ArrayList<>();

    private String mActivityTitle = "";

    private int mCurrentInputMode = INPUT_MODE_STT;

    //private MessageData mTransmissionData = null;

    private boolean mIsFile = false;

    private int mMemberCount = -1;
    private int mMessageCount = 0;

    //단어 검색 Index 배열
    private ArrayList<Integer> mSearchIndexArr = new ArrayList<>();
    private int mTotalSearchCnt = 0;
    private int mCurSearchIdx = 0;

    private String contactString = "";

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_chatting_detail;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            mThreadKey = getIntent().getExtras().getString(COLUMN_THREAD_KEY);
            mThreadName = getIntent().getExtras().getString(COLUMN_TRREAD_NAME);
            mMemberCount = getIntent().getExtras().getInt(COLUMN_MEMBER_COUNT);
            isSecurityMode = getIntent().getExtras().getBoolean(COLUMN_SECURITY_MODE);

            //mBinding.layoutSecurityStart.setVisibility(isSecurityMode ? View.VISIBLE : View.GONE);
            mBinding.cbChatSecurity.setChecked(Preferences.getChatSecurityEnable(mThreadKey));
            mBinding.cbAutoTrans.setChecked(Preferences.getChatAutoTransEnable(mThreadKey));
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(mThreadKey, 0);

        MineTalk.PUSH_THREAD_KEY = "";
        MineTalk.PUSH_SERVICE_THREAD_KEY = "";
        MineTalk.PUSH_SERVICE_SECURITY_KEY = "";

        String myChattingThreadKey = Preferences.getMyChattingThreadKey();
        if (myChattingThreadKey.equals(mThreadKey)) {
            mThreadName = getString(R.string.popup_profile_button_chat_me);
            mBinding.layoutAutoTrans.setVisibility(View.VISIBLE);
            //mBinding.btnOption.setVisibility(View.INVISIBLE);
        } else {
            //그룹채팅이 아닌 경우만 자동번역/보안 채팅 가능 함.
            if(mMemberCount <= 2)
                mBinding.layoutAutoTrans.setVisibility(View.VISIBLE);
        }

        //보안채팅 타이틀 자물쇠 표시
        mBinding.ivSecurity.setVisibility(isSecurityMode ? View.VISIBLE : View.GONE);
        mBinding.tvActivityTitle.setText(mThreadName);

        sessionData = new SessionData();
        try {
            if (MineTalkApp.getUserInfoModel().getUser_xid() != null)
                sessionData.setXid(MineTalkApp.getUserInfoModel().getUser_xid());
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        if(MineTalkApp.getUserInfoModel().getUser_name() != null)
            sessionData.setNickName(MineTalkApp.getUserInfoModel().getUser_name());
        else
            sessionData.setNickName("");

        sessionData.setToken("token");


        mGridLayoutManager = new GridLayoutManager(this, 4);
        mBinding.recyclerView.setLayoutManager(mGridLayoutManager);
        mEmoticonAdapter = new EmoticonAdapter(this);
        mBinding.recyclerView.setAdapter(mEmoticonAdapter);


        initApi();
        setUIEventListener();
        updateTheme();

        mAdapter = new ChatDetailAdapter(this);
        mAdapter.setEventListener(mChatTranslationListener);


        mBinding.listView.setAdapter(mAdapter);
        mBinding.listView.setVisibility(View.INVISIBLE);
        mBinding.listView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {

                if(!isConnected) return;
                //공유 메세지가 있을 경우 메세지를 전송 한다.
                if(MineTalk.SHARED_TEXT_SELECTED_MESSAGE.length() != 0){
                    sendMessageType(MineTalk.SHARED_TEXT_SELECTED_MESSAGE);
                    MineTalk.SHARED_TEXT_SELECTED_MESSAGE = "";
                    isLoaded = true;
                }else if(MineTalk.SHARED_IMG_URI != null){
                    //String contentPath = CommonUtils.getImageMediaPath(getApplicationContext(), MineTalk.SHARED_IMG_URI);
                    //String contentPath = MineTalk.SHARED_IMG_URI.getPath();
                    //String folder = Environment.getExternalStorageDirectory();

                    try {
                        //공유 이미지 처리
                        ContentResolver cr = getApplicationContext().getContentResolver();
                        InputStream is = cr.openInputStream(MineTalk.SHARED_IMG_URI);
                        Bitmap image = BitmapFactory.decodeStream(is);
                        if (is != null) is.close();

                        String saveTempBmImage = Environment.getExternalStorageDirectory() + "/MineChat/" + "tmp_up_" + System.currentTimeMillis() + ".jpg";
                        if (saveBitmapToFileCache(image, saveTempBmImage)) {
                            mCameraphotonameTemp = saveTempBmImage;

                            mImageWidth = String.valueOf(CommonUtils.getBitmapOfWidth(saveTempBmImage));
                            mImageHeight = String.valueOf(CommonUtils.getBitmapOfHeight(saveTempBmImage));

                            imageUploadRequest(saveTempBmImage);

                            MineTalk.SHARED_IMG_URI = null;
                        }

                    }catch (FileNotFoundException e){
                        e.printStackTrace();
                        MineTalk.SHARED_IMG_URI = null;
                        return;
                    }catch(IOException e){
                        e.printStackTrace();
                        MineTalk.SHARED_IMG_URI = null;
                        return;
                    }
                }


                if(isLoaded&&!isTransRequest){
                    //mAdapter.setData(threadController.getMessages());
                    //mBinding.listView.setSelection(mAdapter.getCount()-1);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //mAdapter.setData(threadController.getMessages());
                            //mAdapter.notifyDataSetChanged();
                            if( mBinding.listView.getVisibility() == View.INVISIBLE ||
                                    mBinding.listView.getLastVisiblePosition() == (mAdapter.getData().size() - 1) ) {
                                mBinding.listView.setSelection(mAdapter.getCount() - 1);
                                mBinding.tvButtonNew.setVisibility(View.GONE);
                            }
                            if(mAdapter.getCount() > 0) {
                                MessageData data = (MessageData) mAdapter.getItem(mAdapter.getCount() - 1);
                                mBinding.tvDate.setText(mDtFormat.format(data.getSendDate()));
                            }
                            mBinding.listView.setVisibility(View.VISIBLE);
                            mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                            isLoaded = false;
                        }
                    }, 300);
                }

            }
        });

//        mBinding.listView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch ( event.getAction( ) ) {
//                    case MotionEvent.ACTION_SCROLL:
//                    case MotionEvent.ACTION_MOVE:
//                        Log.e( "SCROLL", "ACTION_SCROLL" );
//                        mIsScrollLock = true;
//                        break;
//                    case MotionEvent.ACTION_DOWN:
//                        Log.e( "SCROLL", "ACTION_DOWN" );
//                        mIsScrollLock = true;
//                        break;
//                    case MotionEvent.ACTION_CANCEL:
//                    case MotionEvent.ACTION_UP:
//                        Log.e( "SCROLL", "SCROLL_STOP" );
//                        mIsScrollLock = false;
//                        break;
//                }
//                return false;
//            }
//        });

        mBinding.listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE : // 스크롤이 정지되어 있는 상태야.
                        mBinding.tvDate.setVisibility(View.GONE);
                        mIsScrollLock = false;
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL : // 스크롤이 터치되어 있을 때 상태고,
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING : // 이건 스크롤이 움직이고 있을때 상태야.
                        mBinding.tvDate.setVisibility(View.VISIBLE);
                        mIsScrollLock = true;
                        break;
                    default: break;
                }

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    // check if we reached the top or bottom of the list
                    View v = mBinding.listView.getChildAt(0);
                    int offset = (v == null) ? 0 : v.getTop();
                    if (offset == 0) {
                        // reached the top:
                        mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                        if(mBinding.listView.getVisibility() == View.VISIBLE && !mIsScrollLock)
                            mBinding.btnScrollBottom.setVisibility(View.VISIBLE);

                        mBinding.tvDate.setVisibility(View.GONE);
                        return;
                    }
                } else if (totalItemCount - visibleItemCount == firstVisibleItem){
                    View v =  mBinding.listView.getChildAt(totalItemCount-1);
                    int offset = (v == null) ? 0 : v.getTop();
                    if (offset == 0) {
                        // reached the bottom:
                        mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                        mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                        mBinding.tvButtonNew.setVisibility(View.GONE);
                        //mIsScrollLock = false;
                        return;
                    }
                }else{
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                    //mIsScrollLock = false;
                }

                MessageData data = (MessageData)mAdapter.getItem(firstVisibleItem);
                Log.d("QWDQOIDJQOD:", mDtFormat.format(data.getSendDate()));
                mBinding.tvDate.setText(mDtFormat.format(data.getSendDate()));
            }
        });

        setEmoticonResource(EMOTICON_TYPE_MUSTELA);


        MessageData data = MessageThreadManager.getInstance().getLastMessage(this, mThreadKey);
        int lastSeq = 0;

        if (data == null) {
            lastSeq = 0;
//            String saveLastSeq = Preferences.getChatLastSeq(mThreadKey);
//            if(saveLastSeq != null && !saveLastSeq.equals("")) {
//                lastSeq = Integer.parseInt(saveLastSeq);
//            }
        } else {
            lastSeq = data.getSeq();
        }

        messageSyncRequest(mThreadKey, lastSeq);

        //자동번역 설정
        //mBinding.cbAutoTrans.setChecked(Preferences.getAutoTransEnalbe());
        mAdapter.setAutoTrans(Preferences.getChatAutoTransEnable(mThreadKey));

        //알림 설정
        alarmRequest(Preferences.getNoticeEnable());

        setKeyboardVisibilityListener(this);

        initBroadCastReceiver();
    }


    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBinding.cbAutoTrans.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.bk_point_color)));
                mBinding.cbChatSecurity.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.bk_point_color)));
            }

            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.layoutAutoTrans.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutBottom.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvMessageSend.setBackgroundColor(getResources().getColor(R.color.bk_point_color));
            mBinding.etInput.setBackgroundResource(R.drawable.bg_round_333d49);
            mBinding.etInput.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.btnSearchConfirm.setBackgroundResource(R.drawable.bg_round_cbd60c);
            mBinding.layoutSearchHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnSearchClose.setImageResource(R.drawable.btn_close_wh);

            //검색 하단 입력 영역
            mBinding.layoutSearchBottomHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.btnChatSearch.setImageResource(R.drawable.icon_search_bk);
            mBinding.btnOption.setImageResource(R.drawable.icon_set_bk);

            mBinding.cbAutoTrans.setTextColor(getResources().getColor(R.color.white_color));
            //mBinding.cbChatSecurity.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutChatListRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));

            mBinding.tvButtonNew.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.tvButtonNew.setTextColor(getResources().getColor(R.color.main_text_color));

            //mBinding.layoutChatListRoot.setBackgroundColor(Color.parseColor(isSecurityMode ? "#fff5ef" : "#151a21"));
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBinding.cbAutoTrans.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.app_point_color)));
                mBinding.cbChatSecurity.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.app_point_color)));
            }

            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutAutoTrans.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutBottom.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvMessageSend.setBackgroundColor(getResources().getColor(R.color.app_point_color));
            mBinding.etInput.setBackgroundResource(R.drawable.bg_round_f2f2f2);
            mBinding.etInput.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnSearchConfirm.setBackgroundResource(R.drawable.bg_round_16c066);
            mBinding.layoutSearchHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnSearchClose.setImageResource(R.drawable.btn_close);

            //검색 하단 입력 영역
            mBinding.layoutSearchBottomHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.btnChatSearch.setImageResource(R.drawable.icon_search);
            mBinding.btnOption.setImageResource(R.drawable.icon_set);

            mBinding.cbAutoTrans.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.cbChatSecurity.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutChatListRoot.setBackgroundColor(Color.parseColor(isSecurityMode ? "#fff5ef" : "#ebf7e3"));

            mBinding.tvButtonNew.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.tvButtonNew.setTextColor(getResources().getColor(R.color.white_color));
        }
    }


    private void initApi() {
        mGoogleTranslateApi = new GoogleTranslateApi(this, new ApiBase.ApiCallBack<TranslateModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChatDetailActivity.this);
            }

            @Override
            public void onSuccess(int request_code, TranslateModel translateModel) {
                ProgressUtil.hideProgress(ChatDetailActivity.this);
                if (translateModel != null) {
                    String code = Preferences.getChatTranslationLanguageCode();
                    final String transJson = makeTranslationMessage(code, translateModel.getText());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            isTransRequest = true;
                            threadController.updateTranslaitonMsg(mThreadKey, mCurrentTransMsgIdx, transJson);
                        }
                    });
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                isTransRequest = false;
                ProgressUtil.hideProgress(ChatDetailActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mMessageImageApi = new MessageImageApi(this, new ApiBase.ApiCallBack<FileInfoModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChatDetailActivity.this);
            }

            @Override
            public void onSuccess(int request_code, FileInfoModel fileInfoModel) {
                ProgressUtil.hideProgress(ChatDetailActivity.this);
                if (fileInfoModel != null) {
                    String langCode = Preferences.getMyChatLanguage();

                    if (mImageWidth.equals("") || mImageHeight.equals("")) {
                        String jsonMessage = "";
                        if (mIsFile) {
                            jsonMessage = makeSendMessage(getResources().getString(R.string.msg_type_file), langCode, MineTalk.MSG_TYPE_FILE, mFileName, fileInfoModel.getUrl(), "", "", "", "");
                        } else {
                            jsonMessage = makeSendMessage(getResources().getString(R.string.msg_type_video), langCode, MineTalk.MSG_TYPE_VIDEO, "", fileInfoModel.getUrl(), "", "", "", "");
                        }

                        threadController.sendMessage(jsonMessage, 1, MineTalkApp.getUserInfoModel().getUser_name());
                        mBinding.etInput.setText("");
                        mIsMoveLast = true;
                    } else {
                        String jsonMessage = makeSendMessage(getResources().getString(R.string.msg_type_image), langCode, MineTalk.MSG_TYPE_IMAGE, mSelectEmoticonName, fileInfoModel.getUrl(), mImageWidth, mImageHeight, "", "");

                        threadController.sendMessage(jsonMessage, 1, MineTalkApp.getUserInfoModel().getUser_name());
                        mBinding.etInput.setText("");
                        mIsMoveLast = true;
                    }


                    mBinding.layoutEmoticonViewer.setVisibility(View.GONE);
                    mBinding.layoutEmoticonContainer.setVisibility(View.GONE);
                    mSelectEmoticonName = "";

                    mImageWidth = "";
                    mImageHeight = "";
                    mImagePath = "";
                }


                if (!mCameraphotonameTemp.equals("")) {
                    try {
                        File tempImageFile = new File(mCameraphotonameTemp);
                        tempImageFile.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mCameraphotonameTemp = "";
                }

                mIsFile = false;

                MineTalk.SHARED_IMG_URI = null;

            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(ChatDetailActivity.this);
                showMessageAlert(message);

                mIsFile = false;

                MineTalk.SHARED_IMG_URI = null;
            }

            @Override
            public void onCancellation() {

            }
        });

        // 친구 정보 불러오기 api
        mFriendInfoApi = new FriendInfoApi(this, new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChatDetailActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(ChatDetailActivity.this);
                if (userInfoBaseModel != null) {
                    setFriendPopup(userInfoBaseModel);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(ChatDetailActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void messageSyncRequest(String threadKey, int lastSeq) {
        ProgressUtil.showProgress(this);
        NewThreadMessageApi api = new NewThreadMessageApi(this, threadKey, lastSeq);
        api.request(new BaseHttpRequest.APICallbackListener<List<MessageData>>() {
            @Override
            public void onSuccess(List<MessageData> res) {

                if (!res.isEmpty()) {
                    if (res != null && res.size() > 0) {
                        MessageThreadManager.getInstance().appendMessages(ChatDetailActivity.this, threadKey, res);
                    }
                } else {
                    Log.e("@@@TAG", "res empty");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatDetailActivity.this);
                    }
                });
                threadStart(mThreadKey);
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatDetailActivity.this);
                        Log.e("@@@JEROME", "SYNC FAIL!!!");
                    }
                });

                threadStart(mThreadKey);
            }
        });
    }

    private void setBackgroundInfo() {
        String type = Preferences.getChatBackgroundType();
        if (type.equals(MineTalk.BACKGROUND_TYPE_COLOR)) {
            //String color = Preferences.getChatBackgroundColor();

            /// To-Do 추후 변경
            String color = MineTalk.isBlackTheme ? "151a21" : "F2F2F2";
            mBinding.ivBackgroundImage.setVisibility(View.GONE);
            mBinding.layoutChatRoot.setBackgroundColor(Color.parseColor("#" + color));
        } else {
            mBinding.ivBackgroundImage.setVisibility(View.VISIBLE);

            String filePath = Preferences.getChatBackgroundImage();
            if (filePath != null && !filePath.equals("")) {
                File imgFile = new File(filePath);
                if (imgFile.exists()) {

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 4;

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                    Bitmap resizeBitmap = CommonUtils.resizeBitmapImageFn(myBitmap, 1080);

                    ExifInterface exif = null;
                    try {
                        exif = new ExifInterface(imgFile.getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_UNDEFINED);

                    Bitmap remakeBitmap = CommonUtils.rotateBitmap(resizeBitmap, orientation);

                    mBinding.ivBackgroundImage.setImageBitmap(remakeBitmap);
                } else {
                    mBinding.ivBackgroundImage.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBackgroundInfo();

        mBinding.cbAutoTrans.setChecked(Preferences.getChatAutoTransEnable(mThreadKey));
        mAdapter.setAutoTrans(mBinding.cbAutoTrans.isChecked());

        String chatName = Preferences.getChattingName(mThreadKey);
        if (!chatName.equals("")) {
            mBinding.tvActivityTitle.setText(chatName);
        }

        mActivityTitle = mBinding.tvActivityTitle.getText().toString();

        reConnectChannel();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mIsFinish = true;
        if (threadController != null) {
            threadController.stop();
        }
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                mIsFinish = true;
                if (threadController != null && threadController.isConnected()) {
                    threadController.stop();
                }
                finish();

            }
        });

        mBinding.btnOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatOptionActivity.startActivity(ChatDetailActivity.this, mThreadKey, mBinding.tvActivityTitle.getText().toString(), isSecurityMode, mMemberCount, MineTalk.REQUEST_CODE_CHAT_OPTION);
            }
        });

        mBinding.layoutButtonSendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCurrentInputMode == INPUT_MODE_TEXT) {
                    sendMessageType(mBinding.etInput.getText().toString());
                } else {
                    callSTTPopup();

                }


            }
        });

        mBinding.ivOptionAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsOptionOpen = !mIsOptionOpen;
                initEmoticon();

                if (mIsOptionOpen) {
                    mBinding.ivOptionAdd.setImageResource(R.drawable.btn_add_option);
                    mBinding.layoutOption.setVisibility(View.GONE);
                    mBinding.layoutEmoticonViewer.setVisibility(View.GONE);
                    mBinding.layoutEmoticonContainer.setVisibility(View.GONE);
                    mBinding.ivEmoji.setImageResource(R.drawable.icon_emoji_off);
                } else {
                    mBinding.ivOptionAdd.setImageResource(R.drawable.btn_option_off);
                    mBinding.layoutOption.setVisibility(View.VISIBLE);
                }
            }
        });


        mBinding.layoutButtonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initEmoticon();

                if (Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(ChatDetailActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(ChatDetailActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ChatDetailActivity.this, new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION);
                    } else {
//                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(cameraIntent,MineTalk.REQUEST_CODE_CAMERA);

                        requestTakePhotoRequest(MineTalk.REQUEST_CODE_CAMERA);
                    }
                }
            }
        });

        mBinding.layoutButtonPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //AppLocker.getInstance().enableChatDetailAppLock(false);

                initEmoticon();


                if (Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(ChatDetailActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(ChatDetailActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ChatDetailActivity.this, new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION);
                    } else {
                        //AppLocker.getInstance().enableChatDetailAppLock(false);

                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, null);
                        galleryIntent.setType("image/*");
                        galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
                        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
                        chooser.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.image_select));

                        MineTalkApp.mIsOutOfApp = true;
                        startActivityForResult(chooser, MineTalk.REQUEST_CODE_PICTURE);
                    }
                }


            }
        });


        mBinding.layoutButtonFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(ChatDetailActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ChatDetailActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION);
                    } else {
                        //AppLocker.getInstance().enableChatDetailAppLock(false);

                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        String[] mimeTypes =
                                {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                                        "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                                        "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                                        "text/plain",
                                        "application/pdf",
                                        "application/zip"};

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
                            if (mimeTypes.length > 0) {
                                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                            }
                        } else {
                            String mimeTypesStr = "";
                            for (String mimeType : mimeTypes) {
                                mimeTypesStr += mimeType + "|";
                            }
                            intent.setType(mimeTypesStr.substring(0,mimeTypesStr.length() - 1));
                        }


                        //intent.setType("image/*|video/*|audio/*|");
                        MineTalkApp.mIsOutOfApp = true;
                        startActivityForResult(intent, MineTalk.REQUEST_CODE_FILE);
                    }
                }
            }
        });

        mBinding.layoutButtonMov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(ChatDetailActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(ChatDetailActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ChatDetailActivity.this, new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION);
                    } else {
                        //AppLocker.getInstance().enableChatDetailAppLock(true);

                        Intent moveIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        moveIntent.setType("video/*");
                        MineTalkApp.mIsOutOfApp = true;
                        startActivityForResult(moveIntent, MineTalk.REQUEST_CODE_MOV);
                    }
                }

            }
        });

        mBinding.layoutButtonEmoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBinding.layoutEmoticonContainer.getVisibility() == View.VISIBLE) {
                    mBinding.layoutEmoticonContainer.setVisibility(View.GONE);
                    mBinding.layoutEmoticonViewer.setVisibility(View.GONE);

                    mSelectEmoticonName = "";

                    Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.icon_emoji_off).into(mBinding.ivEmoji);
                } else {
                    mBinding.layoutEmoticonContainer.setVisibility(View.VISIBLE);
                    mSelectEmoticonName = "";
                    initEmoticon();

                    Glide.with(MineTalkApp.getCurrentActivity()).load(MineTalk.isBlackTheme ? R.drawable.icon_emoji_on_bk : R.drawable.icon_emoji_on).into(mBinding.ivEmoji);
                }
            }
        });

        mBinding.layoutButtonEmoticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBinding.layoutEmoticonContainer.getVisibility() == View.VISIBLE) {
                    mBinding.layoutEmoticonContainer.setVisibility(View.GONE);
                    mBinding.layoutEmoticonViewer.setVisibility(View.GONE);

                    mSelectEmoticonName = "";
                } else {
                    mBinding.layoutEmoticonContainer.setVisibility(View.VISIBLE);
                    mSelectEmoticonName = "";
                    initEmoticon();
                }
            }
        });

        mBinding.layoutButtonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //AppLocker.getInstance().enableChatDetailAppLock(false);
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setData(ContactsContract.CommonDataKinds.Phone.CONTENT_URI);

                MineTalkApp.mIsOutOfApp = true;
                startActivityForResult(intent, MineTalk.REQUEST_CODE_CONTACT);
            }
        });

        mBinding.recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, mBinding.recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                mBinding.layoutEmoticonViewer.setVisibility(View.VISIBLE);
                mSelectEmoticonName = mEmoticonResource.get(position).getEmoticonName();
                Glide.with(ChatDetailActivity.this).asGif().load(mEmoticonResource.get(position).getEmoticonRes()).into(mBinding.ivEmoticonViewer);

                mCurrentInputMode = INPUT_MODE_TEXT;
                mBinding.tvMessageSend.setVisibility(View.VISIBLE);
                mBinding.ivStt.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        mBinding.ivBtnMustela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEmoticonResource(EMOTICON_TYPE_MUSTELA);
            }
        });

        mBinding.ivBtnPandagirl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEmoticonResource(EMOTICON_TYPE_PANDAGIRL);
            }
        });

        mBinding.ivBtnSeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEmoticonResource(EMOTICON_TYPE_SEAL);
            }
        });


        mBinding.ivEmoticonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.layoutEmoticonViewer.setVisibility(View.GONE);
                mSelectEmoticonName = "";


                String input = mBinding.etInput.getText().toString();
                if (input == null || input.equals("")) {
                    mCurrentInputMode = INPUT_MODE_STT;
                    mBinding.tvMessageSend.setVisibility(View.INVISIBLE);
                    mBinding.ivStt.setVisibility(View.VISIBLE);
                } else {
                    mCurrentInputMode = INPUT_MODE_TEXT;
                    mBinding.tvMessageSend.setVisibility(View.VISIBLE);
                    mBinding.ivStt.setVisibility(View.INVISIBLE);
                }


            }
        });

        //자동 번역 여부
        mBinding.cbAutoTrans.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!buttonView.isPressed()) { return; }

                if(MineTalk.isBlackTheme)
                    buttonView.setTextColor(isChecked ? ContextCompat.getColor(getApplicationContext(), R.color.bk_point_color) : ContextCompat.getColor(getApplicationContext(), R.color.tab_menu_text_color_nor));
                else
                    buttonView.setTextColor(isChecked ? ContextCompat.getColor(getApplicationContext(), R.color.app_point_color) : ContextCompat.getColor(getApplicationContext(), R.color.tab_menu_text_color_nor));
                mAdapter.setAutoTrans(isChecked);

                Preferences.setChatAutoTransEnable(mThreadKey, isChecked);

                //mAdapter.notifyDataSetChanged();
            }
        });

        mBinding.cbChatSecurity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!buttonView.isPressed()) { return; }
                Preferences.setChatSecurityEnable(mThreadKey, isChecked);
                buttonView.setTextColor(isChecked ? ContextCompat.getColor(getApplicationContext(), R.color.app_point_color) : ContextCompat.getColor(getApplicationContext(), R.color.tab_menu_text_color_nor));
            }
        });
        mBinding.etInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String inputMsg = s.toString();
                if (inputMsg != null && !inputMsg.equals("")) {
                    mCurrentInputMode = INPUT_MODE_TEXT;
                    mBinding.tvMessageSend.setVisibility(View.VISIBLE);
                    mBinding.ivStt.setVisibility(View.INVISIBLE);
                } else {
                    mCurrentInputMode = INPUT_MODE_STT;
                    mBinding.tvMessageSend.setVisibility(View.INVISIBLE);
                    mBinding.ivStt.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.etInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    isTransRequest = false;
                    isLoaded = true;
                }
                return false;
            }
        });

        //채팅 검색
        mBinding.btnChatSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.layoutSearchHeader.setVisibility(View.VISIBLE);
                mBinding.layoutBottom.setVisibility(View.GONE);
                mBinding.layoutSearchBottom.setVisibility(View.VISIBLE);

                mBinding.layoutOption.setVisibility(View.GONE);
                mBinding.layoutEmoticonViewer.setVisibility(View.GONE);
                mBinding.layoutEmoticonContainer.setVisibility(View.GONE);
                
                mBinding.etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.etSearch.setFocusableInTouchMode(true);
                        mBinding.etSearch.requestFocus();
                        CommonUtils.showKeypad(ChatDetailActivity.this, mBinding.etSearch);
                    }
                });
            }
        });

        //채팅 검색 닫기
        mBinding.btnSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.layoutSearchHeader.setVisibility(View.GONE);
                mBinding.layoutBottom.setVisibility(View.VISIBLE);
                mBinding.layoutSearchBottom.setVisibility(View.GONE);

                //검색 조건 초기화
                mBinding.etSearch.setText("");
                mSearchIndexArr.clear();
                mCurSearchIdx = 0;

                mBinding.etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.etSearch.setFocusableInTouchMode(false);
                        mBinding.etSearch.clearFocus();
                        CommonUtils.hideKeypad(ChatDetailActivity.this, mBinding.etSearch);
                    }
                });
            }
        });

        //채팅 검색 확인
        mBinding.btnSearchConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchIndexArr.clear();

                String searchText = mBinding.etSearch.getText().toString().trim().toLowerCase();
                ArrayList<MessageData> messages = mAdapter.getData();

                int nCnt = 0;
                for(MessageData item : messages){
                    MessageJSonModel msgModel = new MessageJSonModel();
                    try {
                        JSONObject msgObject = new JSONObject(item.getMessage());
                        msgModel = MessageJSonModel.parse(msgObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String messge = StringEscapeUtils.unescapeJava(msgModel.getText());
                    if(messge.toLowerCase().contains(searchText)){
                        mSearchIndexArr.add(nCnt);
                    }

                    nCnt++;
                }

                if(mSearchIndexArr.size() > 0){
                    //검색 된 배열 수
                    mTotalSearchCnt = mSearchIndexArr.size();
                    mCurSearchIdx = mTotalSearchCnt-1;
                    mBinding.listView.setSelection(mSearchIndexArr.get(mCurSearchIdx) - 1);
                }
            }
        });

        //채팅 상단 검색 이벤트
        mBinding.btnSearchTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTotalSearchCnt == 0) return;
                mCurSearchIdx = (mCurSearchIdx+mTotalSearchCnt-1) % mTotalSearchCnt;
                int idx = mSearchIndexArr.get(mCurSearchIdx);
                if(idx > 0)
                    mBinding.listView.setSelection(idx-1);
            }
        });

        //채팅 아래 검색 이벤트
        mBinding.btnSearchBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTotalSearchCnt == 0) return;
                mCurSearchIdx = (mCurSearchIdx+mTotalSearchCnt+1) % mTotalSearchCnt;
                int idx = mSearchIndexArr.get(mCurSearchIdx);
                if(idx > 0)
                    mBinding.listView.setSelection(idx-1);
            }
        });

        //상단 이동
        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.listView.setSelection(0);
            }
        });

        //하단 이동
        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.listView.setSelection(mAdapter.getCount()-1);
            }
        });

        //새로운 메시지
        mBinding.tvButtonNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.listView.setSelection(mAdapter.getCount()-1);
            }
        });
    }


    private void sendMessageType(final String message){
        String text = message;

        text = StringEscapeUtils.escapeJava(text);


        if (text.trim().equals("") && mSelectEmoticonName.equals("")) return;

        String langCode = Preferences.getMyChatLanguage();
        String jsonMessage = makeSendMessage(mSelectEmoticonName.equals("") ? text : getResources().getString(R.string.msg_type_emoticon), langCode, MineTalk.MSG_TYPE_TEXT, mSelectEmoticonName, "", "", "", "", "");

        threadController.sendMessage(jsonMessage, 1, MineTalkApp.getUserInfoModel().getUser_name());
        mBinding.etInput.setText("");
        mIsMoveLast = true;


        mBinding.layoutEmoticonViewer.setVisibility(View.GONE);
        mBinding.layoutEmoticonContainer.setVisibility(View.GONE);
        mSelectEmoticonName = "";

        Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.icon_emoji_off).into(mBinding.ivEmoji);
    }

    private void callSTTPopup() {


        String langCode = Preferences.getMyChatLanguage();
        CountryInfoModel countryInfoModel = CountryRepository.getInstance().getCountryModel(langCode);
        String voiceLanguageCode = countryInfoModel.getVoice_code();

        Log.e("@@@TAG", "voice : " + voiceLanguageCode);


        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getPackageName());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, voiceLanguageCode);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.stt_promep1));

        try {
            MineTalkApp.mIsOutOfApp = true;
            startActivityForResult(intent, MineTalk.REQUEST_CODE_STT);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, getString(R.string.stt_not_subport), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }


    // 방에서 뒤로가기로 이탈 했을때 했을때
    private void outThreadRequest(String xid, String threadKey) {
        ProgressUtil.showProgress(this);
        OutThreadApi outThreadApi = new OutThreadApi(this, xid, threadKey);
        outThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatDetailActivity.this);

                        mIsFinish = true;
                        if (threadController != null) {
                            threadController.stop();
                        }
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
            }
        });
    }

    // 보안채팅방 나가기
    private void outSecurityThreadRequest(String xid, String threadKey) {
        ProgressUtil.showProgress(this);
        OutSecurityThreadApi outThreadApi = new OutSecurityThreadApi(this, xid, threadKey);
        outThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatDetailActivity.this);

                        mIsFinish = true;
                        if (threadController != null) {
                            threadController.stop();
                        }
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
            }
        });
    }

    private void alarmRequest(boolean enable) {
        ProgressUtil.showProgress(this);

        String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
        AlarmThreadApi alarmThreadApi = new AlarmThreadApi(this, userXid, mThreadKey, enable);
        alarmThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                ProgressUtil.hideProgress(ChatDetailActivity.this);
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                ProgressUtil.hideProgress(ChatDetailActivity.this);
                Log.e("@@@TAG", error.getMessage());
            }
        });
    }

    private void threadStart(String joinThreadKey) {

        ThreadChannelApi threadApi = new ThreadChannelApi(this, joinThreadKey);
        threadApi.request(new BaseHttpRequest.APICallbackListener<ThreadWithChannel>() {
            @Override
            public void onSuccess(ThreadWithChannel item) {

                String activityTitle = mBinding.tvActivityTitle.getText().toString();

                if (activityTitle == null || activityTitle.equals("")) {
                    HashMap<String, String> contactMap = new HashMap<>();
                    contactMap.clear();
                    contactMap = FriendManager.getInstance().selectFriendXidToNameMap();

                    if (item.getMembers().size() > 2) {
                        if (item.getMembers().size() > 0) {
                            String members = "";
                            int count = 0;
                            for (ThreadMember tmember : item.getMembers()) {
                                String contactName = contactMap.get(tmember.getXid());
                                if (contactName == null || contactName.equals("")) {
                                    members = members + tmember.getNickName();
                                } else {
                                    members = members + contactName;
                                }

                                if (count != (item.getMembers().size() - 1)) {
                                    members = members + ",";
                                }
                                count++;
                            }

                            mBinding.tvActivityTitle.setText(members);
                        }
                    } else {
                        String members = "";
                        for (ThreadMember tmember : item.getMembers()) {
                            if (!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                String contactName = contactMap.get(tmember.getXid());
                                if (contactName == null || contactName.equals("")) {
                                    members = tmember.getNickName();
                                } else {
                                    members = contactName;
                                }
                                break;
                            }
                        }
                        mBinding.tvActivityTitle.setText(members);
                    }
                }

                threadController = new ThreadController(ChatDetailActivity.this, sessionData, item);
                mAdapter.setThreadController(threadController);
                threadController.setThreadEventListener(threadEventListner);
                threadController.setMessageDecode(newDecode);

                threadController.connect();
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {

            }
        });
    }

    private MessageDecode newDecode = new MessageDecode() {
        @Override
        public String getPreviewText(String message, int category) {
            return message;
        }
    };

    private final ThreadController.ThreadEventListner threadEventListner = new ThreadController.ThreadEventListner() {
        @Override
        public void onConnectResult(boolean connected) {
            if (connected) isConnected = true;


            if (!connected) {
                reConnectChannel();
            }else{
                //보낼 연락처가 있을 경우 쓰레드가 연결 된 후 보낸다.
                if (!contactString.equals("")) {
                    threadController.sendMessage(contactString, 1, MineTalkApp.getUserInfoModel().getUser_name());
                    contactString = "";
                }
            }

            //전달 메세지가 있을 경
            if (MineTalk.mTransmissionData != null) {
                if (mThreadKey != null && !mThreadKey.equals("")) {
                    try {
                        JSONObject messageObject = new JSONObject(MineTalk.mTransmissionData.getMessage());
                        MessageJSonModel item = new MessageJSonModel();
                        item = MessageJSonModel.parse(messageObject);

                        String jsonMessage = makeSendMessage(item.getText(), item.getLanguageCode(), item.getMsgType(), item.getExtra(),
                                item.getFileUrl(), item.getWidth(), item.getHeight(), item.getContactName(), item.getContactHp());

                        threadController.sendMessage(jsonMessage, 1, MineTalkApp.getUserInfoModel().getUser_name(), mThreadKey);
                        MineTalk.mTransmissionData = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                        MineTalk.mTransmissionData = null;
                    }
                }
            }
        }

        @Override
        public void onConnected() {
            isConnected = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    Toast.makeText(ChatDetailActivity.this, "CONNECT TCP/IP", Toast.LENGTH_SHORT).show();
                }
            });

            System.out.print("CONNECT TCP/IP");
        }

        @Override
        public void onDisconnected() {
            System.out.print("DISCONNECT TCP/IP");
//            reConnectChannel();
        }

        @Override
        public void onErrorOccur(Exception exception) {
            // 서버 연결 에러
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    Toast.makeText(ChatDetailActivity.this, "onErrorOccur", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void refreshChatList() {
            // 메시지 목록 갱신
            // 신규 또는 메시지 목록 변경
            if (!isConnected) return;

            final int size = threadController.getMessageModelsCount();

            if (size > 0) {
                final MessageData msg = threadController.getMessageModel(size - 1);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //보안채팅 시작 여부 Layout
                        mBinding.layoutSecurityStart.setVisibility(View.GONE);
                        if(!isLoaded) isLoaded = true;
                        Log.e("@@@JEROME", "message size : " + threadController.getMessages().size());

                        mAdapter.setData(threadController.getMessages(), isSecurityMode);

                        //새로운 메세지일 경우
                        if(mMessageCount < size || mMessageCount == 0){
                            isLoaded = true;
                            isTransRequest = false;

                            if (mBinding.listView.getVisibility() == View.VISIBLE && mBinding.listView.getLastVisiblePosition() != (mAdapter.getData().size() - 1)){


                                if(!msg.getXid().equals( MineTalkApp.getUserInfoModel().getUser_xid() ))
                                    mBinding.tvButtonNew.setVisibility(View.VISIBLE);
                            }
                            //if(!mIsScrollLock && !isTransRequest)
                                //mBinding.listView.setSelection(mAdapter.getCount()-1);
                        }
                        mMessageCount = size;
                    }
                });
            }else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(isSecurityMode)
                         mBinding.layoutSecurityStart.setVisibility(View.VISIBLE);
                    }
                });

            }
        }

        @Override
        public void showMoreButton(boolean visibility) {
            // More 없음

        }

        @Override
        public void setScrollLastposition(int position) {
            // 메시지 스크롤 변경

        }

        @Override
        public void onAppendMessage(MessageData messageData) {
            if (messageData != null) {
            }

        }
    };

    private void reConnectChannel() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    new Handler().postAtTime(new Runnable() {
                        @Override
                        public void run() {
                            if (threadController != null) {
                                threadController.connect();
                            }
                        }
                    }, 5000);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        mIsFinish = true;
        if (threadController != null) {
            threadController.stop();
        }
        finish();
    }

    private String makeSendMessage(String text, String langCode, String msgType, String extra, String fileUrl, String width, String height, String contactName, String contactHp) {
        try {

            String senderName = MineTalkApp.getUserInfoModel().getUser_name();
            String senderHp = MineTalkApp.getUserInfoModel().getUser_hp();
            String senderProfileUrl = MineTalkApp.getUserInfoModel().getUser_profile_image();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("text", text);
            jsonObject.put("langCode", langCode);
            jsonObject.put("msgType", msgType);
            jsonObject.put("extra", extra);
            jsonObject.put("fileUrl", fileUrl);
            jsonObject.put("width", width);
            jsonObject.put("height", height);
            jsonObject.put("contactName", contactName);
            jsonObject.put("contactHp", contactHp);
            jsonObject.put("senderName", senderName);
            jsonObject.put("senderHp", senderHp);
            jsonObject.put("senderProfileUrl", senderProfileUrl);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return text;
    }

    private String makeSendMessage(String text, String langCode, String msgType, String extra, String fileUrl, String width, String height, String contactName, String contactHp, String deleteYn) {
        try {

            String senderName = MineTalkApp.getUserInfoModel().getUser_name();
            String senderHp = MineTalkApp.getUserInfoModel().getUser_hp();
            //String senderProfileUrl = MineTalkApp.getUserInfoModel().getUser_profile_image();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("text", text);
            jsonObject.put("langCode", langCode);
            jsonObject.put("msgType", msgType);
            jsonObject.put("extra", extra);
            jsonObject.put("fileUrl", fileUrl);
            jsonObject.put("width", "");
            jsonObject.put("height", "");
            jsonObject.put("contactName", contactName);
            jsonObject.put("contactHp", contactHp);
            jsonObject.put("senderName", senderName);
            jsonObject.put("senderHp", senderHp);
            jsonObject.put("senderProfileUrl", fileUrl);
            jsonObject.put("deleteYn", deleteYn);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return text;
    }

    private String makeTranslationMessage(String languageCode, String text) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("languageCode", languageCode);
            jsonObject.put("text", StringEscapeUtils.escapeJava(text));
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    private int mCurrentTransMsgIdx = -1;
    private OnChatMessageListener mChatTranslationListener = new OnChatMessageListener() {
        @Override
        public void onTranslation(MessageData messageData) {
            String transLanguageCode = Preferences.getChatTranslationLanguageCode();
            String myLanguageCode = Preferences.getMyChatLanguage();

            try {
                JSONObject messageObject = new JSONObject(messageData.getMessage());
                MessageJSonModel item = new MessageJSonModel();
                item = MessageJSonModel.parse(messageObject);

                mCurrentTransMsgIdx = messageData.getSeq();
                mGoogleTranslateApi.execute(StringEscapeUtils.unescapeJava(item.getText()), myLanguageCode, transLanguageCode);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onProfile(String userXid) {
            if (FriendManager.getInstance().selectFriendXidToNameMap().containsKey(userXid)) {
                ArrayList<FriendListModel> friendListModels = MineTalkApp.getFriendList();

                FriendListModel selectUser = new FriendListModel();
                for (FriendListModel friendListModel : friendListModels) {
                    if (friendListModel.getUser_xid().equals(userXid)) {
                        selectUser = friendListModel;
                    }
                }

                FriendProfileActivity.startActivity(ChatDetailActivity.this, selectUser, FriendProfileActivity.FriendType.FRIEND);

                //showProfilePopup(BottomProfilePopup.ViewType.NORMAl, selectUser, mProfileListener);
            } else {
                mFriendInfoApi.execute(userXid);
            }
        }


        @Override
        public void onLongPressMsgItem(int type, final MessageData messageData, final boolean isAllDelete) {
            //대화 내용 가공 시 Shared Text 정보는 지운다.
            MineTalk.SHARED_IMAGE_MESSAGE = MineTalk.SHARED_TEXT_SELECTED_MESSAGE = MineTalk.SHARED_MESSAGE = "";
            MineTalk.SHARED_IMG_URI = null;

            Log.e("@@@TAG", "long : " + messageData.getMessage());

            MessageJSonModel msgModel = new MessageJSonModel();

            try {
                JSONObject msgObject = new JSONObject(messageData.getMessage());
                msgModel = MessageJSonModel.parse(msgObject);
            } catch (JSONException e) {
                e.printStackTrace();
                msgModel = null;
            }

            if (msgModel != null) {
                if (msgModel.getDeleteYn().equals("Y")) {
                    return;
                }

                boolean isCopy = true;
                boolean isDelete = true;
                boolean isDeleteAll = isAllDelete;
                boolean isTransmission = true;
                boolean isSave = true;
                boolean isCopyTrans = true;

                // 텍스트 메시지만 복사 가능
                if (msgModel.getMsgType().equals("TEXT") && msgModel.getExtra().equals("")) {
                    isCopy = true;
                } else {
                    isCopy = false;
                }

                if (type == ChatDetailAdapter.MSG_TYPE_ME) {
                    isDelete = true;
                } else if (type == ChatDetailAdapter.MSG_TYPE_OTHER) {
                    isDelete = true;
                }

                if (msgModel.getMsgType().equals("IMAGE")) {
                    isSave = true;
                } else {
                    isSave = false;
                }

                if (messageData.getTran() != null && !messageData.getTran().equals("")) {
                    isCopyTrans = true;
                } else {
                    isCopyTrans = false;
                }

                SimpleChatWorkPopup chatWorkPopup = new SimpleChatWorkPopup();
                chatWorkPopup.setTitle("");
                chatWorkPopup.setSetting(isCopy, isDeleteAll, isDelete, isTransmission, isSave, isCopyTrans);
                chatWorkPopup.setEventListener(new SimpleChatWorkPopup.OnOptionPopupListener() {
                    @Override
                    public void onItemClick(int index) {

                        if (index == 0) {
                            // 복사
                            MessageJSonModel msgJson = new MessageJSonModel();

                            try {
                                JSONObject msgObject = new JSONObject(messageData.getMessage());
                                msgJson = MessageJSonModel.parse(msgObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                msgJson = null;
                            }

                            if (msgJson != null) {
                                ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                ClipData clipData = ClipData.newPlainText("chat_message", StringEscapeUtils.unescapeJava(msgJson.getText()));
                                clipboardManager.setPrimaryClip(clipData);

                                Toast.makeText(ChatDetailActivity.this, getResources().getString(R.string.mission_complete), Toast.LENGTH_SHORT).show();
                            }
                        } else if (index == 1) {
                            // 삭제(내가 보낸 메시지만)
                            MessageJSonModel deleteJson = new MessageJSonModel();
                            try {
                                JSONObject msgObject = new JSONObject(messageData.getMessage());
                                deleteJson = MessageJSonModel.parse(msgObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                deleteJson = null;
                            }

                            if (deleteJson != null) {
                                String updateTextMessage = makeSendMessage(deleteJson.getText(), deleteJson.getLanguageCode(), deleteJson.getMsgType(), deleteJson.getExtra(),
                                        deleteJson.getSenderProfileUrl(), deleteJson.getWidth(), deleteJson.getHeight(), deleteJson.getContactName(), deleteJson.getContactHp(), "Y");
                                MessageThreadManager.getInstance().updateMessageDelete(ChatDetailActivity.this, mThreadKey, messageData.getSeq(), updateTextMessage);


                                String lastDelSeq = Preferences.getLocalDeleteSeq(mThreadKey);
                                if(!lastDelSeq.isEmpty() && !lastDelSeq.equals("")){
                                    int lastSeq = Integer.parseInt(lastDelSeq);
                                    int delSeq  = messageData.getSeq();
                                    if(lastSeq < delSeq)
                                        Preferences.setLocalDeleteSeq(mThreadKey, messageData.getSeq().toString());

                                }else
                                    Preferences.setLocalDeleteSeq(mThreadKey, messageData.getSeq().toString());

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        threadController.reLoadMessageList(ChatDetailActivity.this);
                                    }
                                }, 200);

                            }
                        }else if (index == 5) {
                            // 삭제(모두에게서)
                            MessageJSonModel deleteJson = new MessageJSonModel();
                            try {
                                JSONObject msgObject = new JSONObject(messageData.getMessage());
                                deleteJson = MessageJSonModel.parse(msgObject);
                                threadController.reqDeleteMessage(messageData.getSeq());
                            } catch (JSONException e) {
                                e.printStackTrace();
                                deleteJson = null;
                            }

                            if (deleteJson != null) {
                                String updateTextMessage = makeSendMessage(deleteJson.getText(), deleteJson.getLanguageCode(), deleteJson.getMsgType(), deleteJson.getExtra(),
                                        deleteJson.getFileUrl(), deleteJson.getWidth(), deleteJson.getHeight(), deleteJson.getContactName(), deleteJson.getContactHp(), "Y");
                                MessageThreadManager.getInstance().updateMessageDelete(ChatDetailActivity.this, mThreadKey, messageData.getSeq(), updateTextMessage);

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        threadController.reLoadMessageList(ChatDetailActivity.this);
                                    }
                                }, 200);

                            }
                        } else if (index == 2) {
                            // 전달하기
                            MineTalk.mTransmissionData = messageData;
                            TransmissionActivity.startActivity(ChatDetailActivity.this, MineTalk.REQUEST_CODE_TRANSMISSION);
                        } else if (index == 3) {
                            // 이미지 저장
                            MessageJSonModel msgJson = new MessageJSonModel();

                            try {
                                JSONObject msgObject = new JSONObject(messageData.getMessage());
                                msgJson = MessageJSonModel.parse(msgObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                msgJson = null;
                            }

                            if (msgJson != null) {
                                String url = msgJson.getFileUrl();
                                if (url != null && !url.equals("")) {
                                    new ImageDownload().execute(url);
                                }
                            }
                        } else if (index == 4) {
                            try {
                                JSONObject transJson = new JSONObject(messageData.getTran());

                                String copyMsg = StringEscapeUtils.unescapeJava(transJson.getString("text"));
                                ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                ClipData clipData = ClipData.newPlainText("chat_message", copyMsg);
                                clipboardManager.setPrimaryClip(clipData);

                                Toast.makeText(ChatDetailActivity.this, getResources().getString(R.string.mission_complete), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                chatWorkPopup.show(getSupportFragmentManager(), "popup_chat_work");
            }
        }
    };

    private void imageUploadRequest(String filePath) {
        File file = new File(filePath);

        if(file.exists())
            mMessageImageApi.execute(this, file);
        else
            showMessageAlert(getResources().getString(R.string.upload_failed_msg_file));
    }

    private OnProfileListener mProfileListener = new OnProfileListener() {
        @Override
        public void onTalk(FriendListModel data) {

        }

        @Override
        public void onFavorite(FriendListModel data, String yn) {
            FriendManager.getInstance().updateFavorite(data.getUser_xid(), yn);
            MineTalkApp.setFriendList(FriendManager.getInstance().selectFriend());
        }

        @Override
        public void onCall(FriendListModel data) {

            FriendManager.getInstance().removeFriend(data.getUser_xid());
            MineTalkApp.setFriendList(FriendManager.getInstance().selectFriend());
            userBlockRequest(data.getUser_xid());
        }

        @Override
        public void onGift(FriendListModel data) {
            PointSendActivity.startActivity(ChatDetailActivity.this, PointSendActivity.TYPE_GIFT_POINT, PointSendActivity.MODE_TOKEN, data.getUser_xid(), data.getUser_name(), data.getUser_profile_image());
        }

        @Override
        public void onBlock(FriendListModel data) {

        }

        @Override
        public void onFriendAdd(FriendListModel data) {
            addFriendRequest(data.getUser_xid());
        }

        @Override
        public void onTalkMe(UserInfoBaseModel data) {

        }

        @Override
        public void onEditProfile() {

        }
    };

    private String chooseFileName(String path) {
        try {
            String fileName = path.substring(path.lastIndexOf('/') + 1);

            return fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MineTalkApp.mIsOutOfApp = false;

        if (requestCode == MineTalk.REQUEST_CODE_PICTURE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (uri != null) {
                String contentType = CommonUtils.getMediaType(this, uri);
                String contentPath = CommonUtils.getImageMediaPath(this, uri);

                Hashtable<String, Object> info = FileUtils.getFileInfo(this, uri);
                if (info == null) {
                    Toast.makeText(this, getResources().getString(R.string.image_process_failed_msg), Toast.LENGTH_LONG).show();
                    return;
                }

                final String path = (String) info.get("path");
                int size = 0;

                try {
                    size = (int) info.get("size");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // file max size 10mb
                if (size > MAX_FILE_SIZE) {
                    showMessageAlert(getResources().getString(R.string.error_file_upload_message));
                    return;
                }

                showMessageAlert(getResources().getString(R.string.chat_send_image),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    try {
                                        ExifInterface exif = new ExifInterface(contentPath);
                                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                                        Bitmap originalImage = BitmapFactory.decodeFile(contentPath);
                                        Bitmap rotateImage = CommonUtils.rotateBitmap(originalImage, orientation);

                                        String saveTempBmImage = Environment.getExternalStorageDirectory() + "/MineChat/" + "tmp_up_" + System.currentTimeMillis() + ".jpg";
                                        if (saveBitmapToFileCache(rotateImage, saveTempBmImage)) {
                                            mCameraphotonameTemp = saveTempBmImage;

                                            mImageWidth = String.valueOf(CommonUtils.getBitmapOfWidth(saveTempBmImage));
                                            mImageHeight = String.valueOf(CommonUtils.getBitmapOfHeight(saveTempBmImage));

                                            imageUploadRequest(saveTempBmImage);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();

                                        mImageWidth = String.valueOf(CommonUtils.getBitmapOfWidth(contentPath));
                                        mImageHeight = String.valueOf(CommonUtils.getBitmapOfHeight(contentPath));

                                        imageUploadRequest(contentPath);
                                    }
                                }
                            }
                        });

            }
        } else if (requestCode == MineTalk.REQUEST_CODE_CONTACT && resultCode == RESULT_OK) {
            Cursor cursor = getContentResolver().query(data.getData(),
                    new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                            ContactsContract.CommonDataKinds.Phone.NUMBER}, null, null, null);
            cursor.moveToFirst();
            final String name = cursor.getString(0);        //0은 이름을 얻어옵니다.
            final String number = cursor.getString(1);   //1은 번호를 받아옵니다.
            cursor.close();

            String langCode = Preferences.getMyChatLanguage();
            contactString = makeSendMessage(getResources().getString(R.string.msg_type_contact), langCode, MineTalk.MSG_TYPE_CONTACT, "", "", "", "", name, number);

            //이떄 서버가 연결 되어 있는지 확인 후 보내야 함.
            //threadController.sendMessage(contactString, 1, MineTalkApp.getUserInfoModel().getUser_name());
            mBinding.etInput.setText("");
            mIsMoveLast = true;

            mBinding.layoutEmoticonViewer.setVisibility(View.GONE);
            mBinding.layoutEmoticonContainer.setVisibility(View.GONE);
            mSelectEmoticonName = "";

            mImageWidth = "";
            mImageHeight = "";
            mImagePath = "";


        } else if (requestCode == MineTalk.REQUEST_CODE_MOV && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (uri != null) {
//                String contentType = CommonUtils.getMediaType(this, uri);
//                  String contentPath = CommonUtils.getImageMediaPath(this, uri);

//                String contentType = CommonUtils.getMediaType(this, uri);
//                String contentPath = CommonUtils.getPathFromUri(this,uri);
//
//                Log.e("@@@TAG", "file path : " + contentPath);
//                Log.e("@@@TAG", "file path : " + uri.toString());

                Hashtable<String, Object> info = FileUtils.getFileInfo(this, uri);
                if (info == null) {
                    Toast.makeText(this, getResources().getString(R.string.upload_failed_msg_mov), Toast.LENGTH_LONG).show();
                    return;
                }

                final String path = (String) info.get("path");
                int size = 0;

                try {
                    size = (int) info.get("size");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // file max size 10mb
                if (size < MAX_FILE_SIZE && size > 0) {
                    imageUploadRequest(path);
                } else {
                    showMessageAlert(getResources().getString(R.string.error_file_upload_message));
                }


//
            }
        } else if (requestCode == MineTalk.REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {

            if (data != null && data.getExtras() != null) {
                Bitmap bmImage = (Bitmap) data.getExtras().get("data");
//                if(bmImage != null) {
////                    File image = saveBitmapToFileCache(bmImage, getCacheDir().getPath(), System.currentTimeMillis() + ".jpg");
//
//                    File image = new File(Environment.getExternalStorageDirectory() + "/MineChat/" + mCameraPhotoName);
//
//                    mImageWidth = String.valueOf(CommonUtils.getBitmapOfWidth(image.getPath()));
//                    mImageHeight = String.valueOf(CommonUtils.getBitmapOfHeight(image.getPath()));
//
//
//                    imageUploadRequest(image.getPath());
//                }
            }


            File image = new File(Environment.getExternalStorageDirectory() + "/MineChat/" + mCameraPhotoName);
            Bitmap originalImage = BitmapFactory.decodeFile(image.getPath());

            if (originalImage != null) {
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(image.getPath());
                } catch (Exception e) {
                    e.printStackTrace();

                    mImageWidth = String.valueOf(CommonUtils.getBitmapOfWidth(image.getPath()));
                    mImageHeight = String.valueOf(CommonUtils.getBitmapOfHeight(image.getPath()));

                    imageUploadRequest(image.getPath());
                }
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                Bitmap rotateImage = CommonUtils.rotateBitmap(originalImage, orientation);

                String saveTempBmImage = Environment.getExternalStorageDirectory() + "/MineChat/" + "bm_" + mCameraPhotoName;
                if (saveBitmapToFileCache(rotateImage, saveTempBmImage)) {
                    mImageWidth = String.valueOf(CommonUtils.getBitmapOfWidth(saveTempBmImage));
                    mImageHeight = String.valueOf(CommonUtils.getBitmapOfHeight(saveTempBmImage));

                    mCameraphotonameTemp = saveTempBmImage;

                    imageUploadRequest(saveTempBmImage);

                } else {
                    mImageWidth = String.valueOf(CommonUtils.getBitmapOfWidth(image.getPath()));
                    mImageHeight = String.valueOf(CommonUtils.getBitmapOfHeight(image.getPath()));

                    imageUploadRequest(image.getPath());
                }


            } else {
                mImageWidth = String.valueOf(CommonUtils.getBitmapOfWidth(image.getPath()));
                mImageHeight = String.valueOf(CommonUtils.getBitmapOfHeight(image.getPath()));

                imageUploadRequest(image.getPath());
            }


            Log.e("@@@TAG", "path : " + image.getPath());

        } else if (requestCode == MineTalk.REQUEST_CODE_CHAT_OPTION && resultCode == RESULT_OK) {
            if (data != null && data.getExtras() != null) {
                String resultData = data.getExtras().getString("result");
                if (resultData.equals("exit_out")) {

                    MessageData messageData = MessageThreadManager.getInstance().getLastMessage(this, mThreadKey);
                    int lastSeq = 0;

                    if (data == null) {
                        lastSeq = 0;
                    } else {
                        if (messageData != null) {
                            lastSeq = messageData.getSeq();

                            MessageThreadManager.getInstance().updateMessageData(this, mThreadKey, lastSeq, " ");
                            MessageThreadManager.getInstance().deleteMessage(this, mThreadKey, lastSeq);
                        }
                    }

                    String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
                    outThreadRequest(userXid, mThreadKey);
                } else if (resultData.equals("group_exit")) {

                    MessageThreadManager.getInstance().removeThread(this, mThreadKey);
                    String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
                    outThreadRequest(userXid, mThreadKey);
                } else if (resultData.equals("security_exit_out")) { //보안채팅 나가기

                    MessageData messageData = MessageThreadManager.getInstance().getLastMessage(this, mThreadKey);
                    int lastSeq = 0;

                    if (data == null) {
                        lastSeq = 0;
                    } else {
                        if (messageData != null) {
                            lastSeq = messageData.getSeq();

                            MessageThreadManager.getInstance().updateMessageData(this, mThreadKey, lastSeq, " ");
                            MessageThreadManager.getInstance().deleteMessage(this, mThreadKey, lastSeq);
                        }
                    }

                    String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
                    outSecurityThreadRequest(userXid, mThreadKey);
                }
            }
        } else if (requestCode == MineTalk.REQUEST_CODE_STT && resultCode == RESULT_OK) {
            ArrayList<String> sstResult = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String result_stt = sstResult.get(0);

            mBinding.etInput.setText(result_stt);
        } else if (requestCode == MineTalk.REQUEST_CODE_TRANSMISSION) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.getExtras() != null) {
                    String threadKey = data.getExtras().getString("select_threadKey", "");
                    int memberCnt = data.getExtras().getInt("member_count");
                    String threadName = data.getExtras().getString("select_threadName", "");
                    finish();
                    ChatDetailActivity.startActivity(ChatDetailActivity.this, threadKey, memberCnt, threadName);

//                    if (threadKey != null && !threadKey.equals("")) {
//                        try {
//                            JSONObject messageObject = new JSONObject(MineTalk.mTransmissionData.getMessage());
//                            MessageJSonModel item = new MessageJSonModel();
//                            item = MessageJSonModel.parse(messageObject);
//
//                            String jsonMessage = makeSendMessage(item.getText(), item.getLanguageCode(), item.getMsgType(), item.getExtra(),
//                                    item.getFileUrl(), item.getWidth(), item.getHeight(), item.getContactName(), item.getContactHp());
//
//                            threadController.sendMessage(jsonMessage, 1, MineTalkApp.getUserInfoModel().getUser_name(), threadKey);
//                            MineTalk.mTransmissionData = null;
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            MineTalk.mTransmissionData = null;
//                        }
//                    }
                }
            } else {
                MineTalk.mTransmissionData = null;
            }

        } else if (requestCode == MineTalk.REQUEST_CODE_FILE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (uri != null) {
//                String contentType = CommonUtils.getMediaType(this, uri);
//                String contentPath = CommonUtils.getImageMediaPath(this, uri);
//
//                String contentType = CommonUtils.getMediaType(this, uri);
//                String contentPath = CommonUtils.getPathFromUri(this,uri);
//
//                Log.e("@@@TAG", "file path : " + contentPath);
//                Log.e("@@@TAG", "file path : " + uri.toString());
//
//                String contentPath = CommonUtils.getImageMediaPath(this, uri);
//                int size = (int)new File(contentPath).length();
//
//                // file max size
//                if(size < MAX_FILE_SIZE && size > 0) {
//                    mIsFile = true;
//                    imageUploadRequest(contentPath);
//                } else {
//                    showMessageAlert(getResources().getString(R.string.error_file_upload_message));
//                }

                Hashtable<String, Object> info = FileUtils.getFileInfo(this, uri);
                if (info == null) {
                    Toast.makeText(this, getResources().getString(R.string.upload_failed_msg_file), Toast.LENGTH_LONG).show();
                    return;
                }

                final String path = (String) info.get("path");
                int size = 0;

                try {
                    size = (int) info.get("size");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // file max size
                if (size < MAX_FILE_SIZE && size > 0) {
                    mIsFile = true;
                    mFileName = chooseFileName(path);
                    imageUploadRequest(path);
                } else {
                    mFileName = "";
                    showMessageAlert(getResources().getString(R.string.error_file_upload_message));
                }

            }
        }
    }

    // 실제 경로 찾기
    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    // 파일명 찾기
    private String getName(Uri uri) {
        String[] projection = {MediaStore.Images.ImageColumns.DISPLAY_NAME};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DISPLAY_NAME);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void initBroadCastReceiver() {
        IntentFilter securityChatOutFilter = new IntentFilter();
        securityChatOutFilter.addAction(MineTalk.BROAD_CAST_SECURITY_CHAT_OUT_MESSAGE);
        registerReceiver(mSecurityChattingOutReceiver, securityChatOutFilter);
    }


    private BroadcastReceiver mSecurityChattingOutReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(isSecurityMode && mThreadKey.equals(MineTalk.PUSH_DELETE_THREAD_KEY)) {
                showMessageAlert(getResources().getString(R.string.chat_security_room_exit_message),
                        getResources().getString(R.string.common_ok),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    finish();
                                }
                            }
                        });
            }
        }
    };

    private void initEmoticon() {
        mSelectEmoticonName = "";
        setEmoticonResource(EMOTICON_TYPE_PANDAGIRL);
    }


    private void setEmoticonResource(String type) {
        mEmoticonAdapter.clear();
        mEmoticonResource.clear();

        for (int i = 1; i <= 10; i++) {
            String emoticonName = type + i;
            int emoticonRes = CommonUtils.getResourceImage(this, emoticonName);

            EmoticonDataModel dataModel = new EmoticonDataModel();
            dataModel.setEmoticonName(emoticonName);
            dataModel.setEmoticonRes(emoticonRes);

            mEmoticonResource.add(dataModel);
        }
        mEmoticonAdapter.setData(mEmoticonResource);
    }

    public File saveBitmapToFileCache(Bitmap bitmap, String strFilePath, String filename) {
        File fileCacheItem = new File(strFilePath + filename);
        OutputStream out = null;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fileCacheItem;
    }

    private void setFriendPopup(UserInfoBaseModel userInfo) {

        FriendListModel friendListModel = new FriendListModel();
        friendListModel.setUser_xid(userInfo.getUser_xid());
        friendListModel.setUser_name(userInfo.getUser_name());
        friendListModel.setUser_profile_image(userInfo.getUser_profile_image());
        friendListModel.setUser_state_message(userInfo.getUser_state_message());
        friendListModel.setFavorite_yn("N");

        OnFragmentEventListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnFragmentEventListner();
        if(profileListener != null) {
            profileListener.showProfile(friendListModel, FriendProfileActivity.FriendType.FRIEND_OTHER);
        }
        //showProfilePopup(BottomProfilePopup.ViewType.NEW, friendListModel, mProfileListener);
    }


    private Uri mCameraPhotoUri;
    private String mCameraPhotoName;
    private String mCameraphotonameTemp = "";

    public void requestTakePhotoRequest(int request_code) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); //사진을 찍기 위하여 설정합니다.
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException e) {
            Toast.makeText(this, getResources().getString(R.string.image_process_failed_msg), Toast.LENGTH_SHORT).show();
        }
        if (photoFile != null) {
            //AppLocker.getInstance().enableChatDetailAppLock(false);
            mCameraPhotoUri = FileProvider.getUriForFile(this,
                    "kr.co.minetalk.provider", photoFile); //FileProvider의 경우 이전 포스트를 참고하세요.
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mCameraPhotoUri); //사진을 찍어 해당 Content uri를 photoUri에 적용시키기 위함
            MineTalkApp.mIsOutOfApp = true;
            startActivityForResult(intent, request_code);
        }
    }

    // Android M에서는 Uri.fromFile 함수를 사용하였으나 7.0부터는 이 함수를 사용할 시 FileUriExposedException이
    // 발생하므로 아래와 같이 함수를 작성합니다. 이전 포스트에 참고한 영문 사이트를 들어가시면 자세한 설명을 볼 수 있습니다.
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        String imageFileName = "IP" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/MineChat/"); //test라는 경로에 이미지를 저장하기 위함
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCameraPhotoName = image.getName();

        return image;
    }


    private boolean saveBitmapToFileCache(Bitmap bitmap, String strFilePath) {

        File fileCacheItem = new File(strFilePath);
        OutputStream out = null;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private void setKeyboardVisibilityListener(final OnKeyboardVisibilityListener onKeyboardVisibilityListener) {
        final View parentView = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);
        parentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            private boolean alreadyOpen;
            private final int defaultKeyboardHeightDP = 100;
            private final int EstimatedKeyboardDP = defaultKeyboardHeightDP + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);
            private final Rect rect = new Rect();

            @Override
            public void onGlobalLayout() {
                int estimatedKeyboardHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, parentView.getResources().getDisplayMetrics());
                parentView.getWindowVisibleDisplayFrame(rect);
                int heightDiff = parentView.getRootView().getHeight() - (rect.bottom - rect.top);
                boolean isShown = heightDiff >= estimatedKeyboardHeight;

                if (isShown == alreadyOpen) {
                    Log.i("Keyboard state", "Ignoring global layout change...");
                    return;
                }
                alreadyOpen = isShown;
                onKeyboardVisibilityListener.onVisibilityChanged(isShown);
            }
        });
    }

    @Override
    public void onVisibilityChanged(boolean visible) {
        mBinding.listView.setSelection(mAdapter.getCount()-1);
        //mBinding.listView.smoothScrollToPosition(mAdapter.getCount()-1);
        //Toast.makeText(ChatDetailActivity.this, visible ? "Keyboard is active" : "Keyboard is Inactive", Toast.LENGTH_SHORT).show();
    }


    /**
     * url 이미지 다운로드
     */
    private class ImageDownload extends AsyncTask<String, Void, Void> {

        /**
         * 파일명
         */
        private String fileName;
        /**
         * 저장할 폴더
         */
        private final String SAVE_FOLDER = "/MineChat";


        @Override
        protected Void doInBackground(String... params) {


            //다운로드 경로를 지정
            String savePath = Environment.getExternalStorageDirectory().toString() + SAVE_FOLDER;


            File dir = new File(savePath);

            //상위 디렉토리가 존재하지 않을 경우 생성

            if (!dir.exists()) {

                dir.mkdirs();

            }


            //파일 이름 :날짜_시간
            Date day = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.KOREA);
            fileName = String.valueOf(sdf.format(day));

            //웹 서버 쪽 파일이 있는 경로
            String fileUrl = params[0];


            //다운로드 폴더에 동일한 파일명이 존재하는지 확인
            if (new File(savePath + "/" + fileName).exists() == false) {

            } else {

            }

            String localPath = savePath + "/" + fileName + ".jpg";
            try {

                URL imgUrl = new URL(fileUrl);

                //서버와 접속하는 클라이언트 객체 생성
                HttpURLConnection conn = (HttpURLConnection) imgUrl.openConnection();

                int len = conn.getContentLength();
                byte[] tmpByte = new byte[len];

                //입력 스트림을 구한다
                InputStream is = conn.getInputStream();

                File file = new File(localPath);

                //파일 저장 스트림 생성
                FileOutputStream fos = new FileOutputStream(file);

                int read;

                //입력 스트림을 파일로 저장
                for (; ; ) {
                    read = is.read(tmpByte);
                    if (read <= 0) {
                        break;
                    }
                    fos.write(tmpByte, 0, read); //file 생성
                }

                is.close();
                fos.close();
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //저장한 이미지 열기
//            Intent i = new Intent(Intent.ACTION_VIEW);
//            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            String targetDir = Environment.getExternalStorageDirectory().toString() + SAVE_FOLDER;
            File file = new File(targetDir + "/" + fileName + ".jpg");

            //type 지정 (이미지)

//            i.setDataAndType(Uri.fromFile(file), "image/*");
//            getApplicationContext().startActivity(i);

            //이미지 스캔해서 갤러리 업데이트
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ChatDetailActivity.this, getResources().getString(R.string.qr_save_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


}
