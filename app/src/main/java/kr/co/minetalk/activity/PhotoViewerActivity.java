package kr.co.minetalk.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BaseActivity;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.UrlFileSizeAsyncTask;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by kyd0822 on 2017. 9. 18..
 */

public class PhotoViewerActivity extends BaseActivity {

    public static void startActivity(Context context, String url, String type) {
        Intent intent = new Intent(context, PhotoViewerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("arg_photo_url", url);
        intent.putExtra("arg_type", type);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String url, String name, String status, String type) {
        Intent intent = new Intent(context, PhotoViewerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("arg_photo_url", url);
        intent.putExtra("arg_type", type);
        intent.putExtra("arg_name", name);
        intent.putExtra("arg_status", status);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String url, String type, String chatName, boolean isChatImage) {
        Intent intent = new Intent(context, PhotoViewerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("arg_photo_url", url);
        intent.putExtra("arg_type", type);
        intent.putExtra("arg_chat_name", chatName);
        intent.putExtra("arg_is_chat_image", isChatImage);
        context.startActivity(intent);
    }
    public static void startActivity(Context context, String url, String type, int requestCode) {
        Intent intent = new Intent(context, PhotoViewerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("arg_photo_url", url);
        intent.putExtra("arg_type", type);
        ((Activity)context).startActivityForResult(intent, requestCode);
    }

    private String mName = "";
    private String mStatus = "";

    private String mUrl = "";
    private String mType = "";

    private PhotoView mIvImage = null;

    private RelativeLayout mRlHeaderLayout = null;
    private TextView mTvTitle = null;

    private RelativeLayout mRlInfoLayout = null;
    private TextView mTvFileType = null;
    private TextView mTvFileSize = null;
    private TextView mTvFileResolution = null;

    private LinearLayout mLinearButtonLayout = null;
    private RelativeLayout mProfileButtonLayout = null;

    private ImageView mIvBtnDownload = null;
    private ImageView mIvBtnShare    = null;
    private ImageView mIvBtnInfo     = null;

    private ImageView mBtnBack       = null;
    private TextView  mTvDownload    = null;
    private TextView  mTvName        = null;
    private TextView  mTvStatus      = null;


    private String mTitleName = "";
    private String mFileName = "";
    private String mFileType = "";
    private String mFileSize = "";
    private String mFileResolution = "";

    private boolean isShare = false;
    private UrlFileSizeAsyncTask mFileSizeAsyncTask = null;

    private boolean mIsChatImage = false;


    private boolean isShareEvent = false;
    private boolean isSaveEvent = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent() != null && getIntent().getExtras() != null) {
            mUrl = getIntent().getExtras().getString("arg_photo_url", "");
            mType = getIntent().getExtras().getString("arg_type", "");
            mIsChatImage = getIntent().getExtras().getBoolean("arg_is_chat_image", false);

            mName = getIntent().getExtras().getString("arg_name", "");
            mStatus = getIntent().getExtras().getString("arg_status", "");

            mTitleName = getIntent().getExtras().getString("arg_chat_name", getResources().getString(R.string.profile_image_title));
        }
        chooseInfo(mUrl);


        setContentView(R.layout.activity_photo_viewer);

        initViews();
        setUIEventListener();

        if(!mIsChatImage) {
            mLinearButtonLayout.setVisibility(View.GONE);
            mProfileButtonLayout.setVisibility(View.VISIBLE);
        } else {
            mLinearButtonLayout.setVisibility(View.VISIBLE);
            mProfileButtonLayout.setVisibility(View.GONE);
        }


        setData();
        updateTheme();
    }

    private void chooseInfo(String path) {
        try {
            String fileName = path.substring(path.lastIndexOf('/') + 1);
            mFileName = fileName;

            String[] split = fileName.split("\\.");
            if(split != null && split.length > 1) {
                String type = split[1];
                mFileType = type.toUpperCase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void initViews() {
        mRlHeaderLayout = findViewById(R.id.layout_header);
        mTvTitle = findViewById(R.id.tv_title);

        mBtnBack = findViewById(R.id.btn_back);
        mTvDownload = findViewById(R.id.tv_download);
        mTvName     = findViewById(R.id.tv_name);
        mTvStatus   = findViewById(R.id.tv_status);

        mIvImage = (PhotoView) findViewById(R.id.iv_image);

        mRlInfoLayout = findViewById(R.id.rl_info_layout);
        mTvFileType = (TextView) findViewById(R.id.tv_file_type);
        mTvFileSize = (TextView) findViewById(R.id.tv_file_size);
        mTvFileResolution = (TextView) findViewById(R.id.tv_file_resolution);

        mProfileButtonLayout = findViewById(R.id.ll_profile_layout);
        mLinearButtonLayout = (LinearLayout) findViewById(R.id.ll_button_layout);
        mIvBtnDownload = (ImageView) findViewById(R.id.iv_download);
        mIvBtnShare    = (ImageView) findViewById(R.id.iv_share);
        mIvBtnInfo     = (ImageView) findViewById(R.id.iv_info);

        mFileSizeAsyncTask = new UrlFileSizeAsyncTask(mUrl);

        mTvTitle.setText(mTitleName);
        mTvName.setText(mName);
        mTvStatus.setText(StringEscapeUtils.unescapeJava(mStatus));
    }

    private void setUIEventListener() {
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mTvDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isShare = false;
                isSaveEvent = true;


                String localPath = Environment.getExternalStorageDirectory().toString() + "/MineChat";
                if (new File(localPath + "/" + mFileName).exists() == false) {
                    new ImageDownload().execute(mUrl);
                } else {
                    isSaveEvent = false;
                }
            }
        });

        mIvImage.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
            @Override
            public void onViewTap(View view, float x, float y) {

                if(!mIsChatImage)   return;

                try {
                    int visible = mLinearButtonLayout.getVisibility();
                    if (visible == View.VISIBLE) {
                        mLinearButtonLayout.setVisibility(View.GONE);
                    } else {
                        mLinearButtonLayout.setVisibility(View.VISIBLE);
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });

        mIvBtnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRlInfoLayout.setVisibility(View.GONE);
                isShare = false;

                isSaveEvent = true;


                String localPath = Environment.getExternalStorageDirectory().toString() + "/MineChat";
                if (new File(localPath + "/" + mFileName).exists() == false) {
                    new ImageDownload().execute(mUrl);
                } else {
                    isSaveEvent = false;
                }

            }
        });

        mIvBtnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mRlInfoLayout.setVisibility(View.GONE);
//                String localPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + mFileName;
                String localPath = Environment.getExternalStorageDirectory().toString() + "/MineChat";



                if (new File(localPath + "/" + mFileName).exists()) {
                    File shareFile = new File(localPath);

                    if(shareFile.exists()) {
//                    Uri uri = Uri.fromFile(shareFile);
                        Uri uri = Uri.parse(shareFile.getPath());

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.putExtra(Intent.EXTRA_STREAM, uri);
                        intent.setType("image/*");

                        Intent chooser = Intent.createChooser(intent, getResources().getString(R.string.text_share));
                        startActivity(chooser);
                    } else {
                        isShare = true;
                    }
                } else {
                    isSaveEvent = false;
                    new ImageDownload().execute(mUrl);
                }

            }
        });



        mIvBtnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRlInfoLayout.setVisibility(View.VISIBLE);
                mTvFileType.setText(mFileType);
                mTvFileResolution.setText(mFileResolution);
            }
        });

        mRlInfoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRlInfoLayout.setVisibility(View.GONE);
            }
        });

        mFileSizeAsyncTask.setCallback(new UrlFileSizeAsyncTask.OnFileSizeCallback() {
            @Override
            public void callBackSize(int size) {
                try {
                    if(size != 0) {
                        String imgSize = String.valueOf((size / 1024));
                        mTvFileSize.setText(imgSize + "KB");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(PhotoViewerActivity.this);
                    }
                });
            }
        });
    }


    private void setData() {

        if(!mUrl.equals("")) {
            if (mType != null && mType.toLowerCase().contains("gif")) {
                Glide.with(this).asGif().load(mUrl).into(mIvImage);
            } else {
                Glide.with(this).load(mUrl).into(mIvImage);
            }

            ProgressUtil.showProgress(this);
            mFileSizeAsyncTask.execute("");
        } else {
            Glide.with(this).load(R.drawable.img_basic).into(mIvImage);
        }

    }


    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mFileSizeAsyncTask.cancel(true);
    }



    /**
     * url 이미지 다운로드
     *
     */
    private class ImageDownload extends AsyncTask<String, Void, Void> {

        /**
         * 파일명
         */
        private String fileName;
        /**
         * 저장할 폴더
         */
        private final String SAVE_FOLDER = "/MineChat";



        @Override
        protected Void doInBackground(String... params) {



            //다운로드 경로를 지정
            String savePath = Environment.getExternalStorageDirectory().toString() + SAVE_FOLDER;



            File dir = new File(savePath);

            //상위 디렉토리가 존재하지 않을 경우 생성

            if (!dir.exists()) {

                dir.mkdirs();

            }



            //파일 이름 :날짜_시간
//            Date day = new Date();
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.KOREA);
//            fileName = String.valueOf(sdf.format(day));

            fileName = mFileName;

            //웹 서버 쪽 파일이 있는 경로
            String fileUrl = params[0];



            //다운로드 폴더에 동일한 파일명이 존재하는지 확인
            if (new File(savePath + "/" + fileName).exists() == false) {

            } else {

            }

            String localPath = savePath + "/" + fileName + ".jpg";
            try {

                URL imgUrl = new URL(fileUrl);

                //서버와 접속하는 클라이언트 객체 생성
                HttpURLConnection conn = (HttpURLConnection)imgUrl.openConnection();

                int len = conn.getContentLength();
                byte[] tmpByte = new byte[len];

                //입력 스트림을 구한다
                InputStream is = conn.getInputStream();

                File file = new File(localPath);

                //파일 저장 스트림 생성
                FileOutputStream fos = new FileOutputStream(file);

                int read;

                //입력 스트림을 파일로 저장
                for (;;) {
                    read = is.read(tmpByte);
                    if (read <= 0) {
                        break;
                    }
                    fos.write(tmpByte, 0, read); //file 생성
                }

                is.close();
                fos.close();
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //저장한 이미지 열기
//            Intent i = new Intent(Intent.ACTION_VIEW);
//            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            String targetDir = Environment.getExternalStorageDirectory().toString() + SAVE_FOLDER;
            File file = new File(targetDir + "/" + fileName + ".jpg");

            //type 지정 (이미지)

//            i.setDataAndType(Uri.fromFile(file), "image/*");
//            getApplicationContext().startActivity(i);

            //이미지 스캔해서 갤러리 업데이트
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));

            if(isSaveEvent) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(PhotoViewerActivity.this, getResources().getString(R.string.qr_save_msg), Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
//                File shareFile = new File(localPath);

                if(file.exists()) {
//                    Uri uri = Uri.fromFile(shareFile);
                    Uri uri = Uri.parse(file.getPath());

                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.setType("image/*");

                    Intent chooser = Intent.createChooser(intent, getResources().getString(R.string.text_share));
                    startActivity(chooser);
                }
            }

            isSaveEvent = false;

        }
    }

    public void updateTheme(){
        if(MineTalk.isBlackTheme){
            mRlHeaderLayout.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mTvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBtnBack.setImageResource(R.drawable.btn_close_wh);
        }else{
            mRlHeaderLayout.setBackgroundColor(getResources().getColor(R.color.white_color));
            mTvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBtnBack.setImageResource(R.drawable.btn_close);

        }
    }

}
