package kr.co.minetalk.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Date;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ListRecommendApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.RecommendModel;
import kr.co.minetalk.api.model.RecommendResponse;
import kr.co.minetalk.databinding.ActivityRecommendBinding;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.RecommendListItemView;

public class RecommendActivity extends BindActivity<ActivityRecommendBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, RecommendActivity.class);
        context.startActivity(intent);
    }

    private String mStartDate  = "";
    private String mEndDate    = "";

    private ListRecommendApi mListRecommendApi;

    private boolean mIsOpenFilter = true;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_recommend;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        initApi();
        setUIEventListener();

        initDefaultData();
    }


    private void initApi() {
        mListRecommendApi = new ListRecommendApi(this, new ApiBase.ApiCallBack<RecommendResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RecommendActivity.this);
            }

            @Override
            public void onSuccess(int request_code, RecommendResponse recommendResponse) {
                ProgressUtil.hideProgress(RecommendActivity.this);
                if(recommendResponse != null) {
                    setData(recommendResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RecommendActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void listRecommendRequest(String keyword) {
        mListRecommendApi.execute(keyword);
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if(mIsOpenFilter) {
//                    String keyword = mBinding.etSearch.getText().toString();
//                    //String userPhone = mBinding.etSearch.getText().toString();
//
//                    listRecommendRequest(keyword);
//                } else {
//                    openFilter();
//                }

                String keyword = mBinding.etSearch.getText().toString();
                //String userPhone = mBinding.etSearch.getText().toString();

                listRecommendRequest(keyword);

            }
        });

        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 1){
                    mBinding.btnSearch.setEnabled(true);
                    mBinding.btnSearch.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

//        mBinding.layoutButtonStartDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Calendar cal = Calendar.getInstance();
//                DatePickerDialog dialog = new DatePickerDialog(RecommendActivity.this, new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker datePicker, int year, int month, int date) {
//
//                        String msg = String.format("%d-%02d-%02d", year, month+1, date);
//                        mStartDate = msg;
//                        mBinding.tvStartDate.setText(msg);
//
//                    }
//                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
//
//                dialog.show();
//            }
//        });

//        mBinding.layoutButtonEndDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Calendar cal = Calendar.getInstance();
//                DatePickerDialog dialog = new DatePickerDialog(RecommendActivity.this, new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker datePicker, int year, int month, int date) {
//
//                        String msg = String.format("%d-%02d-%02d", year, month+1, date);
//                        mEndDate = msg;
//                        mBinding.tvEndDate.setText(msg);
//
//                    }
//                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
//
//                dialog.show();
//            }
//        });
    }

    private void initTime() {
        final Calendar cal = Calendar.getInstance();
        mStartDate = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DATE));
        mEndDate   = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DATE));

        //mBinding.tvStartDate.setText(mStartDate);
        //mBinding.tvEndDate.setText(mEndDate);
    }

    private void initDefaultData() {
        initTime();
        openFilter();

        listRecommendRequest("");
    }

    private void openFilter() {
        mIsOpenFilter = true;
        //mBinding.layoutFilter.setVisibility(View.VISIBLE);
        //mBinding.ivArrowDown.setVisibility(View.INVISIBLE);
    }

    private void closeFilter() {
        mIsOpenFilter = false;
        //mBinding.layoutFilter.setVisibility(View.GONE);
        //mBinding.ivArrowDown.setVisibility(View.VISIBLE);
    }

    public void setData(RecommendResponse response) {
        if(response.getData().size() > 0){
            mBinding.etSearch.setText("");
            mBinding.btnSearch.setEnabled(false);
            mBinding.btnSearch.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);
        }else{
            mBinding.btnSearch.setEnabled(true);
            mBinding.btnSearch.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
        }

        mBinding.layoutListContainer.removeAllViews();
        if(response != null) {
            for(int i = 0 ; i < response.getData().size() ; i++) {
                RecommendListItemView itemView = new RecommendListItemView(this);
                itemView.setData(response.getData().get(i));
                mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
            }
        }

        closeFilter();
    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.height_24);

        return params;
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            
            mBinding.btnSearch.setBackgroundResource(R.drawable.button_bg_cdb60c);
        }else{

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.btnSearch.setBackgroundResource(R.drawable.button_bg_16c066);
        }
    }
}
