package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ModifyUserBirthDayApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditUserBirthdayBinding;
import kr.co.minetalk.utils.ProgressUtil;

public class EditUserBirthDayActivity extends BindActivity<ActivityEditUserBirthdayBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditUserBirthDayActivity.class);
        context.startActivity(intent);
    }

    private ModifyUserBirthDayApi mModifyUserBirthDayApi;


    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_user_birthday;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        String defaultValue = MineTalkApp.getUserInfoModel().getUser_birthday();
        mBinding.etBirthDay.setText(defaultValue);
    }

    private void initApi() {
        mModifyUserBirthDayApi = new ModifyUserBirthDayApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditUserBirthDayActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditUserBirthDayActivity.this);
                if(baseModel != null) {
                    userInfoRequest();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditUserBirthDayActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editBirthDayReqeust(mBinding.etBirthDay.getText().toString());
            }
        });
    }

    private void editBirthDayReqeust(String birthDay) {
        if(birthDay == null || birthDay.equals("")) {
            showMessageAlert(getResources().getString(R.string.edit_birthday_vaildation_birth_day));
            return;
        }
        mModifyUserBirthDayApi.execute(birthDay);
    }
}
