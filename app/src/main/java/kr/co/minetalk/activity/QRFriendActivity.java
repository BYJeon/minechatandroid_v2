package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.google.zxing.Result;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.FriendInfoApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityQrFriendBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.ProgressUtil;
import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRFriendActivity extends BindActivity<ActivityQrFriendBinding> implements ZXingScannerView.ResultHandler{
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, QRFriendActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }

    private ZXingScannerView mScannerView;
    private FriendInfoApi mFriendInfoApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_qr_friend;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);


        initApi();
        setUIEventListener();

        mScannerView = new ZXingScannerView(this) {
            @Override
            protected IViewFinder createViewFinderView(Context context) {
                return new QRFriendActivity.CustomViewFinderView(context);
            }
        };
        mBinding.contentFrame.addView(mScannerView);
    }

    private void initApi() {
        mFriendInfoApi = new FriendInfoApi(this, new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(QRFriendActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(QRFriendActivity.this);
                if(userInfoBaseModel != null) {
                    FriendListModel model = new FriendListModel();
                    if(model.getUser_xid().equals("")){ //검색 결과가 없을 시

                        showMessageAlert(getResources().getString(R.string.qr_code_wrong_msg),
                                getResources().getString(R.string.common_ok),
                                new PopupListenerFactory.SimplePopupListener() {
                                    @Override
                                    public void onClick(DialogInterface f, int state) {
                                        if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                            refreshScanCamera();
                                        }
                                    }
                                });

                    }else {
                        model.setUser_name(userInfoBaseModel.getUser_name());
                        model.setUser_hp(userInfoBaseModel.getUser_hp());
                        model.setUser_xid(userInfoBaseModel.getUser_xid());
                        model.setUser_profile_image(userInfoBaseModel.getUser_profile_image());
                        FriendProfileActivity.startActivity(QRFriendActivity.this, model, FriendProfileActivity.FriendType.FRIEND_OTHER);
                        finish();
                    }
                }

            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(QRFriendActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void friendInfoRequest(String friend_xid) {
        mFriendInfoApi.execute(friend_xid);
    }

    private void refreshScanCamera() {
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        String result = rawResult.getText();

        friendInfoRequest(result);

    }

    private static class CustomViewFinderView extends ViewFinderView {
        public static final String TRADE_MARK_TEXT = "";
        public static final int TRADE_MARK_TEXT_SIZE_SP = 40;
        public final Paint PAINT = new Paint();

        public CustomViewFinderView(Context context) {
            super(context);
            init();
        }

        public CustomViewFinderView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        private void init() {
            PAINT.setColor(Color.WHITE);
            PAINT.setAntiAlias(true);
            float textPixelSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                    TRADE_MARK_TEXT_SIZE_SP, getResources().getDisplayMetrics());
            PAINT.setTextSize(textPixelSize);
            setSquareViewFinder(true);
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
//            drawTradeMark(canvas);    // 텍스트 추가 할 경우 사용
        }

        private void drawTradeMark(Canvas canvas) {
            Rect framingRect = getFramingRect();
            float tradeMarkTop;
            float tradeMarkLeft;
            if (framingRect != null) {
                tradeMarkTop = framingRect.bottom + PAINT.getTextSize() + 10;
                tradeMarkLeft = framingRect.left;
            } else {
                tradeMarkTop = 10;
                tradeMarkLeft = canvas.getHeight() - PAINT.getTextSize() - 10;
            }
            canvas.drawText(TRADE_MARK_TEXT, tradeMarkLeft, tradeMarkTop, PAINT);
        }
    }

    @Override
    public void updateTheme() {
        super.updateTheme();
        if(MineTalk.isBlackTheme){
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
        }else{
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }

}
