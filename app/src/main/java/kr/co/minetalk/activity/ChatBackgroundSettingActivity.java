package kr.co.minetalk.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityChatBackgroundSettingBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;

public class ChatBackgroundSettingActivity extends BindActivity<ActivityChatBackgroundSettingBinding> {
    private final int PERMISSON = 1;
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ChatBackgroundSettingActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_chat_background_setting;
    }

    @Override
    protected void initView() {

        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.layoutButtonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(ChatBackgroundSettingActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(ChatBackgroundSettingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(ChatBackgroundSettingActivity.this, new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        onChooseMedia();
                    }
                }
            }
        });

        mBinding.layoutButtonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(ChatBackgroundSettingActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(ChatBackgroundSettingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(ChatBackgroundSettingActivity.this, new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        onChooseMedia();
                    }
                }
            }
        });

        mBinding.layoutButtonColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectBackgroundActivity.startActivity(ChatBackgroundSettingActivity.this);
            }
        });
    }

    private void onChooseMedia() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,null);
        galleryIntent.setType("image/*");
        galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.select_work));

        Intent[] intentArray =  {cameraIntent};
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser,MineTalk.REQUEST_CODE_MEDIA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onChooseMedia();
                } else {
                    return;
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MineTalk.REQUEST_CODE_MEDIA) {
            if(resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                if (uri != null) {
                    String contentType = CommonUtils.getMediaType(this, uri);
                    String contentPath = CommonUtils.getImageMediaPath(this, uri);

                    Preferences.setChatBackgroundType(MineTalk.BACKGROUND_TYPE_IMAGE);
                    Preferences.setChatBackgroundImage(contentPath);
                } else {
                    if(data != null && data.getExtras() != null) {
                        Bitmap bmImage = (Bitmap) data.getExtras().get("data");
                        if(bmImage != null) {
                            File image = saveBitmapToFileCache(bmImage, getFilesDir().getPath(), "chat_background" + ".jpg");

                            Preferences.setChatBackgroundType(MineTalk.BACKGROUND_TYPE_IMAGE);
                            Preferences.setChatBackgroundImage(image.getPath());
                        }
                    }
                }
            }
        }
    }

    public File saveBitmapToFileCache(Bitmap bitmap, String strFilePath, String filename) {
        File fileCacheItem = new File(strFilePath + filename);
        OutputStream out = null;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileCacheItem;
    }


}
