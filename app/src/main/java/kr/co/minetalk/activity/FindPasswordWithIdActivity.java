package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.api.FindUserPwdApi;
import kr.co.minetalk.api.SnsCheckApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.SnsCheckModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityFindPasswordBinding;
import kr.co.minetalk.repository.RegistRepository;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.Regex;

public class FindPasswordWithIdActivity extends BindActivity<ActivityFindPasswordBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, FindPasswordWithIdActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    private SnsCheckApi mSnsCheckApi;
    private RegistRepository mRegistRepository;
    private FindUserPwdApi   mFindUserPwdApi;

    private ArrayAdapter<String> mSenderAdapter;
    private ArrayList<String> mSenderList = new ArrayList<>();

    private int mCurSenderType = 0; //기본은 휴대전화

    private String mNationCode = "82";
    private int mNationCodePositionIndex = 0;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
        syncFriendList();
        //MainActivity.startActivity(LoginActivity.this);
        //finish();
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        Preferences.setLoginAuthorization("");
        Preferences.setAuthorization("");
        Preferences.setLoginType("");

        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_find_password;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        mRegistRepository = RegistRepository.getInstance();
        initApi();
        setUIEventListener();

        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getNumCode();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            mBinding.tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            mBinding.tvNationCode.setText("+82");
            mNationCode = "82";
        }

        mNationCodePositionIndex = 0;

        mSenderList.clear();
        mSenderList.add(getString(R.string.recommend_phone));
        mSenderList.add(getString(R.string.email_title));

        mSenderAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, mSenderList);
        mSenderAdapter.setDropDownViewResource(R.layout.spinner_text_color);
        mBinding.snSenderType.setAdapter(mSenderAdapter);

        mSyncListener = mFriendSyncListner;

//        if(Build.VERSION.SDK_INT >= 23) {
//            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
//                    ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
//                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.RECEIVE_SMS,
//                        Manifest.permission.READ_SMS}, PERMISSON);
//            } else {
//                initSMSCatchReceiver();
//            }
//        }
    }

    private OnSyncListener mFriendSyncListner = new OnSyncListener() {
        @Override
        public void onSyncComplete() {
            MainActivity.startActivity(FindPasswordWithIdActivity.this);
            finish();
        }
    };

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.ibGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SnsGoogleLoginActivity.startActivity(FindPasswordWithIdActivity.this, MineTalk.REQUEST_CODE_GOOGLE_LOGIN);
            }
        });

        mBinding.ibFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FacebookLoginActivity.startActivity(FindPasswordWithIdActivity.this, MineTalk.REQUEST_CODE_FACEBOOK_LOGIN);
            }
        });

        mBinding.ibKakao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SnsKakaoLoginActivity.startActvity(FindPasswordWithIdActivity.this, MineTalk.REQUEST_CODE_KAKAO_LOGIN);
            }
        });

        mBinding.tvGoFindId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FindIdActivity.startActivity(FindPasswordWithIdActivity.this);
            }
        });

        mBinding.tvGoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.startActivity(FindPasswordWithIdActivity.this);
            }
        });

        mBinding.tvGoRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistNormalActivity.startActivity(FindPasswordWithIdActivity.this);
            }
        });

        //비밀번호 발송
        mBinding.btnSendId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.layoutWarningEmail.setVisibility(View.INVISIBLE);
                mBinding.layoutSendEmail.setVisibility(View.INVISIBLE);

                String id = mBinding.etId.getText().toString().trim();
                if(id == null || id.length() == 0){
                    showMessageAlert(getResources().getString(R.string.find_password_hint_id));
                    return;
                }

                String email = mBinding.etEmail.getText().toString().trim();
                if(mCurSenderType == 1) {
                    if (email == null || email.length() == 0 || !Regex.validateEmail(email)) {
                        showMessageAlert(getResources().getString(R.string.find_id_warning_email));
                        return;
                    }
                }
                if(mCurSenderType == 0)
                    mFindUserPwdApi.execute(id, mNationCode, email);
                else
                    mFindUserPwdApi.execute(id, email);
            }
        });

        //타입 변경
        mBinding.snSenderType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TextView textView = (TextView)parent.getChildAt(0);
                //textView.setTextSize(CommonUtils.convertDpToPixel(5.1f, getApplicationContext()));
                if(textView != null) {
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textSize_40pt));
                    textView.setTextColor(getResources().getColor(R.color.main_text_color));
                }

                mCurSenderType = position;
                mBinding.tvTypeDesc.setText( position == 0 ? getResources().getString(R.string.phone_user_correct_desc) : getResources().getString(R.string.email_user_correct_desc));
                mBinding.etEmail.setHint(position == 0 ? getResources().getString(R.string.find_by_phone_hint) : getResources().getString(R.string.find_by_email_hint));
                mBinding.layoutNationCode.setVisibility(position == 0 ? View.VISIBLE : View.INVISIBLE);
                mBinding.etEmail.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.layoutNationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String[] array = NumberCode.getIntance().getmTitleArray();
                    showBottomWheelPopup(array, mNationCodePositionIndex, mNationCodePickerListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mBinding.etId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() != 0 && mBinding.etEmail.getText().length() != 0){
                    mBinding.btnSendId.setEnabled(true);
                    mBinding.btnSendId.setBackgroundResource(R.drawable.button_bg_16c066);
                }else{
                    mBinding.btnSendId.setEnabled(false);
                    mBinding.btnSendId.setBackgroundResource(R.drawable.button_bg_4d16c066);
                }
            }
        });

        mBinding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() != 0 && mBinding.etId.getText().length() != 0){
                    mBinding.btnSendId.setEnabled(true);
                    mBinding.btnSendId.setBackgroundResource(R.drawable.button_bg_16c066);
                }else{
                    mBinding.btnSendId.setEnabled(false);
                    mBinding.btnSendId.setBackgroundResource(R.drawable.button_bg_4d16c066);
                }
            }
        });
    }

    // 전화번호 국가 코드 선택 Listener
    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
        @Override
        public void onClick(int state, int num) {
            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                mNationCodePositionIndex = num;
                try {
                    String viewName = NumberCode.getIntance().getDataList().get(num).getNumCode();
                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");

                    mBinding.tvNationCode.setText(viewName);
                } catch (Exception e) {
                    e.printStackTrace();
                    mNationCode = "82";
                }
            }
        }

        @Override
        public void onClick(int state, int num, int position) {

        }
    };

    private void initApi() {

        mSnsCheckApi = new SnsCheckApi(this, new ApiBase.ApiCallBack<SnsCheckModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FindPasswordWithIdActivity.this);
            }

            @Override
            public void onSuccess(int request_code, SnsCheckModel snsCheckModel) {
                ProgressUtil.hideProgress(FindPasswordWithIdActivity.this);
                if(snsCheckModel != null) {
                    if(snsCheckModel.getRegist_yn().toUpperCase().equals("Y")) {
                        // 로그인
                        loginRequest("", mRegistRepository.getProviderId(), "", mRegistRepository.getProviderType());
                    } else {
                        // 회원가입(SNS) 이동(추가정보를 받기 위해 이동)
                        RegistSnsActivity.startActivity(FindPasswordWithIdActivity.this, MineTalk.REQUEST_CODE_REGIST);
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FindPasswordWithIdActivity.this);
                showMessageAlert(message);
                mRegistRepository.initialize();
            }

            @Override
            public void onCancellation() {

            }
        });

        mFindUserPwdApi = new FindUserPwdApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FindPasswordWithIdActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(FindPasswordWithIdActivity.this);
                if(baseModel != null) {
                    if(baseModel.getCode().equals("0000")){
                        showMessageAlert(baseModel.getMessage());
                    }
//                    if(baseModel.getCode().equals("0000")){
//                        mBinding.layoutSendEmail.setVisibility(View.VISIBLE);
//                    }else{
//                        mBinding.layoutWarningEmail.setVisibility(View.VISIBLE);
//                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FindPasswordWithIdActivity.this);
                showMessageAlert(message);
                mRegistRepository.initialize();
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void snsCheckRequest(String provider_type, String provider_id) {
        mSnsCheckApi.execute(provider_type, provider_id);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MineTalk.REQUEST_CODE_FACEBOOK_LOGIN) {
            if(resultCode == RESULT_OK) {
                if(data != null && data.getExtras() != null) {
                    mRegistRepository.initialize();
                    mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_S);
                    mRegistRepository.setProviderType(MineTalk.LOGIN_TYPE_FACEBOOK);
                    mRegistRepository.setUserName(data.getExtras().getString(MineTalk.ARG_SNS_USER_NAME));
                    mRegistRepository.setUserEmail(data.getExtras().getString(MineTalk.ARG_SNS_USER_EMAIL));
                    mRegistRepository.setProviderId(data.getExtras().getString(MineTalk.ARG_SNS_ID));
                    mRegistRepository.setUserProfileImageUrl(data.getExtras().getString(MineTalk.ARG_SNS_USER_PROFILE_IMG));

                    snsCheckRequest(mRegistRepository.getProviderType(), mRegistRepository.getProviderId());
                }
            }
        } else if(requestCode == MineTalk.REQUEST_CODE_KAKAO_LOGIN) {
            if(resultCode == RESULT_OK) {
                mRegistRepository.initialize();
                mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_S);
                mRegistRepository.setProviderType(MineTalk.LOGIN_TYPE_KAKAO);
                mRegistRepository.setUserName(data.getExtras().getString(MineTalk.ARG_SNS_USER_NAME));
                mRegistRepository.setUserEmail(data.getExtras().getString(MineTalk.ARG_SNS_USER_EMAIL));
                mRegistRepository.setProviderId(data.getExtras().getString(MineTalk.ARG_SNS_ID));
                mRegistRepository.setUserProfileImageUrl(data.getExtras().getString(MineTalk.ARG_SNS_USER_PROFILE_IMG));

                snsCheckRequest(mRegistRepository.getProviderType(), mRegistRepository.getProviderId());
            }
        }else if(requestCode == MineTalk.REQUEST_CODE_GOOGLE_LOGIN){
            if(resultCode == RESULT_OK) {
                mRegistRepository.initialize();
                mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_S);
                mRegistRepository.setProviderType(MineTalk.LOGIN_TYPE_GOOGLE);
                mRegistRepository.setUserName(data.getExtras().getString(MineTalk.ARG_SNS_USER_NAME));
                mRegistRepository.setUserEmail(data.getExtras().getString(MineTalk.ARG_SNS_USER_EMAIL));
                mRegistRepository.setProviderId(data.getExtras().getString(MineTalk.ARG_SNS_ID));
                mRegistRepository.setUserProfileImageUrl(data.getExtras().getString(MineTalk.ARG_SNS_USER_PROFILE_IMG));

                snsCheckRequest(mRegistRepository.getProviderType(), mRegistRepository.getProviderId());
            }
        }else if(requestCode == MineTalk.REQUEST_CODE_REGIST) {
            try {
                mRegistRepository.initialize();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
