package kr.co.minetalk.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ListAdMessageApi;
import kr.co.minetalk.api.SendAdMessageApi;
import kr.co.minetalk.api.TargetCountApi;
import kr.co.minetalk.api.UploadAdImageV2Api;
import kr.co.minetalk.api.model.AdListImageModel;
import kr.co.minetalk.api.model.AdListImageResponse;
import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.api.model.AdListResponse;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FileInfoModel;
import kr.co.minetalk.api.model.TargetMessageModel;
import kr.co.minetalk.databinding.ActivityCreateMarketingChatBinding;
import kr.co.minetalk.repository.TargetMessageRepository;
import kr.co.minetalk.ui.CustomScrollView;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.listener.OnAdChatItemListener;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.AdImageListView;
import kr.co.minetalk.view.AdListItemView;
import kr.co.minetalk.view.listener.OnQnAListViewListener;

public class CreateMarketingChatActivity extends BindActivity<ActivityCreateMarketingChatBinding> {
    private final int PERMISSON = 1;
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, CreateMarketingChatActivity.class);
        context.startActivity(intent);
    }

    private ListAdMessageApi mListAdMessageApi;
    //private UploadAdImageApi mUploadAdImageApi;
    private UploadAdImageV2Api mUploadAdImageV2Api;
    private SendAdMessageApi mSendAdMessageApi;
    private TargetCountApi   mTargetCountApi;

    //private String mUploadImageUrl = "";
    //private String mUploadImageUrl2 = "";
    //private String mUploadImageUrl3 = "";

    private String[] mUploadImageUrl = { "", "", "" };

    private String mSendStartDate  = "";
    private String mSendStartTime  = "";
    private String mSendEndDate    = "";
    private String mSendEndTime    = "";

    private int mTargetUserCount = 0;
    private int mMaxValue = 0;

    private long mAmountToken = 0;

    private ArrayAdapter<String> adTypeAdapter;
    private ArrayAdapter<String> coinTypeAdapter;

    private TargetMessageRepository mTargetMessageRepository;

    private String[] mAmountArray =  { "10", "20", "30", "40", "50", "60", "70", "80", "90", "100"};
    private String[] mTargetUserCountArray = null;
    private int mAmountArrayIndex = 0;

    private String mCurrentType;

    private final int MAX_AD_IMG_CNT = 3;
    private ArrayList<String> mImgPath = new ArrayList<>();

    // Paging Variable /////////////////////
    private int mPage               = 1;
    private int mTotalCount         = 0;
    private int mDataCount          = 0;
    private boolean mLockScrollView = false;
    ////////////////////////////////////////

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_create_marketing_chat;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        mTargetMessageRepository = TargetMessageRepository.getInstance();
        mTargetMessageRepository.initialize();
        initApi();
        setUIEventListener();

        initTime();
    }

    private void initTime() {
        final Calendar cal = Calendar.getInstance();

        mSendStartDate = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DATE));
        mSendStartTime = String.format("%02d:%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));

        mBinding.tvStartDate.setText(mSendStartDate);

        String sHour = String.format("%02d", cal.get(Calendar.HOUR_OF_DAY));
        String sMin = String.format("%02d", (cal.get(Calendar.MINUTE)));



        if(Integer.parseInt(sHour) >= 18 && Integer.parseInt(sHour) > 0) {
            sHour = "09";
            sMin  = "00";
            mSendStartTime = "09:00";

            try {
                mSendStartDate = addDate(mSendStartDate, 0, 0 , 1);
                mBinding.tvStartDate.setText(mSendStartDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            sHour = String.format("%02d", Integer.parseInt(sHour) + 2);
            sMin = "00";
        }

        mBinding.tvStartHour.setText(sHour);
        mBinding.tvStartMin.setText(sMin);


//        mSendEndDate = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DATE + 7));
        try {
            mSendEndDate = addDate(mSendStartDate, 0, 0, 7);
        } catch (Exception e) {
            e.printStackTrace();
            mSendEndDate = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DATE + 7));
        }

        mSendEndTime = String.format("%02d:%02d", cal.get(Calendar.HOUR_OF_DAY) + 1, cal.get(Calendar.MINUTE));
        mBinding.tvEndDate.setText(mSendEndDate);

        String eHour = String.format("%02d", (cal.get(Calendar.HOUR_OF_DAY) + 2));

        if(eHour.equals("25")) {
            eHour = "01";
        }


//        String eMin = String.format("%02d", (cal.get(Calendar.MINUTE)));
        String eMin = String.format("%02d", 0);
        mBinding.tvEndHour.setText(eHour);
        mBinding.tvEndMin.setText(eMin);

    }


    private  String addDate(String dt, int y, int m, int d) throws Exception  {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();
        Date date = format.parse(dt);
        cal.setTime(date);
        cal.add(Calendar.YEAR, y);      //년 더하기
        cal.add(Calendar.MONTH, m);     //년 더하기
        cal.add(Calendar.DATE, d);      //년 더하기



        return format.format(cal.getTime());

    }


    private void initApi() {
        mUploadAdImageV2Api = new UploadAdImageV2Api(this, new ApiBase.ApiCallBack<AdListImageResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(CreateMarketingChatActivity.this);
            }


            @Override
            public void onSuccess(int request_code, AdListImageResponse response) {
                ProgressUtil.hideProgress(CreateMarketingChatActivity.this);
                if(response != null) {
                    ArrayList<AdListImageModel> imgArr = response.geImage_list();

                    for(int i = 0; i < imgArr.size(); ++i)
                        mUploadImageUrl[i] = imgArr.get(i).getImg_url();

                    sendAdRequest();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(CreateMarketingChatActivity.this);
                showMessageAlert(message);

            }

            @Override
            public void onCancellation() {

            }
        });

        mSendAdMessageApi = new SendAdMessageApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(CreateMarketingChatActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(CreateMarketingChatActivity.this);
                if(baseModel != null) {
                    finish();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(CreateMarketingChatActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mTargetCountApi = new TargetCountApi(this, new ApiBase.ApiCallBack<TargetMessageModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(CreateMarketingChatActivity.this);
            }

            @Override
            public void onSuccess(int request_code, TargetMessageModel targetMessageModel) {
                ProgressUtil.hideProgress(CreateMarketingChatActivity.this);
                if(targetMessageModel != null) {

                    mTargetUserCount = Integer.parseInt(targetMessageModel.getMember_count());
                    if(mTargetUserCount > 100) {

                        int arrayCount = mTargetUserCount / 100;
                        ArrayList<String> tempArray = new ArrayList<>();

                        for(int i = 1 ; i <= arrayCount ; i ++) {
                            tempArray.add(String.valueOf(i * 100));
                        }
                        tempArray.add(String.valueOf(mTargetUserCount));

                        mTargetUserCountArray = new String[tempArray.size()];
                        for(int i = 0 ; i < tempArray.size() ; i++) {
                            mTargetUserCountArray[i] = tempArray.get(i);
                        }
                    } else {
                        mTargetUserCountArray = null;
                    }


                    mMaxValue = targetMessageModel.getMax_value();

                    mBinding.tvTargetCount.setText(targetMessageModel.getMember_count() + getResources().getString(R.string.person_unit));
                    mBinding.tvTargetCountCombo.setText(targetMessageModel.getMember_count() + getResources().getString(R.string.person_unit));

                    process();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(CreateMarketingChatActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mListAdMessageApi = new ListAdMessageApi(this, new ApiBase.ApiCallBack<AdListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(CreateMarketingChatActivity.this);
            }

            @Override
            public void onSuccess(int request_code, AdListResponse adListResponse) {
                ProgressUtil.hideProgress(CreateMarketingChatActivity.this);
                if(adListResponse != null) {
                    setAdListData(adListResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(CreateMarketingChatActivity.this);
            }

            @Override
            public void onCancellation() {
                ProgressUtil.hideProgress(CreateMarketingChatActivity.this);
            }
        });
    }

    private void setAdListData(AdListResponse response) {
        if(mLockScrollView) {
            mTotalCount = response.getTotal_cnt();
            mDataCount = mDataCount + response.getAd_list().size();
        } else {
            mBinding.layoutAdListContainer.removeAllViews();
            mTotalCount = response.getTotal_cnt();
            mDataCount = mDataCount + response.getAd_list().size();
        }

        for(int i = 0 ; i < response.getAd_list().size() ; i++) {
            AdListItemView itemView = new AdListItemView(this);
            itemView.setData(mCurrentType, response.getAd_list().get(i));
            itemView.setOnEventListener(mAdChatListItemListener);
            mBinding.layoutAdListContainer.addView(itemView);
        }

        mLockScrollView = false;
    }

    private OnAdChatItemListener mAdChatListItemListener = new OnAdChatItemListener() {
        @Override
        public void onItemClick(String type, AdListModel data) {
            if(type.equals(MineTalk.AD_LIST_TYPE_R)) {
                AdsMessageDetailActivity.startActivity(CreateMarketingChatActivity.this, type, data.getAd_idx(), data.getUser_name());
            } else if(type.equals(MineTalk.AD_LIST_TYPE_S)) {

            }
        }
    };

    private void setUIEventListener() {

        ArrayList arrayList = new ArrayList<>();
        arrayList.add("일반광고");

        adTypeAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,arrayList);
        mBinding.snType.setAdapter(adTypeAdapter);

        ArrayList coinList = new ArrayList<>();
        coinList.add("마인토큰");

        coinTypeAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,coinList);
        mBinding.snCoinType.setAdapter(coinTypeAdapter);

        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.layoutButtonTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageTargetActivity.startActivity(CreateMarketingChatActivity.this, MineTalk.REQUEST_CODE_TARGET);
            }
        });

        mBinding.layoutButtonSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(CreateMarketingChatActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(CreateMarketingChatActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(CreateMarketingChatActivity.this, new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        onChooseMedia();
                    }
                }
            }
        });

        mBinding.btnFileAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int nCnt = mImgPath.size();
                if(mImgPath.size() >= MAX_AD_IMG_CNT){
                    Toast.makeText(getApplicationContext(), "이미지는 최대 3개까지 업로드 가능합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(CreateMarketingChatActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(CreateMarketingChatActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(CreateMarketingChatActivity.this, new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        onChooseMedia();
                    }
                }
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mImgPath.size() == 0)
                    sendAdRequest();
                else
                    uploadImageRequest();
            }
        });

        mBinding.layoutButtonStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cal = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(CreateMarketingChatActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int date) {

                        String msg = String.format("%d-%02d-%02d", year, month+1, date);
                        mSendStartDate = msg;
                        mBinding.tvStartDate.setText(msg);

                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));

                dialog.getDatePicker().setMinDate(new Date().getTime());    //입력한 날짜 이후로 클릭 안되게 옵션
                dialog.show();

            }
        });

        mBinding.layoutBtnStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cal = Calendar.getInstance();
                TimePickerDialog dialog = new TimePickerDialog(CreateMarketingChatActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {

                        String msg = String.format("%02d:%02d", hour, min);
                        String strHour = String.format("%02d", hour);
                        String strMin = String.format("%02d", min);

                        mSendStartTime = msg;

                        mBinding.tvStartHour.setText(strHour);
                        mBinding.tvStartMin.setText(strMin);

                    }
                }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true);  //마지막 boolean 값은 시간을 24시간으로 보일지 아닐지

                dialog.show();

            }
        });

        mBinding.layoutButtonEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cal = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(CreateMarketingChatActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int date) {

                        String msg = String.format("%d-%02d-%02d", year, month+1, date);
                        mSendEndDate = msg;
                        mBinding.tvEndDate.setText(msg);

                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));

                dialog.getDatePicker().setMinDate(new Date().getTime());    //입력한 날짜 이후로 클릭 안되게 옵션
                dialog.show();
            }
        });

        mBinding.layoutBtnEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cal = Calendar.getInstance();
                TimePickerDialog dialog = new TimePickerDialog(CreateMarketingChatActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {

                        String msg = String.format("%02d:%02d", hour, min);
                        mSendEndTime = msg;
                        String strHour = String.format("%02d", hour);
                        String strMin = String.format("%02d", min);

                        mBinding.tvEndHour.setText(strHour);
                        mBinding.tvEndMin.setText(strMin);

                    }
                }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true);  //마지막 boolean 값은 시간을 24시간으로 보일지 아닐지

                dialog.show();
            }
        });

        mBinding.layoutButtonTargetCountCombo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTargetUserCountArray != null) {
                    showBottomWheelPopup(mTargetUserCountArray, 0, new PopupListenerFactory.BaseInputWheelListener() {
                        @Override
                        public void onClick(int state, int num) {
                            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                mTargetUserCount = Integer.parseInt(mTargetUserCountArray[num]);
                                mBinding.tvTargetCountCombo.setText(String.valueOf(mTargetUserCount) + getResources().getString(R.string.person_unit));

                                process();
                            }
                        }

                        @Override
                        public void onClick(int state, int num, int position) {

                        }
                    });
                }
            }
        });

        mBinding.etUnitPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomWheelPopup(mAmountArray, mAmountArrayIndex, new PopupListenerFactory.BaseInputWheelListener() {
                    @Override
                    public void onClick(int state, int num) {
                        if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                            mAmountArrayIndex = num;
                            mBinding.etUnitPay.setText(mAmountArray[num]);
                            process();
                        }
                    }

                    @Override
                    public void onClick(int state, int num, int position) {

                    }
                });
            }
        });


        // 광고 리스트 스크롤뷰
        mBinding.scrollView.setScrollViewListener(new CustomScrollView.ScrollViewListener() {
            @Override
            public void onScrollChanged(CustomScrollView scrollView, int x, int y, int oldx, int oldy) {
                View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                if(mTotalCount != 0 && mDataCount < mTotalCount) {
                    if (diff == 0 && mLockScrollView == false) { // 스크롤 bottom
                        mLockScrollView = true;
                        mPage = mPage + 1;

                        listAdMessageRequest(mCurrentType, mPage);
                    }
                }
            }
        });


        //광고 보내기
        mBinding.btnSendAddTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.btnSendAddTitle.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
                mBinding.btnAdList.setBackgroundResource(R.drawable.button_bg_bfbfbf);

                mBinding.btnConfirm.setVisibility(View.VISIBLE);
                mBinding.scrollSendAd.setVisibility(View.VISIBLE);
                mBinding.scrollView.setVisibility(View.INVISIBLE);
            }
        });

        //광고 리스트
        mBinding.btnAdList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.btnSendAddTitle.setBackgroundResource(R.drawable.button_bg_bfbfbf);
                mBinding.btnAdList.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);

                mBinding.btnConfirm.setVisibility(View.INVISIBLE);
                mBinding.scrollSendAd.setVisibility(View.INVISIBLE);
                mBinding.scrollView.setVisibility(View.VISIBLE);

                mBinding.layoutAdListContainer.removeAllViews();
                initializePagingInfo();

                mCurrentType = MineTalk.AD_LIST_TYPE_S;
                listAdMessageRequest(mCurrentType, mPage);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MineTalk.REQUEST_CODE_MEDIA) {
            if(resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                if (uri != null) {
                    String contentType = CommonUtils.getMediaType(this, uri);
                    String contentPath = CommonUtils.getImageMediaPath(this, uri);

                    mImgPath.add(contentPath);
                    loadAdImageData();
                    //uploadImageRequest(new File(contentPath));
                }
            }
        } else if( requestCode == MineTalk.REQUEST_CODE_TARGET) {
            if(resultCode == RESULT_OK) {
                targetCountRequest();
            }
        }
    }

    private void loadAdImageData(){
        mBinding.layoutContainer.removeAllViews();
        for(int i = 0 ; i < mImgPath.size() ; i++) {
            AdImageListView itemView = new AdImageListView(this);
            itemView.setData(mImgPath.get(i));
            itemView.setOnItemListener(mAdItemViewListener);
            mBinding.layoutContainer.addView(itemView, getListItemLayoutParams());
        }
    }

    private OnQnAListViewListener mAdItemViewListener = new OnQnAListViewListener() {
        @Override
        public void onClickItem(String path) {
            ArrayList<String> tmpArr = new ArrayList<>();
            for(int i = 0; i < mImgPath.size(); ++i){
                if(!mImgPath.get(i).equals(path))
                    tmpArr.add(mImgPath.get(i));
            }

            mImgPath = tmpArr;
            loadAdImageData();

        }
    };

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //params.topMargin = getResources().getDimensionPixelOffset(R.dimen.height_24);

        return params;
    }

    private void initializePagingInfo() {
        mTotalCount     = 0;
        mDataCount      = 0;
        mPage           = 1;
        mLockScrollView = false;
    }

    private void listAdMessageRequest(String type, int page) {
        mListAdMessageApi.execute(type, page);
    }

    private void onChooseMedia() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,null);
        galleryIntent.setType("image/*");
        galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.select_work));

        startActivityForResult(chooser,MineTalk.REQUEST_CODE_MEDIA);
    }

    private void setImageData(FileInfoModel data) {
        //mUploadImageUrl = data.getUrl();
        if(mUploadImageUrl == null || mUploadImageUrl.equals("")) {
            Glide.with(this).load(R.drawable.img_user).into(mBinding.ivSelectImage);
        } else {
            Glide.with(this).load(mUploadImageUrl).into(mBinding.ivSelectImage);
        }
    }

    private void uploadImageRequest() {
        for( int i = 0; i < MAX_AD_IMG_CNT; ++i)
            mUploadImageUrl[i] = "";

        mUploadAdImageV2Api.execute(this, mImgPath);
    }

    private void sendAdRequest() {

        String subject = mBinding.etSubject.getText().toString();
        String contents = mBinding.etContent.getText().toString();

        if(subject.equals("")) {
            showMessageAlert(getResources().getString(R.string.validation_ad_subject));
            return;
        }

        if(contents.equals("")) {
            showMessageAlert(getResources().getString(R.string.validation_ad_content));
            return;
        }


        try {
//            String ad_token_amount = String.valueOf(mAmountToken);
            String ad_token_amount = String.valueOf(mBinding.etUnitPay.getText().toString());
            JSONArray ad_gender = makeStringArrayParams(mTargetMessageRepository.getGenderMap());
            JSONArray ad_age    = makeStringArrayParams(mTargetMessageRepository.getAgeMap());
            JSONArray ad_addr   = makeAddressArrayParam(mTargetMessageRepository.getAddressMap());
            JSONArray ad_job    = makeStringArrayParams(mTargetMessageRepository.getJobMap());


            if(ad_gender.equals("")) {
                showMessageAlert(getResources().getString(R.string.validation_ad_target));
                return;
            }


            mSendAdMessageApi.execute(subject, contents, mUploadImageUrl[0], mUploadImageUrl[1], mUploadImageUrl[2],
                    ad_token_amount, ad_gender, ad_age, ad_addr, ad_job, mSendStartDate + " " +mSendStartTime, mSendEndDate + " " + mSendEndTime);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private JSONArray makeStringArrayParams(HashMap<String, String> map) {
        if(map.size() > 0) {
            JSONArray jsonArray = new JSONArray();

            Iterator<String> keys = map.keySet().iterator();
            while( keys.hasNext() ){
                String key = keys.next();
                jsonArray.put(map.get(key));
            }
            return jsonArray;
        } else {
            return null;
        }
    }

    private JSONArray makeAddressArrayParam(HashMap<String, String> map) {
        if(map.size() > 0) {
            JSONArray jsonArray = new JSONArray();

            Iterator<String> keys = map.keySet().iterator();
            while( keys.hasNext() ){
                String key = keys.next();

                try {
                    String[] splitAddr = key.split(" ");
                    JSONObject addrObj = new JSONObject();
                    addrObj.put("si" , splitAddr[0]);
                    addrObj.put("gu" , splitAddr[1]);

                    jsonArray.put(addrObj);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return jsonArray;
        } else {
            return null;
        }

    }


    private void targetCountRequest() {

        JSONArray ad_gender = makeStringArrayParams(mTargetMessageRepository.getGenderMap());
        JSONArray ad_age = makeStringArrayParams(mTargetMessageRepository.getAgeMap());
        JSONArray ad_addr = makeAddressArrayParam(mTargetMessageRepository.getAddressMap());
        JSONArray ad_job = makeStringArrayParams(mTargetMessageRepository.getJobMap());
        mTargetCountApi.execute(ad_gender, ad_age, ad_addr, ad_job);
    }


    private void process() {
        int person_token = Integer.parseInt(mBinding.etUnitPay.getText().toString());

        long burnToken = mTargetUserCount * mMaxValue * person_token;
        mBinding.tvBurnToken.setText(CommonUtils.comma_won(String.valueOf(burnToken)));

        mAmountToken = burnToken;
    }


    @Override
    public void updateTheme() {
        super.updateTheme();

        mBinding.btnSendAddTitle.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.btnConfirm.setBackgroundColor(getResources().getColor(R.color.bk_point_color));

        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.btnConfirm.setBackgroundColor(getResources().getColor(R.color.app_point_color));
        }
    }
}
