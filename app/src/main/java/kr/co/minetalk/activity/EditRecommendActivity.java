package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.CheckRecommendApi;
import kr.co.minetalk.api.ModifyRecommendApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.RecommendCheckModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditRecommendBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class EditRecommendActivity extends BindActivity<ActivityEditRecommendBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditRecommendActivity.class);
        context.startActivity(intent);
    }

    private CheckRecommendApi mRecommendCheckApi;
    private ModifyRecommendApi mModifyRecommendApi;

    private String mRecommendKeyword = "";
    private String mRecommendNationCode = "";
    private String mRecommendName = "";
    private String mRecommendHP = "";

    private boolean mIsCorrectRecommend = false;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);

        showMessageAlert(getResources().getString(R.string.recommend_save_success), getResources().getString(R.string.btn_go_back_title),
                new PopupListenerFactory.SimplePopupListener() {
                    @Override
                    public void onClick(DialogInterface f, int state) {
                        if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                            finish();
                        }
                    }
                });

        //syncFriendList();
        //MainActivity.startActivity(LoginActivity.this);
        //finish();
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        Preferences.setLoginAuthorization("");
        Preferences.setAuthorization("");
        Preferences.setLoginType("");

        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_recommend;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });


        //추천인 등록 여부 확인
        mBinding.btnCheckDuplicate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecommendKeyword = mBinding.etRecommendUser.getText().toString().trim();

                if(mRecommendKeyword == null || mRecommendKeyword.length() == 0){
                    showMessageAlert(getResources().getString(R.string.recommend_format_wrong_warning));
                    return;
                }

                mRecommendCheckApi.execute(mRecommendKeyword);
            }
        });

        //추천인 등록 하기.
        mBinding.btnRecommendConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mRecommendKeyword == null || mRecommendKeyword.length() == 0 || !mIsCorrectRecommend){
                    showMessageAlert(getResources().getString(R.string.recommend_format_wrong_warning));
                    return;
                }

                showMessageAlert(getResources().getString(R.string.recommend_last_confirm_msg),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    mModifyRecommendApi.execute(mRecommendKeyword);
                                }
                            }
                        });
            }
        });

        mBinding.etRecommendUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if( s.length() > 1){
                    mBinding.btnCheckDuplicate.setEnabled(true);
                    mBinding.btnCheckDuplicate.setBackgroundResource(R.drawable.button_bg_16c066);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void initApi() {
        mRecommendCheckApi = new CheckRecommendApi(this, new ApiBase.ApiCallBack<RecommendCheckModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditRecommendActivity.this);
            }

            @Override
            public void onSuccess(int request_code, RecommendCheckModel baseModel) {
                ProgressUtil.hideProgress(EditRecommendActivity.this);
                if(baseModel != null) {
                    //추천인이 검색되었을 경우
                    if(baseModel.getUser_login_id().length() > 0 || baseModel.getUser_hp().length() > 0 || baseModel.getUser_nation_code().length() > 0) {
                        mRecommendNationCode = baseModel.getUser_nation_code();
                        mRecommendName = baseModel.getUser_login_id();
                        mRecommendHP = baseModel.getUser_hp();

                        mIsCorrectRecommend = true;

                        if(!mRecommendNationCode.equals("") && !mRecommendHP.equals(""))
                            mBinding.etRecommendUser.setText(String.format("%s(+%s %s)", baseModel.getUser_login_id(), baseModel.getUser_nation_code(), baseModel.getUser_hp()));
                        else
                            mBinding.etRecommendUser.setText(String.format("%s", baseModel.getUser_login_id()));

                        mBinding.btnCheckDuplicate.setEnabled(false);
                        mBinding.btnCheckDuplicate.setBackgroundResource(R.drawable.button_bg_4d16c066);
                    }else {
                        mRecommendKeyword = "";
                        mRecommendNationCode = "";
                        mRecommendName = "";
                        mRecommendHP = "";

                        mBinding.etRecommendUser.setText("");
                        mIsCorrectRecommend = false;

                        mBinding.btnCheckDuplicate.setEnabled(true);
                        mBinding.btnCheckDuplicate.setBackgroundResource(R.drawable.button_bg_16c066);

                        showMessageAlert(getResources().getString(R.string.recommend_no_exist_waring));
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mRecommendKeyword = "";
                ProgressUtil.hideProgress(EditRecommendActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mModifyRecommendApi = new ModifyRecommendApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditRecommendActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditRecommendActivity.this);
                if(baseModel != null) {
                    MineTalkApp.getUserInfoModel().setRecommend_hp(mRecommendHP);
                    MineTalkApp.getUserInfoModel().setRecommend_name(mRecommendName);
                    MineTalkApp.getUserInfoModel().setRecommend_nation_code(mRecommendNationCode);

                    userInfoRequest();

                }
            }

            @Override
            public void onFailure(int request_code, String message) {

                mRecommendKeyword = "";
                ProgressUtil.hideProgress(EditRecommendActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
