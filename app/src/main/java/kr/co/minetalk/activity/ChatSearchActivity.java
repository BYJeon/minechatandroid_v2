package kr.co.minetalk.activity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnDataObserverListener;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.adapter.ChatSearchFragmentPagerAdapter;
import kr.co.minetalk.adapter.FriendSearchFragmentPagerAdapter;
import kr.co.minetalk.api.FindFriendApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.FriendSearchResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.ActivityChatSearchBinding;
import kr.co.minetalk.databinding.ActivityFriendSearchBinding;
import kr.co.minetalk.fragment.ChatSearchFragment;
import kr.co.minetalk.fragment.FriendFragment;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.CreateThreadApi;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.ui.listener.OnProfileListener;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.Regex;

public class ChatSearchActivity extends BindActivity<ActivityChatSearchBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ChatSearchActivity.class);
        context.startActivity(intent);
    }

    private FindFriendApi mFindFriendApi;
    private ChatSearchFragmentPagerAdapter mAdapter;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_chat_search;
    }


    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        initApi();

        mSyncListener = mFriendSearchSyncListener;

        setUIEventListener();
        setSearchFriendPagerView();
        updateTheme();
        refreshChildView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshChildView();
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        boolean listType = Preferences.getSearchChatListType();

        if(MineTalk.isBlackTheme){
            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.btnConfirm.setBackgroundResource(R.drawable.bg_round_4dcbd60c);

            mBinding.etSearch.setBackgroundResource(R.drawable.bg_round_333d49);
            mBinding.etSearch.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutSearch.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutTablayout.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tablayout.setBackgroundResource(R.drawable.bg_tab_layout_272f39);
            mBinding.tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tablayout.setTabTextColors(getResources().getColor(R.color.white_color), getResources().getColor(R.color.white_color));

        }else{
            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.btnConfirm.setBackgroundResource(R.drawable.bg_round_4d16c066);

            mBinding.etSearch.setBackgroundResource(R.drawable.bg_round_f2f2f2);
            mBinding.etSearch.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutSearch.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutTablayout.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tablayout.setBackgroundResource(R.drawable.bg_tab_layout);
            mBinding.tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.app_point_color));
            mBinding.tablayout.setTabTextColors(getResources().getColor(R.color.main_text_color), getResources().getColor(R.color.main_text_color));
        }

    }

    private void initApi() {
        mFindFriendApi = new FindFriendApi(this, new ApiBase.ApiCallBack<FriendSearchResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChatSearchActivity.this);
            }

            @Override
            public void onSuccess(int request_code, FriendSearchResponse friendSearchResponse) {
                ProgressUtil.hideProgress(ChatSearchActivity.this);
                if(friendSearchResponse != null) {
                    setupData(friendSearchResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(ChatSearchActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });


        mDataObserver = new OnDataObserverListener() {
            @Override
            public void friendListLoadComplete() {

                String search_text = mBinding.etSearch.getText().toString();
                if(!search_text.equals("")) {
                    searchRequest(search_text);
                }
                CommonUtils.hideKeypad(ChatSearchActivity.this, mBinding.etSearch);

            }
        };
    }

    private void setSearchFriendPagerView(){
        final ViewPager viewPager = mBinding.viewPager;
        viewPager.setOffscreenPageLimit(3);

        mAdapter = new ChatSearchFragmentPagerAdapter(getSupportFragmentManager(), ((MainActivity)MineTalkApp.getmMainContext()).getOnFragmentEventListner());
        viewPager.setAdapter(mAdapter);

        mBinding.tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(), false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mBinding.tablayout));

        mBinding.getRoot().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int topPos = (mBinding.tablayout.getHeight() / 2) - (mBinding.ivListType.getHeight() / 2);

                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams)mBinding.ivListType.getLayoutParams();
                if( marginParams.topMargin == 0 )
                    marginParams.setMargins(marginParams.leftMargin, topPos, marginParams.rightMargin, marginParams.rightMargin);
            }
        });


    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search_text = mBinding.etSearch.getText().toString();

                if(!search_text.equals("") && search_text.length() > 1) {
                    boolean isValidate = Regex.validatePhone(search_text);
                    if(isValidate) {
                        // 연락처 검색 로직 추가 필요(전화번호검색)!!
                        ArrayList<ContactModel> contactList = getContactList(ChatSearchActivity.this, SearchType.PHONE, search_text);
                        if(contactList != null) {
                            for(int i = 0 ; i < contactList.size() ; i++) {
                                Log.e("@@@TAG2", "" + contactList.get(i).getUser_phone() + ", " + contactList.get(i).getUser_name());
                            }
                        }

                    } else {
                        // 연락처 검색 로직 추가 필요(이름검색)!!
                        ArrayList<ContactModel> contactList = getContactList(ChatSearchActivity.this, SearchType.TEXT, search_text);
                        if(contactList != null) {
                            for(int i = 0 ; i < contactList.size() ; i++) {
                                Log.e("@@@TAG2", "" + contactList.get(i).getUser_name() + ", " + contactList.get(i).getUser_phone());
                            }
                        }

                    }


                    ChatSearchFragment chatFragment = (ChatSearchFragment)mAdapter.getItem(0);
                    FriendFragment friendFragment   = (FriendFragment)mAdapter.getItem(1);
                    boolean listType = Preferences.getSearchChatListType();
                    chatFragment.setSearchKeyword(search_text, listType);
                    friendFragment.setSearchKeyword(search_text, listType);
                }
                CommonUtils.hideKeypad(ChatSearchActivity.this, mBinding.etSearch);
            }
        });

        //친구 리스트 ViewType(List,Grid)
        mBinding.ivListType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setSearchChatListType(!Preferences.getSearchChatListType());
                boolean listType = Preferences.getSearchChatListType();

                if(MineTalk.isBlackTheme) { //GridType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
                } else { //ListType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
                }

                for(int i = 0; i < mAdapter.getCount(); ++i){
                    BaseFragment baseFragment = (BaseFragment) mAdapter.getItem(i);
                    baseFragment.refreshView(listType);
                }
            }
        });

        // 채팅 검색어 변경 Listener
        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 1)
                    mBinding.btnConfirm.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.bg_round_cbd60c : R.drawable.bg_round_16c066);
                else
                    mBinding.btnConfirm.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.bg_round_4dcbd60c : R.drawable.bg_round_4d16c066);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void searchRequest(String search_text) {
        mFindFriendApi.execute(search_text);
    }

    private void setupData(FriendSearchResponse response) {

        refreshChildView();

        if(response != null) {
            //mAdapter.clear();
            //mAdapter.setData(response.getFriend_list());

            //String countMessage = String.format(getString(R.string.friend_search_total_count), response.getFriend_list().size());
            //mBinding.tvSearchFriendCount.setText(countMessage);

        }
    }

    private void refreshChildView(){
        boolean listType = Preferences.getSearchChatListType();

        for(int i = 0; i < mAdapter.getCount(); ++i){
            BaseFragment baseFragment = (BaseFragment) mAdapter.getItem(i);
            baseFragment.refreshView(listType);
        }
    }


    private OnProfileListener mProfileListener = new OnProfileListener() {
        @Override
        public void onTalk(FriendListModel data) {
            newThreadRequest(MineTalkApp.getUserInfoModel().getUser_xid(), data.getUser_xid());
        }

        @Override
        public void onFavorite(FriendListModel data, String yn) {
            FriendManager.getInstance().updateFavorite(data.getUser_xid(), yn);
            MineTalkApp.setFriendList(FriendManager.getInstance().selectFriend());

        }

        @Override
        public void onCall(FriendListModel data) {
            FriendManager.getInstance().removeFriend(data.getUser_xid());
            MineTalkApp.setFriendList(FriendManager.getInstance().selectFriend());

            userBlockRequest(data.getUser_xid());
        }

        @Override
        public void onGift(FriendListModel data) {
            PointSendActivity.startActivity(ChatSearchActivity.this, PointSendActivity.TYPE_GIFT_POINT, PointSendActivity.MODE_TOKEN, data.getUser_xid(), data.getUser_name(), data.getUser_profile_image());
        }

        @Override
        public void onBlock(FriendListModel data) {

        }

        @Override
        public void onFriendAdd(FriendListModel data) {
            addFriendRequest(data.getUser_xid());


        }

        @Override
        public void onTalkMe(UserInfoBaseModel data) {

        }

        @Override
        public void onEditProfile() {

        }
    };

    private void newThreadRequest(String user_xid, String friend_xid) {
        ProgressUtil.showProgress(this);
        CreateThreadApi createThreadApi = new CreateThreadApi(this, user_xid, friend_xid);
        createThreadApi.request(new BaseHttpRequest.APICallbackListener<ThreadData>() {
            @Override
            public void onSuccess(ThreadData res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatSearchActivity.this);
                        mAdapter.notifyDataSetChanged();


                        if(user_xid.equals(friend_xid)) {
                            Preferences.setMyChattingThreadKey(res.getThreadKey());
                            String title = getString(R.string.popup_profile_button_chat_me);

                            ChatDetailActivity.startActivity(ChatSearchActivity.this, res.getThreadKey(), title);

                        } else {
                            String threadName = "";
                            if(res.getMembers().size() > 0) {
                                String members = "";
                                for(ThreadMember tmember : res.getMembers()) {
                                    if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                        members = tmember.getNickName();
                                        break;
                                    }
                                }
                                threadName = members;
                            }

                            ChatDetailActivity.startActivity(ChatSearchActivity.this, res.getThreadKey(), threadName);
                        }


                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatSearchActivity.this);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private OnSyncListener mFriendSearchSyncListener = new OnSyncListener() {
        @Override
        public void onSyncComplete() {
            String search_text = mBinding.etSearch.getText().toString();
            if(!search_text.equals("")) {
                searchRequest(search_text);
            }
        }
    };



    private enum SearchType{
        TEXT,
        PHONE
    }

    /**
     * 주소록 검색
     * @param context
     * @param type
     * @param searchText
     * @return
     */
    public static ArrayList<ContactModel> getContactList(Context context, SearchType type, String searchText) {


        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        };

        ContentResolver mContentResolver = context.getContentResolver();
        String[] selectionArgs = new String[] { searchText };
        String selection = null;

        if(type == SearchType.TEXT) {
            selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " = ?";
//            selectionArgs = new String[] { searchText };
        } else if(type == SearchType.PHONE) {
            selection = ContactsContract.CommonDataKinds.Phone.NUMBER + " = ?";
//            selectionArgs = new String[] { searchText };
        }

        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor cursor = mContentResolver.query(uri, projection, selection, new String[] { searchText }, sortOrder);
        ArrayList<ContactModel> contactList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                String phoneNumber = cursor.getString(0);
                String name = cursor.getString(1);
                if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
                    continue;
                }
                phoneNumber = phoneNumber.replaceAll("-", "");
                contactList.add(new ContactModel(name, phoneNumber));
            } while (cursor.moveToNext());
        }
        return contactList;
    }

}
