package kr.co.minetalk.activity.listener;

public interface OnKeyboardVisibilityListener {
    void onVisibilityChanged(boolean visible);
}
