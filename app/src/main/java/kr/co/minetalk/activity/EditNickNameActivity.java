package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import java.util.HashMap;

import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.ActivityEditNickNameBinding;

public class EditNickNameActivity extends BindActivity<ActivityEditNickNameBinding> {
    private static final String COLUMN_USER_HP = "column_user_hp";
    private static final String COLUMN_USER_NAME = "column_user_name";

    public static void startActivity(Context context, String userHP, String userName) {
        Intent intent = new Intent(context, EditNickNameActivity.class);
        intent.putExtra(COLUMN_USER_HP, userHP);
        intent.putExtra(COLUMN_USER_NAME, userName);
        context.startActivity(intent);
    }

    //private ModifyUserNameApi mModifyUserNameApi;

    private String mUserHP;
    private String mUserName;
    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_nick_name;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            mUserHP = getIntent().getExtras().getString(COLUMN_USER_HP);
            mUserName = getIntent().getExtras().getString(COLUMN_USER_NAME);
            if(mUserName != null && mUserName.length() > 0){
                mBinding.etName.setText(mUserName);
                mBinding.tvCurNickName.setText( String.format("친구가 입력한 닉네임 : %s", mUserName));
            }
        }

        initApi();
        setUIEventListener();
    }

    private void initApi() {

    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nickName = mBinding.etName.getText().toString().trim();
                if(nickName.length() > 0) {
                    FriendManager.getInstance().updateUserNickName(mUserHP, nickName);
                }
                finish();
            }
        });
    }
}
