package kr.co.minetalk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.activity.base.BaseActivity;

/**
 * Created by kyd0822 on 2017. 8. 8..
 */

public class SnsGoogleLoginActivity extends BaseActivity {
    public static final int RC_SIGN_IN = 1;
    private GoogleSignInClient mGoogleSignInClient;

    public static void startActivity(Context context, int reqeustCode) {
        Intent intent = new Intent(context, SnsGoogleLoginActivity.class);
        ((Activity)context).startActivityForResult(intent, reqeustCode);
    }

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);



        signIn();
    }

    private void signIn(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private GoogleApiClient.OnConnectionFailedListener mFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            finish();
        }
    };


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            String email = account.getEmail();
            String m = account.getFamilyName();
            String m2 = account.getGivenName();
            String m3 = account.getDisplayName();

            String id = account.getId();
            Uri img_uri = account.getPhotoUrl();

            Log.e("@@@JBY", "id : " + id);
            Log.e("@@@JBY", "name : " + m+m2+m3);

            Intent intent = new Intent();
            intent.putExtra(MineTalk.ARG_SNS_USER_NAME, account.getDisplayName());
            intent.putExtra(MineTalk.ARG_SNS_USER_EMAIL, account.getEmail());
            intent.putExtra(MineTalk.ARG_SNS_ID, String.valueOf(account.getId()));
            if(img_uri != null)
                intent.putExtra(MineTalk.ARG_SNS_USER_PROFILE_IMG, img_uri.getPath());

            setResult(RESULT_OK, intent);
            finish();

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
            finish();
        }
    }
}
