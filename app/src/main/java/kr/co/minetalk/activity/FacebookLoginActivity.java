package kr.co.minetalk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.activity.base.BaseActivity;


/**
 * Created by episode on 2018. 10. 5..
 */

public class FacebookLoginActivity extends BaseActivity {

    private CallbackManager mCallbackmanager = null;

    public static void startActivity(Context context, int reqeustCode) {
        Intent intent = new Intent(context, FacebookLoginActivity.class);
        ((Activity)context).startActivityForResult(intent, reqeustCode);
    }

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        onFblogin();

    }

    private void onFblogin()
    {
        mCallbackmanager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        AccessToken.refreshCurrentAccessTokenAsync();
        if(Profile.getCurrentProfile() == null )
        {

        }


        LoginManager.getInstance().registerCallback(mCallbackmanager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        Profile profile = Profile.getCurrentProfile();

                        if (Profile.getCurrentProfile() == null) {
                            AccessToken.getCurrentAccessToken();
                            Profile.fetchProfileForCurrentAccessToken();
                        }

                        GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject json, GraphResponse response) {
                                        if (response.getError() != null) {
                                            // handle error
                                            System.out.println("ERROR");
                                            Log.e("@@@KYD","ERROR");
                                        } else {
                                            System.out.println("Success");
                                            Log.e("@@@KYD","Success");

//                                            Log.e("@@@KYD","accesToken ==> " + AccessToken.getCurrentAccessToken());

                                            try {
                                                String jsonresult = String.valueOf(json);

                                                Log.e("@@@KYD","Success ==> " + jsonresult);
                                                String str_id = json.getString("id");
                                                String str_name = json.getString("name");
                                                String thum_img = "https://graph.facebook.com/" + str_id + "/picture";

                                                Log.e("@@@KYD", "reponse : " + json.toString());
                                                Log.e("@@@KYD", "id : " + str_id);
                                                Log.e("@@@KYD", "name : " + str_name);
                                                Log.e("@@@KYD", "thum_img :" + thum_img);


                                                Intent intent = new Intent();
                                                intent.putExtra(MineTalk.ARG_SNS_USER_NAME, str_name);
                                                intent.putExtra(MineTalk.ARG_SNS_USER_EMAIL,"");
                                                intent.putExtra(MineTalk.ARG_SNS_ID, str_id );
                                                intent.putExtra(MineTalk.ARG_SNS_USER_PROFILE_IMG, thum_img);

                                                setResult(RESULT_OK, intent);
                                                finish();

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }
                                }).executeAsync();
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        AccessToken.setCurrentAccessToken(null);
                        onFblogin();
                        if (error instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                onFblogin();
                            }
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackmanager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_CANCELED) {
            finish();
        }
    }

}
