package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.SendPointUserAdapter;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.ActivitySendPointUserBinding;

public class SendPointUserActivity extends BindActivity<ActivitySendPointUserBinding> {

    public static final String COLUMN_POINT_TYPE = "point_type";

    public static void startActivity(Context context, String point_type) {
        Intent intent = new Intent(context, SendPointUserActivity.class);
        intent.putExtra(COLUMN_POINT_TYPE, point_type);
        context.startActivity(intent);
    }

    private SendPointUserAdapter mAdapter;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }


    private String mPointType;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_send_point_user;
    }

    @Override
    protected void initView() {
        if(getIntent() != null && getIntent().getExtras() != null ) {
            mPointType = getIntent().getExtras().getString(COLUMN_POINT_TYPE, MineTalk.POINT_TYPE_CASH);
        }
        mBinding.setHandlers(this);

        mAdapter = new SendPointUserAdapter();
        mBinding.listView.setAdapter(mAdapter);

        setUIEventListener();

        setupData();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectIndex = mAdapter.getSelectIndex();
                if( selectIndex == -1) {
                    return;
                }

                FriendListModel item = (FriendListModel) mAdapter.getItem(selectIndex);
                if(mPointType.equals(MineTalk.POINT_TYPE_CASH)) {
                    PointSendActivity.startActivity(SendPointUserActivity.this,
                            PointSendActivity.TYPE_GIFT_POINT,
                            PointSendActivity.MODE_CASH,
                            item.getUser_xid(),
                            item.getUser_name(),
                            item.getUser_profile_image());
                } else {
                    PointSendActivity.startActivity(SendPointUserActivity.this,
                            PointSendActivity.TYPE_GIFT_POINT,
                            PointSendActivity.MODE_TOKEN,
                            item.getUser_xid(),
                            item.getUser_name(),
                            item.getUser_profile_image());

                }
                finish();
            }
        });

        mBinding.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mAdapter.setSelectIndex(position);
            }
        });

        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setupData() {
        ArrayList<FriendListModel> friendList = new ArrayList<>();
        for(int i = 0 ; i < MineTalkApp.getFriendList().size() ; i++) {
            FriendListModel data = MineTalkApp.getFriendList().get(i);

            FriendListModel item = new FriendListModel();
            item.setUser_xid(data.getUser_xid());
            item.setUser_hp(data.getUser_hp());
            item.setUser_name(data.getUser_name());
            item.setUser_state_message(data.getUser_state_message());
            item.setUser_profile_image(data.getUser_profile_image());

            friendList.add(item);
        }


        mAdapter.setData(friendList);
    }

    @Override
    public void updateTheme() {
        super.updateTheme();
    }
}
