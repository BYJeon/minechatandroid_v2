package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import java.util.ArrayList;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnFilterListener;
import kr.co.minetalk.adapter.HorizontalListAdapter;
import kr.co.minetalk.adapter.decoration.HorizontalListDecoration;
import kr.co.minetalk.api.ListFAQApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FaqModel;
import kr.co.minetalk.api.model.FaqResponse;
import kr.co.minetalk.databinding.ActivityFaqBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.FAQItemView;

public class FAQActivity extends BindActivity<ActivityFaqBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, FAQActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    private ListFAQApi mListFAQApi;
    private String mFilter = "";
    private ArrayList<FaqModel> mFaqList = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_faq;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        faqListRequest();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mBinding.recyclerView.setLayoutManager(layoutManager);

        ArrayList<String> itemList = new ArrayList<>();
        itemList.add("All");
        itemList.add(getResources().getString(R.string.mine_token_title));
        itemList.add(getResources().getString(R.string.mine_cash_title));
        itemList.add("마이닝쿠폰");
        itemList.add(getResources().getString(R.string.year_member_title));
        itemList.add("계정관리");
        itemList.add("콘텐츠(쇼핑,게임,기프티콘)");
        itemList.add(getResources().getString(R.string.ads_access_title));
        itemList.add(getResources().getString(R.string.msg_target_btn_job_999));

        //mFilter = getResources().getString(R.string.mine_token_title);

        HorizontalListAdapter adapter = new HorizontalListAdapter(this, itemList, mOnFilterListener);
        mBinding.recyclerView.setAdapter(adapter);

        HorizontalListDecoration decoration = new HorizontalListDecoration();
        mBinding.recyclerView.addItemDecoration(decoration);
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void initApi() {
        mListFAQApi = new ListFAQApi(this, new ApiBase.ApiCallBack<FaqResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(FAQActivity.this);
            }

            @Override
            public void onSuccess(int request_code, FaqResponse faqResponse) {
                ProgressUtil.hideProgress(FAQActivity.this);
                if(faqResponse != null) {
                    setupData(faqResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(FAQActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private OnFilterListener mOnFilterListener = new OnFilterListener() {
        @Override
        public void onFilterChanged(String filter) {
            mFilter = filter;
            setupDataByFilter();
        }
    };

    private void faqListRequest() {
        mListFAQApi.execute();
    }

    private void setupData(FaqResponse response) {
        mBinding.layoutListContainer.removeAllViews();
        FAQItemView itemView = new FAQItemView(this, FAQItemView.ItemType.ITEM_TYPE_TITLE);
        FaqModel model = new FaqModel();
        model.setFaq_type(getString(R.string.type_title));
        model.setFaq_question(getString(R.string.marketting_message_text_subject));
        itemView.setData(model);
        mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());

        for(int i = 0 ; i < response.getData().size() ; i++) {
            mFaqList.add(response.getData().get(i));
            FaqModel faqModel = response.getData().get(i);
            if(!faqModel.getFaq_type().contains(mFilter)) continue;
            itemView = new FAQItemView(this, (i%2) == 0 ? FAQItemView.ItemType.ITEM_TYPE_ODD : FAQItemView.ItemType.ITEM_TYPE_EVEN);
            itemView.setData(response.getData().get(i));

            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }
    }

    private void setupDataByFilter() {
        mBinding.layoutListContainer.removeAllViews();
        FAQItemView itemView = new FAQItemView(this, FAQItemView.ItemType.ITEM_TYPE_TITLE);
        FaqModel model = new FaqModel();
        model.setFaq_type(getString(R.string.type_title));
        model.setFaq_question(getString(R.string.marketting_message_text_subject));
        itemView.setData(model);
        mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());

        for(int i = 0 ; i < mFaqList.size() ; i++) {
            FaqModel faqModel = mFaqList.get(i);
            if(!faqModel.getFaq_type().contains(mFilter)) continue;
            itemView = new FAQItemView(this, (i%2) == 0 ? FAQItemView.ItemType.ITEM_TYPE_ODD : FAQItemView.ItemType.ITEM_TYPE_EVEN);
            itemView.setData(faqModel);

            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }
    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //params.topMargin = getResources().getDimensionPixelOffset(R.dimen.height_24);

        return params;
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.layoutFilterRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
        }else{

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.layoutFilterRoot.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }
}
