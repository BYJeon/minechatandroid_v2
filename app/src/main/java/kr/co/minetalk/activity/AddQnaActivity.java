package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import android.app.Activity;
import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.AddQnaApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityAddQnaBinding;
import kr.co.minetalk.utils.ProgressUtil;

public class AddQnaActivity extends BindActivity<ActivityAddQnaBinding> {

    public static void startActivity(Context context, int requestCode) {
        Intent intent = new Intent(context, AddQnaActivity.class);
        ((Activity)context).startActivityForResult(intent, requestCode);
    }

    private AddQnaApi mAddQnaApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_qna;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        initApi();
        setUIEnvetListener();
    }

    private void initApi() {

        mAddQnaApi = new AddQnaApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(AddQnaActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(AddQnaActivity.this);
                if(baseModel != null) {
                    Toast.makeText(AddQnaActivity.this, getResources().getString(R.string.qna_question_register), Toast.LENGTH_SHORT).show();
                    setResult(RESULT_OK);
                    finish();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(AddQnaActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void addQnaRequest(String question) {
        mAddQnaApi.execute(question);
    }

    private void setUIEnvetListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qnaMessage = mBinding.etQnaMessage.getText().toString();


                if(qnaMessage.equals("")) {
                    showMessageAlert(getResources().getString(R.string.qna_question_validate));
                    return;
                }

                addQnaRequest(qnaMessage);
            }
        });
    }
}
