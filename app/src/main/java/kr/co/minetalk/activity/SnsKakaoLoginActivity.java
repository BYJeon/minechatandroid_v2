package kr.co.minetalk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;


import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.callback.MeV2ResponseCallback;
import com.kakao.usermgmt.callback.UnLinkResponseCallback;
import com.kakao.usermgmt.response.MeV2Response;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;

import java.util.ArrayList;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BaseActivity;

/**
 * Created by kyd0822 on 2017. 8. 8..
 */

public class SnsKakaoLoginActivity extends BaseActivity {


    public static final void startActvity(Context context) {
        Intent intent = new Intent(context, SnsKakaoLoginActivity.class);
        context.startActivity(intent);
    }

    public static final void startActvity(Context context, int requestCode) {
        Intent intent = new Intent(context, SnsKakaoLoginActivity.class);
        ((Activity)context).startActivityForResult(intent, requestCode);
    }



    private final ISessionCallback callback = new SessionCallback();
    private Session session;

    private RelativeLayout mRlRootLayout = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sns_kakao_login);

        session = Session.getCurrentSession();
        session.addCallback(callback);
//        session.checkAndImplicitOpen();
//        unLinkKakao();
        requestKakaoLogin();

        initViews();
        setUIComponentListener();

    }

    private void initViews() {
        mRlRootLayout = (RelativeLayout) findViewById(R.id.rl_root_layout);
        mRlRootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    private void setUIComponentListener() {

    }

    private void requestKakaoLogin() {

        if (Session.getCurrentSession().getAuthCodeManager().isTalkLoginAvailable()) {
            Session.getCurrentSession().open(AuthType.KAKAO_TALK, (Activity) SnsKakaoLoginActivity.this);
        } else {
            Session.getCurrentSession().open(AuthType.KAKAO_ACCOUNT, (Activity) SnsKakaoLoginActivity.this);
        }
    }


    /**
     * 사용자의 상태를 알아 보기 위해 me API 호출을 한다.
     */
    protected void requestMe() {
        List<String> propertyKeys = new ArrayList<String>();
        propertyKeys.add("kaccount_email");
        propertyKeys.add("nickname");
        propertyKeys.add("profile_image");
        propertyKeys.add("thumbnail_image");

        UserManagement.getInstance().me(new MeV2ResponseCallback() {
            @Override
            public void onSessionClosed(ErrorResult errorResult) {

            }

            @Override
            public void onSuccess(MeV2Response result) {
                Log.e("@@@TAG","onSuccess => " + result.getKakaoAccount());

                Log.e("@@TAG","nickName => " + result.getNickname());
                Log.e("@@TAG","email    => " + result.getKakaoAccount().getEmail());
                Log.e("@@TAG","user_sns_id => " + result.getKakaoAccount().getDisplayId());
                Log.e("@@TAG","user_thum_img => " + result.getThumbnailImagePath());
                Log.e("@@TAG","user_profile_img => " + result.getProfileImagePath());

                Intent intent = new Intent();
                intent.putExtra(MineTalk.ARG_SNS_USER_NAME, result.getNickname());
                intent.putExtra(MineTalk.ARG_SNS_USER_EMAIL,result.getKakaoAccount().getEmail());
                intent.putExtra(MineTalk.ARG_SNS_ID, String.valueOf(result.getId()));
                intent.putExtra(MineTalk.ARG_SNS_USER_PROFILE_IMG, result.getProfileImagePath());

                //Toast.makeText(getApplicationContext(), String.valueOf(result.getId()), Toast.LENGTH_LONG).show();
                setResult(RESULT_OK, intent);
                finish();
            }
        });


//        UserManagement.getInstance().requestMe(new MeV() {
//            @Override
//            public void onFailure(ErrorResult errorResult) {
//                String message = "failed to get user info. msg=" + errorResult;
//                Log.e("@@@TAG","requestMe " + message);
//            }
//
//            @Override
//            public void onSessionClosed(ErrorResult errorResult) {
//            }
//
//            @Override
//            public void onSuccess(UserProfile userProfile) {
//                Log.e("@@@TAG","onSuccess => " + userProfile);
//
//                Log.e("@@TAG","nickName => " + userProfile.getNickname());
//                Log.e("@@TAG","email    => " + userProfile.getEmail());
//                Log.e("@@TAG","user_sns_id => " + userProfile.getId());
//                Log.e("@@TAG","user_thum_img => " + userProfile.getThumbnailImagePath());
//                Log.e("@@TAG","user_profile_img => " + userProfile.getProfileImagePath());
//
//                Intent intent = new Intent();
//                intent.putExtra(MineTalk.ARG_SNS_USER_NAME, userProfile.getNickname());
//                intent.putExtra(MineTalk.ARG_SNS_USER_EMAIL,userProfile.getEmail());
//                intent.putExtra(MineTalk.ARG_SNS_ID, String.valueOf(userProfile.getId()) );
//                intent.putExtra(MineTalk.ARG_SNS_USER_PROFILE_IMG, userProfile.getProfileImagePath());
//
//                setResult(RESULT_OK, intent);
//                finish();
//            }
//
//            @Override
//            public void onNotSignedUp() {
//                requestKakaoLogin();
//            }
//        }, propertyKeys, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        session.removeCallback(callback);
    }


    private class SessionCallback implements ISessionCallback {

        @Override
        public void onSessionOpened() {
            SnsKakaoLoginActivity.this.onSessionOpened();
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            if(exception != null) {
            }

            setResult(RESULT_CANCELED);
            finish();
        }
    }

    protected void onSessionOpened() {
        requestMe();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }


    /**
     * 앱 연결 해제
     */
    private void unLinkKakao() {
        UserManagement.getInstance().requestUnlink(new UnLinkResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                setResult(RESULT_CANCELED);
                finish();
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                requestKakaoLogin();

            }

            @Override
            public void onNotSignedUp() {
                requestKakaoLogin();

            }

            @Override
            public void onSuccess(Long userId) {
                requestKakaoLogin();

            }
        });
    }
}
