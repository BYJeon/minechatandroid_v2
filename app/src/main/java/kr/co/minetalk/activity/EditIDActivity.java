package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ChangeUserLogIDApi;
import kr.co.minetalk.api.ModifyCustomClearanceNumberApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditCustomBinding;
import kr.co.minetalk.databinding.ActivityEditIdBinding;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.Regex;

public class EditIDActivity extends BindActivity<ActivityEditIdBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditIDActivity.class);
        context.startActivity(intent);
    }

    private ChangeUserLogIDApi mChangeIDApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_id;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        mBinding.tvCurId.setText(String.format(getString(R.string.cur_id_title), MineTalkApp.getUserInfoModel().getUser_hp()));
    }

    private void initApi() {
        mChangeIDApi = new ChangeUserLogIDApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditIDActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditIDActivity.this);

                if(baseModel.getCode().equals("0000")){
                    String id = mBinding.etId.getText().toString().trim();
                    MineTalkApp.getUserInfoModel().setUser_login_id(id);
                    Toast.makeText(getApplicationContext(), getString(R.string.success_id_reset), Toast.LENGTH_LONG).show();
                    setResult(RESULT_OK);
                    finish();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditIDActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = mBinding.etId.getText().toString().trim();
                if(id == null || id.length() == 0 || !Regex.validateID(id)) {
                    showMessageAlert(getResources().getString(R.string.id_format_wrong_warning));
                    return;
                }

                mChangeIDApi.execute(id);
            }
        });
    }


}
