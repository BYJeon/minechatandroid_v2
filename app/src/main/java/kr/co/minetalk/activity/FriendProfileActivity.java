package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;


import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.HashMap;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.FriendBlockGridAdapter;
import kr.co.minetalk.adapter.FriendBlockListAdapter;
import kr.co.minetalk.adapter.listener.OnFriendBlockAdapterListener;
import kr.co.minetalk.api.AddFriendApi;
import kr.co.minetalk.api.ListFriendApi;
import kr.co.minetalk.api.ListNoticeApi;
import kr.co.minetalk.api.SetRejectFriendApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.FriendListResponse;
import kr.co.minetalk.api.model.NoticeResponse;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.ActivityFriendProfileBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.listener.OnProfileListener;
import kr.co.minetalk.ui.popup.BottomProfilePopup;
import kr.co.minetalk.utils.ProgressUtil;

public class FriendProfileActivity extends BindActivity<ActivityFriendProfileBinding> {
    public static enum FriendType {
        FRIEND,
        FAVORITE,
        CONTACT,
        FRIEND_OTHER
    }

    private FriendType mFriendType = FriendType.FRIEND;
    private FriendListModel mData;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, FriendProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, final FriendListModel model, FriendType type) {
        int friendType = 0;
        if(type == FriendType.FAVORITE)
            friendType = 1;
        else if(type == FriendType.CONTACT)
            friendType = 2;
        else if(type == FriendType.FRIEND_OTHER)
            friendType = 3;

        Intent intent = new Intent(context, FriendProfileActivity.class);
        intent.putExtra("friendListModel", model);
        intent.putExtra("friendType", friendType);
        context.startActivity(intent);
    }

    private ListFriendApi mListFriendApi;
    private SetRejectFriendApi mSetRejectFriendApi;

    private FriendBlockListAdapter mListAdapter;
    private FriendBlockGridAdapter mGridAdapter;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_friend_profile;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        Intent intent = getIntent();
        FriendListModel model = (FriendListModel)intent.getSerializableExtra("friendListModel");
        int type = intent.getIntExtra("friendType", 0);
        String titleText;
        if(type == 0 || model.isIs_friend() ) {
            mFriendType = FriendType.FRIEND;
            titleText = getString(R.string.friend_fragment_text_friend);
        }
        else if(type == 1) {
            mFriendType = FriendType.FAVORITE;
            titleText = getString(R.string.friend_fragment_text_favorite_friend);
        }
        else if(type == 2){
            mFriendType = FriendType.CONTACT;
            titleText = getString(R.string.friend_fragment_text_contact);
        }else{
            mFriendType = FriendType.FRIEND_OTHER;
            titleText = getString(R.string.friend_fragment_text_friend);
        }

        mBinding.tvActivityTitle.setText(titleText);

        setData(model);
        setUIEventListener();
        updateTheme();
    }

    @Override
    protected void onResume() {
        super.onResume();

        HashMap<String, String> nickNameMap = FriendManager.getInstance().selectFriendNickNameMap();
        if(mData != null && !mData.getUser_hp().equals("") && nickNameMap.containsKey(mData.getUser_hp())){
            mBinding.tvName.setText(nickNameMap.get(mData.getUser_hp()));
        }
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.tvName.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvPhoneNum.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvStatusMe.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnBack.setImageResource(R.drawable.btn_close_wh);
        }else{
            mBinding.tvName.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvPhoneNum.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvStatusMe.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_close);
        }
    }

    private void setData(FriendListModel data){
        this.mData = data;

        if(mFriendType == FriendType.FRIEND) {
            mBinding.layoutFriend.setVisibility(View.VISIBLE);
            mBinding.layoutNoFriend.setVisibility(View.INVISIBLE);
            mBinding.layoutFavorite.setVisibility(View.INVISIBLE);
            mBinding.layoutContact.setVisibility(View.INVISIBLE);
        } else if(mFriendType == FriendType.FAVORITE){
            mBinding.layoutFriend.setVisibility(View.VISIBLE);
            mBinding.layoutNoFriend.setVisibility(View.INVISIBLE);
            mBinding.layoutFavorite.setVisibility(View.INVISIBLE);
            mBinding.layoutContact.setVisibility(View.INVISIBLE);

        } else if(mFriendType == FriendType.CONTACT) {
            mBinding.layoutContact.setVisibility(View.VISIBLE);
            mBinding.layoutFavorite.setVisibility(View.INVISIBLE);
            mBinding.layoutFriend.setVisibility(View.INVISIBLE);
            mBinding.layoutNoFriend.setVisibility(View.INVISIBLE);
        }else if(mFriendType == FriendType.FRIEND_OTHER){
            mBinding.layoutFavorite.setVisibility(View.INVISIBLE);
            mBinding.layoutFriend.setVisibility(View.INVISIBLE);
            mBinding.layoutContact.setVisibility(View.INVISIBLE);
            mBinding.layoutNoFriend.setVisibility(View.VISIBLE);
        }

        //mBinding.tvName.setText(mData.getUser_name());
        if(mData != null && !mData.getUser_hp().equals(""))
            mBinding.tvName.setText(MineTalkApp.getUserNameByPhoneNum(mData.getUser_name(), mData.getUser_hp()));

        if(mData.getUser_hp() == null || mData.getUser_hp().equals(""))
            mBinding.tvName.setText(mData.getUser_name());

        String profileImgUrl = mData.getUser_profile_image();
        if(profileImgUrl == null || profileImgUrl.equals("")) {
            Glide.with(this).load(R.drawable.img_basic).into(mBinding.ivUserImage);
        } else {
            Glide.with(this).load(profileImgUrl).into(mBinding.ivUserImage);
        }

        //즐겨찾기 여부
        if(mData.getFavorite_yn().equals("Y")) {
            mBinding.tvButtonFavorite.setText("즐겨찾기 해제");
        } else {
            mBinding.tvButtonFavorite.setText("즐겨찾기 추가");
        }

        mBinding.tvStatusMe.setText(mData.getUser_state_message());
        mBinding.tvPhoneNum.setText(mData.getUser_hp());
    }
    private void initApi() {
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //친구 추가
        mBinding.tvButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFriendRequest(mData.getUser_xid());
            }
        });

        //친구 아닌 대상과 1:1 채팅
        mBinding.tvButtonChatOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnProfileListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnProfileListner();
                if(profileListener != null) {
                    profileListener.onTalk(mData);
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });

        //친구 아닌 대상 포인트 선물
        mBinding.tvButtonPresentOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnProfileListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnProfileListner();
                if(profileListener != null) {
                    profileListener.onGift(mData);
                }
            }
        });

        mBinding.ivUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoViewerActivity.startActivity(getApplicationContext(), mData.getUser_profile_image(), mData.getUser_name(), mData.getUser_state_message(), "");
            }
        });

        mBinding.tvUserPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mData.getUser_hp()));
                startActivity(intent);
            }
        });

        mBinding.tvButtonChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnProfileListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnProfileListner();
                if(profileListener != null) {
                    profileListener.onTalk(mData);
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });

        mBinding.tvButtonPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnProfileListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnProfileListner();
                if(profileListener != null) {
                    profileListener.onGift(mData);
                }
            }
        });


        mBinding.tvButtonFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String favo = mData.getFavorite_yn().equals("Y") ? "N" : "Y";
                OnProfileListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnProfileListner();
                if(profileListener != null) {
                    profileListener.onFavorite(mData, favo);
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });

        mBinding.tvButtonBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMessageAlert(String.format(getResources().getString(R.string.popup_friend_block_message), mData.getUser_name()),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    OnProfileListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnProfileListner();
                                    if(profileListener != null) {
                                        profileListener.onBlock(mData);
                                        setResult(RESULT_CANCELED);
                                        finish();
                                    }
                                }
                            }
                        });


            }
        });

        //즐겨찾기 친구 즐겨찾기 해제
        mBinding.tvButtonFavoriteN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnProfileListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnProfileListner();
                if(profileListener != null) {
                    profileListener.onFavorite(mData, "N");
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });

        //즐겨찾기 친구 차단
        mBinding.tvButtonBlockFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnProfileListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnProfileListner();
                if(profileListener != null) {
                    profileListener.onBlock(mData);
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });

        //연락처
        mBinding.tvContactPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mData.getUser_hp()));
                startActivity(intent);
            }
        });

        mBinding.tvSendSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSMS(getApplicationContext(), mData.getUser_hp(), "");
            }
        });

        mBinding.tvSendInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sendUserName = MineTalkApp.getUserInfoModel().getUser_name();
                String sendUserHp = MineTalkApp.getUserInfoModel().getUser_hp();
                String msg = String.format(getResources().getString(R.string.invite_sms_msg),
                        sendUserName,
                        sendUserHp);

                sendSMS(getApplicationContext(), mData.getUser_hp(), msg);
            }
        });

        //닉네임 변경
        mBinding.tvButtonEditNickName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditNickNameActivity.startActivity(MineTalkApp.getCurrentActivity(), mData.getUser_hp(), mBinding.tvName.getText().toString());
            }
        });
    }


    private void sendSMS(Context context, String phone, String msg) {
        try {
            Uri smsUri = Uri.parse("sms:" + phone);
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, smsUri);
            sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            sendIntent.putExtra("sms_body", msg);
            context.startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void blockListRequest() {
        mListFriendApi.execute("reject");
    }

    private void blockLiftRequest(String user_xid, String reject_yn) {
        mSetRejectFriendApi.execute(user_xid, reject_yn);
    }

    private void setupData(FriendListResponse response) {
//        ArrayList<FriendListModel> arrayList = new ArrayList<>();
//        //테스트를 위한 친구 리스트 추가
//        for(int i = 0; i < 20 ; i++) {
//
//            FriendListModel item = new FriendListModel();
//            item.setUser_xid("test");
//            item.setUser_hp("010-4444-8888");
//            item.setUser_name("테스트");
//            item.setUser_state_message("상태 메세지 테스트");
//            item.setUser_profile_image(null);
//
//            arrayList.add(item);
//        }
//
//        mListAdapter.setItem(arrayList);
//        mGridAdapter.setItem(arrayList);
//
//        String countMessage = String.format("차단친구 %d명", arrayList.size());
//        mBinding.tvBlockCount.setText(countMessage);
//
//        if(response != null) {
//            Preferences.setBlockFriendListType(!Preferences.getBlockFriendListType());
//            boolean listType = Preferences.getBlockFriendListType();
//
//            if(listType) { //GridType
//                mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
//                mBinding.recyclerView.setAdapter(mGridAdapter);
//                Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.icon_top_grid_on).into(mBinding.ivListType);
//            }
//
//            //String countMessage = String.format("차단친구 %d명", response.getFriend_list().size());
//            //mBinding.tvBlockCount.setText(countMessage);
//            //mAdapter.setItem(response.getFriend_list());
//
//        }
    }

    private OnFriendBlockAdapterListener mFriendBlockAdapterListener = new OnFriendBlockAdapterListener() {
        @Override
        public void onBlockLift(String user_xid) {
            blockLiftRequest(user_xid, "N");
        }

        @Override
        public void onClickUser(String user_xid) {

        }
    };

}
