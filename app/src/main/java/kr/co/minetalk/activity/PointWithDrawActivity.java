package kr.co.minetalk.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.CheckWalletAddrApi;
import kr.co.minetalk.api.ListTokensApi;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.TransferTokenApi;
import kr.co.minetalk.api.UserExchangeInfoApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.NumberCodeData;
import kr.co.minetalk.api.model.TokenListModel;
import kr.co.minetalk.api.model.TokenListResponse;
import kr.co.minetalk.api.model.UserExchangeInfoResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.api.model.WalletModel;
import kr.co.minetalk.databinding.ActivityPointExchangeBinding;
import kr.co.minetalk.databinding.ActivityPointWithdrawBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.FAQItemView;
import kr.co.minetalk.view.WalletInfoView;

public class PointWithDrawActivity extends BindActivity<ActivityPointWithdrawBinding> {
    private final int PERMISSION = 1;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PointWithDrawActivity.class);
        context.startActivity(intent);
    }

    private ListTokensApi mListTokensApi;
    private UserExchangeInfoApi mUserExchangeTokenApi;
    private RequestSmsApi mRequestSmsApi;
    private TransferTokenApi mTransferTokenApi;
    private CheckWalletAddrApi mCheckWalletApi;

    private Map<String, TokenListModel> mMapListToken = new HashMap<>();
    private Map<String, TokenListModel> mMapUserListToken = new HashMap<>();
    private ArrayAdapter<String> mCoinAdapter;
    private String mCurCoinSymbol = "";
    private String mCurCoinFee = "";//토큰 수수료
    private boolean isChangedCoinTextColor = false;

    private String mNationCode = "82";
    private int mNationCodePositionIndex = 0;
    private long totalCoinAmount = 0;

    private long mDayMaxLimitAmount = 0;
    private long mRequestAmount = 0;
    private long mUserTokenAmount = 0;

    private boolean isKeyboardShowing = false;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            tokenListRequest();
        }

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_point_withdraw;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
        tokenListRequest();


        //checkPermission();

        /**************************
         * Nation Code initialize
         **************************/
        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            mBinding.tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            mBinding.tvNationCode.setText("한국 " + "+82");
            mNationCode = "82";
        }
        mNationCodePositionIndex = 0;
        /**************************/

        setUserPhoneNum();

        initWithDrawAmount();
    }

    private void initApi() {
        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointWithDrawActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(PointWithDrawActivity.this);
                if(baseModel != null) {
                    showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointWithDrawActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mListTokensApi = new ListTokensApi(this, new ApiBase.ApiCallBack<TokenListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointWithDrawActivity.this);
            }

            @Override
            public void onSuccess(int request_code, TokenListResponse response) {
                ProgressUtil.hideProgress(PointWithDrawActivity.this);
                if(response != null) {
                    setCoinInfo(response);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointWithDrawActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mUserExchangeTokenApi = new UserExchangeInfoApi(this, new ApiBase.ApiCallBack<UserExchangeInfoResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointWithDrawActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserExchangeInfoResponse response) {
                ProgressUtil.hideProgress(PointWithDrawActivity.this);
                if(response != null) {
                    setUserCoinInfo(response);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointWithDrawActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mTransferTokenApi = new TransferTokenApi(MineTalkApp.getCurrentActivity(), new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MineTalkApp.getCurrentActivity());
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                if(userInfoBaseModel != null) {
                    MineTalkApp.setUserInfoModel(userInfoBaseModel);
                    initWithDrawAmount();
                    showMessageAlert(getResources().getString(R.string.withdraw_wallet_success));
                    //Toast.makeText(MineTalkApp.getCurrentActivity(), "출금이 완료 되었습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                if(request_code == 1001)
                    mBinding.tvAuthConfirmInfo.setVisibility(View.VISIBLE);
                //Toast.makeText(MineTalkApp.getCurrentActivity(), message, Toast.LENGTH_SHORT).show();
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckWalletApi = new CheckWalletAddrApi(MineTalkApp.getCurrentActivity(), new ApiBase.ApiCallBack<WalletModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MineTalkApp.getCurrentActivity());
            }

            @Override
            public void onSuccess(int request_code, WalletModel walletModel) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                mBinding.btnConfirm.setEnabled(true);
                if(walletModel != null) {
                    //내 지갑 주소일 경우
                    if(walletModel.getMy_wallet_addr().equals("Y")){
                        //showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
                        showMessageAlert(getResources().getString(R.string.withdraw_wallet_allert));
                        return;
                    }

                    //마인토큰 주소가 아닐 경우
                    if(walletModel.getMinechat_wallet_addr_yn().equals("N")){
                        //showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
                        showMessageAlert(getResources().getString(R.string.withdraw_wallet_mine_allert));
                        return;
                    }

                    showMessageAlert(getResources().getString(R.string.withdraw_wallet_confirm),
                            getResources().getString(R.string.common_ok),
                            getResources().getString(R.string.common_cancel),
                            new PopupListenerFactory.SimplePopupListener() {
                                @Override
                                public void onClick(DialogInterface f, int state) {
                                    if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                        String authNumber = mBinding.etAuthNumber.getText().toString();
                                        String walletAddr = mBinding.etWalletAddr.getText().toString();
                                        transferRequest(authNumber, Long.toString(mRequestAmount), walletAddr);
                                    }
                                }
                            });
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                mBinding.btnConfirm.setEnabled(true);
                if(request_code == 1001)
                    mBinding.tvAuthConfirmInfo.setVisibility(View.VISIBLE);
                Toast.makeText(MineTalkApp.getCurrentActivity(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void transferRequest(String auth_number, String amount_token, String user_wallet_address) {
        //휴대폰 이메일 인증이 안되었을 경우 출금 금지
        if(!MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("Y") || !MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("Y")){
            showMessageAlert(getString(R.string.request_phone_email_certi_warning));
            return;
        }
        mTransferTokenApi.execute(auth_number, amount_token, user_wallet_address);
    }

    private void smsRequest() {
        String phoneNumber = mBinding.etPhoneNumber.getText().toString();
        if(phoneNumber == null || phoneNumber.replace(" ", "").equals("")) {
            showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
            return;
        }

        mRequestSmsApi.execute(MineTalk.SMS_TYPE_EXCHANGE, mNationCode, phoneNumber);
    }

    // 전화번호 국가 코드 선택 Listener
    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
        @Override
        public void onClick(int state, int num) {
            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                mNationCodePositionIndex = num;
                try {
                    String viewName = NumberCode.getIntance().getDataList().get(num).getName();
                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");

                    mBinding.tvNationCode.setText(viewName);
                } catch (Exception e) {
                    e.printStackTrace();
                    mNationCode = "82";
                }
            }
        }

        @Override
        public void onClick(int state, int num, int position) {

        }
    };

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.snCoinType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextSize(CommonUtils.convertDpToPixel(5.0f, getApplicationContext()));
                textView.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.white_color));
                changeSpinnerArrowColor(MineTalk.isBlackTheme ? R.color.main_text_color : R.color.white_color);

                String selCoin = mCoinAdapter.getItem(position);
                if(mMapListToken.containsKey(selCoin)){
                    TokenListModel model = mMapListToken.get(selCoin);
                    mCurCoinSymbol = model.getUser_token_symble();
                    requestUserExchangeInfo();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.snExchangeType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextSize(CommonUtils.convertDpToPixel(5.0f, getApplicationContext()));
                textView.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.white_color));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        
        mBinding.layoutNationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String[] array = NumberCode.getIntance().getmTitleArray();
                    showBottomWheelPopup(array, mNationCodePositionIndex, mNationCodePickerListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mBinding.btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.tvAuthConfirmInfo.setVisibility(View.INVISIBLE);
                smsRequest();
            }
        });

        //붙혀넣기
        mBinding.btnPasteWalletAddr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String clipBoard = MineTalk.getClipBoardLink(MineTalkApp.getAppContext());
                if(clipBoard != null && clipBoard.length() > 0) {
                    mBinding.etWalletAddr.setText(clipBoard);
                    isAllDataFillOut();
                }

            }
        });

        mBinding.etWithdrawNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    mBinding.etWithdrawNumber.setText("");
                    initWithDrawAmount();
                }
            }
        });
        mBinding.etWithdrawNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        setWithDrawAmount();
                        mBinding.etWithdrawNumber.clearFocus();
                        // 검색 동작
                        break;
                    default:
                        setWithDrawAmount();
                        mBinding.etWithdrawNumber.clearFocus();
                        // 기본 엔터키 동작
                        return false;
                }
                return true;
            }
        });

        //출금신청하기
        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String authNumber = mBinding.etAuthNumber.getText().toString();
                if(authNumber == null || authNumber.replace(" ", "").equals("")) {
                    showMessageAlert("인증번호를 입력해 주세요.");
                    return;
                }

                String walletAddr = mBinding.etWalletAddr.getText().toString();
                if(walletAddr == null || walletAddr.replace(" ", "").equals("") || !isCorrectWalletAddress(walletAddr.trim()) ) {
                    showMessageAlert("지갑 주소를 확인해 주세요.");
                    return;
                }

                mBinding.btnConfirm.setEnabled(false);
                mCheckWalletApi.execute(walletAddr);
                //transferRequest(authNumber, Long.toString(mRequestAmount), walletAddr);
            }
        });

        //바로가기
        mBinding.btnGoExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentWeb = new Intent(Intent.ACTION_VIEW, Uri.parse("https://neobit.unionex.market"));
                startActivity(intentWeb);
            }
        });


        //키보드 Show/Hide 이벤트 감지
        mBinding.layoutRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                mBinding.layoutRoot.getWindowVisibleDisplayFrame(r);
                int screenHeight = mBinding.layoutRoot.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d(TAG, "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    if (!isKeyboardShowing) {
                        isKeyboardShowing = true;
                        onKeyboardVisibilityChanged(true);
                    }
                }
                else {
                    // keyboard is closed
                    if (isKeyboardShowing) {
                        isKeyboardShowing = false;
                        onKeyboardVisibilityChanged(false);
                    }
                }
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void changeSpinnerArrowColor(int color){
        mBinding.snCoinType.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
        mBinding.snExchangeType.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
    }

    void onKeyboardVisibilityChanged(boolean opened) {
        Log.d(TAG, "keyboard " + opened);
        isAllDataFillOut();
    }

    private void initWithDrawAmount(){
        totalCoinAmount = 0;

        //mDayMaxLimitAmount = 0;
        mRequestAmount = 0;
        //mUserTokenAmount = 0;

        String possibleToken = (mDayMaxLimitAmount - mRequestAmount) >= mUserTokenAmount
                ? Long.toString((mDayMaxLimitAmount - mRequestAmount)) : Long.toString(mUserTokenAmount);
        mBinding.tvUserMineToken.setText(mCurCoinSymbol + " " + CommonUtils.comma_won(possibleToken));

        mBinding.etWithdrawNumber.setText("");
        mBinding.tvFeeUserMineToken.setText("");
        mBinding.tvTotalUserMineToken.setText("");
    }

    private void setWithDrawAmount(){
        totalCoinAmount = 0;
        mRequestAmount = mBinding.etWithdrawNumber.getText().toString().length() == 0 ?
                         0 : Long.parseLong(mBinding.etWithdrawNumber.getText().toString());

        Long orgAmount = mRequestAmount;
        Long feeAmount = Long.parseLong(mCurCoinFee);
        Long totalFee  = (orgAmount * feeAmount) / 100;
        totalCoinAmount = orgAmount + totalFee;

        String possibleToken = (mDayMaxLimitAmount - totalCoinAmount) >= mUserTokenAmount
                ? Long.toString((mDayMaxLimitAmount - totalCoinAmount)) : Long.toString(mUserTokenAmount);
        mBinding.tvUserMineToken.setText(mCurCoinSymbol + " " + CommonUtils.comma_won(possibleToken));


        mBinding.etWithdrawNumber.setText(mCurCoinSymbol + " " + CommonUtils.comma_won(Long.toString(orgAmount)));
        mBinding.tvFeeUserMineToken.setText(mCurCoinSymbol + " " + CommonUtils.comma_won(Long.toString(totalFee)));
        mBinding.tvTotalUserMineToken.setText(mCurCoinSymbol + " " + CommonUtils.comma_won(Long.toString(totalCoinAmount)));

        isAllDataFillOut();
    }

    private void tokenListRequest() {
        mListTokensApi.execute();
    }

    private void setCoinInfo(TokenListResponse response) {
        mMapListToken.clear();
        mMapUserListToken.clear();

        ArrayList<String> coinList = new ArrayList<>();
        ArrayList<String> exchangeList = new ArrayList<>();

        for(TokenListModel models :  response.getToken_list() ) {
            String tokenName = models.getUser_token_symble() + " (" + models.getUser_token_name() + ")";
            mMapListToken.put(tokenName, models);

            if(models.getUser_token_symble().toLowerCase().equals("mctk"))
                coinList.add(0, tokenName);
            else
                coinList.add(tokenName);
            exchangeList.add(models.getUser_token_name());
        }

        //임시 하드 코딩
        coinList.add("MCTK (MineToken)");
        exchangeList.add("네오비트 iex");

        mCoinAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, coinList);
        mCoinAdapter.setDropDownViewResource(R.layout.spinner_text_color);
        mBinding.snCoinType.setAdapter(mCoinAdapter);

        ArrayAdapter<String> exchangeAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, exchangeList);
        mBinding.snExchangeType.setAdapter(exchangeAdapter);

        int nCnt = 0;
        mBinding.layoutWalletContainer.removeAllViews();
        for(TokenListModel models :  response.getUser_token_list() ) {
            mMapUserListToken.put(models.getUser_token_symble(), models);

            WalletInfoView infoView = new WalletInfoView(this);
            infoView.setData(models, nCnt%2!=0 ? FAQItemView.ItemType.ITEM_TYPE_EVEN : FAQItemView.ItemType.ITEM_TYPE_ODD);
            mBinding.layoutWalletContainer.addView(infoView);
            if(nCnt == 0)
                mBinding.tvWalletAddr.setText(models.getUser_token_address());
            ++nCnt;
        }



        int idx = coinList.get(0).indexOf("(");
        mCurCoinSymbol = coinList.get(0).substring(0, idx-1).trim();

        requestUserExchangeInfo();
    }

    private boolean isCorrectWalletAddress(String walletAddr){
        int len = walletAddr.length();
        if(walletAddr.length() != 42) return false;

        String preFix = walletAddr.substring(0, 2);
        if(!preFix.toLowerCase().equals("0x")) return  false;
        walletAddr = walletAddr.substring(2);
        //매칭시킬 정규식 패턴을 생성
        Pattern p = Pattern.compile("^[0-9A-Fa-f]+$");
        Matcher m = p.matcher(walletAddr);

        boolean result = m.find();
        return result;
    }

    private void setUserCoinInfo(UserExchangeInfoResponse response) {
        String tokenMin = response.getUser_mine_token();
        //사용자 토큰
        mUserTokenAmount = Long.parseLong(tokenMin);

        if(tokenMin.length() > 0)
            tokenMin = CommonUtils.comma_won(tokenMin);

        String monthLimit = response.getExchange_month_limit();
        if(monthLimit.length() > 0)
            monthLimit = CommonUtils.comma_won(monthLimit);

        String dayLimit = response.getExchange_day_limit();
        if(dayLimit.length() > 0)
            dayLimit = CommonUtils.comma_won(dayLimit);

        String depositAmount = response.getTotal_deposit_amount();
        if(depositAmount.length() > 0)
            depositAmount = CommonUtils.comma_won(depositAmount);

        /***
         * 해당 부분 공식입니다.
         * 출금신청내의 출금가능은
         * A = 일일최대수량 - 당일신청수량
         * B = 일반토큰(신청인)
         * A >= B 이면 출금가능 수량은 A
         * A < B 이면 출금가능 수량은 B
         ***/

        mDayMaxLimitAmount = Long.parseLong(response.getExchange_day_max_limit());
        mRequestAmount     = Long.parseLong(mBinding.etWithdrawNumber.getText().toString().length() == 0 ?
                                            "0" : mBinding.etWithdrawNumber.getText().toString() );

        String possibleToken = (mDayMaxLimitAmount - mRequestAmount) >= mUserTokenAmount
                             ? Long.toString((mDayMaxLimitAmount - mRequestAmount)) : Long.toString(mUserTokenAmount);
        mBinding.tvUserMineToken.setText(mCurCoinSymbol + " " + CommonUtils.comma_won(possibleToken));

        mCurCoinFee = response.getExchange_token_fee_per();
    }

    private void requestUserExchangeInfo(){
        if(mMapUserListToken.containsKey(mCurCoinSymbol)){

            TokenListModel coinType = mMapListToken.get(mCurCoinSymbol);
            TokenListModel userCoinType = mMapUserListToken.get(mCurCoinSymbol);

            //임시 하드 코딩
           mUserExchangeTokenApi.execute("7", "2");
        }

        mBinding.tvUserMineToken.setText(mCurCoinSymbol + " 0");
        //mBinding.tvMonthlyLimit.setText(mCurCoinSymbol + " 0");
        //mBinding.tvDailyLimit.setText(mCurCoinSymbol + " 0");
        //mBinding.tvInputTotal.setText(mCurCoinSymbol + " 0");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        setUserPhoneNum();
                    }catch (SecurityException e){
                        e.printStackTrace();
                    }

                } else {
                    return;
                }
        }
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION);
            }else{
                setUserPhoneNum();
            }
        }
    }

    private void setUserPhoneNum(){
        String phoneNum = "";
        try {
            phoneNum = MineTalkApp.getUserInfoModel().getUser_hp();

            try {
                ArrayList<NumberCodeData> array = NumberCode.getIntance().getDataList();
                for( NumberCodeData data : array ) {
                    if(phoneNum.contains(data.getNumCode())) {
                        phoneNum = phoneNum.replace(data.getNumCode(), "0");
                        break;
                    }
                }

            } catch (Exception e) {
                phoneNum = "";
            }

        }catch (SecurityException e){
            e.printStackTrace();
            return;
        }


        if(phoneNum != null && !phoneNum.equals("")) {
            mBinding.etPhoneNumber.setText(phoneNum);
            //mBinding.etPhoneNumber.setBackgroundResource(R.drawable.button_bg_dcdcdc);
            mBinding.etPhoneNumber.setEnabled(false);

            //mBinding.layoutNationCode.setBackgroundResource(R.drawable.button_bg_dcdcdc);
            mBinding.layoutNationCode.setEnabled(false);
        }
    }

    //모든 데이터가 기입되어 있는지 확
    private void isAllDataFillOut(){
        if( mBinding.etPhoneNumber.getText().length() != 0 && mBinding.etWalletAddr.getText().length() != 0 &&
                mBinding.etAuthNumber.getText().length() != 0 && mBinding.etWithdrawNumber.getText().length() != 0 &&
                mBinding.etWithdrawNumber.getText().toString().toLowerCase().contains("mctk")){
            mBinding.btnConfirm.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
            mBinding.btnConfirm.setEnabled(true);
        }else{
            mBinding.btnConfirm.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);
            mBinding.btnConfirm.setEnabled(false);
        }
    }

    @RequiresPermission(Manifest.permission.READ_PHONE_STATE)
    public static String getPhoneNumber(Context context){

        String phoneNumber = "";

        TelephonyManager mgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {

            String tmpPhoneNumber = mgr.getLine1Number();
            ArrayList<NumberCodeData> array = NumberCode.getIntance().getDataList();
            for( NumberCodeData data : array ) {
                if(tmpPhoneNumber.contains(data.getNumCode())) {
                    phoneNumber = tmpPhoneNumber.replace(data.getNumCode(), "0");
                    break;
                }
            }

        } catch (Exception e) {
            phoneNumber = "";
        }

        return phoneNumber;

    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutSpinnerRoot.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.layoutSpinnerExchange.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.layoutCoinType.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvCoinTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutUserMineToken.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvUserMinTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserMineToken.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.btnGoExchange.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnPasteWalletAddr.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.btnAuth.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnConfirm.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutCoinTitle.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvCoinListTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvCoinAddrTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonWalletAddr.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonWalletAddrSub.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvWalletAddr.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvWalletAddrTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutUserPossibleMineToken.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvUserPossibleTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonRequestToken.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvUserRequestMineTokenTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutUserMineTokenFee.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvFeeUserMinTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvFeeUserMineToken.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonTotalUserMineToken.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTotalUserMineTokenTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTotalUserMineToken.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvCoinInfoTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvExchangeInfoTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvAuthTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvWithdrawTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));

            mBinding.btnGoExchange.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnPasteWalletAddr.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnAuth.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutAuthRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutAuthSubRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.viewAuthInfo.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));
            mBinding.viewCoinAddr.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.tvAuthInfoTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.viewMineTokenFee.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));
            mBinding.viewPossibleMineToken.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));
            mBinding.viewRequestToken.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.tvNationCode.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.etPhoneNumber.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.etPhoneNumber.setTextColor(getResources().getColor(R.color.white_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutSpinnerRoot.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.layoutSpinnerExchange.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.layoutCoinType.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvCoinTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutUserMineToken.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserMinTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUserMineToken.setTextColor(getResources().getColor(R.color.main_text_color));


            mBinding.btnGoExchange.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnPasteWalletAddr.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.btnAuth.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnConfirm.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutCoinTitle.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvCoinListTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvCoinAddrTitle.setTextColor(getResources().getColor(R.color.main_text_color));


            mBinding.layoutButtonWalletAddr.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonWalletAddrSub.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvWalletAddr.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvWalletAddrTitle.setTextColor(getResources().getColor(R.color.main_text_color));


            mBinding.layoutUserPossibleMineToken.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserPossibleTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutButtonRequestToken.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserRequestMineTokenTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutUserMineTokenFee.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvFeeUserMinTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvFeeUserMineToken.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutButtonTotalUserMineToken.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTotalUserMineTokenTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTotalUserMineToken.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvCoinInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvExchangeInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAuthTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvWithdrawTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnGoExchange.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnPasteWalletAddr.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnAuth.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutAuthRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutAuthSubRoot.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.viewAuthInfo.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
            mBinding.viewCoinAddr.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.tvAuthInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.viewMineTokenFee.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
            mBinding.viewPossibleMineToken.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
            mBinding.viewRequestToken.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.tvNationCode.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.etPhoneNumber.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.etPhoneNumber.setTextColor(getResources().getColor(R.color.main_text_color));
        }

        isAllDataFillOut();
    }
}
