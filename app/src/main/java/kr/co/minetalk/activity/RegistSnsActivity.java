package kr.co.minetalk.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.SmsMessage;
import android.view.View;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.api.CheckRecommendApi;
import kr.co.minetalk.api.RegistApi;
import kr.co.minetalk.api.RegistV2Api;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.RecommendCheckModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityRegistBinding;
import kr.co.minetalk.databinding.ActivityRegistSnsBinding;
import kr.co.minetalk.repository.RegistRepository;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.Regex;
import kr.co.minetalk.utils.Sha256Util;

public class RegistSnsActivity extends BindActivity<ActivityRegistSnsBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, RegistSnsActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int request_code) {
        Intent intent = new Intent(context, RegistSnsActivity.class);
        ((Activity)context).startActivityForResult(intent, request_code);
    }

    private final int PERMISSON = 1;

    private RegistV2Api mRegistV2Api;
    private RequestSmsApi mRequestSmsApi;
    private CheckRecommendApi mRecommendCheckApi;

    private RegistRepository mRegistRepository;
    private String mRecommendKeyword = "";

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
//        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
//        MainActivity.startActivity(RegistActivity.this);
//        finish();

        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
        syncFriendList();
    }

    private OnSyncListener mFriendSyncListner = new OnSyncListener() {
        @Override
        public void onSyncComplete() {
            try {
                MineTalkApp.clearActivity();
            } catch (Exception e) {
                e.printStackTrace();
            }

            MainActivity.startActivity(RegistSnsActivity.this);
            finish();
        }
    };

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }



    @Override
    protected int getLayoutId() {
        return R.layout.activity_regist_sns;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        mRegistRepository = RegistRepository.getInstance();

        String snsTitle = "";
        //구글 로그인
        if(mRegistRepository.getRegistType().equals(MineTalk.REGIST_TYPE_G)){
            snsTitle = String.format(getResources().getString(R.string.regist_sns_select_content), getResources().getString(R.string.sns_google));
            mBinding.ibGoogle.setImageResource(R.drawable.btn_check_on);
        }else if(mRegistRepository.getRegistType().equals(MineTalk.REGIST_TYPE_F)){//페이스북 로그
            snsTitle = String.format(getResources().getString(R.string.regist_sns_select_content), getResources().getString(R.string.sns_facebook));
            mBinding.ibFacebook.setImageResource(R.drawable.btn_check_on);
        }else{//카카오톡 로그인
            snsTitle = String.format(getResources().getString(R.string.regist_sns_select_content), getResources().getString(R.string.sns_kakao));
            mBinding.ibKakao.setImageResource(R.drawable.btn_check_on);
        }

        mBinding.tvSnsDesc.setText(snsTitle);
        initApi();
        setUIEventListener();

        mSyncListener = mFriendSyncListner;


        /*if(Build.VERSION.SDK_INT >= 23) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS}, PERMISSON);
            } else {
                initSMSCatchReceiver();
            }

        }*/


        String recommandCode = Preferences.getREcommandUserCode();
        if(!recommandCode.equals("")) {
            mRecommendKeyword = recommandCode;
            mBinding.etRecommandUser.setText(recommandCode);
            //mBinding.etRecommandUser.setEnabled(false);
            //mBinding.etRecommandUser.setTextColor(Color.parseColor("#9b9b9b"));
            //mBinding.btnRecommendConfirm.setEnabled(false);
        } else {
            mBinding.etRecommandUser.setText("");
            //mBinding.etRecommandUser.setEnabled(true);
            //mBinding.etRecommandUser.setTextColor(Color.parseColor("#4a4a4a"));
            //mBinding.btnRecommendConfirm.setEnabled(true);
        }

    }

    private void initApi() {
        mRegistV2Api = new RegistV2Api(this, new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistSnsActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(RegistSnsActivity.this);

                if(userInfoBaseModel != null) {
                    Preferences.setRecommandUserCode("");
                    loginRequest("", userInfoBaseModel.getUser_provider_id(), "", mRegistRepository.getProviderType());
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistSnsActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistSnsActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(RegistSnsActivity.this);
                if(baseModel != null) {
                    showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistSnsActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRecommendCheckApi = new CheckRecommendApi(this, new ApiBase.ApiCallBack<RecommendCheckModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistSnsActivity.this);
            }

            @Override
            public void onSuccess(int request_code, RecommendCheckModel baseModel) {
                ProgressUtil.hideProgress(RegistSnsActivity.this);
                if(baseModel != null) {
                    //추천인이 검색되었을 경우
                    if(baseModel.getUser_login_id().length() > 0 || baseModel.getUser_hp().length() > 0 || baseModel.getUser_nation_code().length() > 0)
                        mBinding.etRecommandUser.setText(String.format("%s(+%s%s)", baseModel.getUser_login_id(), baseModel.getUser_nation_code(), baseModel.getUser_hp()));
                    else {
                        mBinding.etRecommandUser.setText("");
                        mRecommendKeyword = "";
                        showMessageAlert(getResources().getString(R.string.recommend_no_exist_waring));
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistSnsActivity.this);
                mRecommendKeyword = "";
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.cbMinechatAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.tvMinechatAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.bk_main_text_nor_color));
                isAllAgreeChecked();
                isAgreeChecked();
            }
        });

        mBinding.cbPrivacyAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.tvPrivacyAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.bk_main_text_nor_color));
                isAllAgreeChecked();
                isAgreeChecked();
            }
        });

        mBinding.cbOperationAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.tvOperationAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.bk_main_text_nor_color));
                isAllAgreeChecked();
                isAgreeChecked();
            }
        });

        mBinding.cbContentsAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.tvContentsAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.bk_main_text_nor_color));
                isAllAgreeChecked();
                isAgreeChecked();
            }
        });

        mBinding.cbAllAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.cbMinechatAgree.setSelected(isSelect);
                mBinding.cbPrivacyAgree.setSelected(isSelect);
                mBinding.cbOperationAgree.setSelected(isSelect);
                mBinding.cbContentsAgree.setSelected(isSelect);

                mBinding.tvMinechatAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                mBinding.tvPrivacyAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                mBinding.tvOperationAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                mBinding.tvContentsAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                mBinding.tvAllAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                isAgreeChecked();
                isAllAgreeChecked();
            }
        });

        mBinding.btnRecommendConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecommendKeyword = mBinding.etRecommandUser.getText().toString().trim();

                if(mRecommendKeyword == null || mRecommendKeyword.length() == 0){
                    showMessageAlert(getResources().getString(R.string.recommend_format_wrong_warning));
                    return;
                }

                mRecommendCheckApi.execute(mRecommendKeyword);
            }
        });

        mBinding.btnRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snsTypeRegistReqeust();
            }
        });

    }


    private void isAgreeChecked(){
        if(mBinding.cbContentsAgree.isSelected() && mBinding.cbOperationAgree.isSelected() &&
                mBinding.cbPrivacyAgree.isSelected() && mBinding.cbMinechatAgree.isSelected()){

            mBinding.cbAllAgree.setSelected(true);

            mBinding.btnRegist.setEnabled(true);
            mBinding.btnRegist.setBackgroundResource(R.drawable.button_bg_16c066);
        }else{
            mBinding.cbAllAgree.setSelected(false);
            mBinding.btnRegist.setEnabled(false);
            mBinding.btnRegist.setBackgroundResource(R.drawable.button_bg_4d16c066);
        }

        mBinding.tvAllAgree.setTextColor(getResources().getColor(mBinding.cbAllAgree.isSelected() ? R.color.app_point_color : R.color.main_text_color));
    }

    private void isAllAgreeChecked(){
        if(mBinding.cbContentsAgree.isSelected() && mBinding.cbOperationAgree.isSelected() &&
           mBinding.cbPrivacyAgree.isSelected() && mBinding.cbMinechatAgree.isSelected()){
            mBinding.btnRegist.setEnabled(true);
            mBinding.btnRegist.setBackgroundResource(R.drawable.button_bg_16c066);
        }else{
            mBinding.btnRegist.setEnabled(false);
            mBinding.btnRegist.setBackgroundResource(R.drawable.button_bg_4d16c066);
        }
    }

    private void snsTypeRegistReqeust() {
        String registType      = "S";
        String userName        = mRegistRepository.getUserName();
        String userEmail       = mRegistRepository.getUserEmail();
        String userRecommend    = mRecommendKeyword;
        String userProviderType = mRegistRepository.getProviderType();

        String userProviderId   = mRegistRepository.getProviderId();
        String userProfileImage = mRegistRepository.getUserProfileImageUrl();
        String userPlatform     = MineTalk.getUserPlatform();



        mRegistV2Api.execute(registType, userName, userEmail, userRecommend, userProviderType, userProviderId, userProfileImage, userPlatform);
    }

//    private void smsRequest() {
//        String phoneNumber = mBinding.etPhoneNumber.getText().toString();
//        if(phoneNumber == null || phoneNumber.replace(" ", "").equals("")) {
//            showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
//            return;
//        }
//        mRequestSmsApi.execute(MineTalk.SMS_TYPE_REGIST, mNationCode, phoneNumber);
//    }
//
//    private void phoneTypeReigstReqeust() {
//
//        String registType      = mRegistRepository.getRegistType();
//        String userName        = mBinding.etName.getText().toString();
//        String userPass        = mBinding.etPassword.getText().toString();
//        String userPassConfirm = mBinding.etPasswordConfirm.getText().toString();
//        String userNationCode  = mNationCode;
//
//        String userPhoneNumber  = mBinding.etPhoneNumber.getText().toString();
//        String authNumber       = mBinding.etAuthNumber.getText().toString();
//        String userRecommend    = mBinding.etRecommandUser.getText().toString();
//        String userProviderType = "";
//
//        String userProviderId   = "";
//        String userProfileImage = "";
//        String userPlatform     = MineTalk.getUserPlatform();
//
//        mRegistRepository.setUserPassword(userPass);
//
//
//        if(userName == null || userName.equals("")) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_user_name));
//            return;
//        }
//
//        if(userPass == null || userPass.equals("")) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_password));
//            return;
//        }
//
//        if(!Regex.validatePassword(userPass)) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_password) + getResources().getString(R.string.validation_edit_error_1));
//            return;
//        }
//
//        if(userPassConfirm == null || userPassConfirm.equals("")) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_password));
//            return;
//        }
//
//        if(!userPass.equals(userPassConfirm)) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_password));
//            return;
//        }
//
//
//        if(userPhoneNumber == null || userPhoneNumber.equals("")) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_phone_number));
//            return;
//        }
//
//        if(!Regex.validatePhone(userPhoneNumber)) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_phone_number));
//            return;
//        }
//
//
//        if(authNumber == null || authNumber.equals("")) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_auth_number));
//            return;
//        }
//
//
//        if(userRecommend != null && !userRecommend.equals("")) {
//            if(!Regex.validatePhone(userRecommend)) {
//                showMessageAlert(getString(R.string.regist_hint_recommand_user));
//                return;
//            }
//        }
//
//
//        Preferences.setUserPassword(userPass);
//
//        try {
//            String sha256Pass = Sha256Util.encoding(userPass);
//            mRegistApi.execute(registType, userName, sha256Pass, userNationCode, userPhoneNumber, authNumber, userRecommend, userProviderType, userProviderId, userProfileImage, userPlatform);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void snsTypeRegistReqeust() {
//        String registType      = mRegistRepository.getRegistType();
//        String userName        = mRegistRepository.getUserName();
//        String userPass        = "";
//        String userNationCode  = mNationCode;
//
//        String userPhoneNumber  = mBinding.etPhoneNumber.getText().toString();
//        String authNumber       = mBinding.etAuthNumber.getText().toString();
//        String userRecommend    = mBinding.etRecommandUser.getText().toString();
//        String userProviderType = mRegistRepository.getProviderType();
//
//        String userProviderId   = mRegistRepository.getProviderId();
//        String userProfileImage = mRegistRepository.getUserProfileImageUrl();
//        String userPlatform     = MineTalk.getUserPlatform();
//
//        if(userPhoneNumber == null || userPhoneNumber.equals("")) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_phone_number));
//            return;
//        }
//
//        if(authNumber == null || authNumber.equals("")) {
//            showMessageAlert(getResources().getString(R.string.regist_validation_auth_number));
//            return;
//        }
//
//        mRegistApi.execute(registType, userName, userPass, userNationCode, userPhoneNumber, authNumber, userRecommend, userProviderType, userProviderId, userProfileImage, userPlatform);
//    }
//
//    // 전화번호 국가 코드 선택 Listener
//    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
//        @Override
//        public void onClick(int state, int num) {
//            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
//                mNationCodePositionIndex = num;
//                try {
//                    String viewName = NumberCode.getIntance().getDataList().get(num).getName();
//                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");
//
//                    mBinding.tvNationCode.setText(viewName);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    mNationCode = "82";
//                }
//            }
//        }
//
//        @Override
//        public void onClick(int state, int num, int position) {
//
//        }
//    };
}
