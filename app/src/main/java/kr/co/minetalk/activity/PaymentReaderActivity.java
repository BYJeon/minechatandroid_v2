package kr.co.minetalk.activity;

import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;

public class PaymentReaderActivity extends BindActivity {
    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }
}
