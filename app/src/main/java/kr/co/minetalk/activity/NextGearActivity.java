package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ApplyNextGearApi;
import kr.co.minetalk.api.CheckSmsApi;
import kr.co.minetalk.api.NextGearInfoApi;
import kr.co.minetalk.api.PointInfoApi;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.NextGearInfoResponse;
import kr.co.minetalk.api.model.NumberCodeData;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityNextGearBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class NextGearActivity extends BindActivity<ActivityNextGearBinding> {

    private static final String COLUMN_TOTAL_CASH = "column_total_cash";

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, NextGearActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, long totalCash) {
        Intent intent = new Intent(context, NextGearActivity.class);
        intent.putExtra(COLUMN_TOTAL_CASH, totalCash);
        context.startActivity(intent);
    }

    private static final int SEARCH_ADDRESS_ACTIVITY = 10000;
    private static final int NEXT_GEAR_NEED_CASH = 697000;

    private ApplyNextGearApi mApplyNextGearApi;
    private RequestSmsApi mRequestSmsApi;
    private CheckSmsApi mCheckSmsApi;
    private NextGearInfoApi mNextGearInfoApi;
    private PointInfoApi mPointInfoApi;

    private String mNationCode = "82";
    private String mphoneNum   = "";
    private String mPostCode = "";
    private long mTotalCash = 0;

    private int mRemainTime = 180;
    private Timer mTimer = null;

    private boolean mIsPhoneCertify = false;
    private boolean mIsModiMode = false;
    private boolean mIsStatusFinished = false;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            //setupUserInfo();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_next_gear;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        if (getIntent() != null && getIntent().getExtras() != null) {
            mTotalCash = getIntent().getExtras().getLong(COLUMN_TOTAL_CASH);
        }

        /**************************
         * Nation Code initialize
         **************************/
        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");
        } catch (Exception e) {
            e.printStackTrace();
            mNationCode = "82";
        }
        /**************************/

        mNextGearInfoApi.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            //mPointInfoApi.execute();
            setUserInfo();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initApi() {
//        mPointInfoApi = new PointInfoApi(this, new ApiBase.ApiCallBack<PointInfoResponse>() {
//            @Override
//            public void onPreparation() {
//                try {
//                    ProgressUtil.showProgress(NextGearActivity.this);
//                }catch (IllegalArgumentException e){
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onSuccess(int request_code, PointInfoResponse response) {
//                try {
//                    ProgressUtil.hideProgress(NextGearActivity.this);
//                }catch (IllegalArgumentException e){
//                    e.printStackTrace();
//                }
//
//                if(response != null) {
//                    mTotalCash = Integer.parseInt(response.getUser_mine_cash());
//                }
//            }
//
//            @Override
//            public void onFailure(int request_code, String message) {
//
//            }
//
//            @Override
//            public void onCancellation() {
//
//            }
//        });

        mApplyNextGearApi = new ApplyNextGearApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(NextGearActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(NextGearActivity.this);
                if(baseModel != null && baseModel.getCode().equals("0000")) {
                    String message = mIsModiMode ? getResources().getString(R.string.next_gear_modify_complete) : getResources().getString(R.string.next_gear_buy_complete);
                    showMessageAlert(message, getResources().getString(R.string.common_close),
                            new PopupListenerFactory.SimplePopupListener() {
                                @Override
                                public void onClick(DialogInterface f, int state) {
                                    if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                        finish();
                                    }
                                }
                            });
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(NextGearActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(NextGearActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(NextGearActivity.this);
                if(baseModel != null && baseModel.getCode().equals("0000")) {

                    mBinding.btnSendAuth.setEnabled(true);
                    //mBinding.btnSendAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
                    mBinding.btnSendAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
                    //인증 번호 발송 성공
                    showMessageAlert(getResources().getString(R.string.phone_send_auth_success));
                    mBinding.tvRemainTime.setVisibility(View.VISIBLE);
                    mBinding.tvAuthComplete.setVisibility(View.INVISIBLE);
                    TimerStop();

                    mTimer = new Timer();
                    mTimer.schedule(new CustomTimer(), 1000, 1000);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                mBinding.tvAuthComplete.setVisibility(View.INVISIBLE);
                TimerStop();
                ProgressUtil.hideProgress(NextGearActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckSmsApi = new CheckSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(NextGearActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(NextGearActivity.this);
                if (baseModel != null) {
                    mIsPhoneCertify = true;
                    String message = "";
                    if(mIsModiMode)
                        message = getResources().getString(R.string.next_gear_modify_auth_match_desc);
                    else
                        message = getResources().getString(R.string.next_gear_auth_match_desc);

                    //인증 성공
                    showMessageAlert(message);
                    mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                    mBinding.tvAuthComplete.setVisibility(View.VISIBLE);
                    TimerStop();


                    mBinding.btnConfirm.setBackgroundColor(MineTalk.isBlackTheme ? Color.parseColor("#ffd000") : Color.parseColor("#16c066"));
                    mBinding.btnConfirm.setEnabled(true);
                    checkAllInfoFillOut();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mIsPhoneCertify = false;
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                TimerStop();
                ProgressUtil.hideProgress(NextGearActivity.this);
                showMessageAlert(message);

                mBinding.btnConfirm.setBackgroundColor(MineTalk.isBlackTheme ? Color.parseColor("#4dffd000") : Color.parseColor("#4d16c066"));
                mBinding.btnConfirm.setEnabled(false);
            }

            @Override
            public void onCancellation() {

            }
        });

        mNextGearInfoApi = new NextGearInfoApi(this, new ApiBase.ApiCallBack<NextGearInfoResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(NextGearActivity.this);
            }

            @Override
            public void onSuccess(int request_code, NextGearInfoResponse response) {
                ProgressUtil.hideProgress(NextGearActivity.this);
                if (response != null) {
                    mBinding.tvUserId.setText(response.getUser_login_id());
                    mBinding.tvUserNickname.setText(response.getUser_name());
                    mBinding.tvUserCoupon.setText(response.getCoupon_name());

                    //if(!response.getZipcode().equals(""))
                    if(!response.getZipcode().isEmpty()) {
                        String zipCode = response.getZipcode();
                        int index = zipCode.indexOf("(");
                        if( index != -1){
                            zipCode = zipCode.substring(index+1);
                            index = zipCode.indexOf(")");
                            if(index != -1) {
                                zipCode = zipCode.substring(0, index);
                            }
                        }
                        mPostCode = zipCode;
                        mBinding.tvUserPostcode.setText("(" + zipCode + ")");
                    }
                        //mBinding.tvUserPostcode.setText( "("+ response.getZipcode() + ")");
                    //if(!response.getAddr().equals(""))
                    mBinding.tvUserAddr.setText(response.getAddr());
                    //if(!response.getAddr_detail().equals(""))
                    mBinding.etUserAddrDetail.setText(response.getAddr_detail());
                    //mBinding.tvCustomNum.setText(response.getCustoms_clearance_number());
                    mBinding.etUserPartner.setText(response.getAffiliate_code());

                    //신청이 완료 상태인지 체크
                    mIsStatusFinished = !response.getStatus().toLowerCase().equals("c");
                    setRequestStatus(mIsStatusFinished);

                    int userNumber = Integer.parseInt(response.getUser_number());
                    int systemNumber = Integer.parseInt(response.getSystem_number());

                    //systemNumber = 2;
                    //신청 차수와 현재 차수가 같을 경우만 수정하게 한다.
                    setRequestStatus(userNumber == systemNumber);

                    boolean isPossible = false;
                    if(systemNumber > userNumber){
                        //신청자 중 구매 가능 쿠폰 유/무에 따른 처리.
                        try {
                            int minCouponPrice = Integer.parseInt(response.getMinimum_coupon_name().split(" ")[2]);
                            int userCouponPrice = Integer.parseInt(response.getCoupon_name().split(" ")[2]);

                            isPossible = userCouponPrice >= minCouponPrice && mTotalCash >= NEXT_GEAR_NEED_CASH;
                            setRequestStatus(isPossible);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    String expireData = response.getExpire_date();
                    try {
                        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(expireData);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy년도 MM월 dd일");
                        expireData = sdf.format(date);
                    }catch (ParseException e){
                        e.printStackTrace();
                    }
                    if(!response.getAddr().equals("") && !response.getAddr_detail().equals("") && !isPossible){
                        mIsModiMode = true;
                        mBinding.tvNextGearSubInfo1Title.setText(getResources().getString(R.string.next_gear_modify_desc_1));
                        mBinding.tvNextGearSubInfo2Title.setText(String.format(getString(R.string.next_gear_modify_desc_2), expireData));
                        mBinding.btnConfirm.setText(getResources().getString(R.string.modify_title));
                    }else{
                        mIsModiMode = false;
                        mBinding.tvNextGearSubInfo1Title.setText(getResources().getString(R.string.next_gear_desc_1));
                        mBinding.tvNextGearSubInfo2Title.setText(String.format(getString(R.string.next_gear_desc_2), expireData));
                        mBinding.btnConfirm.setText(getResources().getString(R.string.request_title));
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(NextGearActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });


        mBinding.etUserAddrDetail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllInfoFillOut();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.etUserPartner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllInfoFillOut();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //사용자 주소 찾기
        mBinding.btnUserPostcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NextGearActivity.this, FindAddrActivity.class);
                startActivityForResult(i, SEARCH_ADDRESS_ACTIVITY);

                //FindAddrActivity.startActivity(NextGearActivity.this);
            }
        });

        //통관번호 찾기
        mBinding.btnChangeCustomNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditCustomNumActivity.startActivity(NextGearActivity.this);
            }
        });

        //인증번호 발급받기
        mBinding.btnAuthNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);


                if(mphoneNum.equals("")){
                    showMessageAlert(getString(R.string.phone_input_empty_warning));
                    return;
                }

                mRequestSmsApi.execute(MineTalk.SMS_TYPE_NEXTGEAR, mNationCode, mphoneNum);
            }
        });

        //인증하기
        mBinding.btnSendAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String authNum = mBinding.etAuthNumber.getText().toString().trim();

                if (authNum.equals("")) {
                    showMessageAlert(getString(R.string.phone_empty_auth_num));
                    return;
                }

                mCheckSmsApi.execute(MineTalk.SMS_TYPE_NEXTGEAR, mNationCode, mphoneNum, authNum);
            }
        });

        //구매 신청하기.
        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = mIsModiMode ? getResources().getString(R.string.next_gear_modify_warning) : getResources().getString(R.string.next_gear_buy_warning);

                showMessageAlert(message,
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    String zipCode = mPostCode;
                                    String addr = mBinding.tvUserAddr.getText().toString();
                                    String addrDetail = mBinding.etUserAddrDetail.getText().toString();
                                    String customClearanceNum = mBinding.tvCustomNum.getText().toString();
                                    String affliateCode = mBinding.etUserPartner.getText().toString();

                                    mApplyNextGearApi.execute(zipCode, addr, addrDetail, customClearanceNum, affliateCode);
                                }
                            }
                        });
            }
        });
    }


    private void TimerStop(){
        if(mTimer != null)
            mTimer.cancel();

        mRemainTime = 180;
    }

    private void setUserPhoneNum(){
        try {
            mphoneNum = MineTalkApp.getUserInfoModel().getUser_hp();

            if(mphoneNum == null) return;
            try {
                ArrayList<NumberCodeData> array = NumberCode.getIntance().getDataList();
                for( NumberCodeData data : array ) {
                    if(mphoneNum.contains(data.getNumCode())) {
                        mphoneNum = mphoneNum.replace(data.getNumCode(), "0");
                        break;
                    }
                }

            } catch (Exception e) {
                mphoneNum = "";
            }

        }catch (SecurityException e){
            e.printStackTrace();
            return;
        }


        if(mphoneNum != null && !mphoneNum.equals("")) {
            mBinding.tvPhoneNum.setText( "(+" + mNationCode + ") " + mphoneNum);
            mBinding.tvPhoneNum.setEnabled(false);
        }
    }

    private void setUserInfo(){
        setUserPhoneNum();

        String customClearanceNum = Preferences.getCustomNumber(MineTalkApp.getUserInfoModel().getUser_xid());
        if(customClearanceNum.equals("")){
            mBinding.tvCustomNum.setText("");
            mBinding.btnChangeCustomNum.setText(getResources().getString(R.string.btn_add_regist_title));
        }else {
            mBinding.tvCustomNum.setText(customClearanceNum);
            mBinding.btnChangeCustomNum.setText(getResources().getString(R.string.bottom_button_confirm));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode){

            case SEARCH_ADDRESS_ACTIVITY:

                if(resultCode == RESULT_OK){
                    String extras = data.getExtras().getString("data");
                    if (extras != null){
                        String orgAddr = extras;

                        int postCodeIdx = extras.indexOf(",");
                        if(postCodeIdx > 0){
                            mPostCode = extras.substring(0, postCodeIdx);
                            orgAddr = extras.substring(postCodeIdx+1).trim();
                        }

                        if(!mPostCode.isEmpty()) {
                            mBinding.tvUserPostcode.setText("(" + mPostCode + ")");
                        }
                        mBinding.tvUserAddr.setText(orgAddr);

                        checkAllInfoFillOut();
                    }


                }
                break;

        }
    }

    private void setRequestStatus(boolean possible) {
        mBinding.btnUserPostcode.setEnabled(possible);
        mBinding.btnChangeCustomNum.setEnabled(possible);
        mBinding.btnAuthNum.setEnabled(possible);

        mBinding.etUserAddrDetail.setEnabled(possible);
        mBinding.etUserPartner.setEnabled(possible);
        mBinding.etAuthNumber.setEnabled(possible);

        mBinding.btnUserPostcode.setBackgroundResource(possible ? MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066 :
                MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);

        mBinding.btnChangeCustomNum.setBackgroundResource(possible ? MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066 :
                MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);

        mBinding.btnAuthNum.setBackgroundResource(possible ? MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066 :
                MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);
    }

    private void checkAllInfoFillOut(){
        String detailAddr = mBinding.etUserAddrDetail.getText().toString();
        String customNum  = mBinding.tvCustomNum.getText().toString();
        String partnerNum = mBinding.etUserPartner.getText().toString();
        String addr = mBinding.tvUserAddr.getText().toString();


        if(/*mTotalCash < NEXT_GEAR_NEED_CASH ||*/addr.isEmpty() || detailAddr.isEmpty() || customNum.isEmpty() || partnerNum.isEmpty() || !mIsPhoneCertify){
            mBinding.btnConfirm.setEnabled(false);
            mBinding.btnConfirm.setBackgroundColor(MineTalk.isBlackTheme ?  Color.parseColor("#4dffd000") : Color.parseColor("#4d16c066"));
        }else{
            mBinding.btnConfirm.setEnabled(true);
            mBinding.btnConfirm.setBackgroundColor(MineTalk.isBlackTheme ?  Color.parseColor("#ffd000") : Color.parseColor("#16c066"));
        }
    }
    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.layoutButtonId.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvIdTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserId.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonNickname.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvNicknameTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserNickname.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonCoupon.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvCouponTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserCoupon.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutUserPostcode.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvPostcodeTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnUserPostcode.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnUserPostcode.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUserPostcode.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutUserAddr.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvUserAddr.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutUserAddrDetail.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.etUserAddrDetail.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutCustoms.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvCustomTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvCustomNum.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnChangeCustomNum.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnChangeCustomNum.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutPartner.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvPartnerTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.etUserPartner.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvUserInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvRecommedInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));

            mBinding.layoutPhoneNum.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvPhoneTitle.setTextColor(getResources().getColor(R.color.white));
            mBinding.tvPhoneNum.setTextColor(getResources().getColor(R.color.white));
            mBinding.btnAuthNum.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnAuthNum.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutAuthNum.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvAuthTitle.setTextColor(getResources().getColor(R.color.white));
            mBinding.etAuthNumber.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnSendAuth.setBackgroundResource(R.drawable.button_bg_4dcdb60c);

            mBinding.tvNextgearInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvNextGearInfo1Title.setTextColor(getResources().getColor(R.color.white));
            mBinding.tvNextGearSubInfo1Title.setTextColor(getResources().getColor(R.color.white));
            mBinding.tvNextGearInfo2Title.setTextColor(getResources().getColor(R.color.white));
            mBinding.tvNextGearSubInfo2Title.setTextColor(getResources().getColor(R.color.white));

        }else{
//            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
//            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
//
//            mBinding.tvIdTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvUserId.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvNicknameTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvUserNickname.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvNicknameTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvUserNickname.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvPasswordTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvSnsTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvSnsId.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvRecommendTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvRecommendUser.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvAddInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvNameTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.etUserName.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvStatusTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.etStateMessage.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvBirthTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvUserBirthDay.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvGenderTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            //mBinding.tvUserGender.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvJobTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            //mBinding.tvUserJob.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvAddrTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvUserAddress.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvCustomTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvCustomNum.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            //추가 색상
//            mBinding.tvUserInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvAddInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvExtraInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.layoutButtonRegType.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.viewRegType.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
//
//            mBinding.layoutButtonId.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.viewId.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
//
//            mBinding.layoutUserPassword.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.btnUserPassword.setBackgroundResource(R.drawable.button_bg_16c066);
//            mBinding.btnUserPassword.setTextColor(getResources().getColor(R.color.white_color));
//
//            mBinding.btnChangeRecommend.setBackgroundResource(R.drawable.button_bg_16c066);
//            mBinding.btnChangeRecommend.setTextColor(getResources().getColor(R.color.white_color));
//
//            mBinding.btnEditId.setBackgroundResource(R.drawable.button_bg_16c066);
//            mBinding.btnEditId.setTextColor(getResources().getColor(R.color.white_color));
//
//            mBinding.viewPassword.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
//
//            mBinding.layoutRecommend.setBackgroundColor(getResources().getColor(R.color.white_color));
//
//            mBinding.layoutUserName.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.viewName.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
//
//            mBinding.layoutStateMessage.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.viewStatusMessage.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
//
//            mBinding.layoutUserBirthDay.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.btnChangeBirth.setBackgroundResource(R.drawable.button_bg_16c066);
//            mBinding.btnChangeBirth.setTextColor(getResources().getColor(R.color.white_color));
//            mBinding.viewBirth.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
//
//            mBinding.layoutUserGender.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.viewGender.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
//
//            mBinding.layoutUserAddress.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.btnChangeAddress.setBackgroundResource(R.drawable.button_bg_16c066);
//            mBinding.btnChangeAddress.setTextColor(getResources().getColor(R.color.white_color));
//            mBinding.viewAddress.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
//
//            mBinding.layoutUserJob.setBackgroundColor(getResources().getColor(R.color.white_color));
//
//            mBinding.layoutCustoms.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.btnChangeCustomNum.setBackgroundResource(R.drawable.button_bg_16c066);
//            mBinding.btnChangeCustomNum.setTextColor(getResources().getColor(R.color.white_color));
//
//            //탈퇴하기
//            mBinding.tvWithdrawInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvAccountTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvAccountStatus.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.layoutAccount.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.btnUserWithdraw.setBackgroundResource(R.drawable.button_bg_16c066);
//            mBinding.btnUserWithdraw.setTextColor(getResources().getColor(R.color.white_color));
//
//            mBinding.layoutPhoneAuthRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.layoutEmailAuthRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
//
//            mBinding.btnPhoneAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);
//            mBinding.btnPhoneAuth.setTextColor(getResources().getColor(R.color.white_color));
//
//            if( MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("") || MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("N") ){
//                mBinding.btnPhoneAuth.setBackgroundResource(R.drawable.button_bg_16c066);
//                mBinding.btnPhoneAuth.setEnabled(true);
//            }
//
//            mBinding.btnEmailAuth.setBackgroundResource(R.drawable.button_bg_4d16c066);
//            mBinding.btnEmailAuth.setTextColor(getResources().getColor(R.color.white_color));
//            if( MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("") || MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("N") ){
//                mBinding.btnEmailAuth.setBackgroundResource(R.drawable.button_bg_16c066);
//                mBinding.btnEmailAuth.setEnabled(true);
//            }
//
//            mBinding.tvPhoneTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.etPhoneNumber.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.etPhoneNumber.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.etEmail.setBackgroundColor(getResources().getColor(R.color.white_color));
//            mBinding.etEmail.setTextColor(getResources().getColor(R.color.main_text_color));
//
//            mBinding.tvRecommedInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvPhoneAuthInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
//            mBinding.tvEmailAuthInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
        }
    }

    // 첫 번째 TimerTask 를 이용한 방법
    class CustomTimer extends TimerTask {
        @Override
        public void run() {
            mRemainTime -= 1;

            if(mRemainTime == 0){
                mIsPhoneCertify = false;
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                mBinding.tvAuthComplete.setVisibility(View.INVISIBLE);
                showMessageAlert(getResources().getString(R.string.phone_time_over_auth_num));
                TimerStop();
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBinding.tvRemainTime.setText(String.format("%02d:%02d", mRemainTime / 60 , mRemainTime % 60) );
                }
            });
        }
    }
}
