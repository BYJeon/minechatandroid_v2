package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ExchangeInfoApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.ExchangeInfoModel;
import kr.co.minetalk.databinding.ActivityExchangeBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.popup.SimpleExchangeAuthPopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;

public class ExchangeActivity extends BindActivity<ActivityExchangeBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ExchangeActivity.class);
        context.startActivity(intent);
    }

    private ExchangeInfoApi mExchangeInfoApi;
    private long mMinLimit = 0;

    private boolean mUseFlag = false;
    private String mAlertMessage = "";


    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exchange;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();


        exchangeInfoRequest();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        mBinding.ivRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exchangeInfoRequest();
            }
        });

        mBinding.tvButtonRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mUseFlag) {
                    showMessageAlert(mAlertMessage);
                    return;
                }

                String exchangeToken = mBinding.etInputToken.getText().toString();
                String userWalletAddress = mBinding.etInputWalletAddress.getText().toString();

                if(TextUtils.isEmpty(userWalletAddress)) {
                    showMessageAlert("전자지갑 주소를 입력해주세요.");
                    return;
                }

                if(!exchangeToken.equals("") && !exchangeToken.equals("0")) {
                    if(mMinLimit == 0) {
                        showExchangeAuthPopup(exchangeToken, userWalletAddress);
                    } else {
                        if(Integer.parseInt(exchangeToken) >= mMinLimit) {
                            showExchangeAuthPopup(exchangeToken, userWalletAddress);
                        } else {
                            showMessageAlert(getResources().getString(R.string.exchange_validation_min_limit));
                        }
                    }
                } else {
                    showMessageAlert(getResources().getString(R.string.exchange_validation_token));
                }

            }
        });
    }

    private void initApi() {
        mExchangeInfoApi = new ExchangeInfoApi(this, new ApiBase.ApiCallBack<ExchangeInfoModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ExchangeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, ExchangeInfoModel exchangeInfoModel) {
                ProgressUtil.hideProgress(ExchangeActivity.this);
                if(exchangeInfoModel != null) {
                    setData(exchangeInfoModel);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(ExchangeActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void exchangeInfoRequest() {
        mExchangeInfoApi.execute();
    }

    private void setData(ExchangeInfoModel data) {
        if(data != null) {
            String enable_token = getResources().getString(R.string.exchange_enable_sum) + " " + CommonUtils.comma_won(data.getUser_mine_token());
            String today_limit = getResources().getString(R.string.exchange_today_limit) + " " + CommonUtils.comma_won(data.getExchange_mine_token_day_limit());
            String min_limit = getResources().getString(R.string.exchange_min_limit) + " " + CommonUtils.comma_won(data.getExchange_mine_token_min_request());

            if(data.getExchange_mine_token_min_request() == null || data.getExchange_mine_token_min_request().equals("")) {
                mMinLimit = 0;
            } else {
                mMinLimit = Long.parseLong(data.getExchange_mine_token_min_request());
            }

            mBinding.tvEnableTokenAmount.setText(enable_token);
            mBinding.tvTodayLimitToken.setText(today_limit);
            mBinding.tvMinLimit.setText(min_limit);

            mUseFlag = data.getUse_flag().toUpperCase().equals("Y") ? true : false;
            mAlertMessage = data.getAlert_message();

            if(!TextUtils.isEmpty(data.getUser_wallet_addr())) {
                mBinding.etInputWalletAddress.setText(data.getUser_wallet_addr());
            }
        }
    }


    private void showExchangeAuthPopup(String exchangeToken, String userWalletAddress) {

        SimpleExchangeAuthPopup popup = new SimpleExchangeAuthPopup();
        popup.setUserWalletAddress(userWalletAddress);
        popup.setSmsType(MineTalk.SMS_TYPE_EXCHANGE);
        popup.setExchangeTokenValue(exchangeToken);
        popup.setEventListener(new SimpleExchangeAuthPopup.OnEventListener() {
            @Override
            public void onComplete() {
                showMessageAlert(getResources().getString(R.string.exchange_complete_msg), new PopupListenerFactory.SimplePopupListener() {
                    @Override
                    public void onClick(DialogInterface f, int state) {
                        mBinding.etInputToken.setText("");
                        exchangeInfoRequest();

                    }
                });
            }
        });
        popup.show(getSupportFragmentManager(), "exchange_auth_popup");
    }


    @Override
    public void updateTheme() {
        super.updateTheme();
    }
}
