package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ListMineTokenPointApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CashModel;
import kr.co.minetalk.api.model.MineTokenHistoryModel;
import kr.co.minetalk.api.model.MineTokenHistoryResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityPointTokenGainBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.FAQItemView;
import kr.co.minetalk.view.PointCashItemView;
import kr.co.minetalk.view.PointTokenItemView;

public class PointTokenGainActivity extends BindActivity<ActivityPointTokenGainBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PointTokenGainActivity.class);
        context.startActivity(intent);
    }

    private ListMineTokenPointApi mListTokenPointApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            pointGainHistoryRequest();
        }

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_point_token_gain;
    }

    @Override
    protected void initView() {


        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        mBinding.tvTitle.setText("마인토큰(유예) 보유내역");

        pointGainHistoryRequest();
    }

    @Override
    public void updateTheme() {
        super.updateTheme();
        
        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));


        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }

    private void initApi() {
        mListTokenPointApi = new ListMineTokenPointApi(this, new ApiBase.ApiCallBack<MineTokenHistoryResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointTokenGainActivity.this);
            }

            @Override
            public void onSuccess(int request_code, MineTokenHistoryResponse tokenHistoryResponse) {
                ProgressUtil.hideProgress(PointTokenGainActivity.this);
                if(tokenHistoryResponse != null) {
                    setupData(tokenHistoryResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {

            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

    }


    private void pointGainHistoryRequest() {
        mListTokenPointApi.execute();
    }

    //마인 토큰 보유 내역
    private void setupData(MineTokenHistoryResponse response) {
        PointTokenItemView itemView = new PointTokenItemView(this, PointTokenItemView.ItemType.ITEM_TYPE_TITLE);
        MineTokenHistoryModel model = new MineTokenHistoryModel();
        model.setToken_type("구분");
        model.setEnd_date("내용");
        model.setPoint_amount("수량");
        itemView.setData(model);
        mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());

        PointTokenItemView totalView = new PointTokenItemView(this, PointTokenItemView.ItemType.ITEM_TYPE_TOTAL);
        model = new MineTokenHistoryModel();
        model.setToken_type("");
        model.setEnd_date("마인토큰(유예) 합계");
        model.setPoint_amount( CommonUtils.comma_won(response.getTotal_mine_token_lock_point()));
        totalView.setData(model);
        mBinding.layoutListContainer.addView(totalView, getListItemLayoutParams());

        for(int i = 0 ; i < response.getToken_history().size() ; i++) {
            itemView = new PointTokenItemView(this, (i%2) == 0 ? PointTokenItemView.ItemType.ITEM_TYPE_EVEN : PointTokenItemView.ItemType.ITEM_TYPE_ODD);
            itemView.setData(response.getToken_history().get(i));

            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }

    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        return params;
    }

}
