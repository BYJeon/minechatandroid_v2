package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.bumptech.glide.Glide;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.GiftPointApi;
import kr.co.minetalk.api.PaymentPointApi;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivitySendPointBinding;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.popup.SimpleAuthPopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.ProgressUtil;

public class PointSendActivity extends BindActivity<ActivitySendPointBinding> {


    public static final int TYPE_GIFT_POINT = 1;
    public static final int TYPE_PAY        = 2;

    public static final int MODE_CASH = 11;
    public static final int MODE_TOKEN = 22;


    private static final String POINT_TYPE_MINE_CASH = "mine_cash";
    private static final String POINT_TYPE_MINE_TOKEN = "mine_token";


    public static final String COLUMN_VIEW_TYPE = "view_type";
    public static final String COLUMN_MODE = "mode";
    public static final String COLUMN_FRIEND_XID = "friend_xid";
    public static final String COLUMN_FRIEND_NAME = "friend_name";
    public static final String COLUMN_FRIEND_IMAGE = "friend_image";

    private final int POINT_MIN_LIMIT = 10000;

    private RequestSmsApi mRequestSmsApi;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PointSendActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int viewType, int mode, String friend_xid, String friend_name, String friend_image) {
        Intent intent = new Intent(context, PointSendActivity.class);
        intent.putExtra(COLUMN_VIEW_TYPE, viewType);
        intent.putExtra(COLUMN_MODE, mode);
        intent.putExtra(COLUMN_FRIEND_XID, friend_xid);
        intent.putExtra(COLUMN_FRIEND_NAME, friend_name);
        intent.putExtra(COLUMN_FRIEND_IMAGE, friend_image);
        context.startActivity(intent);
    }

    private int mViewType = TYPE_GIFT_POINT;
    private int mCurrentMode = MODE_CASH;
    private String mFriendXid;
    private String mFriendName;
    private String mFriendImage;

    private GiftPointApi mGiftPointApi;
    private PaymentPointApi mPaymentPointApi;

    private String mCurrentPointType = POINT_TYPE_MINE_CASH;

    private String mNationCode = "82";
    private int mNationCodePositionIndex = 0;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setResult(RESULT_OK);
            finish();
        }

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_send_point;
    }

    @Override
    protected void initView() {
        if(getIntent() != null && getIntent().getExtras() != null) {
            mViewType = getIntent().getExtras().getInt(COLUMN_VIEW_TYPE);
            mCurrentMode = getIntent().getExtras().getInt(COLUMN_MODE);
            mFriendXid = getIntent().getExtras().getString(COLUMN_FRIEND_XID);
            mFriendName = getIntent().getExtras().getString(COLUMN_FRIEND_NAME);
            mFriendImage = getIntent().getExtras().getString(COLUMN_FRIEND_IMAGE);
        }

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        mBinding.tvTitle.setText(mCurrentMode == MODE_CASH ? "마인캐쉬 선물하기" : "마인토큰 선물하기");

        /**************************
         * Nation Code initialize
         **************************/
        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            mBinding.tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            mBinding.tvNationCode.setText("한국 " + "+82");
            mNationCode = "82";
        }
        mNationCodePositionIndex = 0;
        /**************************/

        setupData();
        refreshCashData();
    }

    private void initApi() {
        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointSendActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(PointSendActivity.this);
                if(baseModel != null) {
                    showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointSendActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mGiftPointApi = new GiftPointApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointSendActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                mBinding.btnConfirm.setEnabled(true);
                try {
                    ProgressUtil.hideProgress(PointSendActivity.this);
                    if (baseModel != null) {
                        userInfoRequest();
                    }
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.btnConfirm.setEnabled(true);
                try {
                    ProgressUtil.hideProgress(PointSendActivity.this);
                    showMessageAlert(message);
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancellation() {

            }
        });

        mPaymentPointApi = new PaymentPointApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                try {
                    ProgressUtil.showProgress(PointSendActivity.this);
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {

                mBinding.btnConfirm.setEnabled(true);
                try {
                    ProgressUtil.hideProgress(PointSendActivity.this);
                    if (baseModel != null) {
                        userInfoRequest();
                    }
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.btnConfirm.setEnabled(true);
                try {
                    ProgressUtil.hideProgress(PointSendActivity.this);
                    showMessageAlert(message);
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.tvCheckCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentPointType = POINT_TYPE_MINE_CASH;
                mBinding.tvCheckToken.setSelected(false);
                mBinding.tvCheckCash.setSelected(true);

                mCurrentMode = MODE_CASH;
                refreshCashData();
            }
        });

        mBinding.tvCheckToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentPointType = POINT_TYPE_MINE_TOKEN;
                mBinding.tvCheckToken.setSelected(true);
                mBinding.tvCheckCash.setSelected(false);

                mCurrentMode = MODE_TOKEN;
                refreshCashData();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String amount = mBinding.edCoin.getText().toString();

                if(amount == null || amount.equals("")) {
                    showMessageAlert(getResources().getString(R.string.min_cash_amount_msg));
                    return;
                }

                if( Integer.parseInt(amount) == 0){
                    showMessageAlert(getResources().getString(R.string.min_cash_amount_msg));
                    return;
                }

                String authNumber = "";
                if(mBinding.layoutAuthRoot.getVisibility() != View.GONE) {
                    authNumber = mBinding.etAuthNumber.getText().toString();
                    if (authNumber == null || authNumber.equals("")) {
                        return;
                    }
                }

                if(mViewType == TYPE_GIFT_POINT) {
                    if(mCurrentMode == MODE_CASH) {

                        if(Integer.parseInt(amount) > 10000) {
                            showAuthPopup(mFriendXid, mCurrentPointType, amount);
                        } else {
                            giftPointRequest(mFriendXid, mCurrentPointType, amount, authNumber);
                        }
                    } else if(mCurrentMode == MODE_TOKEN) {
                        /*if(Integer.parseInt(amount) > 1000) {
                            showAuthPopup(mFriendXid, mCurrentPointType, amount);
                        } else {
                            giftPointRequest(mFriendXid, mCurrentPointType, amount, "");
                        }*/
                        giftPointRequest(mFriendXid, mCurrentPointType, amount, authNumber);
                    }


//                    giftPointRequest(mFriendXid, mCurrentPointType, amount);
                } else {
                    paymentRequest(mFriendXid, mCurrentPointType, amount);
                }
            }
        });

        mBinding.layoutNationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String[] array = NumberCode.getIntance().getmTitleArray();
                    showBottomWheelPopup(array, mNationCodePositionIndex, mNationCodePickerListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mBinding.btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smsRequest();
            }
        });

        mBinding.edCoin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    int amount = Integer.parseInt(s.toString());
                    mBinding.layoutAuthRoot.setVisibility(amount > POINT_MIN_LIMIT ? View.VISIBLE : View.GONE);
                }catch (NumberFormatException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void smsRequest() {
        String phoneNumber = mBinding.etPhoneNumber.getText().toString();
        if(phoneNumber == null || phoneNumber.replace(" ", "").equals("")) {
            showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
            return;
        }

        mRequestSmsApi.execute(MineTalk.SMS_TYPE_SENDPOINT, mNationCode, phoneNumber);
    }

    // 전화번호 국가 코드 선택 Listener
    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
        @Override
        public void onClick(int state, int num) {
            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                mNationCodePositionIndex = num;
                try {
                    String viewName = NumberCode.getIntance().getDataList().get(num).getName();
                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");

                    mBinding.tvNationCode.setText(viewName);
                } catch (Exception e) {
                    e.printStackTrace();
                    mNationCode = "82";
                }
            }
        }

        @Override
        public void onClick(int state, int num, int position) {

        }
    };

    private void setupData() {

        String userInfo = String.format(getString(R.string.point_send_activity_to_user), mFriendName);
        mBinding.tvToUserName.setText(userInfo);

        if(mCurrentMode == MODE_CASH) {
            mBinding.tvCheckToken.setSelected(false);
            mBinding.tvCheckCash.setSelected(true);
        } else {
            mBinding.tvCheckToken.setSelected(true);
            mBinding.tvCheckCash.setSelected(false);
        }

        if(mViewType == TYPE_GIFT_POINT) {
            mBinding.btnConfirm.setText(getString(R.string.point_send_button_send));
        } else {
            mBinding.btnConfirm.setText(getString(R.string.point_send_button_pay));
        }

        if(mFriendImage == null || mFriendImage.equals("")) {
            Glide.with(this).load(R.drawable.img_user).into(mBinding.ivUserImage);
        } else {
            Glide.with(this).load(mFriendImage).into(mBinding.ivUserImage);
        }
    }

    private void refreshCashData() {
        if(mCurrentMode == MODE_CASH) {
            mCurrentPointType = POINT_TYPE_MINE_CASH;
            String cash = String.format(getString(R.string.point_send_activity_my_point),
                    CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getUser_mine_cash())
            );
            mBinding.tvUserPoint.setText(cash);
            mBinding.tvUserPoint.setVisibility(View.GONE);
            mBinding.layoutTokenInfo.setVisibility(View.GONE);

            mBinding.layoutButtonInfo1.setVisibility(View.VISIBLE);
            mBinding.layoutButtonInfo2.setVisibility(View.GONE);
            mBinding.layoutButtonInfo3.setVisibility(View.GONE);

            mBinding.tvInfo1Title.setText("잔액 캐쉬");
            mBinding.tvInfo1.setText(CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getUser_mine_cash()));


        } else {
            mCurrentPointType = POINT_TYPE_MINE_TOKEN;

            mBinding.layoutButtonInfo1.setVisibility(View.VISIBLE);
            mBinding.layoutButtonInfo2.setVisibility(View.VISIBLE);
            mBinding.layoutButtonInfo3.setVisibility(View.VISIBLE);

            setupTokenInfoData();

            mBinding.tvUserPoint.setVisibility(View.GONE);
            mBinding.layoutTokenInfo.setVisibility(View.GONE);

        }
    }

    private void giftPointRequest(String friend_xid, String point_type, String amount, String auth_number) {
        //전화번호 인증이 안되어있을 경우 포인트 선물 불가
        if(!MineTalkApp.getUserInfoModel().getUser_hp_confirm().equals("Y")){
            showMessageAlert(getString(R.string.request_phone_certi_warning));
            return;
        }
        mBinding.btnConfirm.setEnabled(false);
        mGiftPointApi.execute(friend_xid, point_type, amount, auth_number);
    }

    private void paymentRequest(String friend_xid, String point_type, String amount) {
        mBinding.btnConfirm.setEnabled(false);
        mPaymentPointApi.execute(friend_xid, point_type, amount);
    }

    private void showAuthPopup(String friend_xid, String point_type, String amount) {
        SimpleAuthPopup simpleAuthPopup = new SimpleAuthPopup();
        simpleAuthPopup.setSmsType(MineTalk.SMS_TYPE_SENDPOINT);
        simpleAuthPopup.setEventListener(new SimpleAuthPopup.OnEventListener() {
            @Override
            public void onConfirm(String authNumber) {
                giftPointRequest(friend_xid, point_type, amount, authNumber);
            }
        });
        simpleAuthPopup.show(getSupportFragmentManager(), "popup_auth");
    }

    private void setupTokenInfoData() {
        String totalToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getTotal_mine_token());
        String nomalToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getUser_mine_token());
        String lockToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getLock_mine_token());

        String unit = getResources().getString(R.string.more_fragment_text_unit_token);

        //mBinding.tvTotal.setText("합산 : " + totalToken + unit);
        //mBinding.tvNormal.setText("일반 : " + nomalToken + unit);
        //mBinding.tvDelay.setText("유예 : " + lockToken + unit);

        mBinding.tvInfo1Title.setText("합산 토큰");
        mBinding.tvInfo2Title.setText("일반 토큰");
        mBinding.tvInfo3Title.setText("유예 토큰");

        mBinding.tvInfo1.setText(totalToken + unit);
        mBinding.tvInfo2.setText(nomalToken + unit);
        mBinding.tvInfo3.setText(lockToken + unit);
    }
}
