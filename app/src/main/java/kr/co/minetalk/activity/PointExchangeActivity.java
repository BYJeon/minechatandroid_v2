package kr.co.minetalk.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ListTokensApi;
import kr.co.minetalk.api.UserExchangeInfoApi;
import kr.co.minetalk.api.model.UserExchangeInfoResponse;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.TokenListModel;
import kr.co.minetalk.api.model.TokenListResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityPointExchangeBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;

public class PointExchangeActivity extends BindActivity<ActivityPointExchangeBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PointExchangeActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flag) {
        Intent intent = new Intent(context, PointExchangeActivity.class);
        intent.addFlags(flag);
        context.startActivity(intent);
    }

    private ListTokensApi mListTokensApi;
    private UserExchangeInfoApi mUserExchangeTokenApi;

    private Map<String, TokenListModel> mMapListToken = new HashMap<>();
    private Map<String, TokenListModel> mMapUserListToken = new HashMap<>();
    private ArrayAdapter<String> mCoinAdapter;
    private String mCurCoinSymbol = "";
    private boolean isChangedCoinTextColor = false;
    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            tokenListRequest();
        }

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_point_exchange;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
        tokenListRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tokenListRequest();
    }

    private void initApi() {
        mListTokensApi = new ListTokensApi(this, new ApiBase.ApiCallBack<TokenListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointExchangeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, TokenListResponse response) {
                ProgressUtil.hideProgress(PointExchangeActivity.this);
                if(response != null) {
                    setCoinInfo(response);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointExchangeActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mUserExchangeTokenApi = new UserExchangeInfoApi(this, new ApiBase.ApiCallBack<UserExchangeInfoResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointExchangeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserExchangeInfoResponse response) {
                ProgressUtil.hideProgress(PointExchangeActivity.this);
                if(response != null) {
                    setUserCoinInfo(response);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointExchangeActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //출금내역
        mBinding.btnWithdrawList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointAmountHistoryActivity.startActivity(PointExchangeActivity.this, MineTalk.POINT_TYPE_WITHDRAW);
            }
        });

        //입금내역
        mBinding.btnDepositList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointAmountHistoryActivity.startActivity(PointExchangeActivity.this, MineTalk.POINT_TYPE_DEPOSIT);
            }
        });

        mBinding.btnRefreshHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCurCoinSymbol.length() != 0 )
                    requestUserExchangeInfo();
            }
        });

        mBinding.snCoinType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextSize(CommonUtils.convertDpToPixel(5.0f, getApplicationContext()));
                textView.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.white_color));
                changeSpinnerArrowColor(MineTalk.isBlackTheme ? R.color.main_text_color : R.color.white_color);

                String selCoin = mCoinAdapter.getItem(position);
                if(mMapListToken.containsKey(selCoin)){
                    TokenListModel model = mMapListToken.get(selCoin);
                    mCurCoinSymbol = model.getUser_token_symble();
                    requestUserExchangeInfo();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //출금신청
        mBinding.btnRequestWithdrawCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointWithDrawActivity.startActivity(PointExchangeActivity.this);
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void changeSpinnerArrowColor(int color){
        mBinding.snCoinType.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
    }

    private void tokenListRequest() {
        mListTokensApi.execute();
    }

    private void setCoinInfo(TokenListResponse response) {
        mMapListToken.clear();
        mMapUserListToken.clear();

        ArrayList<String> coinList = new ArrayList<>();
        for(TokenListModel models :  response.getToken_list() ) {
            String tokenName = models.getUser_token_symble() + " (" + models.getUser_token_name() + ")";
            mMapListToken.put(tokenName, models);

            if(models.getUser_token_symble().toLowerCase().equals("mctk"))
                coinList.add(0, tokenName);
            else
                coinList.add(tokenName);
        }

        //임시 하드 코딩
        coinList.add("MCTK (MineToken)");

        mCoinAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, coinList);
        mCoinAdapter.setDropDownViewResource(R.layout.spinner_text_color);
        mBinding.snCoinType.setAdapter(mCoinAdapter);

        for(TokenListModel models :  response.getUser_token_list() )
            mMapUserListToken.put(models.getUser_token_symble(), models);



        int idx = coinList.get(0).indexOf("(");
        mCurCoinSymbol = coinList.get(0).substring(0, idx-1).trim();

        requestUserExchangeInfo();
    }

    private void setUserCoinInfo(UserExchangeInfoResponse response) {
        String tokenMin = response.getUser_mine_token();
        if(tokenMin.length() > 0)
            tokenMin = CommonUtils.comma_won(tokenMin);

        String monthLimit = response.getExchange_month_limit();
        if(monthLimit.length() > 0)
            monthLimit = CommonUtils.comma_won(monthLimit);

        String dayMinLimit = response.getExchange_day_min_limit();
        if(dayMinLimit.length() > 0)
            dayMinLimit = CommonUtils.comma_won(dayMinLimit);

        String dayMaxLimit = response.getExchange_day_max_limit();
        if(dayMaxLimit.length() > 0)
            dayMaxLimit = CommonUtils.comma_won(dayMaxLimit);

        String depositAmount = response.getTotal_deposit_amount();
        if(depositAmount.length() > 0)
            depositAmount = CommonUtils.comma_won(depositAmount);

        String fee = response.getExchange_token_fee_per();
        String dayCount = response.getExchange_day_count();
        if(dayCount == null || dayCount.length() == 0)
            dayCount = "0";

        mBinding.tvUserMineToken.setText(mCurCoinSymbol + " " + tokenMin);
        mBinding.tvMonthlyLimit.setText(mCurCoinSymbol + " " + monthLimit);
        mBinding.tvDailyLimit.setText(mCurCoinSymbol + " " + dayMinLimit);
        mBinding.tvDailyMaxLimit.setText(mCurCoinSymbol + " " + dayMaxLimit);

        mBinding.tvInputTotal.setText(mCurCoinSymbol + " "+ depositAmount);

        mBinding.tvExchangeExtraInfo.setVisibility(View.VISIBLE);

        int requestWithdrawCnt = response.getExchange_day_request_count().length() == 0 ? 0 :
                                 Integer.parseInt(response.getExchange_day_request_count());
        int extraWithdrawCnt = Integer.parseInt(dayCount) - requestWithdrawCnt;
        mBinding.tvExchangeExtraInfo.setText( String.format(getString(R.string.exchange_info_desc_1), Integer.parseInt(fee), extraWithdrawCnt));
    }

    private void requestUserExchangeInfo(){
        if(mMapUserListToken.containsKey(mCurCoinSymbol)){

            TokenListModel coinType = mMapListToken.get(mCurCoinSymbol);
            TokenListModel userCoinType = mMapUserListToken.get(mCurCoinSymbol);

            //임시 하드 코딩
           mUserExchangeTokenApi.execute("7", "2");
        }

        mBinding.tvUserMineToken.setText(mCurCoinSymbol + " 0");
        mBinding.tvMonthlyLimit.setText(mCurCoinSymbol + " 0");
        mBinding.tvDailyLimit.setText(mCurCoinSymbol + " 0");
        mBinding.tvDailyMaxLimit.setText(mCurCoinSymbol + " 0");
        mBinding.tvInputTotal.setText(mCurCoinSymbol + " 0");

        mBinding.tvExchangeExtraInfo.setVisibility(View.INVISIBLE);
    }
    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutSpinnerRoot.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.layoutCoinType.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvCoinTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutUserMineToken.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.viewUserMineToken.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.tvUserMinTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvUserMineToken.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonMonthlyToken.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.viewButtonMonthlyToken.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.tvMonthlyLimitTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvMonthlyLimit.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonDailyToken.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.viewButtonDailyToken.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.tvDailyLimit.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvDailyLimitTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonMaxDailyToken.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvDailyMaxLimitTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvDailyMaxLimit.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonTokenMtck.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvCoinInputTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvInputTotal.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvCoinTypeTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvExchangeWithdrawTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvDepositTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvDepositTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));

            mBinding.btnWithdrawList.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnRefreshHistory.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnRequestWithdrawCash.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnDepositList.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.btnWithdrawList.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnRefreshHistory.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnRequestWithdrawCash.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnDepositList.setTextColor(getResources().getColor(R.color.main_text_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutSpinnerRoot.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.layoutCoinType.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvCoinTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutUserMineToken.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.viewUserMineToken.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.tvUserMinTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUserMineToken.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutButtonMonthlyToken.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.viewButtonMonthlyToken.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.tvMonthlyLimitTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvMonthlyLimit.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutButtonDailyToken.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.viewButtonDailyToken.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.tvDailyLimit.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvDailyLimitTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutButtonMaxDailyToken.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvDailyMaxLimitTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvDailyMaxLimit.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutButtonTokenMtck.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvCoinInputTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvInputTotal.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvCoinTypeTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvExchangeWithdrawTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvDepositTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvDepositTitle.setTextColor(getResources().getColor(R.color.main_text_color));


            mBinding.btnWithdrawList.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnRefreshHistory.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnRequestWithdrawCash.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnDepositList.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.btnWithdrawList.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnRefreshHistory.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnRequestWithdrawCash.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnDepositList.setTextColor(getResources().getColor(R.color.white_color));
        }
    }
}
