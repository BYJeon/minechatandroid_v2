package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ListMineCashPointApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CashModel;
import kr.co.minetalk.api.model.MineCashHistoryResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityPointCashGainBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.PointCashItemView;

public class PointCashGainActivity extends BindActivity<ActivityPointCashGainBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PointCashGainActivity.class);
        context.startActivity(intent);
    }

    private ListMineCashPointApi mListCashPointApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            pointGainHistoryRequest();
        }

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_point_cash_gain;
    }

    @Override
    protected void initView() {


        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        mBinding.tvTitle.setText("마인캐쉬 보유내역");
        pointGainHistoryRequest();
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));


        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }

    private void initApi() {
        mListCashPointApi = new ListMineCashPointApi(this, new ApiBase.ApiCallBack<MineCashHistoryResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointCashGainActivity.this);
            }

            @Override
            public void onSuccess(int request_code, MineCashHistoryResponse cashHistoryResponse) {
                ProgressUtil.hideProgress(PointCashGainActivity.this);
                if(cashHistoryResponse != null) {
                    setupData(cashHistoryResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {

            }

            @Override
            public void onCancellation() {

            }
        });

    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }



    private void pointGainHistoryRequest() {
        mListCashPointApi.execute();
    }

    //마인 캐쉬 보유 내역
    private void setupData(MineCashHistoryResponse response) {
        PointCashItemView itemView = new PointCashItemView(this, PointCashItemView.ItemType.ITEM_TYPE_TITLE);
        CashModel model = new CashModel();
        model.setCash_type("구분");
        model.setCash_content("내용");
        model.setCash_amount("수량");
        itemView.setData(model);
        mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());

        PointCashItemView totalView = new PointCashItemView(this, PointCashItemView.ItemType.ITEM_TYPE_TOTAL);
        model = new CashModel();
        model.setCash_type("");
        model.setCash_content("마인 캐쉬 합계");
        model.setCash_amount( CommonUtils.comma_won(response.getTotal_cash_point()));
        totalView.setData(model);
        mBinding.layoutListContainer.addView(totalView, getListItemLayoutParams());

        PointCashItemView memberView = new PointCashItemView(this, PointCashItemView.ItemType.ITEM_TYPE_EVEN);
        model = new CashModel();
        model.setCash_type("");
        model.setCash_content("연간회원권 금액 현황");
        model.setCash_amount(CommonUtils.comma_won(response.getAnnual_membership_cash()));
        memberView.setData(model);
        mBinding.layoutListContainer.addView(memberView, getListItemLayoutParams());

        PointCashItemView normallView = new PointCashItemView(this, PointCashItemView.ItemType.ITEM_TYPE_ODD);
        model = new CashModel();
        model.setCash_type("");
        model.setCash_content("일반 현황");
        model.setCash_amount(CommonUtils.comma_won(response.getBasic_cash()));
        normallView.setData(model);
        mBinding.layoutListContainer.addView(normallView, getListItemLayoutParams());
    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        return params;
    }

    private void setupTokenInfoData() {
        String totalToken = "";
        String nomalToken = "";
        String lockToken = "";

        try {
            totalToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getTotal_mine_token());
            nomalToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getUser_mine_token());
            lockToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getLock_mine_token());
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        String unit = getResources().getString(R.string.more_fragment_text_unit_token);

        //mBinding.tvTotal.setText("합산 : " + totalToken + unit);
        //mBinding.tvNormal.setText("일반 : " + nomalToken + unit);
        //mBinding.tvDelay.setText("유예 : " + lockToken + unit);

    }


}
