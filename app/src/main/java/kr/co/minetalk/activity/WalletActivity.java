package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ListTokensApi;
import kr.co.minetalk.api.MakeWalletApi;
import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.TokenListModel;
import kr.co.minetalk.api.model.TokenListResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityWalletBinding;
import kr.co.minetalk.ui.listener.OnAdChatItemListener;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.FAQItemView;
import kr.co.minetalk.view.WalletInfoView;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class WalletActivity extends BindActivity<ActivityWalletBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, WalletActivity.class);
        context.startActivity(intent);
    }

    private ListTokensApi mListTokensApi;
    private MakeWalletApi mMakeWalletApi;

    private Map<String, TokenListModel> mMapListToken = new HashMap<>();
    private Map<String, TokenListModel> mMapUserListToken = new HashMap<>();


    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            tokenListRequest();
        }

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_wallet;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
        tokenListRequest();
    }

    private void initApi() {
        mListTokensApi = new ListTokensApi(this, new ApiBase.ApiCallBack<TokenListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(WalletActivity.this);
            }

            @Override
            public void onSuccess(int request_code, TokenListResponse response) {
                ProgressUtil.hideProgress(WalletActivity.this);
                if(response != null) {
                    setCoinInfo(response);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(WalletActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mMakeWalletApi = new MakeWalletApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(WalletActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel response) {
                ProgressUtil.hideProgress(WalletActivity.this);
                if(response != null) {
                    tokenListRequest();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(WalletActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        // 내 포인트 클릭
        mBinding.layoutButtonMypoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointReportMoreActivity.startActivity(getApplicationContext(), FLAG_ACTIVITY_NEW_TASK);
            }
        });

        // 거래소 클릭
        mBinding.layoutButtonExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointExchangeActivity.startActivity(getApplicationContext(), FLAG_ACTIVITY_NEW_TASK);
            }
        });

//        mBinding.snCoinType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String selCoin = mCoinAdapter.getItem(position);
//                if(mMapListToken.containsKey(selCoin)){
//                    TokenListModel model = mMapListToken.get(selCoin);
//                    mCurCoinSymbol = model.getUser_token_symble();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        //전자 지갑 생성
//        mBinding.btnMakeWallet.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String selCoin = mBinding.snCoinType.getSelectedItem().toString();
//                if(mMapListToken.containsKey(selCoin)){
//                    TokenListModel model = mMapListToken.get(selCoin);
//                    String token_id = model.getUser_token_id();
//                    String coin_id = model.getUser_token_coin_id();
//
//                    mMakeWalletApi.execute(token_id, coin_id);
//                }
//
//            }
//        });
    }

    private void tokenListRequest() {
        mListTokensApi.execute();
    }

    private void setCoinInfo(TokenListResponse response) {
        mMapListToken.clear();
        mMapUserListToken.clear();

        ArrayList<String> coinList = new ArrayList<>();
        for(TokenListModel models :  response.getToken_list() ) {
            String tokenName = models.getUser_token_symble() + " (" + models.getUser_token_name() + ")";
            mMapListToken.put(tokenName, models);

            if(models.getUser_token_symble().toLowerCase().equals("mctk"))
                coinList.add(0, tokenName);
            else
                coinList.add(tokenName);
        }

        //임시 하드 코딩
        coinList.add("MCTK (MineToken)");

        //mCoinAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, coinList);
        //mBinding.snCoinType.setAdapter(mCoinAdapter);

        mBinding.layoutWalletContainer.removeAllViews();

        int nCnt = 0;
        for(TokenListModel models :  response.getUser_token_list() ) {
            mMapUserListToken.put(models.getUser_token_symble(), models);

            WalletInfoView infoView = new WalletInfoView(this);
            infoView.setData(models, nCnt%2!=0 ? FAQItemView.ItemType.ITEM_TYPE_EVEN : FAQItemView.ItemType.ITEM_TYPE_ODD);
            mBinding.layoutWalletContainer.addView(infoView);
            ++nCnt;
        }

        //int idx = coinList.get(0).indexOf("(");
        //mCurCoinSymbol = coinList.get(0).substring(0, idx-1).trim();
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.layoutPointRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvMyPoint.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvExchange.setTextColor(getResources().getColor(R.color.white_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.layoutPointRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvMyPoint.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvExchange.setTextColor(getResources().getColor(R.color.main_text_color));
        }
    }
}
