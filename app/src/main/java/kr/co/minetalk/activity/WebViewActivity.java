package kr.co.minetalk.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kr.co.minetalk.BuildConfig;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityWebviewBinding;

public class WebViewActivity extends BindActivity<ActivityWebviewBinding> {

    private static final String COLUMN_TITLE = "column_title";
    private static final String COLUMN_URL = "column_url";
    public static void startActivity(Context context, String activityTitle, String url) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(COLUMN_TITLE, activityTitle);
        intent.putExtra(COLUMN_URL, url);
        context.startActivity(intent);
    }

    private String mLoadUrl;
    private String mTitle;

    private WebSettings mWebSettings = null;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_webview;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        if(getIntent() != null && getIntent().getExtras() != null) {
            mTitle = getIntent().getExtras().getString(COLUMN_TITLE);
            mLoadUrl = getIntent().getExtras().getString(COLUMN_URL);
        }

        setUIEventListener();

        mBinding.tvActivityTitle.setText(mTitle);
        loadWeb(mLoadUrl);
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadWeb(String url) {

        Log.e("@@@TAG", "load url : " + url);
        // Setting
        if (mWebSettings == null) {
            mWebSettings = mBinding.webView.getSettings();
            mWebSettings.setJavaScriptEnabled(true);
            mWebSettings.setLoadWithOverviewMode(true);
//            mWebSettings.setAllowFileAccess(true);
            mWebSettings.setDomStorageEnabled(true);
            mWebSettings.setJavaScriptEnabled(true);
            mWebSettings.setLoadWithOverviewMode(true);
            mWebSettings.setUseWideViewPort(true);
            mWebSettings.setSupportZoom(true);
            mWebSettings.setBuiltInZoomControls(false);
            mWebSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            mWebSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            mWebSettings.setDomStorageEnabled(true);
            mWebSettings.setAllowFileAccess(false);

//            mBinding.webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            mBinding.webView.setScrollbarFadingEnabled(true);
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
//                mWebSettings.setTextZoom(100);
//            }

            if (BuildConfig.isDebug && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mBinding.webView.setWebContentsDebuggingEnabled(true);
            }
            mBinding.webView.setWebViewClient(new CustomWebViewClient());
            // mBinding.webview.setWebChromeClient(new CustomChromeClient());


        }
        // Load
        mBinding.webView.loadUrl(url);
    }

    class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }

    class CustomWebChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            return super.onJsAlert(view, url, message, result);
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
            return super.onJsConfirm(view, url, message, result);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            final WebSettings settings = view.getSettings();
            settings.setDomStorageEnabled(true);
            settings.setJavaScriptEnabled(true);
            settings.setAllowFileAccess(true);
            settings.setAllowContentAccess(true);
            view.setWebChromeClient(this);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(view);
            resultMsg.sendToTarget();
            return false;
        }

        @Override
        public void onCloseWindow(WebView window) {
            super.onCloseWindow(window);
        }
    }
}
