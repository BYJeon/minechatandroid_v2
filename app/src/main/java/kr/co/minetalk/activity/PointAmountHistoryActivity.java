package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.ReportAmountListAdapter;
import kr.co.minetalk.adapter.ReportListAdapter;
import kr.co.minetalk.api.ListDepositApi;
import kr.co.minetalk.api.ListPointApi;
import kr.co.minetalk.api.ListWidthDrawApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.DepositListResponse;
import kr.co.minetalk.api.model.PointHistoryResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.api.model.WithDrawListResponse;
import kr.co.minetalk.databinding.ActivityPointAmountHistoryBinding;
import kr.co.minetalk.databinding.ActivityPointHistoryBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;

public class PointAmountHistoryActivity extends BindActivity<ActivityPointAmountHistoryBinding> {

    private static final String COLUMN_POINT_TYPE = "point_type";

    public static void startActivity(Context context, String point_type) {
        Intent intent = new Intent(context, PointAmountHistoryActivity.class);
        intent.putExtra(COLUMN_POINT_TYPE, point_type);
        context.startActivity(intent);
    }


    private ListWidthDrawApi mListWithDrawApi;
    private ListDepositApi mListDepositApi;

    private ReportAmountListAdapter mAdapter;

    private String mPointType = MineTalk.POINT_TYPE_CASH;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            pointHistoryRequest(mPointType);
        }

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_point_amount_history;
    }

    @Override
    protected void initView() {
        if(getIntent() != null && getIntent().getExtras() != null) {
            mPointType = getIntent().getExtras().getString(COLUMN_POINT_TYPE, MineTalk.POINT_TYPE_DEPOSIT);

            mBinding.tvTitle.setText( mPointType.equals(MineTalk.POINT_TYPE_DEPOSIT) ? "입금내역" : "출금내역");
        }
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        mAdapter = new ReportAmountListAdapter();
        mBinding.listView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        pointHistoryRequest(mPointType);
    }

    private void pointHistoryRequest(String point_type) {
        if(point_type.equals(MineTalk.POINT_TYPE_DEPOSIT))
            mListDepositApi.execute();
        else
            mListWithDrawApi.execute();
    }

    private void initApi() {
        mListDepositApi = new ListDepositApi(this, new ApiBase.ApiCallBack<DepositListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointAmountHistoryActivity.this);
            }

            @Override
            public void onSuccess(int request_code, DepositListResponse response) {
                ProgressUtil.hideProgress(PointAmountHistoryActivity.this);
                if(response != null) {
                    mAdapter.setData(response.getDeposit_list(), response.getDeposit_total_point(), MineTalk.POINT_TYPE_DEPOSIT);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointAmountHistoryActivity.this);
                showMessageAlert(message);

            }

            @Override
            public void onCancellation() {

            }
        });

        mListWithDrawApi = new ListWidthDrawApi(this, new ApiBase.ApiCallBack<WithDrawListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointAmountHistoryActivity.this);
            }

            @Override
            public void onSuccess(int request_code, WithDrawListResponse response) {
                ProgressUtil.hideProgress(PointAmountHistoryActivity.this);
                if(response != null) {
                    mAdapter.setData(response.getWithdraw_list(), response.getWithdraw_total_point(), MineTalk.POINT_TYPE_WITHDRAW);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointAmountHistoryActivity.this);
                showMessageAlert(message);

            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });


        mBinding.ivRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userInfoRequest();
            }
        });
    }

    @Override
    public void updateTheme() {
        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }
}
