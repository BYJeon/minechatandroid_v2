package kr.co.minetalk.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ListTokensApi;
import kr.co.minetalk.api.MiningInfoApi;
import kr.co.minetalk.api.UserExchangeInfoApi;
import kr.co.minetalk.api.UserLimitListApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CouponModel;
import kr.co.minetalk.api.model.FaqModel;
import kr.co.minetalk.api.model.FaqResponse;
import kr.co.minetalk.api.model.MiningInfoResponse;
import kr.co.minetalk.api.model.MiningModel;
import kr.co.minetalk.api.model.TokenListModel;
import kr.co.minetalk.api.model.TokenListResponse;
import kr.co.minetalk.api.model.UserExchangeInfoResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.api.model.UserLimitListResponse;
import kr.co.minetalk.databinding.ActivityBuyCouponBinding;
import kr.co.minetalk.databinding.ActivityLimitCheckBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.CouponItemView;
import kr.co.minetalk.view.FAQItemView;
import kr.co.minetalk.view.MiningItemView;

public class LimitCheckActivity extends BindActivity<ActivityLimitCheckBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, LimitCheckActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flag) {
        Intent intent = new Intent(context, LimitCheckActivity.class);
        intent.addFlags(flag);
        context.startActivity(intent);
    }

    private UserLimitListApi mUserLimitListApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_limit_check;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUserLimitListApi.execute();
    }

    private void initApi() {
        mUserLimitListApi = new UserLimitListApi(LimitCheckActivity.this, new ApiBase.ApiCallBack<UserLimitListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(LimitCheckActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserLimitListResponse response) {
                ProgressUtil.hideProgress(LimitCheckActivity.this);
                if(response != null) {

                    String accumulateAmount = CommonUtils.comma_won(response.getAccumulate_amount());
                    String limitAmount = CommonUtils.comma_won(response.getLimit_amount());

                    long balenceAmount = Long.parseLong(response.getLimit_amount()) - Long.parseLong(response.getAccumulate_amount());
                    mBinding.tvTotalAmount.setText(accumulateAmount);
                    mBinding.tvTotalBalanceAmount.setText(limitAmount);
                    mBinding.tvBalanceAmount.setText( CommonUtils.comma_won(Long.toString(balenceAmount)));
                    setupData(response);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(LimitCheckActivity.this);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnBuyCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //BuyCouponActivity.startActivity(LimitCheckActivity.this);
                String message = "현재 서비스 개선중입니다.\n" +
                        "본 서비스를 이용하시려면 1:1문의에 연락처를 남겨주시면 상담 전화 드리겠습니다.\n\n" +
                        "1833-3245";
                showMessageAlert(message);
            }
        });

        //상세보기
        mBinding.btnViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveHistoryActivity.startActivity(LimitCheckActivity.this);
            }
        });
    }

    private void setupData(UserLimitListResponse response) {
        mBinding.layoutListContainer.removeAllViews();

        ArrayList<MiningModel> arrs = response.getMiningList();
//        arrs.add(new CouponModel("1주년 이벤트 (Sample)", "행사", "2019-11-26 16:07:09","1000000"));
//        arrs.add(new CouponModel("본인구매 (Sample)", "구매", "2019-11-26 16:07:09","12000000"));
//        arrs.add(new CouponModel("본인구매 (Sample)", "구매", "2019-11-26 16:07:09","5000000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));

        for(int i = 0; i < arrs.size(); i++) {
            MiningItemView itemView = new MiningItemView(this);
            itemView.setData(arrs.get(i));
            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }
    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //params.topMargin = getResources().getDimensionPixelOffset(R.dimen.height_24);

        return params;
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.btnViewDetails.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnViewDetails.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.btnBuyCoupon.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBuyCoupon.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.tvPurchaseAmountTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTotalAmount.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvTotalBalanceAmountTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTotalBalanceAmount.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvBalanceInfoTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvExtraDepositeInfoTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.btnViewDetails.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnViewDetails.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.btnBuyCoupon.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBuyCoupon.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.tvPurchaseAmountTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvTotalAmount.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvTotalBalanceAmountTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvTotalBalanceAmount.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvBalanceInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvExtraDepositeInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
        }
    }
}
