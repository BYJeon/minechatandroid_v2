package kr.co.minetalk.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ChargePointApi;
import kr.co.minetalk.api.GetChargeConfigApi;
import kr.co.minetalk.api.ListMemberShipApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.ChargeAmountModel;
import kr.co.minetalk.api.model.ChargeConfigModel;
import kr.co.minetalk.api.model.MemberShipHistoryModel;
import kr.co.minetalk.api.model.MemberShipHistoryResponse;
import kr.co.minetalk.databinding.ActivityChargeBinding;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.popup.BottomWheelChargePopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.AdMessageReceiveView;
import kr.co.minetalk.view.FAQItemView;
import kr.co.minetalk.view.MemberShipHistoryView;

public class ChargeActivity extends BindActivity<ActivityChargeBinding> {

    public static final String COLUMN_POINT_TYPE = "point_type";
    public static final String POINT_TYPE_CASH = "mine_cash";
    public static final String POINT_TYPE_TOKEN= "mine_token";

    public static void startActivity(Context context, String pointType) {
        Intent intent = new Intent(context, ChargeActivity.class);
        intent.putExtra(COLUMN_POINT_TYPE, pointType);
        context.startActivity(intent);
    }



    private GetChargeConfigApi mGetChargeConfigApi;
    private ChargePointApi mChargePointApi;
    private ListMemberShipApi mListMemberShipApi;

    private String mOfficeTel = "";
    private String mSelectAmount = "10000";
    private int mSelectWheelPosition = 0;
    private String[] mAmountArray;

    private String[] mVisibleAmountArray;
    private ArrayList<ChargeAmountModel> mChargeAmountList = new ArrayList<>();


    private String mPointType;


    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_charge;
    }

    @Override
    protected void initView() {
        if(getIntent() != null && getIntent().getExtras() != null) {
            mPointType = getIntent().getExtras().getString(COLUMN_POINT_TYPE);
        }

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        chargeConfigRequest();
        memberShipHistoryRequest();
    }

    private void initApi() {
        mListMemberShipApi = new ListMemberShipApi(this, new ApiBase.ApiCallBack<MemberShipHistoryResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChargeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, MemberShipHistoryResponse response) {
                ProgressUtil.hideProgress(ChargeActivity.this);
                if(response != null) {
                    setupMemberShipData(response);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {

            }

            @Override
            public void onCancellation() {

            }
        });

        mGetChargeConfigApi = new GetChargeConfigApi(this, new ApiBase.ApiCallBack<ChargeConfigModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChargeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, ChargeConfigModel chargeConfigModel) {
                ProgressUtil.hideProgress(ChargeActivity.this);
                if(chargeConfigModel != null) {
                    setupData(chargeConfigModel);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(ChargeActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mChargePointApi = new ChargePointApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChargeActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(ChargeActivity.this);
                if(baseModel != null) {
                    finish();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(ChargeActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {

        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.layoutButtonAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showWheelAmount();
                showAmountPopup();
            }
        });

        mBinding.tvButtonRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chargeRequest(mPointType, mSelectAmount, mBinding.tvDepositor.getText().toString());
            }
        });

        mBinding.tvButtonCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
                ClipData clipData = ClipData.newPlainText("accountInfo", mBinding.tvAccountInfo.getText().toString());
                clipboardManager.setPrimaryClip(clipData);

                Toast.makeText(ChargeActivity.this, getResources().getString(R.string.mission_complete), Toast.LENGTH_SHORT).show();
            }
        });

        mBinding.tvButtonTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOfficeTel != null && !mOfficeTel.equals("")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+mOfficeTel));
                    startActivity(intent);
                }
            }
        });
    }

    private void chargeConfigRequest() {
        mGetChargeConfigApi.execute();
    }

    private void memberShipHistoryRequest() {
        mListMemberShipApi.execute();
    }

    private void chargeRequest(String point_type, String point_amount, String depositor) {
        mChargePointApi.execute(point_type, point_amount, depositor);
    }

    private void setupMemberShipData(MemberShipHistoryResponse reponse) {
        if(reponse.getMembership_end_date().length() > 0)
            mBinding.tvGradeDate.setText("기한일자 : " + reponse.getMembership_end_date());


        mBinding.layoutContainer.removeAllViews();

        if(reponse.getMembership_history().size() > 0){
            MemberShipHistoryView itemView = new MemberShipHistoryView(this);
            itemView.setData(reponse.getMembership_history().get(0), FAQItemView.ItemType.ITEM_TYPE_TITLE);
            mBinding.layoutContainer.addView(itemView);
        }

        int nCnt = 0;
        for(MemberShipHistoryModel model : reponse.getMembership_history() ){
            MemberShipHistoryView itemView = new MemberShipHistoryView(this);
            itemView.setData(model, nCnt%2 == 0 ? FAQItemView.ItemType.ITEM_TYPE_ODD : FAQItemView.ItemType.ITEM_TYPE_ODD);
            mBinding.layoutContainer.addView(itemView);
            nCnt++;
        }

    }

    private void setupData(ChargeConfigModel configModel) {
        if(configModel != null) {
            mBinding.tvBankName.setText(configModel.getBank_name());
            mBinding.tvAccountInfo.setText(configModel.getBank_account());
            String phone = MineTalkApp.getUserInfoModel().getUser_hp();
            String userName = MineTalkApp.getUserInfoModel().getUser_name();

            if(phone != null && phone.length() > 10) {
                String phoneLast4 = phone.substring(phone.length() - 4, phone.length());
                String dopositor = userName + phoneLast4;
                mBinding.tvDepositor.setText(dopositor);
            } else {
                mBinding.tvDepositor.setText(userName);
            }

            mOfficeTel = configModel.getOffice_tel();

            mChargeAmountList.clear();
            mChargeAmountList.addAll(configModel.getCharge_amount_list());


            mVisibleAmountArray = new String[configModel.getCharge_amount_list().size()];
            for (int i = 0 ; i < configModel.getCharge_amount_list().size() ; i++) {
                ChargeAmountModel item = configModel.getCharge_amount_list().get(i);
                String str = item.getText() + " : " + CommonUtils.comma_won(item.getAmount()) + getResources().getString(R.string.money_won);

                mVisibleAmountArray[i] = str;
            }


            if(mChargeAmountList.size() > 0) {
                mSelectAmount = mChargeAmountList.get(0).getAmount();
                mSelectWheelPosition = 0;
            }



            mBinding.tvAmount.setText(mVisibleAmountArray[mSelectWheelPosition]);

            mBinding.tvTelNumber.setText(mOfficeTel);

        }
    }

    private void showWheelAmount() {
        showBottomWheelPopup(mAmountArray, 0, new PopupListenerFactory.BaseInputWheelListener() {
            @Override
            public void onClick(int state, int num) {
                if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                    mSelectWheelPosition = num;
                    mBinding.tvAmount.setText(mAmountArray[num]);
                }
            }

            @Override
            public void onClick(int state, int num, int position) {

            }
        });
    }

    private void showAmountPopup() {

        BottomWheelChargePopup chargePopup = new BottomWheelChargePopup(this);
        chargePopup.setData(mVisibleAmountArray, mSelectWheelPosition);
        chargePopup.setDialogEventListener(new PopupListenerFactory.BaseInputWheelListener() {
            @Override
            public void onClick(int state, int num) {
                if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                    mSelectWheelPosition = num;

                    mSelectAmount = mChargeAmountList.get(num).getAmount();
                    mBinding.tvAmount.setText(mVisibleAmountArray[num]);
                }
            }

            @Override
            public void onClick(int state, int num, int position) {

            }
        });
        chargePopup.show();
    }


    @Override
    public void updateTheme() {
        super.updateTheme();if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.layoutButtonGrade.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonAmount.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));


            mBinding.tvGradeTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvGradeDate.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAmountTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvAmount.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.tvButtonRequest.setBackgroundColor(getResources().getColor(R.color.bk_point_color));

        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonGrade.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonAmount.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));

            mBinding.tvGradeTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvGradeDate.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAmountTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvAmount.setBackgroundResource(R.drawable.bg_chat_round_16c066);
            mBinding.tvButtonRequest.setBackgroundColor(getResources().getColor(R.color.app_point_color));
        }
    }
}
