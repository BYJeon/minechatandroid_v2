package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.CouponPurchaseListApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CouponModel;
import kr.co.minetalk.api.model.CouponPurchaseListResponse;
import kr.co.minetalk.databinding.ActivityBuyHistoryBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.CouponItemView;

public class BuyHistoryActivity extends BindActivity<ActivityBuyHistoryBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, BuyHistoryActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flag) {
        Intent intent = new Intent(context, BuyHistoryActivity.class);
        intent.addFlags(flag);
        context.startActivity(intent);
    }

    private CouponPurchaseListApi mCouponPurchaseListApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_buy_history;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCouponPurchaseListApi.execute();
    }

    private void initApi() {
        mCouponPurchaseListApi = new CouponPurchaseListApi(this, new ApiBase.ApiCallBack<CouponPurchaseListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(BuyHistoryActivity.this);
            }

            @Override
            public void onSuccess(int request_code, CouponPurchaseListResponse couponResponse) {
                ProgressUtil.hideProgress(BuyHistoryActivity.this);
                if(couponResponse != null) {
                    setupData(couponResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(BuyHistoryActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //한도조회
        mBinding.btnLimitHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LimitCheckActivity.startActivity(BuyHistoryActivity.this);
            }
        });
    }

    private void setupData(CouponPurchaseListResponse response) {
        mBinding.layoutListContainer.removeAllViews();
        String totalAmount = CommonUtils.comma_won(response.getTotal_coupon_amount());
        mBinding.tvTotalAmount.setText(totalAmount);

        ArrayList<CouponModel> arrs = response.getCouponInfo_list();
//        arrs.add(new CouponModel("1주년 이벤트 (Sample)", "행사", "2019-11-26 16:07:09","1000000"));
//        arrs.add(new CouponModel("본인구매 (Sample)", "구매", "2019-11-26 16:07:09","12000000"));
//        arrs.add(new CouponModel("본인구매 (Sample)", "구매", "2019-11-26 16:07:09","5000000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));
//        arrs.add(new CouponModel("홍길동(hong1234) (Sample)", "추천", "2019-11-26 16:07:09","500000"));

        for(int i = 0; i < arrs.size() ; i++) {
            CouponItemView itemView = new CouponItemView(this);
            itemView.setData(arrs.get(i));
            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }

//        ArrayList<CouponModel> arrs = response.getCouponInfo_list();
//        for(int i = 0; i < arrs.size() ; i++) {
//            CouponItemView itemView = new CouponItemView(this);
//            itemView.setData(arrs.get(i));
//            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
//        }
    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //params.topMargin = getResources().getDimensionPixelOffset(R.dimen.height_24);

        return params;
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvSavePointHistoryInfoTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));

            mBinding.btnLimitHistory.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnLimitHistory.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.tvSavePointTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTotalAmount.setTextColor(getResources().getColor(R.color.white_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvSavePointHistoryInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnLimitHistory.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnLimitHistory.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.tvSavePointTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvTotalAmount.setTextColor(getResources().getColor(R.color.main_text_color));
        }
    }
}
