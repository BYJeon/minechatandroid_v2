package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivitySelectBackgroundBinding;
import kr.co.minetalk.utils.Preferences;

public class SelectBackgroundActivity extends BindActivity<ActivitySelectBackgroundBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SelectBackgroundActivity.class);
        context.startActivity(intent);
    }

    private String mSelectColor = "";

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_select_background;
    }

    @Override
    protected void initView() {
        setColorBoxInitialize();
        refreshView();
        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setChatBackgroundType(MineTalk.BACKGROUND_TYPE_COLOR);
                Preferences.setChatBackgroundColor(mSelectColor);
                setResult(RESULT_OK);
                finish();
            }
        });

        mBinding.colorView1.setOnClickListener(mColorBoxListener);
        mBinding.colorView2.setOnClickListener(mColorBoxListener);
        mBinding.colorView3.setOnClickListener(mColorBoxListener);
        mBinding.colorView4.setOnClickListener(mColorBoxListener);
        mBinding.colorView5.setOnClickListener(mColorBoxListener);
        mBinding.colorView6.setOnClickListener(mColorBoxListener);
    }


    private View.OnClickListener mColorBoxListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.color_view_1 :
                    mSelectColor = "f5f5f5";
                    mBinding.colorView1.setChecked(true);
                    mBinding.colorView2.setChecked(false);
                    mBinding.colorView3.setChecked(false);
                    mBinding.colorView4.setChecked(false);
                    mBinding.colorView5.setChecked(false);
                    mBinding.colorView6.setChecked(false);
                    break;
                case R.id.color_view_2 :
                    mSelectColor = "e2f6f4";
                    mBinding.colorView1.setChecked(false);
                    mBinding.colorView2.setChecked(true);
                    mBinding.colorView3.setChecked(false);
                    mBinding.colorView4.setChecked(false);
                    mBinding.colorView5.setChecked(false);
                    mBinding.colorView6.setChecked(false);
                    break;
                case R.id.color_view_3 :
                    mSelectColor = "e8edf3";
                    mBinding.colorView1.setChecked(false);
                    mBinding.colorView2.setChecked(false);
                    mBinding.colorView3.setChecked(true);
                    mBinding.colorView4.setChecked(false);
                    mBinding.colorView5.setChecked(false);
                    mBinding.colorView6.setChecked(false);
                    break;
                case R.id.color_view_4 :
                    mSelectColor = "fcece4";
                    mBinding.colorView1.setChecked(false);
                    mBinding.colorView2.setChecked(false);
                    mBinding.colorView3.setChecked(false);
                    mBinding.colorView4.setChecked(true);
                    mBinding.colorView5.setChecked(false);
                    mBinding.colorView6.setChecked(false);
                    break;
                case R.id.color_view_5 :
                    mSelectColor = "ecf6e2";
                    mBinding.colorView1.setChecked(false);
                    mBinding.colorView2.setChecked(false);
                    mBinding.colorView3.setChecked(false);
                    mBinding.colorView4.setChecked(false);
                    mBinding.colorView5.setChecked(true);
                    mBinding.colorView6.setChecked(false);
                    break;
                case R.id.color_view_6 :
                    mSelectColor = "e2e4f6";
                    mBinding.colorView1.setChecked(false);
                    mBinding.colorView2.setChecked(false);
                    mBinding.colorView3.setChecked(false);
                    mBinding.colorView4.setChecked(false);
                    mBinding.colorView5.setChecked(false);
                    mBinding.colorView6.setChecked(true);
                    break;
            }
        }
    };

    private void setColorBoxInitialize() {
        mBinding.colorView1.setBackColor("f5f5f5");
        mBinding.colorView2.setBackColor("e2f6f4");
        mBinding.colorView3.setBackColor("e8edf3");
        mBinding.colorView4.setBackColor("fcece4");
        mBinding.colorView5.setBackColor("ecf6e2");
        mBinding.colorView6.setBackColor("e2e4f6");
    }


    private void refreshView() {
        String color = Preferences.getChatBackgroundColor();
        if(color == null || color.equals("")) {
            Preferences.setChatBackgroundColor("e8edf3");
            mBinding.colorView1.setChecked(false);
            mBinding.colorView2.setChecked(false);
            mBinding.colorView3.setChecked(true);
            mBinding.colorView4.setChecked(false);
            mBinding.colorView5.setChecked(false);
            mBinding.colorView6.setChecked(false);

        } else {
            if(color.equals("f5f5f5")) {
                mBinding.colorView1.setChecked(true);
                mBinding.colorView2.setChecked(false);
                mBinding.colorView3.setChecked(false);
                mBinding.colorView4.setChecked(false);
                mBinding.colorView5.setChecked(false);
                mBinding.colorView6.setChecked(false);
            } else if(color.equals("e2f6f4")) {
                mBinding.colorView1.setChecked(false);
                mBinding.colorView2.setChecked(true);
                mBinding.colorView3.setChecked(false);
                mBinding.colorView4.setChecked(false);
                mBinding.colorView5.setChecked(false);
                mBinding.colorView6.setChecked(false);
            } else if(color.equals("e8edf3")) {
                mBinding.colorView1.setChecked(false);
                mBinding.colorView2.setChecked(false);
                mBinding.colorView3.setChecked(true);
                mBinding.colorView4.setChecked(false);
                mBinding.colorView5.setChecked(false);
                mBinding.colorView6.setChecked(false);
            } else if(color.equals("fcece4")) {
                mBinding.colorView1.setChecked(false);
                mBinding.colorView2.setChecked(false);
                mBinding.colorView3.setChecked(false);
                mBinding.colorView4.setChecked(true);
                mBinding.colorView5.setChecked(false);
                mBinding.colorView6.setChecked(false);
            } else if(color.equals("ecf6e2")) {

                mBinding.colorView1.setChecked(false);
                mBinding.colorView2.setChecked(false);
                mBinding.colorView3.setChecked(false);
                mBinding.colorView4.setChecked(false);
                mBinding.colorView5.setChecked(true);
                mBinding.colorView6.setChecked(false);
            } else if(color.equals("e2e4f6")) {
                mBinding.colorView1.setChecked(false);
                mBinding.colorView2.setChecked(false);
                mBinding.colorView3.setChecked(false);
                mBinding.colorView4.setChecked(false);
                mBinding.colorView5.setChecked(false);
                mBinding.colorView6.setChecked(true);
            }
        }
    }




}
