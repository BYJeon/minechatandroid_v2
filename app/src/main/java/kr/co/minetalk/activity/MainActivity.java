package kr.co.minetalk.activity;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;
import com.igaworks.IgawCommon;

import org.json.JSONArray;
import org.json.JSONException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnDataObserverListener;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.adapter.MainFragmentPagerAdapter;
import kr.co.minetalk.api.ListFriendApi;
import kr.co.minetalk.api.LogoutApi;
import kr.co.minetalk.api.SecessionUserApi;
import kr.co.minetalk.api.SetMessageReceiveFlagApi;
import kr.co.minetalk.api.UploadContactApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.FriendListResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.ActivityMainBinding;
import kr.co.minetalk.fragment.ChatPagerFragment;
import kr.co.minetalk.fragment.FriendPagerFragment;
import kr.co.minetalk.fragment.MyMineFragment;
import kr.co.minetalk.fragment.ShopFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.locker.view.AppLocker;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.CreateThreadApi;
import kr.co.minetalk.message.api.ThreadListApi;
import kr.co.minetalk.message.manager.MessageThreadManager;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.ui.PermissionNotiDialog;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.listener.OnProfileListener;
import kr.co.minetalk.ui.popup.BottomProfilePopup;
import kr.co.minetalk.ui.popup.SimpleAuthPopup;
import kr.co.minetalk.ui.popup.SimpleNoticePopup;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class MainActivity extends BindActivity<ActivityMainBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }


    private MainFragmentPagerAdapter mAdapter;
    private int mViewPagerPosition = 1;

    private SetMessageReceiveFlagApi mSetMessageReceiveFlagApi;
    private SecessionUserApi mSecessionUserApi;

    private ListFriendApi mRefreshListFriendApi;
    private LogoutApi mLogoutApi;
    private int mContactListCount = 0;

    //지문 인증
//    private FingerprintManager fingerprintManager;
//    private KeyguardManager keyguardManager;
//    private KeyStore keyStore;
//    private KeyGenerator keyGenerator;
//    private Cipher cipher;
//    private FingerprintManager.CryptoObject cryptoObject;
//    private static final String KEY_NAME = "minechat_key";

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if (baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
        }
        mAdapter.notifyDataSetChanged();
        refreshFragment();
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }


    @Override
    protected void initView() {

        mBinding.setHandlers(this);

        //첫 실행 시 MainActivity를 Ignore 한다.
        MineTalkApp.setMainContext(this);

        mSyncListener = mMainSyncListner;
        initBroadCastReceiver();

        initApi();
        setUIEventListener();

        setupBottonNavigation();

//        if (!Preferences.getAuthorityNotice()) {
//            PermissionNotiDialog dialog = new PermissionNotiDialog(this);
//            dialog.setCancelable(false);
//            dialog.show();
//        }

        final Calendar cal = Calendar.getInstance();
        String today = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE));
        String saveNoticeDate = Preferences.getNoticeDate();
        long diffDay = getDateDiff(saveNoticeDate, today);

        //if(!today.equals(saveNoticeDate)) {

//        if(MineTalkApp.NOTICE_UPDATE_YN.toUpperCase().equals("S")){
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    showMessageAlert(getResources().getString(R.string.optional_update_desc));
//                }
//            }, 500);
//
//        }else {
//
//        }

        if (saveNoticeDate.equals("") || Math.abs(diffDay) > 7) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (MineTalkApp.NOITICE_YN.toUpperCase().equals("Y")) {
                        SimpleNoticePopup simpleNoticePopup = new SimpleNoticePopup();
                        simpleNoticePopup.setContents(MineTalkApp.NOITICE_CONTENTS);
                        simpleNoticePopup.setEventListener(new SimpleNoticePopup.OnNoticeListener() {
                            @Override
                            public void onTodayNotVisible(String today) {
                                //Preferences.setNoticeDate(today);
                            }

                            @Override
                            public void onWeekNotVisible(String today) {
                                Preferences.setNoticeDate(today);
                            }
                        });

                        try {
                            simpleNoticePopup.show(getSupportFragmentManager(), "notice_popup");
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, 500);
        }

        try {
            Intent intent = getIntent();
            if (intent != null) {
                String action = intent.getAction();
                String mineType = intent.getType();

                if (action != null && Intent.ACTION_SEND.equals(action)) {
                    goTrasmissionByIntent(action, mineType, intent);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "getInstanceId failed", task.getException());
//                            return;
//                        }
//
//                        // Get new Instance ID token
//                        String token = task.getResult().getToken();
//
//                        Log.d(token, token);
//                        Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
//                    }
//                });

        //모든 오류를 잡자
//        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//            @Override
//            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
//                String stackTraceMsg = "paramThread: " + paramThread + " paramThrowable: " + paramThrowable + "\n";
//                StackTraceElement[] stackTraces = paramThrowable.getStackTrace();
//
//                for (int i = 0; i < stackTraces.length; i++) {
//                    stackTraceMsg += stackTraces[i].toString();
//                    stackTraceMsg += "\n";
//                }
//                paramThrowable = paramThrowable.getCause();
//                if (paramThrowable != null) {
//                    stackTraceMsg += "Caused by: " + paramThrowable + "\n";
//                    stackTraces = paramThrowable.getStackTrace();
//                    for (int i = 0; i < stackTraces.length; i++) {
//                        stackTraceMsg += stackTraces[i].toString();
//                        stackTraceMsg += "\n";
//                    }
//
//                    paramThrowable = paramThrowable.getCause();
//                    if (paramThrowable != null) {
//                        stackTraceMsg += "Caused by: " + paramThrowable + "\n";
//                        stackTraces = paramThrowable.getStackTrace();
//                        for (int i = 0; i < stackTraces.length; i++) {
//                            stackTraceMsg += stackTraces[i].toString();
//                            stackTraceMsg += "\n";
//                        }
//                    }
//                }
//
//                Log.e("Crash", stackTraceMsg);
//                android.os.Process.killProcess(android.os.Process.myPid());
//            }
//        });

        if (getIntent() != null && getIntent().getExtras() != null) {
            MineTalk.PUSH_THREAD_KEY = getIntent().getExtras().getString("threadKey");
            MineTalk.AD_IDX = getIntent().getExtras().getString("adIdx", "");
            MineTalk.AD_TITLE = getIntent().getExtras().getString("adTitle", "");

            //채팅 푸쉬 알림의 경우
            MineTalk.PUSH_SERVICE_THREAD_KEY = getIntent().getExtras().getString("threadKey");
            MineTalk.PUSH_SERVICE_SECURITY_KEY = getIntent().getExtras().getString("securityMode");
        }
    }

    private void initBroadCastReceiver() {
        IntentFilter newChatFilter = new IntentFilter();
        newChatFilter.addAction(MineTalk.BROAD_CAST_NEW_MESSAGE);
        registerReceiver(mNewChattingReceiver, newChatFilter);

        IntentFilter deleteChatFilter = new IntentFilter();
        deleteChatFilter.addAction(MineTalk.BROAD_CAST_DELETE_MESSAGE);
        registerReceiver(mDeleteChattingReceiver, deleteChatFilter);

        IntentFilter securityChatOutFilter = new IntentFilter();
        securityChatOutFilter.addAction(MineTalk.BROAD_CAST_SECURITY_CHAT_OUT_MESSAGE);
        registerReceiver(mSecurityChattingOutReceiver, securityChatOutFilter);

        IntentFilter lockScreenFilter = new IntentFilter();
        lockScreenFilter.addAction(MineTalk.BROAD_CAST_LOCK_SCREEN_PAUSE_MESSAGE);
        registerReceiver(mLockerPauseReceiver, lockScreenFilter);
    }

    private BroadcastReceiver mLockerPauseReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean visible = intent.getExtras().getBoolean("visible", false);
            //mBinding.layoutLocker.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    };

    private BroadcastReceiver mNewChattingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshChatList();
        }
    };

    private BroadcastReceiver mDeleteChattingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
            refreshChatList();
        }
    };

    private BroadcastReceiver mSecurityChattingOutReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
            refreshChatList();
        }
    };

    private void chatListRequest(String user_xid) {
        ThreadListApi threadListApi = new ThreadListApi(MineTalkApp.getAppContext(), user_xid);
        threadListApi.request(new BaseHttpRequest.APICallbackListener<List<ThreadData>>() {
            @Override
            public void onSuccess(List<ThreadData> res) {

                ArrayList<ThreadData> threadList = new ArrayList<>();

                for (ThreadData data : res) {
                    //삭제된 메세지가 마지막 SEQ일떄만 푸쉬 메세지를 삭제 한다.
                    if( data.getThreadKey().equals(MineTalk.PUSH_THREAD_KEY) && data.getLastSeq().toString().equals(MineTalk.PUSH_DELETE_SEQ)){
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancel(MineTalk.PUSH_THREAD_KEY, 0);
                        break;
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                    }
                });
            }
        });
    }

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
//            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
//
//            if(!fingerprintManager.isHardwareDetected()){//Manifest에 Fingerprint 퍼미션을 추가해 워야 사용가능
//                //tv_message.setText("지문을 사용할 수 없는 디바이스 입니다.");
//            } else if(ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED){
//                //tv_message.setText("지문사용을 허용해 주세요.");
//                /*잠금화면 상태를 체크한다.*/
//            } else if(!keyguardManager.isKeyguardSecure()){
//                //tv_message.setText("잠금화면을 설정해 주세요.");
//            } else if(!fingerprintManager.hasEnrolledFingerprints()){
//                //tv_message.setText("등록된 지문이 없습니다.");
//            } else {//모든 관문을 성공적으로 통과(지문인식을 지원하고 지문 사용이 허용되어 있고 잠금화면이 설정되었고 지문이 등록되어 있을때)
//                //tv_message.setText("손가락을 홈버튼에 대 주세요.");
//
//                generateKey();
//                if(cipherInit()){
//                    cryptoObject = new FingerprintManager.CryptoObject(cipher);
//                    //핸들러실행
//                    FingerprintHandler fingerprintHandler = new FingerprintHandler(this);
//                    fingerprintHandler.startAutho(fingerprintManager, cryptoObject);
//                }
//            }
//        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Toast.makeText(this, "onResume", Toast.LENGTH_LONG).show();
        Log.d("Main Mode : ", "OnResume");

        //Toast.makeText(this, MineTalk.PUSH_SERVICE_THREAD_KEY, Toast.LENGTH_LONG).show();

        if (MineTalkApp.getUserInfoModel() == null) {
            SplashActivity.startActivity(MainActivity.this);
            finish();
            return;
        }

        if (!MineTalk.PUSH_SERVICE_THREAD_KEY.equals("")) {
            if (MineTalk.PUSH_SERVICE_SECURITY_KEY.equals("Y"))
                ChatDetailActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.PUSH_THREAD_KEY, true);
            else
                ChatDetailActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.PUSH_THREAD_KEY);

            //하단 메뉴를 채팅으로 이동
            setBottomMenuPosition(1);
        }

        if (MineTalk.AD_IDX != null && !MineTalk.AD_IDX.equals("")) {
            AdsMessageDetailActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.AD_LIST_TYPE_R, MineTalk.AD_IDX, MineTalk.AD_TITLE);
        }

        refreshFragment();

        if (MineTalk.SHARED_MESSAGE.length() != 0 || MineTalk.SHARED_IMAGE_MESSAGE.length() != 0) {
            if (MineTalk.SHARED_MESSAGE.length() != 0)
                TransmissionActivity.startActivity(MainActivity.this, MineTalk.SHARED_MESSAGE, MineTalk.REQUEST_CODE_TRANSMISSION);
            else if (MineTalk.SHARED_IMG_URI != null)
                TransmissionActivity.startActivity(MainActivity.this, MineTalk.REQUEST_CODE_TRANSMISSION);

            MineTalk.SHARED_MESSAGE = "";
            MineTalk.SHARED_IMAGE_MESSAGE = "";
        }

        IgawCommon.startSession(MainActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        IgawCommon.startSession(MainActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(mNewChattingReceiver);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        //Toast.makeText(this, "OnNewIntent", Toast.LENGTH_LONG).show();

        if (intent != null) {
            String action = intent.getAction();
            String mineType = intent.getType();

            if (action != null && Intent.ACTION_SEND.equals(action)) {
                goTrasmissionByIntent(action, mineType, intent);
            } else {
                //setBottomMenuPosition(0);
            }

            if(intent.getExtras() != null) {
                //채팅 푸쉬 알림의 경우
                String threadKey = intent.getExtras().getString("threadKey");
                String securityMode = intent.getExtras().getString("securityMode");

                if (threadKey != null && !threadKey.equals("")) {
                    Log.d("Security T : ", threadKey);
                    Log.d("Security M : ", securityMode);
                    if (securityMode.equals("Y")) { //보안 채팅 일 경우
                        ChatDetailActivity.startActivity(this, threadKey, true);
                    } else {
                        ChatDetailActivity.startActivity(this, threadKey);
                    }

                    setBottomMenuPosition(1);
                }
            }
        }

    }

    // 공유 하기
    private void goTrasmissionByIntent(@NonNull String action, @NonNull String mineType, @NonNull Intent receiveIntent) {
        if (Intent.ACTION_SEND.equals(action)) {
            if ("text/plain".equals(mineType)) {
                MineTalk.SHARED_MESSAGE = receiveIntent.getStringExtra(Intent.EXTRA_TEXT);
                MineTalk.SHARED_TEXT_SELECTED_MESSAGE = MineTalk.SHARED_MESSAGE;
            } else if (mineType.startsWith("image/")) {
                MineTalk.SHARED_IMAGE_MESSAGE = "image";
                MineTalk.SHARED_IMG_URI = receiveIntent.getParcelableExtra(Intent.EXTRA_STREAM);
            }
        }
    }

    private void initApi() {
        mSetMessageReceiveFlagApi = new SetMessageReceiveFlagApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MainActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(MainActivity.this);
                if (baseModel != null) {
                    userInfoRequest();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MainActivity.this);
                showMessageAlert(message);

            }

            @Override
            public void onCancellation() {

            }
        });

        mSecessionUserApi = new SecessionUserApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MainActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(MainActivity.this);
                if (baseModel != null) {

                    MessageThreadManager.getInstance().removeThreadAll();

                    MineTalkApp.saveLoginInfoClear();
                    LoginActivity.startActivity(MainActivity.this);
                    finish();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MainActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mLogoutApi = new LogoutApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MainActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(MainActivity.this);
                if (baseModel != null) {

                    MessageThreadManager.getInstance().removeThreadAll();

                    MineTalkApp.saveLoginInfoClear();
                    LoginActivity.startActivity(MainActivity.this);
                    finish();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MainActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mUploadContactApi = new UploadContactApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                if(mIsShowProgress) {
                    //ProgressUtil.showProgress(MainActivity.this);
                }

            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                //ProgressUtil.hideProgress(MainActivity.this);
                if(baseModel != null) {
                    //mListFriendApi.execute("friend");
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
//                ProgressUtil.hideProgress(BindActivity.this);
//                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRefreshListFriendApi = new ListFriendApi(this, new ApiBase.ApiCallBack<FriendListResponse>() {
            @Override
            public void onPreparation() {
                if(mIsShowProgress) {
                    //ProgressUtil.showProgress(MainActivity.this);
                }
            }

            @Override
            public void onSuccess(int request_code, FriendListResponse response) {
                //ProgressUtil.hideProgress(MainActivity.this);
                if(response != null) {
                    //MineTalkApp.setRecommendList(response.getRecommend_list());

                    MineTalkApp.setFriendList(response.getFriend_list());
                    ((FriendPagerFragment) mAdapter.getItem(0)).refreshView();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MainActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mDataObserver = new OnDataObserverListener() {
            @Override
            public void friendListLoadComplete() {
                refreshFragment();
            }
        };
    }

    private void drawBottomNavigation(AHBottomNavigation bottomNavigation) {
        bottomNavigation.removeAllItems();
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
        ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();
        if (MineTalk.isBlackTheme) {
            bottomNavigation.setDefaultBackgroundResource(R.color.bk_theme_status);
            bottomNavigation.setAccentColor(Color.parseColor("#cdb60c"));
            bottomNavigation.setInactiveColor(Color.parseColor("#858992"));

            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_1_selector_bk));
            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_2_selector_bk));
            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_3_selector_bk));
            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_4_selector_bk));
            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_5_selector_bk));
            bottomNavigation.addItems(bottomNavigationItems);
        } else {
            bottomNavigation.setDefaultBackgroundResource(R.color.bottom_menu_bg);
            bottomNavigation.setAccentColor(Color.parseColor("#16c066"));
            bottomNavigation.setInactiveColor(Color.parseColor("#858992"));

            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_1_selector));
            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_2_selector));
            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_3_selector));
            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_4_selector));
            bottomNavigationItems.add(new AHBottomNavigationItem("", R.drawable.navi_menu_5_selector));
            bottomNavigation.addItems(bottomNavigationItems);
        }
    }

    private void setupBottonNavigation() {
        mBinding.contentMain.bottomNavigation.setEnabled(false);
        mBinding.contentMain.viewPager.setOffscreenPageLimit(5);

        final AHBottomNavigation bottomNavigation = mBinding.contentMain.bottomNavigation;
        final AHBottomNavigationViewPager viewPager = mBinding.contentMain.viewPager;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter = new MainFragmentPagerAdapter(getSupportFragmentManager(), mOnFragmentListener, mProfileListner);
                viewPager.setAdapter(mAdapter);

                try {
                    JSONArray contactParams = makeContactParams(getContactList());
                    mUploadContactApi.execute(contactParams);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        drawBottomNavigation(bottomNavigation);

        //하단 메뉴 Paging 막는다.
        viewPager.setPagingEnabled(false);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                mViewPagerPosition = position;
                bottomNavigation.setCurrentItem(position, true);

                refreshFragment();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //첫 페이지 화면을 채팅으로 설정
        viewPager.setCurrentItem(mViewPagerPosition, false);

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.setForceTint(false);


        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (bottomNavigation.getCurrentItem() == position) {
                    return false;
                }

                mBinding.contentMain.viewPager.setCurrentItem(position, false);

                invalidateOptionsMenu();

                return true;
            }
        });

    }

    public ArrayList<ContactModel> getContactList() {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        };
        String[] selectionArgs = null;
        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor cursor = mContentResolver.query(uri, projection, null, selectionArgs, sortOrder);
        ArrayList<ContactModel> contactList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                String phoneNumber = cursor.getString(0);
                String name = cursor.getString(1);
                if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
                    continue;
                }
                phoneNumber = phoneNumber.replaceAll("-", "");
                contactList.add(new ContactModel(name, phoneNumber));
            } while (cursor.moveToNext());
        }
        return contactList;
    }

    public void setBottomMenuPosition(int pos) {
        //하단 메뉴를 채팅으로 이동 한다.
        mBinding.contentMain.viewPager.setCurrentItem(pos);
    }

    public OnFragmentEventListener getOnFragmentEventListner() {
        return mOnFragmentListener;
    }

    private OnFragmentEventListener mOnFragmentListener = new OnFragmentEventListener() {
        @Override
        public void reqBuyCoupon() {
            String message = "현재 서비스 개선중입니다.\n" +
                    "본 서비스를 이용하시려면 1:1문의에 연락처를 남겨주시면 상담 전화 드리겠습니다.\n\n" +
                    "1833-3245";
            showMessageAlert(message);
        }

        @Override
        public void logout() {
            showMessageAlert(getResources().getString(R.string.logout_message),
                    getResources().getString(R.string.common_ok),
                    getResources().getString(R.string.common_cancel),
                    new PopupListenerFactory.SimplePopupListener() {
                        @Override
                        public void onClick(DialogInterface f, int state) {
                            if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
//                                MineTalkApp.saveLoginInfoClear();
//                                LoginActivity.startActivity(MainActivity.this);
//                                finish();
                                logoutRequest();
                            }
                        }
                    });
        }

        @Override
        public void withDrawal() {
            showMessageAlert(getResources().getString(R.string.width_drawal_message),
                    getResources().getString(R.string.common_ok),
                    getResources().getString(R.string.common_cancel),
                    new PopupListenerFactory.SimplePopupListener() {
                        @Override
                        public void onClick(DialogInterface f, int state) {
                            if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                showAuthPopup();
                            }
                        }
                    });
        }

        @Override
        public void changeReceiveMessageFlag(String flag) {
            setMessageFlagRequest(flag);
        }

        @Override
        public void showProfile(FriendListModel data) {
            //showProfilePopup(BottomProfilePopup.ViewType.NORMAl, data, mProfileListner);
        }

        @Override
        public void showProfile(FriendListModel data, FriendProfileActivity.FriendType type) {
            //showProfilePopup(BottomProfilePopup.ViewType.NORMAl, data, mProfileListner);
            FriendProfileActivity.startActivity(MainActivity.this, data, type);
        }

        @Override
        public void selectLanguage() {
//            mBinding.layoutSelectLanguage.init();
//            mBinding.layoutSelectLanguage.setVisibility(View.VISIBLE);
            MyLanguageActivity.startActivity(MainActivity.this, MyLanguageActivity.TYPE_MY_LANGUAGE);
        }

        @Override
        public void makeChatting() {
            mBinding.layoutCreateChat.updateTheme();
            mBinding.layoutCreateChat.setVisibility(View.VISIBLE);
        }

        @Override
        public void searchChat() {
            FriendSearchActivity.startActivity(MainActivity.this);
        }

        @Override
        public void addFriend(FriendListModel data) {
            addFriendRequest(data.getUser_xid());
        }

        @Override
        public void makeChattingToMe() {

        }

        @Override
        public void editProfile() {

        }

        @Override
        public void showMyProfile() {
            UserInfoBaseModel meInfo = MineTalkApp.getUserInfoModel();
            showMyProfilePopup(BottomProfilePopup.ViewType.ME, meInfo, mProfileListner);
        }

        @Override
        public void onRefresh() {
            userInfoRequest();
        }

        @Override
        public void onFaq() {
            FAQActivity.startActivity(MainActivity.this);
        }

        @Override
        public void onQna() {
            QnAActivity.startActivity(MainActivity.this);
        }

        @Override
        public void onRecommendManager() {
            RecommendActivity.startActivity(MainActivity.this);
        }

        @Override
        public void onPolicy() {
            TermsActivity.startActivity(MainActivity.this);
        }

        @Override
        public void onChangeTheme() {
            updateTheme();
            drawBottomNavigation(mBinding.contentMain.bottomNavigation);
        }

        @Override
        public void onEvent() {
            EventActivity.startActivity(MainActivity.this);
        }

        @Override
        public void onUpdate() {
            UpdateActivity.startActivity(MainActivity.this);
        }

        @Override
        public void onUpdateFriendList() {
            mRefreshListFriendApi.execute("friend");
        }

        @Override
        public void onSendSMS(String toName, String toPhoneNo) {
            String userPhoneNumber = MineTalkApp.getUserInfoModel().getUser_hp();
            String userPhoneAuth   = MineTalkApp.getUserInfoModel().getUser_hp_confirm();

            if(userPhoneNumber.equals("") || !userPhoneAuth.equals("Y")){
                showMessageAlert(getString(R.string.phone_auth_need_warning));
            }else{
                showMessageAlert(String.format(getResources().getString(R.string.popup_send_invite_message), toName),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    String sendUserName = MineTalkApp.getUserInfoModel().getUser_name();
                                    String sendUserID = MineTalkApp.getUserInfoModel().getUser_login_id();
                                    String msg = String.format(getResources().getString(R.string.invite_sms_msg),
                                            sendUserName,
                                            sendUserID);

                                    sendSMS(getApplicationContext(), toPhoneNo, msg);
                                }
                            }
                        });
            }
        }

        @Override
        public void onUpdateBottomMenuNotification(int notiCnt, int index) {
            mBinding.contentMain.bottomNavigation.setNotification(Integer.toString(notiCnt), index);
        }

        @Override
        public void onShowMessagePopupByLimitCheck() {
            showMessageWithWarningAlert("한도조회는 다음번 업데이트부터 지원합니다.",
                    "조회되는 데이터는 샘플입니다.",
                    "닫기",
                    new PopupListenerFactory.SimplePopupListener() {
                        @Override
                        public void onClick(DialogInterface f, int state) {
                            if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                LimitCheckActivity.startActivity(MainActivity.this);
                            }
                        }
                    });
        }

        @Override
        public void onShowMessagePopupBySaveHistory() {
            showMessageWithWarningAlert("적립이력은 다음번 업데이트부터 지원합니다.",
                    "조회되는 데이터는 샘플입니다.",
                    "닫기",
                    new PopupListenerFactory.SimplePopupListener() {
                        @Override
                        public void onClick(DialogInterface f, int state) {
                            if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                SaveHistoryActivity.startActivity(MainActivity.this);
                            }
                        }
                    });
        }

        @Override
        public void showMessagePopup(String message) {
            showMessageAlert(message);
        }
    };

    public void sendSMS(Context context, String phone, String msg) {
        try {
            Uri smsUri = Uri.parse("sms:" + phone);
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, smsUri);
            sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            sendIntent.putExtra("sms_body", msg);
            context.startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setMessageFlagRequest(String messageFlag) {
        mSetMessageReceiveFlagApi.execute(messageFlag);
    }

    private void secessionUserRequest(String authNumber) {
        mSecessionUserApi.execute(authNumber);
    }

    //새로운 메세지 도착으로 인해 메세지 정보 업데이트
    private void refreshChatList() {
        ChatPagerFragment chatFragment = (ChatPagerFragment) mAdapter.getItem(1);
        chatFragment.refreshChatList();
    }

    private void refreshFragment() {
        if (mViewPagerPosition == 0) {
            mRefreshListFriendApi.execute("friend");
            //((FriendPagerFragment) mAdapter.getItem(mViewPagerPosition)).refreshView();
        } else if (mViewPagerPosition == 1) {
            //ChatPagerFragment chatFragment = (ChatPagerFragment)mAdapter.getItem(mViewPagerPosition);
            //chatFragment.refresh();
        } else if (mViewPagerPosition == 2) {

        } else if (mViewPagerPosition == 3) {

        } else if (mViewPagerPosition == 4) {
            MyMineFragment myMineFragment = (MyMineFragment) mAdapter.getItem(mViewPagerPosition);
            myMineFragment.refreshView();
        }

        // 연락처 수가 로딩 된 수가 다를 경우 다시 싱크를 맞춘다.
//        if( CommonUtils.getContactListCount(getApplicationContext()) != mContactListCount ){
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        int page = 0;
//                        String para = "";
//                        ArrayList<ContactModel> contactAll = new ArrayList<>();
//                        ArrayList<ContactModel> contacts;
//
//                        mContactListCount = CommonUtils.getContactListCount(MineTalkApp.getCurrentActivity());
//                        int nCnt = (mContactListCount / MineTalk.CONTACT_RANGE) + 1;
//                        for( int i = 0; i < nCnt; ++ i){
//                            para = String.format("%d, %d", page * MineTalk.CONTACT_RANGE, MineTalk.CONTACT_RANGE);
//                            contacts = CommonUtils.getContactList(MineTalkApp.getCurrentActivity(), para);
//                            contactAll.addAll(contacts);
//                            ++page;
//                        }
//
//                        JSONArray contactParams = makeContactParams(contactAll);
//                        mUploadContactApi.execute(contactParams);
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//        }
    }


    public OnProfileListener getOnProfileListner() {
        return mProfileListner;
    }

    private OnProfileListener mProfileListner = new OnProfileListener() {
        @Override
        public void onTalk(FriendListModel data) {
            setBottomMenuPosition(1);
            newThreadRequest(MineTalkApp.getUserInfoModel().getUser_xid(), data.getUser_xid());
        }

        @Override
        public void onFavorite(FriendListModel data, String yn) {
            FriendManager.getInstance().updateFavorite(data.getUser_xid(), yn);
            MineTalkApp.setFriendList(FriendManager.getInstance().selectFriend());

            refreshFragment();
        }

        @Override
        public void onCall(FriendListModel data) {

//            FriendManager.getInstance().removeFriend(data.getUser_xid());
//            MineTalkApp.setFriendList(FriendManager.getInstance().selectFriend());
//            refreshFragment();
//            userBlockRequest(data.getUser_xid());

        }

        @Override
        public void onGift(FriendListModel data) {
            PointSendActivity.startActivity(MainActivity.this, PointSendActivity.TYPE_GIFT_POINT, PointSendActivity.MODE_TOKEN, data.getUser_xid(), data.getUser_name(), data.getUser_profile_image());
        }

        @Override
        public void onBlock(FriendListModel data) {
            FriendManager.getInstance().removeFriend(data.getUser_xid());
            MineTalkApp.setFriendList(FriendManager.getInstance().selectFriend());
            userBlockRequest(data.getUser_xid());
            refreshFragment();
        }

        @Override
        public void onFriendAdd(FriendListModel data) {
            addFriendRequest(data.getUser_xid());
        }

        @Override
        public void onTalkMe(UserInfoBaseModel data) {
            newThreadRequest(MineTalkApp.getUserInfoModel().getUser_xid(), data.getUser_xid());

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setBottomMenuPosition(1);
                }
            }, 500);
        }

        @Override
        public void onEditProfile() {
            EditProfileActivity.startActivity(MainActivity.this);
        }
    };

    private void newThreadRequest(String user_xid, String friend_xid) {
        //ProgressUtil.showProgress(this);
        CreateThreadApi createThreadApi = new CreateThreadApi(this, user_xid, friend_xid);
        createThreadApi.request(new BaseHttpRequest.APICallbackListener<ThreadData>() {
            @Override
            public void onSuccess(ThreadData res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //ProgressUtil.hideProgress(MainActivity.this);
                        mAdapter.notifyDataSetChanged();

                        Log.e("@@@TAG", "new Thread success");

                        if (user_xid.equals(friend_xid)) {
                            Preferences.setMyChattingThreadKey(res.getThreadKey());
                            String title = getString(R.string.popup_profile_button_chat_me);

                            ChatDetailActivity.startActivity(MainActivity.this, res.getThreadKey(), title);
                        } else {
                            String threadName = "";
                            if (res.getMembers().size() > 0) {
                                String members = "";
                                for (ThreadMember tmember : res.getMembers()) {
                                    if (!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                        String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                        if(contactName == null || contactName.equals("")) {
                                            members = members + tmember.getNickName();
                                        } else {
                                            members = members + contactName;
                                        }
                                        break;
                                    }
                                }
                                threadName = members;
                            }
                            ChatDetailActivity.startActivity(MainActivity.this, res.getThreadKey(), threadName);
                        }


                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(MainActivity.this);
                        Log.e("@@@TAG", "new Thread onFailure");
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    public void logoutRequest() {
        mLogoutApi.execute();
    }

    private OnSyncListener mMainSyncListner = new OnSyncListener() {
        @Override
        public void onSyncComplete() {
            mAdapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MineTalk.REQUEST_CODE_TRANSMISSION) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.getExtras() != null) {
                    String threadKey = data.getExtras().getString("select_threadKey", "");
                    if (threadKey != null && !threadKey.equals("")) {
                        ChatDetailActivity.startActivity(MainActivity.this, threadKey);
                        setBottomMenuPosition(1);
                    }
                }
            } else {
                MineTalk.SHARED_MESSAGE = "";
                MineTalk.SHARED_TEXT_SELECTED_MESSAGE = "";
                MineTalk.SHARED_IMAGE_MESSAGE = "";
                MineTalk.SHARED_IMG_URI = null;
            }

        }

    }

    @Override
    public void onBackPressed() {
        if (mViewPagerPosition == 3) {
            ShopFragment shopFragment = (ShopFragment) mAdapter.getItem(3);
            if (!shopFragment.canHistoryBack()) {
                AppLocker.getInstance().enableAppLock(getApplication(), false);
                finish();
            }
        } else {
            AppLocker.getInstance().enableAppLock(getApplication(), false);
            finish();
        }
    }

    private void showAuthPopup() {
        SimpleAuthPopup simpleAuthPopup = new SimpleAuthPopup();
        simpleAuthPopup.setSmsType(MineTalk.SMS_TYPE_SECESSION);
        simpleAuthPopup.setEventListener(new SimpleAuthPopup.OnEventListener() {
            @Override
            public void onConfirm(String authNumber) {
                secessionUserRequest(authNumber);
            }
        });
        simpleAuthPopup.show(getSupportFragmentManager(), "popup_auth");
    }


    private long getDateDiff(String target, String source) {
        long diffDay = -1;
        if (target == null || target.length() == 0) return -1;
        String strFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
        try {
            Date startDate = sdf.parse(target);
            Date endDate = sdf.parse(source);

            //두날짜 사이의 시간 차이(ms)를 하루 동안의 ms(24시*60분*60초*1000밀리초) 로 나눈다.
            diffDay = (startDate.getTime() - endDate.getTime()) / (24 * 60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diffDay;
    }
}
