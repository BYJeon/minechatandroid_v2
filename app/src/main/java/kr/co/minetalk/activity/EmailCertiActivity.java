package kr.co.minetalk.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.SmsMessage;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.CheckAddCertificationApi;
import kr.co.minetalk.api.CheckUserEmailApi;
import kr.co.minetalk.api.CheckUserHPApi;
import kr.co.minetalk.api.PwinquiryApi;
import kr.co.minetalk.api.RequestEmailCertificationApi;
import kr.co.minetalk.api.RequestPhoneCertificationApi;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.PwInquiryModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEmailCertiBinding;
import kr.co.minetalk.databinding.ActivityPhoneCertiBinding;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class EmailCertiActivity extends BindActivity<ActivityEmailCertiBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EmailCertiActivity.class);
        context.startActivity(intent);
    }

    private RequestSmsApi mRequestSmsApi;
    private PwinquiryApi mPwinquiryApi;

    private boolean isEmailDuplicateCheck = false;

    private RequestEmailCertificationApi mEmailCertiApi;
    private CheckUserEmailApi mCheckUserEmailApi;
    private CheckAddCertificationApi mCheckAddCertiApi;

    private int mRemainTime = 600;
    private Timer mTimer = null;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
        //syncFriendList();
        //MainActivity.startActivity(LoginActivity.this);
        //finish();
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        Preferences.setLoginAuthorization("");
        Preferences.setAuthorization("");
        Preferences.setLoginType("");

        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_email_certi;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        if(MineTalkApp.getUserInfoModel().getUser_email_confirm().equals("Y") && !MineTalkApp.getUserInfoModel().getUser_email().equals("")){ //인증이 되어있는 경우
            mBinding.layoutOrgRoot.setVisibility(View.VISIBLE);
            mBinding.tvOrgEmail.setText(MineTalkApp.getUserInfoModel().getUser_email());
            mBinding.tvPhoneAuthInfoTitle.setText(getString(R.string.change_email_title));
        }else{
            mBinding.tvPhoneAuthInfoTitle.setText(getString(R.string.new_email_title));
            mBinding.layoutOrgRoot.setVisibility(View.GONE);
        }

//        if(Build.VERSION.SDK_INT >= 23) {
//            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
//                    ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
//                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.RECEIVE_SMS,
//                        Manifest.permission.READ_SMS}, PERMISSON);
//            } else {
//                initSMSCatchReceiver();
//            }
//        }
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });


        //중복확인
        mBinding.btnCheckDuplicate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);

                String email = mBinding.etEmail.getText().toString();

                mBinding.layoutSendEmail.setVisibility(View.INVISIBLE);
                mBinding.layoutWarningEmail.setVisibility(View.INVISIBLE);

                if(email.equals("")){
                    showMessageAlert(getString(R.string.email_format_empty_warning));
                    return;
                }

                mCheckUserEmailApi.execute(email);
            }
        });

        //인증번호 발송
        mBinding.btnSendAuthNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mBinding.etEmail.getText().toString();

                if(!isEmailDuplicateCheck){
                    showMessageAlert("중복 확인을 해주세요.");
                    return;
                }

                mEmailCertiApi.execute(email);
            }
        });


        //인증 요청
        mBinding.btnSendAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String authNum = mBinding.etAuthNumber.getText().toString().trim();

                if(authNum.equals("")){
                    showMessageAlert(getString(R.string.email_empty_auth_num));
                    return;
                }

                if(!isEmailDuplicateCheck){
                    showMessageAlert(getString(R.string.phone_do_not_auth_num));
                    return;
                }

                mCheckAddCertiApi.execute("email", authNum);
            }
        });

    }

    private void initApi() {
        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EmailCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EmailCertiActivity.this);
                if(baseModel != null) {
                    showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EmailCertiActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mEmailCertiApi = new RequestEmailCertificationApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EmailCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EmailCertiActivity.this);
                if(baseModel != null && baseModel.getCode().equals("0000")) {
                    mBinding.btnSendAuth.setEnabled(true);
                    mBinding.btnSendAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);

                    //인증 번호 발송 성공
                    showMessageAlert(getResources().getString(R.string.success_send_email_certification));

                    mBinding.tvRemainTime.setVisibility(View.VISIBLE);
                    TimerStop();

                    mTimer = new Timer();
                    mTimer.schedule(new CustomTimer(), 1000, 1000);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                TimerStop();

                ProgressUtil.hideProgress(EmailCertiActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckUserEmailApi = new CheckUserEmailApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EmailCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EmailCertiActivity.this);
                if(baseModel != null && baseModel.getCode().equals("0000")) {
                    mBinding.tvInputAuthNumDesc.setVisibility(View.VISIBLE);
                    //중복
                    //showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));

                    mBinding.layoutSendEmail.setVisibility(View.VISIBLE);
                    mBinding.layoutWarningEmail.setVisibility(View.INVISIBLE);
                    isEmailDuplicateCheck = true;

                    //중복이 없을 시 인증 번호 발송 요청을 한다.
                    String email = mBinding.etEmail.getText().toString();
                    mEmailCertiApi.execute(email);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mBinding.tvInputAuthNumDesc.setVisibility(View.INVISIBLE);
                ProgressUtil.hideProgress(EmailCertiActivity.this);
                showMessageAlert(message);
                mBinding.layoutSendEmail.setVisibility(View.INVISIBLE);
                mBinding.layoutWarningEmail.setVisibility(View.VISIBLE);
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                isEmailDuplicateCheck = false;
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckAddCertiApi = new CheckAddCertificationApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EmailCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EmailCertiActivity.this);
                if(baseModel != null && baseModel.getCode().equals("0000")) {
                    MineTalkApp.getUserInfoModel().setUser_email_confirm("Y");
                    MineTalkApp.getUserInfoModel().setUser_email(mBinding.etEmail.getText().toString());
                    mBinding.etAuthNumber.setText("");
                    mBinding.etEmail.setText("");
                    TimerStop();
                    mBinding.tvRemainTime.setVisibility(View.INVISIBLE);

                    mBinding.btnSendAuth.setEnabled(false);
                    mBinding.btnSendAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);

                    //인증완료
                    showMessageAlert(getResources().getString(R.string.success_add_certification));
                }else if(baseModel != null && baseModel.getCode().equals("0002")) {
                    MineTalkApp.getUserInfoModel().setUser_email_confirm("Y");
                    MineTalkApp.getUserInfoModel().setUser_email(mBinding.etEmail.getText().toString());
                    mBinding.etAuthNumber.setText("");
                    mBinding.etEmail.setText("");
                    TimerStop();
                    mBinding.tvRemainTime.setVisibility(View.INVISIBLE);

                    mBinding.btnSendAuth.setEnabled(false);
                    mBinding.btnSendAuth.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);

                    //탈퇴자 재인증
                    showMessageAlert(baseModel.getMessage());
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                TimerStop();
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                ProgressUtil.hideProgress(EmailCertiActivity.this);
                showMessageAlert(message);
                mBinding.layoutSendEmail.setVisibility(View.INVISIBLE);
                mBinding.layoutWarningEmail.setVisibility(View.INVISIBLE);
                //isEmailDuplicateCheck = false;
            }

            @Override
            public void onCancellation() {

            }
        });

        mPwinquiryApi = new PwinquiryApi(this, new ApiBase.ApiCallBack<PwInquiryModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EmailCertiActivity.this);
            }

            @Override
            public void onSuccess(int request_code, PwInquiryModel pwInquiryModel) {
                ProgressUtil.hideProgress(EmailCertiActivity.this);
                if(pwInquiryModel != null) {
                    //mBinding.tvFindPassword.setText(pwInquiryModel.getTemp_password());
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EmailCertiActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mAuthSMSBroadCastReceiver != null) {
            unregisterReceiver(mAuthSMSBroadCastReceiver);
        }

        TimerStop();
    }

    private void TimerStop(){
        if(mTimer != null)
            mTimer.cancel();

        mRemainTime = 600;
    }

    private static final String ACTION_SMS = "android.provider.Telephony.SMS_RECEIVED";
    private BroadcastReceiver mAuthSMSBroadCastReceiver;
    private void initSMSCatchReceiver() {

        IntentFilter filter = new IntentFilter();
        filter.setPriority(999);
        filter.addAction(ACTION_SMS);


        mAuthSMSBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent.getAction().equals(ACTION_SMS)) {

                    Bundle bundle = intent.getExtras();
                    if(bundle == null)
                        return;

                    Object[] pdusObj = (Object[]) bundle.get("pdus");
                    if(pdusObj == null)
                        return;

                    SmsMessage[] smsMessages = new SmsMessage[pdusObj.length];

                    for (int i = 0; i < pdusObj.length; i++) {

                        smsMessages[i] = SmsMessage.createFromPdu((byte[])pdusObj[i]);

                        final String address = smsMessages[i].getOriginatingAddress();
                        final String message = smsMessages[i].getDisplayMessageBody();

                        String smsPhone = Preferences.getSmsPhone();
                        if( smsPhone.equals(address.replace("-","")) ) {
                            abstrackNumber(message);
                        }
                    }

                    this.abortBroadcast();
                }
            }

            private void abstrackNumber(String message) {
                Pattern p = Pattern.compile("([^\\d])");
                Matcher matcher = p.matcher(message);
                StringBuffer destStringBuffer = new StringBuffer();

                while (matcher.find()){
                    matcher.appendReplacement(destStringBuffer, "");
                }

                matcher.appendTail(destStringBuffer);
                mBinding.etAuthNumber.setText(destStringBuffer.toString());

            }

        };
        registerReceiver(mAuthSMSBroadCastReceiver, filter);
    }

    private final int PERMISSON = 1;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initSMSCatchReceiver();
                } else {
                    return;
                }
        }
    }

    // 첫 번째 TimerTask 를 이용한 방법
    class CustomTimer extends TimerTask {
        @Override
        public void run() {
            mRemainTime -= 1;

            if(mRemainTime == 0){
                mBinding.tvRemainTime.setVisibility(View.INVISIBLE);
                showMessageAlert(getResources().getString(R.string.phone_time_over_auth_num));
                TimerStop();
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBinding.tvRemainTime.setText(String.format("%02d:%02d", mRemainTime / 60 , mRemainTime % 60) );
                }
            });


        }
    }
}
