package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.GoogleTranslateApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.TranslateModel;
import kr.co.minetalk.databinding.ActivityVoiceTranslationBinding;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.ui.popup.SimpleListPopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class VoiceTranslationActivity extends BindActivity<ActivityVoiceTranslationBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, VoiceTranslationActivity.class);
        context.startActivity(intent);
    }

    private GoogleTranslateApi mGoogleTranslateApi;

    private String mTopLanguageCode = "";
    private String mTopVoiceCode    = "";
    private int    mTopFlagResource = -1;

    private String mBottomLanguageCode = "";
    private String mBottomVoiceCode    = "";
    private int    mBottomFlagResource = -1;

    private CountryRepository mCountryRepository;
    public static final int REQUEST_SPEECH_MSG = 1001;

    private final int STATE_TOP = 0;
    private final int STATE_BOT = 1;

    private int mCurrentState = 0;

    private TextToSpeech mTTS;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_voice_translation;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        mCountryRepository = CountryRepository.getInstance();

        initApi();
        setUIEventListener();

        initSetting();
        updateTheme();
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutTransBg.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.ivButtonTopVoice.setImageResource(R.drawable.btn_mic_bk);
            mBinding.ivButtonBottomVoice.setImageResource(R.drawable.btn_mic_bk);
        }else{
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutTransBg.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.ivButtonTopVoice.setImageResource(R.drawable.btn_mic_b);
            mBinding.ivButtonBottomVoice.setImageResource(R.drawable.btn_mic_b);
        }
    }

    private void initTTS(final String langCode, String text) {
        if(mTTS != null) {
            mTTS.stop();
            mTTS.shutdown();
            mTTS = null;
        }

        mTTS = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    //mTTS.setEngineByPackageName("com.google.android.tts");

                    Locale loc = new Locale(langCode);
                    mTTS.setLanguage(loc);

                    mTTS.setPitch(1.0f);
                    mTTS.setSpeechRate(1.0f);
                    mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        }, "com.google.android.tts");
    }

    private void initApi() {
        mGoogleTranslateApi = new GoogleTranslateApi(this, new ApiBase.ApiCallBack<TranslateModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(VoiceTranslationActivity.this);
            }

            @Override
            public void onSuccess(int request_code, TranslateModel translateModel) {
                ProgressUtil.hideProgress(VoiceTranslationActivity.this);
                if(translateModel != null) {
                    if(mCurrentState == STATE_TOP) {
                        mBinding.tvBottomTransResult.setText(translateModel.getText());
                        initTTS(mBottomLanguageCode, translateModel.getText());
                    } else {
                        mBinding.tvTopTransResult.setText(translateModel.getText());
                        initTTS(mTopLanguageCode, translateModel.getText());
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(VoiceTranslationActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void initSetting() {
        String topLanguage = Preferences.getVoiceTopLanguage();
        if(topLanguage.equals("")) {
            mTopLanguageCode = mCountryRepository.getCountryModel("en").getCode();
            mTopVoiceCode    = mCountryRepository.getCountryModel("en").getVoice_code();
            mTopFlagResource = CommonUtils.getResourceImage(this,mCountryRepository.getCountryModel("en").getFlag_image());
        } else {
            mTopLanguageCode = mCountryRepository.getCountryModel(topLanguage).getCode();
            mTopVoiceCode    = mCountryRepository.getCountryModel(topLanguage).getVoice_code();
            mTopFlagResource = CommonUtils.getResourceImage(this,mCountryRepository.getCountryModel(topLanguage).getFlag_image());
        }


        String bottomLanguage = Preferences.getVoiceBottomLanguage();
        if(bottomLanguage.equals("")) {
            mBottomLanguageCode = mCountryRepository.getCountryModel("ko").getCode();
            mBottomVoiceCode    = mCountryRepository.getCountryModel("ko").getVoice_code();
            mBottomFlagResource = CommonUtils.getResourceImage(this,mCountryRepository.getCountryModel("ko").getFlag_image());
        } else {
            mBottomLanguageCode = mCountryRepository.getCountryModel(bottomLanguage).getCode();
            mBottomVoiceCode    = mCountryRepository.getCountryModel(bottomLanguage).getVoice_code();
            mBottomFlagResource = CommonUtils.getResourceImage(this,mCountryRepository.getCountryModel(bottomLanguage).getFlag_image());
        }

        mBinding.ivButtonTopLanguage.setImageResource(mTopFlagResource);
        mBinding.ivButtonBottomLanguage.setImageResource(mBottomFlagResource);
    }

    private void setUIEventListener() {

        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.ivButtonTopLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupTopLanuge();
            }
        });


        mBinding.ivButtonBottomLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupBottomLanuge();
            }
        });

        mBinding.ivButtonTopVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentState = STATE_TOP;
                try {
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//                    intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, mContext.getPackageName());
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, mTopVoiceCode);
                    intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getResources().getString(R.string.speech_title_msg));
                    startActivityForResult(intent,MineTalk.REQUEST_CODE_STT_TOP);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mBinding.ivButtonBottomVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentState = STATE_BOT;
                try {
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//                    intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, mContext.getPackageName());
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, mBottomVoiceCode);
                    intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getResources().getString(R.string.speech_title_msg));
                    startActivityForResult(intent,MineTalk.REQUEST_CODE_STT_BOTTOM);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void popupTopLanuge() {
        SimpleListPopup simpleListPopup = new SimpleListPopup();
        simpleListPopup.setOnItemIndexListener(new SimpleListPopup.OnItemIndexListener() {
            @Override
            public void onItemClick(int index) {
                mTopLanguageCode = mCountryRepository.getItem(index).getCode();
                mTopVoiceCode    = mCountryRepository.getItem(index).getVoice_code();
                mTopFlagResource = CommonUtils.getResourceImage(VoiceTranslationActivity.this, mCountryRepository.getItem(index).getFlag_image());

                mBinding.ivButtonTopLanguage.setImageResource(mTopFlagResource);
                mBinding.ivButtonBottomLanguage.setImageResource(mBottomFlagResource);

                Preferences.setVoiceTopLanguage(mTopLanguageCode);
            }
        });
        simpleListPopup.show(getSupportFragmentManager(), "popup_top_lang");
    }

    private void popupBottomLanuge() {
        SimpleListPopup simpleListPopup = new SimpleListPopup();
        simpleListPopup.setOnItemIndexListener(new SimpleListPopup.OnItemIndexListener() {
            @Override
            public void onItemClick(int index) {
                mBottomLanguageCode = mCountryRepository.getItem(index).getCode();
                mBottomVoiceCode    = mCountryRepository.getItem(index).getVoice_code();
                mBottomFlagResource = CommonUtils.getResourceImage(VoiceTranslationActivity.this, mCountryRepository.getItem(index).getFlag_image());

                mBinding.ivButtonTopLanguage.setImageResource(mTopFlagResource);
                mBinding.ivButtonBottomLanguage.setImageResource(mBottomFlagResource);

                Preferences.setVoiceBottomLanguage(mBottomLanguageCode);
            }
        });
        simpleListPopup.show(getSupportFragmentManager(), "popup_bot_lang");
    }



    private void translationRequest(String sourceLanguageCode, String sourceText, String targetLanguageCode) {
        mGoogleTranslateApi.execute(sourceText, sourceLanguageCode, targetLanguageCode);
    }


    private int getCountryImageResource(String languageCode) {
        if(languageCode.equals("ko")) {
            return R.drawable.btn_option_korean;
        } else if(languageCode.equals("zh-CN")) {
            return R.drawable.btn_option_china;
        } else if(languageCode.equals("en")) {
            return R.drawable.btn_option_english;
        } else if(languageCode.equals("ja")) {
            return R.drawable.btn_option_japan;
        } else if(languageCode.equals("vi")) {
            return R.drawable.btn_option_vietnam;
        } else  {
            return R.drawable.btn_option_korean;
        }
    }

    private Bitmap makeReverseImage(int resId) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), resId);
        Bitmap reverseBitmap = null;

        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, -1.0f);
        reverseBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);

        return reverseBitmap;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MineTalk.REQUEST_CODE_STT_TOP && resultCode == RESULT_OK) {
            mCurrentState = STATE_TOP;
            ArrayList<String> sstResult = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String result_sst = sstResult.get(0);
            mBinding.tvTopTransResult.setText(result_sst);
            translationRequest(mTopLanguageCode, result_sst, mBottomLanguageCode);

        } else if(requestCode == MineTalk.REQUEST_CODE_STT_BOTTOM && resultCode == RESULT_OK) {
            mCurrentState = STATE_BOT;
            ArrayList<String> sstResult = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String result_sst = sstResult.get(0);
            mBinding.tvBottomTransResult.setText(result_sst);
            translationRequest(mBottomLanguageCode, result_sst, mTopLanguageCode);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mTTS != null) {
            mTTS.stop();
            mTTS.shutdown();
            mTTS = null;
        }
    }
}
