package kr.co.minetalk.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Message;
import android.provider.Browser;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityGamePlayBinding;
import kr.co.minetalk.ui.data.PopupListenerFactory;

public class PointGamePlayActivity extends BindActivity<ActivityGamePlayBinding> {
    private final int PERMISSON = 1;

    private static final String COLUMN_GAME_TYPE_BLOCK = "column_game_type_block";

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PointGamePlayActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, PointGameActivity.GameType gameType) {
        Intent intent = new Intent(context, PointGamePlayActivity.class);
        intent.putExtra(COLUMN_GAME_TYPE_BLOCK, gameType);
        context.startActivity(intent);
    }

    private WebSettings mWebSettings;
    private WebSettings mWebGuideSettings;

    private PointGameActivity.GameType mGameType = PointGameActivity.GameType.BLOCK;
    //게임 진행 여부
    private boolean bPlayGame = false;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_game_play;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        if (getIntent() != null && getIntent().getExtras() != null) {
            mGameType = (PointGameActivity.GameType) getIntent().getExtras().getSerializable(COLUMN_GAME_TYPE_BLOCK);
        }
        initApi();
        setUIEventListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Log.d("KeyUP Event", "뒤로 가기 키 down");
                if(mBinding.webGuide.getVisibility() == View.VISIBLE){
                    mBinding.webGuide.setVisibility(View.GONE);
                }else{
                    closeGame();
                }
                return true;
        }
        return false;
    }

    private void initApi() {

    }

    private void closeGame(){
        if(bPlayGame){
            showMessageAlert(getResources().getString(R.string.game_exit_warning),
                    getResources().getString(R.string.common_ok),
                    getResources().getString(R.string.common_cancel),
                    new PopupListenerFactory.SimplePopupListener() {
                        @Override
                        public void onClick(kr.co.minetalk.ui.data.DialogInterface f, int state) {
                            if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                setResult(RESULT_CANCELED);
                                finish();
                            }
                        }
                    });
        }else{
            setResult(RESULT_CANCELED);
            finish();
        }
    }
    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBinding.webGuide.getVisibility() == View.VISIBLE){
                    mBinding.webGuide.setVisibility(View.GONE);
                }else{
                    closeGame();
                }
            }
        });

        mWebSettings = mBinding.webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setLoadWithOverviewMode(true);
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setSupportZoom(true);
        mWebSettings.setBuiltInZoomControls(false);
        mWebSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings.setAllowFileAccess(true);

        mWebGuideSettings = mBinding.webGuide.getSettings();
        mWebGuideSettings.setJavaScriptEnabled(true);
        mWebGuideSettings.setLoadWithOverviewMode(true);
        mWebGuideSettings.setUseWideViewPort(true);
        mWebGuideSettings.setSupportZoom(true);
        mWebGuideSettings.setBuiltInZoomControls(false);
        mWebGuideSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebGuideSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebGuideSettings.setDomStorageEnabled(true);
        mWebGuideSettings.setAllowFileAccess(false);

        mBinding.webView.setInitialScale(1);
        mBinding.webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mBinding.webView.setScrollbarFadingEnabled(true);

        mBinding.webGuide.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mBinding.webGuide.setScrollbarFadingEnabled(true);

        mBinding.webView.addJavascriptInterface(new AndroidBrigde(), "minechat");

        mBinding.webView.setWebViewClient(new CustomWebViewClient());
        mBinding.webView.setWebChromeClient(new CustomWebChromeClient());

        mBinding.webGuide.setWebViewClient(new CustomWebViewClient());
        mBinding.webGuide.setWebChromeClient(new CustomWebChromeClient());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mBinding.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mBinding.webGuide.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            mBinding.webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            mBinding.webGuide.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        // android 5.0부터 앱에서 API수준21이상을 타겟킹하는 경우 아래추가
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBinding.webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            mBinding.webGuide.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(mBinding.webView, true);

            CookieManager cookieManagerGuide = CookieManager.getInstance();
            cookieManagerGuide.setAcceptCookie(true);
            cookieManagerGuide.setAcceptThirdPartyCookies(mBinding.webGuide, true);
        }

        String xid = MineTalkApp.getUserInfoModel().getUser_xid();
        if(!xid.equals("")){
            Log.e("@@@TAG","user auth : " + MineTalk.getGameBlockServer() + "?user_id=" + MineTalkApp.getUserInfoModel().getUser_xid());
            try {
                StringBuffer buffer = new StringBuffer();

                if(mGameType == PointGameActivity.GameType.BLOCK){
                    buffer.append( MineTalk.getGameBlockServer());
                    buffer.append("?user_id=" + URLEncoder.encode(xid, "utf-8"));
                }else if(mGameType == PointGameActivity.GameType.TYCOON){
                    buffer.append( MineTalk.getGameTycoonServer());
                    buffer.append("?user_id=" + URLEncoder.encode(xid, "utf-8"));
                }else if(mGameType == PointGameActivity.GameType.RPS){
                    buffer.append(MineTalk.getGameRockPaperScissorsServer());
                    buffer.append("&user_id=" + URLEncoder.encode(xid, "utf-8"));
                }else if(mGameType == PointGameActivity.GameType.BAKETBALL){
                    buffer.append(MineTalk.getGameBasketBallServer());
                    buffer.append("&user_id=" + URLEncoder.encode(xid, "utf-8"));
                }

                mBinding.webView.loadUrl(buffer.toString());
            }catch (UnsupportedEncodingException e){
                e.printStackTrace();
                setResult(RESULT_CANCELED);
                finish();
            }
        }else{
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
        }else{
            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
    }

    class CustomWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return loadPage(view, url);
//            return super.shouldOverrideUrlLoading(view, url);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            return loadPage(view, url);
//            return super.shouldOverrideUrlLoading(view, request);
        }

        @Nullable
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            return super.shouldInterceptRequest(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        private boolean loadPage(WebView view, String url) {
            Context context = view.getContext();
            if(context == null) {
                context = getApplicationContext();
            }

            Log.e("LOG.. CALL URL ==>", url);

            /* ISP(비씨, 국민) 인증 어플 연동 */
            if (url.startsWith("ispmobile")) {
                Log.e("LOG..ISP URL ==>", url);
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                try {
                    startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException ex) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mobile.vpay.co.kr/jsp/MISP/andown.jsp"));
                    startActivity(intent);
                }
                return true;
            }else if (url.contains("market://") ||
                    url.endsWith(".apk") ||
                    url.contains("http://market.android.com") ||
                    url.contains("vguard") ||
                    url.contains("v3mobile") ||
                    url.contains("ansimclick") ||
                    url.contains("smhyundaiansimclick://") ||
                    url.contains("smshinhanansimclick://") ||
                    url.contains("smshinhancardusim://") ||
                    url.contains("hanaansim://") ||
                    url.contains("citiansimmobilevaccine://") ||
                    url.contains("droidxantivirus") ||
                    url.contains("market://details?id=com.shcard.smartpay") ||
                    url.contains("shinhan-sr-ansimclick://") ||
                    url.contains("lottesmartpay") ||
                    url.contains("com.ahnlab.v3mobileplus") ||
                    url.contains("v3mobile") ||
                    url.contains("mvaccine") ||
                    url.contains("http://m.ahnlab.com/kr/site/download") ||
                    url.contains("com.lotte.lottesmartpay") ||
                    url.contains("com.lcacApp") ||
                    url.contains("lotteappcard://") ||
                    url.contains("eftpay") ||
                    url.contains("cloudpay")) {
                Log.e("LOG.. app URL ==>", url);
                return callApp(context, url);
            }else if (url.startsWith("http://") || url.startsWith("https://")) {
                Log.e("LOG.. http URL ==>", url);
                view.loadUrl(url);
                return true;
            }else if (url.startsWith("smartxpay-transfer://")) {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                try {
                    startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException ex) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=kr.co.uplus.ecredit"));
                    startActivity(intent);
                }
                return true;
            } else if (url.startsWith("smartxpay-transfer://")) {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                try {
                    startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException ex) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=kr.co.uplus.ecredit"));
                    startActivity(intent);
                }
                return true;
            } else if (url.startsWith("lguthepay")) {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException ex) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.lguplus.paynow"));
                    startActivity(intent);
                }
                return true;
            } else {
                Log.e("LOG.. else url ==>", url);
                return callApp(context, url);
            }
        }

        //외부앱호출
        public boolean callApp(Context context, String url) {
            Intent intent = null;
            try {
                Log.e("LOG.. callApp ==>", url);
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                Log.e("LOG.. get Scheme ==>", intent.getScheme());
                Log.e("LOG.. get Scheme ==>", intent.getDataString());

            } catch (URISyntaxException ex) {
                Log.e("LOG.. Browser", "Bad URI " + url + ":" + ex.getMessage());
                return false;
            }
            try {
                //국민앱 통합 17.05.18 변경
                if (url.contains("kb-acp")) {
                    Log.e("LOG..kb-acp ==>", url);
                    String srCode = null;
                    try {
                        Uri uri = Uri.parse(url);
                        srCode = uri.getQueryParameter("srCode");
                        Intent intent2 = new Intent(Intent.ACTION_VIEW);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Uri data = Uri.parse("kb-acp://pay?srCode=" + srCode + "&kb-acp://");
                        intent2.setData(data);
                        startActivity(intent2);
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            Intent intent3 = new Intent(Intent.ACTION_VIEW);
                            intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent3.setData(Uri.parse("market://details?id=com.kbcard.kbkookmincard"));
                            startActivity(intent3);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    return true;
                }
                //crome버전방식 2014.4 추가 (킷캣버전이후)
                if (url.startsWith("intent")) {
                    Log.e("LOG.. intent ==>", url);
                    if (context.getPackageManager().resolveActivity(intent, 0) == null) {
                        String packagename = intent.getPackage();
                        Log.e("LOG.. packagename  ==>", packagename);
                        if (packagename != null) {
                            Uri uri = Uri.parse("market://search?q=pname:" + packagename);
                            intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                            return true;
                        }
                    }else {
                        Log.e("LOG.. intent2 ==>", url);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setComponent(null);

                        try{
                            if(((Activity)context).startActivityIfNeeded(intent,-1)){

                                return true;
                            }
                        }catch(ActivityNotFoundException ex){
                            return false;
                        }
                    }
                    //Uri uri = Uri.parse(intent.getDataString());
                    //intent = new Intent(Intent.ACTION_VIEW, uri);
                    //startActivity(intent);
                    //return true;
                } else { //구방식
                    Log.e("LOG.. callApp url ==>", url);
                    Uri uri = Uri.parse(url);
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                return true;
            } catch (Exception e) {
                Log.e("LOG.. Exception ==>", e.getMessage());
                e.printStackTrace();
                return false;
            }
        }


    }

    class CustomWebChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
//            builder.setMessage(message);
//            builder.setPositiveButton(android.R.string.ok,
//                    new AlertDialog.OnClickListener(){
//                        public void onClick(DialogInterface dialog, int which) {
//                            result.confirm();
//                        }
//                    });
//            builder.setCancelable(true);
//            builder.create();
//
//            AlertDialog dialog = builder.show();
//            TextView messageView = dialog.findViewById(android.R.id.message);
//            messageView.setGravity(Gravity.CENTER);

            //return super.onJsAlert(view, url, message, result);
            new AlertDialog.Builder(view.getContext())
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok,
                            new AlertDialog.OnClickListener(){
                                public void onClick(DialogInterface dialog, int which) {
                                    result.confirm();
                                }
                            })
                    .setCancelable(true)
                    .create()
                    .show();

            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
            //return super.onJsConfirm(view, url, message, result);
            new AlertDialog.Builder(view.getContext())
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    result.confirm();
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    result.cancel();
                                }
                            })
                    .create()
                    .show();

            return true;
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            final WebSettings settings = view.getSettings();
            settings.setDomStorageEnabled(true);
            settings.setJavaScriptEnabled(true);
            settings.setAllowFileAccess(true);
            settings.setAllowContentAccess(true);
            view.setWebChromeClient(this);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(view);
            resultMsg.sendToTarget();
            return false;
        }

        @Override
        public void onCloseWindow(WebView window) {
            super.onCloseWindow(window);
        }
    }


    public boolean canHistoryBack() {
        if(mBinding.webView.canGoBack()) {
            mBinding.webView.goBack();
            return true;
        } else {
            return false;
        }
    }

    private class AndroidBrigde{
        @JavascriptInterface
        public void setMessage(final String arg){
            MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject pushObject = new JSONObject(arg);
                        String type = pushObject.getString("type");
                        String data = pushObject.getString("data");

                        if(type.equals("redirect")){
                            mBinding.webGuide.setVisibility(View.VISIBLE);
                            mBinding.webGuide.loadUrl(data);
                        }else if(type.equals("close")){
                            finish();
                        }else if(type.equals("status")){
                            bPlayGame = data.toUpperCase().equals("Y");
                        }
                        //Toast.makeText(getApplicationContext(), arg, Toast.LENGTH_SHORT).show();
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

}
