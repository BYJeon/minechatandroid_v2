package kr.co.minetalk.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.AddQnaApi;
import kr.co.minetalk.api.ListQnaApi;
import kr.co.minetalk.api.UploadImageApi;
import kr.co.minetalk.api.UploadQnAImageApi;
import kr.co.minetalk.api.UserInfoApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.QnAModel;
import kr.co.minetalk.api.model.QnAResponse;
import kr.co.minetalk.api.model.QnaListImageModel;
import kr.co.minetalk.api.model.QnaListImageResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityQnaListBinding;
import kr.co.minetalk.message.model.QnaImgData;
import kr.co.minetalk.ui.CustomScrollView;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.listener.OnMediaPopupListener;
import kr.co.minetalk.ui.popup.BottomMediaPopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.FAQItemView;
import kr.co.minetalk.view.QnAImageListView;
import kr.co.minetalk.view.QnAItemView;
import kr.co.minetalk.view.listener.OnFriendListViewListener;
import kr.co.minetalk.view.listener.OnQnAListViewListener;

public class QnAActivity extends BindActivity<ActivityQnaListBinding> {
    private final int PERMISSON = 1;
    private final float MAX_FILE_SIZE = 10.0f;
    private static int REQUEST_CODE_ADD_QNA = 1001;

    private ArrayList<String> arrayList;
    private ArrayAdapter<String> arrayAdapter;
    private Map<String, String> typeMap = new HashMap<>();
    private ArrayList<QnaImgData> qnaImgPath = new ArrayList<>();

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, QnAActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    private ListQnaApi mListQnaApi;
    private AddQnaApi mAddQnaApi;
    private UploadQnAImageApi mUploadQnaImageApi;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_qna_list;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        qnaListRequest();

        //문의하기 선택 Spinner
        typeMap.put("회원정보","1");
        typeMap.put("서비스","2");
        typeMap.put("쇼핑몰","3");
        typeMap.put("결제","4");
        typeMap.put("포인트","5");
        typeMap.put("기타","9");

        arrayList = new ArrayList<>();
        arrayList.add("회원정보");
        arrayList.add("서비스");
        arrayList.add("쇼핑몰");
        arrayList.add("결제");
        arrayList.add("포인트");
        arrayList.add("기타");

        arrayAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,arrayList);
        mBinding.snType.setAdapter(arrayAdapter);
    }

    private OnQnAListViewListener mQnaItemViewListener = new OnQnAListViewListener() {
        @Override
        public void onClickItem(String path) {
            ArrayList<QnaImgData> tmpArr = new ArrayList<>();
            for(int i = 0; i < qnaImgPath.size(); ++i){
                if(!qnaImgPath.get(i).getPath().equals(path))
                    tmpArr.add(qnaImgPath.get(i));
            }

            qnaImgPath = tmpArr;
            loadQnaImageData();

            if(path.length() > 0)
                deleteTmpFile(path);
        }
    };

    private void deleteTmpFile(String path){
        try {
            File tempImageFile = new File(path);
            tempImageFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteAllTmpFile(){
        for( int i = 0; i < qnaImgPath.size(); ++i)
            deleteTmpFile(qnaImgPath.get(i).getPath());

        qnaImgPath.clear();
        loadQnaImageData();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int i = 0; i < qnaImgPath.size(); ++i){
                    deleteTmpFile(qnaImgPath.get(i).getPath());
                }

                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String qnaMessage = mBinding.etQnaMessage.getText().toString();
//                String qnaType = typeMap.get(mBinding.snType.getSelectedItem().toString());
//
//                if(qnaType == null || qnaType.length() == 0)
//                    qnaType  = "1";

                if(qnaMessage.equals("")) {
                    showMessageAlert(getResources().getString(R.string.qna_question_validate));
                    return;
                }

                showMessageWithWarningAlert(getResources().getString(R.string.qna_add_message),
                        getResources().getString(R.string.qna_add_warning),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    String type = typeMap.get(mBinding.snType.getSelectedItem().toString());
                                    if(type == null || type.length() == 0)
                                        type  = "1";

                                    if(qnaImgPath.size() == 0)
                                        addQnaRequest(qnaMessage, type);
                                    else{
                                        ArrayList<File> files = new ArrayList<>();
                                        for(QnaImgData item : qnaImgPath){
                                            files.add(new File(item.getPath()));
                                        }
                                        uploadImageRequest(files);
                                    }
                                }
                            }
                        });

            }
        });

        //QNA 등록하기 화면 전환 버튼
        mBinding.btnQna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mBinding.btnQna.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
                //mBinding.btnQnaList.setBackgroundResource(R.drawable.button_bg_bfbfbf);

                mBinding.viewQnaListSel.setVisibility(View.INVISIBLE);
                mBinding.viewQnaSel.setVisibility(View.VISIBLE);

                mBinding.layoutAddQna.setVisibility(View.VISIBLE);
                mBinding.scrollView.setVisibility(View.INVISIBLE);
                mBinding.btnConfirm.setVisibility(View.VISIBLE);
            }
        });

        //QNA 리스트 화면 전환 버튼
        mBinding.btnQnaList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mBinding.btnQna.setBackgroundResource(R.drawable.button_bg_bfbfbf);
                //mBinding.btnQnaList.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);

                mBinding.viewQnaListSel.setVisibility(View.VISIBLE);
                mBinding.viewQnaSel.setVisibility(View.INVISIBLE);

                mBinding.layoutAddQna.setVisibility(View.INVISIBLE);
                mBinding.scrollView.setVisibility(View.VISIBLE);
                mBinding.btnConfirm.setVisibility(View.GONE);
                qnaListRequest();
            }
        });


        mBinding.btnFileAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( qnaImgPath.size() >= 3 ) return;

                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MineTalkApp.getCurrentActivity(), new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        showMediaBottomPopup();
                    }
                }
            }
        });

        mBinding.scrollView.setScrollViewListener(new CustomScrollView.ScrollViewListener() {
            @Override
            public void onScrollChanged(CustomScrollView scrollView, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                if(scrollView.getScrollY() == 0){ //Top
                    mBinding.viewGradient.setVisibility(View.GONE);
                }else if(diff == 0){//Bottom
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                }else{//idle
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void showMediaBottomPopup() {
        BottomMediaPopup mediaPopup = new BottomMediaPopup(MineTalkApp.getCurrentActivity());
        mediaPopup.setEventListener(new OnMediaPopupListener() {
            @Override
            public void onCamera() {
                requestTakePhotoRequest(MineTalk.REQUEST_CODE_CAMERA);
            }

            @Override
            public void onGallery() {
                onChooseMedia();
            }
        });
        mediaPopup.show();
    }

    private Uri mCameraPhotoUri;
    private String mCameraPhotoName;
    private String mCameraphotonameTemp = "";
    public void requestTakePhotoRequest(int request_code) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); //사진을 찍기 위하여 설정합니다.
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException e) {
            Toast.makeText(MineTalkApp.getCurrentActivity(), getResources().getString(R.string.image_process_failed_msg), Toast.LENGTH_SHORT).show();
        }
        if (photoFile != null) {
            mCameraPhotoUri = FileProvider.getUriForFile(MineTalkApp.getCurrentActivity(),
                    "kr.co.minetalk.provider", photoFile); //FileProvider의 경우 이전 포스트를 참고하세요.
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mCameraPhotoUri); //사진을 찍어 해당 Content uri를 photoUri에 적용시키기 위함
            startActivityForResult(intent, request_code);
        }
    }

    // Android M에서는 Uri.fromFile 함수를 사용하였으나 7.0부터는 이 함수를 사용할 시 FileUriExposedException이
    // 발생하므로 아래와 같이 함수를 작성합니다. 이전 포스트에 참고한 영문 사이트를 들어가시면 자세한 설명을 볼 수 있습니다.
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        String imageFileName = "PP" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/MineChat/"); //test라는 경로에 이미지를 저장하기 위함
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCameraPhotoName = image.getName();

        return image;
    }

    private float saveBitmapToFileCache(Bitmap bitmap, String strFilePath) {

        File fileCacheItem = new File(strFilePath);
        OutputStream out = null;
        float fileSize = 0.0f;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            long ff = fileCacheItem.length();

            //파일 사이즈를 mb로 변환
            fileSize = (float)(ff) / (1024*1024);
            return fileSize;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(out != null)
                    out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return -1;
    }

    private void onChooseMedia() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,null);
        galleryIntent.setType("image/*");
        galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.select_work));

        startActivityForResult(chooser,MineTalk.REQUEST_CODE_MEDIA);
    }

    private void initApi() {
        mListQnaApi = new ListQnaApi(this, new ApiBase.ApiCallBack<QnAResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(QnAActivity.this);
            }

            @Override
            public void onSuccess(int request_code, QnAResponse qnAResponse) {
                ProgressUtil.hideProgress(QnAActivity.this);
                if(qnAResponse != null) {
                    setupData(qnAResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(QnAActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mAddQnaApi = new AddQnaApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(QnAActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(QnAActivity.this);
                if(baseModel != null) {
                    //Toast.makeText(QnAActivity.this, getResources().getString(R.string.qna_question_register), Toast.LENGTH_SHORT).show();
                    mBinding.snType.setSelection(0);
                    mBinding.etQnaMessage.setText("");

                    deleteAllTmpFile();

                    showMessageAlert(getResources().getString(R.string.qna_question_register));

                    //setResult(RESULT_OK);
                    //finish();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(QnAActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });


        mUploadQnaImageApi = new UploadQnAImageApi(MineTalkApp.getCurrentActivity(), new ApiBase.ApiCallBack<QnaListImageResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MineTalkApp.getCurrentActivity());
            }

            @Override
            public void onSuccess(int request_code, QnaListImageResponse qnaListImageResponse) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                AddQnaListRequest(qnaListImageResponse.geImage_list());
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

    }

    private void qnaListRequest() {
        mListQnaApi.execute();
    }

    private void AddQnaListRequest(ArrayList<QnaListImageModel> models) {
        String qnaMessage = mBinding.etQnaMessage.getText().toString();
        String qnaType = typeMap.get(mBinding.snType.getSelectedItem().toString());

        if(qnaType == null || qnaType.length() == 0)
            qnaType  = "1";

        mAddQnaApi.execute(qnaMessage, qnaType, models);
    }

    private void addQnaRequest(String question, String type) {
        mAddQnaApi.execute(question, type);
    }

    private void addQnaRequest(String question, String type, ArrayList<QnaListImageModel> models) {
        //mAddQnaApi.execute(question, type);
        if(qnaImgPath.size() == 0){
            mAddQnaApi.execute(question, type, models);
        }else{
            ArrayList<File> files = new ArrayList<>();
            for(QnaImgData item : qnaImgPath){
                files.add(new File(item.getPath()));
            }
            uploadImageRequest(files);
        }
    }

    private void uploadImageRequest(ArrayList<File> image) {
        mUploadQnaImageApi.execute(MineTalkApp.getCurrentActivity(), image);
    }



    private void loadQnaImageData(){
        mBinding.layoutContainer.removeAllViews();
        for(int i = 0 ; i < qnaImgPath.size() ; i++) {
            QnAImageListView itemView = new QnAImageListView(this);
            itemView.setData(qnaImgPath.get(i).getPath(), qnaImgPath.get(i).getSize());
            itemView.setOnItemListener(mQnaItemViewListener);
            mBinding.layoutContainer.addView(itemView, getListItemLayoutParams());
        }

        mBinding.tvFileListTitle.setVisibility(qnaImgPath.size() > 0 ? View.VISIBLE : View.INVISIBLE);
    }

    private void setupData(QnAResponse response) {
        mBinding.layoutListContainer.removeAllViews();

        if(response.getData().size() > 0) {
            QnAItemView itemView = new QnAItemView(this, FAQItemView.ItemType.ITEM_TYPE_TITLE);
            QnAModel model = new QnAModel();
            model.setQna_type(getString(R.string.type_title));
            model.setQna_question(getString(R.string.qna_message_text_subject));
            itemView.setData(model);
            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }

        for(int i = 0 ; i < response.getData().size() ; i++) {
            QnAItemView itemView = new QnAItemView(this, (i%2) == 0 ? FAQItemView.ItemType.ITEM_TYPE_ODD : FAQItemView.ItemType.ITEM_TYPE_EVEN);
            itemView.setData(response.getData().get(i));

            mBinding.layoutListContainer.addView(itemView, getListItemLayoutParams());
        }
    }

    private LinearLayout.LayoutParams getListItemLayoutParams() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //params.topMargin = getResources().getDimensionPixelOffset(R.dimen.height_24);

        return params;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MineTalk.REQUEST_CODE_MEDIA) {
            if(resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                if (uri != null) {
                    String contentType = CommonUtils.getMediaType(MineTalkApp.getCurrentActivity(), uri);
                    String contentPath = CommonUtils.getImageMediaPath(MineTalkApp.getCurrentActivity(), uri);

                    Log.e("@@@TAG", "file path : " + contentPath);

                    try {
                        ExifInterface exif = new ExifInterface(contentPath);
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                        Bitmap originalImage = BitmapFactory.decodeFile(contentPath);
                        Bitmap rotateImage = CommonUtils.rotateBitmap(originalImage, orientation);

                        String saveTempBmImage = Environment.getExternalStorageDirectory() + "/MineChat/" + "tmp_profile_" + System.currentTimeMillis() + ".jpg";
                        float fileSize = saveBitmapToFileCache(rotateImage, saveTempBmImage);

                        if(MAX_FILE_SIZE <= fileSize){
                            deleteTmpFile(mCameraphotonameTemp);
                            showMessageAlert(getString(R.string.qna_attachment_alert));
                        }else{
                            if(fileSize > 0) {
                                mCameraphotonameTemp  = saveTempBmImage;
                                qnaImgPath.add(new QnaImgData(mCameraphotonameTemp, fileSize));
                                loadQnaImageData();
                                //uploadImageRequest(new File(saveTempBmImage));
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        } else if(requestCode == MineTalk.REQUEST_CODE_CAMERA && resultCode == Activity.RESULT_OK) {


            File image = new File(Environment.getExternalStorageDirectory() + "/MineChat/" + mCameraPhotoName);
            Bitmap originalImage = BitmapFactory.decodeFile(image.getPath());

            if(originalImage != null) {
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(image.getPath());

                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Bitmap rotateImage = CommonUtils.rotateBitmap(originalImage, orientation);
                    String saveTempBmImage = Environment.getExternalStorageDirectory() + "/MineChat/" + "tmp_profile_" + mCameraPhotoName;

                    float fileSize = saveBitmapToFileCache(rotateImage, saveTempBmImage);

                    if(MAX_FILE_SIZE <= fileSize){
                        deleteTmpFile(mCameraphotonameTemp);
                        showMessageAlert(getString(R.string.qna_attachment_alert));
                    }else{
                        if(fileSize > 0) {
                            mCameraphotonameTemp  = saveTempBmImage;
                            qnaImgPath.add(new QnaImgData(mCameraphotonameTemp, fileSize));
                            loadQnaImageData();
                            //uploadImageRequest(new File(saveTempBmImage));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if(resultCode == RESULT_OK) {
            //qnaListRequest();
        }
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            //mBinding.btnQnaList.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnConfirm.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutSelectRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnQna.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.viewQnaSel.setBackgroundColor(getResources().getColor(R.color.bk_point_color));
            mBinding.btnQnaList.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.viewQnaListSel.setBackgroundColor(getResources().getColor(R.color.bk_point_color));

            mBinding.layoutQnaType.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvTypeTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.viewType.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.layoutQnaContent.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.tvContentTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnFileAdd.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnFileAdd.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutQnaMessage.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvFileListTitle.setTextColor(getResources().getColor(R.color.white_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            //mBinding.btnQnaList.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnConfirm.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutSelectRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnQna.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.viewQnaSel.setBackgroundColor(getResources().getColor(R.color.app_point_color));
            mBinding.btnQnaList.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.viewQnaListSel.setBackgroundColor(getResources().getColor(R.color.app_point_color));

            mBinding.layoutQnaType.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTypeTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.viewType.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.layoutQnaContent.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvContentTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnFileAdd.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnFileAdd.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutQnaMessage.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvFileListTitle.setTextColor(getResources().getColor(R.color.main_text_color));
        }
    }
}
