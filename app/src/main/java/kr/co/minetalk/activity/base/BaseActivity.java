package kr.co.minetalk.activity.base;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;


import java.util.List;

import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.activity.SplashActivity;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.LocaleUtils;

public class BaseActivity extends FragmentActivity {
    protected final String TAG = BaseActivity.class.getSimpleName();
    protected boolean mResumed;



    public BaseActivity() {
        LocaleUtils.updateConfig(this);
        MineTalkApp.setCurrentActivity(this);
    }

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        finish();

        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void startActivity(Intent intent) {
        this.startActivity(intent, null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        MineTalkApp.setCurrentActivity(this);

        mResumed = true;
    }

    @Override
    protected void onPause() {
        mResumed = false;

        try {
            View view = getCurrentFocus();

            if (view != null) {
                if (view instanceof EditText) {
                    CommonUtils.hideKeypad(this, view);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void dismissDialog() {
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList != null) {
            for (Fragment fragment : fragmentList) {
                if (fragment instanceof android.support.v4.app.DialogFragment) {
                    android.support.v4.app.DialogFragment dialogFragment = (android.support.v4.app.DialogFragment) fragment;
                    if (isDialogShowing(dialogFragment.getDialog())) {
                        dialogFragment.dismiss();
                        dialogFragment.dismissAllowingStateLoss();
                    }
                }
            }
        }
    }

    private boolean isDialogShowing(Dialog dialog) {
        if (dialog != null) {
            return dialog.isShowing();
        }

        return false;
    }





    private void getLanguage(String language) {
        String localCode = "";
        if (language.toLowerCase().equals("en")) {
            localCode = "en";
        } else if (language.toLowerCase().equals("es")) {
            localCode = "es";
        } else if (language.toLowerCase().equals("ko")) {
            localCode = "ko";
        } else {
            localCode = "en";
        }
        CommonUtils.setLanguage(localCode);

    }

    /*
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
    */
}
