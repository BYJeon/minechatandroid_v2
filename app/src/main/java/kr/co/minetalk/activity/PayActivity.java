package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.google.zxing.Result;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.FriendInfoApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityPayBinding;
import kr.co.minetalk.utils.ProgressUtil;
import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class PayActivity extends BindActivity<ActivityPayBinding> implements ZXingScannerView.ResultHandler{


    public static void startActivity(Context context, int mode) {
        Intent intent = new Intent(context, PayActivity.class);
        intent.putExtra(PointSendActivity.COLUMN_MODE, mode);
        context.startActivity(intent);
    }

    private ZXingScannerView mScannerView;
    private FriendInfoApi mFriendInfoApi;

    private int mMode;
    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pay;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        if(getIntent() != null && getIntent().getExtras() != null) {
            mMode = getIntent().getExtras().getInt(PointSendActivity.COLUMN_MODE);
        }

        initApi();
        setUIEventListener();

        mScannerView = new ZXingScannerView(this) {
            @Override
            protected IViewFinder createViewFinderView(Context context) {
                return new CustomViewFinderView(context);
            }
        };
        mBinding.contentFrame.addView(mScannerView);
    }

    private void initApi() {
        mFriendInfoApi = new FriendInfoApi(this, new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PayActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(PayActivity.this);
                if(userInfoBaseModel != null) {
                    PointSendActivity.startActivity(PayActivity.this, PointSendActivity.TYPE_PAY, mMode, userInfoBaseModel.getUser_xid(),
                            userInfoBaseModel.getUser_name(), userInfoBaseModel.getUser_profile_image());
                    finish();
                }

            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PayActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void friendInfoRequest(String friend_xid) {
        mFriendInfoApi.execute(friend_xid);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        String result = rawResult.getText();

        friendInfoRequest(result);

    }

    private static class CustomViewFinderView extends ViewFinderView {
        public static final String TRADE_MARK_TEXT = "";
        public static final int TRADE_MARK_TEXT_SIZE_SP = 40;
        public final Paint PAINT = new Paint();

        public CustomViewFinderView(Context context) {
            super(context);
            init();
        }

        public CustomViewFinderView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        private void init() {
            PAINT.setColor(Color.WHITE);
            PAINT.setAntiAlias(true);
            float textPixelSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                    TRADE_MARK_TEXT_SIZE_SP, getResources().getDisplayMetrics());
            PAINT.setTextSize(textPixelSize);
            setSquareViewFinder(true);
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
//            drawTradeMark(canvas);    // 텍스트 추가 할 경우 사용
        }

        private void drawTradeMark(Canvas canvas) {
            Rect framingRect = getFramingRect();
            float tradeMarkTop;
            float tradeMarkLeft;
            if (framingRect != null) {
                tradeMarkTop = framingRect.bottom + PAINT.getTextSize() + 10;
                tradeMarkLeft = framingRect.left;
            } else {
                tradeMarkTop = 10;
                tradeMarkLeft = canvas.getHeight() - PAINT.getTextSize() - 10;
            }
            canvas.drawText(TRADE_MARK_TEXT, tradeMarkLeft, tradeMarkTop, PAINT);
        }
    }




}
