package kr.co.minetalk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.api.CheckRecommendApi;
import kr.co.minetalk.api.CheckUserEmailApi;
import kr.co.minetalk.api.CheckUserIDApi;
import kr.co.minetalk.api.RegistApi;
import kr.co.minetalk.api.RegistV2Api;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.SnsCheckApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.RecommendCheckModel;
import kr.co.minetalk.api.model.SnsCheckModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityRegistNormalBinding;
import kr.co.minetalk.repository.RegistRepository;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.Regex;
import kr.co.minetalk.utils.Sha256Util;

public class RegistNormalActivity extends BindActivity<ActivityRegistNormalBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, RegistNormalActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int request_code) {
        Intent intent = new Intent(context, RegistNormalActivity.class);
        ((Activity)context).startActivityForResult(intent, request_code);
    }

    private final int PERMISSON = 1;

    private RegistApi mRegistApi;
    private RequestSmsApi mRequestSmsApi;
    private CheckRecommendApi mRecommendCheckApi;
    private CheckUserIDApi    mCheckUserIDApi;
    private CheckUserEmailApi mCheckUserEmailApi;
    private RegistV2Api mRegistV2Api;

    private RegistRepository mRegistRepository;
    private SnsCheckApi mSnsCheckApi;

    private String mNationCode = "82";
    private String mRecommendKeyword = "";
    private String mRealRecommendKeyword = "";

    private boolean isIdValidated = false;
    private boolean isEmailValidated = false;
    private boolean isPasswordValidate = false;
    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
//        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
//        MainActivity.startActivity(RegistActivity.this);
//        finish();

        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
        syncFriendList();
    }

    private OnSyncListener mFriendSyncListner = new OnSyncListener() {
        @Override
        public void onSyncComplete() {
            try {
                MineTalkApp.clearActivity();
            } catch (Exception e) {
                e.printStackTrace();
            }

            MainActivity.startActivity(RegistNormalActivity.this);
            finish();
        }
    };

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }



    @Override
    protected int getLayoutId() {
        return R.layout.activity_regist_normal;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        mRegistRepository = RegistRepository.getInstance();

        initApi();
        setUIEventListener();

        mSyncListener = mFriendSyncListner;

        String recommandCode = Preferences.getREcommandUserCode();
        if(!recommandCode.equals("")) {
            mRecommendKeyword = recommandCode;
            mBinding.etRecommandUser.setText(recommandCode);
            //mBinding.etRecommandUser.setEnabled(false);
            //mBinding.etRecommandUser.setTextColor(Color.parseColor("#9b9b9b"));
            //mBinding.btnRecommendConfirm.setEnabled(false);
        } else {
            mBinding.etRecommandUser.setText("");
            //mBinding.etRecommandUser.setEnabled(true);
            //mBinding.etRecommandUser.setTextColor(Color.parseColor("#4a4a4a"));
            //mBinding.btnRecommendConfirm.setEnabled(true);
        }
    }

    private void initApi() {
        mRegistApi = new RegistApi(this, new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistNormalActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);

                if(userInfoBaseModel != null) {
                    Preferences.setRecommandUserCode("");

                    if(mRegistRepository.getRegistType().equals(MineTalk.REGIST_TYPE_S)) {
                        loginRequest(mNationCode, userInfoBaseModel.getUser_provider_id(), "", mRegistRepository.getProviderType());
                    } else {
                        loginRequest(mNationCode, userInfoBaseModel.getUser_hp(), mRegistRepository.getUserPassword(), MineTalk.LOGIN_TYPE_APP);
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mSnsCheckApi = new SnsCheckApi(this, new ApiBase.ApiCallBack<SnsCheckModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistNormalActivity.this);
            }

            @Override
            public void onSuccess(int request_code, SnsCheckModel snsCheckModel) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                if(snsCheckModel != null) {
                    if(snsCheckModel.getRegist_yn().toUpperCase().equals("Y")) {
                        // 로그인
                        loginRequest("", mRegistRepository.getProviderId(), "", mRegistRepository.getProviderType());
                    } else {
                        // 회원가입(SNS) 이동(추가정보를 받기 위해 이동)
                        RegistSnsActivity.startActivity(RegistNormalActivity.this, MineTalk.REQUEST_CODE_REGIST);
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                showMessageAlert(message);
                mRegistRepository.initialize();
            }

            @Override
            public void onCancellation() {

            }
        });

        mRegistV2Api = new RegistV2Api(this, new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistNormalActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);

                if(userInfoBaseModel != null) {
                    Preferences.setRecommandUserCode("");
                    //showMessageAlert("회원가입이 완료되었습니다.");
                    loginRequest("", mRegistRepository.getUserLoginId(), mRegistRepository.getUserPassword(), MineTalk.LOGIN_TYPE_IP);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistNormalActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                if(baseModel != null) {
                    showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRecommendCheckApi = new CheckRecommendApi(this, new ApiBase.ApiCallBack<RecommendCheckModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistNormalActivity.this);
            }

            @Override
            public void onSuccess(int request_code, RecommendCheckModel baseModel) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                if(baseModel != null) {
                    //추천인이 검색되었을 경우
                    if(baseModel.getUser_login_id().length() > 0 && baseModel.getUser_hp().length() > 0 ) {
                        mRealRecommendKeyword = baseModel.getUser_login_id();
                        mRecommendKeyword = mBinding.etRecommandUser.getText().toString();
                        mBinding.etRecommandUser.setText(String.format("%s(+%s%s)", baseModel.getUser_login_id(), baseModel.getUser_nation_code(), baseModel.getUser_hp()));
                    }else if(baseModel.getUser_login_id().length() > 0 && baseModel.getUser_hp().equals("")){
                        mRealRecommendKeyword = baseModel.getUser_login_id();
                        mRecommendKeyword = mBinding.etRecommandUser.getText().toString();
                        mBinding.etRecommandUser.setText(String.format("%s", baseModel.getUser_login_id()));
                    }else {
                        mRecommendKeyword = "";
                        mBinding.etRecommandUser.setText("");
                        showMessageAlert(getResources().getString(R.string.recommend_no_exist_waring));
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                mRecommendKeyword = "";
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckUserIDApi = new CheckUserIDApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistNormalActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                if(baseModel != null) {
                    isIdValidated = baseModel.getCode().equals("0000");
                    if(isIdValidated){
                        showMessageAlert( String.format(getString(R.string.id_use_success), mBinding.etId.getText().toString()));
                        isIdValidated = true;
                    }else{
                        showMessageAlert(getResources().getString(R.string.id_already_used_warning));
                        isIdValidated = false;
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckUserEmailApi = new CheckUserEmailApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistNormalActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                if(baseModel != null) {
                    isEmailValidated = baseModel.getCode().equals("0000");
                    if(isEmailValidated){
                        showMessageAlert(String.format(getString(R.string.email_use_success), mBinding.etEmail.getText().toString()));
                    }else{
                        showMessageAlert(getResources().getString(R.string.regist_email_invalidate));
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistNormalActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.cbMinechatAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.tvMinechatAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.bk_main_text_nor_color));
                isAgreeChecked();
                isAllAgreeChecked();
            }
        });

        mBinding.cbPrivacyAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.tvPrivacyAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.bk_main_text_nor_color));
                isAgreeChecked();
                isAllAgreeChecked();
            }
        });

        mBinding.cbOperationAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.tvOperationAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.bk_main_text_nor_color));
                isAgreeChecked();
                isAllAgreeChecked();
            }
        });

        mBinding.cbContentsAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.tvContentsAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                isAgreeChecked();
                isAllAgreeChecked();
            }
        });

        mBinding.cbAllAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mBinding.cbMinechatAgree.setSelected(isSelect);
                mBinding.cbPrivacyAgree.setSelected(isSelect);
                mBinding.cbOperationAgree.setSelected(isSelect);
                mBinding.cbContentsAgree.setSelected(isSelect);

                mBinding.tvMinechatAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                mBinding.tvPrivacyAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                mBinding.tvOperationAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                mBinding.tvContentsAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                mBinding.tvAllAgree.setTextColor(getResources().getColor(isSelect ? R.color.app_point_color : R.color.main_text_color));
                isAllAgreeChecked();
            }
        });

        mBinding.btnRecommendConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int index = mBinding.etRecommandUser.getText().toString().trim().indexOf("(+");
                mRecommendKeyword = mBinding.etRecommandUser.getText().toString().trim();
                if(index > 0)
                    mRecommendKeyword = mRecommendKeyword.substring(0, index);

                if(mRecommendKeyword == null || mRecommendKeyword.length() == 0){
                    showMessageAlert(getResources().getString(R.string.recommend_format_wrong_warning));
                    return;
                }

                mRecommendCheckApi.execute(mRecommendKeyword);
            }
        });

        //아이 중복 확인
        mBinding.btnCheckId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = mBinding.etId.getText().toString().trim();
                if(id == null || id.length() == 0 || !Regex.validateID(id)) {
                    showMessageAlert(getResources().getString(R.string.id_format_wrong_warning));
                    return;
                }

                mCheckUserIDApi.execute(id);
            }
        });

        //이메일 중복 확인
        mBinding.btnCheckEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mBinding.etEmail.getText().toString().trim();
                if(email == null || email.length() == 0 || !Regex.validateEmail(email)){
                    showMessageAlert(getResources().getString(R.string.email_format_wrong_warning));
                    return;
                }

                mCheckUserEmailApi.execute(email);
            }
        });

        mBinding.btnRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pwd = mBinding.etPassword.getText().toString().trim();
                if(pwd == null || pwd.length() == 0 || !Regex.validatePassword(pwd)){
                    showMessageAlert(getResources().getString(R.string.regist_password_hint_new));
                    return;
                }

//                if(!isIdValidated){
//                    showMessageAlert(getResources().getString(R.string.regist_password_hint_new));
//                    return;
//                }

                idTypeRegistReqeust();
            }
        });

        mBinding.etId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                isAllAgreeChecked();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                isAllAgreeChecked();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                isAllAgreeChecked();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.etRecommandUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //mRecommendKeyword = "";
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBinding.ibGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SnsGoogleLoginActivity.startActivity(RegistNormalActivity.this, MineTalk.REQUEST_CODE_GOOGLE_LOGIN);
            }
        });

        mBinding.ibFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FacebookLoginActivity.startActivity(RegistNormalActivity.this, MineTalk.REQUEST_CODE_FACEBOOK_LOGIN);
            }
        });

        mBinding.ibKakao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SnsKakaoLoginActivity.startActvity(RegistNormalActivity.this, MineTalk.REQUEST_CODE_KAKAO_LOGIN);
            }
        });

        mBinding.etRePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String password = mBinding.etPassword.getText().toString().trim();
                String rePassword = mBinding.etRePassword.getText().toString().trim();

                isPasswordValidate = password.equals(rePassword);
                if(isPasswordValidate){
                    mBinding.ivMatchEmail.setVisibility(View.VISIBLE);
                }else{
                    mBinding.ivMatchEmail.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void snsCheckRequest(String provider_type, String provider_id) {
        mSnsCheckApi.execute(provider_type, provider_id);
    }

    private void isAgreeChecked(){
        if(mBinding.cbContentsAgree.isSelected() && mBinding.cbOperationAgree.isSelected() &&
                mBinding.cbPrivacyAgree.isSelected() && mBinding.cbMinechatAgree.isSelected()){

            mBinding.cbAllAgree.setSelected(true);

            mBinding.btnRegist.setEnabled(true);
            mBinding.btnRegist.setBackgroundResource(R.drawable.button_bg_16c066);
        }else{
            mBinding.cbAllAgree.setSelected(false);
            mBinding.btnRegist.setEnabled(false);
            mBinding.btnRegist.setBackgroundResource(R.drawable.button_bg_4d16c066);
        }

        mBinding.tvAllAgree.setTextColor(getResources().getColor(mBinding.cbAllAgree.isSelected() ? R.color.app_point_color : R.color.main_text_color));
    }

    private void isAllAgreeChecked(){
        if(mBinding.cbContentsAgree.isSelected() && mBinding.cbOperationAgree.isSelected() &&
                mBinding.cbPrivacyAgree.isSelected() && mBinding.cbMinechatAgree.isSelected() &&
                mBinding.etId.getText().toString().length() > 0 && mBinding.etPassword.getText().toString().length() > 0 && mBinding.etEmail.getText().toString().length() > 0 &&
                isPasswordValidate){

            mBinding.btnRegist.setEnabled(true);
            mBinding.btnRegist.setBackgroundResource(R.drawable.button_bg_16c066);
        }else{
            mBinding.btnRegist.setEnabled(false);
            mBinding.btnRegist.setBackgroundResource(R.drawable.button_bg_4d16c066);
        }
    }

    private void idTypeRegistReqeust() {
        mRegistRepository.setRegistType(MineTalk.LOGIN_TYPE_IP);

        String id    = mBinding.etId.getText().toString().trim();
        mRegistRepository.setUserLoginId(id);
        String email = mBinding.etEmail.getText().toString().trim();
        if(email != null && email.length() != 0)
            mRegistRepository.setUserEmail(email);

        String userPass = mBinding.etPassword.getText().toString().trim();
        mRegistRepository.setUserPassword(userPass);
        Preferences.setUserPassword(userPass);

        String registType      = MineTalk.REGIST_TYPE_I;
        String userEmail       = mRegistRepository.getUserEmail();
        String userProviderType = mRegistRepository.getProviderType();

        String userLoginId   = mRegistRepository.getUserLoginId();
        String userProfileImage = mRegistRepository.getUserProfileImageUrl();
        String userPlatform     = MineTalk.getUserPlatform();


        try {
            String sha256Pass = Sha256Util.encoding(userPass);
            mRegistV2Api.execute(registType, "", userEmail, mRealRecommendKeyword, userProviderType, userLoginId, sha256Pass, userProfileImage, userPlatform);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MineTalk.REQUEST_CODE_FACEBOOK_LOGIN) {
            if(resultCode == RESULT_OK) {
                if(data != null && data.getExtras() != null) {
                    mRegistRepository.initialize();
                    mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_S);
                    mRegistRepository.setProviderType(MineTalk.LOGIN_TYPE_FACEBOOK);
                    mRegistRepository.setUserName(data.getExtras().getString(MineTalk.ARG_SNS_USER_NAME));
                    mRegistRepository.setUserEmail(data.getExtras().getString(MineTalk.ARG_SNS_USER_EMAIL));
                    mRegistRepository.setProviderId(data.getExtras().getString(MineTalk.ARG_SNS_ID));
                    mRegistRepository.setUserProfileImageUrl(data.getExtras().getString(MineTalk.ARG_SNS_USER_PROFILE_IMG));

                    snsCheckRequest(mRegistRepository.getProviderType(), mRegistRepository.getProviderId());
                }
            }
        } else if(requestCode == MineTalk.REQUEST_CODE_KAKAO_LOGIN) {
            if(resultCode == RESULT_OK) {
                mRegistRepository.initialize();
                mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_S);
                mRegistRepository.setProviderType(MineTalk.LOGIN_TYPE_KAKAO);
                mRegistRepository.setUserName(data.getExtras().getString(MineTalk.ARG_SNS_USER_NAME));
                mRegistRepository.setUserEmail(data.getExtras().getString(MineTalk.ARG_SNS_USER_EMAIL));
                mRegistRepository.setProviderId(data.getExtras().getString(MineTalk.ARG_SNS_ID));
                mRegistRepository.setUserProfileImageUrl(data.getExtras().getString(MineTalk.ARG_SNS_USER_PROFILE_IMG));

                snsCheckRequest(mRegistRepository.getProviderType(), mRegistRepository.getProviderId());
            }
        }else if(requestCode == MineTalk.REQUEST_CODE_GOOGLE_LOGIN){
            if(resultCode == RESULT_OK) {
                mRegistRepository.initialize();
                mRegistRepository.setRegistType(MineTalk.REGIST_TYPE_S);
                mRegistRepository.setProviderType(MineTalk.LOGIN_TYPE_GOOGLE);
                mRegistRepository.setUserName(data.getExtras().getString(MineTalk.ARG_SNS_USER_NAME));
                mRegistRepository.setUserEmail(data.getExtras().getString(MineTalk.ARG_SNS_USER_EMAIL));
                mRegistRepository.setProviderId(data.getExtras().getString(MineTalk.ARG_SNS_ID));
                mRegistRepository.setUserProfileImageUrl(data.getExtras().getString(MineTalk.ARG_SNS_USER_PROFILE_IMG));

                snsCheckRequest(mRegistRepository.getProviderType(), mRegistRepository.getProviderId());
            }
        }else if(requestCode == MineTalk.REQUEST_CODE_REGIST) {
            try {
                mRegistRepository.initialize();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
