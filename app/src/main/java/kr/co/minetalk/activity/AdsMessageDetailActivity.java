package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ViewAdMessageApi;
import kr.co.minetalk.api.model.AdMessageModel;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityAdsMessageDetailBinding;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.AdMessageReceiveView;

public class AdsMessageDetailActivity extends BindActivity<ActivityAdsMessageDetailBinding> {

    private static final String COLUMN_TYPE = "column_type";
    private static final String COLUMN_AD_IDX = "column_ad_idx";
    private static final String COLUMN_TITLE = "column_title";

    public static void startActivity(Context context, String type, String ad_idx, String title) {
        Intent intent = new Intent(context, AdsMessageDetailActivity.class);
        intent.putExtra(COLUMN_TYPE, type);
        intent.putExtra(COLUMN_AD_IDX, ad_idx);
        intent.putExtra(COLUMN_TITLE, title);
        context.startActivity(intent);
    }

    private ViewAdMessageApi mViewAdMessageApi;

    private String mAd_Idx = "";
    private String mViewType = MineTalk.AD_LIST_TYPE_R;

    private String mTitle;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_ads_message_detail;
    }

    @Override
    protected void initView() {

        MineTalk.AD_IDX = "";
        MineTalk.AD_TITLE = "";

        if(getIntent() != null && getIntent().getExtras() != null) {
            mAd_Idx = getIntent().getExtras().getString(COLUMN_AD_IDX);
            mViewType = getIntent().getExtras().getString(COLUMN_TYPE);
            mTitle = getIntent().getExtras().getString(COLUMN_TITLE);
        }

        mBinding.tvActivityTitle.setText(mTitle);

        mBinding.setHandlers(this);
        initApi();

        setUIEventListener();

        viewAdMessageRequest(mAd_Idx);
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void initApi() {
        mViewAdMessageApi = new ViewAdMessageApi(this, new ApiBase.ApiCallBack<AdMessageModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(AdsMessageDetailActivity.this);
            }

            @Override
            public void onSuccess(int request_code, AdMessageModel adMessageModel) {
                ProgressUtil.hideProgress(AdsMessageDetailActivity.this);
                if(adMessageModel != null) {
                    setupData(adMessageModel);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(AdsMessageDetailActivity.this);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void viewAdMessageRequest(String ad_idx) {
        mViewAdMessageApi.execute(Integer.parseInt(ad_idx));
    }

    private void setupData(AdMessageModel data) {
        mBinding.layoutContainer.removeAllViews();

        AdMessageReceiveView itemView = new AdMessageReceiveView(this);
        itemView.setData(data);

        mBinding.layoutContainer.addView(itemView);
    }

    @Override
    public void updateTheme() {
        super.updateTheme();
    }
}
