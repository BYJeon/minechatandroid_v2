package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.AttendanceCheckApi;
import kr.co.minetalk.api.ChangeMiningPushFlagApi;
import kr.co.minetalk.api.CheckMiningPushApi;
import kr.co.minetalk.api.model.AttendanceResponse;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.MiningPushModel;
import kr.co.minetalk.databinding.ActivityPointSaveBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class PointSaveActivity extends BindActivity<ActivityPointSaveBinding> {
    private final int PERMISSON = 1;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PointSaveActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flag) {
        Intent intent = new Intent(context, PointSaveActivity.class);
        intent.addFlags(flag);
        context.startActivity(intent);
    }

    private int coinType = PointSendActivity.MODE_CASH;

    private AttendanceCheckApi mAttendanceCheckApi;
    private CheckMiningPushApi mCheckMiningPushApi;
    private ChangeMiningPushFlagApi mChangeMiningPushFlagApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_point_save;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCheckMiningPushApi.execute();
    }

    private void initApi() {
        mAttendanceCheckApi = new AttendanceCheckApi(PointSaveActivity.this, new ApiBase.ApiCallBack<AttendanceResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointSaveActivity.this);
            }

            @Override
            public void onSuccess(int request_code, AttendanceResponse attendanceResponse) {
                ProgressUtil.hideProgress(PointSaveActivity.this);
                if(attendanceResponse != null) {
                    if(attendanceResponse.getCouponYn().equals("Y")) {
                        String curTime = CommonUtils.getKorTime("yyyy-MM-dd");
                        Preferences.setMiningSaveDate(MineTalkApp.getUserInfoModel().getUser_xid(), curTime);
                        showMessageAlert(String.format(getResources().getString(R.string.save_point_complete_warning), attendanceResponse.getEtc_data(), attendanceResponse.getAmount()), getResources().getString(R.string.common_close));
                    }else{
                        showMessageAlert(String.format(getResources().getString(R.string.save_point_complete_warning_none_coupon), attendanceResponse.getAmount()), getResources().getString(R.string.common_close));
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointSaveActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCheckMiningPushApi = new CheckMiningPushApi(PointSaveActivity.this, new ApiBase.ApiCallBack<MiningPushModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(PointSaveActivity.this);
            }

            @Override
            public void onSuccess(int request_code, MiningPushModel pushResponse) {
                ProgressUtil.hideProgress(PointSaveActivity.this);
                if(pushResponse != null){
                    mBinding.swAlarm.setSelected(pushResponse.getPushYn().equals("Y") ? true : false);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointSaveActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mChangeMiningPushFlagApi = new ChangeMiningPushFlagApi(PointSaveActivity.this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                //ProgressUtil.showProgress(PointSaveActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                //ProgressUtil.hideProgress(PointSaveActivity.this);

            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(PointSaveActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //일일 접속
        mBinding.tvTodayAccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAttendanceCheckApi.execute();
            }
        });

        mBinding.layoutTodayAccessRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAttendanceCheckApi.execute();
            }
        });

        //게임
        mBinding.btnGameAccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointGameActivity.startActivity(PointSaveActivity.this);
            }
        });

        mBinding.layoutGameRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointGameActivity.startActivity(PointSaveActivity.this);
            }
        });
        //푸쉬 알림 받기
        mBinding.swAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                mChangeMiningPushFlagApi.execute(isSelect ? "Y" : "N");
            }
        });

    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvMineTokenTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));

            //mBinding.btnTodayAccess.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnTodayAccess.setBackgroundResource(R.drawable.button_flat_cdb60c);

            mBinding.layoutGameRoot.setBackgroundResource(R.drawable.button_flat_cdb60c);

            //mBinding.btnGameAccess.setBackgroundResource(R.drawable.bg_round_272f39);
            mBinding.btnGameAccess.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnAdsAccess.setBackgroundResource(R.drawable.bg_round_272f39);

            mBinding.tvTodayAccess.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.swAlarm.setImageResource(R.drawable.selector_switch_bk);
            mBinding.ivAlarm.setImageResource(R.drawable.icon_alarm_w);
            mBinding.tvAlarmTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.layoutAlarmRoot.setBackgroundResource(R.drawable.bg_flat_151a21_line_858992);
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvMineTokenTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            //mBinding.btnTodayAccess.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnTodayAccess.setBackgroundResource(R.drawable.button_flat_16c066);

            mBinding.layoutGameRoot.setBackgroundResource(R.drawable.button_flat_16c066);

            //mBinding.btnGameAccess.setBackgroundResource(R.drawable.bg_round_f9f9f9);
            mBinding.btnGameAccess.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnAdsAccess.setBackgroundResource(R.drawable.bg_round_f9f9f9);

            mBinding.tvTodayAccess.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.swAlarm.setImageResource(R.drawable.selector_switch);
            mBinding.ivAlarm.setImageResource(R.drawable.icon_alarm);
            mBinding.tvAlarmTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.layoutAlarmRoot.setBackgroundResource(R.drawable.bg_flat_ffffff_line_e6e6e6);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PayActivity.startActivity(MineTalkApp.getCurrentActivity(), coinType);
                } else {
                    return;
                }
        }
    }
}
