package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import java.util.Locale;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ModifyUserPwdApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditUserPassBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.Regex;
import kr.co.minetalk.utils.Sha256Util;

public class EditUserPassActivity extends BindActivity<ActivityEditUserPassBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditUserPassActivity.class);
        context.startActivity(intent);
    }

    private ModifyUserPwdApi mModifyUserPwdApi;

    private String mChangePassword = "";
    private boolean mCurrentPasswordAccord = false;
    private boolean mChangePasswordAccord = false;

    private String mCurrentPassword = "";

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        mChangePassword = "";
        showMessageAlert(message, new PopupListenerFactory.SimplePopupListener() {
            @Override
            public void onClick(DialogInterface f, int state) {
                LoginActivity.startActivity(EditUserPassActivity.this);
            }
        });

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_user_pass;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();


        mCurrentPassword = Preferences.getUserPassword();
        Log.e("@@@TAG", "current password : " + mCurrentPassword);

    }

    private void initApi() {
        mModifyUserPwdApi = new ModifyUserPwdApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditUserPassActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditUserPassActivity.this);
                if(baseModel != null) {
                    String nationCode = MineTalkApp.getUserInfoModel().getUser_nation_code();
                    String user_phone = MineTalkApp.getUserInfoModel().getUser_hp();
                    String userId = MineTalkApp.getUserInfoModel().getUser_login_id();

                    if(userId.equals("")){
                        if(!user_phone.equals(""))
                            userId = user_phone;
                    }

                    Preferences.setUserPassword(mChangePassword);
                    loginRequest(nationCode, userId, mChangePassword, MineTalk.LOGIN_TYPE_IP);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditUserPassActivity.this);
                mChangePassword = "";
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputStr = mBinding.etPassword.getText().toString();
                String currentPassowrd = Preferences.getUserPassword();


                if(inputStr.equals(currentPassowrd)) {
                    mCurrentPasswordAccord = true;
                    //mBinding.tvCurrentPasswordAccord.setVisibility(View.VISIBLE);
                } else {
                    mCurrentPasswordAccord = false;
                    //mBinding.tvCurrentPasswordAccord.setVisibility(View.INVISIBLE);
                    showMessageAlert(getResources().getString(R.string.pwd_match_wrong_warning));
                    return;
                }



                if(mCurrentPasswordAccord) {
                    if(!Regex.validatePassword(mBinding.etNewPassword.getText().toString())) {
                        showMessageAlert(getResources().getString(R.string.pass_regex_not_match));
                        return;
                    }

                    showMessageAlert(getResources().getString(R.string.pwd_change_complete_warning),
                            getResources().getString(R.string.chat_popup_save),
                            getResources().getString(R.string.common_cancel),
                            new PopupListenerFactory.SimplePopupListener() {
                                @Override
                                public void onClick(DialogInterface f, int state) {
                                    if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                        editPasswordRequest(mBinding.etNewPassword.getText().toString());
                                    }
                                }
                            });
                }
            }
        });

        mBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String inputStr = s.toString();

                Log.e("@@@TAG", "input pass[" + inputStr + "]");

                if(inputStr.equals(mCurrentPassword)) {
                    mCurrentPasswordAccord = true;
                } else {
                    mCurrentPasswordAccord = false;
                }

                allInputRegisted();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.etNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String inputStr = s.toString();
                String otherStr = mBinding.etRePassword.getText().toString();

                if(inputStr.equals(otherStr)) {
                    mBinding.ivMatchPassword.setVisibility(View.VISIBLE);
                    mChangePasswordAccord = true;
                } else {
                    mBinding.ivMatchPassword.setVisibility(View.INVISIBLE);
                    mChangePasswordAccord = false;
                }

                allInputRegisted();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.etRePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String inputStr = s.toString();
                String otherStr = mBinding.etNewPassword.getText().toString();

                if(inputStr.equals(otherStr)) {
                    mBinding.ivMatchPassword.setVisibility(View.VISIBLE);
                    mChangePasswordAccord = true;
                } else {
                    mBinding.ivMatchPassword.setVisibility(View.INVISIBLE);
                    mChangePasswordAccord = false;
                }

                allInputRegisted();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void allInputRegisted(){
        if(mCurrentPasswordAccord && mChangePasswordAccord){
            mBinding.btnConfirm.setEnabled(true);
            mBinding.btnConfirm.setBackgroundColor(Color.parseColor("#16c066"));
        }else{
            mBinding.btnConfirm.setEnabled(false);
            mBinding.btnConfirm.setBackgroundColor(Color.parseColor("#4d16c066"));
        }
    }

    private void editPasswordRequest(String input_password) {
        String changePassword = "";
        try {
            changePassword = Sha256Util.encoding(input_password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mChangePassword = input_password;
        mModifyUserPwdApi.execute(changePassword);
    }
}
