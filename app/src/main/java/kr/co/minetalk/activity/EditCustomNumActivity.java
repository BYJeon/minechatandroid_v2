package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.ModifyCustomClearanceNumberApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityEditCustomBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class EditCustomNumActivity extends BindActivity<ActivityEditCustomBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, EditCustomNumActivity.class);
        context.startActivity(intent);
    }

    private ModifyCustomClearanceNumberApi mModifyCustomClearanceNumApi;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_custom;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        String defaultValue = Preferences.getCustomNumber(MineTalkApp.getUserInfoModel().getUser_xid());
        if(defaultValue.equals("")){
            mBinding.layoutCurCustom.setVisibility(View.GONE);
        }else{
            mBinding.layoutCurCustom.setVisibility(View.VISIBLE);
            mBinding.tvCurCustomNum.setText(defaultValue);
        }
    }

    private void initApi() {
        mModifyCustomClearanceNumApi = new ModifyCustomClearanceNumberApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(EditCustomNumActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(EditCustomNumActivity.this);

                showMessageAlert(getResources().getString(R.string.custom_save_complete),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    setResult(RESULT_OK);
                                    finish();
                                }
                            }
                        });

            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(EditCustomNumActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showMessageAlert(getResources().getString(R.string.custom_save_warning),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    editUserClearanceNumRequest(mBinding.etNum.getText().toString());
                                }
                            }
                        });

                if( !mBinding.etNum.getText().toString().equals("")) {
                }
            }
        });

        mBinding.btnSearchCustomNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentWeb = new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.customs.go.kr/pms/html/mos/extr/MOS0101053S.do"));
                startActivity(intentWeb);
            }
        });
    }

    private void editUserClearanceNumRequest(String clearanceNum) {
//        if(clearanceNum == null || clearanceNum.equals("")) {
//            showMessageAlert("");
//            return;
//        }

        Preferences.setCustomNumber(MineTalkApp.getUserInfoModel().getUser_xid(), clearanceNum);
        mModifyCustomClearanceNumApi.execute(clearanceNum);
    }


}
