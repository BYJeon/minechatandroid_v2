package kr.co.minetalk.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaCodec;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.activity.listener.OnSyncListener;
import kr.co.minetalk.api.RegistApi;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityRegistBinding;
import kr.co.minetalk.repository.RegistRepository;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.Regex;
import kr.co.minetalk.utils.Sha256Util;

public class RegistActivity extends BindActivity<ActivityRegistBinding> {
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, RegistActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int request_code) {
        Intent intent = new Intent(context, RegistActivity.class);
        ((Activity)context).startActivityForResult(intent, request_code);
    }

    private final int PERMISSON = 1;

    private RegistApi mRegistApi;
    private RequestSmsApi mRequestSmsApi;

    private RegistRepository mRegistRepository;

    private String mNationCode = "82";
    private int mNationCodePositionIndex = 0;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
//        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
//        MainActivity.startActivity(RegistActivity.this);
//        finish();

        MineTalkApp.setUserInfoModel((UserInfoBaseModel)baseModel);
        syncFriendList();
    }

    private OnSyncListener mFriendSyncListner = new OnSyncListener() {
        @Override
        public void onSyncComplete() {
            try {
                MineTalkApp.clearActivity();
            } catch (Exception e) {
                e.printStackTrace();
            }

            MainActivity.startActivity(RegistActivity.this);
            finish();
        }
    };

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {
        showMessageAlert(message);
    }



    @Override
    protected int getLayoutId() {
        return R.layout.activity_regist;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);
        mRegistRepository = RegistRepository.getInstance();

        initApi();
        setUIEventListener();

        mSyncListener = mFriendSyncListner;

        /********************************************
         * 전화번호 가입/SNS 가입에 따른 UI 초기 셋팅
         ********************************************/
        if(mRegistRepository.getRegistType().equals(MineTalk.REGIST_TYPE_S)) {
            mBinding.layoutUserName.setVisibility(View.GONE);
            mBinding.layoutPassword.setVisibility(View.GONE);
        } else {
            mBinding.layoutUserName.setVisibility(View.VISIBLE);
            mBinding.layoutPassword.setVisibility(View.VISIBLE);
        }
        /********************************************/


        /**************************
         * Nation Code initialize
         **************************/
        try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            mBinding.tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            mBinding.tvNationCode.setText("한국 " + "+82");
            mNationCode = "82";
        }
        mNationCodePositionIndex = 0;
        /**************************/


        /*if(Build.VERSION.SDK_INT >= 23) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS}, PERMISSON);
            } else {
                initSMSCatchReceiver();
            }

        }*/


        String recommandCode = Preferences.getREcommandUserCode();
        if(!recommandCode.equals("")) {
            mBinding.etRecommandUser.setText(recommandCode);
            mBinding.etRecommandUser.setEnabled(false);
            mBinding.etRecommandUser.setTextColor(Color.parseColor("#9b9b9b"));
        } else {
            mBinding.etRecommandUser.setText("");
            mBinding.etRecommandUser.setEnabled(true);
            mBinding.etRecommandUser.setTextColor(Color.parseColor("#4a4a4a"));
        }

    }

    private void initApi() {
        mRegistApi = new RegistApi(this, new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistActivity.this);
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(RegistActivity.this);

                if(userInfoBaseModel != null) {
                    Preferences.setRecommandUserCode("");

                    if(mRegistRepository.getRegistType().equals(MineTalk.REGIST_TYPE_S)) {
                        loginRequest(mNationCode, userInfoBaseModel.getUser_provider_id(), "", mRegistRepository.getProviderType());
                    } else {
                        loginRequest(mNationCode, userInfoBaseModel.getUser_hp(), mRegistRepository.getUserPassword(), MineTalk.LOGIN_TYPE_APP);
                    }
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRequestSmsApi = new RequestSmsApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(RegistActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(RegistActivity.this);
                if(baseModel != null) {
                    showMessageAlert(getResources().getString(R.string.common_request_sms_complete_msg));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(RegistActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.checkAgree1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);
            }
        });

        mBinding.checkAgree2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);
            }
        });

        mBinding.layoutNationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String[] array = NumberCode.getIntance().getmTitleArray();
                    showBottomWheelPopup(array, mNationCodePositionIndex, mNationCodePickerListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mBinding.btnAgreeDescView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int isVisible = mBinding.tvAgree1Desc.getVisibility();
//                if(isVisible == View.VISIBLE) {
//                    mBinding.btnAgreeDescClose.setText(getString(R.string.regist_button_visible));
//                    mBinding.tvAgree1Desc.setVisibility(View.GONE);
//                } else {
//                    mBinding.btnAgreeDescClose.setText(getString(R.string.regist_button_close));
//                    mBinding.tvAgree1Desc.setVisibility(View.VISIBLE);
//                }
                WebViewActivity.startActivity(RegistActivity.this, "이용약관", "http://app.minetalk.co.kr/terms_of_use");
            }
        });
        mBinding.btnAgreeDescView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int isVisible = mBinding.tvAgree1Desc.getVisibility();
//                if(isVisible == View.VISIBLE) {
//                    mBinding.btnAgreeDescClose.setText(getString(R.string.regist_button_visible));
//                    mBinding.tvAgree1Desc.setVisibility(View.GONE);
//                } else {
//                    mBinding.btnAgreeDescClose.setText(getString(R.string.regist_button_close));
//                    mBinding.tvAgree1Desc.setVisibility(View.VISIBLE);
//                }
                WebViewActivity.startActivity(RegistActivity.this, "개인정보취급방침", "http://app.minetalk.co.kr/privacy_policy");
            }
        });

        mBinding.btnRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mRegistRepository.getRegistType().equals(MineTalk.REGIST_TYPE_S)) {
                    snsTypeRegistReqeust();
                } else {
                    phoneTypeReigstReqeust();
                }
            }
        });

        mBinding.btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smsRequest();
            }
        });
    }

    private void smsRequest() {
        String phoneNumber = mBinding.etPhoneNumber.getText().toString();
        if(phoneNumber == null || phoneNumber.replace(" ", "").equals("")) {
            showMessageAlert(getResources().getString(R.string.find_password_validation_phone_number));
            return;
        }
        mRequestSmsApi.execute(MineTalk.SMS_TYPE_REGIST, mNationCode, phoneNumber);
    }

    private void phoneTypeReigstReqeust() {

        String registType      = mRegistRepository.getRegistType();
        String userName        = mBinding.etName.getText().toString();
        String userPass        = mBinding.etPassword.getText().toString();
        String userPassConfirm = mBinding.etPasswordConfirm.getText().toString();
        String userNationCode  = mNationCode;

        String userPhoneNumber  = mBinding.etPhoneNumber.getText().toString();
        String authNumber       = mBinding.etAuthNumber.getText().toString();
        String userRecommend    = mBinding.etRecommandUser.getText().toString();
        String userProviderType = "";

        String userProviderId   = "";
        String userProfileImage = "";
        String userPlatform     = MineTalk.getUserPlatform();

        mRegistRepository.setUserPassword(userPass);


        if(userName == null || userName.equals("")) {
            showMessageAlert(getResources().getString(R.string.regist_validation_user_name));
            return;
        }

        if(userPass == null || userPass.equals("")) {
            showMessageAlert(getResources().getString(R.string.regist_validation_password));
            return;
        }

        if(!Regex.validatePassword(userPass)) {
            showMessageAlert(getResources().getString(R.string.regist_validation_password) + getResources().getString(R.string.validation_edit_error_1));
            return;
        }

        if(userPassConfirm == null || userPassConfirm.equals("")) {
            showMessageAlert(getResources().getString(R.string.regist_validation_password));
            return;
        }

        if(!userPass.equals(userPassConfirm)) {
            showMessageAlert(getResources().getString(R.string.regist_validation_password));
            return;
        }


        if(userPhoneNumber == null || userPhoneNumber.equals("")) {
            showMessageAlert(getResources().getString(R.string.regist_validation_phone_number));
            return;
        }

        if(!Regex.validatePhone(userPhoneNumber)) {
            showMessageAlert(getResources().getString(R.string.regist_validation_phone_number));
            return;
        }


        if(authNumber == null || authNumber.equals("")) {
            showMessageAlert(getResources().getString(R.string.regist_validation_auth_number));
            return;
        }


        if(userRecommend != null && !userRecommend.equals("")) {
            if(!Regex.validatePhone(userRecommend)) {
                showMessageAlert(getString(R.string.regist_hint_recommand_user));
                return;
            }
        }


        Preferences.setUserPassword(userPass);

        try {
            String sha256Pass = Sha256Util.encoding(userPass);
            mRegistApi.execute(registType, userName, sha256Pass, userNationCode, userPhoneNumber, authNumber, userRecommend, userProviderType, userProviderId, userProfileImage, userPlatform);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void snsTypeRegistReqeust() {
        String registType      = mRegistRepository.getRegistType();
        String userName        = mRegistRepository.getUserName();
        String userPass        = "";
        String userNationCode  = mNationCode;

        String userPhoneNumber  = mBinding.etPhoneNumber.getText().toString();
        String authNumber       = mBinding.etAuthNumber.getText().toString();
        String userRecommend    = mBinding.etRecommandUser.getText().toString();
        String userProviderType = mRegistRepository.getProviderType();

        String userProviderId   = mRegistRepository.getProviderId();
        String userProfileImage = mRegistRepository.getUserProfileImageUrl();
        String userPlatform     = MineTalk.getUserPlatform();

        if(userPhoneNumber == null || userPhoneNumber.equals("")) {
            showMessageAlert(getResources().getString(R.string.regist_validation_phone_number));
            return;
        }

        if(authNumber == null || authNumber.equals("")) {
            showMessageAlert(getResources().getString(R.string.regist_validation_auth_number));
            return;
        }

        mRegistApi.execute(registType, userName, userPass, userNationCode, userPhoneNumber, authNumber, userRecommend, userProviderType, userProviderId, userProfileImage, userPlatform);
    }

    // 전화번호 국가 코드 선택 Listener
    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
        @Override
        public void onClick(int state, int num) {
            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                mNationCodePositionIndex = num;
                try {
                    String viewName = NumberCode.getIntance().getDataList().get(num).getName();
                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");

                    mBinding.tvNationCode.setText(viewName);
                } catch (Exception e) {
                    e.printStackTrace();
                    mNationCode = "82";
                }
            }
        }

        @Override
        public void onClick(int state, int num, int position) {

        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mAuthSMSBroadCastReceiver != null) {
            unregisterReceiver(mAuthSMSBroadCastReceiver);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initSMSCatchReceiver();
                } else {
                    return;
                }
        }
    }



    private static final String ACTION_SMS = "android.provider.Telephony.SMS_RECEIVED";
    private BroadcastReceiver mAuthSMSBroadCastReceiver;
    private void initSMSCatchReceiver() {

        IntentFilter filter = new IntentFilter();
        filter.setPriority(999);
        filter.addAction(ACTION_SMS);


        mAuthSMSBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent.getAction().equals(ACTION_SMS)) {

                    Bundle bundle = intent.getExtras();
                    if(bundle == null)
                        return;

                    Object[] pdusObj = (Object[]) bundle.get("pdus");
                    if(pdusObj == null)
                        return;

                    SmsMessage[] smsMessages = new SmsMessage[pdusObj.length];

                    for (int i = 0; i < pdusObj.length; i++) {

                        smsMessages[i] = SmsMessage.createFromPdu((byte[])pdusObj[i]);

                        final String address = smsMessages[i].getOriginatingAddress();
                        final String message = smsMessages[i].getDisplayMessageBody();

                        String smsPhone = Preferences.getSmsPhone();
                        if( smsPhone.equals(address.replace("-","")) ) {
                            abstrackNumber(message);
                        }
                    }

                    this.abortBroadcast();
                }
            }

            private void abstrackNumber(String message) {
                Pattern p = Pattern.compile("([^\\d])");
                Matcher matcher = p.matcher(message);
                StringBuffer destStringBuffer = new StringBuffer();

                while (matcher.find()){
                    matcher.appendReplacement(destStringBuffer, "");
                }

                matcher.appendTail(destStringBuffer);
                mBinding.etAuthNumber.setText(destStringBuffer.toString());

            }

        };
        registerReceiver(mAuthSMSBroadCastReceiver, filter);
    }
}
