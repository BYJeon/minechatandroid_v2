package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.AttendanceCheckApi;
import kr.co.minetalk.api.ChangeMiningPushFlagApi;
import kr.co.minetalk.api.CheckMiningPushApi;
import kr.co.minetalk.api.model.AttendanceResponse;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.MiningPushModel;
import kr.co.minetalk.databinding.ActivityPointGameBinding;
import kr.co.minetalk.databinding.ActivityPointSaveBinding;
import kr.co.minetalk.fragment.ShopFragment;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.utils.Sha256Util;

public class PointGameActivity extends BindActivity<ActivityPointGameBinding> {
    private final int PERMISSON = 1;

    public static enum GameType {
        BLOCK,
        TYCOON,
        RPS,
        BAKETBALL
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PointGameActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flag) {
        Intent intent = new Intent(context, PointGameActivity.class);
        intent.addFlags(flag);
        context.startActivity(intent);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {
    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_point_game;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initApi() {

    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //블록쌓기 게임
        mBinding.ivGameBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointGamePlayActivity.startActivity(PointGameActivity.this, GameType.BLOCK);
            }
        });

        //타이쿤 게임
        mBinding.ivGameTycoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointGamePlayActivity.startActivity(PointGameActivity.this, GameType.TYCOON);
            }
        });

        //가위바위보 게임
        mBinding.ivGameRps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointGamePlayActivity.startActivity(PointGameActivity.this, GameType.RPS);
            }
        });

        //농구 게임
        mBinding.ivGameBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointGamePlayActivity.startActivity(PointGameActivity.this, GameType.BAKETBALL);
            }
        });
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));


            mBinding.tvBlockTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.layoutBlockRoot.setBackgroundResource(R.drawable.bg_flat_trans_line_858992);

            mBinding.tvTycoonTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.layoutTycoonRoot.setBackgroundResource(R.drawable.bg_flat_trans_line_858992);
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvBlockTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.layoutBlockRoot.setBackgroundResource(R.drawable.bg_flat_ffffff_line_e6e6e6);

            mBinding.tvTycoonTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.layoutTycoonRoot.setBackgroundResource(R.drawable.bg_flat_ffffff_line_e6e6e6);
        }
    }

}
