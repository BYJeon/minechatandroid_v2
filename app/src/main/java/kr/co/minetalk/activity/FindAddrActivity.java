package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityFindAddrBinding;


public class FindAddrActivity extends BindActivity<ActivityFindAddrBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, FindAddrActivity.class);
        context.startActivity(intent);
    }

    class AddrJavaScriptInterface
    {
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processDATA(String data) {

            Bundle extra = new Bundle();
            Intent intent = new Intent();
            extra.putString("data", data);
            intent.putExtras(extra);
            setResult(RESULT_OK, intent);
            finish();

        }
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_find_addr;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        mBinding.webView.getSettings().setJavaScriptEnabled(true);
        mBinding.webView.addJavascriptInterface(new AddrJavaScriptInterface(), "Android");

        mBinding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {

                mBinding.webView.loadUrl("javascript:execDaumPostcode();");
            }
        });

        mBinding.webView.loadUrl("http://app.minetalk.co.kr/search_address_kakao");

        initApi();
        setUIEventListener();
    }

    private void initApi() {

    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }


    @Override
    public void updateTheme() {
        super.updateTheme();
    }
}
