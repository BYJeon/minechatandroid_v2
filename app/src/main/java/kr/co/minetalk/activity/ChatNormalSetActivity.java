package kr.co.minetalk.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.LogoutApi;
import kr.co.minetalk.api.RestoreBackupApi;
import kr.co.minetalk.api.SaveBackupApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.ActivityChatNormalOptionBinding;
import kr.co.minetalk.databinding.ActivityChatOptionBinding;
import kr.co.minetalk.message.api.AlarmThreadApi;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.ThreadChannelApi;
import kr.co.minetalk.message.manager.MessageThreadManager;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.message.model.ThreadWithChannel;
import kr.co.minetalk.repository.ChatRepository;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.UserIconView;

public class ChatNormalSetActivity extends BindActivity<ActivityChatNormalOptionBinding> {
    private static final String COLUMN_THREAD_KEY = "column_thread_key";
    private static final String COLUMN_DEFAULT_CHAT_NAME = "column_default_chat_name";

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ChatNormalSetActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String threadKey) {
        Intent intent = new Intent(context, ChatNormalSetActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, threadKey);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String threadKey, String chatName, int requestCode) {
        Intent intent = new Intent(context, ChatNormalSetActivity.class);
        intent.putExtra(COLUMN_THREAD_KEY, threadKey);
        intent.putExtra(COLUMN_DEFAULT_CHAT_NAME, chatName);
        ((Activity)context).startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    private String mCurrentThreadKey;
    private boolean mIsAlarm = false;
    private String mDefaultChatName;
    private boolean mIsDirect = false;

    private SaveBackupApi mSaveBackupApi;
    private RestoreBackupApi mRestoreApi;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_chat_normal_option;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        if(getIntent() != null && getIntent().getExtras() != null) {
            mCurrentThreadKey = getIntent().getExtras().getString(COLUMN_THREAD_KEY, "");
            mDefaultChatName = getIntent().getExtras().getString(COLUMN_DEFAULT_CHAT_NAME, "");
        }

        initApi();
        setUIEventListener();

        setDefaultValue();

        updateTheme();
        if(mCurrentThreadKey != null && !mCurrentThreadKey.equals("")) {
            getThreadInfo(mCurrentThreadKey);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setDefaultValue();
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            //백업 기능
            mBinding.tvBackupTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBackupInfo.setTextColor(getResources().getColor(R.color.white_color));

            //복원 기능
            mBinding.tvRestoreTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvRestoreInfo.setTextColor(getResources().getColor(R.color.white_color));

            //시스템 언어
            mBinding.tvSystemLan.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTransLan.setTextColor(getResources().getColor(R.color.white_color));

            //정렬
            mBinding.tvOrderTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvChatOrderInfo.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvAutoTrans.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAutoTransInfo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAutoAlert.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvAutoAlertInfo.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvSecurity.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvSecurityInfo.setTextColor(getResources().getColor(R.color.white_color));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBinding.cbAutoTrans.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.bk_point_color)));
                mBinding.cbAutoAlert.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.bk_point_color)));
            }

        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            //백업 기능
            mBinding.tvBackupTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBackupInfo.setTextColor(getResources().getColor(R.color.main_text_color));

            //복원 기능
            mBinding.tvRestoreTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvRestoreInfo.setTextColor(getResources().getColor(R.color.main_text_color));

            //시스템 언어
            mBinding.tvSystemLan.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTransLan.setTextColor(getResources().getColor(R.color.main_text_color));

            //정렬
            mBinding.tvOrderTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvChatOrderInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            
            mBinding.tvAutoTrans.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAutoTransInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAutoAlert.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAutoAlertInfo.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvSecurity.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvSecurityInfo.setTextColor(getResources().getColor(R.color.main_text_color));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBinding.cbAutoTrans.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.app_point_color)));
                mBinding.cbAutoAlert.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.app_point_color)));
            }
        }
    }

    private void initApi() {
        mSaveBackupApi = new SaveBackupApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChatNormalSetActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(ChatNormalSetActivity.this);
                if(baseModel != null) {
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(ChatNormalSetActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mRestoreApi = new RestoreBackupApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(ChatNormalSetActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(ChatNormalSetActivity.this);
                if(baseModel != null) {
                    long now = System.currentTimeMillis();
                    Date date = new Date(now);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:dd");

                    String endDate = String.format(getResources().getString(R.string.chat_manage_text_restore_date), sdf.format(date));
                    mBinding.tvRestoreDate.setText(endDate);
                    Preferences.setBackUpRestoreTime(sdf.format(date));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(ChatNormalSetActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {

        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });


        //백업하기
        mBinding.tvButtonSaveBackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long now = System.currentTimeMillis();
                Date date = new Date(now);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:dd");

                String endDate = String.format(getResources().getString(R.string.chat_manage_text_backup_date), sdf.format(date));
                mBinding.tvBackupDate.setText(endDate);
                Preferences.setBackUpSaveTime(sdf.format(date));

                //mSaveBackupApi.execute();
            }
        });

        //복원하기
        mBinding.tvButtonRestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRestoreApi.execute();
            }
        });

//        mBinding.layoutButtonChatName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                EditChatNameActivity.startActivity(ChatNormalSetActivity.this, mDefaultChatName);
//            }
//        });


        mBinding.cbAutoAlert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //alarmRequest(b);
            }
        });

        mBinding.cbAutoTrans.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Preferences.setChatTranslationUse(b);
            }
        });

        mBinding.tvButtonTransLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyLanguageActivity.startActivity(ChatNormalSetActivity.this, MyLanguageActivity.TYPE_TRANSLATION_LANGUAGE);
            }
        });

        mBinding.cbAutoTrans.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(!compoundButton.isPressed()) {
                    return;
                }

                Preferences.setAutoTransEnable(isChecked);
            }
        });

        mBinding.cbAutoAlert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(!compoundButton.isPressed()) {
                    return;
                }

                Preferences.setNoticeEnable(isChecked);
            }
        });

        mBinding.tvButtonChatOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean chatTimeOrder = Preferences.getChatTimeOrder();
                Preferences.setChatTimeOrder(!chatTimeOrder);
                mBinding.tvButtonChatOrder.setText(Preferences.getChatTimeOrder() ?
                        getResources().getString(R.string.text_up_title) :  getResources().getString(R.string.text_down_title));
            }
        });
    }


    private void setDefaultValue() {
        //boolean isTranslation = Preferences.getChatTranslationUse();
        //mBinding.switchMessageTranslation.setSelected(isTranslation);
        //mBinding.cbAutoTrans.setChecked(isTranslation);

        String myLanguage = Preferences.getChatTranslationLanguageCode();
        String mSelectLanguage = "";


        if(myLanguage.equals("ko")) {
            mSelectLanguage = getString(R.string.lang_ko);
        } else if(myLanguage.equals("zh-CN")) {
            mSelectLanguage = getString(R.string.lang_cn);
        } else if(myLanguage.equals("en")) {
            mSelectLanguage = getString(R.string.lang_en);
        } else if(myLanguage.equals("id")) {
            mSelectLanguage = "bahasa Indonesia";
        } else if(myLanguage.equals("vi")) {
            mSelectLanguage = getString(R.string.lang_vi);
        }else{
            CountryRepository mCountryRepository = CountryRepository.getInstance();
            mSelectLanguage = mCountryRepository.getCountryModel(myLanguage.length() == 0 ? "en" : myLanguage).getCountryName();

        }

        mBinding.tvButtonTransLanguage.setText(mSelectLanguage + "↓");


        String saveDate = String.format(getResources().getString(R.string.chat_manage_text_backup_date), Preferences.getBackUpSaveTime());
        mBinding.tvBackupDate.setText(saveDate);

        String restoreDate = String.format(getResources().getString(R.string.chat_manage_text_restore_date), Preferences.getBackUpRestoreTime());
        mBinding.tvRestoreDate.setText(restoreDate);

        mBinding.cbAutoTrans.setChecked(Preferences.getAutoTransEnalbe());
        mBinding.cbAutoAlert.setChecked(Preferences.getNoticeEnable());
        mBinding.tvButtonChatOrder.setText(Preferences.getChatTimeOrder() ?
                getResources().getString(R.string.text_up_title) :  getResources().getString(R.string.text_down_title));
    }

    private void getThreadInfo(String threadKey) {
        ProgressUtil.showProgress(this);
        ThreadChannelApi threadApi = new ThreadChannelApi(this, threadKey);
        threadApi.request(new BaseHttpRequest.APICallbackListener<ThreadWithChannel>() {
            @Override
            public void onSuccess(ThreadWithChannel res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatNormalSetActivity.this);
                        setChatMember(res.getMembers());

                        Log.e("@@@TAG", "thread info : " + res.getIsDirect());

                        mIsDirect = (res.getIsDirect().equals("Y")) ? true : false;

                        //mBinding.switchMessageAlert.setSelected(mIsAlarm);
                        mBinding.cbAutoAlert.setChecked(mIsAlarm);
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(ChatNormalSetActivity.this);
                    }
                });
            }
        });
    }


    private void alarmRequest(boolean enable) {
        ProgressUtil.showProgress(this);

        String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
        AlarmThreadApi alarmThreadApi = new AlarmThreadApi(this, userXid, mCurrentThreadKey, enable);
        alarmThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                ProgressUtil.hideProgress(ChatNormalSetActivity.this);
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                ProgressUtil.hideProgress(ChatNormalSetActivity.this);
                Log.e("@@@TAG", error.getMessage());
            }
        });
    }


    private void setChatMember(ArrayList<ThreadMember> members) {

        ChatRepository.getInstance().initialize();
        ChatRepository.getInstance().setThreadKey(mCurrentThreadKey);
        //mBinding.layoutFriendContainer.removeAllViews();
        ArrayList<FriendListModel> friendListModels = new ArrayList<>();

        String userXidMe = MineTalkApp.getUserInfoModel().getUser_xid();

        if(members.size() > 0) {
            for(int i = 0 ; i < members.size() ; i++) {

                if(userXidMe.equals(members.get(i).getXid())) {
                    mIsAlarm = members.get(i).getAlarm().toUpperCase().equals("Y") ? true : false;
                }

                FriendListModel itemModel = new FriendListModel();
                itemModel.setUser_xid(members.get(i).getXid());

                itemModel.setUser_name(members.get(i).getNickName());

//                String contactName = contactMap.get(members.get(i).getXid());
//                if(contactName == null || contactMap.equals("")) {
//                    itemModel.setUser_name(members.get(i).getNickName());
//                } else {
//                    itemModel.setUser_name(contactName);
//                }

                itemModel.setUser_profile_image(members.get(i).getProfileImageUrl());

                friendListModels.add(itemModel);

                ChatRepository.getInstance().addUserInfo(itemModel.getUser_xid(), itemModel.getUser_name());
            }
        }

        for(int i = 0 ; i < friendListModels.size() ; i++) {
            UserIconView iconView = new UserIconView(this);
            iconView.setData(friendListModels.get(i));

            //mBinding.layoutFriendContainer.addView(iconView);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MineTalk.REQUEST_CODE_SELECT_FRIEND && resultCode == RESULT_OK) {
            if(data != null && data.getExtras() != null) {
                String result = data.getExtras().getString("result");
                ChatRepository.getInstance().initialize();
                if(result.equals("invite_complete")) {
                    setResult(RESULT_OK);
                    finish();
                }
            }
        }
    }
}
