package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.app.Activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.SelectFriendGridAdapter;
import kr.co.minetalk.adapter.SelectFriendListAdapter;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.ActivitySelectFriendBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.CreateGroupThreadApi;
import kr.co.minetalk.message.api.CreateThreadApi;
import kr.co.minetalk.message.api.InviteGroupThreadApi;
import kr.co.minetalk.message.api.KickThreadApi;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.repository.ChatRepository;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class SelectFriendActivity extends BindActivity<ActivitySelectFriendBinding> {

    private static final String COLUMN_ACTIVITY_TITLE = "activity_title";
    private static final String COLUMN_TYPE = "type";

    public static final String TYPE_INVITE = "type_invite";
    public static final String TYPE_NEW = "type_new";


    public static void startActivity(Context context, String activity_title) {
        Intent intent = new Intent(context, SelectFriendActivity.class);
        intent.putExtra(COLUMN_ACTIVITY_TITLE, activity_title);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String activity_title, String type) {
        Intent intent = new Intent(context, SelectFriendActivity.class);
        intent.putExtra(COLUMN_ACTIVITY_TITLE, activity_title);
        intent.putExtra(COLUMN_TYPE, type);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String activity_title, String type, int requestCode) {
        Intent intent = new Intent(context, SelectFriendActivity.class);
        intent.putExtra(COLUMN_ACTIVITY_TITLE, activity_title);
        intent.putExtra(COLUMN_TYPE, type);
        ((Activity) context).startActivityForResult(intent, requestCode);

    }

    private GridLayoutManager mGridLayoutManager;
    private SelectFriendListAdapter mListAdapter;
    private SelectFriendGridAdapter mGridAdapter;
    private ArrayList<FriendListModel> mFriendList = new ArrayList<>();
    private String mActivityTitle;
    private String mCurrentType = TYPE_NEW;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }


    private void loadFriend() {

        if (mCurrentType.equals(TYPE_NEW)) {
            mFriendList.clear();

            for (int i = 0; i < MineTalkApp.getFriendList().size(); i++) {
                FriendListModel data = MineTalkApp.getFriendList().get(i);

                FriendListModel item = new FriendListModel();
                item.setUser_xid(data.getUser_xid());
                item.setUser_hp(data.getUser_hp());
                item.setUser_name(data.getUser_name());
                item.setUser_state_message(data.getUser_state_message());
                item.setUser_profile_image(data.getUser_profile_image());

                mFriendList.add(item);
            }

        } else if (mCurrentType.equals(TYPE_INVITE)) {
            mFriendList.clear();

            for (int i = 0; i < MineTalkApp.getFriendList().size(); i++) {
                FriendListModel data = MineTalkApp.getFriendList().get(i);

                FriendListModel item = new FriendListModel();
                item.setUser_xid(data.getUser_xid());
                item.setUser_hp(data.getUser_hp());
                item.setUser_name(data.getUser_name());
                item.setUser_state_message(data.getUser_state_message());
                item.setUser_profile_image(data.getUser_profile_image());

                if (!ChatRepository.getInstance().containUser(item.getUser_xid())) {
                    mFriendList.add(item);
                }
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_select_friend;
    }

    @Override
    protected void initView() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            mActivityTitle = getIntent().getExtras().getString(COLUMN_ACTIVITY_TITLE, MineTalk.POINT_TYPE_CASH);
            mCurrentType = getIntent().getExtras().getString(COLUMN_TYPE, TYPE_NEW);
        }
        mBinding.setHandlers(this);

        if (mActivityTitle == null || mActivityTitle.equals("")) {
            mActivityTitle = getString(R.string.select_friend_title);
        }

        mBinding.tvActivityTitle.setText(mActivityTitle);

        mListAdapter = new SelectFriendListAdapter();
        mGridAdapter = new SelectFriendGridAdapter();

        mGridLayoutManager = new GridLayoutManager(this, 1);
        mBinding.recyclerView.setLayoutManager(mGridLayoutManager);
        mBinding.recyclerView.setAdapter(mListAdapter);

        String countMessage = String.format("친구 %d명", 0);
        mBinding.tvSelectCount.setText(countMessage);

        setUIEventListener();
        setupData();
        updateTheme();
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.ibSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                mBinding.ibSortList.setText(bDescOrder ? "가↑" : "가↓");
                setupData();
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.viewGradient.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);

                mBinding.viewGradient.setVisibility(View.INVISIBLE);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Preferences.getSelectFriendListType()){ // Grid Type
                    mBinding.recyclerView.scrollToPosition(mGridAdapter.getItemCount()-1);
                }else{
                    mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                }

                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.ivListType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setSelectFriendListType(!Preferences.getSelectFriendListType());
                boolean listType = Preferences.getSelectFriendListType();

                if(MineTalk.isBlackTheme) { //GridType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
                } else { //ListType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
                }

                if (listType) { //GridType
                    mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                    mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            return position == 0 ? BaseFragment.mRowNum : 1;
                        }
                    });
                    mBinding.recyclerView.setAdapter(mGridAdapter);
                } else {
                    mGridLayoutManager.setSpanCount(1);
                    mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            return 1;
                        }
                    });
                    mBinding.recyclerView.setAdapter(mListAdapter);
                }
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean listType = Preferences.getSelectFriendListType();
                int selectCount = listType ? mGridAdapter.getSelectData().size() : mListAdapter.getSelectData().size();
                ArrayList<FriendListModel> selData = listType ? mGridAdapter.getSelectData() : mListAdapter.getSelectData();

                if (mCurrentType.equals(TYPE_NEW)) {
                    if (selectCount > 0) {
                        String user_xid = MineTalkApp.getUserInfoModel().getUser_xid();
                        if (selectCount == 1) {
                            newThreadRequest(user_xid, selData.get(0).getUser_xid());
                        } else if (selectCount > 1) {

                            String[] xids = new String[selectCount];
                            for (int i = 0; i < selectCount; i++) {
                                xids[i] = selData.get(i).getUser_xid();
                            }
                            newThreadGroupRequest(user_xid, xids);

                        }
                    }
                } else if (mCurrentType.equals(TYPE_INVITE)) {
                    int chattingRoomCount = ChatRepository.getInstance().chattingRoomMemberCount();
                    if (chattingRoomCount <= 2) {
                        if (selectCount > 0) {
                            ArrayList<String> member = new ArrayList<>();
                            String user_xid = MineTalkApp.getUserInfoModel().getUser_xid();

                            Set<String> keyset = ChatRepository.getInstance().getmFriendXidInfoMap().keySet();
                            Iterator<String> iterator = keyset.iterator();

                            int inviteCount = 0;
                            while (iterator.hasNext()) {
                                String memberXid = iterator.next();
                                if (!memberXid.equals(user_xid)) {
                                    member.add(memberXid);
                                    inviteCount = inviteCount + 1;
                                }
                            }

                            String[] xids = new String[selectCount + inviteCount];
                            for (int i = 0; i < selectCount; i++) {
                                xids[i] = selData.get(i).getUser_xid();
                            }

                            int xidsIndex = xids.length - 1;
                            for (int j = 0; j < member.size(); j++) {
                                xids[xidsIndex + j] = member.get(j);
                            }

                            newThreadGroupRequest(user_xid, xids);
                        }

                    }else {
                        if (selectCount > 0) {
                            String user_xid = MineTalkApp.getUserInfoModel().getUser_xid();
                            String[] xids = new String[selectCount];
                            for (int i = 0; i < selectCount; i++) {
                                xids[i] = selData.get(i).getUser_xid();
                            }
                            initThreadMemeberRequest(ChatRepository.getInstance().getThreadKey(), user_xid, xids);
                        }
                    }
                }
            }
        });

        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mListAdapter.filter(s.toString());
                mGridAdapter.filter(s.toString());
                if(s.length() > 0){
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setupData() {
        loadFriend();

//        for( int i = 0; i < 100; ++i)
//            mFriendList.add(mFriendList.get(0));

        //정렬
        Collections.sort(mFriendList, Preferences.getFriendSelectOrder() ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);

        mListAdapter.setData(mFriendList);
        mListAdapter.setAdapterListener(mFriendListViewOrderListener);
        mGridAdapter.setData(mFriendList);
        mGridAdapter.setAdapterListener(mFriendListViewOrderListener);

        boolean listType = Preferences.getSelectFriendListType();
        if (listType) { //GridType
            mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            mGridLayoutManager.setSpanCount(1);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }

        String countMessage = String.format("친구 %d명", mFriendList.size());
        mBinding.tvSelectCount.setText(countMessage);
    }


    private void newThreadRequest(String user_xid, String friend_xids) {
        ProgressUtil.showProgress(this);
        CreateThreadApi createThreadApi = new CreateThreadApi(this, user_xid, friend_xids);
        createThreadApi.request(new BaseHttpRequest.APICallbackListener<ThreadData>() {
            @Override
            public void onSuccess(ThreadData res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(SelectFriendActivity.this);
                        mListAdapter.notifyDataSetChanged();

                        String threadName = "";
                        if (res.getMembers().size() > 0) {
                            String members = "";
                            for (ThreadMember tmember : res.getMembers()) {
                                if (!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {

                                    String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                    if(contactName == null || contactName.equals("")) {
                                        members = members + tmember.getNickName();
                                    } else {
                                        members = members + contactName;
                                    }
                                    break;
                                }
                            }
                            threadName = members;
                        }

                        ChatDetailActivity.startActivity(SelectFriendActivity.this, res.getThreadKey(), res.getJoinMemberCount(), threadName);
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(SelectFriendActivity.this);
                        mListAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private void newThreadGroupRequest(String user_xid, String[] friend_xid) {
        ProgressUtil.showProgress(this);
        CreateGroupThreadApi createGroupThreadApi = new CreateGroupThreadApi(this, user_xid, friend_xid);
        createGroupThreadApi.request(new BaseHttpRequest.APICallbackListener<ThreadData>() {
            @Override
            public void onSuccess(ThreadData res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(SelectFriendActivity.this);
                        mListAdapter.notifyDataSetChanged();

                        String threadName = "";
                        if (res.getMembers().size() > 0) {
                            String members = "";
                            int count = 0;
                            for (ThreadMember tmember : res.getMembers()) {
                                //members = members + tmember.getNickName() + ",";

                                String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                if(contactName == null || contactName.equals("")) {
                                    members = members + tmember.getNickName();
                                } else {
                                    members = members + contactName;
                                }

                                if(count != (res.getMembers().size() - 1)) {
                                    members = members + ",";
                                }
                                count++;
                            }
                            threadName = members;
                        }

                        ChatDetailActivity.startActivity(SelectFriendActivity.this, res.getThreadKey(), res.getJoinMemberCount(), threadName);
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(SelectFriendActivity.this);
                        mListAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private void initThreadMemeberRequest(String threadKey, String user_xid, String[] friend_xid) {

        ProgressUtil.showProgress(this);
        InviteGroupThreadApi inviteGroupThreadApi = new InviteGroupThreadApi(this, user_xid, friend_xid, threadKey);
        inviteGroupThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(SelectFriendActivity.this);

                        Intent intent = new Intent();
                        intent.putExtra("result", "invite_complete");

                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(SelectFriendActivity.this);
                    }
                });
            }
        });
    }

    private OnFriendListViewOrderListener mFriendListViewOrderListener = new OnFriendListViewOrderListener() {
        @Override
        public void refreshListType(boolean bGridType) {
            boolean listType = Preferences.getSelectFriendListType();

            if(MineTalk.isBlackTheme) { //GridType
                mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
            } else { //ListType
                mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
            }

            if (listType) { //GridType
                mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return position == 0 ? BaseFragment.mRowNum : 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mGridAdapter);
            } else {
                mGridLayoutManager.setSpanCount(1);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mListAdapter);
            }
        }

        @Override
        public void refreshOrder(boolean bOrder) {
            bDescOrder = bOrder;
            setupData();
        }
    };

    public void updateTheme() {
        super.updateTheme();

        boolean listType = Preferences.getSelectFriendListType();

        if (MineTalk.isBlackTheme) {
            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.bk_point_color));

        } else {
            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);

            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.app_point_color));

        }


    }
}
