package kr.co.minetalk.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.PointInfoApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.PointInfoResponse;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.ActivityPointReportMoreBinding;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;

public class PointReportMoreActivity extends BindActivity<ActivityPointReportMoreBinding> {
    private final int PERMISSON = 1;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PointReportMoreActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flag) {
        Intent intent = new Intent(context, PointReportMoreActivity.class);
        intent.addFlags(flag);
        context.startActivity(intent);
    }

//    private ListMineCashPointApi mListCashPointApi;
//    private ListMineTokenPointApi mListTokenPointApi;
    private PointInfoApi mPointInfoApi;
    private int coinType = PointSendActivity.MODE_CASH;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

        if(baseModel instanceof UserInfoBaseModel) {
            MineTalkApp.setUserInfoModel((UserInfoBaseModel) baseModel);
            pointInfoRequest();
        }

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_point_report_more;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();

        pointInfoRequest();

        //setupCheckBox();
        //pointHistoryRequest(m PointType);
    }

    @Override
    protected void onResume() {
        super.onResume();
        pointInfoRequest();
    }

    private void initApi() {
        mPointInfoApi = new PointInfoApi(this, new ApiBase.ApiCallBack<PointInfoResponse>() {
            @Override
            public void onPreparation() {
                try {
                    ProgressUtil.showProgress(PointReportMoreActivity.this);
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(int request_code, PointInfoResponse response) {
                try {
                    ProgressUtil.hideProgress(PointReportMoreActivity.this);
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }

                if(response != null) {
                    setPointInfo(response);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {

            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //캐쉬 결제하기(QR)
        mBinding.layoutButtonCashGiftQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= 23) {
                    coinType = PointSendActivity.MODE_CASH;
                    if(ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MineTalkApp.getCurrentActivity(), new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        PayActivity.startActivity(MineTalkApp.getCurrentActivity(), PointSendActivity.MODE_CASH);
                    }
                }
            }
        });

        //캐쉬 선물하기
        mBinding.layoutButtonCashGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendPointUserActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.POINT_TYPE_CASH);
            }
        });

        //캐쉬 구매내역
        mBinding.layoutButtonCashHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointReportActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.POINT_TYPE_CASH);
            }
        });

        //캐쉬 연간회원권
        mBinding.layoutButtonCashMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChargeActivity.startActivity(MineTalkApp.getCurrentActivity(), ChargeActivity.POINT_TYPE_CASH);
            }
        });

        //토큰 결제하기(QR)
        mBinding.layoutButtonTokenGiftQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= 23) {
                    coinType = PointSendActivity.MODE_TOKEN;
                    if(ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MineTalkApp.getCurrentActivity(), new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        PayActivity.startActivity(MineTalkApp.getCurrentActivity(), PointSendActivity.MODE_TOKEN);
                    }
                }
            }
        });

        //토큰 선물하기
        mBinding.layoutButtonTokenGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendPointUserActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.POINT_TYPE_TOKEN);
            }
        });

        //토큰 거래내역
        mBinding.layoutButtonTokenHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointReportActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.POINT_TYPE_TOKEN);
            }
        });

        mBinding.btnCashRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pointInfoRequest();
            }
        });

        mBinding.btnTokenRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pointInfoRequest();
            }
        });


        //캐쉬 보유내역
        mBinding.btnCashHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointCashGainActivity.startActivity(MineTalkApp.getCurrentActivity());
            }
        });

        //유예 토큰 보유 내역
        mBinding.btnTokenLockHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointTokenGainActivity.startActivity(MineTalkApp.getCurrentActivity());
            }
        });

    }

    private void pointInfoRequest() {
        mPointInfoApi.execute();
    }

    private void setPointInfo(PointInfoResponse response) {
        String totalCash = "";
        String normalCash = "";

        String mctk = "";
        String nomalToken = "";
        String lockToken = "";

        try {
            totalCash = CommonUtils.comma_won(response.getUser_mine_cash());
            normalCash = CommonUtils.comma_won(response.getBasic_mine_cash());

            mctk = CommonUtils.comma_won(response.getMctk());
            nomalToken = CommonUtils.comma_won(response.getUser_mine_token());
            lockToken = CommonUtils.comma_won(response.getLock_mine_token());
        }catch (NullPointerException e){
            e.printStackTrace();
        }


        mBinding.tvCashTotal.setText(totalCash);
        mBinding.tvCashNormal.setText(normalCash);
        mBinding.tvTokenMctk.setText(mctk);
        mBinding.tvTokenMine.setText(nomalToken);
        mBinding.tvTokenLock.setText(lockToken);
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            //Cash
            mBinding.tvCashTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvCashTotal.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvCashNormalTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvCashNormal.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvCashLockTitle.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvCashSendPresentQr.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvCashSendPresent.setTextColor(getResources().getColor(R.color.white_color));

            //mBinding.ivCashGiftQr.setImageResource(R.drawable.icon_history_bk);
            //mBinding.ivCashGift.setImageResource(R.drawable.icon_history_bk);
            mBinding.tvCashHistory.setTextColor(getResources().getColor(R.color.white_color));
            //mBinding.ivCashHistory.setImageResource(R.drawable.icon_contents_bk);
            mBinding.tvCashMember.setTextColor(getResources().getColor(R.color.white_color));
            //mBinding.ivCashMember.setImageResource(R.drawable.icon_exchange_bk);

            mBinding.layoutButtonCash.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonCashNormal.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonCashGiftQr.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonCashGift.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonCashLock.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonCashMember.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonCashCharge.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonCashHistory.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));


            //Token
            mBinding.tvMctkTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTokenMctk.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTokenNormalTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTokenMine.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTokenLockTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvTokenLock.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvSendPresentQr.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvSendPresent.setTextColor(getResources().getColor(R.color.white_color));

            //mBinding.ivTokenGiftQr.setImageResource(R.drawable.icon_history_bk);
            //mBinding.ivTokenGift.setImageResource(R.drawable.icon_history_bk);

            //mBinding.ivTokenHistory.setImageResource(R.drawable.icon_contents_bk);
            mBinding.tvTokenHistory.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonTokenMtck.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonTokenNormal.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonTokenLock.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.layoutButtonTokenGiftQr.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonTokenGift.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutButtonTokenHistory.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));


            mBinding.viewTokenMtck.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));
            mBinding.viewButtonTokenNormal.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));
            mBinding.viewButtonCash.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));
            mBinding.viewButtonCashNormal.setBackgroundColor(getResources().getColor(R.color.bk_divider_bg));

            mBinding.tvMineTokenTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvMineCashTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));

            mBinding.btnTokenRefresh.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnTokenLockHistory.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnCashRefresh.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnCashHistory.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnCashLockHistory.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnTokenRefresh.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnTokenLockHistory.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnCashRefresh.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnCashHistory.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnCashLockHistory.setBackgroundResource(R.drawable.button_bg_4dcdb60c);
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));

            //Cash
            mBinding.tvCashTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvCashTotal.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvCashNormalTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvCashNormal.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvCashSendPresent.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvCashLockTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            //mBinding.ivCashGiftQr.setImageResource(R.drawable.icon_history);
            //mBinding.ivCashGift.setImageResource(R.drawable.icon_history);

            mBinding.tvCashHistory.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.ivCashHistory.setImageResource(R.drawable.icon_left_list);
            mBinding.tvCashMember.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.ivCashMember.setImageResource(R.drawable.icon_ex);

            mBinding.layoutButtonCash.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonCashNormal.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonCashGiftQr.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonCashGift.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonCashLock.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonCashMember.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonCashCharge.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonCashHistory.setBackgroundColor(getResources().getColor(R.color.white_color));


            //Token
            mBinding.layoutButtonTokenMtck.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonTokenNormal.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonTokenLock.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonTokenGiftQr.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonTokenGift.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonTokenHistory.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvMctkTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTokenMctk.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTokenNormalTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTokenMine.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTokenLockTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTokenLock.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvSendPresent.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.ivTokenGiftQr.setImageResource(R.drawable.icon_history);
            //mBinding.ivTokenGift.setImageResource(R.drawable.icon_history);
            mBinding.tvTokenHistory.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.ivTokenHistory.setImageResource(R.drawable.icon_left_list);

            mBinding.viewTokenMtck.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
            mBinding.viewButtonTokenNormal.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
            mBinding.viewButtonCash.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));
            mBinding.viewButtonCashNormal.setBackgroundColor(getResources().getColor(R.color.main_divider_bg));

            mBinding.tvMineTokenTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvMineCashTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnTokenRefresh.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnTokenLockHistory.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnCashRefresh.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnCashHistory.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnCashLockHistory.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.btnTokenRefresh.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnTokenLockHistory.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnCashRefresh.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnCashHistory.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnCashLockHistory.setBackgroundResource(R.drawable.button_bg_4d16c066);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PayActivity.startActivity(MineTalkApp.getCurrentActivity(), coinType);
                } else {
                    return;
                }
        }
    }
}
