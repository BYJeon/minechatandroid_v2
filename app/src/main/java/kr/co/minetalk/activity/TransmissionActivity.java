package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.view.View;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.TransmissionPagerAdapter;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.databinding.ActivityTransmissionBinding;
import kr.co.minetalk.fragment.SelectChatListFragment;
import kr.co.minetalk.fragment.SelectFriendFragment;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.CreateThreadApi;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;

public class TransmissionActivity extends BindActivity<ActivityTransmissionBinding> {
    public static void startActivity(Context context, int requestCode) {
        Intent intent = new Intent(context, TransmissionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        ((Activity) context).startActivityForResult(intent, requestCode);
    }

    public static void startActivity(Context context, String sharedText, int requestCode) {
        Intent intent = new Intent(context, TransmissionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(Intent.EXTRA_TEXT, sharedText);
        ((Activity) context).startActivityForResult(intent, requestCode);
    }

    public static void startActivity(Context context, Uri imgUri, int requestCode) {
        Intent intent = new Intent(context, TransmissionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(Intent.EXTRA_STREAM, imgUri);
        ((Activity) context).startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }
    private static final int TAB_FRIEND = 0;
    private static final int TAB_CHAT   = 1;
    private int mCurrentTab = TAB_FRIEND;

    private TransmissionPagerAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_transmission;
    }

    @Override
    protected void initView() {
        mBinding.setHandlers(this);

        mCurrentTab = TAB_FRIEND;
        mBinding.viewTabFriend.setVisibility(View.VISIBLE);
        mBinding.viewTabChat.setVisibility(View.INVISIBLE);

        setUIEventListener();

        mAdapter = new TransmissionPagerAdapter(getSupportFragmentManager(), null);
        mBinding.viewPager.setAdapter(mAdapter);
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.tvBtnFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentTab = TAB_FRIEND;
                mBinding.viewTabFriend.setVisibility(View.VISIBLE);
                mBinding.viewTabChat.setVisibility(View.INVISIBLE);

                mBinding.viewPager.setCurrentItem(0);
            }
        });

        mBinding.tvBtnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentTab = TAB_CHAT;
                mBinding.viewTabFriend.setVisibility(View.INVISIBLE);
                mBinding.viewTabChat.setVisibility(View.VISIBLE);

                mBinding.viewPager.setCurrentItem(1);
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageAlert(getResources().getString(R.string.chat_trasmission_message),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    if(mCurrentTab == TAB_FRIEND) {
                                        SelectFriendFragment fragment = (SelectFriendFragment)mAdapter.getItem(0);
                                        if(fragment != null) {
                                            String selectUserXid = fragment.getSelectUserXid();
                                            if(selectUserXid != null && !selectUserXid.equals("")) {
                                                String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
                                                newThreadRequest(userXid, selectUserXid);
                                            }
                                        }
                                    } else if(mCurrentTab == TAB_CHAT) {
                                        SelectChatListFragment fragment = (SelectChatListFragment)mAdapter.getItem(1);
                                        if(fragment != null) {
                                            String threadKey = fragment.getSelectData();

                                            if(threadKey == null || threadKey.equals("")) {
                                                return;
                                            }

                                            Intent intentData = new Intent();
                                            intentData.putExtra("select_threadKey", threadKey);

                                            setResult(RESULT_OK, intentData);
                                            finish();
                                        }
                                    }
                                }
                            }
                        });

            }
        });

        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0) {
                    mCurrentTab = TAB_FRIEND;
                    mBinding.viewTabFriend.setVisibility(View.VISIBLE);
                    mBinding.viewTabChat.setVisibility(View.INVISIBLE);
                } else if(position == 1) {
                    mCurrentTab = TAB_CHAT;
                    mBinding.viewTabFriend.setVisibility(View.INVISIBLE);
                    mBinding.viewTabChat.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void newThreadRequest(String user_xid, String friend_xids) {
        ProgressUtil.showProgress(this);
        CreateThreadApi createThreadApi = new CreateThreadApi(this, user_xid, friend_xids);
        createThreadApi.request(new BaseHttpRequest.APICallbackListener<ThreadData>() {
            @Override
            public void onSuccess(ThreadData res) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(TransmissionActivity.this);
                        String threadKey = res.getThreadKey();

                        String threadName = "";
                        if (res.getMembers().size() > 0) {
                            String members = "";
                            for (ThreadMember tmember : res.getMembers()) {
                                if (!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {

                                    String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                    if(contactName == null || contactName.equals("")) {
                                        members = members + tmember.getNickName();
                                    } else {
                                        members = members + contactName;
                                    }
                                    break;
                                }
                            }
                            threadName = members;
                        }

                        //ChatDetailActivity.startActivity(TransmissionActivity.this, res.getThreadKey(), res.getJoinMemberCount(), threadName);
                        //finish();

                        Intent intentData = new Intent();
                        intentData.putExtra("select_threadKey", threadKey);
                        intentData.putExtra("member_count", res.getJoinMemberCount());
                        intentData.putExtra("select_threadName", threadName);

                        setResult(RESULT_OK, intentData);
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(TransmissionActivity.this);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }
}
