package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.yalantis.ucrop.UCrop;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.GoogleOcrTextDetectionApi;
import kr.co.minetalk.api.GoogleTranslateApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.TextDetectionModel;
import kr.co.minetalk.api.model.TranslateModel;
import kr.co.minetalk.databinding.ActivityCameraTranslationBinding;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.ui.popup.SimpleListPopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class CameraTranslactionActivity extends BindActivity<ActivityCameraTranslationBinding> {

    public static final String COLOUMN_IS_IMAGE = "is_image";

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, CameraTranslactionActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, boolean isImageTrans) {
        Intent intent = new Intent(context, CameraTranslactionActivity.class);
        intent.putExtra(COLOUMN_IS_IMAGE, isImageTrans);
        context.startActivity(intent);
    }

    private String mRootFilePath = "";
    private String mTempFilePath = "";
    private String mEditTempImagePath = "";

    private GoogleOcrTextDetectionApi mGoogleOcrTextDetectionApi;
    private GoogleTranslateApi mGoogleTranslateApi;

    private boolean mIsImageTranslation = false;

    private String mTopLanguageCode = "";
    private String mTopVoiceCode    = "";
    private String mTopLanguageName = "";
    private int    mTopFlagResource = -1;

    private String mBottomLanguageCode = "";
    private String mBottomVoiceCode    = "";
    private String mBottomLanguageName = "";
    private int    mBottomFlagResource = -1;

    private CountryRepository mCountryRepository;

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    private SurfaceHolder holder = null;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_camera_translation;
    }

    @Override
    protected void initView() {
        if(getIntent() != null && getIntent().getExtras() != null) {
            mIsImageTranslation = getIntent().getExtras().getBoolean(COLOUMN_IS_IMAGE, false);
        }

        mCountryRepository = CountryRepository.getInstance();

        mBinding.setHandlers(this);
        initSurfaceView();
        initApi();

        initSetting();

        setUIEventListener();

        makeDirectory();
        mRootFilePath = Environment.getExternalStorageDirectory() + "/MineTalk/trans/";

        if(mIsImageTranslation) {
            onChooseMedia();
        }
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutLanHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvSourceName.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.tvTargetName.setBackgroundResource(R.drawable.button_bg_cdb60c);
        }else{
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutLanHeader.setBackgroundColor(getResources().getColor(R.color.main_theme_sub_bg));

            mBinding.tvSourceName.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.tvTargetName.setBackgroundResource(R.drawable.button_bg_16c066);
        }
    }

    private void initSurfaceView() {
        holder = mBinding.cameraPreview.getHolder();
        holder.addCallback(mBinding.cameraPreview);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    private void initSetting() {
        String topLanguage = Preferences.getCameraSourceCode();
        if(topLanguage.equals("")) {
            mTopLanguageCode = mCountryRepository.getCountryModel("ko").getCode();
            mTopVoiceCode    = mCountryRepository.getCountryModel("ko").getVoice_code();
            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                mTopLanguageName = mCountryRepository.getCountryModel("ko").getCountryName();
            } else {
                mTopLanguageName = mCountryRepository.getCountryModel("ko").getCountryNameOther();
            }
            mTopFlagResource = CommonUtils.getResourceImage(this, mCountryRepository.getCountryModel("ko").getFlag_image());

        } else {
            mTopLanguageCode = mCountryRepository.getCountryModel(topLanguage).getCode();
            mTopVoiceCode    = mCountryRepository.getCountryModel(topLanguage).getVoice_code();
            mTopLanguageName    = mCountryRepository.getCountryModel(topLanguage).getCountryName();

            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                mTopLanguageName = mCountryRepository.getCountryModel(topLanguage).getCountryName();
            } else {
                mTopLanguageName = mCountryRepository.getCountryModel(topLanguage).getCountryNameOther();
            }

            mTopFlagResource = CommonUtils.getResourceImage(this, mCountryRepository.getCountryModel(topLanguage).getFlag_image());
        }


        String bottomLanguage = Preferences.getCameraTargetCode();
        if(bottomLanguage.equals("")) {
            mBottomLanguageCode = mCountryRepository.getCountryModel("en").getCode();
            mBottomVoiceCode    = mCountryRepository.getCountryModel("en").getVoice_code();
            mBottomLanguageName = mCountryRepository.getCountryModel("en").getCountryName();

            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                mBottomLanguageName = mCountryRepository.getCountryModel("en").getCountryName();
            } else {
                mBottomLanguageName = mCountryRepository.getCountryModel("en").getCountryNameOther();
            }

            mBottomFlagResource = CommonUtils.getResourceImage(this, mCountryRepository.getCountryModel("en").getFlag_image());
        } else {
            mBottomLanguageCode = mCountryRepository.getCountryModel(bottomLanguage).getCode();
            mBottomVoiceCode    = mCountryRepository.getCountryModel(bottomLanguage).getVoice_code();

            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                mBottomLanguageName = mCountryRepository.getCountryModel(bottomLanguage).getCountryName();
            } else {
                mBottomLanguageName = mCountryRepository.getCountryModel(bottomLanguage).getCountryNameOther();
            }


            mBottomFlagResource = CommonUtils.getResourceImage(this, mCountryRepository.getCountryModel(bottomLanguage).getFlag_image());
        }

//        mBinding.ivSourceFlag.setImageResource(getCountryImageResource(mTopLanguageCode));
        mBinding.ivSourceFlag.setImageResource(mTopFlagResource);
        mBinding.tvSourceName.setText(mTopLanguageName);

//        mBinding.ivTargetFlag.setImageResource(getCountryImageResource(mBottomLanguageCode));
        mBinding.ivTargetFlag.setImageResource(mBottomFlagResource);
        mBinding.tvTargetName.setText(mBottomLanguageName);
    }

    private void popupTopLanuge() {
        SimpleListPopup simpleListPopup = new SimpleListPopup();
        simpleListPopup.setOnItemIndexListener(new SimpleListPopup.OnItemIndexListener() {
            @Override
            public void onItemClick(int index) {
                mTopLanguageCode = mCountryRepository.getItem(index).getCode();
                mTopVoiceCode    = mCountryRepository.getItem(index).getVoice_code();
                mTopLanguageName = mCountryRepository.getItem(index).getCountryName();
                mTopFlagResource = CommonUtils.getResourceImage(CameraTranslactionActivity.this, mCountryRepository.getItem(index).getFlag_image());


//                mBinding.ivSourceFlag.setImageResource(getCountryImageResource(mTopLanguageCode));
                mBinding.ivSourceFlag.setImageResource(mTopFlagResource);


                if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                    mBinding.tvSourceName.setText(mCountryRepository.getItem(index).getCountryName());
                } else {
                    mBinding.tvSourceName.setText(mCountryRepository.getItem(index).getCountryNameOther());
                }

                Preferences.setCameraSourceCode(mTopLanguageCode);

//                mBinding.ivTargetFlag.setImageResource(getCountryImageResource(mBottomLanguageCode));
//                mBinding.tvTargetName.setText(mBottomLanguageName);
            }
        });
        simpleListPopup.show(getSupportFragmentManager(), "popup_top_lang");
    }

    private void  popupBottomLanuge() {
        SimpleListPopup simpleListPopup = new SimpleListPopup();
        simpleListPopup.setOnItemIndexListener(new SimpleListPopup.OnItemIndexListener() {
            @Override
            public void onItemClick(int index) {
                mBottomLanguageCode = mCountryRepository.getItem(index).getCode();
                mBottomVoiceCode    = mCountryRepository.getItem(index).getVoice_code();
                mBottomFlagResource = CommonUtils.getResourceImage(CameraTranslactionActivity.this, mCountryRepository.getItem(index).getFlag_image());

//                mBinding.ivSourceFlag.setImageResource(getCountryImageResource(mTopLanguageCode));
//                mBinding.tvSourceName.setText(mCountryRepository.getItem(index).getCountryName());

//                mBinding.ivTargetFlag.setImageResource(getCountryImageResource(mBottomLanguageCode));
                mBinding.ivTargetFlag.setImageResource(mBottomFlagResource);

                if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                    mBinding.tvTargetName.setText(mCountryRepository.getItem(index).getCountryName());
                } else {
                    mBinding.tvTargetName.setText(mCountryRepository.getItem(index).getCountryNameOther());
                }


                Preferences.setCameraTargetCode(mBottomLanguageCode);

            }
        });
        simpleListPopup.show(getSupportFragmentManager(), "popup_bot_lang");
    }


    private void initApi() {
        mGoogleOcrTextDetectionApi = new GoogleOcrTextDetectionApi(this, new ApiBase.ApiCallBack<TextDetectionModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(CameraTranslactionActivity.this);
            }

            @Override
            public void onSuccess(int request_code, TextDetectionModel textDetectionModel) {
                ProgressUtil.hideProgress(CameraTranslactionActivity.this);
                if(textDetectionModel != null) {
                    Log.e("@@@JEROME", "locale : " + textDetectionModel.getLocale());
                    Log.e("@@@JEROME", "desc : " + textDetectionModel.getDescription());

                    String description = textDetectionModel.getDescription();
                    String languageCode = textDetectionModel.getLocale();

//                    translationRequest(description, languageCode, "en");
                    TranslationResultActivity.startActivity(CameraTranslactionActivity.this, languageCode, description, mBottomLanguageCode, "");


                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(CameraTranslactionActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mGoogleTranslateApi = new GoogleTranslateApi(this, new ApiBase.ApiCallBack<TranslateModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(CameraTranslactionActivity.this);
            }

            @Override
            public void onSuccess(int request_code, TranslateModel translateModel) {
                ProgressUtil.hideProgress(CameraTranslactionActivity.this);
                if(translateModel != null) {
                    Log.e("@@@JEROME", "text : " + translateModel.getText());
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(CameraTranslactionActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void translationRequest(String originalText, String originalLanguageCode, String targetCode) {
        mGoogleTranslateApi.execute(originalText, originalLanguageCode, targetCode);
    }

    private void googleOcrRequest(String base64_image) {


        mGoogleOcrTextDetectionApi.execute(base64_image);
    }


    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mBinding.ivAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChooseMedia();
            }
        });

        mBinding.ivTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mBinding.cameraPreview.takePhoto(new android.hardware.Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(byte[] data, android.hardware.Camera camera) {
                            File file = new File(mRootFilePath+"temp_file"+".jpg");
                            mTempFilePath = mRootFilePath+"temp_file"+".jpg";
                            try {
                                FileOutputStream fos = new FileOutputStream(file);
                                fos.write(data);
                                fos.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            startCropImage(file);

                        }
                    });
                } catch (Exception e) {
                    Toast.makeText(CameraTranslactionActivity.this, "ERROR => " + e.getMessage(), Toast.LENGTH_SHORT  ).show();
                }
            }
        });

        mBinding.layoutButtonSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupTopLanuge();
            }
        });

        mBinding.layoutButtonTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupBottomLanuge();
            }
        });
    }

    private void makeDirectory() {
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/MineTalk/trans/.nomedia"); //test라는 경로에 이미지를 저장하기 위함
        if(!storageDir.exists()) {
            storageDir.mkdirs();
        }

        File saveDir = new File(Environment.getExternalStorageDirectory() + "/MineTalk/trans/save/.nomedia"); // 번역 이미지 저장 폴도

        if(!saveDir.exists()) {
            saveDir.mkdirs();
        }
    }

    private void startCropImage(File file) {
        mBinding.cameraPreview.surfaceDestroyed(holder);

        // 임시로 사용할 파일의 경로를 생성

        Uri mImageCaptureUri;
        mImageCaptureUri = Uri.fromFile(file);


        startCropActivity(mImageCaptureUri);
    }

    private void startCropActivity(@NonNull Uri uri) {
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(mRootFilePath, "temp_file.jpg")));
        uCrop = advancedConfig(uCrop);

        uCrop.start(this);
    }

    private UCrop advancedConfig(@NonNull UCrop uCrop) {

        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);

        options.setCompressionQuality(50);
        options.setToolbarColor(Color.parseColor("#159de8"));
        options.setActiveWidgetColor(Color.parseColor("#159de8"));

        options.setToolbarTitle(getResources().getString(R.string.image_edit));
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(true);


        return uCrop.withOptions(options);
    }


    private void onChooseMedia() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,null);
        galleryIntent.setType("image/*");
        galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.select_work));

        startActivityForResult(chooser,MineTalk.REQUEST_CODE_MEDIA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MineTalk.REQUEST_CODE_MEDIA) {
            if(resultCode == RESULT_OK) {
                Uri uri = data.getData();
                if (uri != null) {
                    String contentType = CommonUtils.getMediaType(this, uri);
                    String contentPath = CommonUtils.getImageMediaPath(this, uri);

                    mTempFilePath = mRootFilePath + "temp_file"+".jpg";
                    startCropActivity(data.getData());

                }
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            mBinding.cameraPreview.startCameraPreView();
            Log.e("@@@TAG","REQUEST_CROP + " + resultCode);
            if (resultCode == RESULT_OK ) {
                Log.e("@@@TAG","REQUEST_CROP : OK");
                handleCropResult(data);
            }

            tmpDeleteFile(mTempFilePath);
        }
    }

    private void tmpDeleteFile(String path) {
        File file = new File(path);
        if(file.exists()) {
            file.delete();
        }
        mTempFilePath = "";
    }

    private Thread mOtherProcess = null;
    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {

            Bitmap photo = null;
            try {
                photo = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            storeCropImage(photo, mRootFilePath + System.currentTimeMillis()+".png");
//            makeBase64ToString(photo);

            if(mOtherProcess != null) {
                mOtherProcess.interrupt();
                mOtherProcess = null;
            }

            final Bitmap remakePhoto = photo;

            ProgressUtil.showProgress(this);
            mOtherProcess = new Thread(new Runnable() {

                @Override
                public void run() {
                    makeBase64ToString(remakePhoto);
                    handler.sendEmptyMessage(0);
                }
            });
            mOtherProcess.start();


        } else {
            Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT).show();
        }
    }


    private void storeCropImage(Bitmap bitmap, String filePath) {
        mEditTempImagePath = filePath;
        File copyFile = new File(filePath);
        BufferedOutputStream out = null;

        try {
            copyFile.createNewFile();
            out = new BufferedOutputStream(new FileOutputStream(copyFile));

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String mImageCode = "";
    private void makeBase64ToString(Bitmap bitmap) {
        Bitmap resize = CommonUtils.resizeBitmapImageFn(bitmap, 320);

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            resize.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] ba = bos.toByteArray();

            mImageCode = android.util.Base64.encodeToString(ba, android.util.Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final Handler handler = new Handler() {
        public void handleMessage(Message msg) {


            String message = null;
            switch(msg.what) {
                case 0:
                    googleOcrRequest(mImageCode);
                    break;
            }
        }
    };

    private int getCountryImageResource(String languageCode) {
        if(languageCode.equals("ko")) {
            return R.drawable.btn_option_korean;
        } else if(languageCode.equals("zh-CN")) {
            return R.drawable.btn_option_china;
        } else if(languageCode.equals("en")) {
            return R.drawable.btn_option_english;
        } else if(languageCode.equals("ja")) {
            return R.drawable.btn_option_japan;
        } else if(languageCode.equals("vi")) {
            return R.drawable.btn_option_vietnam;
        } else  {
            return R.drawable.btn_option_korean;
        }
    }
}
