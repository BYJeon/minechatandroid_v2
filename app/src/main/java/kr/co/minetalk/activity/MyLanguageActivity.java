package kr.co.minetalk.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.adapter.LanguageAdapter;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.databinding.ActivityMyLanguageBinding;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;

public class MyLanguageActivity extends BindActivity<ActivityMyLanguageBinding> {

    public static final int TYPE_MY_LANGUAGE = 0;
    public static final int TYPE_TRANSLATION_LANGUAGE = 1;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MyLanguageActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int type) {
        Intent intent = new Intent(context, MyLanguageActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }


    private int mCurrentLanguageType = TYPE_MY_LANGUAGE;
    private String mSelectLanguage = "";
    private LanguageAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_language;
    }

    @Override
    protected void initView() {

        if(getIntent() != null && getIntent().getExtras() != null) {
            mCurrentLanguageType = getIntent().getExtras().getInt("type", TYPE_MY_LANGUAGE);
        }

        if(mCurrentLanguageType == TYPE_MY_LANGUAGE) {
            mBinding.tvActivityTitle.setText(getResources().getString(R.string.layout_select_language));
        } else {
            mBinding.tvActivityTitle.setText(getResources().getString(R.string.chat_option_activity_text_translation_language));
        }

        mBinding.setHandlers(this);
        setUIEventListener();

        init();

        mAdapter = new LanguageAdapter(mCurrentLanguageType);
        mBinding.lvList.setAdapter(mAdapter);

        ArrayList<CountryInfoModel> countryData = CountryRepository.getInstance().getOtherCountry(mCurrentLanguageType);

//        for(int i = 0 ; i < countryData.size() ; i++) {
//            if(countryData.get(i).isSelect()) {
//
//            }
//        }
        mAdapter.setData(countryData);
    }

    private void init() {
        mBinding.iconKor.setData(R.drawable.icon_korea, CountryRepository.getInstance().getCountryModel("ko").getCountryName());
        mBinding.iconCh.setData(R.drawable.icon_china, CountryRepository.getInstance().getCountryModel("zh-CN").getCountryName());
        mBinding.iconEng.setData(R.drawable.icon_english, CountryRepository.getInstance().getCountryModel("en").getCountryName());
        mBinding.iconId.setData(R.drawable.icon_in, CountryRepository.getInstance().getCountryModel("in").getCountryName());
        mBinding.iconVi.setData(R.drawable.icon_vietnam, CountryRepository.getInstance().getCountryModel("vi").getCountryName());

//        String myLanguage = Preferences.getMyChatLanguage();
//        mSelectLanguage = Preferences.getMyChatLanguage();

        String myLanguage = "";
        mSelectLanguage = "";

        if(mCurrentLanguageType == TYPE_MY_LANGUAGE) {
            myLanguage = Preferences.getMyChatLanguage();
            mSelectLanguage = Preferences.getMyChatLanguage();
        } else {
            myLanguage = Preferences.getChatTranslationLanguageCode();
            mSelectLanguage = Preferences.getChatTranslationLanguageCode();
        }

        //현재 선택 언어 정보 표시
        CountryInfoModel transLagnModel = CountryRepository.getInstance().getCountryModel(mSelectLanguage);

        String systemLanguage = Locale.getDefault().getLanguage();
        if(systemLanguage != null && systemLanguage.equals("ko")) {
            mBinding.tvCountryName.setText(transLagnModel.getCountryName());
        } else {
            mBinding.tvCountryName.setText(transLagnModel.getCountryNameOther());
        }
        int flagImage = CommonUtils.getResourceImage(MineTalkApp.getCurrentActivity(), transLagnModel.getFlag_image());
        mBinding.ivFlag.setImageResource(flagImage);

        if(myLanguage.equals("ko")) {
            mBinding.iconKor.select();
            mBinding.iconCh.unSelect();
            mBinding.iconEng.unSelect();
            mBinding.iconId.unSelect();
            mBinding.iconVi.unSelect();
        } else if(myLanguage.equals("zh-CN")) {
            mBinding.iconKor.unSelect();
            mBinding.iconCh.select();
            mBinding.iconEng.unSelect();
            mBinding.iconId.unSelect();
            mBinding.iconVi.unSelect();
        } else if(myLanguage.equals("en")) {
            mBinding.iconKor.unSelect();
            mBinding.iconCh.unSelect();
            mBinding.iconEng.select();
            mBinding.iconId.unSelect();
            mBinding.iconVi.unSelect();
        } else if(myLanguage.equals("in")) {
            mBinding.iconKor.unSelect();
            mBinding.iconCh.unSelect();
            mBinding.iconEng.unSelect();
            mBinding.iconId.select();
            mBinding.iconVi.unSelect();
        } else if(myLanguage.equals("vi")) {
            mBinding.iconKor.unSelect();
            mBinding.iconCh.unSelect();
            mBinding.iconEng.unSelect();
            mBinding.iconId.unSelect();
            mBinding.iconVi.select();
        }

    }

    private void setUIEventListener() {

        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

//        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(mCurrentLanguageType == TYPE_MY_LANGUAGE) {
//                    Preferences.setMyChatLanguage(mSelectLanguage);
//                } else {
//                    Preferences.setChatTranslationLanguageCode(mSelectLanguage);
//                }
//
//                setResult(RESULT_OK);
//                finish();
//            }
//        });



        mBinding.iconKor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.select();
                mBinding.iconCh.unSelect();
                mBinding.iconEng.unSelect();
                mBinding.iconId.unSelect();
                mBinding.iconVi.unSelect();

                mSelectLanguage = "ko";

                mAdapter.allUnSelect();
            }
        });

        mBinding.iconCh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.unSelect();
                mBinding.iconCh.select();
                mBinding.iconEng.unSelect();
                mBinding.iconId.unSelect();
                mBinding.iconVi.unSelect();

                mSelectLanguage = "zh-CN";


                mAdapter.allUnSelect();
            }
        });

        mBinding.iconEng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.unSelect();
                mBinding.iconCh.unSelect();
                mBinding.iconEng.select();
                mBinding.iconId.unSelect();
                mBinding.iconVi.unSelect();

                mSelectLanguage = "en";

                mAdapter.allUnSelect();
            }
        });

        mBinding.iconId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.unSelect();
                mBinding.iconCh.unSelect();
                mBinding.iconEng.unSelect();
                mBinding.iconId.select();
                mBinding.iconVi.unSelect();

                mSelectLanguage = "in";

                mAdapter.allUnSelect();
            }
        });

        mBinding.iconVi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.unSelect();
                mBinding.iconCh.unSelect();
                mBinding.iconEng.unSelect();
                mBinding.iconId.unSelect();
                mBinding.iconVi.select();

                mSelectLanguage = "vi";

                mAdapter.allUnSelect();
            }
        });

        mBinding.lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mAdapter.setSelect(position);

                mBinding.iconKor.unSelect();
                mBinding.iconCh.unSelect();
                mBinding.iconEng.unSelect();
                mBinding.iconId.unSelect();
                mBinding.iconVi.unSelect();


                CountryInfoModel item =(CountryInfoModel)mAdapter.getItem(position);
                mSelectLanguage = item.getCode();

                if(mCurrentLanguageType == TYPE_MY_LANGUAGE) {
                    Preferences.setMyChatLanguage(mSelectLanguage);
                } else {
                    Preferences.setChatTranslationLanguageCode(mSelectLanguage);
                }

                //현재 선택 언어 정보 표시
                CountryInfoModel transLagnModel = CountryRepository.getInstance().getCountryModel(mSelectLanguage);

                String systemLanguage = Locale.getDefault().getLanguage();
                if(systemLanguage != null && systemLanguage.equals("ko")) {
                    mBinding.tvCountryName.setText(item.getCountryName());
                } else {
                    mBinding.tvCountryName.setText(item.getCountryNameOther());
                }

                int flagImage = CommonUtils.getResourceImage(MineTalkApp.getCurrentActivity(), transLagnModel.getFlag_image());
                mBinding.ivFlag.setImageResource(flagImage);

            }
        });


        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


}
