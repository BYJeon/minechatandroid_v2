package kr.co.minetalk.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.CouponListApi;
import kr.co.minetalk.api.PurchaseMiningCouponApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.CouponInfoModel;
import kr.co.minetalk.api.model.CouponListResponse;
import kr.co.minetalk.databinding.ActivityBuyCouponBinding;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;

public class BuyCouponActivity extends BindActivity<ActivityBuyCouponBinding> {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, BuyCouponActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flag) {
        Intent intent = new Intent(context, BuyCouponActivity.class);
        intent.addFlags(flag);
        context.startActivity(intent);
    }

    private CouponListApi mCouponListApi;
    private PurchaseMiningCouponApi mPurchaseMiningCouponApi;

    private Map<String, CouponInfoModel> mMapListCoupon = new HashMap<>();
    private Map<String, CouponInfoModel> mMapUserListToken = new HashMap<>();
    private ArrayAdapter<String> mCouponAdapter;
    private ArrayAdapter<String> mPayAdapter;

    private String curSelMiningCouponIdx = "";

    private String mBankName = "";
    private String mBankAccount = "";
    private String mBankOwner  = "";
    private String mUserName  = "";

    @Override
    protected void onDefaultApiSuccess(BaseModel baseModel) {

    }

    @Override
    protected void onDefaultApiFailure(int request_code, String message) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_buy_coupon;
    }

    @Override
    protected void initView() {

        mBinding.setHandlers(this);
        initApi();
        setUIEventListener();
        String userHp = MineTalkApp.getUserInfoModel().getUser_hp();
        if(userHp != null && !userHp.equals("")) {
            userHp = userHp.substring(userHp.length() - 8);
            mUserName = userHp;
            mBinding.tvInputUserName.setText(userHp);
        }else{
            mBinding.tvInputUserName.setText("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCouponListApi.execute();
        //tokenListRequest();
    }

    private void initApi() {
        mPurchaseMiningCouponApi = new PurchaseMiningCouponApi(this, new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(BuyCouponActivity.this);
            }

            @Override
            public void onSuccess(int request_code, BaseModel reponse) {
                ProgressUtil.hideProgress(BuyCouponActivity.this);
                if(reponse != null) {
                    showMessageAlert( String.format(getResources().getString(R.string.coupon_buy_complete_desc), mBankName, mBankAccount, mBankOwner, mUserName), getResources().getString(R.string.common_close));
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(BuyCouponActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });

        mCouponListApi = new CouponListApi(this, new ApiBase.ApiCallBack<CouponListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(BuyCouponActivity.this);
            }

            @Override
            public void onSuccess(int request_code, CouponListResponse couponListResponse) {
                ProgressUtil.hideProgress(BuyCouponActivity.this);
                if(couponListResponse != null) {
                    setCouponInfo(couponListResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(BuyCouponActivity.this);
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void setUIEventListener() {
        mBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });



        mBinding.snCouponType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                if(textView != null) {
                    textView.setTextSize(CommonUtils.convertDpToPixel(5.0f, getApplicationContext()));
                    textView.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.white_color));
                }
                changeSpinnerArrowColor(MineTalk.isBlackTheme ? R.color.main_text_color : R.color.white_color);
                String selCoin = mCouponAdapter.getItem(position);
                if(mMapListCoupon.containsKey(selCoin)){
                    CouponInfoModel models = mMapListCoupon.get(selCoin);

                    String couponAmount = CommonUtils.comma_won(models.getCoupon_amount());
                    String mineCashAmount = CommonUtils.comma_won(models.getCoupon_mine_cash());
                    String userLimitAmount = CommonUtils.comma_won(models.getCoupon_user_limit());
                    String recommendLimitAmount = CommonUtils.comma_won(models.getCoupon_recommend_limit());

                    mBinding.tvMineCashAmount.setText(mineCashAmount);
                    mBinding.tvPurchaseAmount.setText(couponAmount);
                    mBinding.tvUserLimitAmount.setText(userLimitAmount);
                    mBinding.tvRecommendLimitAmount.setText(recommendLimitAmount);

                    curSelMiningCouponIdx = models.getMining_coupon_idx();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.snPayType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                if(textView != null) {
                    textView.setTextSize(CommonUtils.convertDpToPixel(5.0f, getApplicationContext()));
                    textView.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.white_color));
                }
                changeSpinnerArrowColor(MineTalk.isBlackTheme ? R.color.main_text_color : R.color.white_color);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //구매 목록
        mBinding.btnPurchaseHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BuyHistoryActivity.startActivity(BuyCouponActivity.this);
            }
        });

        //구매하기 버튼
        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageAlert(getResources().getString(R.string.coupon_buy_last_warning),
                        getResources().getString(R.string.common_ok),
                        getResources().getString(R.string.common_cancel),
                        new PopupListenerFactory.SimplePopupListener() {
                            @Override
                            public void onClick(DialogInterface f, int state) {
                                if (state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                                    mPurchaseMiningCouponApi.execute(curSelMiningCouponIdx);
                                }
                            }
                        });

            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void changeSpinnerArrowColor(int color){
        mBinding.snCouponType.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
        mBinding.snPayType.setSupportBackgroundTintList(ColorStateList.valueOf(getResources().getColor(color)));
    }
    private void setCouponInfo(CouponListResponse response) {
        mMapListCoupon.clear();
        mMapUserListToken.clear();

        ArrayList<String> couponList = new ArrayList<>();
        int nCnt = 0;
        for(CouponInfoModel models :  response.getCouponInfo_list() ) {
            couponList.add(models.getCoupon_name());
            mMapListCoupon.put(models.getCoupon_name(), models);
            if(nCnt == 0){
                String couponAmount = CommonUtils.comma_won(models.getCoupon_amount());
                String mineCashAmount = CommonUtils.comma_won(models.getCoupon_mine_cash());
                String userLimitAmount = CommonUtils.comma_won(models.getCoupon_user_limit());
                String recommendLimitAmount = CommonUtils.comma_won(models.getCoupon_recommend_limit());

                mBinding.tvMineCashAmount.setText(mineCashAmount);
                mBinding.tvPurchaseAmount.setText(couponAmount);
                mBinding.tvUserLimitAmount.setText(userLimitAmount);
                mBinding.tvRecommendLimitAmount.setText(recommendLimitAmount);

                curSelMiningCouponIdx = models.getMining_coupon_idx();
            }
            nCnt++;
        }

        mCouponAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, couponList);
        mCouponAdapter.setDropDownViewResource(R.layout.spinner_text_color);
        mBinding.snCouponType.setAdapter(mCouponAdapter);

        ArrayList<String> payList = new ArrayList<>();
        payList.add(getResources().getString(R.string.coupon_pay_cash_title));
        mPayAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, payList);
        mPayAdapter.setDropDownViewResource(R.layout.spinner_text_color);
        mBinding.snPayType.setAdapter(mPayAdapter);

        mBinding.tvBankName.setText(response.getBank_name());
        mBinding.tvBankNum.setText(response.getBank_account());
        mBinding.tvCompanyName.setText(response.getBank_owner());

        mBankName = response.getBank_name();
        mBankAccount = response.getBank_account();
        mBankOwner = response.getBank_owner();
    }

    @Override
    public void updateTheme() {
        super.updateTheme();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutSpinnerRoot.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.layoutSpinnerPayRoot.setBackgroundResource(R.drawable.button_bg_cdb60c);


            mBinding.tvProductInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvPayWayInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));

            mBinding.btnPurchaseHistory.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnPurchaseHistory.setBackgroundResource(R.drawable.button_bg_cdb60c);

            mBinding.tvSelCouponTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvPurchaseAmountTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvPurchaseAmount.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvBankName.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBankNum.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvCompanyName.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvInputUserTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvInputUserName.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvUserLimitAmount.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvRecommendLimitAmount.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.tvProductCategoryTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvPayWayTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBankInfoTitle.setTextColor(getResources().getColor(R.color.white_color));

            //구매하기 설명
            mBinding.tvBuyGuideTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvBuyInfo1Title.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBuySubInfo1Title.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBuyInfo2Title.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBuySubInfo2Title.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBuyInfo3Title.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBuySubInfo3Title.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBuyInfo4Title.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvBuySubInfo4Title.setTextColor(getResources().getColor(R.color.white_color));

            //하단 구매하기 버튼
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnConfirm.setBackgroundColor(getResources().getColor(R.color.bk_point_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutSpinnerRoot.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.layoutSpinnerPayRoot.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.tvProductInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvPayWayInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.btnPurchaseHistory.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnPurchaseHistory.setBackgroundResource(R.drawable.button_bg_16c066);

            mBinding.tvSelCouponTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvPurchaseAmountTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvPurchaseAmount.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvBankName.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBankNum.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvCompanyName.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvInputUserTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvInputUserName.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvUserLimitAmount.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvRecommendLimitAmount.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvProductCategoryTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvPayWayTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBankInfoTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            //구매하기 설명
            mBinding.tvBuyGuideTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBuyInfo1Title.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBuySubInfo1Title.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBuyInfo2Title.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBuySubInfo2Title.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBuyInfo3Title.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBuySubInfo3Title.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBuyInfo4Title.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvBuySubInfo4Title.setTextColor(getResources().getColor(R.color.main_text_color));

            //하단 구매하기 버튼
            mBinding.btnConfirm.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnConfirm.setBackgroundColor(getResources().getColor(R.color.app_point_color));
        }
    }
}
