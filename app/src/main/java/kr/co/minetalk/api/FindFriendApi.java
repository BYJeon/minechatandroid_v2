package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.FriendConfigModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.FriendSearchResponse;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class FindFriendApi extends ApiBase<FriendSearchResponse> {

    public FindFriendApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String search_text) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);

        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("search_text", search_text);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.findFriend(mReqeustBody);
    }

    @Override
    protected FriendSearchResponse parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    FriendSearchResponse friendSearchResponse = new FriendSearchResponse();
                    if(!responseObject.isNull("data")) {
                        friendSearchResponse = FriendSearchResponse.parse(responseObject.getJSONObject("data"));
                    }
                    if(!responseObject.isNull("code")) {
                        friendSearchResponse.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        friendSearchResponse.setMessage(responseObject.getString("message"));
                    }
                    return friendSearchResponse;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
