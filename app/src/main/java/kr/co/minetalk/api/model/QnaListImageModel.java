package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class QnaListImageModel extends BaseModel implements Serializable {

    public static QnaListImageModel parse(JSONObject jsonObject) throws JSONException {
        QnaListImageModel item = new QnaListImageModel();

        if(!jsonObject.isNull("url")) {
            item.setImg_url(jsonObject.getString("url"));
        }

        if(!jsonObject.isNull("file_name")) {
            item.setImg_name(jsonObject.getString("file_name"));
        }

        if(!jsonObject.isNull("file_type")) {
            item.setImg_file_type(jsonObject.getString("file_type"));
        }

        if(!jsonObject.isNull("file_ext")) {
            item.setImg_ext(jsonObject.getString("file_ext"));
        }

        if(!jsonObject.isNull("file_size")) {
            item.setImg_size(jsonObject.getString("file_size"));
        }

        if(!jsonObject.isNull("image_type")) {
            item.setImg_type(jsonObject.getString("image_type"));
        }


        return item;
    }

    private String img_url = "";
    private String img_name = "";
    private String img_file_type   = "";
    private String img_ext = "";
    private String img_size = "";
    private String img_type = "";


    public String getImg_url() {
        return img_url;
    }
    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getImg_name() {
        return img_name;
    }
    public void setImg_name(String img_name) {
        this.img_name = img_name;
    }

    public String getImg_file_type() {
        return img_file_type;
    }
    public void setImg_file_type(String img_file_type) {
        this.img_file_type = img_file_type;
    }

    public String getImg_ext() {
        return img_ext;
    }
    public void setImg_ext(String img_ext) {
        this.img_ext = img_ext;
    }

    public String getImg_size() {
        return img_size;
    }
    public void setImg_size(String img_size) {
        this.img_size = img_size;
    }

    public String getImg_type() {
        return img_type;
    }
    public void setImg_type(String img_url) {
        this.img_type = img_type;
    }

}
