package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.ResponseBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.MineTokenHistoryModel;
import kr.co.minetalk.api.model.PointInfoResponse;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class PointInfoApi extends ApiBase<PointInfoResponse> {

    public PointInfoApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute() {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.getPointInfo();
    }

    @Override
    protected PointInfoResponse parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    PointInfoResponse pointInfoResponse = new PointInfoResponse();
                    if(!responseObject.isNull("data")) {
                        //pointInfoResponse = PointInfoResponse.parse(responseObject.getJSONObject("data"));
                        JSONArray jsonArray = responseObject.getJSONArray("data");
                        //ArrayList<MineTokenHistoryModel> tokenHistoryModels = new ArrayList<>();
                        //for(int i = 0 ; i < jsonArray.length() ; i++) {
                        //    MineTokenHistoryModel tokenHistoryModel = new MineTokenHistoryModel();
                        //    tokenHistoryModel = MineTokenHistoryModel.parse(jsonArray.getJSONObject(i));
                        //    tokenHistoryModels.add(tokenHistoryModel);
                        //}
                        //item.setPoint_history(tokenHistoryModels);

                        if(jsonArray.length() > 0){
                            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                            if(!jsonObject.isNull("user_mine_cash")) {
                                pointInfoResponse.setUser_mine_cash(jsonObject.getString("user_mine_cash"));
                            }

                            if(!jsonObject.isNull("basic_mine_cash")) {
                                pointInfoResponse.setBasic_mine_cash(jsonObject.getString("basic_mine_cash"));
                            }

                            if(!jsonObject.isNull("mctk")) {
                                pointInfoResponse.setMctk(jsonObject.getString("mctk"));
                            }

                            if(!jsonObject.isNull("user_mine_token")) {
                                pointInfoResponse.setUser_mine_token(jsonObject.getString("user_mine_token"));
                            }

                            if(!jsonObject.isNull("lock_mine_token")) {
                                pointInfoResponse.setLock_mine_token(jsonObject.getString("lock_mine_token"));
                            }
                        }
                    }
                    if(!responseObject.isNull("code")) {
                        pointInfoResponse.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        pointInfoResponse.setMessage(responseObject.getString("message"));
                    }
                    return pointInfoResponse;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
