package kr.co.minetalk.api.model;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class FriendListModel extends BaseModel implements Serializable {

    public static FriendListModel parse(JSONObject jsonObject) throws JSONException {
        FriendListModel item = new FriendListModel();

        if(!jsonObject.isNull("user_xid")) {
            item.setUser_xid(jsonObject.getString("user_xid"));
        }

        if(!jsonObject.isNull("user_name")) {
            item.setUser_name(jsonObject.getString("user_name"));
        }

        if(!jsonObject.isNull("user_hp")) {
            item.setUser_hp(jsonObject.getString("user_hp"));
        }

        if(!jsonObject.isNull("user_profile_image")) {
            item.setUser_profile_image(jsonObject.getString("user_profile_image"));
        }

        if(!jsonObject.isNull("user_state_message")) {
            item.setUser_state_message(jsonObject.getString("user_state_message"));
        }

        if(!jsonObject.isNull("user_nation_code")) {
            item.setUser_nation_code(jsonObject.getString("user_nation_code"));
        }

        if(!jsonObject.isNull("is_friend")) {
            String yn = jsonObject.getString("is_friend");
            item.setIs_friend(yn.equals("Y") ? true : false);
        }

        return item;
    }

    private String user_xid = "";
    private String user_name = "";
    private String user_hp   = "";
    private String user_profile_image = "";
    private String user_state_message = "";
    private String favorite_yn = "N";
    private String user_nation_code = "";
    private boolean is_friend = false;

    private boolean is_select = false;


    public String getUser_nation_code() {
        return user_nation_code;
    }

    public void setUser_nation_code(String user_nation_code) {
        this.user_nation_code = user_nation_code;
    }

    public String getUser_xid() {
        return user_xid;
    }

    public void setUser_xid(String user_xid) {
        this.user_xid = user_xid;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_hp() {
        return user_hp;
    }

    public void setUser_hp(String user_hp) {
        this.user_hp = user_hp;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getUser_state_message() {
        return StringEscapeUtils.unescapeJava(user_state_message);
    }

    public void setUser_state_message(String user_state_message) {
        this.user_state_message = user_state_message;
    }

    public boolean isIs_friend() {
        return is_friend;
    }

    public void setIs_friend(boolean is_friend) {
        this.is_friend = is_friend;
    }

    public String getFavorite_yn() {
        return favorite_yn;
    }

    public void setFavorite_yn(String favorite_yn) {
        this.favorite_yn = favorite_yn;
    }

    public boolean isIs_select() {
        return is_select;
    }

    public void setIs_select(boolean is_select) {
        this.is_select = is_select;
    }
}
