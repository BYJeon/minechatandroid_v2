package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class CashModel extends BaseModel {

//    public static CashModel parse(JSONObject jsonObject) throws JSONException {
//        CashModel cashModel = new CashModel();
//
//        if(!jsonObject.isNull("type")) {
//            cashModel.setFaq_type(jsonObject.getString("type"));
//        }
//
//        if(!jsonObject.isNull("faq_question")) {
//            cashModel.setFaq_question(jsonObject.getString("faq_question"));
//        }
//
//        if(!jsonObject.isNull("faq_answer")) {
//            cashModel.setFaq_answer(jsonObject.getString("faq_answer"));
//        }
//
//        if(!jsonObject.isNull("reg_date")) {
//            cashModel.setReg_date(jsonObject.getString("reg_date"));
//        }
//
//        return cashModel;
//    }


    private String cash_type = "";
    private String cash_content = "";
    private String cash_amount = "";

    public String getCash_type() {
        return cash_type;
    }

    public void setCash_type(String cash_type) {
        this.cash_type = cash_type;
    }

    public String getCash_content() {
        return cash_content;
    }

    public void setCash_content(String cash_content) {
        this.cash_content = cash_content;
    }

    public String getCash_amount() {
        return cash_amount;
    }

    public void setCash_amount(String cash_amount) {
        this.cash_amount = cash_amount;
    }

}
