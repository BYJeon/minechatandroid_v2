package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.api.model.AddressListModel;
import kr.co.minetalk.api.model.TranslateModel;
import retrofit.Call;

public class RequestAddressApi extends ApiBase<AddressListModel> {
    public RequestAddressApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    /**
     * 시 정보 불러오기
     * @param type
     */
    public void execute(String type) {
        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("type", type);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    /**
     *
     * @param type si => 시 정보 조회, gu => 구 정보 조회
     * @param si 구 정보 조회 시 필수 값
     */
    public void execute(String type, String si) {
        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("type", type);
            mReqeustParams.put("si", si);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.requestAddress(mReqeustBody);
    }

    @Override
    protected AddressListModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    AddressListModel addressListModel = new AddressListModel();
                    if(!responseObject.isNull("data")) {
                        addressListModel = AddressListModel.parse(responseObject.getJSONObject("data"));
                    }
                    if(!responseObject.isNull("code")) {
                        addressListModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        addressListModel.setMessage(responseObject.getString("message"));
                    }
                    return addressListModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
