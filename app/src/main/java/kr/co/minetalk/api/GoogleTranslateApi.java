package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.TextDetectionModel;
import kr.co.minetalk.api.model.TranslateModel;
import retrofit.Call;

public class GoogleTranslateApi extends ApiBase<TranslateModel> {
    public GoogleTranslateApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String text, String source, String target) {
        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("text", text);
            mReqeustParams.put("source", source);
            mReqeustParams.put("target", target);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.googleTranslate(mReqeustBody);
    }

    @Override
    protected TranslateModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    TranslateModel translateModel = new TranslateModel();
                    if(!responseObject.isNull("data")) {
                        translateModel = TranslateModel.parse(responseObject.getJSONObject("data"));
                    }
                    if(!responseObject.isNull("code")) {
                        translateModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        translateModel.setMessage(responseObject.getString("message"));
                    }
                    return translateModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
