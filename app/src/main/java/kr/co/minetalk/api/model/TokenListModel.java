package kr.co.minetalk.api.model;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class TokenListModel extends BaseModel implements Serializable {

    public static TokenListModel parse(JSONObject jsonObject) throws JSONException {
        TokenListModel item = new TokenListModel();

        if(!jsonObject.isNull("token_id")) {
            item.setUser_token_id(jsonObject.getString("token_id"));
        }

        if(!jsonObject.isNull("name")) {
            item.setUser_token_name(jsonObject.getString("name"));
        }

        if(!jsonObject.isNull("symbol")) {
            item.setUser_token_symble(jsonObject.getString("symbol"));
        }

        if(!jsonObject.isNull("type")) {
            item.setUser_token_type(jsonObject.getString("type"));
        }

        if(!jsonObject.isNull("decimalUnit")) {
            item.setUser_token_decimalUnit(jsonObject.getString("decimalUnit"));
        }

        if(!jsonObject.isNull("protocol")) {
            item.setUser_token_protocol(jsonObject.getString("protocol"));
        }

        if(!jsonObject.isNull("coin_id")) {
            item.setUser_token_coin_id(jsonObject.getString("coin_id"));
        }

        if(!jsonObject.isNull("enabled")) {
            item.setUser_token_enabled(jsonObject.getString("enabled"));
        }

        if(!jsonObject.isNull("logo")) {
            item.setUser_token_logo(jsonObject.getString("logo"));
        }

        if(!jsonObject.isNull("userId")) {
            item.setUser_token_user_id(jsonObject.getString("userId"));
        }

        if(!jsonObject.isNull("address")) {
            item.setUser_token_address(jsonObject.getString("address"));
        }


        if(!jsonObject.isNull("userId")) {
            item.setUser_token_user_id(jsonObject.getString("userId"));
        }


        if(!jsonObject.isNull("balance")) {
            item.setUser_token_balance(jsonObject.getString("balance"));
        }


        if(!jsonObject.isNull("priceKrw")) {
            item.setUser_token_priceKrw(jsonObject.getString("priceKrw"));
        }

        if(!jsonObject.isNull("priceUsd")) {
            item.setUser_token_priceUsd(jsonObject.getString("priceUsd"));
        }

        if(!jsonObject.isNull("lockBalance")) {
            item.setUser_token_lockBalance(jsonObject.getString("lockBalance"));
        }

        if(!jsonObject.isNull("visible")) {
            item.setUser_token_visible(jsonObject.getString("visible"));
        }

        if(!jsonObject.isNull("reg_date")) {
            item.setUser_token_regDate(jsonObject.getString("reg_date"));
        }

        return item;
    }

    private String user_token_id = "";
    private String user_token_name = "";
    private String user_token_symble   = "";
    private String user_token_type   = "";
    private String user_token_decimalUnit = "";
    private String user_token_protocol = "";
    private String user_token_coin_id = "";
    private String user_token_enabled = "";
    private String user_token_logo = "";
    private String user_token_user_id = "";
    private String user_token_address = "";
    private String user_token_balance = "";
    private String user_token_priceKrw = "";
    private String user_token_priceUsd = "";
    private String user_token_lockBalance = "";
    private String user_token_visible = "";
    private String user_token_regDate = "";


    public void setUser_token_id(String user_token_id){this.user_token_id = user_token_id;}
    public String getUser_token_id(){ return user_token_id; }

    public void setUser_token_name(String user_token_name){this.user_token_name = user_token_name;}
    public String getUser_token_name(){return user_token_name;}

    public void setUser_token_symble(String user_token_symble){this.user_token_symble = user_token_symble;}
    public String getUser_token_symble(){return user_token_symble;}

    public void setUser_token_type(String user_token_type){this.user_token_type = user_token_type;}
    public String getUser_token_type(){return user_token_type;}

    public void setUser_token_decimalUnit(String user_token_decimalUnit){this.user_token_decimalUnit = user_token_decimalUnit;}
    public String getUser_token_decimalUnit(){return user_token_decimalUnit;}

    public void setUser_token_protocol(String user_token_protocol){this.user_token_protocol = user_token_protocol;}
    public String getUser_token_protocol(){return user_token_protocol;}

    public void setUser_token_coin_id(String user_token_coin_id){this.user_token_coin_id = user_token_coin_id;}
    public String getUser_token_coin_id(){return user_token_coin_id;}

    public void setUser_token_enabled(String user_token_enabled){this.user_token_enabled = user_token_enabled;}
    public Boolean getUser_token_enabled(){return user_token_enabled == "Y" ? true : false;}

    public void setUser_token_logo(String user_token_logo){this.user_token_logo = user_token_logo;}
    public String getUser_token_logo(){return user_token_logo;}

    public void setUser_token_user_id(String user_token_user_id){this.user_token_user_id = user_token_user_id;}
    public String getUser_token_user_id(){return user_token_user_id;}

    public void setUser_token_address(String user_token_address){this.user_token_address = user_token_address;}
    public String getUser_token_address(){return user_token_address;}

    public void setUser_token_balance(String user_token_balance){this.user_token_balance = user_token_balance;}
    public String getUser_token_balance(){return user_token_balance;}

    public void setUser_token_priceKrw(String user_token_priceKrw){this.user_token_priceKrw = user_token_priceKrw;}
    public String getUser_token_priceKrw(){return user_token_priceKrw;}

    public void setUser_token_priceUsd(String user_token_priceUsd){this.user_token_priceUsd = user_token_priceUsd;}
    public String getUser_token_priceUsd(){return user_token_priceUsd;}

    public void setUser_token_lockBalance(String user_token_lockBalance){this.user_token_lockBalance = user_token_lockBalance;}
    public String getUser_token_lockBalance(){return user_token_lockBalance;}

    public void setUser_token_visible(String visible){this.user_token_visible = user_token_visible;}
    public Boolean getUser_token_visible(){return user_token_visible == "Y" ? true : false;}

    public void setUser_token_regDate(String user_token_regDate){this.user_token_regDate = user_token_regDate;}
    public String getUser_token_regDate(){ return user_token_regDate; }
}
