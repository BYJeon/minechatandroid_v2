package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TermsResponse extends BaseModel {

    public static TermsResponse parse(JSONObject jsonObject) throws JSONException {
        TermsResponse item = new TermsResponse();

        if(!jsonObject.isNull("data")) {
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            ArrayList<TermsModel> termsModels = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                TermsModel termsModel = new TermsModel();
                termsModel = TermsModel.parse(jsonArray.getJSONObject(i));

                termsModels.add(termsModel);
            }
            item.setData(termsModels);
        }

        return item;
    }


    private ArrayList<TermsModel> data = new ArrayList<>();

    public ArrayList<TermsModel> getData() {
        return data;
    }

    public void setData(ArrayList<TermsModel> data) {
        this.data = data;
    }
}
