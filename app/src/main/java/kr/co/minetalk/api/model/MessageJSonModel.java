package kr.co.minetalk.api.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MessageJSonModel extends BaseModel {

    public static MessageJSonModel parse(JSONObject jsonObject) throws JSONException {
        MessageJSonModel item = new MessageJSonModel();

        Log.e("@@@TAG","messageJson : " + jsonObject.toString());

        if(!jsonObject.isNull("text")) {
            item.setText(jsonObject.getString("text"));
        }
        if(!jsonObject.isNull("langCode")) {
            item.setLanguageCode(jsonObject.getString("langCode"));
        }
        if(!jsonObject.isNull("msgType")) {
            item.setMsgType(jsonObject.getString("msgType"));
        }
        if(!jsonObject.isNull("extra")) {
            item.setExtra(jsonObject.getString("extra"));
        }
        if(!jsonObject.isNull("fileUrl")) {
            item.setFileUrl(jsonObject.getString("fileUrl"));
        }
        if(!jsonObject.isNull("contactName")) {
            item.setContactName(jsonObject.getString("contactName"));
        }
        if(!jsonObject.isNull("contactHp")) {
            item.setContactHp(jsonObject.getString("contactHp"));
        }

        if(!jsonObject.isNull("width")) {
            item.setWidth(jsonObject.getString("width"));
        }
        if(!jsonObject.isNull("height")) {
            item.setHeight(jsonObject.getString("height"));
        }

        if(!jsonObject.isNull("senderName")) {
            item.setSenderName(jsonObject.getString("senderName"));
        }

        if(!jsonObject.isNull("senderHp")) {
            item.setSenderHp(jsonObject.getString("senderHp"));
        }

        if(!jsonObject.isNull("senderProfileUrl")) {
            item.setSenderProfileUrl(jsonObject.getString("senderProfileUrl"));
        }

        if(!jsonObject.isNull("userInfo")) {
            ArrayList<InviteUserModel> userModels = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray("userInfo");
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                InviteUserModel inviteUserModel = new InviteUserModel();
                inviteUserModel = InviteUserModel.parse(jsonArray.getJSONObject(i));
                userModels.add(inviteUserModel);
            }
            item.setInviteUserInfo(userModels);
        }

        if(!jsonObject.isNull("deleteYn")) {
            item.setDeleteYn(jsonObject.getString("deleteYn"));
        }

        return item;
    }

    private String text = "";
    private String languageCode = "";
    private String msgType = "";
    private String extra = "";
    private String fileUrl = "";
    private String contactName = "";
    private String contactHp = "";
    private String senderName = "";
    private String senderHp = "";
    private String width = "";
    private String height = "";
    private String senderProfileUrl = "";
    private String deleteYn = "";

    public String getSenderProfileUrl() {
        return senderProfileUrl;
    }

    public void setSenderProfileUrl(String senderProfileUrl) {
        this.senderProfileUrl = senderProfileUrl;
    }

    private ArrayList<InviteUserModel> inviteUserInfo = new ArrayList<>();

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactHp() {
        return contactHp;
    }

    public void setContactHp(String contactHp) {
        this.contactHp = contactHp;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderHp() {
        return senderHp;
    }

    public void setSenderHp(String senderHp) {
        this.senderHp = senderHp;
    }

    public ArrayList<InviteUserModel> getInviteUserInfo() {
        return inviteUserInfo;
    }

    public void setInviteUserInfo(ArrayList<InviteUserModel> inviteUserInfo) {
        this.inviteUserInfo = inviteUserInfo;
    }

    public String getDeleteYn() {
        return deleteYn;
    }

    public void setDeleteYn(String deleteYn) {
        this.deleteYn = deleteYn;
    }
}
