package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.ApiBase;
import kr.co.minetalk.api.ApiInterface;
import kr.co.minetalk.api.model.UpdateResponse;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class ListUpdateApi extends ApiBase<UpdateResponse> {

    public ListUpdateApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute() {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.listUpdate();
    }

    @Override
    protected UpdateResponse parseResult(Context context, int request_code, String response_str) throws JSONException {

        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    UpdateResponse updateResponse = new UpdateResponse();
                    if(!responseObject.isNull("data")) {
                        updateResponse = UpdateResponse.parse(responseObject);
                    }
                    if(!responseObject.isNull("code")) {
                        updateResponse.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        updateResponse.setMessage(responseObject.getString("message"));
                    }
                    return updateResponse;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }
}
