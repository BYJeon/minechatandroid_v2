package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ChargeAmountModel extends BaseModel {


    public static ChargeAmountModel parse(JSONObject jsonObject) throws JSONException {
        ChargeAmountModel item = new ChargeAmountModel();

        if(!jsonObject.isNull("text")) {
            item.setText(jsonObject.getString("text"));
        }

        if(!jsonObject.isNull("amount")) {
            item.setAmount(jsonObject.getString("amount"));
        }

        return  item;
    }

    private String text = "";
    private String amount = "";

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
