package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class CouponInfoModel extends BaseModel {

    public static CouponInfoModel parse(JSONObject jsonObject) throws JSONException {
        CouponInfoModel faqModel = new CouponInfoModel();

        if(!jsonObject.isNull("coupon_name")) {
            faqModel.setCoupon_name(jsonObject.getString("coupon_name"));
        }

        if(!jsonObject.isNull("coupon_amount")) {
            faqModel.setCoupon_Amount(jsonObject.getString("coupon_amount"));
        }

        if(!jsonObject.isNull("coupon_mine_cash")) {
            faqModel.setCoupon_mine_cash(jsonObject.getString("coupon_mine_cash"));
        }

        if(!jsonObject.isNull("coupon_user_limit")) {
            faqModel.setCoupon_user_limit(jsonObject.getString("coupon_user_limit"));
        }

        if(!jsonObject.isNull("coupon_recommend_limit")) {
            faqModel.setCoupon_recommend_limit(jsonObject.getString("coupon_recommend_limit"));
        }

        if(!jsonObject.isNull("mining_coupon_idx")) {
            faqModel.setMining_coupon_idx(jsonObject.getString("mining_coupon_idx"));
        }

        return faqModel;
    }



    private String coupon_name = "";
    private String coupon_amount = "";
    private String coupon_mine_cash = "";
    private String coupon_user_limit = "";
    private String coupon_recommend_limit = "";
    private String mining_coupon_idx = "";

    public String getMining_coupon_idx(){return mining_coupon_idx;}
    public void setMining_coupon_idx(String mining_coupon_idx){this.mining_coupon_idx = mining_coupon_idx;}

    public String getCoupon_name() {
        return coupon_name;
    }
    public void setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
    }

    public String getCoupon_amount(){return coupon_amount;}
    public void setCoupon_Amount(String coupon_amount){ this.coupon_amount = coupon_amount;}

    public String getCoupon_mine_cash() {
        return coupon_mine_cash;
    }

    public void setCoupon_mine_cash(String coupon_mine_cash) {
        this.coupon_mine_cash = coupon_mine_cash;
    }

    public String getCoupon_user_limit() {
        return coupon_user_limit;
    }

    public void setCoupon_user_limit(String coupon_user_limit) {
        this.coupon_user_limit = coupon_user_limit;
    }

    public String getCoupon_recommend_limit() {
        return coupon_recommend_limit;
    }

    public void setCoupon_recommend_limit(String coupon_recommend_limit) {
        this.coupon_recommend_limit = coupon_recommend_limit;
    }
}
