package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class MiningInfoResponse extends BaseModel {


    public static MiningInfoResponse parse(JSONObject jsonObject) throws JSONException {
        MiningInfoResponse item = new MiningInfoResponse();

        if (!jsonObject.isNull("accumulate_amount")) {
            item.setAccumulate_amount(jsonObject.getString("accumulate_amount"));
        }

        if (!jsonObject.isNull("limit_amount")) {
            item.setLimit_amount(jsonObject.getString("limit_amount"));
        }

        return item;
    }


    private String accumulate_amount = "";
    private String limit_amount = "";


    public String getAccumulate_amount() {
        return accumulate_amount;
    }

    public void setAccumulate_amount(String accumulate_amount) {
        this.accumulate_amount = accumulate_amount;
    }

    public String getLimit_amount(){ return this.limit_amount; }
    public void setLimit_amount(String limit_amount){
        this.limit_amount = limit_amount;
    }
}
