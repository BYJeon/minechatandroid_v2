package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class SnsCheckModel extends BaseModel {

    public static SnsCheckModel parse(JSONObject jsonObject) throws JSONException {
        SnsCheckModel item = new SnsCheckModel();

        if(!jsonObject.isNull("regist_yn")) {
            item.setRegist_yn(jsonObject.getString("regist_yn"));
        }

        return item;
    }

    private String regist_yn = "";

    public String getRegist_yn() {
        return regist_yn;
    }

    public void setRegist_yn(String regist_yn) {
        this.regist_yn = regist_yn;
    }
}
