package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class EventModel extends BaseModel {

    public static EventModel parse(JSONObject jsonObject) throws JSONException {
        EventModel item = new EventModel();

        if(!jsonObject.isNull("event_idx")) {
            item.setEvent_idx(jsonObject.getString("event_idx"));
        }

        if(!jsonObject.isNull("event_title")) {
            item.setEvent_title(jsonObject.getString("event_title"));
        }

        if(!jsonObject.isNull("event_contents")) {
            item.setEvent_contents(jsonObject.getString("event_contents"));
        }

        if(!jsonObject.isNull("reg_date")) {
            item.setReg_date(jsonObject.getString("reg_date"));
        }

        return item;
    }

    private String event_idx = "";
    private String event_title = "";
    private String event_contents = "";
    private String reg_date = "";


    public String getEvent_idx() {
        return event_idx;
    }

    public void setEvent_idx(String event_idx) {
        this.event_idx = event_idx;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public String getEvent_contents() {
        return event_contents;
    }

    public void setEvent_contents(String event_contents) {
        this.event_contents = event_contents;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }
}
