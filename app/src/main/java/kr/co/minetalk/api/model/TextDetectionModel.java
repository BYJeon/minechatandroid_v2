package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class TextDetectionModel extends BaseModel {

    public static TextDetectionModel parse(JSONObject jsonObject) throws JSONException {
        TextDetectionModel item = new TextDetectionModel();

        if(!jsonObject.isNull("locale")) {
            item.setLocale(jsonObject.getString("locale"));
        }

        if(!jsonObject.isNull("description")) {
            item.setDescription(jsonObject.getString("description"));
        }

        return item;
    }

    private String locale      = "";
    private String description = "";

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
