package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserLimitListResponse extends BaseModel {

    public static UserLimitListResponse parse(JSONObject jsonObject) throws JSONException {
        UserLimitListResponse item = new UserLimitListResponse();

        if(!jsonObject.isNull("mining_history_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("mining_history_list");
            ArrayList<MiningModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                MiningModel miningModel = new MiningModel();
                miningModel = MiningModel.parse(jsonArray.getJSONObject(i));
                dataList.add(miningModel);
            }
            item.setMining_list(dataList);
        }

        if(!jsonObject.isNull("limit_amount")) {
            item.setLimit_amount(jsonObject.getString("limit_amount"));
        }

        if(!jsonObject.isNull("accumulate_amount")) {
            item.setAccumulate_amount(jsonObject.getString("accumulate_amount"));
        }

        return item;
    }

    private String limit_amount = "";
    private String accumulate_amount = "";


    public String getLimit_amount(){return this.limit_amount;}
    public void setLimit_amount(String limit_amount){this.limit_amount = limit_amount;}

    public String getAccumulate_amount(){return accumulate_amount;}
    public void setAccumulate_amount(String accumulate_amount){this.accumulate_amount = accumulate_amount;}

    private ArrayList<MiningModel> mining_list = new ArrayList<>();
    public ArrayList<MiningModel> getMiningList() {
        return mining_list;
    }

    public void setMining_list(ArrayList<MiningModel> mining_list) {
        this.mining_list = mining_list;
    }


}
