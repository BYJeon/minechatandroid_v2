package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ExchangeInfoModel extends BaseModel {

    public static ExchangeInfoModel parse(JSONObject jsonObject) throws JSONException {
        ExchangeInfoModel item = new ExchangeInfoModel();

        if(!jsonObject.isNull("user_mine_token")) {
            item.setUser_mine_token(jsonObject.getString("user_mine_token"));
        }

        if(!jsonObject.isNull("exchange_mine_token_day_limit")) {
            item.setExchange_mine_token_day_limit(jsonObject.getString("exchange_mine_token_day_limit"));
        }

        if(!jsonObject.isNull("exchange_mine_token_min_request")) {
            item.setExchange_mine_token_min_request(jsonObject.getString("exchange_mine_token_min_request"));
        }

        if(!jsonObject.isNull("use_flag")) {
            item.setUse_flag(jsonObject.getString("use_flag"));
        }

        if(!jsonObject.isNull("alert_message")) {
            item.setAlert_message(jsonObject.getString("alert_message"));
        }

        if(!jsonObject.isNull("user_wallet_addr")) {
            item.setUser_wallet_addr(jsonObject.getString("user_wallet_addr"));
        }

        return item;
    }

    private String user_mine_token = "";
    private String exchange_mine_token_day_limit = "";
    private String exchange_mine_token_min_request = "";
    private String use_flag = "";
    private String alert_message = "";
    private String user_wallet_addr = "";

    public String getUser_mine_token() {
        return user_mine_token;
    }

    public void setUser_mine_token(String user_mine_token) {
        this.user_mine_token = user_mine_token;
    }

    public String getExchange_mine_token_day_limit() {
        return exchange_mine_token_day_limit;
    }

    public void setExchange_mine_token_day_limit(String exchange_mine_token_day_limit) {
        this.exchange_mine_token_day_limit = exchange_mine_token_day_limit;
    }

    public String getExchange_mine_token_min_request() {
        return exchange_mine_token_min_request;
    }

    public void setExchange_mine_token_min_request(String exchange_mine_token_min_request) {
        this.exchange_mine_token_min_request = exchange_mine_token_min_request;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getAlert_message() {
        return alert_message;
    }

    public void setAlert_message(String alert_message) {
        this.alert_message = alert_message;
    }

    public String getUser_wallet_addr() {
        return user_wallet_addr;
    }

    public void setUser_wallet_addr(String user_wallet_addr) {
        this.user_wallet_addr = user_wallet_addr;
    }
}
