package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NoticeResponse extends BaseModel {

    public static NoticeResponse parse(JSONObject jsonObject) throws JSONException {
        NoticeResponse response = new NoticeResponse();

        JSONArray jsonArray = jsonObject.getJSONArray("data");
        ArrayList<NoticeModel> noticeModels = new ArrayList<>();
        for(int i = 0 ; i < jsonArray.length() ; i++) {
            NoticeModel noticeModel = new NoticeModel();
            noticeModel = NoticeModel.parse(jsonArray.getJSONObject(i));
            noticeModels.add(noticeModel);

        }
        response.setData(noticeModels);

        return response;
    }

    private ArrayList<NoticeModel> data = new ArrayList<>();

    public ArrayList<NoticeModel> getData() {
        return data;
    }

    public void setData(ArrayList<NoticeModel> data) {
        this.data = data;
    }
}
