package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class RecommendCheckModel extends BaseModel {

    public static RecommendCheckModel parse(JSONObject jsonObject) throws JSONException {
        RecommendCheckModel item = new RecommendCheckModel();
        if(!jsonObject.isNull("user_login_id")) {
            item.setUser_login_id(jsonObject.getString("user_login_id"));
        }

        if(!jsonObject.isNull("user_nation_code")) {
            item.setUser_nation_code(jsonObject.getString("user_nation_code"));
        }

        if(!jsonObject.isNull("user_hp")) {
            item.setUser_hp(jsonObject.getString("user_hp"));
        }

        if(!jsonObject.isNull("user_name")) {
            item.setUser_name(jsonObject.getString("user_name"));
        }

        return item;
    }
    private String user_login_id = "";
    private String user_nation_code   = "";
    private String user_hp  = "";
    private String user_name  = "";

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
    public String getUser_name(){return user_name;}

    public void setUser_login_id(String user_login_id) {
        this.user_login_id = user_login_id;
    }

    public String getUser_login_id(){return user_login_id;}

    public void setUser_nation_code(String user_nation_code){
        this.user_nation_code = user_nation_code;
    }

    public String getUser_nation_code(){return user_nation_code;}

    public void setUser_hp(String user_hp){
        this.user_hp = user_hp;
    }

    public String getUser_hp(){return user_hp;}
}
