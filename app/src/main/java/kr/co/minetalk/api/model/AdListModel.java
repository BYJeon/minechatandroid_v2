package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class AdListModel extends BaseModel {

    public static AdListModel parse(JSONObject jsonObject) throws JSONException {
        AdListModel item = new AdListModel();

        if(!jsonObject.isNull("ad_idx")) {
            item.setAd_idx(jsonObject.getString("ad_idx"));
        }

        if(!jsonObject.isNull("user_xid")) {
            item.setUser_xid(jsonObject.getString("user_xid"));
        }

        if(!jsonObject.isNull("user_name")) {
            item.setUser_name(jsonObject.getString("user_name"));
        }

        if(!jsonObject.isNull("user_profile_image")) {
            item.setUser_profile_image(jsonObject.getString("user_profile_image"));
        }

        if(!jsonObject.isNull("ad_title")) {
            item.setAd_title(jsonObject.getString("ad_title"));
        }

        if(!jsonObject.isNull("ad_send_date")) {
            item.setAd_send_date(jsonObject.getString("ad_send_date"));
        }

        if(!jsonObject.isNull("ad_end_date")) {
            item.setAd_end_date(jsonObject.getString("ad_end_date"));
        }

        if(!jsonObject.isNull("ad_contents")) {
            item.setAd_contents(jsonObject.getString("ad_contents"));
        }

        if(!jsonObject.isNull("ad_send_count")) {
            item.setAd_send_count(jsonObject.getString("ad_send_count"));
        }

        if(!jsonObject.isNull("receive_count")) {
            item.setReceive_count(jsonObject.getString("receive_count"));
        }

        if(!jsonObject.isNull("receive_yn")) {
            item.setReceive_yn(jsonObject.getString("receive_yn"));
        }

        if(!jsonObject.isNull("ad_token_amount")) {
            item.setAd_token_amount(jsonObject.getString("ad_token_amount"));
        }
        return item;
    }

    private String ad_idx = "";
    private String user_xid = "";
    private String user_name = "";
    private String user_profile_image = "";
    private String ad_title = "";
    private String ad_send_date = "";
    private String ad_end_date = "";
    private String ad_contents = "";
    private String ad_send_count = "";
    private String receive_count = "";
    private String receive_yn = "";
    private String ad_token_amount = "";

    public String getAd_idx() {
        return ad_idx;
    }

    public void setAd_idx(String ad_idx) {
        this.ad_idx = ad_idx;
    }

    public String getUser_xid() {
        return user_xid;
    }

    public void setUser_xid(String user_xid) {
        this.user_xid = user_xid;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getAd_title() {
        return ad_title;
    }

    public void setAd_title(String ad_title) {
        this.ad_title = ad_title;
    }

    public String getAd_send_date() {
        return ad_send_date;
    }

    public void setAd_send_date(String ad_send_date) {
        this.ad_send_date = ad_send_date;
    }

    public String getAd_end_date() {
        return ad_end_date;
    }

    public void setAd_end_date(String ad_end_date) {
        this.ad_end_date = ad_end_date;
    }

    public String getAd_contents() {
        return ad_contents;
    }

    public void setAd_contents(String ad_contents) {
        this.ad_contents = ad_contents;
    }

    public String getAd_send_count() {
        return ad_send_count;
    }

    public void setAd_send_count(String ad_send_count) {
        this.ad_send_count = ad_send_count;
    }

    public String getReceive_count() {
        return receive_count;
    }

    public void setReceive_count(String receive_count) {
        this.receive_count = receive_count;
    }

    public String getReceive_yn() {
        return receive_yn;
    }

    public void setReceive_yn(String receive_yn) {
        this.receive_yn = receive_yn;
    }

    public String getAd_token_amount() {
        return ad_token_amount;
    }

    public void setAd_token_amount(String ad_token_amount) {
        this.ad_token_amount = ad_token_amount;
    }
}
