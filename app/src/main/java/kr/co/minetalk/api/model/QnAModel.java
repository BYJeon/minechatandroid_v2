package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class QnAModel extends BaseModel {

    public static QnAModel parse(JSONObject jsonObject) throws JSONException {
        QnAModel faqModel = new QnAModel();

        if(!jsonObject.isNull("type")) {
            faqModel.setQna_type(jsonObject.getString("type"));
        }

        if(!jsonObject.isNull("qna_question")) {
            faqModel.setQna_question(jsonObject.getString("qna_question"));
        }

        if(!jsonObject.isNull("qna_answer")) {
            faqModel.setQna_answer(jsonObject.getString("qna_answer"));
        }

        if(!jsonObject.isNull("reg_date")) {
            faqModel.setReg_date(jsonObject.getString("reg_date"));
        }

        if(!jsonObject.isNull("qna_answer_date")) {
            faqModel.setAnswer_reg_date(jsonObject.getString("qna_answer_date"));
        }

        return faqModel;
    }

    private String qna_type = "";
    private String qna_question = "";
    private String qna_answer = "";
    private String reg_date = "";
    private String answer_reg_date = "";

    public String getQna_type() {
        return qna_type;
    }

    public void setQna_type(String qna_type) {
        this.qna_type = qna_type;
    }

    public String getQna_question() {
        return qna_question;
    }

    public void setQna_question(String qna_question) {
        this.qna_question = qna_question;
    }

    public String getQna_answer() {
        return qna_answer;
    }

    public void setQna_answer(String qna_answer) {
        this.qna_answer = qna_answer;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getAnswer_reg_date() {
        return answer_reg_date;
    }

    public void setAnswer_reg_date(String reg_date) {
        this.answer_reg_date = reg_date;
    }
}
