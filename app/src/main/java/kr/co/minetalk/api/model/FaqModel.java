package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class FaqModel extends BaseModel {

    public static FaqModel parse(JSONObject jsonObject) throws JSONException {
        FaqModel faqModel = new FaqModel();

        if(!jsonObject.isNull("type")) {
            faqModel.setFaq_type(jsonObject.getString("type"));
        }

        if(!jsonObject.isNull("faq_question")) {
            faqModel.setFaq_question(jsonObject.getString("faq_question"));
        }

        if(!jsonObject.isNull("faq_answer")) {
            faqModel.setFaq_answer(jsonObject.getString("faq_answer"));
        }

        if(!jsonObject.isNull("reg_date")) {
            faqModel.setReg_date(jsonObject.getString("reg_date"));
        }

        return faqModel;
    }


    private String faq_type = "";
    private String faq_question = "";
    private String faq_answer = "";
    private String reg_date = "";

    public String getFaq_type() {
        return faq_type;
    }

    public void setFaq_type(String faq_type) {
        this.faq_type = faq_type;
    }

    public String getFaq_question() {
        return faq_question;
    }

    public void setFaq_question(String faq_question) {
        this.faq_question = faq_question;
    }

    public String getFaq_answer() {
        return faq_answer;
    }

    public void setFaq_answer(String faq_answer) {
        this.faq_answer = faq_answer;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }
}
