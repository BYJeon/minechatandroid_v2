package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class MemberShipHistoryModel extends BaseModel {

    public static MemberShipHistoryModel parse(JSONObject jsonObject) throws JSONException {
        MemberShipHistoryModel item = new MemberShipHistoryModel();

        if(!jsonObject.isNull("reg_date")) {
            item.setReg_date(jsonObject.getString("reg_date"));
        }

        if(!jsonObject.isNull("point_text")) {
            item.setPoint_text(jsonObject.getString("point_text"));
        }

        if(!jsonObject.isNull("point_amount")) {
            item.setPoint_amount(jsonObject.getString("point_amount"));
        }

        if(!jsonObject.isNull("status_text")) {
            item.setStatus_text(jsonObject.getString("status_text"));
        }

        if(!jsonObject.isNull("upd_date")) {
            item.setUpd_date(jsonObject.getString("upd_date"));
        }

        return item;
    }

    private String reg_date = "";
    private String point_text = "";
    private String point_amount = "";
    private String status_text = "";
    private String upd_date = "";

    public String getReg_date() { return reg_date; }
    public void setReg_date(String reg_date){this.reg_date = reg_date;}


    public String getPoint_text(){return point_text;}
    public void setPoint_text(String point_text){this.point_text = point_text;}

    public String getPoint_amount(){return point_amount;}
    public void setPoint_amount(String point_amount){this.point_amount = point_amount;}

    public String getStatus_text(){return status_text;}
    public void setStatus_text(String status_text){this.status_text = status_text;}

    public String getUpd_date(){return upd_date;}
    public void setUpd_date(String upd_date){this.upd_date = upd_date;}
}
