package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FriendSearchResponse extends BaseModel {

    public static FriendSearchResponse parse(JSONObject jsonObject) throws JSONException {
        FriendSearchResponse item = new FriendSearchResponse();

        if(!jsonObject.isNull("friend_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("friend_list");
            ArrayList<FriendListModel> arrayList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                FriendListModel friendListModel = new FriendListModel();
                friendListModel = FriendListModel.parse(jsonArray.getJSONObject(i));
                arrayList.add(friendListModel);
            }
            item.setFriend_list(arrayList);
        }
        return item;
    }

    private ArrayList<FriendListModel> friend_list = new ArrayList<>();

    public ArrayList<FriendListModel> getFriend_list() {
        return friend_list;
    }

    public void setFriend_list(ArrayList<FriendListModel> friend_list) {
        this.friend_list = friend_list;
    }
}
