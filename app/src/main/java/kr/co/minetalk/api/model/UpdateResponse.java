package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UpdateResponse extends BaseModel {

    public static UpdateResponse parse(JSONObject jsonObject) throws JSONException {
        UpdateResponse response = new UpdateResponse();

        JSONArray jsonArray = jsonObject.getJSONArray("data");
        ArrayList<UpdateModel> updateModels = new ArrayList<>();
        for(int i = 0 ; i < jsonArray.length() ; i++) {
            UpdateModel updateModel = new UpdateModel();
            updateModel = UpdateModel.parse(jsonArray.getJSONObject(i));
            updateModels.add(updateModel);

        }
        response.setData(updateModels);

        return response;
    }

    private ArrayList<UpdateModel> data = new ArrayList<>();

    public ArrayList<UpdateModel> getData() {
        return data;
    }

    public void setData(ArrayList<UpdateModel> data) {
        this.data = data;
    }
}
