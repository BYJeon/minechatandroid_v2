package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class WalletModel extends BaseModel {

    public static WalletModel parse(JSONObject jsonObject) throws JSONException {
        WalletModel walletModel = new WalletModel();

        if(!jsonObject.isNull("my_wallet_addr")) {
            walletModel.setMy_wallet_addr(jsonObject.getString("my_wallet_addr"));
        }

        if(!jsonObject.isNull("minechat_wallet_addr_yn")) {
            walletModel.setMinechat_wallet_addr_yn(jsonObject.getString("minechat_wallet_addr_yn"));
        }

        return walletModel;
    }


    private String my_wallet_addr = "";
    private String minechat_wallet_addr_yn = "";

    public String getMy_wallet_addr(){return my_wallet_addr;}
    public void setMy_wallet_addr(String my_wallet_addr){
        this.my_wallet_addr = my_wallet_addr;
    }

    public String getMinechat_wallet_addr_yn(){return minechat_wallet_addr_yn;}
    public void setMinechat_wallet_addr_yn(String minechat_wallet_addr_yn){
        this.minechat_wallet_addr_yn = minechat_wallet_addr_yn;
    }
}
