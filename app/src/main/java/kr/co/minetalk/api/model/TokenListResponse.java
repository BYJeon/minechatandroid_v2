package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TokenListResponse extends BaseModel {

    public static TokenListResponse parse(JSONObject jsonObject) throws JSONException {
        TokenListResponse item = new TokenListResponse();

        if(!jsonObject.isNull("tokens_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("tokens_list");
            ArrayList<TokenListModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                TokenListModel tokenListModel = new TokenListModel();
                tokenListModel = TokenListModel.parse(jsonArray.getJSONObject(i));
                dataList.add(tokenListModel);
            }
            item.setToken_list(dataList);
        }

        if(!jsonObject.isNull("user_tokens_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("user_tokens_list");
            ArrayList<TokenListModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                TokenListModel tokenListModel = new TokenListModel();
                tokenListModel = TokenListModel.parse(jsonArray.getJSONObject(i));
                dataList.add(tokenListModel);
            }
            item.setUser_token_list(dataList);
        }

        return item;
    }

    private ArrayList<TokenListModel> token_list = new ArrayList<>();
    private ArrayList<TokenListModel> user_token_list    = new ArrayList<>();

    public ArrayList<TokenListModel> getToken_list() {
        return token_list;
    }

    public void setToken_list(ArrayList<TokenListModel> token_list) {
        this.token_list = token_list;
    }

    public ArrayList<TokenListModel> getUser_token_list() {
        return user_token_list;
    }

    public void setUser_token_list(ArrayList<TokenListModel> user_token_list) {
        this.user_token_list = user_token_list;
    }
}
