package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class AdListImageResponse extends BaseModel {

    public static AdListImageResponse parse(JSONArray jsonArray) throws JSONException {
        AdListImageResponse item = new AdListImageResponse();

        ArrayList<AdListImageModel> dataList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            AdListImageModel imageModel = new AdListImageModel();
            imageModel = AdListImageModel.parse(jsonArray.getJSONObject(i));
            dataList.add(imageModel);
        }

        item.setImage_list(dataList);

        return item;
    }

    private ArrayList<AdListImageModel> image_list = new ArrayList<>();


    public void setImage_list(ArrayList<AdListImageModel> image_list) {
        this.image_list = image_list;
    }

    public ArrayList<AdListImageModel> geImage_list() {
        return image_list;
    }

}
