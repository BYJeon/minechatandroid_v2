package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WithDrawListResponse extends BaseModel {

    public static WithDrawListResponse parse(JSONObject jsonObject) throws JSONException {
        WithDrawListResponse item = new WithDrawListResponse();

        long nTotal = 0;
        if(!jsonObject.isNull("withdraw_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("withdraw_list");
            ArrayList<PointAmountListModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                PointAmountListModel withDrawListModel = new PointAmountListModel();
                withDrawListModel = PointAmountListModel.parse(jsonArray.getJSONObject(i));
                nTotal += Long.parseLong(withDrawListModel.getPoint_amount());
                dataList.add(withDrawListModel);
            }
            item.setWithdraw_list(dataList);
            item.setWithdraw_total_point(Long.toString(nTotal));
        }

        return item;
    }

    private String withdraw_total_point = "";
    private ArrayList<PointAmountListModel> withdraw_list = new ArrayList<>();

    public void setWithdraw_total_point(String withdraw_total_point) {
        this.withdraw_total_point = withdraw_total_point;
    }
    public String getWithdraw_total_point(){return withdraw_total_point;}

    public ArrayList<PointAmountListModel> getWithdraw_list(){return withdraw_list;}

    public void setWithdraw_list(ArrayList<PointAmountListModel> withdraw_list) {
        this.withdraw_list = withdraw_list;
    }
}
