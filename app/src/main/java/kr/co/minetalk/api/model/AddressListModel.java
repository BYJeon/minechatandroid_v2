package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddressListModel extends BaseModel {

    public static AddressListModel parse(JSONObject jsonObject) throws JSONException {
        AddressListModel item = new AddressListModel();

        if(!jsonObject.isNull("addr_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("addr_list");
            ArrayList<String> addressList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                addressList.add(jsonArray.getString(i));
            }
            item.setAddr_list(addressList);
        }
        return item;
    }

    private ArrayList<String> addr_list = new ArrayList<>();

    public ArrayList<String> getAddr_list() {
        return addr_list;
    }

    public void setAddr_list(ArrayList<String> addr_list) {
        this.addr_list = addr_list;
    }
}
