package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class MineTokenHistoryModel extends BaseModel {

    public static MineTokenHistoryModel parse(JSONObject jsonObject) throws JSONException {
        MineTokenHistoryModel item = new MineTokenHistoryModel();

        if(!jsonObject.isNull("end_date")) {
            item.setEnd_date(jsonObject.getString("end_date"));
        }

        if(!jsonObject.isNull("point_amount")) {
            item.setPoint_amount(jsonObject.getString("point_amount"));
        }

        return item;
    }

    private String token_type = "유예토큰";
    private String end_date = "";
    private String point_amount = "";

    public String getToken_type(){return token_type;}
    public void setToken_type(String token_type){ this.token_type = token_type;}

    public String getPoint_amount(){return point_amount;}
    public void setPoint_amount(String point_amount){ this.point_amount = point_amount;}

    public String getEnd_date(){return end_date;}
    public void setEnd_date(String end_date){ this.end_date = end_date;}
}
