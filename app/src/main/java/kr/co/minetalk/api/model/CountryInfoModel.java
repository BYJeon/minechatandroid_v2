package kr.co.minetalk.api.model;

import kr.co.minetalk.utils.Preferences;

public class CountryInfoModel extends BaseModel {


    private String countryNameOther = "";
    private String countryName = "";
    private String code          = "";
    private String voice_code    = "";
    private String flag_image    = "";
    private boolean isSelect     = false;

    public CountryInfoModel(String countryNameOther, String countryName, String flag_image, String code, String voice_code) {
        this.countryNameOther = countryNameOther;
        this.countryName = countryName;
        this.code = code;
        this.voice_code = voice_code;
        this.flag_image = flag_image;
    }

    public String getCountryNameOther() {
        return countryNameOther;
    }

    public void setCountryNameOther(String countryNameOther) {
        this.countryNameOther = countryNameOther;
    }

    public String getCountryName() {

        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    public String getVoice_code() {
        return voice_code;
    }

    public void setVoice_code(String voice_code) {
        this.voice_code = voice_code;
    }

    public String getFlag_image() {
        return flag_image;
    }

    public void setFlag_image(String flag_image) {
        this.flag_image = flag_image;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
