package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class RegistApi extends ApiBase<UserInfoBaseModel> {

    public RegistApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String regist_type, String user_name, String user_pwd, String user_nation_code,
                        String user_hp, String auth_number, String user_recommend, String user_provider_type,
                        String user_provider_id, String user_profile_image, String user_platform) {

        String user_registration_id = Preferences.getPushToken();

        try {
            mReqeustParams = new JSONObject();

            mReqeustParams.put("regist_type", regist_type);
            mReqeustParams.put("user_name", user_name);
            mReqeustParams.put("user_pwd", user_pwd);
            mReqeustParams.put("user_nation_code", user_nation_code);

            mReqeustParams.put("user_hp", user_hp);
            mReqeustParams.put("auth_number", auth_number);
            mReqeustParams.put("user_recommend", user_recommend);
            mReqeustParams.put("user_provider_type", user_provider_type);

            mReqeustParams.put("user_provider_id", user_provider_id);
            mReqeustParams.put("user_profile_image", user_profile_image);
            mReqeustParams.put("user_platform", user_platform);
            mReqeustParams.put("user_registration_id", user_registration_id);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e("@@@TAG","reg : " + mReqeustParams.toString());
        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.regist(mReqeustBody);
    }

    @Override
    protected UserInfoBaseModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {

            Log.e("@@@TAG","res : " + response_str);
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    UserInfoBaseModel userInfoModel = new UserInfoBaseModel();
                    if(!responseObject.isNull("data")) {
                        userInfoModel = UserInfoBaseModel.parse(responseObject.getJSONObject("data"));
                    }
                    if(!responseObject.isNull("code")) {
                        userInfoModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        userInfoModel.setMessage(responseObject.getString("message"));
                    }
                    return userInfoModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
