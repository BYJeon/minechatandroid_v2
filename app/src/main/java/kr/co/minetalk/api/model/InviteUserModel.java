package kr.co.minetalk.api.model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class InviteUserModel extends BaseModel {

    public static InviteUserModel parse(JSONObject jsonObject) throws JSONException {

        InviteUserModel item = new InviteUserModel();

        if(!jsonObject.isNull("user_xid")) {
            item.setUser_xid(jsonObject.getString("user_xid"));
        }

        if(!jsonObject.isNull("user_name")) {
            item.setUser_name(jsonObject.getString("user_name"));
        }

        if(!jsonObject.isNull("user_hp")) {
            item.setUser_hp(jsonObject.getString("user_hp"));
        }

        return item;
    }

    private String user_xid  = "";
    private String user_name = "";
    private String user_hp   = "";

    public String getUser_xid() {
        return user_xid;
    }

    public void setUser_xid(String user_xid) {
        this.user_xid = user_xid;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_hp() {
        return user_hp;
    }

    public void setUser_hp(String user_hp) {
        this.user_hp = user_hp;
    }
}
