package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FriendListResponse;
import kr.co.minetalk.api.model.QnaListImageResponse;
import kr.co.minetalk.utils.Preferences;
import okhttp3.MultipartBody;
import retrofit.Call;
import retrofit.http.Multipart;


/**
 * Created by episode on 2018. 10. 7..
 */

public class UploadQnAImageApi extends ApiBase<BaseModel> {

    private ArrayList<File> mImageFile;

    public UploadQnAImageApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(Context context, ArrayList<File> imageFiles) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage();
        setHeader(authorization, x_login_type, x_lang);



        this.mImageFile = imageFiles;
        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        HashMap<String, RequestBody> parmsMap = new HashMap<>();

        for( File file : mImageFile){
            RequestBody reqeustBody = RequestBody.create(MediaType.parse("image/*"), file);
            String filename = file.getName();
            parmsMap.put("image[]\"; filename=\"" + filename, reqeustBody);
        }

        return apiInterface.uploadQnAPicture(parmsMap);
    }

    @Override
    protected QnaListImageResponse parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                JSONObject responseObject = new JSONObject(response_str);

                QnaListImageResponse qnaImgListResponse = new QnaListImageResponse();
                if(!responseObject.isNull("data")) {
                    qnaImgListResponse = QnaListImageResponse.parse(responseObject.getJSONArray("data"));
                }

                if(!responseObject.isNull("code")) {
                    qnaImgListResponse.setCode(responseObject.getString("code"));
                }

                if(!responseObject.isNull("message")) {
                    qnaImgListResponse.setMessage(responseObject.getString("message"));
                }


                return qnaImgListResponse;
            }
        }
        return null;
    }
}
