package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MineTokenHistoryResponse extends BaseModel {

    public static MineTokenHistoryResponse parse(JSONObject jsonObject) throws JSONException {
        MineTokenHistoryResponse item = new MineTokenHistoryResponse();

        if(!jsonObject.isNull("user_mine_token")) {
            item.setTotal_mine_token_point(jsonObject.getString("user_mine_token"));
        }

        if(!jsonObject.isNull("lock_token_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("lock_token_list");
            ArrayList<MineTokenHistoryModel> tokenHistoryModels = new ArrayList<>();
            int nTotalPoint = 0;
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                MineTokenHistoryModel tokenHistoryModel = new MineTokenHistoryModel();
                tokenHistoryModel = MineTokenHistoryModel.parse(jsonArray.getJSONObject(i));
                nTotalPoint += Integer.parseInt(tokenHistoryModel.getPoint_amount());
                tokenHistoryModels.add(tokenHistoryModel);
            }

            item.setTotal_mine_token_lock_point(Integer.toString(nTotalPoint));
            item.setPoint_history(tokenHistoryModels);
        }

        return item;
    }


    private String total_mine_token_point = "";
    private String total_mine_token_lock_point = "";
    private ArrayList<MineTokenHistoryModel> token_history = new ArrayList<>();

    public String getTotal_mine_token_lock_point() { return total_mine_token_lock_point; }

    public void setTotal_mine_token_lock_point(String total_lock_point) { this.total_mine_token_lock_point = total_lock_point; }

    public String getTotal_mine_token_point() { return total_mine_token_point; }

    public void setTotal_mine_token_point(String total_point) { this.total_mine_token_point = total_point; }

    public ArrayList<MineTokenHistoryModel> getToken_history() {
        return token_history;
    }

    public void setPoint_history(ArrayList<MineTokenHistoryModel> token_history) {
        this.token_history = token_history;
    }
}
