package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class NoticeModel extends BaseModel {

    public static NoticeModel parse(JSONObject jsonObject) throws JSONException {
        NoticeModel item = new NoticeModel();

        if(!jsonObject.isNull("notice_idx")) {
            item.setNotice_idx(jsonObject.getString("notice_idx"));
        }

        if(!jsonObject.isNull("notice_title")) {
            item.setNotice_title(jsonObject.getString("notice_title"));
        }

        if(!jsonObject.isNull("notice_contents")) {
            item.setNotice_contents(jsonObject.getString("notice_contents"));
        }

        if(!jsonObject.isNull("reg_date")) {
            item.setReg_date(jsonObject.getString("reg_date"));
        }

        return item;
    }

    private String notice_idx = "";
    private String notice_title = "";
    private String notice_contents = "";
    private String reg_date = "";


    public String getNotice_idx() {
        return notice_idx;
    }

    public void setNotice_idx(String notice_idx) {
        this.notice_idx = notice_idx;
    }

    public String getNotice_title() {
        return notice_title;
    }

    public void setNotice_title(String notice_title) {
        this.notice_title = notice_title;
    }

    public String getNotice_contents() {
        return notice_contents;
    }

    public void setNotice_contents(String notice_contents) {
        this.notice_contents = notice_contents;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }
}
