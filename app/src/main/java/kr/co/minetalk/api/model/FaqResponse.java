package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FaqResponse extends BaseModel {

    public static FaqResponse parse(JSONObject jsonObject) throws JSONException {
        FaqResponse item = new FaqResponse();

        if(!jsonObject.isNull("data")) {
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            ArrayList<FaqModel> faqModels = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                FaqModel faq = new FaqModel();
                faq = FaqModel.parse(jsonArray.getJSONObject(i));

                faqModels.add(faq);
            }
            item.setData(faqModels);
        }
        return item;
    }

    private ArrayList<FaqModel> data = new ArrayList<>();

    public ArrayList<FaqModel> getData() {
        return data;
    }

    public void setData(ArrayList<FaqModel> data) {
        this.data = data;
    }
}
