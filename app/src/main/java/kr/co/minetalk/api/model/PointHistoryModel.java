package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class PointHistoryModel extends BaseModel {


    public static PointHistoryModel parse(JSONObject jsonObject) throws JSONException {
        PointHistoryModel item = new PointHistoryModel();

        if(!jsonObject.isNull("user_name")) {
            item.setUser_name(jsonObject.getString("user_name"));
        }

        if(!jsonObject.isNull("action_type")) {
            item.setAction_type(jsonObject.getString("action_type"));
        }

        if(!jsonObject.isNull("action_gubun")) {
            item.setAction_gubun(jsonObject.getString("action_gubun"));
        }

        if(!jsonObject.isNull("point_amount")) {
            item.setPoint_amount(jsonObject.getString("point_amount"));
        }

        if(!jsonObject.isNull("reg_date")) {
            item.setReg_date(jsonObject.getString("reg_date"));
        }

        return item;
    }


    private String user_name = "";
    private String action_type = "";
    private String action_gubun = "";
    private String point_amount = "";
    private String reg_date = "";


    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAction_type() {
        return action_type;
    }

    public void setAction_type(String action_type) {
        this.action_type = action_type;
    }

    public String getAction_gubun() {
        return action_gubun;
    }

    public void setAction_gubun(String action_gubun) {
        this.action_gubun = action_gubun;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getPoint_amount() {
        return point_amount;
    }

    public void setPoint_amount(String point_amount) {
        this.point_amount = point_amount;
    }
}
