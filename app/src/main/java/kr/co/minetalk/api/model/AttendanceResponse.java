package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AttendanceResponse extends BaseModel {

    public static AttendanceResponse parse(JSONObject jsonObject) throws JSONException {
        AttendanceResponse item = new AttendanceResponse();

        if(!jsonObject.isNull("etc_data")) {
            item.setEtc_data(jsonObject.getString("etc_data"));
        }

        if(!jsonObject.isNull("amount")) {
            item.setAmount(jsonObject.getString("amount"));
        }

        if(!jsonObject.isNull("mining_coupon_yn")) {
            item.setCouponYn(jsonObject.getString("mining_coupon_yn"));
        }

        return item;
    }

    private String etc_data = "";
    private String amount = "";
    private String couponYn = "";

    public String getEtc_data(){return etc_data;}
    public void setEtc_data(String etc_data){this.etc_data = etc_data;}

    public String getAmount(){return amount;}
    public void setAmount(String amount){this.amount = amount;}

    public String getCouponYn(){return couponYn;};
    public void setCouponYn(String couponYn){this.couponYn = couponYn;}
}
