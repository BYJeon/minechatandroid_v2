package kr.co.minetalk.api.model;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class UserInfoBaseModel extends BaseModel {

    public static UserInfoBaseModel parse(JSONObject jsonObject) throws JSONException {
        UserInfoBaseModel item = new UserInfoBaseModel();

        if(!jsonObject.isNull("user_xid")) {
            item.setUser_xid(jsonObject.getString("user_xid"));
        }

        if(!jsonObject.isNull("user_name")) {
            item.setUser_name(jsonObject.getString("user_name"));
        }

        if(!jsonObject.isNull("user_email")) {
            item.setUser_email(jsonObject.getString("user_email"));
        }

        if(!jsonObject.isNull("user_birthday")) {
            item.setUser_birthday(jsonObject.getString("user_birthday"));
        }

        if(!jsonObject.isNull("user_provider_type")) {
            item.setUser_provider_type(jsonObject.getString("user_provider_type"));
        }

        if(!jsonObject.isNull("user_provider_id")) {
            item.setUser_provider_id(jsonObject.getString("user_provider_id"));
        }

        if(!jsonObject.isNull("user_profile_image")) {
            item.setUser_profile_image(jsonObject.getString("user_profile_image"));
        }

        if(!jsonObject.isNull("user_nation_code")) {
            item.setUser_nation_code(jsonObject.getString("user_nation_code"));
        }

        if(!jsonObject.isNull("user_hp")) {
            item.setUser_hp(jsonObject.getString("user_hp"));
        }

        if(!jsonObject.isNull("user_pwd")) {
            item.setUser_pwd(jsonObject.getString("user_pwd"));
        }

        if(!jsonObject.isNull("user_recommend")) {
            item.setUser_recommend(jsonObject.getString("user_recommend"));
        }

        if(!jsonObject.isNull("user_platform")) {
            item.setUser_platform(jsonObject.getString("user_platform"));
        }

        if(!jsonObject.isNull("user_type")) {
            item.setUser_type(jsonObject.getString("user_type"));
        }

        if(!jsonObject.isNull("user_mine_cash")) {
            item.setUser_mine_cash(jsonObject.getString("user_mine_cash"));
        }

        if(!jsonObject.isNull("user_mine_token")) {
            item.setUser_mine_token(jsonObject.getString("user_mine_token"));
        }

        if(!jsonObject.isNull("user_state_message")) {
            item.setUser_state_message(jsonObject.getString("user_state_message"));
        }

        if(!jsonObject.isNull("user_message_receive_flag")) {
            item.setUser_message_receive_flag(jsonObject.getString("user_message_receive_flag"));
        }

        if(!jsonObject.isNull("user_addr_si")) {
            item.setUser_addr_si(jsonObject.getString("user_addr_si"));
        }

        if(!jsonObject.isNull("user_addr_gu")) {
            item.setUser_addr_gu(jsonObject.getString("user_addr_gu"));
        }

        if(!jsonObject.isNull("user_addr_detail")) {
            item.setUser_addr_detail(jsonObject.getString("user_addr_detail"));
        }

        if(!jsonObject.isNull("user_job")) {
            item.setUser_job(jsonObject.getString("user_job"));
        }

        if(!jsonObject.isNull("user_gender")) {
            item.setUser_gender(jsonObject.getString("user_gender"));
        }

        if(!jsonObject.isNull("total_mine_token")) {
            item.setTotal_mine_token(jsonObject.getString("total_mine_token"));
        }

        if(!jsonObject.isNull("lock_mine_token")) {
            item.setLock_mine_token(jsonObject.getString("lock_mine_token"));
        }

        if(!jsonObject.isNull("shop_id")) {
            item.setShop_id(jsonObject.getString("shop_id"));
        }

        if(!jsonObject.isNull("recommend_hp")) {
            item.setRecommend_hp(jsonObject.getString("recommend_hp"));
        }

        if(!jsonObject.isNull("recommend_name")) {
            item.setRecommend_name(jsonObject.getString("recommend_name"));
        }

        if(!jsonObject.isNull("recommend_nation_code")) {
            item.setRecommend_nation_code(jsonObject.getString("recommend_nation_code"));
        }

        if(!jsonObject.isNull("user_hp_confirm")) {
            item.setUser_hp_confirm(jsonObject.getString("user_hp_confirm"));
        }

        if(!jsonObject.isNull("user_email_confirm")) {
            item.setUser_email_confirm(jsonObject.getString("user_email_confirm"));
        }

        if(!jsonObject.isNull("user_login_id")) {
            item.setUser_login_id(jsonObject.getString("user_login_id"));
        }

        if(!jsonObject.isNull("recommend_change_count")) {
            item.setRecommend_change_count(jsonObject.getString("recommend_change_count"));
        }

        if(!jsonObject.isNull("auth_token")) {
            item.setAuth_token(jsonObject.getString("auth_token"));
        }

        return item;
    }

    private String user_xid = "";
    private String user_name = "";
    private String user_email = "";
    private String user_birthday = "";
    private String user_provider_type = "";
    private String user_provider_id = "";
    private String user_profile_image = "";
    private String user_nation_code = "";
    private String user_hp = "";
    private String user_pwd = "";
    private String user_recommend = "";
    private String user_platform = "";
    private String user_type = "";

    private String user_addr_si = "";
    private String user_addr_gu = "";
    private String user_addr_detail = "";

    private String user_mine_cash = "";
    private String user_mine_token = "";
    private String user_state_message = "";

    private String user_gender = "";
    private String user_job = "";

    private String user_message_receive_flag = "";

    private String total_mine_token = "";
    private String lock_mine_token = "";

    private String shop_id = "";

    private String recommend_hp = "";
    private String recommend_name = "";
    private String recommend_nation_code = "";

    private String user_hp_confirm = "";
    private String user_email_confirm = "";
    private String user_login_id = "";
    private String recommend_change_count = "";

    private String auth_token = "";

    public void setAuth_token(String auth_token){this.auth_token = auth_token;}
    public String getAuth_token(){return this.auth_token;}

    public String getRecommend_change_count(){return recommend_change_count;}

    public void setRecommend_change_count(String recommend_change_count){
        this.recommend_change_count = recommend_change_count;
    }
    public String getUser_hp_confirm() {
        return user_hp_confirm;
    }

    public void setUser_hp_confirm(String user_hp_confirm) {
        this.user_hp_confirm = user_hp_confirm;
    }

    public String getUser_login_id(){return user_login_id;}
    public void setUser_login_id(String user_login_id){
        this.user_login_id = user_login_id;
    }
    public String getUser_email_confirm() {
        return user_email_confirm;
    }

    public void setUser_email_confirm(String user_email_confirm) {
        this.user_email_confirm = user_email_confirm;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getUser_xid() {
        return user_xid;
    }

    public void setUser_xid(String user_xid) {
        this.user_xid = user_xid;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_birthday() {
        return user_birthday;
    }

    public void setUser_birthday(String user_birthday) {
        this.user_birthday = user_birthday;
    }

    public String getUser_provider_type() {
        return user_provider_type;
    }

    public void setUser_provider_type(String user_provider_type) {
        this.user_provider_type = user_provider_type;
    }

    public String getUser_provider_id() {
        return user_provider_id;
    }

    public void setUser_provider_id(String user_provider_id) {
        this.user_provider_id = user_provider_id;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getUser_nation_code() {
        return user_nation_code;
    }

    public void setUser_nation_code(String user_nation_code) {
        this.user_nation_code = user_nation_code;
    }

    public String getRecommend_nation_code() {
        return recommend_nation_code;
    }

    public void setRecommend_nation_code(String recommend_nation_code) {
        this.recommend_nation_code = recommend_nation_code;
    }

    public String getUser_hp() {
        return user_hp;
    }

    public void setUser_hp(String user_hp) {
        this.user_hp = user_hp;
    }

    public String getUser_pwd() {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }

    public String getUser_recommend() {
        return user_recommend;
    }

    public void setUser_recommend(String user_recommend) {
        this.user_recommend = user_recommend;
    }

    public String getUser_platform() {
        return user_platform;
    }

    public void setUser_platform(String user_platform) {
        this.user_platform = user_platform;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getUser_mine_cash() {
        return user_mine_cash;
    }

    public void setUser_mine_cash(String user_mine_case) {
        this.user_mine_cash = user_mine_case;
    }

    public String getUser_mine_token() {
        return user_mine_token;
    }

    public void setUser_mine_token(String user_mine_token) {
        this.user_mine_token = user_mine_token;
    }

    public String getUser_state_message() {
        return StringEscapeUtils.unescapeJava(user_state_message);
    }

    public void setUser_state_message(String user_state_message) {
        this.user_state_message = user_state_message;
    }

    public String getUser_message_receive_flag() {
        return user_message_receive_flag;
    }

    public void setUser_message_receive_flag(String user_message_receive_flag) {
        this.user_message_receive_flag = user_message_receive_flag;
    }

    public String getUser_addr_si() {
        return user_addr_si;
    }

    public void setUser_addr_si(String user_addr_si) {
        this.user_addr_si = user_addr_si;
    }

    public String getUser_addr_gu() {
        return user_addr_gu;
    }

    public void setUser_addr_gu(String user_addr_gu) {
        this.user_addr_gu = user_addr_gu;
    }

    public String getUser_addr_detail() {
        return user_addr_detail;
    }

    public void setUser_addr_detail(String user_addr_detail) {
        this.user_addr_detail = user_addr_detail;
    }

    public String getUser_gender() {
        return user_gender;
    }

    public void setUser_gender(String user_gender) {
        this.user_gender = user_gender;
    }

    public String getUser_job() {
        return user_job;
    }

    public void setUser_job(String user_job) {
        this.user_job = user_job;
    }

    public String getTotal_mine_token() {
        return total_mine_token;
    }

    public void setTotal_mine_token(String total_mine_token) {
        this.total_mine_token = total_mine_token;
    }

    public String getLock_mine_token() {
        return lock_mine_token;
    }

    public void setLock_mine_token(String lock_mine_token) {
        this.lock_mine_token = lock_mine_token;
    }

    public String getRecommend_hp() {
        return recommend_hp;
    }

    public void setRecommend_hp(String recommend_hp) {
        this.recommend_hp = recommend_hp;
    }

    public String getRecommend_name() {
        return recommend_name;
    }

    public void setRecommend_name(String recommend_name) {
        this.recommend_name = recommend_name;
    }
}
