package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class FriendConfigModel extends BaseModel {

    public static FriendConfigModel parse(JSONObject jsonObject) throws JSONException {
        FriendConfigModel item = new FriendConfigModel();

        if(!jsonObject.isNull("auto_sync")) {
            item.setAuto_sync(jsonObject.getString("auto_sync"));
        }

        if(!jsonObject.isNull("sync_date")) {
            item.setSync_date(jsonObject.getString("sync_date"));
        }

        if(!jsonObject.isNull("allow_yn")) {
            item.setAllow_yn(jsonObject.getString("allow_yn"));
        }

        return item;
    }

    private String auto_sync = "";
    private String sync_date = "";
    private String allow_yn  = "";

    public String getAuto_sync() {
        return auto_sync;
    }

    public void setAuto_sync(String auto_sync) {
        this.auto_sync = auto_sync;
    }

    public String getSync_date() {
        return sync_date;
    }

    public void setSync_date(String sync_date) {
        this.sync_date = sync_date;
    }

    public String getAllow_yn() {
        return allow_yn;
    }

    public void setAllow_yn(String allow_yn) {
        this.allow_yn = allow_yn;
    }
}
