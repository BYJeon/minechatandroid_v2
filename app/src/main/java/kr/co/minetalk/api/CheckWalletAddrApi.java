package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.api.model.WalletModel;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class CheckWalletAddrApi extends ApiBase<WalletModel> {
    public CheckWalletAddrApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String user_wallet_addr) {

        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);
        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("user_wallet_addr", user_wallet_addr);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.checkWalletAddr(mReqeustBody);
    }

    @Override
    protected WalletModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    WalletModel walletModel = new WalletModel();

                    if(!responseObject.isNull("my_wallet_addr")) {
                        walletModel.setMy_wallet_addr(responseObject.getString("my_wallet_addr"));
                    }

                    if(!responseObject.isNull("minechat_wallet_addr_yn")) {
                        walletModel.setMinechat_wallet_addr_yn(responseObject.getString("minechat_wallet_addr_yn"));
                    }

                    if(!responseObject.isNull("code")) {
                        walletModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        walletModel.setMessage(responseObject.getString("message"));
                    }
                    return walletModel;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
