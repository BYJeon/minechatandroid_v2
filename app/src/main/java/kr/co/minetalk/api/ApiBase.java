package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import kr.co.minetalk.BuildConfig;
import retrofit.Retrofit;

/**
 * Created by episode on 2018. 9. 18..
 */

public abstract class ApiBase<Result> extends kr.co.idevbank.base.api.ApiBase<Result,ApiInterface> {

    protected final String MEDIA_TYPE_TEXT_PLAIN = "text/plain";
    protected JSONObject mReqeustParams;
    protected RequestBody mReqeustBody;

    public ApiBase(Activity activity, int queue_type, ApiCallBack apiCallBack) {
        super(activity, queue_type, apiCallBack);

        Context context = activity;
    }

    public ApiBase(Activity activity, int queue_type, ApiCallBack apiCallBack, int timeout) {
        super(activity, queue_type, apiCallBack, timeout);

        Context context = activity;
    }

    public ApiBase(Context context, int queue_type, ApiCallBack apiCallBack) {
        super(context, queue_type, apiCallBack);
    }

    public ApiBase(Context context, int queue_type, ApiCallBack apiCallBack, int timeout) {
        super(context, queue_type, apiCallBack, timeout);
    }

    protected ApiInterface createApiInterface(Retrofit retrofit){
        return retrofit.create(ApiInterface.class);
    }

    protected int getVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    protected String getApplicationId(){
        return BuildConfig.APPLICATION_ID;
    }


}
