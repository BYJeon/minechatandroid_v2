package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class PwInquiryModel extends BaseModel {

    public static PwInquiryModel parse(JSONObject jsonObject) throws JSONException {
        PwInquiryModel item = new PwInquiryModel();

        if(!jsonObject.isNull("temp_password")) {
            item.setTemp_password(jsonObject.getString("temp_password"));
        }

        return item;
    }

    private String temp_password = "";

    public String getTemp_password() {
        return temp_password;
    }

    public void setTemp_password(String temp_password) {
        this.temp_password = temp_password;
    }
}
