package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class PointAmountListModel extends BaseModel implements Serializable {

    public static PointAmountListModel parse(JSONObject jsonObject) throws JSONException {
        PointAmountListModel item = new PointAmountListModel();

        if(!jsonObject.isNull("reg_date")) {
            item.setPoint_reg_date(jsonObject.getString("reg_date"));
        }

        if(!jsonObject.isNull("from_address")) {
            item.setPoint_from_address(jsonObject.getString("from_address"));
        }

        if(!jsonObject.isNull("to_address")) {
            item.setPoint_to_address(jsonObject.getString("to_address"));
        }

        if(!jsonObject.isNull("amount")) {
            item.setPoint_amount(jsonObject.getString("amount"));
        }

        return item;
    }

    private String point_reg_date = "";
    private String point_from_address = "";
    private String point_to_address = "";
    private String point_amount   = "";

    public void setPoint_reg_date(String point_reg_date){this.point_reg_date = point_reg_date;}
    public String getPoint_reg_date(){ return point_reg_date; }

    public void setPoint_from_address(String point_from_address){this.point_from_address = point_from_address;}
    public String getPoint_from_address(){return point_from_address;}

    public void setPoint_to_address(String point_to_address){this.point_to_address = point_to_address;}
    public String getPoint_to_address(){return point_to_address;}

    public void setPoint_amount(String point_amount){this.point_amount = point_amount;}
    public String getPoint_amount(){return point_amount;}


}
