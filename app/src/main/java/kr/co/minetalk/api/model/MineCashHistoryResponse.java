package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MineCashHistoryResponse extends BaseModel {


    public static MineCashHistoryResponse parse(JSONObject jsonObject) throws JSONException {
        MineCashHistoryResponse item = new MineCashHistoryResponse();

        if(!jsonObject.isNull("total_mine_cash")) {
            item.setTotal_Cash_point(jsonObject.getString("total_mine_cash"));
        }

        if(!jsonObject.isNull("annual_membership_cash")) {
            item.setAnnual_membership_cash(jsonObject.getString("annual_membership_cash"));
        }

        if(!jsonObject.isNull("basic_cash")) {
            item.setBasic_cash(jsonObject.getString("basic_cash"));
        }

        return item;
    }


    private String total_cash_point = "";
    private String annual_membership_cash = "";
    private String basic_cash = "";


    public String getTotal_cash_point() { return total_cash_point; }
    public void setTotal_Cash_point(String total_point) { this.total_cash_point = total_point; }

    public String getAnnual_membership_cash() { return annual_membership_cash; }
    public void setAnnual_membership_cash(String annual_membership_cash) { this.annual_membership_cash = annual_membership_cash; }

    public String getBasic_cash() { return basic_cash; }
    public void setBasic_cash(String basic_cash) { this.basic_cash = basic_cash; }

}
