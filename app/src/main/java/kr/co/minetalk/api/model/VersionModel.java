package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class VersionModel extends BaseModel {

    public static VersionModel parse(JSONObject jsonObject) throws JSONException {
        VersionModel item = new VersionModel();

        if(!jsonObject.isNull("user_platform")) {
            item.setUser_platform(jsonObject.getString("user_platform"));
        }

        if(!jsonObject.isNull("version")) {
            item.setVersion(jsonObject.getString("version"));
        }

        if(!jsonObject.isNull("force_yn")) {
            item.setForce_yn(jsonObject.getString("force_yn"));
        }

        if(!jsonObject.isNull("sms_phone")) {
            item.setSms_phone(jsonObject.getString("sms_phone"));
        }

        if(!jsonObject.isNull("notice_yn")) {
            item.setNotice_yn(jsonObject.getString("notice_yn"));
        }

        if(!jsonObject.isNull("notice_title")) {
            item.setNotice_title(jsonObject.getString("notice_title"));
        }

        if(!jsonObject.isNull("notice_contents")) {
            item.setNotice_contents(jsonObject.getString("notice_contents"));
        }

        return item;
    }

    private String user_platform = "";
    private String version       = "";
    private String force_yn      = "";
    private String sms_phone     = "";

    private String notice_yn = "";
    private String notice_title = "";
    private String notice_contents = "";

    public String getNotice_yn() {
        return notice_yn;
    }

    public void setNotice_yn(String notice_yn) {
        this.notice_yn = notice_yn;
    }

    public String getNotice_title() {
        return notice_title;
    }

    public void setNotice_title(String notice_title) {
        this.notice_title = notice_title;
    }

    public String getNotice_contents() {
        return notice_contents;
    }

    public void setNotice_contents(String notice_contents) {
        this.notice_contents = notice_contents;
    }

    public String getSms_phone() {
        return sms_phone;
    }

    public void setSms_phone(String sms_phone) {
        this.sms_phone = sms_phone;
    }

    public String getUser_platform() {
        return user_platform;
    }

    public void setUser_platform(String user_platform) {
        this.user_platform = user_platform;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getForce_yn() {
        return force_yn;
    }

    public void setForce_yn(String force_yn) {
        this.force_yn = force_yn;
    }
}
