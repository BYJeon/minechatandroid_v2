package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ChargeConfigModel extends BaseModel {

    public static ChargeConfigModel parse(JSONObject jsonObject) throws JSONException {
        ChargeConfigModel item = new ChargeConfigModel();

        if(!jsonObject.isNull("bank_name")) {
            item.setBank_name(jsonObject.getString("bank_name"));
        }

        if(!jsonObject.isNull("bank_account")) {
            item.setBank_account(jsonObject.getString("bank_account"));
        }

        /*  v1
        if(!jsonObject.isNull("charge_amount_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("charge_amount_list");
            ArrayList<String> arrayList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                String amount = jsonArray.getString(i);
                arrayList.add(amount);
            }
            item.setCharge_amount_list(arrayList);
        }
        */

        if(!jsonObject.isNull("charge_amount_list_v2")) {
            JSONArray jsonArray = jsonObject.getJSONArray("charge_amount_list_v2");
            ArrayList<ChargeAmountModel> arrayList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                ChargeAmountModel chargeAmountModel = new ChargeAmountModel();
                chargeAmountModel = ChargeAmountModel.parse(jsonArray.getJSONObject(i));
                arrayList.add(chargeAmountModel);
            }
            item.setCharge_amount_list(arrayList);
        }

        if(!jsonObject.isNull("office_tel")) {
            item.setOffice_tel(jsonObject.getString("office_tel"));
        }

        return item;
    }

    private String bank_name = "";
    private String bank_account = "";

    private ArrayList<ChargeAmountModel> charge_amount_list = new ArrayList<>();
    private String office_tel = "";

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_account() {
        return bank_account;
    }

    public void setBank_account(String bank_account) {
        this.bank_account = bank_account;
    }

    public ArrayList<ChargeAmountModel> getCharge_amount_list() {
        return charge_amount_list;
    }

    public void setCharge_amount_list(ArrayList<ChargeAmountModel> charge_amount_list) {
        this.charge_amount_list = charge_amount_list;
    }

    public String getOffice_tel() {
        return office_tel;
    }

    public void setOffice_tel(String office_tel) {
        this.office_tel = office_tel;
    }
}
