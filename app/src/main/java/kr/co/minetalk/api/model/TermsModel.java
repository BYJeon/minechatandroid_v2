package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class TermsModel extends BaseModel {

    public static TermsModel parse(JSONObject jsonObject) throws JSONException {
        TermsModel item = new TermsModel();

        if(!jsonObject.isNull("policy_title")) {
            item.setPolicy_title(jsonObject.getString("policy_title"));
        }

        if(!jsonObject.isNull("policy_text")) {
            item.setPolicy_text(jsonObject.getString("policy_text"));
        }



        return item;
    }


    private String policy_title;
    private String policy_text;

    public String getPolicy_title() {
        return policy_title;
    }

    public void setPolicy_title(String policy_title) {
        this.policy_title = policy_title;
    }

    public String getPolicy_text() {
        return policy_text;
    }

    public void setPolicy_text(String policy_text) {
        this.policy_text = policy_text;
    }
}
