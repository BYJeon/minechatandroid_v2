package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdListResponse extends BaseModel {

    public static AdListResponse parse(JSONObject jsonObject) throws JSONException {
        AdListResponse response = new AdListResponse();

        if(!jsonObject.isNull("total_cnt")) {
            response.setTotal_cnt(jsonObject.getInt("total_cnt"));
        }

        if(!jsonObject.isNull("page")) {
            response.setPage(jsonObject.getInt("page"));
        }

        if(!jsonObject.isNull("ad_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("ad_list");
            ArrayList<AdListModel> adListModels = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                AdListModel item = new AdListModel();
                item = AdListModel.parse(jsonArray.getJSONObject(i));
                adListModels.add(item);
            }
            response.setAd_list(adListModels);
        }

        return response;
    }

    private ArrayList<AdListModel> ad_list = new ArrayList<>();
    private int total_cnt = 0;
    private int page = 0;

    public ArrayList<AdListModel> getAd_list() {
        return ad_list;
    }

    public void setAd_list(ArrayList<AdListModel> ad_list) {
        this.ad_list = ad_list;
    }

    public int getTotal_cnt() {
        return total_cnt;
    }

    public void setTotal_cnt(int total_cnt) {
        this.total_cnt = total_cnt;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
