package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MemberShipHistoryResponse extends BaseModel {

    public static MemberShipHistoryResponse parse(JSONObject jsonObject) throws JSONException {
        MemberShipHistoryResponse item = new MemberShipHistoryResponse();

        if(!jsonObject.isNull("membership_end_date")) {
            item.setMembership_end_date(jsonObject.getString("membership_end_date"));
        }

        if(!jsonObject.isNull("annual_membership_history")) {
            JSONArray jsonArray = jsonObject.getJSONArray("annual_membership_history");
            ArrayList<MemberShipHistoryModel> historyModels = new ArrayList<>();

            for(int i = 0 ; i < jsonArray.length() ; i++) {
                MemberShipHistoryModel tokenHistoryModel = new MemberShipHistoryModel();
                tokenHistoryModel = MemberShipHistoryModel.parse(jsonArray.getJSONObject(i));
                historyModels.add(tokenHistoryModel);
            }

            item.setMembership_history(historyModels);
        }

        return item;
    }


    private String membership_end_date = "";
    private ArrayList<MemberShipHistoryModel> membership_history = new ArrayList<>();

    public String getMembership_end_date(){return membership_end_date;}
    public void setMembership_end_date(String membership_end_date){this.membership_end_date = membership_end_date;}

    public ArrayList<MemberShipHistoryModel> getMembership_history(){return membership_history;}
    public void setMembership_history(ArrayList<MemberShipHistoryModel> membership_history) { this.membership_history = membership_history;}
}
