package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class TargetMessageModel extends BaseModel {

    public static TargetMessageModel parse(JSONObject jsonObject) throws JSONException {
        TargetMessageModel item = new TargetMessageModel();

        if(!jsonObject.isNull("member_count")) {
            item.setMember_count(jsonObject.getString("member_count"));
        }

        if(!jsonObject.isNull("max_value")) {
            item.setMax_value(jsonObject.getInt("max_value"));
        }

        return item;
    }

    private String member_count = "";
    private int max_value = 0;

    public String getMember_count() {
        return member_count;
    }

    public void setMember_count(String member_count) {
        this.member_count = member_count;
    }

    public int getMax_value() {
        return max_value;
    }

    public void setMax_value(int max_value) {
        this.max_value = max_value;
    }


}
