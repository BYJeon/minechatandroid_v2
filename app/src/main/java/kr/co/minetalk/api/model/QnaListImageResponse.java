package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QnaListImageResponse extends BaseModel {

    public static QnaListImageResponse parse(JSONArray jsonArray) throws JSONException {
        QnaListImageResponse item = new QnaListImageResponse();

        ArrayList<QnaListImageModel> dataList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            QnaListImageModel imageModel = new QnaListImageModel();
            imageModel = QnaListImageModel.parse(jsonArray.getJSONObject(i));
            dataList.add(imageModel);
        }

        item.setImage_list(dataList);

        return item;
    }

    private ArrayList<QnaListImageModel> image_list = new ArrayList<>();


    public void setImage_list(ArrayList<QnaListImageModel> image_list) {
        this.image_list = image_list;
    }

    public ArrayList<QnaListImageModel> geImage_list() {
        return image_list;
    }

}
