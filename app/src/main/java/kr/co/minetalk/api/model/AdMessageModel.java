package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class AdMessageModel extends BaseModel {

    public static AdMessageModel parse(JSONObject jsonObject) throws JSONException {
        AdMessageModel item = new AdMessageModel();

        if(!jsonObject.isNull("user_xid")) {
            item.setUser_xid(jsonObject.getString("user_xid"));
        }
        if(!jsonObject.isNull("user_profile_image")) {
            item.setUser_profile_image(jsonObject.getString("user_profile_image"));
        }
        if(!jsonObject.isNull("ad_title")) {
            item.setAd_title(jsonObject.getString("ad_title"));
        }
        if(!jsonObject.isNull("ad_contents")) {
            item.setAd_contents(jsonObject.getString("ad_contents"));
        }
        if(!jsonObject.isNull("receive_yn")) {
            item.setReceive_yn(jsonObject.getString("receive_yn"));
        }
        if(!jsonObject.isNull("transaction_uuid")) {
            item.setTransaction_uuid(jsonObject.getString("transaction_uuid"));
        }
        if(!jsonObject.isNull("ad_send_date")) {
            item.setAd_send_date(jsonObject.getString("ad_send_date"));
        }
        if(!jsonObject.isNull("ad_token_amount")) {
            item.setAd_token_amount(jsonObject.getString("ad_token_amount"));
        }
        if(!jsonObject.isNull("ad_image_url")) {
            item.setAd_image_url(jsonObject.getString("ad_image_url"));
        }

        return item;
    }
    private String user_xid = "";
    private String user_profile_image = "";
    private String ad_title = "";
    private String ad_contents = "";
    private String receive_yn = "";
    private String transaction_uuid = "";
    private String ad_send_date = "";
    private String ad_token_amount = "";
    private String ad_image_url = "";

    public String getUser_xid() {
        return user_xid;
    }

    public void setUser_xid(String user_xid) {
        this.user_xid = user_xid;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getAd_title() {
        return ad_title;
    }

    public void setAd_title(String ad_title) {
        this.ad_title = ad_title;
    }

    public String getAd_contents() {
        return ad_contents;
    }

    public void setAd_contents(String ad_contents) {
        this.ad_contents = ad_contents;
    }

    public String getReceive_yn() {
        return receive_yn;
    }

    public void setReceive_yn(String receive_yn) {
        this.receive_yn = receive_yn;
    }

    public String getTransaction_uuid() {
        return transaction_uuid;
    }

    public void setTransaction_uuid(String transaction_uuid) {
        this.transaction_uuid = transaction_uuid;
    }

    public String getAd_send_date() {
        return ad_send_date;
    }

    public void setAd_send_date(String ad_send_date) {
        this.ad_send_date = ad_send_date;
    }

    public String getAd_token_amount() {
        return ad_token_amount;
    }

    public void setAd_token_amount(String ad_token_amount) {
        this.ad_token_amount = ad_token_amount;
    }

    public String getAd_image_url() {
        return ad_image_url;
    }

    public void setAd_image_url(String ad_image_url) {
        this.ad_image_url = ad_image_url;
    }
}
