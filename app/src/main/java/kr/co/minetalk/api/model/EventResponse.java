package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EventResponse extends BaseModel {

    public static EventResponse parse(JSONObject jsonObject) throws JSONException {
        EventResponse response = new EventResponse();

        JSONArray jsonArray = jsonObject.getJSONArray("data");
        ArrayList<EventModel> noticeModels = new ArrayList<>();
        for(int i = 0 ; i < jsonArray.length() ; i++) {
            EventModel eventModel = new EventModel();
            eventModel = EventModel.parse(jsonArray.getJSONObject(i));
            noticeModels.add(eventModel);

        }
        response.setData(noticeModels);

        return response;
    }

    private ArrayList<EventModel> data = new ArrayList<>();

    public ArrayList<EventModel> getData() {
        return data;
    }

    public void setData(ArrayList<EventModel> data) {
        this.data = data;
    }
}
