package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.BaseModel;
import retrofit.Call;

public class CheckSmsApi extends ApiBase<BaseModel> {
    public CheckSmsApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String type, String user_nation_code, String user_hp, String code) {

        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("type", type);
            mReqeustParams.put("user_nation_code", user_nation_code);
            mReqeustParams.put("user_hp", user_hp);
            mReqeustParams.put("code", code);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());

            this.apiExecute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.checkSms(mReqeustBody);
    }

    @Override
    protected BaseModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    BaseModel baseModel = new BaseModel();

                    if(!responseObject.isNull("code")) {
                        baseModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        baseModel.setMessage(responseObject.getString("message"));
                    }
                    return baseModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
