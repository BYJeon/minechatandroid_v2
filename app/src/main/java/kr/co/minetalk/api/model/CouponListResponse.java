package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CouponListResponse extends BaseModel {

    public static CouponListResponse parse(JSONObject jsonObject) throws JSONException {
        CouponListResponse item = new CouponListResponse();

        if(!jsonObject.isNull("coupon_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("coupon_list");
            ArrayList<CouponInfoModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                CouponInfoModel couponInfoModel = new CouponInfoModel();
                couponInfoModel = CouponInfoModel.parse(jsonArray.getJSONObject(i));
                dataList.add(couponInfoModel);
            }
            item.setCouponInfo_list(dataList);
        }

        if(!jsonObject.isNull("payment_info")) {
            JSONObject jsonObj = jsonObject.getJSONObject("payment_info");

            if(!jsonObj.isNull("bank_name")) {
                item.setBank_name(jsonObj.getString("bank_name"));
            }

            if(!jsonObj.isNull("bank_account")) {
                item.setBank_account(jsonObj.getString("bank_account"));
            }

            if(!jsonObj.isNull("bank_owner")) {
                item.setBank_owner(jsonObj.getString("bank_owner"));
            }

        }

        return item;
    }

    private String bank_name = "";
    private String bank_account = "";
    private String bank_owner = "";

    public String getBank_name(){return bank_name;}
    public void setBank_name(String bank_name){this.bank_name = bank_name;}

    public String getBank_account(){return bank_account;}
    public void setBank_account(String bank_account){this.bank_account = bank_account;}

    public String getBank_owner(){return bank_owner;}
    public void setBank_owner(String bank_owner){this.bank_owner = bank_owner;}


    private ArrayList<CouponInfoModel> couponInfo_list = new ArrayList<>();
    public ArrayList<CouponInfoModel> getCouponInfo_list() {
        return couponInfo_list;
    }

    public void setCouponInfo_list(ArrayList<CouponInfoModel> couponInfo_list) {
        this.couponInfo_list = couponInfo_list;
    }


}
