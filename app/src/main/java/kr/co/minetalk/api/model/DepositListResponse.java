package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DepositListResponse extends BaseModel {

    public static DepositListResponse parse(JSONObject jsonObject) throws JSONException {
        DepositListResponse item = new DepositListResponse();

        long nTotal = 0;
        if(!jsonObject.isNull("deposit_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("deposit_list");
            ArrayList<PointAmountListModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                PointAmountListModel depositListModel = new PointAmountListModel();
                depositListModel = PointAmountListModel.parse(jsonArray.getJSONObject(i));
                nTotal += Long.parseLong(depositListModel.getPoint_amount());
                dataList.add(depositListModel);
            }
            item.setDeposit_list(dataList);
            item.setDeposit_total_point(Long.toString(nTotal));
        }

        return item;
    }

    private String deposit_total_point = "";
    private ArrayList<PointAmountListModel> deposit_list = new ArrayList<>();

    public void setDeposit_total_point(String deposit_total_point) {
        this.deposit_total_point = deposit_total_point;
    }
    public String getDeposit_total_point(){return deposit_total_point;}

    public ArrayList<PointAmountListModel> getDeposit_list() {
        return deposit_list;
    }

    public void setDeposit_list(ArrayList<PointAmountListModel> deposit_list) {
        this.deposit_list = deposit_list;
    }
}
