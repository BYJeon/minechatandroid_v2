package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserAccumulateListResponse extends BaseModel {

    public static UserAccumulateListResponse parse(JSONObject jsonObject) throws JSONException {
        UserAccumulateListResponse item = new UserAccumulateListResponse();

        if(!jsonObject.isNull("accumulate_history_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("accumulate_history_list");
            ArrayList<MiningModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                MiningModel miningModel = new MiningModel();
                miningModel = MiningModel.parse(jsonArray.getJSONObject(i));
                dataList.add(miningModel);
            }
            item.setMining_list(dataList);
        }


        if(!jsonObject.isNull("total_accumulate_amount")) {
            item.setTotal_accumulate_amount(jsonObject.getString("total_accumulate_amount"));
        }

        return item;
    }

    private String total_accumulate_amount = "";

    public String getTotal_accumulate_amount(){return total_accumulate_amount;}
    public void setTotal_accumulate_amount(String total_accumulate_amount){this.total_accumulate_amount = total_accumulate_amount;}

    private ArrayList<MiningModel> mining_list = new ArrayList<>();
    public ArrayList<MiningModel> getMiningList() {
        return mining_list;
    }

    public void setMining_list(ArrayList<MiningModel> mining_list) {
        this.mining_list = mining_list;
    }


}
