package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class NextGearResponse extends BaseModel {

    public static NextGearResponse parse(JSONObject jsonObject) throws JSONException {
        NextGearResponse item = new NextGearResponse();

        if(!jsonObject.isNull("nextgear_yn")) {
            item.setNextgear_yn(jsonObject.getString("nextgear_yn"));
        }

        if(!jsonObject.isNull("nextgear_overbalance")) {
            item.setNextgear_overbalance(jsonObject.getString("nextgear_overbalance"));
        }

        if(!jsonObject.isNull("popup_message")) {
            item.setPopup_message(jsonObject.getString("popup_message"));
        }

        if(!jsonObject.isNull("nextgear_apply_yn")) {
            item.setNextgear_apply_yn(jsonObject.getString("nextgear_apply_yn"));
        }

        if(!jsonObject.isNull("system_number")) {
            item.setSystem_number(jsonObject.getString("system_number"));
        }

        if(!jsonObject.isNull("user_number")) {
            item.setUser_number(jsonObject.getString("user_number"));
        }

        return item;
    }

    private String nextgear_yn = "";
    private String nextgear_overbalance = "";
    private String popup_message = "";
    private String nextgear_apply_yn = "";
    private String system_number = "";
    private String user_number = "";

    public String getNextgear_yn(){return nextgear_yn;}
    public void setNextgear_yn(String nextgear_yn){this.nextgear_yn = nextgear_yn;}

    public String getNextgear_overbalance(){return nextgear_overbalance;}
    public void setNextgear_overbalance(String nextgear_overbalance){this.nextgear_overbalance = nextgear_overbalance;}

    public String getPopup_message(){return popup_message;}
    public void setPopup_message(String popup_message){this.popup_message = popup_message;}

    public String getNextgear_apply_yn(){return nextgear_apply_yn;}
    public void setNextgear_apply_yn(String nextgear_apply_yn){this.nextgear_apply_yn = nextgear_apply_yn;}

    public String getSystem_number(){return system_number;}
    public void setSystem_number(String system_number){this.system_number = system_number;}

    public String getUser_number(){return user_number;}
    public void setUser_number(String user_number){this.user_number = user_number;}
}
