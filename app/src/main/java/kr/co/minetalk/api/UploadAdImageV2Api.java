package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.AdListImageResponse;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.QnaListImageResponse;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;


/**
 * Created by episode on 2018. 10. 7..
 */

public class UploadAdImageV2Api extends ApiBase<BaseModel> {

    private ArrayList<String> mImagePath;
    private ArrayList<File> mImageFile = new ArrayList<>();

    public UploadAdImageV2Api(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(Context context, ArrayList<String> imagePath) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage();
        setHeader(authorization, x_login_type, x_lang);



        this.mImagePath = imagePath;
        for(String path : mImagePath){
            mImageFile.add(new File(path));
        }
        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        HashMap<String, RequestBody> parmsMap = new HashMap<>();

        for( File file : mImageFile){
            RequestBody reqeustBody = RequestBody.create(MediaType.parse("image/*"), file);
            String filename = file.getName();
            parmsMap.put("image[]\"; filename=\"" + filename, reqeustBody);
        }

        return apiInterface.uploadAdPicture(parmsMap);
    }

    @Override
    protected AdListImageResponse parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                JSONObject responseObject = new JSONObject(response_str);

                AdListImageResponse adImgListResponse = new AdListImageResponse();
                if(!responseObject.isNull("data")) {
                    adImgListResponse = AdListImageResponse.parse(responseObject.getJSONArray("data"));
                }

                if(!responseObject.isNull("code")) {
                    adImgListResponse.setCode(responseObject.getString("code"));
                }

                if(!responseObject.isNull("message")) {
                    adImgListResponse.setMessage(responseObject.getString("message"));
                }


                return adImgListResponse;
            }
        }
        return null;
    }
}
