package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CouponPurchaseListResponse extends BaseModel {

    public static CouponPurchaseListResponse parse(JSONObject jsonObject) throws JSONException {
        CouponPurchaseListResponse item = new CouponPurchaseListResponse();

        if(!jsonObject.isNull("coupon_purchase_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("coupon_purchase_list");
            ArrayList<CouponModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                CouponModel couponModel = new CouponModel("","","","");
                couponModel = CouponModel.parse(jsonArray.getJSONObject(i));
                dataList.add(couponModel);
            }
            item.setCouponInfo_list(dataList);
        }

        if(!jsonObject.isNull("total_coupon_amount")) {
            item.setTotal_coupon_amount(jsonObject.getString("total_coupon_amount"));
        }

        return item;
    }

    private String total_coupon_amount = "";

    public void setTotal_coupon_amount(String total_coupon_amount){this.total_coupon_amount = total_coupon_amount;}
    public String getTotal_coupon_amount(){return this.total_coupon_amount;}

    private ArrayList<CouponModel> couponInfo_list = new ArrayList<>();
    public ArrayList<CouponModel> getCouponInfo_list() {
        return couponInfo_list;
    }

    public void setCouponInfo_list(ArrayList<CouponModel> couponInfo_list) {
        this.couponInfo_list = couponInfo_list;
    }


}
