package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.FileInfoModel;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;


/**
 * Created by episode on 2018. 10. 7..
 */

public class MessageImageApi extends ApiBase<FileInfoModel> {

    private File mImageFile;

    public MessageImageApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(Context context, File imageFile) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage();
        setHeader(authorization, x_login_type, x_lang);

        this.mImageFile = imageFile;
        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        mReqeustBody = RequestBody.create(MediaType.parse("image/*"), mImageFile);

        String filename = mImageFile.getName();
        HashMap<String, RequestBody> parmsMap = new HashMap<>();
        parmsMap.put("file\"; filename=\"" + filename, mReqeustBody);

        return apiInterface.messageImage(parmsMap);
    }

    @Override
    protected FileInfoModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                JSONObject responseObject = new JSONObject(response_str);
                FileInfoModel fileInfoModel = new FileInfoModel();

                if (!responseObject.isNull("data")) {
                    fileInfoModel = FileInfoModel.parse(responseObject.getJSONObject("data"));
                }

                if(!responseObject.isNull("code")) {
                    fileInfoModel.setCode(responseObject.getString("code"));
                }

                if(!responseObject.isNull("message")) {
                    fileInfoModel.setMessage(responseObject.getString("message"));
                }
                return fileInfoModel;
            }
        }
        return null;
    }
}
