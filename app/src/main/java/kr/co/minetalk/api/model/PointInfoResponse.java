package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class PointInfoResponse extends BaseModel {


    public static PointInfoResponse parse(JSONObject jsonObject) throws JSONException {
        PointInfoResponse item = new PointInfoResponse();

        if(!jsonObject.isNull("user_mine_cash")) {
            item.setUser_mine_cash(jsonObject.getString("user_mine_cash"));
        }

        if(!jsonObject.isNull("basic_mine_cash")) {
            item.setBasic_mine_cash(jsonObject.getString("basic_mine_cash"));
        }

        if(!jsonObject.isNull("mctk")) {
            item.setMctk(jsonObject.getString("mctk"));
        }

        if(!jsonObject.isNull("user_mine_token")) {
            item.setUser_mine_token(jsonObject.getString("user_mine_token"));
        }

        if(!jsonObject.isNull("lock_mine_token")) {
            item.setLock_mine_token(jsonObject.getString("lock_mine_token"));
        }

        return item;
    }


    private String user_mine_cash = "";
    private String basic_mine_cash = "";
    private String mctk = "";
    private String user_mine_token = "";
    private String lock_mine_token = "";


    public String getUser_mine_cash(){return user_mine_cash;}
    public void setUser_mine_cash(String user_mine_cash){this.user_mine_cash = user_mine_cash;}

    public String getBasic_mine_cash(){return basic_mine_cash;}
    public void setBasic_mine_cash(String basic_mine_cash){this.basic_mine_cash = basic_mine_cash;}

    public String getMctk(){return mctk;}
    public void setMctk(String mctk){this.mctk = mctk;}

    public String getUser_mine_token(){return user_mine_token;}
    public void setUser_mine_token(String user_mine_token){this.user_mine_token = user_mine_token;}

    public String getLock_mine_token(){return lock_mine_token;}
    public void setLock_mine_token(String lock_mine_token){this.lock_mine_token = lock_mine_token;}
}
