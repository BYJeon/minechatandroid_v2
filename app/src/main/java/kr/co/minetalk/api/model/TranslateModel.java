package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class TranslateModel extends BaseModel {

    public static TranslateModel parse(JSONObject jsonObject) throws JSONException {
        TranslateModel item = new TranslateModel();

        if(!jsonObject.isNull("source")) {
            item.setSource(jsonObject.getString("source"));
        }

        if(!jsonObject.isNull("input")) {
            item.setInput(jsonObject.getString("input"));
        }

        if(!jsonObject.isNull("text")) {
            item.setText(jsonObject.getString("text"));
        }

        if(!jsonObject.isNull("model")) {
            item.setModel(jsonObject.getString("model"));
        }

        return item;
    }

    private String source = "";
    private String input  = "";
    private String text   = "";
    private String model  = "";

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
