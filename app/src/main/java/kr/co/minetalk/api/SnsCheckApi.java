package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.SnsCheckModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import retrofit.Call;

public class SnsCheckApi extends ApiBase<SnsCheckModel> {
    public SnsCheckApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String user_provider_type, String user_provider_id) {
        try {
            mReqeustParams = new JSONObject();

            mReqeustParams.put("user_provider_type", user_provider_type);
            mReqeustParams.put("user_provider_id", user_provider_id);
            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.snsCheck(mReqeustBody);
    }

    @Override
    protected SnsCheckModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    SnsCheckModel snsCheckModel = new SnsCheckModel();
                    if(!responseObject.isNull("data")) {
                        snsCheckModel = SnsCheckModel.parse(responseObject.getJSONObject("data"));
                    }
                    if(!responseObject.isNull("code")) {
                        snsCheckModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        snsCheckModel.setMessage(responseObject.getString("message"));
                    }
                    return snsCheckModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
