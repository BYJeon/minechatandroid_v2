package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.MiningInfoResponse;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class MiningInfoApi extends ApiBase<MiningInfoResponse> {
    public MiningInfoApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute() {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.miningInfo();
    }

    @Override
    protected MiningInfoResponse parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    MiningInfoResponse response = new MiningInfoResponse();

                    if(!responseObject.isNull("code")) {
                        response.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        response.setMessage(responseObject.getString("message"));
                    }
                    if (!responseObject.isNull("data")) {
                        response = MiningInfoResponse.parse(responseObject.getJSONObject("data"));
                    }
                    return response;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
