package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class UserExchangeInfoResponse extends BaseModel {

    public static UserExchangeInfoResponse parse(JSONObject jsonObject) throws JSONException {
        UserExchangeInfoResponse item = new UserExchangeInfoResponse();

        if(!jsonObject.isNull("user_mine_token")) {
            item.setUser_mine_token(jsonObject.getString("user_mine_token"));
        }

        if(!jsonObject.isNull("exchange_day_limit")) {
            item.setExchange_day_limit(jsonObject.getString("exchange_day_limit"));
        }

        if(!jsonObject.isNull("exchange_day_min_limit")) {
            item.setExchange_day_min_limit(jsonObject.getString("exchange_day_min_limit"));
        }

        if(!jsonObject.isNull("exchange_day_max_limit")) {
            item.setExchange_day_max_limit(jsonObject.getString("exchange_day_max_limit"));
        }

        if(!jsonObject.isNull("exchange_day_count")) {
            item.setExchange_day_count(jsonObject.getString("exchange_day_count"));
        }

        if(!jsonObject.isNull("exchange_day_request_count")) {
            item.setExchange_day_request_count(jsonObject.getString("exchange_day_request_count"));
        }

        if(!jsonObject.isNull("exchange_month_limit")) {
            item.setExchange_month_limit(jsonObject.getString("exchange_month_limit"));
        }

        if(!jsonObject.isNull("exchange_mine_token_min_request")) {
            item.setExchange_mine_token_day_limit(jsonObject.getString("exchange_mine_token_min_request"));
        }

        if(!jsonObject.isNull("exchange_token_fee_per")) {
            item.setExchange_token_fee_per(jsonObject.getString("exchange_token_fee_per"));
        }

        if(!jsonObject.isNull("total_deposit_amount")) {
            item.setTotal_deposit_amount(jsonObject.getString("total_deposit_amount"));
        }

        if(!jsonObject.isNull("use_flag")) {
            item.setUse_flag(jsonObject.getString("use_flag"));
        }

        if(!jsonObject.isNull("alert_message")) {
            item.setAlert_message(jsonObject.getString("alert_message"));
        }

        return item;
    }

    private String user_mine_token = "";
    private String exchange_day_limit = "";

    private String exchange_day_min_limit = "";
    private String exchange_day_max_limit = "";
    private String exchange_day_count = "";
    private String exchange_day_request_count = "";

    private String exchange_month_limit = "";
    private String exchange_mine_token_min_request = "";
    private String exchange_token_fee_per = "";

    private String total_deposit_amount = "";
    private String use_flag = "";
    private String alert_message = "";

    public void setUser_mine_token(String user_mine_token){this.user_mine_token = user_mine_token;}
    public String getUser_mine_token(){return user_mine_token;}

    public void setExchange_day_limit(String exchange_day_limit){this.exchange_day_limit = exchange_day_limit;}
    public String getExchange_day_limit(){return exchange_day_limit;}

    public void setExchange_day_min_limit(String exchange_day_min_limit){this.exchange_day_min_limit = exchange_day_min_limit;}
    public String getExchange_day_min_limit(){return exchange_day_min_limit;}

    public void setExchange_day_max_limit(String exchange_day_max_limit){
        this.exchange_day_max_limit = exchange_day_max_limit;
    }
    public String getExchange_day_max_limit(){return exchange_day_max_limit;}

    public void setExchange_day_count(String exchange_day_count){this.exchange_day_count = exchange_day_count;}
    public String getExchange_day_count(){return exchange_day_count;}

    public void setExchange_day_request_count(String exchange_day_request_count){this.exchange_day_request_count = exchange_day_request_count;}
    public String getExchange_day_request_count(){return exchange_day_request_count;}


    public void setExchange_month_limit(String exchange_month_limit){this.exchange_month_limit = exchange_month_limit;}
    public String getExchange_month_limit(){return exchange_month_limit;}

    public void setExchange_mine_token_day_limit(String exchange_mine_token_min_request){this.exchange_mine_token_min_request = exchange_mine_token_min_request;}
    public String getExchange_mine_token_min_request(){return exchange_mine_token_min_request;}

    public void setExchange_token_fee_per(String exchange_token_fee_per){this.exchange_token_fee_per = exchange_token_fee_per;}
    public String getExchange_token_fee_per(){return exchange_token_fee_per;}

    public void setTotal_deposit_amount(String total_deposit_amount){this.total_deposit_amount = total_deposit_amount;}
    public String getTotal_deposit_amount(){return total_deposit_amount;}

    public void setUse_flag(String use_flag){this.use_flag = use_flag;}
    public String getUse_flag(){return use_flag;}

    public void setAlert_message(String alert_message){this.alert_message = alert_message;}
    public String getAlert_message(){return alert_message;}
}
