package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.VersionModel;
import retrofit.Call;

public class VersionApi extends ApiBase<VersionModel> {

    public VersionApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(Context context, String user_platform) {
        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("user_platform", user_platform);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());

            this.apiExecute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.version(mReqeustBody);
    }

    @Override
    protected VersionModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);

                    VersionModel versionModel = new VersionModel();
                    if(!responseObject.isNull("data")) {
                        versionModel = VersionModel.parse(responseObject.getJSONObject("data"));
                    }

                    if(!responseObject.isNull("code")) {
                        versionModel.setCode(responseObject.getString("code"));
                    }

                    if(!responseObject.isNull("message")) {
                        versionModel.setMessage(responseObject.getString("message"));
                    }

                    return versionModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
