package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class RecommendModel extends BaseModel {

    public static RecommendModel parse(JSONObject jsonObject) throws JSONException {
        RecommendModel item = new RecommendModel();
        if(!jsonObject.isNull("user_name")) {
            item.setUser_name(jsonObject.getString("user_name"));
        }

        if(!jsonObject.isNull("user_hp")) {
            item.setUser_hp(jsonObject.getString("user_hp"));
        }

        if(!jsonObject.isNull("use_flag")) {
            item.setUse_flag(jsonObject.getString("use_flag"));
        }

        if(!jsonObject.isNull("reg_date")) {
            item.setReg_date(jsonObject.getString("reg_date"));
        }

        if(!jsonObject.isNull("user_hp_confirm")) {
            item.setUser_hp_confirm(jsonObject.getString("user_hp_confirm"));
        }

        return item;
    }
    private String user_name = "";
    private String user_hp   = "";
    private String use_flag  = "";
    private String reg_date  = "";
    private String user_hp_confirm  = "";

    public String getUser_hp_confirm(){return user_hp_confirm;}
    public void setUser_hp_confirm(String user_hp_confirm){this.user_hp_confirm = user_hp_confirm;}

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_hp() {
        return user_hp;
    }

    public void setUser_hp(String user_hp) {
        this.user_hp = user_hp;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }
}
