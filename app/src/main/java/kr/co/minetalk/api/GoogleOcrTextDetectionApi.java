package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.SnsCheckModel;
import kr.co.minetalk.api.model.TextDetectionModel;
import retrofit.Call;

public class GoogleOcrTextDetectionApi extends ApiBase<TextDetectionModel> {
    public GoogleOcrTextDetectionApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String base64_image) {
        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("base64_image", base64_image);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.googleOcrTextDetection(mReqeustBody);
    }

    @Override
    protected TextDetectionModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    TextDetectionModel textDetectionModel = new TextDetectionModel();
                    if(!responseObject.isNull("data")) {
                        textDetectionModel = TextDetectionModel.parse(responseObject.getJSONObject("data"));
                    }
                    if(!responseObject.isNull("code")) {
                        textDetectionModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        textDetectionModel.setMessage(responseObject.getString("message"));
                    }
                    return textDetectionModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
