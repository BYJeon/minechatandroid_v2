package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FriendListResponse extends BaseModel {

    public static FriendListResponse parse(JSONObject jsonObject) throws JSONException {
        FriendListResponse item = new FriendListResponse();

        if(!jsonObject.isNull("recommend_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("recommend_list");
            ArrayList<FriendListModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                FriendListModel friendListModel = new FriendListModel();
                friendListModel = FriendListModel.parse(jsonArray.getJSONObject(i));
                dataList.add(friendListModel);
            }
            item.setRecommend_list(dataList);
        }

        if(!jsonObject.isNull("friend_list")) {
            JSONArray jsonArray = jsonObject.getJSONArray("friend_list");
            ArrayList<FriendListModel> dataList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                FriendListModel friendListModel = new FriendListModel();
                friendListModel = FriendListModel.parse(jsonArray.getJSONObject(i));
                dataList.add(friendListModel);
            }
            item.setFriend_list(dataList);
        }

        return item;
    }

    private ArrayList<FriendListModel> recommend_list = new ArrayList<>();
    private ArrayList<FriendListModel> friend_list    = new ArrayList<>();

    public ArrayList<FriendListModel> getRecommend_list() {
        return recommend_list;
    }

    public void setRecommend_list(ArrayList<FriendListModel> recommend_list) {
        this.recommend_list = recommend_list;
    }

    public ArrayList<FriendListModel> getFriend_list() {
        return friend_list;
    }

    public void setFriend_list(ArrayList<FriendListModel> friend_list) {
        this.friend_list = friend_list;
    }
}
