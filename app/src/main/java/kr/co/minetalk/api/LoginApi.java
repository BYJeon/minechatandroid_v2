package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.BuildConfig;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class LoginApi extends ApiBase<UserInfoBaseModel> {

    public LoginApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String platform) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);

        String os_version = String.valueOf(Build.VERSION.SDK_INT);
        String device_model = Build.MODEL;
        String app_version = BuildConfig.VERSION_NAME;
        String user_registration_id = Preferences.getPushToken();

        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("os_version", os_version);
            mReqeustParams.put("device_model", device_model);
            mReqeustParams.put("app_version", app_version);
            mReqeustParams.put("user_platform", platform);
            mReqeustParams.put("user_registration_id",user_registration_id);

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.login(mReqeustBody);
    }

    @Override
    protected UserInfoBaseModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    UserInfoBaseModel userInfoModel = new UserInfoBaseModel();
                    if(!responseObject.isNull("data")) {
                        userInfoModel = UserInfoBaseModel.parse(responseObject.getJSONObject("data"));
                    }
                    if(!responseObject.isNull("code")) {
                        userInfoModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        userInfoModel.setMessage(responseObject.getString("message"));
                    }
                    return userInfoModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
