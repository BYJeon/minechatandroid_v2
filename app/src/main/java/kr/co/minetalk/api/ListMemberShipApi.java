package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.MemberShipHistoryResponse;
import kr.co.minetalk.api.model.MineTokenHistoryResponse;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class ListMemberShipApi extends ApiBase<MemberShipHistoryResponse> {

    public ListMemberShipApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute() {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);
        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.getAnnualMemberShipHistory();
    }

    @Override
    protected MemberShipHistoryResponse parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    MemberShipHistoryResponse memberShipHistoryResponse = new MemberShipHistoryResponse();
                    if(!responseObject.isNull("data")) {
                        memberShipHistoryResponse = MemberShipHistoryResponse.parse(responseObject.getJSONObject("data"));
                    }
                    if(!responseObject.isNull("code")) {
                        memberShipHistoryResponse.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        memberShipHistoryResponse.setMessage(responseObject.getString("message"));
                    }
                    return memberShipHistoryResponse;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
