package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;


import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;


/**
 * Created by episode on 2018. 10. 7..
 */

public class UploadImageApi extends ApiBase<BaseModel> {

    private File mImageFile;

    public UploadImageApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(Context context, File imageFile) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage();
        setHeader(authorization, x_login_type, x_lang);


        this.mImageFile = imageFile;
        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        mReqeustBody = RequestBody.create(MediaType.parse("image/*"), mImageFile);

        String filename = mImageFile.getName();
        HashMap<String, RequestBody> parmsMap = new HashMap<>();
        parmsMap.put("image\"; filename=\"" + filename, mReqeustBody);

        return apiInterface.uploadPicture(parmsMap);
    }

    @Override
    protected BaseModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                JSONObject responseObject = new JSONObject(response_str);
                BaseModel baseModel = new BaseModel();

                if(!responseObject.isNull("code")) {
                    baseModel.setCode(responseObject.getString("code"));
                }

                if(!responseObject.isNull("message")) {
                    baseModel.setMessage(responseObject.getString("message"));
                }
                return baseModel;
            }
        }
        return null;
    }
}
