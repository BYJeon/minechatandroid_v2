package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class FileInfoModel extends BaseModel {

    public static FileInfoModel parse(JSONObject jsonObject) throws JSONException {
        FileInfoModel item = new FileInfoModel();

        if(!jsonObject.isNull("url")) {
            item.setUrl(jsonObject.getString("url"));
        }

        if(!jsonObject.isNull("file_name")) {
            item.setFile_name(jsonObject.getString("file_name"));
        }

        if(!jsonObject.isNull("file_type")) {
            item.setFile_type(jsonObject.getString("file_type"));
        }

        if(!jsonObject.isNull("file_ext")) {
            item.setFile_ext(jsonObject.getString("file_ext"));
        }

        if(!jsonObject.isNull("file_size")) {
            item.setFile_size(jsonObject.getString("file_size"));
        }

        if(!jsonObject.isNull("image_type")) {
            item.setImage_type(jsonObject.getString("image_type"));
        }

        return item;
    }


    private String url        = "";
    private String file_name  = "";
    private String file_type  = "";
    private String file_ext   = "";
    private String file_size  = "";
    private String image_type = "";

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getFile_ext() {
        return file_ext;
    }

    public void setFile_ext(String file_ext) {
        this.file_ext = file_ext;
    }

    public String getFile_size() {
        return file_size;
    }

    public void setFile_size(String file_size) {
        this.file_size = file_size;
    }

    public String getImage_type() {
        return image_type;
    }

    public void setImage_type(String image_type) {
        this.image_type = image_type;
    }
}
