package kr.co.minetalk.api;


import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.PartMap;

public interface ApiInterface {


    /**
     * 버전체크
     * @param body
     * @return
     */
    @POST("/api/app/version")
    Call<ResponseBody> version(@Body RequestBody body);


    /**
     * 회원가입
     * @param body
     * @return
     */
    @POST("/api/user/regist")
    Call<ResponseBody> regist(@Body RequestBody body);


    /**
     * 로그인
     * @param body
     * @return
     */
    @POST("/api/user/login")
    Call<ResponseBody> login(@Body RequestBody body);


    /**
     * SNS 가입여부 체크
     * @param body
     * @return
     */
    @POST("/api/user/sns_check")
    Call<ResponseBody> snsCheck(@Body RequestBody body);


    /**
     * 사용자 정보 조회
     * @return
     */
    @POST("/api/user/info")
    Call<ResponseBody> userInfo();


    /**
     * 비밀번호 찾기
     * @param body
     * @return
     */
    @POST("/api/user/pwinquiry")
    Call<ResponseBody> pwInquiry(@Body RequestBody body);


    /**
     * SMS 인증번호 요청
     * @param body
     * @return
     */
    @POST("/api/common/request_sms")
    Call<ResponseBody> requestSms(@Body RequestBody body);


    /**
     * 회원 이름 변경
     * @param body
     * @return
     */
    @POST("/api/user/modify_user_name")
    Call<ResponseBody> modifyUserName(@Body RequestBody body);

    /**
     * 회원 생년월일 변경
     * @param body
     * @return
     */
    @POST("/api/user/modify_user_birthday")
    Call<ResponseBody> modifyUserBirthDay(@Body RequestBody body);

    /**
     * 회원 이메일 변경
     * @param body
     * @return
     */
    @POST("/api/user/modify_user_email")
    Call<ResponseBody> modifyUserEmail(@Body RequestBody body);


    /**
     * 회원 주소 변경
     * @param body
     * @return
     */
    @POST("/api/user/modify_user_addr")
    Call<ResponseBody> modifyUserAddress(@Body RequestBody body);

    /**
     * 회원 비밀번호 변경
     * @param body
     * @return
     */
    @POST("/api/user/modify_user_pwd")
    Call<ResponseBody> modifyUserPwd(@Body RequestBody body);

    /**
     * 회원 상태메세지 변경
     * @param body
     * @return
     */
    @POST("/api/user/modify_user_state_message")
    Call<ResponseBody> modifyUserStateMessage(@Body RequestBody body);


    /**
     * 구글번역
     * @param body
     * @return
     */
    @POST("/api/translate/google_translate")
    Call<ResponseBody> googleTranslate(@Body RequestBody body);


    /**
     * 구글 OCR 텍스트 추출
     * @param body
     * @return
     */
    @POST("/api/translate/google_ocr_text_detection")
    Call<ResponseBody> googleOcrTextDetection(@Body RequestBody body);


    /**
     * 주소 정보 조회
     * @param body
     * @return
     */
    @POST("/api/common/request_addr")
    Call<ResponseBody> requestAddress(@Body RequestBody body);


    /**
     * 공지사항 리스트
     * @return
     */
    @POST("/api/community/list_notice")
    Call<ResponseBody> listNotice();

    /**
     * 이벤트 리스트
     * @return
     */
    @POST("/api/community/list_event")
    Call<ResponseBody> listEvent();

    /**
     * 업데이트 리스트
     * @return
     */
    @POST("/api/community/list_update")
    Call<ResponseBody> listUpdate();

    /**
     * 회원 메세지 수신여부 설정
     * @param body
     * @return
     */
    @POST("/api/user/set_message_receive_flag")
    Call<ResponseBody> setMessageReceiveFlag(@Body RequestBody body);


    /**
     * 회원 탈퇴
     * @return
     */
    @POST("/api/user/secession_user")
    Call<ResponseBody> secessionUser(@Body RequestBody body);

    /**
     * 연락처 업로드
     * @param body
     * @return
     */
    @POST("/api/user/upload_contact")
    Call<ResponseBody> uploadContact(@Body RequestBody body);

    /**
     * 친구 추가
     * @param body
     * @return
     */
    @POST("/api/friend/add_friend")
    Call<ResponseBody> addFriend(@Body RequestBody body);


    /**
     * 친구 차단 설정
     * @param body
     * @return
     */
    @POST("/api/friend/set_reject_friend")
    Call<ResponseBody> setRejectFriend(@Body RequestBody body);

    /**
     * 친구 설정 변경
     * @param body
     * @return
     */
    @POST("/api/friend/set_friend_config")
    Call<ResponseBody> setFriendConfig(@Body RequestBody body);


    /**
     * 친구 설정 가져오기
     * @return
     */
    @POST("/api/friend/get_friend_config")
    Call<ResponseBody> getFriendConfig();

    /**
     * 친구 찾기
     * @param body
     * @return
     */
    @POST("/api/friend/find_friend")
    Call<ResponseBody> findFriend(@Body RequestBody body);


    /**
     * 친구 리스트
     * @param body
     * @return
     */
    @POST("/api/friend/list_friend")
    Call<ResponseBody> listFriend(@Body RequestBody body);


    /**
     * 친구 정보 조회
     * @param body
     * @return
     */
    @POST("/api/friend/info")
    Call<ResponseBody> friendInfo(@Body RequestBody body);

    /**
     * 마인 캐시/토큰 선물하기
     * @param body
     * @return
     */
//    @POST("/api/point/gift_point")
    @POST("/api/point/gift_point_v2")
    Call<ResponseBody> giftPoint(@Body RequestBody body);


    /**
     * 마인 캐시/토큰 결제하기
     * @param body
     * @return
     */
    @POST("/api/point/payment_point")
    Call<ResponseBody> paymentPoint(@Body RequestBody body);


    /**
     * 마인 캐시/토큰 내역
     * @param body
     * @return
     */
    @POST("/api/point/list_point")
    Call<ResponseBody> listPoint(@Body RequestBody body);


    /**
     * 마인 포인트 내역
     * @return
     */
    @POST("/api/point/get_point_info")
    Call<ResponseBody> getPointInfo();

    /**
     * 마인 캐시 보유현황
     * @return
     */
    @POST("/api/point/get_user_mine_cash_info")
    Call<ResponseBody> listMineCashPoint();

    /**
     * 마인 토큰 보유현황
     * @return
     */
    @POST("/api/point/get_user_mine_token_info")
    Call<ResponseBody> listMineTokenPoint();

    /**
     * 충전하기 설정조회
     * @return
     */
    @POST("/api/point/get_charge_config_v2")
    Call<ResponseBody> getChargeConfig();


    /**
     * 마인 캐시 충전하기
     * @return
     */
    @POST("/api/point/charge_point")
    Call<ResponseBody> chargePoint(@Body RequestBody body);

    /**
     * 이미지 업로드
     * @param mapPhoto
     * @return
     */
    @Multipart
    @POST("/api/upload/picture")
    Call<ResponseBody> uploadPicture(@PartMap() Map<String, RequestBody> mapPhoto);


    /**
     * 메세지 이미지 업로드
     * @param mapPhoto
     * @return
     */
    @Multipart
    @POST("/api/upload/message_file")
    Call<ResponseBody> messageImage(@PartMap() Map<String, RequestBody> mapPhoto);


    /**
     *광고 이미지 업로드
     * @param mapPhoto
     * @return
     */
    @Multipart
    @POST("/api/upload/ad_image")
    Call<ResponseBody> adImage(@PartMap() Map<String, RequestBody> mapPhoto);

    /**
     * 광고 리스트 조회
     * @param body
     * @return
     */
    @POST("/api/ad/list_ad_message")
    Call<ResponseBody> listAdMessage(@Body RequestBody body);


    /**
     * 광고 메세지 조회
     * @param body
     * @return
     */
    @POST("/api/ad/view_ad_message")
    Call<ResponseBody> viewAdMessage(@Body RequestBody body);


    /**
     * 광고 수신 대상 회원수 조회
     * @param body
     * @return
     */
    @POST("/api/ad/target_count")
    Call<ResponseBody> targetCount(@Body RequestBody body);

    /**
     * 광고 메시지 작성
     * @param body
     * @return
     */
    @POST("/api/ad/send_ad_message")
    Call<ResponseBody> sendAdMessage(@Body RequestBody body);


    /**
     *회원 성별 변경
     * @param body
     * @return
     */
    @POST("/api/user/modify_user_gender")
    Call<ResponseBody> modifyUserGender(@Body RequestBody body);


    /**
     *회원 직업 변경
     * @param body
     * @return
     */
    @POST("/api/user/modify_user_job")
    Call<ResponseBody> modifyUserJob(@Body RequestBody body);



    /**
     * 채팅 메시지 데이터 삭제(테스트용)
     * @param body
     * @return
     */
    @POST("/api/user/delete_data")
    Call<ResponseBody> deleteData(@Body RequestBody body);


    /**
     * 로그아웃
     * @return
     */
    @POST("/api/user/logout")
    Call<ResponseBody> logout();


    /**
     * FAQ 리스트
     * @return
     */
    @POST("/api/community/list_faq")
    Call<ResponseBody> listFAQ();

    /**
     * 문의하기 리스트
     * @return
     */
    @POST("/api/community/list_qna")
    Call<ResponseBody> listQnA();

    /**
     * 문의하기 등록
     * @param body
     * @return
     */
    @POST("/api/community/add_qna")
    Call<ResponseBody> addQnA(@Body RequestBody body);


    /**
     * 추천인 리스트 조회
     * @param body
     * @return
     */
    @POST("/api/user/list_recommend")
    Call<ResponseBody> listRecommend(@Body RequestBody body);

    /**
     * 거래소 이동 정보 조회
     * @return
     */
    @POST("/api/exchange/info")
    Call<ResponseBody> exchangeInfo();

    /**
     * 토큰 이체 요청
     * @return
     */
    @POST("/api/exchange/transfer_token")
    Call<ResponseBody> transferToken(@Body RequestBody body);


    /**
     * 약관 및 규정 정보 조회
     * @return
     */
    @POST("/api/common/list_policy")
    Call<ResponseBody> listPolicy();


    /**
     * 보안채팅 나가기
     * @return
     */
    @POST("/api/chat/out_security_thread")
    Call<ResponseBody> outSecurityThread(@Body RequestBody body);

    /**
     * 백업 파일 저장
     * @return
     */
    @POST("/api/chat/save_backup_file")
    Call<ResponseBody> saveBackUpFile();

    /**
     * 백업 파일 저장 URL 조회
     * @return
     */
    @POST("/api/chat/select_backup_file")
    Call<ResponseBody> selectBackupFile();

    /**
     * 문의하기 이미지 업로드
     * @param mapPhoto
     * @return
     */
    @Multipart
    @POST("/api/upload/qna_image")
    //Call<ResponseBody> uploadQnAPicture(@Part("image[]") MultipartBody.Part[] mapPhoto);
    Call<ResponseBody> uploadQnAPicture(@PartMap() Map<String, RequestBody> mapPhoto);

    /**
     * 통관번호 변경
     * @return
     */
    @POST("/api/user/modify_customs_clearance_number")
    Call<ResponseBody> modifyCustomsClearanceNumber(@Body RequestBody body);


    /**
     * 사용자 토큰 목록
     * @return
     */
    @POST("/api/exchange/user_tokens_v2")
    Call<ResponseBody> getUserTokens();

    /**
     * 사용자 거래소 정보
     * @return
     */
    @POST("/api/exchange/user_exchange_info")
    Call<ResponseBody> getUserExchangeInfo(@Body RequestBody body);

    /**
     * 사용자 지갑 생성
     * @return
     */
    @POST("/api/exchange/make_user_wallet")
    Call<ResponseBody> makeUserWallet(@Body RequestBody body);


    /**
     * 광고 이미지 업로드 V2
     * @param mapPhoto
     * @return
     */
    @Multipart
    @POST("/api/upload/ad_image_v2")
    Call<ResponseBody> uploadAdPicture(@PartMap() Map<String, RequestBody> mapPhoto);

    /**
     * 연간회원권 구매내역
     * @return
     */
    @POST("/api/point/get_annual_membership_history")
    Call<ResponseBody> getAnnualMemberShipHistory();


    /**
     * 입금 내역
     * @return
     */
    @POST("/api/exchange/list_deposit")
    Call<ResponseBody> listDeposit();

    /**
     * 출금 내역
     * @return
     */
    @POST("/api/exchange/list_withdraw")
    Call<ResponseBody> listWithDraw();

    /**
     * 지갑 주소 확인
     * @return
     */
    @POST("/api/exchange/check_wallet_addr")
    Call<ResponseBody> checkWalletAddr(@Body RequestBody body);

    /**
     * 추천인 확인
     * @param body
     * @return
     */
    @POST("/api/user/check_recommend_user")
    Call<ResponseBody> checkRecommendUser(@Body RequestBody body);


    /**
     * 회원가입 V2
     * @param body
     * @return
     */
    @POST("/api/user/regist_v2")
    Call<ResponseBody> registV2(@Body RequestBody body);

    /**
     * 비밀번호 찾기
     * @param body
     * @return
     */
    @POST("/api/user/find_user_pwd")
    Call<ResponseBody> findUserPwd(@Body RequestBody body);

    /**
     * 아이디 찾기
     * @param body
     * @return
     */
    @POST("/api/user/find_user_login_id")
    Call<ResponseBody> findUserId(@Body RequestBody body);

    /**
     * 핸드폰 중복 체크
     * @param body
     * @return
     */
    @POST("/api/user/check_user_hp")
    Call<ResponseBody> checkUserHP(@Body RequestBody body);

    /**
     * 아이디 중복 체크
     * @param body
     * @return
     */
    @POST("/api/user/check_user_login_id")
    Call<ResponseBody> checkUserID(@Body RequestBody body);

    /**
     * 이메일 중복 체크
     * @param body
     * @return
     */
    @POST("/api/user/check_user_email")
    Call<ResponseBody> checkUserEmail(@Body RequestBody body);

    /**
     * 추가 인증 요청
     * @param body
     * @return
     */
    @POST("/api/common/request_add_certification")
    Call<ResponseBody> requestAddCertification(@Body RequestBody body);

    /**
     * 추가 인증 확인
     * @param body
     * @return
     */
    @POST("/api/common/check_add_certification")
    Call<ResponseBody> checkAddCertification(@Body RequestBody body);

    /**
     * 아이디 변경
     * @param body
     * @return
     */
    @POST("/api/user/change_user_login_id")
    Call<ResponseBody> changeUserLoginID(@Body RequestBody body);

    /**
     * 추천인 변경
     * @param body
     * @return
     */
    @POST("/api/user/modify_recommend")
    Call<ResponseBody> modifyRecommend(@Body RequestBody body);

    /**
     * 사용자 프로필 이미지 삭제
     * @return
     */
    @POST("/api/user/delete_profile_image")
    Call<ResponseBody> deleteProfileImage();

    /**
     * 사용자 마이닝 정보
     * @return
     */
    @POST("/api/mining/info")
    Call<ResponseBody> miningInfo();

    /**
     * 출첵체크 채굴
     * @return
     */
    @POST("/api/mining/attendance_check")
    Call<ResponseBody> attendanceCheck();

    /**
     * 탈퇴하기 인증번호 확인
     * @param body
     * @return
     */
    @POST("/api/common/check_sms")
    Call<ResponseBody> checkSms(@Body RequestBody body);

    /**
     * 마이닝 쿠폰 리스트
     * @return
     */
    @POST("/api/mining/coupon_list")
    Call<ResponseBody> couponList();

    /**
     * 마이닝 쿠폰 구매 이력
     * @return
     */
    @POST("/api/mining/user_mining_coupon_purchase_list")
    Call<ResponseBody> userMiningCouponPurchaseList();

    /**
     * 쿠폰 구매 요청
     * @param body
     * @return
     */
    @POST("/api/mining/purchase_mining_coupon")
    Call<ResponseBody> purchaseMiningCoupon(@Body RequestBody body);

    /**
     * 마이닝 유저 한도 조회
     * @return
     */
    @POST("/api/mining/user_mining_limit_info")
    Call<ResponseBody> userMiningLimitInfo();

    /**
     * 마이닝 유저 적립 이력
     * @return
     */
    @POST("/api/mining/user_mining_accumulate_list")
    Call<ResponseBody> userMiningAccumulateList();

    /**
     * 마이닝 푸시 수신 설정
     * @param body
     * @return
     */
    @POST("/api/user/change_user_mining_push_flag")
    Call<ResponseBody> changeMiningPushFlag(@Body RequestBody body);

    /**
     * 마이닝 푸시 수신 설정 조회
     * @return
     */
    @POST("/api/user/select_user_mining_push_flag")
    Call<ResponseBody> userMiningPushFlag();

    /**
     * 일반계정으로 전환
     * @param body
     * @return
     */
    @POST("/api/user/change_user_join_type")
    Call<ResponseBody> changeUserJoinType(@Body RequestBody body);

    /**
     * 넥스트 기어 쿠폰 구매 여부
     * @return
     */
    @POST("/api/mining/nextgear_yn")
    Call<ResponseBody> getNextgearYn();

    /**
     * 넥스트 기어 신청 정보 조회
     * @return
     */
    @POST("/api/mining/nextgear_info")
    Call<ResponseBody> getNextGearInfo();

    /**
     * 넥스트 기어 신청
     * @param body
     * @return
     */
    @POST("/api/mining/apply_nextgear")
    Call<ResponseBody> applyNextGear(@Body RequestBody body);
}
