package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.AdMessageModel;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class SendAdMessageApi extends ApiBase<BaseModel> {

    public SendAdMessageApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String ad_title, String ad_contents, String ad_image_url, String ad_image_url2, String ad_image_url3,
                        String ad_token_amount, JSONArray ad_gender, JSONArray ad_age,
                        JSONArray ad_addr, JSONArray ad_job, String ad_send_datetime,
                        String ad_end_datetime) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);



        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("ad_title", ad_title);
            mReqeustParams.put("ad_contents", ad_contents);
            mReqeustParams.put("ad_image_url", ad_image_url);
            mReqeustParams.put("ad_image_url2", ad_image_url2);
            mReqeustParams.put("ad_image_url3", ad_image_url3);

            mReqeustParams.put("ad_token_amount", ad_token_amount);
            if(ad_gender == null) {
                mReqeustParams.put("ad_gender", "");
            } else {
                mReqeustParams.put("ad_gender", ad_gender);
            }

            if(ad_age == null) {
                mReqeustParams.put("ad_age", "");
            } else {
                mReqeustParams.put("ad_age", ad_age);
            }

            if(ad_addr == null) {
                mReqeustParams.put("ad_addr", "");
            } else {
                mReqeustParams.put("ad_addr", ad_addr);
            }

            if(ad_job == null) {
                mReqeustParams.put("ad_job", "");
            } else {
                mReqeustParams.put("ad_job", ad_job);
            }



            mReqeustParams.put("ad_send_datetime", ad_send_datetime);
            mReqeustParams.put("ad_end_datetime", ad_end_datetime);

            Log.e("@@@TAG", "data : " + mReqeustParams.toString());
            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.sendAdMessage(mReqeustBody);
    }

    @Override
    protected BaseModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    BaseModel baseModel = new BaseModel();

                    if(!responseObject.isNull("code")) {
                        baseModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        baseModel.setMessage(responseObject.getString("message"));
                    }
                    return baseModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
