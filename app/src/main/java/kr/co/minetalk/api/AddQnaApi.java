package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.QnaListImageModel;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class AddQnaApi extends ApiBase<BaseModel> {
    private final int MAX_FILE_COUNT = 3;
    public AddQnaApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(String question, String type, ArrayList<QnaListImageModel> models) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);


        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("question", question);
            mReqeustParams.put("type", type);

            for( int i = 0; i < MAX_FILE_COUNT; ++i){
                String url = "";
                if(i < models.size())
                    url = models.get(i).getImg_url();
                mReqeustParams.put(String.format("qna_image%d",i+1), url);
            }

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    public void execute(String question, String type) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);


        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("question", question);
            mReqeustParams.put("type", type);
            mReqeustParams.put("qna_image1", "");
            mReqeustParams.put("qna_image2", "");
            mReqeustParams.put("qna_image3", "");

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    public void execute(String question) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);


        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("question", question);
            mReqeustParams.put("type", "1");
            mReqeustParams.put("qna_image1", "");
            mReqeustParams.put("qna_image2", "");
            mReqeustParams.put("qna_image3", "");

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.addQnA(mReqeustBody);
    }

    @Override
    protected BaseModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    BaseModel baseModel = new BaseModel();

                    if(!responseObject.isNull("code")) {
                        baseModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        baseModel.setMessage(responseObject.getString("message"));
                    }
                    return baseModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
