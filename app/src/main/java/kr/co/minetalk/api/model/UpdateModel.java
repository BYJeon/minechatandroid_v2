package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdateModel extends BaseModel {

    public static UpdateModel parse(JSONObject jsonObject) throws JSONException {
        UpdateModel item = new UpdateModel();

        if(!jsonObject.isNull("update_idx")) {
            item.setUpdate_idx(jsonObject.getString("update_idx"));
        }

        if(!jsonObject.isNull("update_title")) {
            item.setUpdate_title(jsonObject.getString("update_title"));
        }

        if(!jsonObject.isNull("update_contents")) {
            item.setUpdate_contents(jsonObject.getString("update_contents"));
        }

        if(!jsonObject.isNull("reg_date")) {
            item.setReg_date(jsonObject.getString("reg_date"));
        }

        return item;
    }

    private String update_idx = "";
    private String update_title = "";
    private String update_contents = "";
    private String reg_date = "";


    public String getUpdate_idx() {
        return update_idx;
    }

    public void setUpdate_idx(String update_idx) {
        this.update_idx = update_idx;
    }

    public String getUpdate_title() {
        return update_title;
    }

    public void setUpdate_title(String update_title) {
        this.update_title = update_title;
    }

    public String getUpdate_contents() {
        return update_contents;
    }

    public void setUpdate_contents(String update_contents) {
        this.update_contents = update_contents;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }
}
