package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class CouponModel extends BaseModel {

    public static CouponModel parse(JSONObject jsonObject) throws JSONException {
        CouponModel faqModel = new CouponModel("","","","");

        if(!jsonObject.isNull("coupon_name")) {
            faqModel.setCoupon_name(jsonObject.getString("coupon_name"));
        }

        if(!jsonObject.isNull("reg_date")) {
            faqModel.setReg_date(jsonObject.getString("reg_date"));
        }

        if(!jsonObject.isNull("coupon_amount")) {
            faqModel.setCoupon_Amount(jsonObject.getString("coupon_amount"));
        }

        if(!jsonObject.isNull("status")) {
            faqModel.setCoupon_category(jsonObject.getString("status"));
        }

        return faqModel;
    }

    public CouponModel(String coupon_type, String coupon_category, String reg_date, String amount){
        this.coupon_name = coupon_type;
        this.reg_date    = reg_date;
        this.coupon_amount = amount;
        this.coupon_category = coupon_category;
    }

    private String coupon_name = "";
    private String reg_date = "";
    private String coupon_amount = "";
    private String coupon_category = "";

    public String getCoupon_category(){
        return coupon_category;
    }
    public void setCoupon_category(String coupon_category){this.coupon_category = coupon_category;}

    public String getCoupon_name() {
        return coupon_name;
    }
    public void setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getCoupon_amount(){return coupon_amount;}

    public void setCoupon_Amount(String coupon_amount){ this.coupon_amount = coupon_amount;}
}
