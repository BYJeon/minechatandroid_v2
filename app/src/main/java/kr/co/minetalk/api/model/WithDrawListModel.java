package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class WithDrawListModel extends BaseModel implements Serializable {

    public static WithDrawListModel parse(JSONObject jsonObject) throws JSONException {
        WithDrawListModel item = new WithDrawListModel();

        if(!jsonObject.isNull("reg_date")) {
            item.setPoint_reg_date(jsonObject.getString("reg_date"));
        }

        if(!jsonObject.isNull("from_address")) {
            item.setPoint_from_address(jsonObject.getString("from_address"));
        }

        if(!jsonObject.isNull("amount")) {
            item.setPoint_amount(jsonObject.getString("amount"));
        }

        return item;
    }

    private String point_reg_date = "";
    private String point_from_address = "";
    private String point_amount   = "";

    public void setPoint_reg_date(String point_reg_date){this.point_reg_date = point_reg_date;}
    public String getPoint_reg_date(){ return point_reg_date; }

    public void setPoint_from_address(String point_from_address){this.point_from_address = point_from_address;}
    public String getPoint_from_address(){return point_from_address;}

    public void setPoint_amount(String point_amount){this.point_amount = point_amount;}
    public String getPoint_amount(){return point_amount;}


}
