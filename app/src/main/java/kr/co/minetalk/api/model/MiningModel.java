package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class MiningModel extends BaseModel {

    public static MiningModel parse(JSONObject jsonObject) throws JSONException {
        MiningModel faqModel = new MiningModel();

        if(!jsonObject.isNull("mining_name")) {
            faqModel.setMining_name(jsonObject.getString("mining_name"));
        }

        if(!jsonObject.isNull("amount")) {
            faqModel.setAmount(jsonObject.getString("amount"));
        }

        if(!jsonObject.isNull("category")) {
            faqModel.setCategory(jsonObject.getString("category"));
        }

        if(!jsonObject.isNull("etc_data")) {
            faqModel.setEtc_data(jsonObject.getString("etc_data"));
        }

        if(!jsonObject.isNull("reg_date")) {
            faqModel.setReg_date(jsonObject.getString("reg_date"));
        }

        return faqModel;
    }



    private String mining_name = "";
    private String category = "";
    private String amount = "";
    private String etc_data = "";
    private String reg_date = "";

    public String getMining_name(){return mining_name;}
    public void setMining_name(String mining_name){this.mining_name = mining_name;}

    public String getCategory(){return category;}
    public void setCategory(String category){this.category = category;}

    public String getAmount(){return  amount;}
    public void setAmount(String amount){this.amount = amount;}

    public String getEtc_data(){return this.etc_data;}
    public void setEtc_data(String etc_data){this.etc_data = etc_data;}

    public String getReg_date(){return reg_date;}
    public void setReg_date(String reg_date){this.reg_date = reg_date;}
}
