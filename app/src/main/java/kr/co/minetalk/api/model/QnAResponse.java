package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QnAResponse extends BaseModel {

    public static QnAResponse parse(JSONObject jsonObject) throws JSONException {
        QnAResponse item = new QnAResponse();

        if(!jsonObject.isNull("data")) {
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            ArrayList<QnAModel> qnaModels = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                QnAModel qna = new QnAModel();
                qna = QnAModel.parse(jsonArray.getJSONObject(i));

                qnaModels.add(qna);
            }
            item.setData(qnaModels);
        }
        return item;
    }

    private ArrayList<QnAModel> data = new ArrayList<>();

    public ArrayList<QnAModel> getData() {
        return data;
    }

    public void setData(ArrayList<QnAModel> data) {
        this.data = data;
    }
}
