package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PointHistoryResponse extends BaseModel {

    public static PointHistoryResponse parse(JSONObject jsonObject) throws JSONException {
        PointHistoryResponse item = new PointHistoryResponse();

        if(!jsonObject.isNull("total_point")) {
            item.setTotal_point(jsonObject.getString("total_point"));
        }

        if(!jsonObject.isNull("point_history")) {
            JSONArray jsonArray = jsonObject.getJSONArray("point_history");
            ArrayList<PointHistoryModel> pointHistoryModels = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                PointHistoryModel pointHistoryModel = new PointHistoryModel();
                pointHistoryModel = PointHistoryModel.parse(jsonArray.getJSONObject(i));
                pointHistoryModels.add(pointHistoryModel);
            }
            item.setPoint_history(pointHistoryModels);
        }

        return item;
    }


    private String total_point = "";
    private ArrayList<PointHistoryModel> point_history = new ArrayList<>();

    public String getTotal_point() {
        return total_point;
    }

    public void setTotal_point(String total_point) {
        this.total_point = total_point;
    }

    public ArrayList<PointHistoryModel> getPoint_history() {
        return point_history;
    }

    public void setPoint_history(ArrayList<PointHistoryModel> point_history) {
        this.point_history = point_history;
    }
}
