package kr.co.minetalk.api.model;

import android.util.Log;

import kr.co.minetalk.utils.Preferences;

/**
 * Created by episode on 2018. 1. 12..
 */

public class NumberCodeData extends BaseModel {

    public NumberCodeData(String name, String numCode) {
        this.name = name;
        this.numCode = numCode;
        this.listTitle = name;
    }

    public NumberCodeData(String name, String numCode, String listTitleEng) {
        this.name = name;
        this.numCode = numCode;
        this.listTitle = name;
        this.listTitleEng = listTitleEng;
    }

    private String name = "";
    private String numCode = "";
    private String listTitle = "";
    private String listTitleEng = "";

    public String getName() {
        String languageCode = Preferences.getLanguage();
        if(languageCode != null && languageCode.equals("ko")) {
            Log.e("@@@TAG","lang : ko : " + name);
            return name;
        } else {
            Log.e("@@@TAG","lang : other : " + listTitleEng);
            return listTitleEng;
        }

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumCode() {
        return numCode;
    }

    public void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    public String getListTitle() {
        return listTitle;
    }

    public void setListTitle(String listTitle) {
        this.listTitle = listTitle;
    }

    public String getListTitleEng() {
        return listTitleEng;
    }

    public void setListTitleEng(String listTitleEng) {
        this.listTitleEng = listTitleEng;
    }
}
