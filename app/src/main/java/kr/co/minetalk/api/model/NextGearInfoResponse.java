package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class NextGearInfoResponse extends BaseModel {

    public static NextGearInfoResponse parse(JSONObject jsonObject) throws JSONException {
        NextGearInfoResponse item = new NextGearInfoResponse();

        if(!jsonObject.isNull("user_login_id")) {
            item.setUser_login_id(jsonObject.getString("user_login_id"));
        }

        if(!jsonObject.isNull("user_name")) {
            item.setUser_name(jsonObject.getString("user_name"));
        }

        if(!jsonObject.isNull("coupon_name")) {
            item.setCoupon_name(jsonObject.getString("coupon_name"));
        }

        if(!jsonObject.isNull("zipcode")) {
            item.setZipcode(jsonObject.getString("zipcode"));
        }

        if(!jsonObject.isNull("addr")) {
            item.setAddr(jsonObject.getString("addr"));
        }

        if(!jsonObject.isNull("addr_detail")) {
            item.setAddr_detail(jsonObject.getString("addr_detail"));
        }

        if(!jsonObject.isNull("customs_clearance_number")) {
            item.setCustoms_clearance_number(jsonObject.getString("customs_clearance_number"));
        }

        if(!jsonObject.isNull("affiliate_code")) {
            item.setAffiliate_code(jsonObject.getString("affiliate_code"));
        }

        if(!jsonObject.isNull("status")) {
            item.setStatus(jsonObject.getString("status"));
        }

        if(!jsonObject.isNull("system_number")) {
            item.setSystem_number(jsonObject.getString("system_number"));
        }

        if(!jsonObject.isNull("user_number")) {
            item.setUser_number(jsonObject.getString("user_number"));
        }

        if(!jsonObject.isNull("minimum_coupon_name")) {
            item.setMinimum_coupon_name(jsonObject.getString("minimum_coupon_name"));
        }

        if(!jsonObject.isNull("expire_date")) {
            item.setExpire_date(jsonObject.getString("expire_date"));
        }

        return item;
    }

    private String user_login_id = "";
    private String user_name = "";
    private String coupon_name = "";
    private String zipcode = "";
    private String addr = "";
    private String addr_detail = "";
    private String customs_clearance_number = "";
    private String affiliate_code = "";
    private String status = "";
    private String system_number = "";
    private String user_number = "";
    private String minimum_coupon_name = "";
    private String expire_date = "";

    public String getUser_login_id(){return user_login_id;}
    public void setUser_login_id(String user_login_id){this.user_login_id = user_login_id;}

    public String getUser_name(){return user_name;}
    public void setUser_name(String user_name){this.user_name = user_name;}

    public String getCoupon_name(){return coupon_name;};
    public void setCoupon_name(String coupon_name){this.coupon_name = coupon_name;}

    public String getZipcode(){return zipcode;}
    public void setZipcode(String zipcode){this.zipcode = zipcode;}

    public String getAddr(){return addr;}
    public void setAddr(String addr){this.addr = addr;}

    public String getAddr_detail(){return addr_detail;}
    public void setAddr_detail(String addr_detail){this.addr_detail = addr_detail;}

    public String getCustoms_clearance_number(){return customs_clearance_number;}
    public void setCustoms_clearance_number(String customs_clearance_number){this.customs_clearance_number = customs_clearance_number;}

    public String getAffiliate_code(){return affiliate_code;}
    public void setAffiliate_code(String affiliate_code){this.affiliate_code = affiliate_code;}

    public String getStatus(){return status;}
    public void setStatus(String status){this.status = status;}

    public String getSystem_number(){return system_number;}
    public void setSystem_number(String system_number){this.system_number = system_number;}

    public String getUser_number(){return user_number;}
    public void setUser_number(String user_number){this.user_number = user_number;}

    public String getMinimum_coupon_name(){return minimum_coupon_name;}
    public void setMinimum_coupon_name(String minimum_coupon_name){this.minimum_coupon_name=minimum_coupon_name;}

    public String getExpire_date(){return expire_date;}
    public void setExpire_date(String expire_date){this.expire_date = expire_date;}
}
