package kr.co.minetalk.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RecommendResponse extends BaseModel {

    public static RecommendResponse parse(JSONObject jsonObject) throws JSONException {
        RecommendResponse item = new RecommendResponse();

        if(!jsonObject.isNull("data")) {
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            ArrayList<RecommendModel> recommendModelArrayList = new ArrayList<>();
            for(int i = 0 ; i < jsonArray.length() ; i++) {
                RecommendModel recommendModel = new RecommendModel();
                recommendModel = RecommendModel.parse(jsonArray.getJSONObject(i));

                recommendModelArrayList.add(recommendModel);
            }
            item.setData(recommendModelArrayList);
        }
        return item;
    }

    private ArrayList<RecommendModel> data = new ArrayList<>();

    public ArrayList<RecommendModel> getData() {
        return data;
    }

    public void setData(ArrayList<RecommendModel> data) {
        this.data = data;
    }
}
