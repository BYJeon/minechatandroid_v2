package kr.co.minetalk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class MiningPushModel extends BaseModel {

    public static MiningPushModel parse(JSONObject jsonObject) throws JSONException {
        MiningPushModel faqModel = new MiningPushModel();

        if(!jsonObject.isNull("user_mining_push_flag")) {
            faqModel.setPushYn(jsonObject.getString("user_mining_push_flag"));
        }

        return faqModel;
    }


    private String push_yn = "";

    private void setPushYn(String push_yn){
        this.push_yn = push_yn;
    }

    public String getPushYn(){return this.push_yn;}
}
