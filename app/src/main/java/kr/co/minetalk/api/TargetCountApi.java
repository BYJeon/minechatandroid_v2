package kr.co.minetalk.api;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.api.model.AdMessageModel;
import kr.co.minetalk.api.model.TargetMessageModel;
import kr.co.minetalk.utils.Preferences;
import retrofit.Call;

public class TargetCountApi extends ApiBase<TargetMessageModel> {

    public TargetCountApi(Activity activity, ApiCallBack apiCallBack) {
        super(activity, kr.co.idevbank.base.api.ApiBase.QUEUE_TYPE_ONCE_FIRST, apiCallBack);
    }

    @Override
    protected String getBaseUrl() {
        return MineTalk.getServer(MineTalk.ServerType.API);
    }

    @Override
    protected boolean isFailParams() {
        return false;
    }

    public void execute(JSONArray ad_gender, JSONArray ad_age, JSONArray ad_addr, JSONArray ad_job) {
        String authorization = Preferences.getAuthorization();
        String x_login_type  = Preferences.getLoginType();
        String x_lang        = Preferences.getLanguage().toLowerCase();
        setHeader(authorization, x_login_type, x_lang);

        try {
            mReqeustParams = new JSONObject();
            mReqeustParams.put("ad_gender", ad_gender);
            mReqeustParams.put("ad_age", ad_age);
            mReqeustParams.put("ad_addr", ad_addr);
            mReqeustParams.put("ad_job", ad_job);

            Log.e("@@@TAG", "target : " + mReqeustParams.toString());

            mReqeustBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), mReqeustParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.apiExecute();
    }

    @Override
    protected Call<ResponseBody> createCall(ApiInterface apiInterface) {
        return apiInterface.targetCount(mReqeustBody);
    }

    @Override
    protected TargetMessageModel parseResult(Context context, int request_code, String response_str) throws JSONException {
        if(request_code == REQUEST_CODE_SUCCESS) {
            if(response_str != null && !response_str.equals("")) {
                try {
                    JSONObject responseObject = new JSONObject(response_str);
                    TargetMessageModel targetMessageModel = new TargetMessageModel();
                    if(!responseObject.isNull("data")) {
                        targetMessageModel = TargetMessageModel.parse(responseObject.getJSONObject("data"));
                    }
                    if(!responseObject.isNull("code")) {
                        targetMessageModel.setCode(responseObject.getString("code"));
                    }
                    if(!responseObject.isNull("message")) {
                        targetMessageModel.setMessage(responseObject.getString("message"));
                    }
                    return targetMessageModel;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
