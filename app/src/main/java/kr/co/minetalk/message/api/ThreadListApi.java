package kr.co.minetalk.message.api;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;

import kr.co.minetalk.message.model.ThreadData;

import java.util.List;

public class ThreadListApi extends BaseHttpRequest<List<ThreadData>> {
    private static final String PATH = "/members/%s/threads/all";

    public ThreadListApi(Context context, String xid) {
        super(context, PATH, new String[]{xid}, "GET");

        this.registTypeRef(new TypeReference<List<ThreadData>>() {
        });
    }

}
