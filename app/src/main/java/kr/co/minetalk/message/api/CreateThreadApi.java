package kr.co.minetalk.message.api;

import android.content.Context;

import java.util.HashMap;

import kr.co.minetalk.message.model.CreateThreadData;
import kr.co.minetalk.message.model.ThreadData;

public class CreateThreadApi extends BaseHttpRequest<ThreadData> {
    private static final String PATH = "/threads/direct";

    public CreateThreadApi(Context context, final String xid, final String withXid) {
        super(context, PATH, "POST");

        this.reqObject = new HashMap<String, Object>() {{
            put("xid", xid);
            put("withXid", withXid);
            put("type", 1);
        }};
        this.registClassType(ThreadData.class);
    }
}
