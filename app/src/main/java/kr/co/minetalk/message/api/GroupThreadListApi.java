package kr.co.minetalk.message.api;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;

import kr.co.minetalk.message.model.GroupThreadData;

import java.util.List;

public class GroupThreadListApi extends BaseHttpRequest<List<GroupThreadData>> {
    private static final String PATH = "/threads/groups";

    public GroupThreadListApi(Context context) {
        super(context, PATH, "GET");

        this.registTypeRef(new TypeReference<List<GroupThreadData>>() {
        });
    }
}
