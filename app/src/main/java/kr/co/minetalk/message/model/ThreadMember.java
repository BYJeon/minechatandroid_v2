package kr.co.minetalk.message.model;

import java.util.Date;

/**
 * 스레드 참여자 상세 정보
 */
public class ThreadMember {
    /**
     * 사용자 아이디
     */
    private String xid;
    /**
     * 참여일
     */
    private Date joinDate;
    /**
     * 읽은 메시지 시퀀스
     */
    private Integer readSeq;
    /**
     * 스레드 생성자 여부
     */
    private String isOwner;
    /**
     * 닉네임
     */
    private String nickName;
    /**
     * 프로필 이미지 URL
     */
    private String profileImageUrl;
    /**
     * 추가 정보
     */
    private String extraValue;

    private String alarm = "Y";

    private String userHp;

    private String userNationCode;

    public String getUserHp() {
        return userHp;
    }

    public void setUserHp(String userHp) {
        this.userHp = userHp;
    }

    public String getUserNationCode() {
        return userNationCode;
    }

    public void setUserNationCode(String userNationCode) {
        this.userNationCode = userNationCode;
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public Integer getReadSeq() {
        return readSeq;
    }

    public void setReadSeq(Integer readSeq) {
        this.readSeq = readSeq;
    }

    public String getIsOwner() {
        return isOwner;
    }

    public void setIsOwner(String isOwner) {
        this.isOwner = isOwner;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getExtraValue() {
        return extraValue;
    }

    public void setExtraValue(String extraValue) {
        this.extraValue = extraValue;
    }

    public String getAlarm() {
        return alarm;
    }

    public void setAlarm(String alarm) {
        this.alarm = alarm;
    }
}
