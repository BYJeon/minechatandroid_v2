package kr.co.minetalk.message.api;

import android.content.Context;

import kr.co.minetalk.message.model.ThreadWithChannel;

public class ThreadChannelApi extends BaseHttpRequest<ThreadWithChannel> {
    private static final String PATH = "/threads/%s/channel";

    public ThreadChannelApi(Context context, String threadKey) {
        super(context, PATH, new String[]{threadKey}, "GET");
        this.registClassType(ThreadWithChannel.class);
    }
}
