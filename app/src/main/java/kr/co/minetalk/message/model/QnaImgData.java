package kr.co.minetalk.message.model;

public class QnaImgData {
    private String path;
    private float size;

    public QnaImgData() {
    }

    public QnaImgData(String path, float size) {
        this.path = path;
        this.size = size;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public float getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
