package kr.co.minetalk.message.api;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.core.type.TypeReference;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import kr.co.minetalk.message.MessageConst;

import java.util.HashMap;
import java.util.Set;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public abstract class BaseHttpRequest<T> {
    private final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
//    private String baseApiHost = MessageConst.REST_API_HOST;
    private String baseApiHost = MessageConst.getChatRestApi();
    private String apiUrl;

    private final Context context;
    private final String method;
    private final HashMap<String, String> headers;
    private final RequestParams parameters;
    private TypeReference<T> typeReference;
    private Class<T> cls;

    protected Object reqObject = null;


    public BaseHttpRequest(Context context, String apiPathFormat, String[] pathValues, String method) {
        if(pathValues == null) {
            pathValues = new String[]{""};
        }
        this.context = context;
        this.apiUrl = this.baseApiHost + String.format(apiPathFormat, pathValues);
        this.headers = new HashMap<>();
        this.parameters = new RequestParams();
        this.method = method;

    }

    public BaseHttpRequest(Context context, String apiPath, String method) {
        this.context = context;
        this.apiUrl = this.baseApiHost + apiPath;
        this.headers = new HashMap<>();
        this.parameters = new RequestParams();
        this.method = method;
    }

    public BaseHttpRequest(Context context, String method) {
        this.context = context;
        this.apiUrl = MessageConst.getServerLiveRestApi();
        this.headers = new HashMap<>();
        this.parameters = new RequestParams();
        this.method = method;
    }

    public void registTypeRef(TypeReference<T> typeReference) {
        this.typeReference = typeReference;
    }
    public void registClassType(Class<T> cls) {
        this.cls = cls;
    }
    public void addHeader(String name, String value) {
        this.headers.put(name, value);
    }
    public void addParameter(String name, String value) {
        this.parameters.put(name, value);
    }

    public void request(final APICallbackListener<T> listener) {
        AsyncHttpClient client = new AsyncHttpClient();
        Log.e("@@@@@TAG", "req : " + apiUrl);
        if(!headers.isEmpty()) {
            Set<String> keys = headers.keySet();
            for (String key : keys) {
                client.addHeader(key, headers.get(key));
            }
        }

        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                try {
                    String res = new String(responseBody);
                    Log.e("@@@@@TAG","res : " + res);
                    if(BaseHttpRequest.this.typeReference != null) {
                        T obj = OBJECT_MAPPER.readValue(responseBody, BaseHttpRequest.this.typeReference);
                        listener.onSuccess(obj);
                    }
                    else if(BaseHttpRequest.this.cls != null) {
                        T obj = OBJECT_MAPPER.readValue(responseBody, BaseHttpRequest.this.cls);
                        listener.onSuccess(obj);
                    }
                    else {
                        listener.onSuccess(null);
                    }
                }
                catch (Exception e) {
                    listener.onFailure(statusCode, responseBody, e);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Regist Fail
                listener.onFailure(statusCode, responseBody, error);
            }
        };

        StringEntity reqBody = null;

        if(reqObject != null) {
            try {
                reqBody = new StringEntity(OBJECT_MAPPER.writeValueAsString(reqObject));
            }
            catch (Exception e) {}
        }

        if(this.method.toUpperCase().equals("GET")) {

            client.get(this.context, apiUrl, handler);
        }
        else if(this.method.toUpperCase().equals("DELETE")) {

            client.delete(this.context, apiUrl, handler);
        }
        else if(this.method.toUpperCase().equals("POST")) {

            if(reqBody != null) {
                client.post(this.context, apiUrl, reqBody, "application/json", handler);
            }
            else {
                client.post(this.context, apiUrl, this.parameters, handler);
            }
        }
        else if(this.method.toUpperCase().equals("PUT")) {

            if(reqBody != null) {
                client.put(this.context, apiUrl, reqBody, "application/json", handler);
            }
            else {
                client.put(this.context, apiUrl, this.parameters, handler);
            }
        }
    }

    public interface APICallbackListener<T> {
        void onSuccess(T res);
        void onFailure(int statusCode, byte[] responseBody, Throwable error);
    }
}
