package kr.co.minetalk.message.model;

public class ServerLiveData {
    private String server_active;
    private String check_time;

    public String getServer_active() {
        return server_active;
    }

    public void setServer_active(String server_active){
        this.server_active = server_active;
    }

    public String getCheck_time(){
        return check_time;
    }

    public void setCheck_time(String check_time){
        this.check_time = check_time;
    }
}
