package kr.co.minetalk.message.api;

import android.content.Context;

import kr.co.minetalk.message.model.ServerLiveData;


public class ServerLiveApi extends BaseHttpRequest<ServerLiveData> {

    public ServerLiveApi(Context context) {
        super(context, "GET");
        this.registClassType(ServerLiveData.class);
    }
}
