package kr.co.minetalk.message.util;

import java.text.DecimalFormat;

/**
 * Created by Taeun on 14. 12. 30..
 */
public class StringUtil {

    public static String getPreviewText(String value) {

        if(20 < value.length()) {
            return value.substring(0, 20) + "...";
        } else {
            return value;
        }
    }

    public static String readableFileSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/ Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/ Math.pow(1024, digitGroups)) + units[digitGroups];
    }
}
