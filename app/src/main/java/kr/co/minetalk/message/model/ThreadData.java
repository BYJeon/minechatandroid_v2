package kr.co.minetalk.message.model;

import java.util.Date;
import java.util.List;

/**
 * 스레드 상세 정보
 */
public class ThreadData {
    /**
     * 고유 스레드 키
     */
    private String threadKey;
    /**
     * 최종 메시지 시퀀스 번호
     */
    private Integer lastSeq;
    /**
     * 참여중인 사용자 수
     */
    private Integer joinMemberCount;
    /**
     * 등록일
     */
    private Date regDate;
    /**
     * 최종 업데이트 일
     */
    private Date updateDate;
    /**
     * 등록된 메시지 수
     */
    private Integer messageCount;

    private String alias;

    private String isDirect;
    /**
     * 스레드 참여중인 사용자 리스트
     */
    private List<ThreadMember> members;
    /**
     * 스레드 최종 메시지 ( preview )
     */
    private String lastMessage;

    /**
     * 스레드 정보 요청자의 안읽은 메시지 수
     */
    private int unReadCount;

    private String alarm;

    private int threadType;


    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public int getUnReadCount() {
        return unReadCount;
    }

    public void setUnReadCount(int unReadCount) {
        this.unReadCount = unReadCount;
    }

    public List<ThreadMember> getMembers() {
        return members;
    }

    public void setMembers(List<ThreadMember> members) {
        this.members = members;
    }

    public String getThreadKey() {
        return threadKey;
    }

    public void setThreadKey(String threadKey) {
        this.threadKey = threadKey;
    }

    public Integer getLastSeq() {
        return lastSeq;
    }

    public void setLastSeq(Integer lastSeq) {
        this.lastSeq = lastSeq;
    }

    public Integer getJoinMemberCount() {
        return joinMemberCount;
    }

    public void setJoinMemberCount(Integer joinMemberCount) {
        this.joinMemberCount = joinMemberCount;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(Integer messageCount) {
        this.messageCount = messageCount;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getIsDirect() {
        return isDirect;
    }

    public void setIsDirect(String isDirect) {
        this.isDirect = isDirect;
    }

    public String getAlarm() {
        return alarm;
    }

    public void setAlarm(String alarm) {
        this.alarm = alarm;
    }

    public int getThreadType() {
        return threadType;
    }

    public void setThreadType(int threadType) {
        this.threadType = threadType;
    }
}
