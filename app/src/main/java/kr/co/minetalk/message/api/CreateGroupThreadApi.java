package kr.co.minetalk.message.api;

import android.content.Context;

import java.util.HashMap;

import kr.co.minetalk.message.model.ThreadData;

public class CreateGroupThreadApi extends BaseHttpRequest<ThreadData> {
    private static final String PATH = "/threads/group";

    public CreateGroupThreadApi(Context context, final String xid, final String[] withXids) {
        super(context, PATH, "POST");

        this.reqObject = new HashMap<String, Object>() {{
            put("xid", xid);
            put("withXids", withXids);
            put("type", 1);
        }};
        this.registClassType(ThreadData.class);
    }


}
