package kr.co.minetalk.message.manager;

import android.util.Log;

import kr.co.minetalk.message.util.ByteUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * Created by Taeun on 15. 1. 13..
 */
public class SocketContext {

    private Socket mSocket;
    private BufferedInputStream mInputStream;
    private BufferedOutputStream mOutputStream;
    private ConnectStateListener connectStateListener;


    /**
     * 소켓 연결
     * @param ip 아이피
     * @param port 포트
     * @return 성공시 true, 실패시 false
     */
    public boolean connect(final String ip, final int port) throws IllegalStateException {

        if(null == ip || ip.isEmpty() || 0 == port) {
            throw new IllegalStateException();
        }

        try {
            mSocket = new Socket(ip, port);

            // 소켓 옵션 설정
            mSocket.setTcpNoDelay(true);
            mSocket.setKeepAlive(true);

            mInputStream = new BufferedInputStream(mSocket.getInputStream());
            mOutputStream = new BufferedOutputStream(mSocket.getOutputStream());

            if(connectStateListener != null) {
                connectStateListener.onConnected();
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 소켓 연결을 종료한다.
     */
    public void disconnect() {

        try { if(null != mInputStream) { mInputStream.close(); }} catch(Exception e) { e.printStackTrace(); }
        try { if(null != mOutputStream) { mOutputStream.close(); }} catch(Exception e) { e.printStackTrace(); }
        try { if(null != mSocket) { mSocket.close(); mSocket = null;}} catch(Exception e) { e.printStackTrace();}

        if(connectStateListener != null) {
            connectStateListener.onDisconnected();
        }
    }

    /**
     *
     */
    public void sendTimeoutException() {

        if(connectStateListener != null) {
            connectStateListener.onSendTimeout();
        }
    }

    /**
     * 현재 소켓 연결 상태를 반환한다.
     * @return 연결시 true, 연결되지 않았을 경우 false
     */
    public boolean isConneted() {
        if(null == mSocket) {
            return false;
        }
        return mSocket.isConnected();
    }

    /**
     * 버퍼를 전송한다.
     * @param buffer 보내는 데이터
     */
    public void send(byte[] buffer) {

        try {

            ByteBuffer dataBuf = ByteBuffer.allocate(buffer.length + 4);
            dataBuf.put(ByteUtil.intToByteArray(buffer.length));
            dataBuf.put(buffer);

            mOutputStream.write(dataBuf.array());
            mOutputStream.flush();

            dataBuf.clear();
        }
        catch (IOException se) {
            Log.e("@@@TAG", "send Exception");
            disconnect(); // Socket Pipe Error
        }
    }

    public void setConnectStateListener(ConnectStateListener listener) {
        this.connectStateListener = listener;
    }

    /**
     * 전송받은 데이터를 buffer에 담고 받은 길이를 리턴한다.
     * @return 전송받은 길이
     */
    public int receive(byte[] buffer) throws IOException {

        return mInputStream.read(buffer);
    }

    public static interface ConnectStateListener {
        void onConnected();
        void onDisconnected();
        void onSendTimeout();
    }
}

