package kr.co.minetalk.message.api;

import android.content.Context;

import java.util.HashMap;

import kr.co.minetalk.message.model.ThreadData;

public class InviteGroupThreadApi extends BaseHttpRequest<Void> {
    private static final String PATH = "/threads/invite";

    public InviteGroupThreadApi(Context context, final String xid, final String[] withXids, final String threadKey) {
        super(context, PATH, "POST");

        this.reqObject = new HashMap<String, Object>() {{
            put("xid", xid);
            put("inviteXid", withXids);
            put("threadKey", threadKey);

        }};
//        this.registClassType(ThreadData.class);
    }


}
