package kr.co.minetalk.message.api;

import android.content.Context;

import kr.co.minetalk.message.model.ThreadData;

import java.util.HashMap;
import java.util.List;

public class NewThreadListApi extends BaseHttpRequest<List<ThreadData>> {
    private static final String PATH = "/members/%s/threads/new";

    public NewThreadListApi(Context context, String xid) {
        super(context, PATH, new String[]{xid}, "GET");
    }
}
