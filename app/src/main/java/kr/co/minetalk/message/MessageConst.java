package kr.co.minetalk.message;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;

public class MessageConst {
    public final static String REST_API_LIVE_HOST = "https://minechat.s3.ap-northeast-2.amazonaws.com/app/server.json";
    public final static String REST_API_HOST = "http://chat.minetalk.co.kr:8888";
    public final static String REST_API_HOST_DEV = "http://tchat.minetalk.co.kr:8888";

    public final static int SEND_STATE_COMPLETE = 1;
    public final static int SEND_STATE_SENDING  = 2;
    public final static int SEND_STATE_FAIL     = 3;

    public static String getChatRestApi() {
        if(MineTalk.isDebug) {
            return REST_API_HOST_DEV;
        } else {
            return REST_API_HOST;
        }
    }

    public static String getServerLiveRestApi() {
        if(MineTalk.isDebug) {
            return REST_API_LIVE_HOST;
        } else {
            return REST_API_LIVE_HOST;
        }
    }
}
