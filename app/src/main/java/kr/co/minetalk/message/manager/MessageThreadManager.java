package kr.co.minetalk.message.manager;

import android.content.Context;

import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.NewThreadListApi;
import kr.co.minetalk.message.api.ThreadListApi;
import kr.co.minetalk.message.db.MessageDbHelper;
import kr.co.minetalk.message.model.MessageData;
import kr.co.minetalk.message.model.ThreadData;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MessageThreadManager {
    private static volatile MessageThreadManager singletonInstance = null;

    public static MessageThreadManager getInstance() {
        if(singletonInstance == null) {
            throw new NullPointerException("manager init error");
        }

        return singletonInstance;
    }

    public static MessageThreadManager init(Context context) {
        if(singletonInstance==null) {
            synchronized (MessageThreadManager.class) {
                if (singletonInstance == null) {
                    context = context.getApplicationContext();
                    singletonInstance = new MessageThreadManager(context);
                }
            }
        }

        return singletonInstance;
    }

    private final MessageDbHelper messageDbHelper;

    public MessageThreadManager(Context context) {
        messageDbHelper = new MessageDbHelper(context);
    }

    public void release() {
        messageDbHelper.close();
    }

    public void removeThread(Context context, final String threadKey) {
        ThreadData threadData = messageDbHelper.getThreadData(threadKey);

        if(threadData.getIsDirect().equals("Y")) {
            messageDbHelper.removeThread(threadKey);
        } else { // Out Thread
            // TODO :: API 연동
        }
    }

    public void removeThreadAll() {
        messageDbHelper.removeThreadAll();
    }

    public void saveThread(Context context, String xid, ThreadData threadData) {
        messageDbHelper.saveThreadData(threadData, xid);
    }

    public void updateThreadSeq(Context context, String threadKey, int seq) {
        messageDbHelper.updateThreadLastSeq(threadKey, seq);
    }

    public void updateThreadSeq(Context context, String threadKey, int seq, String lastMessage) {
        messageDbHelper.updateThreadNewMessage(threadKey, lastMessage, seq);
    }

    public void updateThreadMemberReadSeq(Context context, String threadKey, String xid, int readSeq) {
        messageDbHelper.updateThreadMemberReadSeq(threadKey, xid, readSeq);
    }

    public void updateSendMessageSeq(Context context, int dbIndex, int seq) {
        messageDbHelper.updateMessageStatus(dbIndex, seq);
    }

    public void updateMsgTranslation(Context context, String threadKey, int idx, String transJson) {
        messageDbHelper.updateMessageTranslation(threadKey, idx, transJson);
    }

    public void updateMessageDelete(Context context, String threadKey, int idx, String msg) {
        messageDbHelper.updateMessageDelete(msg, threadKey, idx);
    }


    public MessageData getMessageBySeq(Context context, String threadKey, int seq) {
        return messageDbHelper.getMessageDataBySeq(threadKey, seq);
    }

    public List<MessageData> getLocalMessages(Context context, String threadKey, int seq) {
        return messageDbHelper.loadMessageData(threadKey, seq);
    }

    public void appendMessage(Context context, String threadKey, MessageData message) {
        messageDbHelper.appendMessage(threadKey, message);
    }

    public void appendMessages(Context context, String threadKey, List<MessageData> messages) {
        messageDbHelper.appendMessages(threadKey, messages);
    }

    public long addSendMessage(Context context, String threadKey, MessageData message) {
        return messageDbHelper.addSendMessage(threadKey, message);
    }

    public MessageData getLastMessage(Context context, String threadKey) {
        return messageDbHelper.getLastMessageData(threadKey);
    }

    public void updateMessageData(Context context, String threadKey, int seq, String message) {
        messageDbHelper.updateThreadLastMessage(threadKey, seq, message);
    }

    public void deleteMessage(Context context, String threadKey, int seq) {
        messageDbHelper.removeMessageData(threadKey, seq);
    }

    public void exitThread(Context context, String threadKey) {
        messageDbHelper.removeThread(threadKey);
    }

    public void loadThread(Context context, final String xid, final BaseHttpRequest.APICallbackListener<List<ThreadData>> callback) {
        try {
            final List<ThreadData> threads = messageDbHelper.loadThreadData();

            BaseHttpRequest.APICallbackListener handler = new BaseHttpRequest.APICallbackListener<List<ThreadData>>() {
                @Override
                public void onSuccess(List<ThreadData> res) {
                    for (ThreadData data : res) {
                        messageDbHelper.saveThreadData(data, xid);

                        int idx = threads.indexOf(data);
                        if(idx != -1)
                            threads.remove(idx);
                    }

                    threads.addAll(res);

                    Collections.sort(threads, new Comparator<ThreadData>() {
                        @Override
                        public int compare(ThreadData t1, ThreadData t2) {
                            return t2.getUpdateDate().compareTo(t1.getUpdateDate());
                        }
                    });

                    callback.onSuccess(threads);
                }

                @Override
                public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                    callback.onFailure(statusCode, responseBody, error);
                }
            };

            if(threads.size() > 0) {
                NewThreadListApi api = new NewThreadListApi(context, xid);
                api.request(handler);
            }
            else {
                ThreadListApi api = new ThreadListApi(context, xid);
                api.request(handler);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            callback.onFailure(999, null, e);
        }
    }
}
