package kr.co.minetalk.message.api;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;

import kr.co.minetalk.message.model.ThreadMember;

import java.util.List;

public class ThreadMemberApi extends BaseHttpRequest<List<ThreadMember>> {
    private static final String PATH = "/threads/%s/members";

    public ThreadMemberApi(Context context, String threadKey) {
        super(context, PATH, new String[]{threadKey}, "GET");

        this.registTypeRef(new TypeReference<List<ThreadMember>>() {
        });
    }
}
