package kr.co.minetalk.message.api;

import android.content.Context;

import java.util.HashMap;

import kr.co.minetalk.message.model.ThreadData;

public class KickThreadApi extends BaseHttpRequest<Void> {
    private static final String PATH = "/threads/kick";

    public KickThreadApi(Context context, final String threadKey, final String xid, final String kickXid) {
        super(context, PATH, "POST");

        this.reqObject = new HashMap<String, Object>() {{
            put("threadKey", threadKey);
            put("xid", kickXid);
            put("kickXid", xid);
            //put("type", 1);
        }};
    }


}
