package kr.co.minetalk.message.fcm;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;


public class MessagingService extends FirebaseMessagingService {

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

//        remoteMessage.getNotification().getSound()
//        I/FCM: DATA :: {threadKey=123123, badge=1, seqNo=1}

        Map<String, String> data = remoteMessage.getData();
//        String title = data.get("title");
//        String message = data.get("message");

        Log.i("FCM", "DATA :: " + data);

//        Intent pushIntent = new Intent(this, MainActivity.class);
//        pushIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        PendingIntent pendingIntent =
//                PendingIntent.getActivity(this, 0, pushIntent, PendingIntent.FLAG_CANCEL_CURRENT);
//
//        Notification.Builder builder = new Notification.Builder(this)
//                .setWhen(System.currentTimeMillis())
//                .setTicker("새로운 메시지가 도착하였습니다.")
//                .setContentText(message)
//                .setContentIntent(pendingIntent)
//                .setAutoCancel(true);
//
//        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
//        PowerManager.WakeLock wakelock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
//        wakelock.acquire(5000);
//
//        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//        notificationManager.notify(0, builder.build());
    }


}
