package kr.co.minetalk.message.api;

import android.content.Context;

import java.util.HashMap;

public class OutThreadApi extends BaseHttpRequest<Void> {
    private static final String PATH = "/threads/out";

    public OutThreadApi(Context context, final String xid, final String threadKey) {
        super(context, PATH, "POST");

        this.reqObject = new HashMap<String, Object>() {{
            put("xid", xid);
            put("threadKey", threadKey);
        }};
    }
}
