package kr.co.minetalk.message.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.co.minetalk.message.MessageConst;

import java.util.Date;

/**
 * thread_message Entity 모델
 */
public class MessageData {
    /**
     * 메시지 시퀀스
     */
    private Integer seq;
    /**
     * 메시지 작성자 아이디
     */
    private String xid;
    /**
     * 메시지 타입(1:사용자 메시지, 2: 시스템 메시지(대화방입장퇴장))
     */
    private Short type;

    /**
     * 사용자 메시지 카테고리
     */
    private Integer category = 0;
    /**
     * 메시지 Body
     */
    private String message;
    /**
     * 메시지 발송일
     */
    private Date sendDate;

    private String nickName;

    /**
     * 내 메시지 중
     * 상대방이 읽지 않은 개수
     */
    private Integer unreadCount = 1;

    /**
     * Delete Flag
     */
    public String deleteFlag;

    @JsonIgnore
    private String tran = "";


    @JsonIgnore
    private Integer dbIndex;

    @JsonIgnore
    private Integer state = MessageConst.SEND_STATE_COMPLETE;

    public String getDeleteFlag(){
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setUnreadCount(Integer unreadCount) {
        this.unreadCount = unreadCount;
    }


    public Integer getUnreadCount() {
        return unreadCount;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Integer getDbIndex() {
        return dbIndex;
    }

    public void setDbIndex(Integer dbIndex) {
        this.dbIndex = dbIndex;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getTran() {
        return tran;
    }

    public void setTran(String tran) {
        this.tran = tran;
    }
}
