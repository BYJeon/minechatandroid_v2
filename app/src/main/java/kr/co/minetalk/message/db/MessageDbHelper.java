package kr.co.minetalk.message.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import kr.co.minetalk.message.MessageConst;
import kr.co.minetalk.message.model.MessageData;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageDbHelper extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 7;

    // Database Name
    private static final String DATABASE_NAME = "message_db";

    private static final String TABLE_THREAD_NAME = "THREAD_TABLE";
    private static final String TABLE_THREAD_MEMBER_NAME = "THREAD_MEMBER_TABLE";
    private static final String TABLE_MESSAGE_NAME = "MESSAGE_TABLE";

    private static final String KEY_THREAD_THREAD_KEY           = "THREAD_KEY";
    private static final String KEY_THREAD_JOIN_MEMBER_COUNT    = "JOIN_MEMBER_COUNT";
    private static final String KEY_THREAD_UNREAD_COUNT         = "UNREAD_COUNT";
    private static final String KEY_THREAD_IS_DIRECT            = "IS_DIRECT";
    private static final String KEY_THREAD_LAST_MESSAGE         = "LAST_MESSAGE";
    private static final String KEY_THREAD_REG_DATE             = "REG_DATE";
    private static final String KEY_THREAD_UPDATE_DATE          = "UPDATE_DATE";
    private static final String KEY_THREAD_ALARM                = "ALARM";
    private static final String KEY_THREAD_LAST_SEQ             = "LAST_SEQ";
    private static final String KEY_THREAD_ALIAS                = "ALIAS";

    private static final String KEY_MEMBER_THREAD_KEY           = "THREAD_KEY";
    private static final String KEY_MEMBER_XID                  = "XID";
    private static final String KEY_MEMBER_JOIN_DATE            = "JOIN_DATE";
    private static final String KEY_MEMBER_READ_SEQ             = "READ_SEQ";
    private static final String KEY_MEMBER_IS_OWNER             = "IS_OWNER";
    private static final String KEY_MEMBER_NICK_NAME            = "NICK_NAME";
    private static final String KEY_MEMBER_PROFILE_IMAGE_URL    = "PROFILE_IMAGE_URL";
    private static final String KEY_MEMBER_EXTRA_VALUE          = "EXTRA_VALUE";

    private static final String KEY_MESSAGE_IDX                 = "IDX";
    private static final String KEY_MESSAGE_THREAD_KEY          = "THREAD_KEY";
    private static final String KEY_MESSAGE_SEQ                 = "SEQ";
    private static final String KEY_MESSAGE_XID                 = "XID";
    private static final String KEY_MESSAGE_MESSAGE_TYPE        = "MESSAGE_TYPE";
    private static final String KEY_MESSAGE_CATEGORY            = "CATEGORY";
    private static final String KEY_MESSAGE_MESSAGE             = "MESSAGE";
    private static final String KEY_MESSAGE_STATE               = "STATE";
    private static final String KEY_MESSAGE_SEND_DATE           = "SEND_DATE";
    private static final String KEY_MESSAGE_NICK_NAME           = "NICK_NAME";

    public final static int SEND_STATE_COMPLETE = 1;
    public final static int SEND_STATE_SENDING  = 2;
    public final static int SEND_STATE_FAIL     = 3;

    public MessageDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String T_THREAD_CREATE = "CREATE TABLE IF NOT EXISTS "+ TABLE_THREAD_NAME +" (\n" +
                "  THREAD_KEY char(32) PRIMARY KEY UNIQUE NOT NULL,\n" +
                "  JOIN_MEMBER_COUNT text,\n" +
                "  UNREAD_COUNT integer,\n" +
                "  IS_DIRECT integer,\n" +
                "  LAST_MESSAGE text,\n" +
                "  REG_DATE double DEFAULT(0),\n" +
                "  UPDATE_DATE double DEFAULT(0),\n" +
                "  ALARM char(1) DEFAULT('N'),\n" +
                "  LAST_SEQ integer DEFAULT(0),\n" +
                "  ALIAS text\n" +
                ");";

        sqLiteDatabase.execSQL(T_THREAD_CREATE);

        String T_THREAD_MEMBER_CREATE = "CREATE TABLE IF NOT EXISTS "+ TABLE_THREAD_MEMBER_NAME +" (\n" +
                "  THREAD_KEY char(32) NOT NULL,\n" +
                "  XID text NOT NULL,\n" +
                "  JOIN_DATE double,\n" +
                "  READ_SEQ integer DEFAULT(0),\n" +
                "  IS_OWNER char(1) DEFAULT('N'),\n" +
                "  NICK_NAME text,\n" +
                "  PROFILE_IMAGE_URL text,\n" +
                "  EXTRA_VALUE text,\n" +
                "  PRIMARY KEY(THREAD_KEY, XID)\n" +
                ");";

        sqLiteDatabase.execSQL(T_THREAD_MEMBER_CREATE);

//        String T_MESSAGE_CREATE = "CREATE TABLE IF NOT EXISTS "+ TABLE_MESSAGE_NAME +" (\n" +
//                "  IDX integer PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
//                "  THREAD_KEY char(32) NOT NULL,\n" +
//                "  NICK_NAME  char(50),\n" +
//                "  SEQ integer NOT NULL DEFAULT(0),\n" +
//                "  XID text NOT NULL,\n" +
//                "  MESSAGE_TYPE integer NOT NULL,\n" +
//                "  CATEGORY integer DEFAULT(0),\n" +
//                "  STATE integer NOT NULL DEFAULT(0),\n" +
//                "  MESSAGE text,\n" +
//                "  SEND_DATE double,\n" +
//                "  UNIQUE (IDX)\n" +
//                ");";

        String T_MESSAGE_CREATE = "CREATE TABLE IF NOT EXISTS "+ TABLE_MESSAGE_NAME +" (\n" +
                "  IDX integer PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "  THREAD_KEY char(32) NOT NULL,\n" +
                "  NICK_NAME  char(50),\n" +
                "  DELETE_FLAG  char(50),\n" +
                "  SEQ integer NOT NULL DEFAULT(0),\n" +
                "  XID text NOT NULL,\n" +
                "  MESSAGE_TYPE integer NOT NULL,\n" +
                "  CATEGORY integer DEFAULT(0),\n" +
                "  STATE integer NOT NULL DEFAULT(0),\n" +
                "  MESSAGE text,\n" +
                "  SEND_DATE double,\n" +
                "  TRAN text,\n" +
                "  UNIQUE (IDX)\n" +
                ");";

        sqLiteDatabase.execSQL(T_MESSAGE_CREATE);
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS IX_SEARCH_THREAD ON MESSAGE_TABLE (THREAD_KEY COLLATE  RTRIM ASC);");
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS IX_THREAD_KEY ON MESSAGE_TABLE (THREAD_KEY COLLATE  RTRIM ASC, SEQ COLLATE  RTRIM ASC);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if(oldVersion < newVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+ TABLE_MESSAGE_NAME);
            onCreate(db);
        }
    }

    public List<ThreadData> loadThreadData() {
        List<ThreadData> result = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_THREAD_NAME, new String[] {
                        KEY_THREAD_THREAD_KEY,
                        KEY_THREAD_JOIN_MEMBER_COUNT,
                        KEY_THREAD_UNREAD_COUNT,
                        KEY_THREAD_IS_DIRECT,
                        KEY_THREAD_LAST_MESSAGE,
                        KEY_THREAD_REG_DATE,
                        KEY_THREAD_UPDATE_DATE,
                        KEY_THREAD_ALARM,
                        KEY_THREAD_LAST_SEQ,
                        KEY_THREAD_ALIAS,
                }, null,
                null, null, null, null, null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                do {
                    ThreadData threadData = new ThreadData();

                    threadData.setThreadKey(cursor.getString(0));
                    threadData.setJoinMemberCount(cursor.getInt(1));
                    threadData.setUnReadCount(cursor.getInt(2));
                    threadData.setIsDirect(cursor.getString(3));
                    threadData.setLastMessage(cursor.getString(4));
                    threadData.setRegDate(new Date(cursor.getLong(5)));
                    threadData.setUpdateDate(new Date(cursor.getLong(6)));
                    threadData.setAlarm(cursor.getString(7));
                    threadData.setLastSeq(cursor.getInt(8));
                    threadData.setAlias(cursor.getString(9));

                    List<ThreadMember> members = loadThreadMember(db, threadData.getThreadKey());
                    threadData.setMembers(members);
                    result.add(threadData);
                } while (cursor.moveToNext());
            }

            cursor.close();
        }

        return result;
    }

    public ThreadData getThreadData(String threadKey) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor =
                db.query(TABLE_THREAD_NAME, new String[] {
                        KEY_THREAD_THREAD_KEY,
                        KEY_THREAD_JOIN_MEMBER_COUNT,
                        KEY_THREAD_UNREAD_COUNT,
                        KEY_THREAD_IS_DIRECT,
                        KEY_THREAD_LAST_MESSAGE,
                        KEY_THREAD_REG_DATE,
                        KEY_THREAD_UPDATE_DATE,
                        KEY_THREAD_ALARM,
                        KEY_THREAD_LAST_SEQ,
                        KEY_THREAD_ALIAS,
                },
                KEY_MEMBER_THREAD_KEY + "= ?",
                        new String[] {
                            threadKey
                        }, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();

            ThreadData threadData = new ThreadData();

            threadData.setThreadKey(cursor.getString(0));
            threadData.setJoinMemberCount(cursor.getInt(1));
            threadData.setUnReadCount(cursor.getInt(2));
            threadData.setIsDirect(cursor.getString(3));
            threadData.setLastMessage(cursor.getString(4));
            threadData.setRegDate(new Date(cursor.getLong(5)));
            threadData.setUpdateDate(new Date(cursor.getLong(6)));
            threadData.setAlarm(cursor.getString(7));
            threadData.setLastSeq(cursor.getInt(8));
            threadData.setAlias(cursor.getString(9));

            List<ThreadMember> members = loadThreadMember(db, threadData.getThreadKey());
            threadData.setMembers(members);

            cursor.close();

            return threadData;
        }

        return null;
    }

    public List<ThreadMember> loadThreadMember(String threadKey) {
        SQLiteDatabase db = this.getReadableDatabase();

        return loadThreadMember(db, threadKey);
    }

    public List<ThreadMember> loadThreadMember(SQLiteDatabase db, String threadKey) {

        List<ThreadMember> members = new ArrayList<>();

        Cursor cursor = db.query(TABLE_THREAD_MEMBER_NAME, new String[] {
                KEY_MEMBER_XID,
                KEY_MEMBER_JOIN_DATE,
                KEY_MEMBER_READ_SEQ,
                KEY_MEMBER_IS_OWNER,
                KEY_MEMBER_NICK_NAME,
                KEY_MEMBER_PROFILE_IMAGE_URL,
                KEY_MEMBER_EXTRA_VALUE,
        }, KEY_MEMBER_THREAD_KEY + "= ?", new String[] {
                threadKey
        }, null, null, null, null);

        if(cursor != null) {
            if(cursor.getCount() > 0 ) {
                if(cursor.moveToFirst()) {
                    do {
                        ThreadMember threadMember = new ThreadMember();

                        threadMember.setXid(cursor.getString(0));
                        threadMember.setJoinDate(new Date(cursor.getLong(1)));
                        threadMember.setReadSeq(cursor.getInt(2));
                        threadMember.setIsOwner(cursor.getString(3));
                        threadMember.setNickName(cursor.getString(4));
                        threadMember.setProfileImageUrl(cursor.getString(5));
                        threadMember.setExtraValue(cursor.getString(6));

                        members.add(threadMember);
                    } while (cursor.moveToNext());
                }
            }

            cursor.close();
        }

        return members;
    }

    public List<MessageData> loadMessageData(String threadKey, int seq) {

        List<MessageData> messageDataList = new ArrayList<>();

//        String query = "SELECT " +
//                "SEQ," +
//                "XID," +
//                "MESSAGE_TYPE," +
//                "CATEGORY," +
//                "MESSAGE," +
//                "SEND_DATE," +
//                "STATE," +
//                "NICK_NAME" +
//                " FROM (SELECT * FROM " + TABLE_MESSAGE_NAME + " WHERE " + KEY_MESSAGE_THREAD_KEY +
//                " = '" + threadKey + "' AND " + KEY_MESSAGE_SEQ + " < " + seq + " ORDER BY " + KEY_MESSAGE_SEQ + " DESC LIMIT 100) ORDER BY " + KEY_MESSAGE_SEQ + " ASC";

        String query = "SELECT " +
                "SEQ," +
                "XID," +
                "MESSAGE_TYPE," +
                "CATEGORY," +
                "MESSAGE," +
                "SEND_DATE," +
                "STATE," +
                "NICK_NAME," +
                "DELETE_FLAG," +
                "TRAN" +
                " FROM (SELECT * FROM " + TABLE_MESSAGE_NAME + " WHERE " + KEY_MESSAGE_THREAD_KEY +
//                " = '" + threadKey + "' AND " + KEY_MESSAGE_SEQ + " < " + seq + " ORDER BY " + KEY_MESSAGE_SEQ + " DESC LIMIT 100) ORDER BY " + KEY_MESSAGE_SEQ + " ASC";
                " = '" + threadKey + "' AND " + KEY_MESSAGE_SEQ + " < " + seq + " AND MESSAGE <> " + "' '" +" ORDER BY " + KEY_MESSAGE_SEQ + " DESC LIMIT 100) ORDER BY " + KEY_MESSAGE_SEQ + " ASC";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        if(cursor != null) {
            if(cursor.getCount() > 0 ) {
                cursor.moveToFirst();

                do {

                    MessageData messageData = new MessageData();
                    messageData.setSeq(cursor.getInt(0));
                    messageData.setXid(cursor.getString(1));
                    messageData.setType((short) cursor.getInt(2));
                    messageData.setCategory(cursor.getInt(3));
                    messageData.setMessage(cursor.getString(4));
                    messageData.setSendDate(new Date(cursor.getLong(5)));
                    messageData.setState(cursor.getInt(6));
                    messageData.setNickName(cursor.getString(7));
                    messageData.setDeleteFlag(cursor.getString(8));
                    messageData.setTran(cursor.getString(9));

                    messageDataList.add(messageData);

                } while (cursor.moveToNext());

            }

            cursor.close();
        }

        return messageDataList;
    }

    public void updateThreadLastMessage(String threadKey, int seq, String message) {
        ContentValues threadValues = new ContentValues();
        threadValues.put("MESSAGE",     message);

        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_MESSAGE_NAME, threadValues, KEY_MEMBER_THREAD_KEY + "= ? AND SEQ = " + seq, new String[] {
                threadKey
        });
    }

    public void removeMessageData(String threadKey, int seq) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        db.delete(TABLE_MESSAGE_NAME, KEY_MEMBER_THREAD_KEY + "= ? AND SEQ <> ?" , new String[] {
                threadKey, String.valueOf(seq)
        });


        db.setTransactionSuccessful();
        db.endTransaction();
    }



    public void removeThreadMessage(String threadKey) {


        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        db.delete(TABLE_MESSAGE_NAME, KEY_MEMBER_THREAD_KEY + "= ?" , new String[] {
                threadKey
        });


        db.setTransactionSuccessful();
        db.endTransaction();

    }


    public MessageData getLastMessageData(String threadKey) {

        MessageData messageData = null;

        String query = "SELECT " +
                "SEQ," +
                "XID," +
                "MESSAGE_TYPE," +
                "CATEGORY," +
                "MESSAGE," +
                "SEND_DATE," +
                "STATE, " +
                "NICK_NAME" +
                " FROM " + TABLE_MESSAGE_NAME + " WHERE " + KEY_MESSAGE_THREAD_KEY +
                " = '" + threadKey + "' AND " + KEY_MESSAGE_SEQ + " <> " + Integer.MAX_VALUE + " ORDER BY " + KEY_MESSAGE_SEQ + " DESC LIMIT 1";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        if(cursor != null) {
            if(cursor.getCount() > 0) {
                if(cursor.moveToFirst()) {

                    messageData = new MessageData();
                    messageData.setSeq(cursor.getInt(0));
                    messageData.setXid(cursor.getString(1));
                    messageData.setType((short)cursor.getInt(2));
                    messageData.setCategory(cursor.getInt(3));
                    messageData.setMessage(cursor.getString(4));
                    messageData.setSendDate(new Date(cursor.getLong(5)));
                    messageData.setState(cursor.getInt(6));
                    messageData.setNickName(cursor.getString(7));
                }
            }


            cursor.close();
        }

        return messageData;
    }

    public MessageData getMessageDataBySeq(String threadKey, int seq) {

        MessageData messageData = null;

        String query = "SELECT " +
                "SEQ," +
                "XID," +
                "MESSAGE_TYPE," +
                "CATEGORY," +
                "MESSAGE," +
                "SEND_DATE," +
                "STATE, " +
                "NICK_NAME" +
                " FROM " + TABLE_MESSAGE_NAME + " WHERE " + KEY_MESSAGE_THREAD_KEY +
                " = '" + threadKey + "' AND " + KEY_MESSAGE_SEQ + " = " + seq + " ORDER BY " + KEY_MESSAGE_SEQ + " DESC LIMIT 1";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        if(cursor != null) {
            if(cursor.getCount() > 0) {
                if(cursor.moveToFirst()) {

                    messageData = new MessageData();
                    messageData.setSeq(cursor.getInt(0));
                    messageData.setXid(cursor.getString(1));
                    messageData.setType((short)cursor.getInt(2));
                    messageData.setCategory(cursor.getInt(3));
                    messageData.setMessage(cursor.getString(4));
                    messageData.setSendDate(new Date(cursor.getLong(5)));
                    messageData.setState(cursor.getInt(6));
                    messageData.setNickName(cursor.getString(7));
                }
            }


            cursor.close();
        }

        return messageData;
    }

    public void saveThreadData(ThreadData threadData, String xid) {

        String myAlarmSet = "Y";

        for (ThreadMember threadMember : threadData.getMembers()) {
            if(threadMember.getXid().equals(xid)) {
                myAlarmSet = threadMember.getAlarm();
                break;
            }
        }

        ContentValues threadValues = new ContentValues();
        threadValues.put("THREAD_KEY",          threadData.getThreadKey());
        threadValues.put("JOIN_MEMBER_COUNT",   threadData.getJoinMemberCount());
        threadValues.put("UNREAD_COUNT",        threadData.getUnReadCount());
        threadValues.put("IS_DIRECT",           threadData.getIsDirect());
        threadValues.put("LAST_MESSAGE",        threadData.getLastMessage());
        threadValues.put("REG_DATE",            threadData.getRegDate().getTime());
        threadValues.put("UPDATE_DATE",         threadData.getUpdateDate().getTime());
        threadValues.put("ALARM",               myAlarmSet);
        threadValues.put("LAST_SEQ",            threadData.getLastSeq());
        threadValues.put("ALIAS",               threadData.getAlias());

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        db.replace(TABLE_THREAD_NAME, null, threadValues);

        ContentValues memberValue;
        for (ThreadMember threadMember : threadData.getMembers()) {
            memberValue = new ContentValues();
            memberValue.put("THREAD_KEY",           threadData.getThreadKey());
            memberValue.put("XID",                  threadData.getThreadKey());
            memberValue.put("JOIN_DATE",            threadMember.getJoinDate().getTime());
            memberValue.put("READ_SEQ",             threadMember.getReadSeq());
            memberValue.put("IS_OWNER",             threadMember.getIsOwner());
            memberValue.put("NICK_NAME",            threadMember.getNickName());
            memberValue.put("PROFILE_IMAGE_URL",    threadMember.getProfileImageUrl());
            memberValue.put("EXTRA_VALUE",          threadMember.getExtraValue());

            db.replace(TABLE_THREAD_MEMBER_NAME, null, memberValue);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void updateThreadNewMessage(String threadKey, String lastMessage, int seq) {
        ContentValues threadValues = new ContentValues();
        threadValues.put("LAST_MESSAGE",    lastMessage);
        threadValues.put("UPDATE_DATE",     new Date().getTime());
        threadValues.put("LAST_SEQ",        seq);

        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_THREAD_NAME, threadValues, KEY_MEMBER_THREAD_KEY + "= ?", new String[] {
                threadKey
        });
    }

    public void updateThreadLastSeq(String threadKey, int seq) {
        ContentValues threadValues = new ContentValues();
        threadValues.put("UPDATE_DATE",     new Date().getTime());
        threadValues.put("LAST_SEQ",        seq);

        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_THREAD_NAME, threadValues, KEY_MEMBER_THREAD_KEY + "= ?", new String[] {
                threadKey
        });
    }





    public void removeThread(String threadKey) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        db.delete(TABLE_MESSAGE_NAME, KEY_MEMBER_THREAD_KEY + "= ?", new String[] {
                threadKey
        });

        db.delete(TABLE_THREAD_MEMBER_NAME, KEY_MEMBER_THREAD_KEY + "= ?", new String[] {
                threadKey
        });

        db.delete(TABLE_THREAD_NAME, KEY_MEMBER_THREAD_KEY + "= ?", new String[] {
                threadKey
        });

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void removeThreadAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        db.delete(TABLE_MESSAGE_NAME, null, new String[] {

        });

        db.delete(TABLE_THREAD_MEMBER_NAME, null, new String[] {

        });

        db.delete(TABLE_THREAD_NAME, null, new String[] {

        });

        db.setTransactionSuccessful();
        db.endTransaction();
    }


    public void appendMessage(String threadKey, MessageData message) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        addMessage(db, threadKey, message);

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void appendMessages(String threadKey, List<MessageData> messages) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        for (MessageData message : messages) {
            addMessage(db, threadKey, message);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public long addSendMessage(String threadKey, MessageData message) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        message.setState(MessageConst.SEND_STATE_SENDING);
        long result = addMessage(db, threadKey, message);

        db.setTransactionSuccessful();
        db.endTransaction();

        return result;
    }


    public void updateMessageTranslation(String threadKey, int seq, String jsonTransMsg) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("TRAN",     jsonTransMsg);

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        db.update(TABLE_MESSAGE_NAME, contentValues, KEY_MESSAGE_THREAD_KEY + " = ? AND " + KEY_MESSAGE_SEQ + " = ?" , new String[] {
                threadKey, String.valueOf(seq)
        });
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void updateMessageStatus(long databaseIndex, int seq) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        ContentValues contentValues = new ContentValues(2);
        contentValues.put(KEY_MESSAGE_SEQ, seq);
        contentValues.put(KEY_MESSAGE_STATE, MessageConst.SEND_STATE_COMPLETE);

        db.update(TABLE_MESSAGE_NAME, contentValues, KEY_MESSAGE_IDX + " = " + databaseIndex, null);

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void updateMessageDelete(String msg, String threadKey, int seq) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("MESSAGE",     msg);

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        db.update(TABLE_MESSAGE_NAME, contentValues, KEY_MESSAGE_THREAD_KEY + " = ? AND " + KEY_MESSAGE_SEQ + " = ?" , new String[] {
                threadKey, String.valueOf(seq)
        });

        db.setTransactionSuccessful();
        db.endTransaction();
    }


    public void updateThreadMemberReadSeq(String threadKey, String xid, int readSeq) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        ContentValues contentValues = new ContentValues(1);
        contentValues.put(KEY_MEMBER_READ_SEQ, readSeq);

        db.update(TABLE_THREAD_MEMBER_NAME, contentValues, KEY_MEMBER_THREAD_KEY + " = ? AND " + KEY_MEMBER_XID + " = ?" , new String[] {
                threadKey, xid
        });

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public long addMessage(SQLiteDatabase db, String threadKey, MessageData messageData) {
        ContentValues messageValues = new ContentValues();

//        messageValues.put("IDX", messageData.);
        messageValues.put("THREAD_KEY",     threadKey);
        messageValues.put("SEQ",            messageData.getSeq());
        messageValues.put("XID",            messageData.getXid());
        messageValues.put("MESSAGE_TYPE",   messageData.getType());
        messageValues.put("CATEGORY",       messageData.getCategory());
        messageValues.put("MESSAGE",        messageData.getMessage());
        messageValues.put("SEND_DATE",      messageData.getSendDate().getTime());
        messageValues.put("STATE",          messageData.getType());
        messageValues.put("NICK_NAME",      messageData.getNickName());
        messageValues.put("TRAN",           messageData.getTran());
        messageValues.put("DELETE_FLAG",    messageData.getDeleteFlag());

        return db.insertOrThrow(TABLE_MESSAGE_NAME, null, messageValues);
    }
}
