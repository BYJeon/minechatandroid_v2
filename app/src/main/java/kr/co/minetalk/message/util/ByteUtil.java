package kr.co.minetalk.message.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Created by Taeun on 14. 12. 26..
 */
public class ByteUtil {

    public static int byteToInt(byte[] byteArray, int offset) {
        return (int)((byteArray[offset]&0XFF) << 24)|(int)((byteArray[offset + 1]&0XFF) << 16)|(int)((byteArray[offset + 2]&0XFF) << 8)|(int)((byteArray[offset + 3]&0XFF));
    }

    public static long byteToLong(byte[] byteArray, int offset) {

        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
        byteBuffer.order(ByteOrder.BIG_ENDIAN);

        byteBuffer.put(Arrays.copyOfRange(byteArray, offset, offset + 8));

        byteBuffer.flip();

        return byteBuffer.getLong();
    }

//    public static byte[] intToByteArray(int a) { // little endian
//        return new byte[] {
//                (byte) (a & 0xFF),
//                (byte) ((a >> 8) & 0xFF),
//                (byte) ((a >> 16) & 0xFF),
//                (byte) ((a >> 24) & 0xFF)
//        };
//    }

    public static final byte[] intToByteArray(int value) { // big endian
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }

    public static byte[] getBodyData(byte[] packet)
    {
        int bodyLength = ByteUtil.byteToInt(packet, 1);

        ByteBuffer bodyBuffer = ByteBuffer.allocateDirect(bodyLength);

        bodyBuffer.put(packet, 5, bodyLength);

        return bodyBuffer.array();
    }
}
