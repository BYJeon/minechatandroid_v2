package kr.co.minetalk.message.api;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;

import kr.co.minetalk.message.model.ThreadData;

public class ThreadTotalListApi extends BaseHttpRequest<List<ThreadData>> {
    private static final String PATH = "/members/%s/threads/total";

    public ThreadTotalListApi(Context context, String xid) {
        super(context, PATH, new String[]{xid}, "GET");

        this.registTypeRef(new TypeReference<List<ThreadData>>() {
        });
    }

}
