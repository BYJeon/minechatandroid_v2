package kr.co.minetalk.message.model;

import java.util.ArrayList;

public class ThreadWithChannel extends ThreadData {
    private ChannelData channelData;

    private ArrayList<ThreadMember> members;

    public ChannelData getChannelData() {
        return channelData;
    }

    public void setChannelData(ChannelData channelData) {
        this.channelData = channelData;
    }

    @Override
    public ArrayList<ThreadMember> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<ThreadMember> members) {
        this.members = members;
    }
}
