package kr.co.minetalk.message.model;

import java.util.List;

public class CreateThreadData {

    private List<ThreadMember> members;

    private String lastMessage;

    private int unReadCount;

    public List<ThreadMember> getMembers() {
        return members;
    }

    public void setMembers(List<ThreadMember> members) {
        this.members = members;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public int getUnReadCount() {
        return unReadCount;
    }

    public void setUnReadCount(int unReadCount) {
        this.unReadCount = unReadCount;
    }
}
