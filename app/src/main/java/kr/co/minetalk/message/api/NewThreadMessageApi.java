package kr.co.minetalk.message.api;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;

import kr.co.minetalk.message.model.MessageData;

import java.util.List;

public class NewThreadMessageApi extends BaseHttpRequest<List<MessageData>> {
    private static final String PATH = "/threads/%s/messages_v2?seq=%s";

    public NewThreadMessageApi(Context context, String threadKey, int lastSeq) {
        super(context, PATH, new String[]{threadKey, String.valueOf(lastSeq)}, "GET");

//        addParameter("seq", String.valueOf(lastSeq));

        this.registTypeRef(new TypeReference<List<MessageData>>() {
        });
    }
}
