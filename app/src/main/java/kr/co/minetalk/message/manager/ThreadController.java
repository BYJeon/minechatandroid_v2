package kr.co.minetalk.message.manager;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.fasterxml.jackson.databind.ObjectMapper;


import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.activity.ChatOptionActivity;
import kr.co.minetalk.activity.MainActivity;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.message.MessageConst;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.NewThreadMessageApi;
import kr.co.minetalk.message.api.ThreadChannelApi;
import kr.co.minetalk.message.api.ThreadMemberApi;
import kr.co.minetalk.message.api.ThreadPrevMessageApi;
import kr.co.minetalk.message.api.ThreadSecurityListApi;
import kr.co.minetalk.message.model.MessageData;
import kr.co.minetalk.message.model.SessionData;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.message.model.ThreadWithChannel;
import kr.co.minetalk.message.util.ByteUtil;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


/**
 * Created by Taeun on 15. 1. 13..
 */
public class ThreadController {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private volatile boolean isConnected = false;

    public enum ReceiveDataType {

        RECEIVE_TYPE_READY(1),
        RECEIVE_TYPE_CONNECTED(2),
        RECEIVE_TYPE_SEND_CALLBACK(3),
        RECEIVE_TYPE_MESSAGE(4),
        RECEIVE_TYPE_SYSTEN(5),
        RECEIVE_TYPE_THREAD_SEQ(6),
        RECEIVE_TYPE_READ_SEQ(7),
        RECEIVE_TYPE_DELETE_MSG(8);

        private int value;

        private ReceiveDataType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum MessageType {
        MESSAGE_TYPE_USER(1),
        MESSAGE_TYPE_SYSTEM(2),
        MESSAGE_TYPE_NOTICE(3);

        private int value;

        MessageType(int value) { this.value = value; }

        public int getValue() { return this.value; }
    }

    public enum SendDataType {
        SEND_TYPE_CONNECT((byte)0x1),
        SEND_TYPE_PING((byte)0x2),
        SEND_TYPE_MESSAGE((byte)0x3),
        SEND_TYPE_REQUEST_THREAD_SEQ((byte)0x4),
        SEND_TYPE_READ_SEQ((byte)0x5),
        SEND_TYPE_DELETE_SEQ((byte)0x6);

        private byte value;

        SendDataType(byte value) { this.value = value; }

        public byte getValue() { return this.value; }
    }

    private Context context;

    private ArrayList<MessageData> messageModels;

    /**
     * Preview 디코더
     */
    private MessageDecode messageDecode;

    /**
     * ThreadModel 현재 진행중인 쓰레드 정보
     */
    private ThreadWithChannel threadData;

    /**
     * 소켓 컨텍스트
     */
    private SocketContext socketContext;

    /**
     * 현재 상태가 Connect 상태인지 아닌지 여부
     */
    private volatile boolean isRun;

    /**
     * 보내기 큐
     */
    private BlockingQueue<byte[]> sendQueue;

    /**
     * 현재 상태가 onPause 상태인지 아닌지 여부
     */
    private boolean isPause = false;

    /**
     * 전송받은 데이터 처리 클래스 함수 맵
     */
    private Map<Integer, Handler> receiveHandlerMap;

    /**
     * 쓰레드 이벤트 리스너
     */
    private ThreadEventListner threadEventListener;

    /**
     * 스레드 사용자 정보 맵
     */
    private HashMap<String, ThreadMember> memberMap;

    /**
     * 현재 로그인 사용자 정보
     */
    private SessionData currentMember;

    /**
     *
     */
    private Thread pingThread;

    public ThreadController(Context context, SessionData sessionData, ThreadWithChannel thread) {

        this.threadData = thread;
        this.context = context;

        this.isRun = false;
        this.receiveHandlerMap = new HashMap<Integer, Handler>();
        this.messageModels = new ArrayList<MessageData>();

        this.currentMember = sessionData;

        sendQueue = new ArrayBlockingQueue<byte[]>(20);

        setThreadMemberData(thread.getMembers());

        registerHandler();

        MessageThreadManager.getInstance().saveThread(context, this.currentMember.getXid(), thread);
    }

    private void setThreadMemberData(List<ThreadMember> members) {

        if(memberMap != null) memberMap.clear();

        memberMap = new HashMap<>();

        for (ThreadMember member : members) {
            memberMap.put(member.getXid(), member);
        }
    }

    public void setThreadEventListener(ThreadEventListner threadEventListener) {
        this.threadEventListener = threadEventListener;
    }

    public void setMessageDecode(MessageDecode decode) {
        this.messageDecode = decode;
    }


    public void refreshMessageModels(Context context, int seq) {

        List<MessageData> messages = MessageThreadManager.getInstance().getLocalMessages(context, threadData.getThreadKey(), seq);

        if(null == messages || messages.isEmpty()) {
            return;
        }

        this.messageModels.clear();
        addMessageModels(messages);

        threadEventListener.refreshChatList();
        threadEventListener.setScrollLastposition(messages.size() - 1);
    }

    public void reLoadMessageList(Context context) {
        refreshMessageModels(context, Integer.MAX_VALUE);
    }

    /**
     * 로컬에 저장된 메시지를 불러온다.
     * @param seq 로드시작 시퀀스
     * @return 매시지 모델 리스트
     */
    public void loadMessageModels(Context context, int seq) {

        List<MessageData> messages = MessageThreadManager.getInstance().getLocalMessages(context, threadData.getThreadKey(), seq);

        if(null == messages || messages.isEmpty()) {
            return;
        }

        messageModels.clear();
        addMessageModels(messages);

        threadEventListener.setScrollLastposition(messages.size() - 1);
    }

    /**
     * 메시지 추가
     * @param model
     */
    private void addMessageModel(MessageData model) {
        messageModels.add(model);
        threadEventListener.refreshChatList();

        threadEventListener.onAppendMessage(model);
    }

    /**
     * 리스트 추가
     * @param models
     */
    private void addMessageModels(List<MessageData> models) {
        messageModels.addAll(models);
        threadEventListener.refreshChatList();
    }

    /**
     * 리스트 헤더에 추가
     * @param models
     */
    public void addMessageModelsFirst(List<MessageData> models) {
        messageModels.addAll(0, models);
        threadEventListener.refreshChatList();
    }

    /**
     * 모델 리스트 갯수 반환
     * @return
     */
    public int getMessageModelsCount() {
        return messageModels.size();
    }

    /**
     * 해당 포지션 메시지 모델 반환
     * @param position
     * @return
     */
    public MessageData getMessageModel(int position) {
        return messageModels.get(position);
    }

    /**
     * 메시지 접근
     * @return
     */
    public ArrayList<MessageData> getMessages() {
        return messageModels;
    }

    /**
     * 소켓을 연결 한다.
     */
    public void connect() {

        // 소켓연결은 백그라운드에서 수행한다.
        new Thread(new Runnable() {

            @Override
            public void run() {

                socketContext = new SocketContext();
                socketContext.setConnectStateListener(new SocketContext.ConnectStateListener() {
                    @Override
                    public void onConnected() {
                        //
                    }

                    @Override
                    public void onDisconnected() {
                        disconnect();
                    }

                    @Override
                    public void onSendTimeout() {

                    }
                });

                if(socketContext.connect(threadData.getChannelData().getIp(), threadData.getChannelData().getPort())) {
//                if(socketContext.connect("10.20.11.13", threadData.getChannelData().getPort())) {
                    isRun = true;

                    // 수신 쓰레드
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            receiveImpl();
                        }
                    }).start();

                    // 전송 쓰레드
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            sendImpl();
                        }
                    }).start();

                    // 소켓연결 통보
                    if(null != threadEventListener) {
                        threadEventListener.onConnectResult(true);
                    }

                } else {
                    if(null != threadEventListener) {
                        threadEventListener.onConnectResult(false);
                    }
                }
            }
        }).start();
    }

    /**
     * 소켓 연결을 종료한다.
     */
    public void disconnect() {

        if(socketContext != null && socketContext.isConneted()) {
            // 소켓 연결 종료
            socketContext.disconnect();
        }

        socketContext = null;
        isRun = false;

        onDisConnected();
    }

    private void onConnected() {
        isConnected = true;

        startPing();
        threadEventListener.onConnected();

        // reqThreadSeq
//JEROME
        try {
            sendReqThreadSeq(this.threadData.getThreadKey());
            sendReadSeq(this.threadData.getThreadKey(), this.threadData.getLastSeq());

        }
        catch (IOException e){
            e.printStackTrace();
        }
//JEROME
        loadMessageModels(context, Integer.MAX_VALUE);
    }

    private void onDisConnected() {
        isConnected = false;
        stopPing();
        threadEventListener.onDisconnected();
    }

    /**
     * 접속 요청
     */
    public void start() {
        if(isPause) {
            isPause = false;
            connect();
        }
    }

    /**
     * 접속 종료
     */
    public void stop() {
        isPause = true;
        disconnect();
    }

    /**
     * 서버 접속 여부 확인
     * @return 서버 접속 여부
     */
    public boolean isConnected() {

        return isConnected;
    }

    private void startPing() {
        // PING 쓰레드
        pingThread = new Thread(new Runnable() {

            @Override
            public void run() {

                while (isRun) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        break;
                    }

                    if(isConnected)
                        sendPing();
                }
            }
        });

        pingThread.start();
    }

    private void stopPing() {
        // PING 쓰레드
//        try {
//            pingThread.join();
//        } catch (InterruptedException e) {
//        }
        try {
            pingThread.interrupt();
        } catch (Exception e) {
        }
    }

    public void reqDeleteMessage(int seq){
        try {
            sendReqDeleteSeq(this.threadData.getThreadKey(), seq);
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
    /**
     * 메시지를 전송한다.
     * @param messageData 전송하려는 메시지 모델
     */
    public void sendMessage(MessageData messageData) {

        long dbIndex = MessageThreadManager.getInstance().addSendMessage(context, threadData.getThreadKey(), messageData);

        //int lastSeq = threadData.getLastSeq();
        //messageData.setSeq(lastSeq+1);
        messageData.setDbIndex((int)dbIndex);

        addMessageModel(messageData);
        sendMessage(messageModelToBytes(threadData.getThreadKey(), messageData));

        Log.e("@@@TAG", "sendMessage : " + messageData.getSeq());
    }


    public void sendMessage(MessageData messageData, String threadKey) {

        long dbIndex = MessageThreadManager.getInstance().addSendMessage(context, threadKey, messageData);

        //int lastSeq = threadData.getLastSeq();
        //messageData.setSeq(lastSeq+1);
        messageData.setDbIndex((int)dbIndex);

//        addMessageModel(messageData);
        sendMessage(messageModelToBytes(threadKey, messageData));
    }

    /**
     * 메시지를 전송한다.
     * @param buffer 전송하려는 버퍼
     */
    public void sendMessage(byte[] buffer) {
        sendQueue.add(buffer);
    }

    /**
     * 로컬 데이터베이스 / 서버에서 이전 메시지를 가져온다.
     */
    public void loadPrevMessageList() {

        int firstSequence = 0;

        if(null != messageModels && !messageModels.isEmpty()) {
            firstSequence = messageModels.get(0).getSeq();
        }

        List<MessageData> models = MessageThreadManager.getInstance().getLocalMessages(context, threadData.getThreadKey(), firstSequence);

        if(null == models || models.isEmpty()) {
            firstSequence = 0 == firstSequence ? threadData.getLastSeq() : firstSequence;
            final int seq = firstSequence;

            if(context instanceof Activity) {
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        requestPrevMessageApi(threadData.getThreadKey(), seq);
                    }
                });
            }
            return;
        } else if(1 >= models.get(0).getSeq()) {
            threadEventListener.showMoreButton(false);
        } else {
            threadEventListener.showMoreButton(true);
        }

        addMessageModelsFirst(models);

        threadEventListener.setScrollLastposition(models.size());
    }

    /**
     * 받은 데이터 처리 클래스를 등록한다.
     */
    private void registerHandler() {

        receiveHandlerMap.put(ReceiveDataType.RECEIVE_TYPE_READY.getValue(), new Handler() {
            @Override
            public void handlerData(byte[] buffer) {
                handleReadyPacket();
            }
        });

        receiveHandlerMap.put(ReceiveDataType.RECEIVE_TYPE_MESSAGE.getValue(), new Handler() {
            @Override
            public void handlerData(byte[] buffer) {
                try {
                    handleMessagePacket(buffer);
                } catch (IOException e) {
                }
            }
        });

        receiveHandlerMap.put(ReceiveDataType.RECEIVE_TYPE_SYSTEN.getValue(), new Handler() {
            @Override
            public void handlerData(byte[] buffer) {
                try {
                    handleSystemPacket(buffer);
                } catch (IOException e) {
                }
            }
        });

        receiveHandlerMap.put(ReceiveDataType.RECEIVE_TYPE_SEND_CALLBACK.getValue(), new Handler() {
            @Override
            public void handlerData(byte[] buffer) {
                handleSendCallbackPacket(buffer);
            }
        });


// JEROME
        receiveHandlerMap.put(ReceiveDataType.RECEIVE_TYPE_THREAD_SEQ.getValue(), new Handler() {
            @Override
            public void handlerData(byte[] buffer) {
                handleSeqPacket(buffer);
            }
        });
//
        receiveHandlerMap.put(ReceiveDataType.RECEIVE_TYPE_READ_SEQ.getValue(), new Handler() {
            @Override
            public void handlerData(byte[] buffer) {
                handleReadSeqPacket(buffer);
            }
        });
// JEROME
        receiveHandlerMap.put(ReceiveDataType.RECEIVE_TYPE_CONNECTED.getValue(), new Handler() {
            @Override
            public void handlerData(byte[] buffer) {
                onConnected();
            }
        });

        //
        receiveHandlerMap.put(ReceiveDataType.RECEIVE_TYPE_DELETE_MSG.getValue(), new Handler() {
            @Override
            public void handlerData(byte[] buffer) {
                handleDeleteMsgPacket(buffer);
            }
        });
    }

    /**
     * 루프를 진행하면서 소켓 컨텍스트로부터 데이터를 가져온다.
     */
    private void receiveImpl() {

        int bufferSize = 1024 * 8;

        // 메인 리시브 버퍼
        ByteBuffer receiveBuffer = ByteBuffer.allocate(bufferSize);

        try {
            byte[] buffer = new byte[bufferSize];
            int readSize = 0;

            while(isRun && socketContext != null && socketContext.isConneted() && -1 != (readSize = socketContext.receive(buffer))) {

                // 메인 리시브 버퍼의 남은 사이즈가 읽어온 데이터의 사이즈 보다 작다면 메인 리시브 버퍼의 사이즈를 확장한다.
                while(bufferSize - receiveBuffer.position() < readSize) {
                    receiveBuffer.limit(receiveBuffer.limit() + bufferSize);
                }

                // 메인 리시브 버퍼에 데이터 복사
                receiveBuffer.put(buffer, 0, readSize);

                // 패킷 처리
                while(4 < receiveBuffer.position() && (ByteUtil.byteToInt(receiveBuffer.array(), 0) + 4) <= receiveBuffer.position()) {

                    // 패킷 추출
                    int packetLength = ByteUtil.byteToInt(receiveBuffer.array(), 0);
                    byte[] packetBuffer = new byte[packetLength];
                    System.arraycopy(receiveBuffer.array(), 4 , packetBuffer, 0, packetLength);

                    // 완성된 패킷 처리
                    handleReceiveData(packetBuffer);

                    // 한개 이상의 패킷이 들어와 있을 경우 버퍼 시프트 처리
                    if(packetLength + 4 < receiveBuffer.position()) {
                        receiveBuffer.compact();
                    } else {
                        receiveBuffer.clear();
                    }
                }
            }
        } catch(IOException e) {
            if(!isPause) {
                threadEventListener.onErrorOccur(e);
            }
        }
    }

    /**
     * 루프를 진행하면서 보내기 큐에서 데이터를 꺼내 전송한다.
     */
    private void sendImpl() {

        while(isRun) {

            try {

                byte[] buffer = sendQueue.take();

                // 데이터 전송
                socketContext.send(buffer);

            } catch (InterruptedException e) {
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendPing() {
        try {
            ByteBuffer dataBuf = ByteBuffer.allocate(1);
            dataBuf.put(SendDataType.SEND_TYPE_PING.getValue());
            dataBuf.flip();

            sendMessage(dataBuf.array());
        }
        catch (Exception e) {}
    }

    /**
     * 참여 메시지를 쓰레드 서버에 전송한다.
     */
    private void sendJoinData() throws IOException {

        byte[] xidByte = this.currentMember.getXid().getBytes("UTF-8");
        byte[] nickNameByte = this.currentMember.getNickName().getBytes("UTF-8");
        byte[] tokenByte = this.currentMember.getToken().getBytes("UTF-8");

        int length = 1 +
                xidByte.length + 4 +
                nickNameByte.length + 4 +
                tokenByte.length + 4;

        ByteBuffer dataBuf = ByteBuffer.allocate(length);

        dataBuf.put(SendDataType.SEND_TYPE_CONNECT.getValue());
        dataBuf.put(ByteUtil.intToByteArray(xidByte.length));
        dataBuf.put(xidByte);
        dataBuf.put(ByteUtil.intToByteArray(nickNameByte.length));
        dataBuf.put(nickNameByte);
        dataBuf.put(ByteUtil.intToByteArray(tokenByte.length));
        dataBuf.put(tokenByte);
        dataBuf.flip();

        sendMessage(dataBuf.array());
    }

    /**
     * 접속하는 스레드에 LastSeq 를 조회 한다
     * @throws IOException
     */
    private void sendReqThreadSeq(String threadKey) throws IOException {

        byte[] threadKeyBytes = threadKey.getBytes("UTF-8");

        int length = 1 +
                threadKeyBytes.length + 4;

        ByteBuffer dataBuf = ByteBuffer.allocate(length);

        dataBuf.put(SendDataType.SEND_TYPE_REQUEST_THREAD_SEQ.getValue());
        dataBuf.put(ByteUtil.intToByteArray(threadKeyBytes.length));
        dataBuf.put(threadKeyBytes);
        dataBuf.flip();

        sendMessage(dataBuf.array());
    }

    private void sendReadSeq(String threadKey, int readSeq) throws IOException {

        byte[] threadKeyBytes = threadKey.getBytes("UTF-8");

        int length = 1 +
                4 +
                threadKeyBytes.length + 4;

        ByteBuffer dataBuf = ByteBuffer.allocate(length);

        dataBuf.put(SendDataType.SEND_TYPE_READ_SEQ.getValue());
        dataBuf.put(ByteUtil.intToByteArray(readSeq));
        dataBuf.put(ByteUtil.intToByteArray(threadKeyBytes.length));
        dataBuf.put(threadKeyBytes);
        dataBuf.flip();

        sendMessage(dataBuf.array());
    }

    private void sendReqDeleteSeq(String threadKey, int readSeq) throws IOException {

        byte[] threadKeyBytes = threadKey.getBytes("UTF-8");
        byte[] deleteMsgBytes = MineTalkApp.getCurrentActivity().getResources().getString(R.string.chat_message_deleted).getBytes("UTF-8");

        int length = 1 +
                4 +
                threadKeyBytes.length + 4 +
                4 + deleteMsgBytes.length;

        ByteBuffer dataBuf = ByteBuffer.allocate(length);

        dataBuf.put(SendDataType.SEND_TYPE_DELETE_SEQ.getValue());
        dataBuf.put(ByteUtil.intToByteArray(threadKeyBytes.length));
        dataBuf.put(threadKeyBytes);
        dataBuf.put(ByteUtil.intToByteArray(readSeq));
        dataBuf.put(ByteUtil.intToByteArray(deleteMsgBytes.length));
        dataBuf.put(deleteMsgBytes);

        dataBuf.flip();

        sendMessage(dataBuf.array());
    }

    /**
     * MessageModel을 소켓전송을 위한 바이트배열로 변환한다.
     * @param threadKey
     * @param messageData
     * @return
     */
    private byte[] messageModelToBytes(final String threadKey , final MessageData messageData) {

        byte[] messageByte = null;

        try {

            HashMap<String, Object> message = new HashMap<String, Object>() {{
                put("threadKey", threadKey);
                put("previewText", messageDecode.getPreviewText(messageData.getMessage(), messageData.getCategory()));
                put("category", messageData.getCategory());
                put("message", messageData.getMessage());
            }};

            byte[] bodyByte = OBJECT_MAPPER.writeValueAsBytes(message);

            int length = 1 +
                    4 + // db index
                    bodyByte.length + 4; // body

            ByteBuffer messageBuf = ByteBuffer.allocate(length);

            messageBuf.put(SendDataType.SEND_TYPE_MESSAGE.getValue());
            messageBuf.put(ByteUtil.intToByteArray(messageData.getDbIndex()));
            messageBuf.put(ByteUtil.intToByteArray(bodyByte.length));
            messageBuf.put(bodyByte);

            messageBuf.flip();

            messageByte = messageBuf.array();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return messageByte;
    }

    /**
     * 전송받은 데이터를 처리한다.
     * @param buffer 전송받은 데이터 버퍼
     */
    private void handleReceiveData(byte[] buffer) {

        if(0 == buffer.length) {
            return;
        }

        int type = (int)buffer[0];
        Handler handler = receiveHandlerMap.get(type);
        if(null != handler) {
            handler.handlerData(buffer);
        }
    }

    /**
     * 정상적으로 입장하였다는 JOIN 메시지 보내기
     */
    private void handleReadyPacket() {
        // 조인데이터 전송
        try {
            sendJoinData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleDeleteMsgPacket(byte[] buffer) {

        int position = 1;
        int threadKeyLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] threadKeyByte = Arrays.copyOfRange(buffer, position, position + threadKeyLength);
        position += threadKeyLength;

        int readSeq = ByteUtil.byteToInt(buffer, position);

        try {
            String threadKey = new String(threadKeyByte, "UTF-8");

            //MessageThreadManager.getInstance().updateThreadMemberReadSeq(context, threadKey, xid, readSeq);

            MessageData messageData = MessageThreadManager.getInstance().getMessageBySeq(context, threadKey, readSeq);
            // 삭제(내가 보낸 메시지만)
            MessageJSonModel deleteJson = new MessageJSonModel();
            try {
                JSONObject msgObject = new JSONObject(messageData.getMessage());
                deleteJson = MessageJSonModel.parse(msgObject);
            } catch (JSONException e) {
                e.printStackTrace();
                deleteJson = null;
            }

            String updateTextMessage = makeSendMessage(deleteJson.getText(), deleteJson.getLanguageCode(), deleteJson.getMsgType(), deleteJson.getExtra(),
                    deleteJson.getSenderProfileUrl(), deleteJson.getWidth(), deleteJson.getHeight(), deleteJson.getContactName(), deleteJson.getContactHp(), "Y");

            MessageThreadManager.getInstance().updateMessageDelete(context, threadKey, readSeq, updateTextMessage);

            if(isCurrentThread(threadKey)) {

                if(context instanceof Activity) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ThreadChannelApi threadApi = new ThreadChannelApi(context, threadKey);
                            threadApi.request(new BaseHttpRequest.APICallbackListener<ThreadWithChannel>() {
                                @Override
                                public void onSuccess(ThreadWithChannel res) {
                                    if(res != null) {
                                        setThreadMemberData(res.getMembers());
                                        reLoadMessageList(context);
                                        threadEventListener.refreshChatList();
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                                }
                            });
                        }
                    });
                }


                //this.memberMap.get(xid).setReadSeq(readSeq);
                //this.threadEventListener.refreshChatList();
            }
        }
        catch (Exception e) {}
    }

    private void handleReadSeqPacket(byte[] buffer) {

        int position = 1;
        int threadKeyLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] threadKeyByte = Arrays.copyOfRange(buffer, position, position + threadKeyLength);
        position += threadKeyLength;

        int xidLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] xidByte = Arrays.copyOfRange(buffer, position, position + xidLength);
        position += xidLength;

        int readSeq = ByteUtil.byteToInt(buffer, position);

        try {
            String threadKey = new String(threadKeyByte, "UTF-8");
            String xid = new String(xidByte, "UTF-8");

            MessageThreadManager.getInstance().updateThreadMemberReadSeq(context, threadKey, xid, readSeq);

            if(isCurrentThread(threadKey)) {

                if(context instanceof Activity) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ThreadChannelApi threadApi = new ThreadChannelApi(context, threadKey);
                            threadApi.request(new BaseHttpRequest.APICallbackListener<ThreadWithChannel>() {
                                @Override
                                public void onSuccess(ThreadWithChannel res) {
                                    if(res != null) {
                                        setThreadMemberData(res.getMembers());
                                        reLoadMessageList(context);
                                        threadEventListener.refreshChatList();
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                                }
                            });
                        }
                    });
                }


                //this.memberMap.get(xid).setReadSeq(readSeq);
                //this.threadEventListener.refreshChatList();
            }
        }
        catch (Exception e) {}
    }

    private void handleSeqPacket(byte[] buffer) {

        int position = 1;
        int threadKeyLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] threadKeyByte = Arrays.copyOfRange(buffer, position, position + threadKeyLength);
        position += threadKeyLength;

        int serverSeq = ByteUtil.byteToInt(buffer, position);

        try {

            String threadKey = new String(threadKeyByte, "UTF-8");

            if(threadKey.equals(threadData.getThreadKey())) {
                if(0 == serverSeq) {
                    /**
                     * 서버에서 받은 시퀀스가 0일 경우 데이터를 로드하지 않고, More Message 버튼을 제거
                     */
                    threadEventListener.showMoreButton(false);
                } else if(0 < serverSeq) {

                    threadEventListener.showMoreButton(true);
                    /**
                     * 서버에서 받은 시퀀스가 0보다 클 경우 로컬디비의 라스트 시퀀스를 검사한다.
                     */
                    MessageData message = MessageThreadManager.getInstance().getLastMessage(context, threadData.getThreadKey());

                    final int lastSeq = (message == null) ? 0 : message.getSeq();

                    if(lastSeq > 0 && messageModels.isEmpty())
                        loadMessageModels(context, Integer.MAX_VALUE);
//JEROME
                    if(0 < lastSeq && lastSeq < serverSeq) {
                        /**
                         * 로컬 라스트 시퀀스보다 서버 라스트 시퀀스가 클 경우 서버에서 새로운 메시지를 로드하여 로컬에 인서트 완료하고 로컬에서 100개를 불러온다.
                         */
                        if(context instanceof Activity) {
                            ((Activity)context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    requestNewMessageApi(threadData.getThreadKey(), lastSeq);
                                }
                            });
                        }
                    } else {
                        threadEventListener.refreshChatList();
                        // JEROME추가
//                        if(lastSeq == 0 && serverSeq > 0) {
//                            if(context instanceof Activity) {
//                                ((Activity)context).runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        requestNewMessageApi(threadData.getThreadKey(), lastSeq);
//                                    }
//                                });
//                            }
//                        } else {
//                            threadEventListener.refreshChatList();
//                        }
                    }
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleMessagePacket(byte[] buffer) throws IOException {

        MessageData messageData = new MessageData();

        int position = 1;
        int threadKeyLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] threadKeyByte = Arrays.copyOfRange(buffer, position, position + threadKeyLength);

        String threadKey = new String(threadKeyByte, "UTF-8");

        position += threadKeyLength;

        int xidLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] xidByte = Arrays.copyOfRange(buffer, position, position + xidLength);

        position += xidLength;

        int seq = ByteUtil.byteToInt(buffer, position);

        position += 4;

        int category = ByteUtil.byteToInt(buffer, position);

        position += 4;

        int messageLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] messageByte = Arrays.copyOfRange(buffer, position, position + messageLength);

        position += messageLength;

        int nickNameLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] nickNameByte = Arrays.copyOfRange(buffer, position, position + nickNameLength);

        position += nickNameLength;

        long sendTime = ByteUtil.byteToLong(buffer, position);

        messageData.setSeq(seq);
        messageData.setCategory(category);
        messageData.setMessage(new String(messageByte, "UTF-8"));
        messageData.setState(MessageConst.SEND_STATE_COMPLETE);
        messageData.setXid(new String(xidByte, "UTF-8"));
        messageData.setType((short) MessageType.MESSAGE_TYPE_USER.getValue());
        messageData.setSendDate(new Date(sendTime));
        messageData.setNickName(new String(nickNameByte, "UTF-8"));

        MessageThreadManager.getInstance().appendMessage(context, threadKey, messageData);

        if(isCurrentThread(threadKey)) {
            addMessageModel(messageData);
            //read seq
            sendReadSeq(threadKey, seq);
        }else{ //다른 Thread에서 메세지를 보냈을 경우 푸쉬를 띄운다.

            if(context instanceof Activity) {
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ThreadSecurityListApi threadListApi = new ThreadSecurityListApi(MineTalkApp.getAppContext(), MineTalkApp.getUserInfoModel().getUser_xid());
                        threadListApi.request(new BaseHttpRequest.APICallbackListener<List<ThreadData>>() {
                            @Override
                            public void onSuccess(List<ThreadData> res) {

                                MessageJSonModel msgModel = new MessageJSonModel();
                                try {
                                    JSONObject msgObject = new JSONObject(messageData.getMessage());
                                    msgModel = MessageJSonModel.parse(msgObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                String title = MineTalkApp.getCurrentActivity().getString(R.string.app_name);
                                String message = StringEscapeUtils.unescapeJava(msgModel.getText());
                                String userName = messageData.getNickName();

                                boolean isSecurityMode = false;
                                for (ThreadData data : res) {
                                    //보안채팅일 경우
                                    if(data.getThreadKey().equals(threadKey)){
                                        isSecurityMode = true;
                                        break;
                                    }

                                }

                                //MineTalk.PUSH_SECURITY_THREAD_KEY = isSecurityMode ? "Y" : "N";
                                sendNotification(title, isSecurityMode ? MineTalkApp.getAppContext().getResources().getString(R.string.push_security_msg) : (userName + " : " + message),
                                        null, threadKey, "", isSecurityMode ? "Y" : "N");

                                MineTalkApp.getCurrentActivity().sendBroadcast(new Intent(MineTalk.BROAD_CAST_NEW_MESSAGE));
                            }

                            @Override
                            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    }

    private void handleSystemPacket(byte[] buffer) throws IOException {

        int position = 1;
        int threadKeyLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] threadKeyByte = Arrays.copyOfRange(buffer, position, position + threadKeyLength);

        final String threadKey = new String(threadKeyByte, "UTF-8");

        position += threadKeyLength;

        int seq = ByteUtil.byteToInt(buffer, position);

        position += 4;

        int messageLength = ByteUtil.byteToInt(buffer, position);

        position += 4;

        byte[] messageByte = Arrays.copyOfRange(buffer, position, position + messageLength);

        position += messageLength;

        long sendTime = ByteUtil.byteToLong(buffer, position);

        final MessageData messageData = new MessageData();
        messageData.setSeq(seq);
        messageData.setCategory(0);
        messageData.setMessage(new String(messageByte, "UTF-8"));
        messageData.setState(MessageConst.SEND_STATE_COMPLETE);
        messageData.setXid("0");
        messageData.setType((short) MessageType.MESSAGE_TYPE_SYSTEM.getValue());
        messageData.setSendDate(new Date(sendTime));

        MessageThreadManager.getInstance().appendMessage(context, threadKey, messageData);

        if(isCurrentThread(threadKey)) {
            MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshMember(new BaseHttpRequest.APICallbackListener<List<ThreadMember>>() {
                        @Override
                        public void onSuccess(List<ThreadMember> res) {

                            addMessageModel(messageData);
                        }

                        @Override
                        public void onFailure(int statusCode, byte[] responseBody, Throwable error) {

                            addMessageModel(messageData);
                        }
                    });
                }
            });
        }
    }

    private void handleSendCallbackPacket(byte[] buffer) {

        int position = 1;
        int dbIndex = ByteUtil.byteToInt(buffer, position);

        position += 4;

        int seq = ByteUtil.byteToInt(buffer, position);

        Log.e("@@@@TAG", "CALL BACK DBINDEX : " + String.valueOf(dbIndex));
        Log.e("@@@@TAG", "CALL BACK SEQ : " + String.valueOf(seq));

        MessageThreadManager.getInstance().updateSendMessageSeq(context, dbIndex, seq);
        MessageThreadManager.getInstance().updateThreadMemberReadSeq(context, threadData.getThreadKey(), currentMember.getXid(), seq);

        if(threadEventListener != null) {
            reLoadMessageList(context);
            threadEventListener.refreshChatList();
        }

        // JEROME 추가 2019.03.29///
//        try {
//            sendReadSeq(threadData.getThreadKey(), seq);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        ///////////////////////////
    }

    public void updateTranslaitonMsg(String threadKey, int seq, String transJson) {
        MessageThreadManager.getInstance().updateMsgTranslation(context, threadKey, seq, transJson);
        messageModels.clear();
        loadMessageModels(context, Integer.MAX_VALUE);
    }

    private String makeSendMessage(String text, String langCode, String msgType, String extra, String fileUrl, String width, String height, String contactName, String contactHp, String deleteYn) {
        try {

            String senderName = MineTalkApp.getUserInfoModel().getUser_name();
            String senderHp = MineTalkApp.getUserInfoModel().getUser_hp();
            //String senderProfileUrl = MineTalkApp.getUserInfoModel().getUser_profile_image();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("text", text);
            jsonObject.put("langCode", langCode);
            jsonObject.put("msgType", msgType);
            jsonObject.put("extra", extra);
            jsonObject.put("fileUrl", fileUrl);
            jsonObject.put("width", "");
            jsonObject.put("height", "");
            jsonObject.put("contactName", contactName);
            jsonObject.put("contactHp", contactHp);
            jsonObject.put("senderName", senderName);
            jsonObject.put("senderHp", senderHp);
            jsonObject.put("senderProfileUrl", fileUrl);
            jsonObject.put("deleteYn", deleteYn);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return text;
    }

    public interface Handler {
        public void handlerData(byte[] buffer);
    }

    public static interface ThreadEventListner {

        /**
         * 스레드 서버에 접속혹은 접속 실패시 호출
         * @param connected true라면 접속 성공
         */
        public void onConnectResult(boolean connected);

        /**
         * 연결이 완료되면 발생
         */
        public void onConnected();

        /**
         * 연결 해재 완료 발생
         */
        public void onDisconnected();

        /**
         * 쓰레드 서버와 통신시 오류 발생할때 호출
         * @param exception 오류 정보
         */
        public void onErrorOccur(Exception exception);

        /**
         * 현재 화면 갱신
         * - 신규 메시지 및 메시지 리스트 변경
         * - 사용자 ReadSeq 변경
         * - 스레드 사용자 변경
         */
        public void refreshChatList();

        /**
         * 더보기 버튼 숨김 여부
         * @param visibility
         */
        public void showMoreButton(boolean visibility);

        /**
         * 스크롤 포지션 이동
         * @param position
         */
        public void setScrollLastposition(int position);

        public void onAppendMessage(MessageData messageData);
    }

    /**
     * New Message API
     * @param threadKey
     */
    private void requestNewMessageApi(final String threadKey, final int lastSeq) {

        NewThreadMessageApi api = new NewThreadMessageApi(context, threadKey, lastSeq);
        api.request(new BaseHttpRequest.APICallbackListener<List<MessageData>>() {
            @Override
            public void onSuccess(List<MessageData> res) {

                if(!res.isEmpty()) {
                    MessageThreadManager.getInstance().appendMessages(context, threadKey, res);
                    addMessageModels(res);
                }
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                Log.d("NewThreadMessageApi : ", error.toString());
            }
        });
    }

    /**
     * Prev Message API
     * @param threadKey
     */
    private void requestPrevMessageApi(final String threadKey, int lastSeq) {

        ThreadPrevMessageApi api = new ThreadPrevMessageApi(context, threadKey, lastSeq);
        api.request(new BaseHttpRequest.APICallbackListener<List<MessageData>>() {
            @Override
            public void onSuccess(List<MessageData> res) {

                if(!res.isEmpty()) {

                    MessageThreadManager.getInstance().appendMessages(context, threadKey, res);
                    addMessageModelsFirst(res);
                }
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {

            }
        });
    }

    /**
     * 메시지 전송
     * @param messageJson 메시지 JSON
     * @param category 메시지 카테고리
     */
    public void sendMessage(String messageJson, int category, String nickName) {
        MessageData messageData = new MessageData();

        messageData.setCategory(category);
        messageData.setMessage(messageJson);
        messageData.setSendDate(new Date());
        messageData.setType((short) MessageType.MESSAGE_TYPE_USER.getValue());
        messageData.setXid(currentMember.getXid());
        messageData.setSeq(Integer.MAX_VALUE);
        messageData.setNickName(nickName);


        sendMessage(messageData);
    }

    public void sendMessage(String messageJson, int category, String nickName, String threadKey) {
        MessageData messageData = new MessageData();

        messageData.setCategory(category);
        messageData.setMessage(messageJson);
        messageData.setSendDate(new Date());
        messageData.setType((short) MessageType.MESSAGE_TYPE_USER.getValue());
        messageData.setXid(currentMember.getXid());
        messageData.setSeq(Integer.MAX_VALUE);
        messageData.setNickName(nickName);


        sendMessage(messageData, threadKey);
    }

    /**
     * 현재 스레드인지 여부 체크
     * @param threadKey 스레드키
     * @return 현재 스레드 여부
     */
    public boolean isCurrentThread(String threadKey) {
        return threadKey.equals(threadData.getThreadKey());
    }

    /**
     * 스레드 멤버 갱신
     * @param callbackListener callback listener
     */
    public void refreshMember(BaseHttpRequest.APICallbackListener<List<ThreadMember>> callbackListener) {
        ThreadMemberApi api = new ThreadMemberApi(context, threadData.getThreadKey());
        api.request(callbackListener);
    }


    /**
     * UI 사용 ( 메시지별로 읽지 않는 수 에 대한 조회 )
     * @param seq 메시지 seq
     * @return
     */
    public int unReadCountByMessageSeq(int seq) {
        int unReadCount = 0;
        Set<String> keys = memberMap.keySet();

        String loginUserXid = MineTalkApp.getUserInfoModel().getUser_xid();
        for (String key : keys) {
            if(memberMap.get(key).getReadSeq() < seq && !loginUserXid.equals(memberMap.get(key).getXid())) {
                unReadCount++;
            }
        }

        return unReadCount;
    }

    /**
     * Thread 에 포함된 Xid 에 대한 사용자 정보 조회
     * @param xid Xid
     * @return 사용자 정보
     */
    public ThreadMember getMemberByXid(String xid) {
        return memberMap.get(xid);
    }

    /**
     * Thread 에 포함된 멤버 수 조회
     * @return Thread 내 멤버 수
     */
    public int getMemberCount() {
        return memberMap.size();
    }


    private void sendNotification(String title, String body, String image, String threadKey, String adIdx, String securityMode) {

        Intent intent = new Intent(MineTalkApp.getCurrentActivity(), MainActivity.class);

        //MineTalk.PUSH_THREAD_KEY = threadKey;

        if(!threadKey.equals("")) {
            intent.putExtra("threadKey", threadKey);
        }

        if(!securityMode.equals("")) {
            intent.putExtra("securityMode", securityMode);
        }

        if(!adIdx.equals("")) {
            MineTalk.AD_IDX = adIdx;
            MineTalk.AD_TITLE = title;

            intent.putExtra("adIdx", adIdx);
            intent.putExtra("adTitle", title);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        PendingIntent pendingIntent = PendingIntent.getActivity(MineTalkApp.getCurrentActivity(), 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = MineTalkApp.getCurrentActivity().getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(MineTalkApp.getCurrentActivity(), channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        if (!TextUtils.isEmpty(title)) {
            notificationBuilder.setContentTitle(title);
        }
        if (!TextUtils.isEmpty(body)) {
            notificationBuilder.setContentText(body);
        }


        if(image == null || image.equals("")) {
            NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle(notificationBuilder);
            style.bigText(body).setBigContentTitle(title);
        } else {
            Bitmap bigPicture = null;
            try {
                URL url = new URL(image);
                bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (Exception e) {
                e.printStackTrace();
                bigPicture = null;
            }

            if(bigPicture != null) {
                NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle(notificationBuilder);
                bigPictureStyle.bigPicture(bigPicture).setBigContentTitle(title).setSummaryText(body);
            } else {
                NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle(notificationBuilder);
                style.bigText(body).setBigContentTitle(title);
            }
        }

        NotificationManager notificationManager = (NotificationManager) MineTalkApp.getCurrentActivity().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        if(!threadKey.equals("")) {
            notificationManager.notify(threadKey, 0 /* ID of notification */, notificationBuilder.build());
        } else {
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }
}