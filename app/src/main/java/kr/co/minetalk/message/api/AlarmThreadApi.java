package kr.co.minetalk.message.api;

import android.content.Context;

import java.util.HashMap;


public class AlarmThreadApi extends BaseHttpRequest<Void> {
    private static final String PATH = "/members/%s/threads/%s/alarm";

    public AlarmThreadApi(Context context, final String xid, final String threadKey, final boolean enable) {
        super(context, PATH, new String[]{xid, threadKey}, "PUT");

//        this.reqObject = new HashMap<String, Object>() {{
//            addParameter("enable", enable);
//        }};

        this.addParameter("enable",  String.valueOf(enable));

    }
}
