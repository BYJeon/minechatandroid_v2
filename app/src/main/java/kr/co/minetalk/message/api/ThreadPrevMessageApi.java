package kr.co.minetalk.message.api;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;

import kr.co.minetalk.message.model.MessageData;

import java.util.List;

public class ThreadPrevMessageApi extends BaseHttpRequest<List<MessageData>> {
    private static final String PATH = "/threads/%s/messages/prev?seq=%s";

    public ThreadPrevMessageApi(Context context, String threadKey, int seq) {
        super(context, PATH, new String[]{threadKey, String.valueOf(seq)}, "GET");

//        addParameter("seq", String.valueOf(seq));

        this.registTypeRef(new TypeReference<List<MessageData>>() {
        });
    }
}
