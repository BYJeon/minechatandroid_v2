package kr.co.minetalk.message.api;

import android.content.Context;

import java.util.HashMap;

/**
 * Created by episode on 2018. 10. 26..
 */

public class JoinThreadApi extends BaseHttpRequest<Void> {
    private static final String PATH = "/threads/join";

    public JoinThreadApi(Context context, final String xid, final String threadKey) {
        super(context, PATH, "POST");

        this.reqObject = new HashMap<String, Object>() {{
            put("xid", xid);
            put("threadKey", threadKey);
        }};
    }
}
