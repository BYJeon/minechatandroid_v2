package kr.co.minetalk.message.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static SimpleDateFormat simpleDateFormat;

	private static class TIME_MAXIMUM {

		public static final int SEC = 60;
		public static final int MIN = 60;
		public static final int HOUR = 24;
		public static final int DAY = 30;
		public static final int MONTH = 12;
	}

	/**
	 * 채팅 타임스탬프
	 * @param timpStamp
	 * @return
	 */
    public static String calculateTime(long timpStamp) {

        simpleDateFormat = new SimpleDateFormat("h:mm a");

        Date date = new Date(timpStamp);

        return simpleDateFormat.format(date);
    }

    /**
     * 채팅 섹션 날짜
     * @param timpStamp
     * @return
     */
    public static String getSectionTime(long timpStamp) {

        String dateString;

        simpleDateFormat = new SimpleDateFormat("MM월 dd, yyyy");

        Date dateValue = new Date(timpStamp);

        Date dateToday = new Date(System.currentTimeMillis());

        if(dateValue.getYear() == dateToday.getYear()
                && dateValue.getMonth() == dateToday.getMonth()
                && dateValue.getDate() == dateToday.getDate()) {
            dateString = "Today";
        } else {
            dateString = simpleDateFormat.format(dateValue);
        }

        return dateString;
    }

    /**
     * 채팅 섹션 날짜 debug
     * @param timpStamp
     * @return
     */
    public static String getSectionTime(String timpStamp) {

        return timpStamp;
    }

	public static TimeModel getRemainTime(long remainTime) {

		TimeModel model = new TimeModel();
        int day = (int)(remainTime / (TIME_MAXIMUM.SEC * TIME_MAXIMUM.MIN * 24));
		int hour = (int)(remainTime / (TIME_MAXIMUM.SEC * TIME_MAXIMUM.MIN)) % 24;
		int minute = (int)((remainTime % (TIME_MAXIMUM.SEC * TIME_MAXIMUM.MIN)) / TIME_MAXIMUM.MIN);
		int second = (int)(remainTime % TIME_MAXIMUM.SEC);

        model.setDay(day);
		model.setSecond(second);
		model.setMinute(minute);
		model.setHour(hour);

		return model;
	}

	public static class TimeModel {

        private int day;
		private int hour;
		private int minute;
		private int second;

		public TimeModel() {

		}

        public String toString() {

            String timeString = "";
            if(0 < day) {
                timeString += day + "일 ";
            }

            if(0 < hour) {
                timeString += hour + "시간 ";
            }

            if(0 < minute) {
                timeString += minute + "분 ";
            }

            if(0 == day && 0 == hour && 0 == minute) {
                timeString = "잠시";
            }

            return timeString;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

		public int getHour() {
			return hour;
		}

		public void setHour(int hour) {
			this.hour = hour;
		}

		public int getMinute() {
			return minute;
		}

		public void setMinute(int minute) {
			this.minute = minute;
		}

		public int getSecond() {
			return second;
		}

		public void setSecond(int second) {
			this.second = second;
		}

	}
}
