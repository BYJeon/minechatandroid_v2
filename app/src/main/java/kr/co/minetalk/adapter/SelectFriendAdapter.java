package kr.co.minetalk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.view.image.CornerRoundImageView;

public class SelectFriendAdapter extends BaseAdapter {

    private ArrayList<FriendListModel> mDataList = new ArrayList<>();

    public void setData(ArrayList<FriendListModel> arrayList) {
        this.mDataList.clear();
        this.mDataList.addAll(arrayList);
        notifyDataSetChanged();
    }

    public void setSelectIndex(int position) {
        boolean isSelect = !mDataList.get(position).isIs_select();
        mDataList.get(position).setIs_select(isSelect);

        notifyDataSetChanged();
    }

    public ArrayList<FriendListModel> getSelectData() {
        ArrayList<FriendListModel> selectList = new ArrayList<>();
        for(int i = 0 ; i < mDataList.size() ; i++) {
            if(mDataList.get(i).isIs_select()) {
                selectList.add(mDataList.get(i));
            }
        }
        return selectList;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();

        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_select_user_list, parent, false);

            viewHolder.mIvUserImage = (CornerRoundImageView) convertView.findViewById(R.id.iv_user_image);
            viewHolder.mTvUserName = (TextView) convertView.findViewById(R.id.tv_user_name);
            viewHolder.mTvStateMessage = (TextView) convertView.findViewById(R.id.tv_state_message);
            viewHolder.mIvChecked = (ImageView) convertView.findViewById(R.id.iv_checked);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        FriendListModel item = mDataList.get(position);

        String profileImgUrl = item.getUser_profile_image();
        if(profileImgUrl == null || profileImgUrl.equals("")) {
            Glide.with(context).load(R.drawable.img_basic).into(viewHolder.mIvUserImage);
        } else {
            Glide.with(context).load(item.getUser_profile_image()).into(viewHolder.mIvUserImage);
        }

        viewHolder.mTvUserName.setText(item.getUser_name());;
        viewHolder.mIvChecked.setSelected(item.isIs_select());

        return convertView;
    }

    class ViewHolder {
        CornerRoundImageView mIvUserImage;
        TextView mTvUserName;
        TextView mTvStateMessage;
        ImageView mIvChecked;

    }
}
