package kr.co.minetalk.adapter.listener;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.minetalk.R;
import kr.co.minetalk.api.model.PointHistoryModel;
import kr.co.minetalk.utils.CommonUtils;

public class PointListAdapter extends BaseAdapter {


    private ArrayList<PointHistoryModel> mDataList = new ArrayList<>();

    public void setData(ArrayList<PointHistoryModel> arrayList) {
        this.mDataList.clear();
        this.mDataList.addAll(arrayList);

        notifyDataSetChanged();
    }

    public void clear() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();


        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.report_list_item, parent, false);

            viewHolder.mTvMsg1 = (TextView) convertView.findViewById(R.id.tv_msg_1);
            //viewHolder.mTvMsg2 = (TextView) convertView.findViewById(R.id.tv_msg_2);
            viewHolder.mTvRegDate = (TextView) convertView.findViewById(R.id.tv_date);
            viewHolder.mTvPoint = (TextView) convertView.findViewById(R.id.tv_point);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PointHistoryModel item = mDataList.get(position);

        if(item.getAction_gubun().equals("U")) {
            // 차감
            viewHolder.mTvPoint.setVisibility(View.VISIBLE);
            viewHolder.mTvPoint.setBackgroundResource(R.drawable.point_round_box_e36f6f);
            String point = CommonUtils.comma_won(item.getPoint_amount());
            viewHolder.mTvPoint.setText("-" + point);
        } else if(item.getAction_gubun().equals("A")) {
            // 증감
            viewHolder.mTvPoint.setVisibility(View.VISIBLE);
            viewHolder.mTvPoint.setBackgroundResource(R.drawable.point_round_box_007cba);
            String point = CommonUtils.comma_won(item.getPoint_amount());
            viewHolder.mTvPoint.setText(point);
        } else {
            viewHolder.mTvPoint.setVisibility(View.INVISIBLE);
            viewHolder.mTvPoint.setText("");
        }

        viewHolder.mTvMsg1.setText(item.getAction_type() + " " + (item.getUser_name()));
        //viewHolder.mTvMsg2.setText(item.getUser_name());
        viewHolder.mTvRegDate.setText(item.getReg_date());

        return convertView;
    }

    class ViewHolder {
        TextView mTvMsg1;
        TextView mTvMsg2;
        TextView mTvRegDate;
        TextView mTvPoint;

    }
}
