package kr.co.minetalk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.image.CornerRoundImageView;

public class SelectChatListAdapter extends BaseAdapter {

    private ArrayList<ThreadData> mDataList = new ArrayList<>();
    private SimpleDateFormat mDtFormat = new SimpleDateFormat("HH:mm");
//    private SimpleDateFormat mDtFormat = new SimpleDateFormat("mm-dd");

    private HashMap<String, String> mContactMap = new HashMap<>();

    private String mSelectThreadKey = "";

    public SelectChatListAdapter() {
        this.mContactMap.clear();
        mContactMap = FriendManager.getInstance().selectFriendXidToNameMap();
    }

    public void setData(ArrayList<ThreadData> arrayList) {
        this.mDataList.clear();
        this.mDataList.addAll(arrayList);
        notifyDataSetChanged();
    }

    public void clear() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }

    public void setSelectThread(String threadKey) {

        if(!mSelectThreadKey.equals(threadKey)) {
            this.mSelectThreadKey = threadKey;
        } else {
            this.mSelectThreadKey = "";
        }

        notifyDataSetChanged();
    }

    public String getSelectThreadKey() {
        return this.mSelectThreadKey;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();


        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.select_chat_list_item, parent, false);

            viewHolder.mIvUserImage = (CornerRoundImageView) convertView.findViewById(R.id.iv_user_image);
            viewHolder.mTvUsers = (TextView) convertView.findViewById(R.id.tv_user_name);
            viewHolder.mTvLastMsg = (TextView) convertView.findViewById(R.id.tv_last_msg);
            viewHolder.mTvMsgTime = (TextView) convertView.findViewById(R.id.tv_msg_time);
            viewHolder.mIvChecked = (ImageView) convertView.findViewById(R.id.iv_checked);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ThreadData item = mDataList.get(position);


        if(this.mSelectThreadKey.equals(item.getThreadKey())) {
            viewHolder.mIvChecked.setSelected(true);
        } else {
            viewHolder.mIvChecked.setSelected(false);
        }



        // 1:1 대화방
        if(item.getIsDirect().toUpperCase().equals("Y")) {
            String myChatThreadKey = Preferences.getMyChattingThreadKey();

            if(item.getThreadKey().equals(myChatThreadKey)) {
                viewHolder.mTvUsers.setText(context.getResources().getString(R.string.popup_profile_button_chat_me));
            } else {

                String chatName = Preferences.getChattingName(item.getThreadKey());
                if(!chatName.equals("")) {
                    viewHolder.mTvUsers.setText(chatName);
                } else {
                    if(item.getMembers().size() > 0) {
                        String members = "";
                        for(ThreadMember tmember : item.getMembers()) {
                            if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                String contactName = mContactMap.get(tmember.getXid());

                                String userProfile = tmember.getProfileImageUrl();
                                if(userProfile == null || userProfile.equals(userProfile.equals(""))) {
                                    Glide.with(context).load(R.drawable.img_basic).into(viewHolder.mIvUserImage);
                                } else {
                                    Glide.with(context).load(userProfile).into(viewHolder.mIvUserImage);
                                }

                                if(contactName == null || contactName.equals("")) {
                                    members = tmember.getNickName();
                                } else {
                                    members = contactName;
                                }
                                break;
                            }

                        }
                        viewHolder.mTvUsers.setText(members);
                    }
                }
            }
        } else {
            // 단체방
            String myChatThreadKey = Preferences.getMyChattingThreadKey();
            if(item.getThreadKey().equals(myChatThreadKey)) {
                viewHolder.mTvUsers.setText(context.getResources().getString(R.string.popup_profile_button_chat_me));
            } else {

                String chatName = Preferences.getChattingName(item.getThreadKey());
                if(!chatName.equals("")) {
                    viewHolder.mTvUsers.setText(chatName);
                } else {
                    if(item.getMembers().size() > 0) {
                        String members = "";
                        int count = 0;
                        for(ThreadMember tmember : item.getMembers()) {
                            String contactName = mContactMap.get(tmember.getXid());
                            if(contactName == null || contactName.equals("")) {
                                members = members + tmember.getNickName();
                            } else {
                                members = members + contactName;
                            }

                            if(count != (item.getMembers().size() - 1)) {
                                members = members + ",";
                            }
                            count++;
                        }

                        viewHolder.mTvUsers.setText(members);
                    }
                }
            }

        }

//        viewHolder.mTvLastMsg.setText(item.getLastMessage());
        MessageJSonModel msgModel = new MessageJSonModel();
        try {
            JSONObject msgObject = new JSONObject(item.getLastMessage());
            msgModel = MessageJSonModel.parse(msgObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(msgModel.getMsgType().equals(MineTalk.MSG_TYPE_TEXT) ||
                msgModel.getMsgType().equals(MineTalk.MSG_TYPE_URL)) {
            if(msgModel.getText().equals("")) {
                viewHolder.mTvLastMsg.setText(context.getString(R.string.msg_type_emoticon));
            } else {
                //viewHolder.mTvLastMsg.setText(msgModel.getText());
                viewHolder.mTvLastMsg.setText(StringEscapeUtils.unescapeJava(msgModel.getText()));
            }
        } else if(msgModel.getMsgType().equals(MineTalk.MSG_TYPE_IMAGE)) {
            viewHolder.mTvLastMsg.setText(context.getString(R.string.msg_type_image));
        } else if(msgModel.getMsgType().equals(MineTalk.MSG_TYPE_VIDEO)) {
            viewHolder.mTvLastMsg.setText(context.getString(R.string.msg_type_video));
        } else if(msgModel.getMsgType().equals(MineTalk.MSG_TYPE_CONTACT)) {
            viewHolder.mTvLastMsg.setText(context.getString(R.string.msg_type_contact));
        }


        return convertView;
    }

    class ViewHolder {
        CornerRoundImageView mIvUserImage;

        TextView mTvUsers;
        TextView mTvLastMsg;
        TextView mTvMsgTime;
        ImageView mIvChecked;
    }

    private int getUnReadCount(int lastSeq, List<ThreadMember> members, String userXid) {
        int readSeq = 0;
        for(ThreadMember tmember : members) {
            if(tmember.getXid().equals(userXid)) {
                readSeq = tmember.getReadSeq();
            }
        }

        return lastSeq - readSeq;
    }
}
