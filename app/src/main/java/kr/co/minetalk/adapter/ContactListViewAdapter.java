package kr.co.minetalk.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.GridContactItemBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.databinding.ListContactItemBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.popup.SimplePopup;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class ContactListViewAdapter extends RecyclerView.Adapter<ContactListViewAdapter.ContactViewHolder> {
    public static enum ViewType {
        NORMAL,
        SEARCH
    }

    private ArrayList<ContactModel> items;
    private OnFriendListViewListener mFriendListener = null;
    private OnFragmentEventListener mFragmentEventListener = null;

    private ViewType mViewType = ViewType.NORMAL;

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0){
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ContactViewHolder(binding);
        }

        ListContactItemBinding binding = ListContactItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ContactViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ContactListViewAdapter.ContactViewHolder holder, int position) {
        if(position == 0){
            //ContactModel item = items.get(position);
            holder.bind(null);
        }else {
            ContactModel item = items.get(position-1);
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

//    public static void bindItem(RecyclerView recyclerView, ArrayList<ContactModel> items) {
//        ContactListViewAdapter adapter = (ContactListViewAdapter) recyclerView.getAdapter();
//
//        if (adapter != null) {
//            adapter.setItem(items);
//        }
//    }

    public void setListener(OnFriendListViewListener listener){
        this.mFriendListener = listener;
    }

    public void setFragmentListener(OnFragmentEventListener listener){
        this.mFragmentEventListener = listener;
    }

    public void addItem(ArrayList<ContactModel> items) {
        if (this.items != null && items != null)
            this.items.addAll(items);
        else
            this.items = items;

        notifyDataSetChanged();
    }

    public void addItem(ContactModel items) {
        if (this.items != null && items != null)
            this.items.add(items);
        //notifyDataSetChanged();
    }


    public void setViewType(ViewType type){
        this.mViewType = type;
    }
    public void setItem(ArrayList<ContactModel> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    public ArrayList<ContactModel> getItem(){return items;}
    /**
     * View holder to display each RecylerView item
     */
    protected class ContactViewHolder extends RecyclerView.ViewHolder {
        ListContactItemBinding mBinding;

        LayoutListHeaderViewBinding mHeaderBinding;
        boolean bDescOrder;

        public ContactViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            this.mHeaderBinding = binding;
        }

        public ContactViewHolder(ListContactItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(ContactModel item){

            if(mBinding == null){ //Header 영역
                bDescOrder = Preferences.getContactSettingOrder();
                mHeaderBinding.btSortList.setText(bDescOrder ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bDescOrder = !bDescOrder;
                        Preferences.setContactSettingOrder(bDescOrder);
                        mHeaderBinding.btSortList.setText(bDescOrder ?
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                        if(mFriendListener != null)
                            mFriendListener.refreshOrder(bDescOrder);
                    }
                });

                String countMessage = String.format("%s %d%s", MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_text_contact),
                        items == null ? 0 : items.size(),
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.human_count));
                mHeaderBinding.tvCount.setText(countMessage);
            }else{
                //mBinding.btnSendSmsInvite.setBackgroundResource(mViewType == ViewType.NORMAL ? R.drawable.button_bg_gray : MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c: R.drawable.button_bg_16c066);
                mBinding.btnSendSmsInvite.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c: R.drawable.button_bg_16c066);
                mBinding.btnSendSmsInvite.setTextColor(MineTalk.isBlackTheme ? MineTalkApp.getAppContext().getResources().getColor(R.color.main_text_color) :
                        MineTalkApp.getAppContext().getResources().getColor(R.color.white_color) );
                mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
                mBinding.tvUserName.setText(item.getUser_name());
                mBinding.tvUserPhone.setText(item.getUser_phone());
                mBinding.btnSendSmsInvite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(mFragmentEventListener != null){
                            mFragmentEventListener.onSendSMS(mBinding.tvUserName.getText().toString(), mBinding.tvUserPhone.getText().toString());
                        }
//                    String sendUserName = MineTalkApp.getUserInfoModel().getUser_name();
//                    String sendUserHp = MineTalkApp.getUserInfoModel().getUser_hp();
//                    String msg = String.format(mBinding.getRoot().getResources().getString(R.string.invite_sms_msg),
//                            sendUserName,
//                            sendUserHp);
//
//                    sendSMS(mBinding.getRoot().getContext(), mBinding.tvUserPhone.getText().toString(), msg);
                    }
                });

                mBinding.layoutRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mFriendListener != null) {
                            FriendListModel data = new FriendListModel();
                            data.setUser_name(item.getUser_name());
                            data.setUser_hp(item.getUser_phone());
                            mFriendListener.onClickItem(data);
                        }
                    }
                });
            }

        }

        private void sendSMS(Context context, String phone, String msg) {
            try {
                Uri smsUri = Uri.parse("sms:" + phone);
                Intent sendIntent = new Intent(Intent.ACTION_SENDTO, smsUri);
                sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                sendIntent.putExtra("sms_body", msg);
                context.startActivity(sendIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
