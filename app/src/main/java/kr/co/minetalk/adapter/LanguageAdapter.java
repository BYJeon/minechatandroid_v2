package kr.co.minetalk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import kr.co.minetalk.R;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;

public class LanguageAdapter extends BaseAdapter {

    private ArrayList<CountryInfoModel> mDataList = new ArrayList<>();
    private ArrayList<CountryInfoModel> mDataListOriginal = new ArrayList<>();
    private String mSystemLanguage = "";
    private int mCurrentType = 0;
    private String  mFilter = "";

    public LanguageAdapter(int type) {
        mSystemLanguage = Preferences.getLanguage();
        mCurrentType = type;
    }

    public void setData(ArrayList<CountryInfoModel> data) {
        this.mDataList.clear();
        this.mDataListOriginal.clear();
        this.mDataList.addAll(data);
        this.mDataListOriginal.addAll(data);
        notifyDataSetChanged();
    }

    public void clear() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }


    public void setSelect(int position) {
        for (int i = 0 ; i < mDataList.size() ; i++) {
            if(position == i) {
                mDataList.get(i).setSelect(true);
            } else {
                mDataList.get(i).setSelect(false);
            }
        }

        CountryInfoModel item = mDataList.get(position);
        for(int i = 0 ; i < mDataListOriginal.size() ; i++) {
            if(mDataListOriginal.get(i).getCode().equals(item.getCode())) {
                mDataListOriginal.get(i).setSelect(true);
            } else {
                mDataListOriginal.get(i).setSelect(false);
            }
        }

        notifyDataSetChanged();
    }

    public void allUnSelect() {
        for (int i = 0 ; i < mDataList.size() ; i++) {
            mDataList.get(i).setSelect(false);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();

        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_language, parent, false);

            viewHolder.mIvFlag = (ImageView) convertView.findViewById(R.id.iv_flag);
            viewHolder.mTvCountryName = (TextView) convertView.findViewById(R.id.tv_country_name);
            viewHolder.mIvCheck = (ImageView) convertView.findViewById(R.id.iv_checked);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CountryInfoModel item = mDataList.get(position);

        int flagImage = CommonUtils.getResourceImage(context, item.getFlag_image());

        viewHolder.mIvFlag.setImageResource(flagImage);

        String systemLanguage = Locale.getDefault().getLanguage();
        if(systemLanguage != null && systemLanguage.equals("ko")) {
            viewHolder.mTvCountryName.setText(item.getCountryName());
        } else {
            viewHolder.mTvCountryName.setText(item.getCountryNameOther());
        }

        viewHolder.mIvCheck.setSelected(item.isSelect());

        return convertView;
    }

    class ViewHolder {
        ImageView mIvFlag;
        TextView  mTvCountryName;
        ImageView mIvCheck;
    }

    public void filter(String str) {
        this.mFilter = str;
        mDataList.clear();
        if(!mFilter.equals("")) {
            for(int i = 0 ; i < mDataListOriginal.size() ; i++ ) {
                CountryInfoModel item = mDataListOriginal.get(i);
                if(item.getCountryName().toLowerCase().contains(mFilter.toLowerCase()) || item.getCountryNameOther().toLowerCase().contains(mFilter.toLowerCase())) {
                    mDataList.add(item);
                }
            }
        } else {
            mDataList.addAll(mDataListOriginal);
        }
        notifyDataSetChanged();
    }
}
