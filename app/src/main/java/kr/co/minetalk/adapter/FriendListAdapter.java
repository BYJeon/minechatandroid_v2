package kr.co.minetalk.adapter;

import android.content.res.AssetManager;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.adapter.listener.OnFriendBlockAdapterListener;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.LayoutChatListItemBinding;
import kr.co.minetalk.databinding.LayoutFriendListViewBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.FriendListView;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ChatViewHolder> implements View.OnClickListener {

    public static enum ViewType {
        NORMAL,
        RECOMMEND,
        ME
    }

    private OnFriendBlockAdapterListener mListener = null;
    private List<FriendListModel> items;
    private HashMap<String, String> mContactMap = null;
    private OnFriendListViewListener mFriendListener = null;
    private ViewType mViewType = ViewType.NORMAL;
    private String mCountTypeStr = MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_text_friend);

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 0){
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new FriendListAdapter.ChatViewHolder(binding);
        }

        LayoutFriendListViewBinding binding = LayoutFriendListViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);

        return new FriendListAdapter.ChatViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        if(position == 0){
            //FriendListModel item = items.get(position);
            holder.bind(null, mFriendListener);
        }else{
            FriendListModel item = items.get(position-1);
            holder.bind(item, mFriendListener);
        }

    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setCountTypeStr(String typeStr){ this.mCountTypeStr = typeStr; }
    public void setListener(OnFriendListViewListener listener){
        this.mFriendListener = listener;
    }

    public void setItem(ArrayList<FriendListModel> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    public List<FriendListModel> getItem(){return items;}
    public void setContactMap(HashMap<String, String> contactMap){
        this.mContactMap = contactMap;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_button_block_lift) {
            String user_xid = v.getTag().toString();
            if (mListener != null) {
                mListener.onBlockLift(user_xid);
            }
        }
    }

    public void setAdapterListener(OnFriendBlockAdapterListener listener) {
        this.mListener = listener;
    }

    /**
     * View holder to display each RecylerView item
     */

    protected class ChatViewHolder extends RecyclerView.ViewHolder {
        LayoutFriendListViewBinding mBinding = null;
        LayoutListHeaderViewBinding mHeaderBinding;

        OnFriendListViewListener mListener;

        protected boolean bDescOrder = false;

        public ChatViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            bDescOrder = Preferences.getFriendSettingOrder();
            this.mHeaderBinding = binding;
        }

        public ChatViewHolder(LayoutFriendListViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(FriendListModel item, OnFriendListViewListener listener){
            this.mListener = listener;

            if(mBinding == null){ //Header 영역
                bDescOrder = Preferences.getFriendSettingOrder();
                mHeaderBinding.btSortList.setText(bDescOrder ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));

                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bDescOrder = !bDescOrder;
                        Preferences.setFriendSettingOrder(bDescOrder);
                        mHeaderBinding.btSortList.setText(bDescOrder ?
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                        if(mListener != null)
                            mListener.refreshOrder(bDescOrder);
                    }
                });

                //friend_search_total_count

                String countMessage = String.format("%s %d%s", mCountTypeStr,
                                       items == null ? 0 : items.size(), MineTalkApp.getCurrentActivity().getResources().getString(R.string.human_count));
                mHeaderBinding.tvCount.setText(countMessage);
            }else{
                if(item != null) {
                    mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

//                //연락처에서 이름을 가져 온다.
//                String contactName = MineTalkApp.getContactUserName(item.getUser_hp());
//                if(contactName != null && !contactName.equals(""))
//                    mBinding.tvUserName.setText(contactName);
//                else
//                    mBinding.tvUserName.setText(item.getUser_name());
//
//                //닉네임 저장이 있다면 닉네임으로 출력.
//                HashMap<String, String> nickNameMap = FriendManager.getInstance().selectFriendNickNameMap();
//                if(nickNameMap.containsKey(item.getUser_hp())){
//                    mBinding.tvUserName.setText(nickNameMap.get(item.getUser_hp()));
//                }

                    mBinding.tvUserName.setText(MineTalkApp.getUserNameByPhoneNum(item.getUser_name(), item.getUser_hp()));

                    String profileImgUrl = item.getUser_profile_image();

                    if(profileImgUrl == null || profileImgUrl.equals("")) {
                        Glide.with(mBinding.getRoot()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                    } else {
                        Glide.with(mBinding.getRoot()).load(item.getUser_profile_image()).into(mBinding.ivUserImage);
                    }

                    if(item.getUser_state_message().equals("")) {
                        mBinding.tvStateMessage.setVisibility(View.GONE);
                        mBinding.tvStateMessage.setText(item.getUser_state_message());
                    } else {
                        mBinding.tvStateMessage.setVisibility(View.VISIBLE);
                        mBinding.tvStateMessage.setText(item.getUser_state_message());
                    }

                    String nationCode = item.getUser_nation_code();

                    if(nationCode != null && !nationCode.equals("")) {
                        try {
                            AssetManager assetManager = MineTalkApp.getAppContext().getResources().getAssets();
                            InputStream inputStream = assetManager.open("flag/" + nationCode + ".jpg");
                            mBinding.ivNationFlag.setImageDrawable(Drawable.createFromStream(inputStream, null));
                            inputStream.close();
                            mBinding.ivNationFlag.setVisibility(View.VISIBLE);
                        } catch (IOException e) {
                            e.printStackTrace();
                            mBinding.ivNationFlag.setVisibility(View.GONE);
                        }
                    } else {
                        mBinding.ivNationFlag.setVisibility(View.GONE);
                    }

                }

                mBinding.layoutRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mListener != null)
                            mListener.onClickItem(item);
                    }
                });
            }
        }
    }

}
