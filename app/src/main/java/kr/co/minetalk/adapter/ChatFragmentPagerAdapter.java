package kr.co.minetalk.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;

import kr.co.minetalk.fragment.ChatFragment;
import kr.co.minetalk.fragment.ChatGroupFragment;
import kr.co.minetalk.fragment.ChatMineFragment;
import kr.co.minetalk.fragment.ChatSecurityFragment;
import kr.co.minetalk.fragment.ContactFragment;
import kr.co.minetalk.fragment.FavoriteFragment;
import kr.co.minetalk.fragment.FriendFragment;
import kr.co.minetalk.fragment.ProfileFragment;
import kr.co.minetalk.fragment.ShopFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.listener.OnProfileListener;

public class ChatFragmentPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private Fragment mCurrentFragment;

    public ChatFragmentPagerAdapter(FragmentManager fragmentManager, OnFragmentEventListener listener, HashMap<String, String> contactMap) {
        super(fragmentManager);


        mFragments.clear();
        ChatFragment chat = ChatFragment.newInstance();
        chat.setOnFragmentListener(listener);
        chat.setOnContactMap(contactMap);

        ChatGroupFragment chatGroup = ChatGroupFragment.newInstance();
        chatGroup.setOnContactMap(contactMap);

        ChatMineFragment chatMine = ChatMineFragment.newInstance();
        chatMine.setOnContactMap(contactMap);

        ChatSecurityFragment chatSecurity = ChatSecurityFragment.newInstance();
        chatSecurity.setOnContactMap(contactMap);

        mFragments.add(chat);
        mFragments.add(chatGroup);
        mFragments.add(chatMine);
        mFragments.add(chatSecurity);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = (Fragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }
}
