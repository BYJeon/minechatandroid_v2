package kr.co.minetalk.adapter;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.AdsMessageDetailActivity;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.activity.CreateMarketingChatActivity;
import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.databinding.LayoutAdListItemBinding;
import kr.co.minetalk.databinding.LayoutChatListItemBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class ChatAdListAdapter extends RecyclerView.Adapter<ChatAdListAdapter.ChatAdViewHolder> implements View.OnClickListener {
    public static enum ChatHolderType {
        NORMAL,
        FRIEND_SEARCH
    }

    private OnFriendListViewOrderListener mListener = null;
    private List<AdListModel> items;
    private SimpleDateFormat mDtFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat mTimeFormat = new SimpleDateFormat("HH:mm");

    private HashMap<String, String> mContactMap = null;
    private ChatHolderType mHolderType = ChatHolderType.NORMAL;

    @NonNull
    @Override
    public ChatAdViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 0){
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ChatAdListAdapter.ChatAdViewHolder(binding);
        }

        LayoutAdListItemBinding binding = LayoutAdListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ChatAdListAdapter.ChatAdViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdViewHolder holder, int position) {
        if(position == 0){
            //FriendListModel item = items.get(position);
            holder.bind(null);
        }else{
            AdListModel item = items.get(position-1);
            holder.bind(item);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size()+1;
    }

    @BindingAdapter("bind:adList")
    public static void bindItem(RecyclerView recyclerView, List<AdListModel> items) {
        ChatAdListAdapter adapter = (ChatAdListAdapter) recyclerView.getAdapter();

        if (adapter != null) {
            adapter.setData(items);
        }
    }

    public List<AdListModel> getData(){
        if(items != null){
            return items;
        }
        return null;
    }
    public void setData(List<AdListModel> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    public void setData(List<AdListModel> items, ChatHolderType type) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }

        this.mHolderType = type;
    }

    public void setContactMap(HashMap<String, String> contactMap){
        this.mContactMap = contactMap;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_button_block_lift) {
            String user_xid = v.getTag().toString();
//            if (mListener != null) {
//                mListener.onBlockLift(user_xid);
//            }
        }
    }

    public void setAdapterListener(OnFriendListViewOrderListener listener) {
        this.mListener = listener;
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ChatAdViewHolder extends RecyclerView.ViewHolder {
        LayoutAdListItemBinding mBinding;

        LayoutListHeaderViewBinding mHeaderBinding;

        public ChatAdViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            this.mHeaderBinding = binding;
        }

        public ChatAdViewHolder(LayoutAdListItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(AdListModel mData) {
            if(mBinding == null) { //Header 영역
                mHeaderBinding.btSortList.setText(Preferences.getMineChatSettingOrder() ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));

                String countMessage = String.format("%s %d%s", MineTalkApp.getCurrentActivity().getResources().getString(R.string.chat_mine_title),
                        items == null ? 0 : items.size(),
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.chat_count));
                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Preferences.setMineChatSettingOrder(!Preferences.getMineChatSettingOrder() );
                        mHeaderBinding.btSortList.setText(Preferences.getMineChatSettingOrder() ?
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                        if(mListener != null)
                            mListener.refreshOrder(Preferences.getMineChatSettingOrder());
                    }
                });


                mHeaderBinding.tvCount.setText(countMessage);

            }else {
                if(mData != null) {
                    mBinding.layoutAdItemRoot.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AdsMessageDetailActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.AD_LIST_TYPE_R, mData.getAd_idx(), mData.getUser_name());
                        }
                    });

                    // 받은 메시지
                    mBinding.tvInfo2.setText("");
                    mBinding.tvInfo2.setVisibility(View.GONE);

                    if(mData.getReceive_yn().equals("Y")) {
                        mBinding.tvInfo1.setText(mData.getAd_token_amount() + MineTalkApp.getCurrentActivity().getString(R.string.get_token));
                    } else {
                        mBinding.tvInfo1.setTextColor(Color.parseColor("#002169"));
                        mBinding.tvInfo1.setText(MineTalkApp.getCurrentActivity().getResources().getString(R.string.ad_unread));
                    }

                    try {
                        String expiredDateStart = CommonUtils.convertDateFormat(mData.getAd_send_date(), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");
                        String expiredDateEnd   = CommonUtils.convertDateFormat(mData.getAd_end_date(), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");

                        String expiredDate = " (" + MineTalkApp.getCurrentActivity().getResources().getString(R.string.ad_experiod) + " : " + expiredDateEnd + ")";
                        String tvInfoStr = mBinding.tvInfo1.getText().toString();

                        boolean isExpired = dateCompaired(getCurrentDate(), expiredDateEnd);
                        if(isExpired) {
                            mBinding.tvInfo1.setTextColor(Color.parseColor("#002169"));
                            mBinding.tvInfo1.setText(tvInfoStr);
                            mBinding.tvInfo2.setTextColor(Color.parseColor("#ff6e6e"));
                            mBinding.tvInfo2.setText(MineTalkApp.getCurrentActivity().getString(R.string.date_expired) );
                            mBinding.tvInfo2.setVisibility(View.VISIBLE);

                            if(mData.getReceive_yn().equals("N")) {
                                mBinding.tvInfo1.setVisibility(View.GONE);
                            } else {
                                mBinding.tvInfo1.setVisibility(View.VISIBLE);
                            }
                        } else {
                            mBinding.tvInfo1.setTextColor(Color.parseColor("#002169"));
                            mBinding.tvInfo1.setText(tvInfoStr + expiredDate);

                            mBinding.tvInfo2.setTextColor(Color.parseColor("#9b9b9b"));
                            mBinding.tvInfo2.setText("");
                            mBinding.tvInfo2.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mBinding.tvMsgDate.setText("");
                    mBinding.tvMsgDate.setVisibility(View.GONE);

                    if(mData.getUser_profile_image() == null || mData.getUser_profile_image().equals("")) {
                        Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                    } else {
                        Glide.with(MineTalkApp.getCurrentActivity()).load(mData.getUser_profile_image()).into(mBinding.ivUserImage);
                    }

                    mBinding.tvUserName.setText(mData.getUser_name());
                    mBinding.tvLastMsg.setText(mData.getAd_title());

                    updateTheme();
                }
            }
        }

        /**
         * 현재 날짜 가져오기 (yyyy-MM-dd HH:mm 형식으로 리턴)
         * @return
         */
        public String getCurrentDate() {
            long now = System.currentTimeMillis();
            Date date = new Date(now);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            return sdf.format(date).toString();
        }

        /**
         * 날짜 비교 함수
         * 날짜 a가 날짜 b 보다 이후 일 경우 true, 아니면 false
         * @param a
         * @param b
         */
        public boolean dateCompaired(String a, String b) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                Date dateA = sdf.parse(a);
                Date dataB = sdf.parse(b);

                return dateA.after(dataB);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

        private void updateTheme(){
            mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
            mBinding.tvInfo1.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
            //mBinding.tvUnreadCount.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.bk_point_color : R.color.app_point_color));
        }
    }

    private int getUnReadCount(int lastSeq, List<ThreadMember> members, String userXid) {
        int readSeq = 0;
        for(ThreadMember tmember : members) {
            if(tmember.getXid().equals(userXid)) {
                readSeq = tmember.getReadSeq();
            }
        }

        return lastSeq - readSeq;
    }

}
