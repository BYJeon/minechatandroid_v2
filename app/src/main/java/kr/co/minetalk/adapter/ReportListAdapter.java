package kr.co.minetalk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.PointHistoryModel;
import kr.co.minetalk.utils.CommonUtils;

public class ReportListAdapter extends BaseAdapter {


    private ArrayList<PointHistoryModel> mDataList = new ArrayList<>();

    public void setData(ArrayList<PointHistoryModel> arrayList) {
        this.mDataList.clear();
//        if(arrayList.size() > 0) {
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//            addData(arrayList.get(0));
//        }

        this.mDataList.addAll(arrayList);

        notifyDataSetChanged();
    }

    public void addData(PointHistoryModel data) {
        //this.mDataList.clear();
        this.mDataList.add(data);

        notifyDataSetChanged();
    }

    public void clear() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();


        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.report_list_item, parent, false);

            viewHolder.mRootLayout = convertView.findViewById(R.id.layout_root);
            viewHolder.mTvMsg1 = (TextView) convertView.findViewById(R.id.tv_msg_1);
            viewHolder.mTvMsg2 = (TextView) convertView.findViewById(R.id.tv_msg_2);
            viewHolder.mTvRegDate = (TextView) convertView.findViewById(R.id.tv_date);
            viewHolder.mTvRegDateTime = (TextView) convertView.findViewById(R.id.tv_date_time);
            viewHolder.mTvPoint = (TextView) convertView.findViewById(R.id.tv_point);

            viewHolder.mTvDateTitle = convertView.findViewById(R.id.tv_date_title);
            viewHolder.mTvHistoryTitle = convertView.findViewById(R.id.tv_history_title);
            viewHolder.mTvAmountTitle = convertView.findViewById(R.id.tv_amount_title);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PointHistoryModel item = mDataList.get(position);

        if(item.getAction_gubun().equals("U")) {
            // 차감
            viewHolder.mTvPoint.setVisibility(View.VISIBLE);
            //viewHolder.mTvPoint.setBackgroundResource(R.drawable.point_round_box_e36f6f);
            String point = CommonUtils.comma_won(item.getPoint_amount());
            viewHolder.mTvPoint.setText("-" + point);
        } else if(item.getAction_gubun().equals("A")) {
            // 증감
            viewHolder.mTvPoint.setVisibility(View.VISIBLE);
            //viewHolder.mTvPoint.setBackgroundResource(R.drawable.point_round_box_007cba);
            String point = CommonUtils.comma_won(item.getPoint_amount());
            viewHolder.mTvPoint.setText(point);
        } else {
            viewHolder.mTvPoint.setVisibility(View.INVISIBLE);
            viewHolder.mTvPoint.setText("");
        }

        //viewHolder.mTvMsg1.setText(item.getAction_type() + " " + (item.getUser_name()));
        //viewHolder.mTvMsg2.setText(item.getUser_name());

        viewHolder.mTvMsg1.setText(item.getAction_type());
        viewHolder.mTvMsg2.setText(item.getUser_name());

        String dateInfo = item.getReg_date();
        int idx = dateInfo.indexOf(" ");
        viewHolder.mTvRegDate.setText(item.getReg_date().substring(0, idx));
        viewHolder.mTvRegDateTime.setText(item.getReg_date().substring(idx+1));

        //첫 Row는 타이틀
//        if(position == 0){
//            viewHolder.mTvDateTitle.setVisibility(View.VISIBLE);
//            viewHolder.mTvHistoryTitle.setVisibility(View.VISIBLE);
//            viewHolder.mTvAmountTitle.setVisibility(View.VISIBLE);
//
//            viewHolder.mTvRegDate.setVisibility(View.INVISIBLE);
//            viewHolder.mTvRegDateTime.setVisibility(View.INVISIBLE);
//
//            viewHolder.mTvMsg1.setVisibility(View.INVISIBLE);
//            viewHolder.mTvMsg2.setVisibility(View.INVISIBLE);
//
//            viewHolder.mTvPoint.setVisibility(View.INVISIBLE);
//        }else{
//            viewHolder.mTvDateTitle.setVisibility(View.INVISIBLE);
//            viewHolder.mTvHistoryTitle.setVisibility(View.INVISIBLE);
//            viewHolder.mTvAmountTitle.setVisibility(View.INVISIBLE);
//
//            viewHolder.mTvRegDate.setVisibility(View.VISIBLE);
//            viewHolder.mTvRegDateTime.setVisibility(View.VISIBLE);
//
//            viewHolder.mTvMsg1.setVisibility(View.VISIBLE);
//            viewHolder.mTvMsg2.setVisibility(View.VISIBLE);
//
//            viewHolder.mTvPoint.setVisibility(View.VISIBLE);
//        }

        if(position%2 == 0){
            viewHolder.mRootLayout.setBackgroundColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.bk_theme_bg :R.color.main_theme_bg));
        }else{
            viewHolder.mRootLayout.setBackgroundColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.bk_theme_status : R.color.white_color));
        }

        viewHolder.mTvDateTitle.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
        viewHolder.mTvHistoryTitle.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
        viewHolder.mTvAmountTitle.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

        viewHolder.mTvRegDate.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
        viewHolder.mTvRegDateTime.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

        viewHolder.mTvMsg1.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
        viewHolder.mTvMsg2.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

        viewHolder.mTvPoint.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

        return convertView;
    }

    class ViewHolder {
        TextView mTvMsg1;
        TextView mTvMsg2;
        TextView mTvRegDate;
        TextView mTvRegDateTime;
        TextView mTvPoint;

        TextView mTvDateTitle;
        TextView mTvHistoryTitle;
        TextView mTvAmountTitle;

        LinearLayout mRootLayout;
    }
}
