package kr.co.minetalk.adapter.listener;

public interface OnFriendBlockAdapterListener {

    public void onBlockLift(String user_xid);
    public void onClickUser(String user_xid);
}
