package kr.co.minetalk.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.GridContactItemBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class ContactGridViewAdapter extends RecyclerView.Adapter<ContactGridViewAdapter.ContactViewHolder> {
    private ArrayList<ContactModel> items;
    private OnFriendListViewListener mFriendListener = null;
    private OnFragmentEventListener  mFragmentListener = null;

    private ContactListViewAdapter.ViewType mViewType = ContactListViewAdapter.ViewType.NORMAL;

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0){
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ContactViewHolder(binding);
        }

        GridContactItemBinding binding = GridContactItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ContactViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ContactGridViewAdapter.ContactViewHolder holder, int position) {
        if(position == 0){
            //ContactModel item = items.get(position);
            holder.bind(null);
        }else {
            ContactModel item = items.get(position-1);
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setListener(OnFriendListViewListener listener){
        this.mFriendListener = listener;
    }

    public void setFragmentListener(OnFragmentEventListener listener){
        this.mFragmentListener = listener;
    }

    public void addItem(ArrayList<ContactModel> items) {
        if (this.items != null && items != null)
            this.items.addAll(items);
        else
            this.items = items;

        notifyDataSetChanged();
    }

    public void addItem(ContactModel items) {
        if (this.items != null && items != null)
            this.items.add(items);
        //notifyDataSetChanged();
    }

    public void setItem(ArrayList<ContactModel> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    public void setViewType(ContactListViewAdapter.ViewType type){
        this.mViewType = type;
    }

    public ArrayList<ContactModel> getItem(){return items;}
    /**
     * View holder to display each RecylerView item
     */
    protected class ContactViewHolder extends RecyclerView.ViewHolder {
        GridContactItemBinding mBinding;
        OnFriendListViewListener mListener;

        LayoutListHeaderViewBinding mHeaderBinding;
        protected boolean bDescOrder = false;

        public ContactViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            bDescOrder = Preferences.getFriendSettingOrder();
            this.mHeaderBinding = binding;
        }

        public ContactViewHolder(GridContactItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(ContactModel item){

            if(mBinding == null){ //Header 영역
                bDescOrder = Preferences.getContactSettingOrder();
                mHeaderBinding.btSortList.setText(bDescOrder ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));

                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bDescOrder = !bDescOrder;
                        Preferences.setContactSettingOrder(bDescOrder);
                        mHeaderBinding.btSortList.setText(bDescOrder ?
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                        if(mFriendListener != null)
                            mFriendListener.refreshOrder(bDescOrder);
                    }
                });

                String countMessage = String.format("%s %d%s", MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_text_contact),
                        items == null ? 0 : items.size(),
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.human_count));
                mHeaderBinding.tvCount.setText(countMessage);
            }else {
                mBinding.btnSendSmsInvite.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);
                mBinding.btnSendSmsInvite.setTextColor(MineTalk.isBlackTheme ? MineTalkApp.getAppContext().getResources().getColor(R.color.main_text_color) :
                        MineTalkApp.getAppContext().getResources().getColor(R.color.white_color));
                mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
                mBinding.tvUserName.setText(item.getUser_name());
                mBinding.tvUserPhone.setText(item.getUser_phone());
                mBinding.btnSendSmsInvite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mFragmentListener != null) {
                            mFragmentListener.onSendSMS(mBinding.tvUserName.getText().toString(), mBinding.tvUserPhone.getText().toString());
                        }
                    }
                });

                mBinding.layoutRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mListener != null) {
                            FriendListModel data = new FriendListModel();
                            data.setUser_name(item.getUser_name());
                            data.setUser_hp(item.getUser_phone());
                            mListener.onClickItem(data);
                        }
                    }
                });
            }
        }

        private void sendSMS(Context context, String phone, String msg) {
            try {
                Uri smsUri = Uri.parse("sms:" + phone);
                Intent sendIntent = new Intent(Intent.ACTION_SENDTO, smsUri);
                sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                sendIntent.putExtra("sms_body", msg);
                context.startActivity(sendIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
