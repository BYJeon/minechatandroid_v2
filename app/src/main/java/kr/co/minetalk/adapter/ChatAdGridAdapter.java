package kr.co.minetalk.adapter;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.AdsMessageDetailActivity;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.databinding.LayoutChatGridItemBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class ChatAdGridAdapter extends RecyclerView.Adapter<ChatAdGridAdapter.ChatViewHolder> implements View.OnClickListener {
    private OnFriendListViewOrderListener mListener = null;
    private List<AdListModel> items;
    private HashMap<String, String> mContactMap = null;

    private SimpleDateFormat mDtFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat mTimeFormat = new SimpleDateFormat("HH:mm");

    private List<ImageView> mProfileImgList = new ArrayList<>();

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ChatAdGridAdapter.ChatViewHolder(binding);
        }

        LayoutChatGridItemBinding binding = LayoutChatGridItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ChatAdGridAdapter.ChatViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        if (position == 0) {
            //FriendListModel item = items.get(position);
            holder.bind(null);
        } else {
            AdListModel item = items.get(position - 1);
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @BindingAdapter("bind:adList")
    public static void bindItem(RecyclerView recyclerView, List<AdListModel> items) {
        ChatAdGridAdapter adapter = (ChatAdGridAdapter) recyclerView.getAdapter();

        if (adapter != null) {
            adapter.setData(items);
        }
    }

    public void setContactMap(HashMap<String, String> contactMap) {
        this.mContactMap = contactMap;
    }

    public void setData(List<AdListModel> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
//        if (v.getId() == R.id.tv_button_block_lift) {
//            String user_xid = v.getTag().toString();
//            if (mListener != null) {
//                mListener.onBlockLift(user_xid);
//            }
//        }
    }

    public void setAdapterListener(OnFriendListViewOrderListener listener) {
        this.mListener = listener;
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ChatViewHolder extends RecyclerView.ViewHolder {
        LayoutChatGridItemBinding mBinding;
        LayoutListHeaderViewBinding mHeaderBinding;

        public ChatViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            this.mHeaderBinding = binding;
        }

        public ChatViewHolder(LayoutChatGridItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(AdListModel mData) {
            if (mBinding == null) { //Header 영역
                mHeaderBinding.btSortList.setText(Preferences.getMineChatSettingOrder() ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));

                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Preferences.setMineChatSettingOrder(!Preferences.getMineChatSettingOrder());
                        mHeaderBinding.btSortList.setText(Preferences.getMineChatSettingOrder() ?
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                        if (mListener != null)
                            mListener.refreshOrder(Preferences.getMineChatSettingOrder());
                    }
                });

                String countMessage = String.format("%s %d%s", MineTalkApp.getCurrentActivity().getResources().getString(R.string.chat_mine_title),
                        items == null ? 0 : items.size(),
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.chat_count));
                mHeaderBinding.tvCount.setText(countMessage);

            } else {
                mBinding.layoutAdItemRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AdsMessageDetailActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.AD_LIST_TYPE_R, mData.getAd_idx(), mData.getUser_name());
                    }
                });

                // 받은 메시지
//                mBinding.tvInfo2.setText("");
//                mBinding.tvInfo2.setVisibility(View.GONE);
//
//                if(mData.getReceive_yn().equals("Y")) {
//                    mBinding.tvInfo1.setText(mData.getAd_token_amount() + MineTalkApp.getCurrentActivity().getString(R.string.get_token));
//                } else {
//                    mBinding.tvInfo1.setTextColor(Color.parseColor("#002169"));
//                    mBinding.tvInfo1.setText(MineTalkApp.getCurrentActivity().getResources().getString(R.string.ad_unread));
//                }

                try {
//                    String expiredDateStart = CommonUtils.convertDateFormat(mData.getAd_send_date(), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");
//                    String expiredDateEnd   = CommonUtils.convertDateFormat(mData.getAd_end_date(), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");
//
//                    String expiredDate = " (" + MineTalkApp.getCurrentActivity().getResources().getString(R.string.ad_experiod) + " : " + expiredDateEnd + ")";
//                    String tvInfoStr = mBinding.tvInfo1.getText().toString();
//
//                    boolean isExpired = dateCompaired(getCurrentDate(), expiredDateEnd);
//                    if(isExpired) {
//                        mBinding.tvInfo1.setTextColor(Color.parseColor("#002169"));
//                        mBinding.tvInfo1.setText(tvInfoStr);
//                        mBinding.tvInfo2.setTextColor(Color.parseColor("#ff6e6e"));
//                        mBinding.tvInfo2.setText(MineTalkApp.getCurrentActivity().getString(R.string.date_expired) );
//                        mBinding.tvInfo2.setVisibility(View.VISIBLE);
//
//                        if(mData.getReceive_yn().equals("N")) {
//                            mBinding.tvInfo1.setVisibility(View.GONE);
//                        } else {
//                            mBinding.tvInfo1.setVisibility(View.VISIBLE);
//                        }
//                    } else {
//                        mBinding.tvInfo1.setTextColor(Color.parseColor("#002169"));
//                        mBinding.tvInfo1.setText(tvInfoStr + expiredDate);
//
//                        mBinding.tvInfo2.setTextColor(Color.parseColor("#9b9b9b"));
//                        mBinding.tvInfo2.setText("");
//                        mBinding.tvInfo2.setVisibility(View.GONE);
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mBinding.tvMsgDate.setText("");
                mBinding.tvMsgDate.setVisibility(View.GONE);

                if(mData.getUser_profile_image() == null || mData.getUser_profile_image().equals("")) {
                    Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                } else {
                    Glide.with(MineTalkApp.getCurrentActivity()).load(mData.getUser_profile_image()).into(mBinding.ivUserImage);
                }

                mBinding.tvUserName.setText(mData.getUser_name());
                mBinding.tvMsgTime.setVisibility(View.GONE);

                updateTheme();
            }
        }

        private void updateTheme(){
            mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
            //mBinding.tvUnreadCount.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.bk_point_color : R.color.app_point_color));
        }
    }

    private int getUnReadCount(int lastSeq, List<ThreadMember> members, String userXid) {
        int readSeq = 0;
        for (ThreadMember tmember : members) {
            if (tmember.getXid().equals(userXid)) {
                readSeq = tmember.getReadSeq();
            }
        }

        return lastSeq - readSeq;
    }
}
