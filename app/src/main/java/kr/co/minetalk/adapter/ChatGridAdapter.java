package kr.co.minetalk.adapter;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.adapter.listener.OnFriendBlockAdapterListener;
import kr.co.minetalk.databinding.LayoutChatGridItemBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class ChatGridAdapter extends RecyclerView.Adapter<ChatGridAdapter.ChatViewHolder> implements View.OnClickListener {
    private OnFriendListViewOrderListener mListener = null;
    private List<ThreadData> items;
    private HashMap<String, String> mContactMap = null;

    private SimpleDateFormat mDtFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat mTimeFormat = new SimpleDateFormat("HH:mm");

    private List<ImageView> mProfileImgList = new ArrayList<>();
    private boolean isSecurityMode = false;

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 0){
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ChatGridAdapter.ChatViewHolder(binding);
        }

        LayoutChatGridItemBinding binding = LayoutChatGridItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ChatGridAdapter.ChatViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        if(position == 0){
            //FriendListModel item = items.get(position);
            holder.bind(null);
        }else{
            ThreadData item = items.get(position-1);
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @BindingAdapter("bind:threadGrid")
    public static void bindItem(RecyclerView recyclerView, List<ThreadData> items) {
        ChatGridAdapter adapter = (ChatGridAdapter) recyclerView.getAdapter();

        if (adapter != null) {
            adapter.setData(items);
        }
    }

    public void setContactMap(HashMap<String, String> contactMap){
        this.mContactMap = contactMap;
    }

    public void setData(List<ThreadData> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    public void setData(List<ThreadData> items, boolean isSecurityMode) {
        if (items != null) {
            this.isSecurityMode = isSecurityMode;
            this.items = items;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
//        if (v.getId() == R.id.tv_button_block_lift) {
//            String user_xid = v.getTag().toString();
//            if (mListener != null) {
//                mListener.onBlockLift(user_xid);
//            }
//        }
    }

    public void setAdapterListener(OnFriendListViewOrderListener listener) {
        this.mListener = listener;
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ChatViewHolder extends RecyclerView.ViewHolder {
        LayoutChatGridItemBinding mBinding;
        LayoutListHeaderViewBinding mHeaderBinding;

        public ChatViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            this.mHeaderBinding = binding;
        }

        public ChatViewHolder(LayoutChatGridItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(ThreadData item) {
            if(mBinding == null) { //Header 영역
                mHeaderBinding.btSortList.setText(Preferences.getChatSettingOrder() ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));

                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Preferences.setChatSettingOrder(!Preferences.getChatSettingOrder() );
                        mHeaderBinding.btSortList.setText(Preferences.getChatSettingOrder() ?
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                        if(mListener != null)
                            mListener.refreshOrder(Preferences.getChatSettingOrder());
                    }
                });

                String countMessage = String.format("%s %d%s", MineTalkApp.getCurrentActivity().getResources().getString(R.string.chatting_title),
                        items == null ? 0 : items.size(),
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.chat_count));

                if(isSecurityMode){
                    countMessage = String.format(MineTalkApp.getCurrentActivity().getResources().getString(R.string.security_chat_count_msg),
                            items == null ? "0" : Integer.toString(items.size()));
                }

                mHeaderBinding.tvCount.setText(countMessage);

            }else{
                mBinding.layoutAdItemRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String threadName = "";
                        if(item.getIsDirect().toUpperCase().equals("Y")) {
                            if(item.getMembers().size() > 0) {
                                String members = "";
                                for(ThreadMember tmember : item.getMembers()) {
                                    if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                        //String contactName = mContactMap.get(tmember.getXid());
                                        //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());
                                        String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                        if(contactName == null || contactName.equals("")) {
                                            members = tmember.getNickName();
                                        } else {
                                            members = contactName;
                                        }
                                        break;
                                    }
                                }
                                threadName = members;
                            }
                        } else {
                            if(item.getMembers().size() > 0) {
                                String members = "";
                                int count = 0;
                                for(ThreadMember tmember : item.getMembers()) {
                                    //String contactName = mContactMap.get(tmember.getXid());
                                    //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());
                                    String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                    if(contactName == null || contactName.equals("")) {
                                        members = members + tmember.getNickName();
                                    } else {
                                        members = members + contactName;
                                    }

                                    if(!members.equals("") && count != (item.getMembers().size() - 1)) {
                                        members = members + ",";
                                    }
                                    count++;
                                }
                                threadName = members;
                            }
                        }

                        ChatDetailActivity.startActivity(MineTalkApp.getCurrentActivity(), item.getThreadKey(), item.getMembers().size(), threadName, isSecurityMode);

                    }
                });

                int unReadCount = getUnReadCount(item.getLastSeq(), item.getMembers(), MineTalkApp.getUserInfoModel().getUser_xid());
                if(unReadCount > 0) {
                    if(unReadCount > 99) {
                        mBinding.tvUnreadCount.setText("99+");
                    } else {
                        mBinding.tvUnreadCount.setText(String.valueOf(unReadCount));
                    }
                    mBinding.tvUnreadCount.setVisibility(View.VISIBLE);
                } else {
                    mBinding.tvUnreadCount.setVisibility(View.GONE);
                    mBinding.tvUnreadCount.setText("");
                }

                long now = System.currentTimeMillis();
                Date date = new Date(now);
                String curDateTime = mDtFormat.format(date);
                String updateTime = mDtFormat.format(item.getUpdateDate());
                long diffDay = MineTalkApp.calDateBetweenDiff(curDateTime, updateTime, mDtFormat);

                if(diffDay != -1 && diffDay == 0) {
                    mBinding.tvMsgDate.setText(mTimeFormat.format(item.getUpdateDate()));
                }
                else
                    mBinding.tvMsgDate.setText(updateTime);

                //mBinding.tvMsgDate.setText(mYearFormat.format(item.getUpdateDate()));
                mBinding.tvMsgTime.setVisibility(View.GONE);
                //mBinding.tvMsgTime.setText(mTimeFormat.format(item.getUpdateDate()));
                mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

                // 1:1 대화방
                if(item.getIsDirect().toUpperCase().equals("Y")) {
                    String myChatThreadKey = Preferences.getMyChattingThreadKey();

                    if(item.getThreadKey().equals(myChatThreadKey)) {
                        mBinding.tvUnreadCount.setVisibility(View.INVISIBLE);
                        mBinding.tvUserName.setText(MineTalkApp.getUserInfoModel().getUser_name());

                        String profileImgUrl = MineTalkApp.getUserInfoModel().getUser_profile_image();

                        if (profileImgUrl == null || profileImgUrl.equals("")) {
                            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                        } else {
                            Glide.with(MineTalkApp.getCurrentActivity()).load(profileImgUrl).into(mBinding.ivUserImage);
                        }

                    } else {

                        String chatName = Preferences.getChattingName(item.getThreadKey());
                        if(!chatName.equals("")) {
                            mBinding.tvUserName.setText(chatName);

                            for(ThreadMember tmember : item.getMembers()) {
                                if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                    String userProfile = tmember.getProfileImageUrl();
                                    if(userProfile == null || userProfile.equals(userProfile.equals(""))) {
                                        Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                                    } else {
                                        Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mBinding.ivUserImage);
                                    }
                                    Log.e("@@@TAG","url : " + userProfile);
                                    break;
                                }
                            }


                        } else {
                            if(item.getMembers().size() > 0) {
                                String members = "";
                                for(ThreadMember tmember : item.getMembers()) {
                                    if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                        //String contactName = mContactMap.get(tmember.getXid());
                                        //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());

                                        String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());

                                        String userProfile = tmember.getProfileImageUrl();
                                        if(userProfile == null || userProfile.equals(userProfile.equals(""))) {
                                            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                                        } else {
                                            Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mBinding.ivUserImage);
                                        }
                                        Log.e("@@@TAG","url : " + userProfile);

                                        if(contactName == null || contactName.equals("")) {
                                            members = tmember.getNickName();
                                        } else {
                                            members = contactName;
                                        }
                                        break;
                                    }

                                }
                                mBinding.tvUserName.setText(members);
                            }
                        }
                    }
                } else {
                    int memberCnt = item.getJoinMemberCount();
                    if(memberCnt == 2 || memberCnt == 3){ //3명 채팅
                        mBinding.layoutOneProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutTwoProfile.setVisibility(View.VISIBLE);
                        mBinding.layoutThreeProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutFourProfile.setVisibility(View.INVISIBLE);

                        mProfileImgList.add(mBinding.ivUserImageTwo1);
                        mProfileImgList.add(mBinding.ivUserImageTwo2);

                    }else if(memberCnt == 4) { //4명 채팅
                        mBinding.layoutOneProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutTwoProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutThreeProfile.setVisibility(View.VISIBLE);
                        mBinding.layoutFourProfile.setVisibility(View.INVISIBLE);

                        mProfileImgList.add(mBinding.ivUserImageThree1);
                        mProfileImgList.add(mBinding.ivUserImageThree2);
                        mProfileImgList.add(mBinding.ivUserImageThree3);
                    }else if(memberCnt > 4) { //5명 이상 채팅
                        mBinding.layoutOneProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutTwoProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutThreeProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutFourProfile.setVisibility(View.VISIBLE);

                        mProfileImgList.add(mBinding.ivUserImageFour1);
                        mProfileImgList.add(mBinding.ivUserImageFour2);
                        mProfileImgList.add(mBinding.ivUserImageFour3);
                        mProfileImgList.add(mBinding.ivUserImageFour4);
                    }

                    // 단체방
                    String myChatThreadKey = Preferences.getMyChattingThreadKey();
                    if(item.getThreadKey().equals(myChatThreadKey)) {
                        mBinding.tvUserName.setText(MineTalkApp.getCurrentActivity().getResources().getString(R.string.popup_profile_button_chat_me));
                    } else {

                        String chatName = Preferences.getChattingName(item.getThreadKey());

                        if(!chatName.equals("")) {
                            mBinding.tvUserName.setText(chatName);

                            int nCnt = 0;
                            for(ThreadMember tmember : item.getMembers()) {
                                if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                    String userProfile = tmember.getProfileImageUrl();

                                    if(nCnt < 4) {
                                        if (userProfile == null || userProfile.equals(userProfile.equals(""))) {
                                            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mProfileImgList.get(nCnt));
                                        } else {
                                            Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mProfileImgList.get(nCnt));
                                        }
                                        Log.e("@@@TAG", "url : " + userProfile);
                                    }
                                    //break;
                                    nCnt++;
                                }
                            }

                        } else {
                            if(item.getMembers().size() > 0) {
                                String members = "";
                                int count = 0;
                                for(ThreadMember tmember : item.getMembers()) {

                                    String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                    //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());
                                    //String contactName = mContactMap.get(tmember.getXid());
                                    if(contactName == null || contactName.equals("")) {
                                        members = members + tmember.getNickName();
                                    } else {
                                        members = members + contactName;
                                    }

                                    if(!members.equals("") && count != (item.getMembers().size() - 1)) {
                                        members = members + ",";
                                    }
                                    count++;
                                }

                                mBinding.tvUserName.setText(members);

                                int nCnt = 0;
                                for(ThreadMember tmember : item.getMembers()) {
                                    if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                        String userProfile = tmember.getProfileImageUrl();
                                        if(nCnt < 4) {
                                            if (userProfile == null || userProfile.equals(userProfile.equals(""))) {
                                                Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mProfileImgList.get(nCnt));
                                            } else {
                                                Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mProfileImgList.get(nCnt));
                                            }
                                            Log.e("@@@TAG", "url : " + userProfile);
                                        }
                                        //break;
                                    }
                                }

                            }
                        }
                    }

                }


//            String chatName = Preferences.getChattingName(item.getThreadKey());
//
//
//            if(!chatName.equals("")) {
//                mBinding.tvUserName.setText(chatName);
//
//                for(ThreadMember tmember : item.getMembers()) {
//                    if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
//                        String userProfile = tmember.getProfileImageUrl();
//                        if(userProfile == null || userProfile.equals(userProfile.equals(""))) {
//                            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
//                        } else {
//                            Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mBinding.ivUserImage);
//                        }
//                        Log.e("@@@TAG","url : " + userProfile);
//                        //break;
//                    }
//                }
//            } else {
//                if(item.getMembers().size() > 0) {
//                    String members = "";
//                    for(ThreadMember tmember : item.getMembers()) {
//                        if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
//                            String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
//                            //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());
//                            //String contactName = mContactMap == null ? "" : mContactMap.get(tmember.getXid());
//
//                            String userProfile = tmember.getProfileImageUrl();
//                            if(userProfile == null || userProfile.equals(userProfile.equals(""))) {
//                                Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
//                            } else {
//                                Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mBinding.ivUserImage);
//                            }
//                            Log.e("@@@TAG","url : " + userProfile);
//
//                            if(contactName == null || contactName.equals("")) {
//                                members = tmember.getNickName();
//                            } else {
//                                members = contactName;
//                            }
//                            break;
//                        }else{ // 나와의 채팅
//                            members = MineTalkApp.getUserInfoModel().getUser_name();
//                            String profileImgUrl = MineTalkApp.getUserInfoModel().getUser_profile_image();
//
//                            if (profileImgUrl == null || profileImgUrl.equals("")) {
//                                Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
//                            } else {
//                                Glide.with(MineTalkApp.getCurrentActivity()).load(profileImgUrl).into(mBinding.ivUserImage);
//                            }
//                        }
//
//                    }
//                    mBinding.tvUserName.setText(members);
//                }
//            }

            }
            }

    }

    private int getUnReadCount(int lastSeq, List<ThreadMember> members, String userXid) {
        int readSeq = 0;
        for(ThreadMember tmember : members) {
            if(tmember.getXid().equals(userXid)) {
                readSeq = tmember.getReadSeq();
            }
        }

        return lastSeq - readSeq;
    }
}
