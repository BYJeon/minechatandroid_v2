package kr.co.minetalk.adapter;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.adapter.listener.OnFriendBlockAdapterListener;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.GridFriendItemBinding;
import kr.co.minetalk.databinding.LayoutChatGridItemBinding;
import kr.co.minetalk.databinding.LayoutFriendGridItemBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class FriendGridAdapter extends RecyclerView.Adapter<FriendGridAdapter.FriendViewHolder> {
    private ArrayList<FriendListModel> items;
    private OnFriendListViewListener mFriendListener = null;
    private String mCountTypeStr = MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_text_friend);

    @Override
    public FriendGridAdapter.FriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0){
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new FriendGridAdapter.FriendViewHolder(binding);
        }

        GridFriendItemBinding binding = GridFriendItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FriendGridAdapter.FriendViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(FriendGridAdapter.FriendViewHolder holder, int position) {
        if(position == 0){
            //FriendListModel item = items.get(position);
            holder.bind(null, mFriendListener);
        }else{
            FriendListModel item = items.get(position-1);
            holder.bind(item, mFriendListener);
        }

    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size()+1;
    }

    @BindingAdapter("bind:item")
    public static void bindItem(RecyclerView recyclerView, ArrayList<FriendListModel> items) {
        FriendGridAdapter adapter = (FriendGridAdapter) recyclerView.getAdapter();

        if (adapter != null) {
            adapter.setItem(items);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setCountTypeStr(String typeStr){ this.mCountTypeStr = typeStr; }

    public void setListener(OnFriendListViewListener listener){
        this.mFriendListener = listener;
    }

    public void setItem(ArrayList<FriendListModel> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    public ArrayList<FriendListModel> getItem(){return items;}
    /**
     * View holder to display each RecylerView item
     */
    protected class FriendViewHolder extends RecyclerView.ViewHolder {
        GridFriendItemBinding mBinding;
        OnFriendListViewListener mListener;

        LayoutListHeaderViewBinding mHeaderBinding;
        protected boolean bDescOrder = false;

        public FriendViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            bDescOrder = Preferences.getFriendSettingOrder();
            this.mHeaderBinding = binding;
        }

        public FriendViewHolder(GridFriendItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(FriendListModel item, OnFriendListViewListener listener){
            this.mListener = listener;

            if(mBinding == null){ //Header 영역
                bDescOrder = Preferences.getFriendSettingOrder();
                mHeaderBinding.btSortList.setText(bDescOrder ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));

                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bDescOrder = !bDescOrder;
                        Preferences.setFriendSettingOrder(bDescOrder);
                        mHeaderBinding.btSortList.setText(bDescOrder ?
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                        if(mListener != null)
                            mListener.refreshOrder(bDescOrder);
                    }
                });

                String countMessage = String.format("%s %d%s", mCountTypeStr,
                        items == null ? 0 : items.size(), MineTalkApp.getCurrentActivity().getResources().getString(R.string.human_count));
                mHeaderBinding.tvCount.setText(countMessage);

            }else{
                if(item != null) {
                    mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

//                //연락처에서 이름을 가져 온다.
//                String contactName = MineTalkApp.getContactUserName(item.getUser_hp());
//                //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), item.getUser_hp());
//
//                if(contactName != null && !contactName.equals(""))
//                    mBinding.tvUserName.setText(contactName);
//                else
//                    mBinding.tvUserName.setText(item.getUser_name());

                    mBinding.tvUserName.setText(MineTalkApp.getUserNameByPhoneNum(item.getUser_name(), item.getUser_hp()));

                    String profileImgUrl = item.getUser_profile_image();

                    if(profileImgUrl == null || profileImgUrl.equals("")) {
                        Glide.with(mBinding.getRoot()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                    } else {
                        Glide.with(mBinding.getRoot()).load(item.getUser_profile_image()).into(mBinding.ivUserImage);
                    }
                }

                mBinding.layoutRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mListener != null)
                            mListener.onClickItem(item);
                    }
                });
            }
        }
    }

}
