package kr.co.minetalk.adapter.holder;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import kr.co.minetalk.databinding.EmoticonViewHolderBinding;

public class EmoticonViewHolder extends RecyclerView.ViewHolder {

    public EmoticonViewHolderBinding mBinding;

    public EmoticonViewHolder(View itemView) {
        super(itemView);
        mBinding = DataBindingUtil.bind(itemView);
    }
}
