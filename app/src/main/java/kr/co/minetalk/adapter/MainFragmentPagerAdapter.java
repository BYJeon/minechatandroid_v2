package kr.co.minetalk.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;


import java.util.ArrayList;

import kr.co.minetalk.fragment.ChatPagerFragment;
import kr.co.minetalk.fragment.FriendPagerFragment;
import kr.co.minetalk.fragment.MyMineFragment;
import kr.co.minetalk.fragment.ShopFragment;
import kr.co.minetalk.fragment.TranslationFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.listener.OnProfileListener;

public class MainFragmentPagerAdapter extends FragmentPagerAdapter {
    public static final int FRAGMENT_HOME = 0;
    public static final int FRAGMENT_GUIDE = 1;
    public static final int FRAGMENT_PRODUCT = 2;
    public static final int FRAGMENT_SHOP = 3;
    public static final int FRAGMENT_MYPAGE = 4;

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private Fragment mCurrentFragment;

    public MainFragmentPagerAdapter(FragmentManager fragmentManager, OnFragmentEventListener listener, OnProfileListener profileListener) {
        super(fragmentManager);


        mFragments.clear();
        FriendPagerFragment friend = FriendPagerFragment.newInstance();
        friend.setOnFragmentListener(listener, profileListener);
        //FriendOrgFragment friend = FriendOrgFragment.newInstance();
        //friend.setOnFragmentListener(listener);

        ChatPagerFragment chat = ChatPagerFragment.newInstance();
        //ChatFragmentOrg chat = ChatFragmentOrg.newInstance();
        chat.setOnFragmentListener(listener);

        TranslationFragment translation = TranslationFragment.newInstance();
        translation.setOnFragmentListener(listener);

        ShopFragment shop = ShopFragment.newInstance();
        shop.setOnFragmentListener(listener);

        //MoreFragment more = MoreFragment.newInstance();
        //more.setOnFragmentListener(listener);

        MyMineFragment mymine = MyMineFragment.newInstance();
        mymine.setOnFragmentListener(listener);

        mFragments.add(friend);
        mFragments.add(chat);
        mFragments.add(translation);
        mFragments.add(shop);
        mFragments.add(mymine);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = (Fragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }
}
