package kr.co.minetalk.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.minetalk.fragment.ChatSearchFragment;
import kr.co.minetalk.fragment.ContactFragment;
import kr.co.minetalk.fragment.FavoriteFragment;
import kr.co.minetalk.fragment.FriendFragment;
import kr.co.minetalk.fragment.FriendSearchFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;

public class FriendSearchFragmentPagerAdapter extends FragmentPagerAdapter {
    public static final int FRAGMENT_FRIEND  = 0;
    public static final int FRAGMENT_CONTACT = 1;
    public static final int FRAGMENT_CHAT    = 2;

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private Fragment mCurrentFragment;

    public FriendSearchFragmentPagerAdapter(FragmentManager fragmentManager, OnFragmentEventListener listener) {
        super(fragmentManager);


        mFragments.clear();

        FriendSearchFragment friend = FriendSearchFragment.newInstance();
        friend.setOnFragmentListener(listener);


        ContactFragment contact = ContactFragment.newInstance();
        contact.setViewType(ContactListViewAdapter.ViewType.SEARCH);
        contact.setOnFragmentListener(listener);

        ChatSearchFragment chatSearch = ChatSearchFragment.newInstance();
        chatSearch.setOnFragmentListener(listener);


        mFragments.add(friend);
        mFragments.add(contact);
        mFragments.add(chatSearch);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = (Fragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }
}
