package kr.co.minetalk.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import kr.co.minetalk.R;
import kr.co.minetalk.adapter.data.EmoticonDataModel;
import kr.co.minetalk.adapter.holder.EmoticonViewHolder;

public class EmoticonAdapter extends RecyclerView.Adapter<EmoticonViewHolder>{

    private Context mContext;
    private ArrayList<EmoticonDataModel> mDataList = new ArrayList<>();

    public EmoticonAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setData(ArrayList<EmoticonDataModel> data) {
        this.mDataList.clear();
        this.mDataList.addAll(data);
        notifyDataSetChanged();
    }

    public void clear() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public EmoticonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.emoticon_view_holder, parent,false);
        return new EmoticonViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull EmoticonViewHolder holder, int position) {

        EmoticonDataModel item = mDataList.get(position);
        Glide.with(mContext).asGif().load(item.getEmoticonRes()).into(holder.mBinding.ivEmoticon);

//        Glide.with(mContext).load(item.getList_image()).apply((new RequestOptions()).fitCenter()).into(holder.mBinding.ivImage);
//        holder.mBinding.tvTitle.setText(item.getGuide_title());
//        holder.mBinding.tvDesc.setText(item.getGuide_description());
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public EmoticonDataModel getItemData(int position) {
        return mDataList.get(position);
    }
}
