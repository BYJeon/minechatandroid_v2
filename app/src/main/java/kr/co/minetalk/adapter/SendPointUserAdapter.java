package kr.co.minetalk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.view.image.CornerRoundImageView;

public class SendPointUserAdapter extends BaseAdapter {

    private ArrayList<FriendListModel> mDataList = new ArrayList<>();
    private ArrayList<FriendListModel> mDataListOriginal = new ArrayList<>();

    private int mSelectPosition = -1;
    private String  mFilter = "";

    public void setData(ArrayList<FriendListModel> data) {
        this.mDataList.clear();
        this.mDataListOriginal.clear();
        this.mDataList.addAll(data);
        this.mDataListOriginal.addAll(data);
        notifyDataSetChanged();
    }

    public void clear() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }

    public void setSelectIndex(int position) {
//        if(position != mSelectPosition) {
//            mSelectPosition = position;
//            for(int i = 0 ; i < mDataList.size() ; i++) {
//                if(i == position) {
//                    mDataList.get(i).setIs_select(true);
//                } else {
//                    mDataList.get(i).setIs_select(false);
//                }
//            }
//        } else {
//            mSelectPosition = -1;
//            for(int i = 0 ; i < mDataList.size() ; i++) {
//                mDataList.get(i).setIs_select(false);
//            }
//        }

        for (int i = 0 ; i < mDataList.size() ; i++) {
            if(position == i) {
                mDataList.get(i).setIs_select(true);
            } else {
                mDataList.get(i).setIs_select(false);
            }
        }

        FriendListModel item = mDataList.get(position);
        for(int i = 0 ; i < mDataListOriginal.size() ; i++) {
            if(mDataListOriginal.get(i).getUser_xid().equals(item.getUser_xid())) {
                mDataListOriginal.get(i).setIs_select(true);
                mSelectPosition = i;
            } else {
                mDataListOriginal.get(i).setIs_select(false);
            }
        }

        notifyDataSetChanged();
    }

    public int getSelectIndex() {
        return mSelectPosition;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataListOriginal.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();

        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_select_user_list, parent, false);

            viewHolder.mIvUserImage = (CornerRoundImageView) convertView.findViewById(R.id.iv_user_image);
            viewHolder.mTvUserName = (TextView) convertView.findViewById(R.id.tv_user_name);
            viewHolder.mTvStateMessage = (TextView) convertView.findViewById(R.id.tv_state_message);
            viewHolder.mIvChecked = (ImageView) convertView.findViewById(R.id.iv_checked);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        FriendListModel item = mDataList.get(position);

        String profileImgUrl = item.getUser_profile_image();
        if(profileImgUrl == null || profileImgUrl.equals("")) {
            Glide.with(context).load(R.drawable.img_basic).into(viewHolder.mIvUserImage);
        } else {
            Glide.with(context).load(item.getUser_profile_image()).into(viewHolder.mIvUserImage);
        }

        viewHolder.mTvUserName.setText(item.getUser_name());;
        viewHolder.mIvChecked.setSelected(item.isIs_select());

        return convertView;
    }

    class ViewHolder {
        CornerRoundImageView mIvUserImage;
        TextView mTvUserName;
        TextView mTvStateMessage;
        ImageView mIvChecked;
    }

    public void filter(String str) {
        this.mFilter = str;
        mDataList.clear();
        if(!mFilter.equals("")) {
            for(int i = 0 ; i < mDataListOriginal.size() ; i++ ) {
                FriendListModel item = mDataListOriginal.get(i);
                if(item.getUser_name().toLowerCase().contains(mFilter.toLowerCase()) || item.getUser_hp().toLowerCase().contains(mFilter.toLowerCase())) {
                    mDataList.add(item);
                }
            }
        } else {
            mDataList.addAll(mDataListOriginal);
        }
        notifyDataSetChanged();
    }
}
