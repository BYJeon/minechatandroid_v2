package kr.co.minetalk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.view.image.CornerRoundImageView;

public class FriendSearchListAdapter extends BaseAdapter{

    private ArrayList<FriendListModel> mDataList = new ArrayList<>();

    public void setData(ArrayList<FriendListModel> arrayList) {
        this.mDataList.clear();
        this.mDataList.addAll(arrayList);

        notifyDataSetChanged();
    }

    public void clear() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();

        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.friend_search_list_item, parent, false);

            viewHolder.mIvUserImage = (CornerRoundImageView) convertView.findViewById(R.id.iv_user_image);
            viewHolder.mTvUserName = (TextView) convertView.findViewById(R.id.tv_user_name);
            viewHolder.mTvStateMessage = (TextView) convertView.findViewById(R.id.tv_state_message);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        FriendListModel item = mDataList.get(position);

        String profileImgUrl = item.getUser_profile_image();
        if(profileImgUrl == null || profileImgUrl.equals("")) {
            Glide.with(context).load(R.drawable.img_basic).into(viewHolder.mIvUserImage);
        } else {
            Glide.with(context).load(item.getUser_profile_image()).into(viewHolder.mIvUserImage);
        }

        viewHolder.mTvUserName.setText(item.getUser_name());;
        String stateMessage = item.getUser_state_message();
        if(stateMessage == null || stateMessage.equals("")) {
            viewHolder.mTvStateMessage.setText("");
            viewHolder.mTvStateMessage.setVisibility(View.GONE);
        } else {
            viewHolder.mTvStateMessage.setText(item.getUser_state_message());
            viewHolder.mTvStateMessage.setVisibility(View.VISIBLE);
        }


        return convertView;
    }



    class ViewHolder {
        CornerRoundImageView mIvUserImage;
        TextView mTvUserName;
        TextView mTvStateMessage;

    }
}
