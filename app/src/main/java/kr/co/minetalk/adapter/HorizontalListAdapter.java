package kr.co.minetalk.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.listener.OnFilterListener;
import kr.co.minetalk.databinding.LayoutHorizontalListItemBinding;

public class HorizontalListAdapter extends RecyclerView.Adapter<HorizontalListAdapter.ViewHolder> {
    private ArrayList<String> itemList;
    private Context context;
    private OnFilterListener mListener = null;
    private int mCurSelIdx = 0;

    public HorizontalListAdapter(Context context, ArrayList<String> itemList, OnFilterListener listener) {
        this.context = context;
        this.itemList = itemList;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public HorizontalListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutHorizontalListItemBinding binding = LayoutHorizontalListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new HorizontalListAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(itemList.get(position), position == mCurSelIdx, position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setSelection(int position){
        mCurSelIdx = position;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LayoutHorizontalListItemBinding mBinding;

        public ViewHolder(LayoutHorizontalListItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(String filter, boolean isSelected, int index) {
            mBinding.tvFilterName.setText(filter);
            if(MineTalk.isBlackTheme) {
                mBinding.tvFilterName.setBackgroundResource(isSelected ? R.drawable.bg_round_cbd60c : R.drawable.bg_circle_272f39_line_858992);
                mBinding.tvFilterName.setTextColor(isSelected ? context.getResources().getColor(R.color.main_text_color) : context.getResources().getColor(R.color.bk_main_text_nor_color));
            }else{
                mBinding.tvFilterName.setBackgroundResource(isSelected ? R.drawable.bg_round_16c066 : R.drawable.bg_circle_ffffff_line_d3d3d3);
                mBinding.tvFilterName.setTextColor(isSelected ? context.getResources().getColor(R.color.white_color) : context.getResources().getColor(R.color.main_text_color));
            }

            mBinding.tvFilterName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(index);
                    if(mListener != null)
                        mListener.onFilterChanged(mCurSelIdx == 0 ? "" : mBinding.tvFilterName.getText().toString());
                }
            });
        }
    }
}
