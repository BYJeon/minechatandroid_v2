package kr.co.minetalk.adapter;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.adapter.listener.OnFriendBlockAdapterListener;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.FriendBlockListItemBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class FriendBlockListAdapter extends RecyclerView.Adapter<FriendBlockListAdapter.FriendViewHolder> implements View.OnClickListener {
    private OnFriendBlockAdapterListener mListener = null;
    private OnFriendListViewOrderListener mOrderListener = null;

    private ArrayList<FriendListModel> items;

    @NonNull
    @Override
    public FriendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 0){
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new FriendBlockListAdapter.FriendViewHolder(binding);
        }

        FriendBlockListItemBinding binding = FriendBlockListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FriendBlockListAdapter.FriendViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendViewHolder holder, int position) {
        if(position == 0){
            //FriendListModel item = items.get(position);
            holder.bind(null, mListener);
        }else{
            FriendListModel item = items.get(position-1);
            holder.bind(item, mListener);
        }

    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size() + 1;
    }

    @BindingAdapter("bind:blockList")
    public static void bindItem(RecyclerView recyclerView, ArrayList<FriendListModel> items) {
        FriendBlockListAdapter adapter = (FriendBlockListAdapter) recyclerView.getAdapter();

        if (adapter != null) {
            adapter.setItem(items);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setItem(ArrayList<FriendListModel> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    public ArrayList<FriendListModel> getItem(){return items;}
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_button_block_lift) {
            String user_xid = v.getTag().toString();
            if (mListener != null) {
                mListener.onBlockLift(user_xid);
            }
        }
    }

    public void setAdapterListener(OnFriendBlockAdapterListener listener, OnFriendListViewOrderListener orderListener) {
        this.mListener = listener;
        this.mOrderListener = orderListener;
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class FriendViewHolder extends RecyclerView.ViewHolder {
        FriendBlockListItemBinding mBinding;
        private OnFriendBlockAdapterListener mListener = null;

        LayoutListHeaderViewBinding mHeaderBinding;

        public FriendViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            this.mHeaderBinding = binding;
        }

        public FriendViewHolder(FriendBlockListItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(FriendListModel item, OnFriendBlockAdapterListener listener) {
            if(mBinding == null){ //Header 영역
                mHeaderBinding.btSortList.setText(Preferences.getFriendBlockOrder() ? "가↑" : "가↓");

                mHeaderBinding.ivListType.setVisibility(View.VISIBLE);
                boolean listType = Preferences.getBlockFriendListType();

                if(MineTalk.isBlackTheme) { //GridType
                    mHeaderBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
                } else { //ListType
                    mHeaderBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
                }

                mHeaderBinding.ivListType.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Preferences.setBlockFriendListType(!Preferences.getBlockFriendListType());
                        if(mOrderListener != null)
                            mOrderListener.refreshListType(Preferences.getBlockFriendListType());
                    }
                });

                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Preferences.setFriendBlockOrder(!Preferences.getFriendBlockOrder());
                        mHeaderBinding.btSortList.setText(Preferences.getFriendBlockOrder() ? "가↑" : "가↓");
                        if(mOrderListener != null)
                            mOrderListener.refreshOrder(Preferences.getFriendBlockOrder());
                    }
                });

                String countMessage = String.format("차단친구 %d명", items == null ? 0 : items.size());
                mHeaderBinding.tvCount.setText(countMessage);
            }else{
                this.mListener = listener;
                mBinding.tvButtonBlockLift.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c: R.drawable.button_bg_16c066);
                mBinding.tvButtonBlockLift.setTextColor(MineTalk.isBlackTheme ? MineTalkApp.getAppContext().getResources().getColor(R.color.main_text_color) :
                        MineTalkApp.getAppContext().getResources().getColor(R.color.white_color) );

                mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
                mBinding.tvUserName.setText(MineTalkApp.getUserNameByPhoneNum(item.getUser_name(), item.getUser_hp()));

                if(item.getUser_state_message().equals("")) {
                    mBinding.tvStateMessage.setVisibility(View.GONE);
                    mBinding.tvStateMessage.setText(StringEscapeUtils.unescapeJava(item.getUser_state_message()));
                } else {
                    mBinding.tvStateMessage.setVisibility(View.VISIBLE);
                    mBinding.tvStateMessage.setText(StringEscapeUtils.unescapeJava(item.getUser_state_message()));
                }

                mBinding.tvButtonBlockLift.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mListener != null){
                            mListener.onBlockLift(item.getUser_xid());
                            notifyDataSetChanged();
                        }
                    }
                });

                if (item != null) {
                    String profileImgUrl = item.getUser_profile_image();

                    if (profileImgUrl == null || profileImgUrl.equals("")) {
                        Glide.with(mBinding.getRoot()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                    } else {
                        Glide.with(mBinding.getRoot()).load(item.getUser_profile_image()).into(mBinding.ivUserImage);
                    }
                }
            }
        }
    }
}
