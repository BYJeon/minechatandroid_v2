package kr.co.minetalk.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.minetalk.fragment.ChatFragment;
import kr.co.minetalk.fragment.FriendFragment;
import kr.co.minetalk.fragment.MoreFragment;
import kr.co.minetalk.fragment.SelectChatListFragment;
import kr.co.minetalk.fragment.SelectFriendFragment;
import kr.co.minetalk.fragment.ShopFragment;
import kr.co.minetalk.fragment.TranslationFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;

public class TransmissionPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private Fragment mCurrentFragment;

    public TransmissionPagerAdapter(FragmentManager fragmentManager, OnFragmentEventListener listener) {
        super(fragmentManager);

        mFragments.clear();

        SelectFriendFragment selectFriendFragment = SelectFriendFragment.newInstance();
        SelectChatListFragment selectChatListFragment = SelectChatListFragment.newInstance();

        mFragments.add(selectFriendFragment);
        mFragments.add(selectChatListFragment);


    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = (Fragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }
}
