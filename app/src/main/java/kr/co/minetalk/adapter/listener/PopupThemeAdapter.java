package kr.co.minetalk.adapter.listener;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.minetalk.R;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;

public class PopupThemeAdapter extends BaseAdapter {

    private ArrayList<CountryInfoModel> mDataList = new ArrayList<>();
    private String mSystemLanguage = "";

    public PopupThemeAdapter() {
        mSystemLanguage = Preferences.getLanguage();
    }

    public void setData(ArrayList<CountryInfoModel> data) {
        this.mDataList.clear();
        this.mDataList.addAll(data);
        notifyDataSetChanged();
    }

    public void clear() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();

        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_popup_language, parent, false);

            viewHolder.mIvFlag = (ImageView) convertView.findViewById(R.id.iv_flag);
            viewHolder.mTvCountryName = (TextView) convertView.findViewById(R.id.tv_country_name);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CountryInfoModel item = mDataList.get(position);

        int flagImage = CommonUtils.getResourceImage(context, item.getFlag_image());

        viewHolder.mIvFlag.setImageResource(flagImage);

        if(item.getFlag_image().equals("theme_normal")){
            viewHolder.mIvFlag.setBackgroundColor(context.getResources().getColor(R.color.main_theme_bg));
        }else if(item.getFlag_image().equals("theme_black")){
            viewHolder.mIvFlag.setBackgroundColor(context.getResources().getColor(R.color.bk_theme_bg));
        }

        if(mSystemLanguage != null && mSystemLanguage.equals("ko")) {
            viewHolder.mTvCountryName.setText(item.getCountryName());
        } else {
            viewHolder.mTvCountryName.setText(item.getCountryNameOther());
        }

        return convertView;
    }

    class ViewHolder {
        ImageView mIvFlag;
        TextView  mTvCountryName;
    }


}
