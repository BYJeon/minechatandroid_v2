package kr.co.minetalk.adapter;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.PhotoViewerActivity;
import kr.co.minetalk.adapter.listener.OnChatMessageListener;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.message.manager.ThreadController;
import kr.co.minetalk.message.model.MessageData;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.view.image.CornerRoundImageView;

public class ChatDetailAdapter extends BaseAdapter {

    public static int MSG_TYPE_ME = 0;
    public static int MSG_TYPE_OTHER = 1;

    private final float COMPRESS_RATIO = 0.3f;

    private ArrayList<MessageData> mDataList = new ArrayList<>();
    private Map<Integer, String> mDatamap = new HashMap<>();

    private SimpleDateFormat mDtFormat = new SimpleDateFormat("HH:mm");
    private SimpleDateFormat mDtFullFormat = new SimpleDateFormat("yyyy년 MM월 dd일 E요일");
    private OnChatMessageListener mListener;

    private Context mContext;
    private TextToSpeech mTTS;

    private boolean mIsAutoTrans = false;
    private boolean mIsSecurity = false;
    private ThreadController mThreadController = null;
    private DownloadManager mDownloadManager = null;
    private long mDownloadReference;
    private BroadcastReceiver receiverDownloadComplete;    //다운로드 완료 체크
    private BroadcastReceiver receiverNotificationClicked;    //다운로드 시작 체크

    public ChatDetailAdapter(Context context) {
        this.mContext = context;

    }


    private void initTTS(final String langCode, String text) {
        if (mTTS != null) {
            mTTS.stop();
            mTTS.shutdown();
            mTTS = null;
        }

        mTTS = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    //mTTS.setEngineByPackageName("com.google.android.tts");
                    //구글 API langCode는 ja인데 ja_JP를 넣어야 음성이 나온다.
                    Locale loc = new Locale(langCode.contains("ja") ? "ja_JP" : langCode);
                    mTTS.setLanguage(loc);

                    mTTS.setPitch(1.0f);
                    mTTS.setSpeechRate(1.0f);
                    try {
                        mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }
        }, "com.google.android.tts");
    }


    public void setAutoTrans(boolean bAutoTrans) {
        this.mIsAutoTrans = bAutoTrans;
    }

    public ArrayList<MessageData> getData() {
        return mDataList;
    }

    public void setData(ArrayList<MessageData> data) {
        this.mDataList.clear();
        this.mDatamap.clear();

        //this.mDataList.addAll(data);

        for(MessageData item : data){
            if(!mDatamap.containsKey(item.getDbIndex())){
                mDatamap.put(item.getSeq(), item.getMessage());
                mDataList.add(item);
            }
        }

        notifyDataSetChanged();
    }

    public void setData(ArrayList<MessageData> data, boolean isSecurity) {
        this.mDataList.clear();
        this.mDatamap.clear();
        this.mIsSecurity = isSecurity;

        //this.mDataList.addAll(data);
        for(MessageData item : data){
            if(!mDatamap.containsKey(item.getDbIndex())){
                mDatamap.put(item.getSeq(), item.getMessage());
                mDataList.add(item);
            }
        }

        notifyDataSetChanged();
    }

    public void clearAll(){
        this.mDataList.clear();
        this.mDatamap.clear();

        notifyDataSetChanged();
    }
    public void setEventListener(OnChatMessageListener listener) {
        this.mListener = listener;
    }

    public void setThreadController(ThreadController threadController){
        this.mThreadController = threadController;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();

        final ViewHolder changeViewHolder = viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.chat_detail_item_layout, parent, false);

            viewHolder.layoutOther = (LinearLayout) convertView.findViewById(R.id.layout_other);

            viewHolder.layoutUserPrfoile = (RelativeLayout) convertView.findViewById(R.id.layout_user_profile);
            viewHolder.ivOtherUserImg = (CornerRoundImageView) convertView.findViewById(R.id.iv_other_user_image);
            viewHolder.tvOtherUserName = (TextView) convertView.findViewById(R.id.tv_other_user_name);
            viewHolder.tvOtherMsgTime = (TextView) convertView.findViewById(R.id.tv_other_msg_time);

            viewHolder.layoutOtherText = (LinearLayout) convertView.findViewById(R.id.layout_other_text);
            viewHolder.tvOtherMessage = (TextView) convertView.findViewById(R.id.tv_other_msg);
            viewHolder.ivOtherEmoticon = (ImageView) convertView.findViewById(R.id.iv_other_emoticon);
            viewHolder.layoutOtherTextBox = (LinearLayout) convertView.findViewById(R.id.layout_other_text_box);

            viewHolder.tvButtonTrans = (TextView) convertView.findViewById(R.id.tv_btn_trans);
            viewHolder.tvOtherTransMessage = (TextView) convertView.findViewById(R.id.tv_other_trans_msg);
            viewHolder.layoutTranslationAear = (LinearLayout) convertView.findViewById(R.id.layout_translation_area);

            viewHolder.layoutOtherImage = (LinearLayout) convertView.findViewById(R.id.layout_other_image);
            viewHolder.ivOtherImage = (CornerRoundImageView) convertView.findViewById(R.id.iv_other_image);

            viewHolder.layoutOtherContact = (LinearLayout) convertView.findViewById(R.id.layout_other_contact);
            viewHolder.tvOtherContactName = (TextView) convertView.findViewById(R.id.tv_other_contact_name);


            viewHolder.layoutMe = (LinearLayout) convertView.findViewById(R.id.layout_me);

            viewHolder.layoutMeText = (LinearLayout) convertView.findViewById(R.id.layout_me_text);
            viewHolder.tvMeMessage = (TextView) convertView.findViewById(R.id.tv_me_msg);
            viewHolder.ivMeEmoticon = (ImageView) convertView.findViewById(R.id.iv_me_emoticon);
            viewHolder.layoutMeTextBox = (LinearLayout) convertView.findViewById(R.id.layout_me_text_box);

            //viewHolder.ivMeMsgVoice = (ImageView) convertView.findViewById(R.id.iv_me_voice);

            viewHolder.layoutMeImage = (RelativeLayout) convertView.findViewById(R.id.layout_me_image);
            viewHolder.ivMeImage = (CornerRoundImageView) convertView.findViewById(R.id.iv_me_image);
            viewHolder.layoutMeVideo = (RelativeLayout) convertView.findViewById(R.id.layout_me_video);
            viewHolder.layoutMeContact = (LinearLayout) convertView.findViewById(R.id.layout_me_contact);
            viewHolder.tvMeContactName = (TextView) convertView.findViewById(R.id.tv_me_contact_name);

            viewHolder.layoutInviteMessage = (RelativeLayout) convertView.findViewById(R.id.layout_invite_msg);
            viewHolder.tvInviteMessage = (TextView) convertView.findViewById(R.id.tv_invite_message);

            viewHolder.tvTransTitle = convertView.findViewById(R.id.tv_other_trans_title);
            viewHolder.ivOriginMsgVoice = (ImageView) convertView.findViewById(R.id.iv_origin_trans_voice);
            viewHolder.ivOtherMsgTransVoice = (ImageView) convertView.findViewById(R.id.iv_other_trans_voice);
            viewHolder.ivOtherMsgVoice = (ImageView) convertView.findViewById(R.id.iv_other_voice);

            viewHolder.tvMeMsgTime = (TextView) convertView.findViewById(R.id.tv_me_msg_time);
            viewHolder.layoutOtherVideo = (RelativeLayout) convertView.findViewById(R.id.layout_other_video);


            viewHolder.layoutMeFile = (LinearLayout) convertView.findViewById(R.id.layout_me_file);
            viewHolder.tvMeFileName = (TextView) convertView.findViewById(R.id.tv_me_file_name);

            viewHolder.layoutOtherFile = (LinearLayout) convertView.findViewById(R.id.layout_other_file);
            viewHolder.tvOtherFileName = (TextView) convertView.findViewById(R.id.tv_other_file_name);
            viewHolder.tvBtnOtherFileSave = (TextView) convertView.findViewById(R.id.tv_other_file_download);

            viewHolder.layoutMeTextAear = convertView.findViewById(R.id.layout_me_chat_arear);
            viewHolder.layoutTransVoiceAear = convertView.findViewById(R.id.layout_trans_voice_root);
            viewHolder.layoutOriginVoiceAear = convertView.findViewById(R.id.layout_origin_voice_root);

            //내 메세지 읽은 카운트
            viewHolder.tvMeMsgReadCount = convertView.findViewById(R.id.tv_me_msg_read_count);
            viewHolder.tvOtherMsgReadCount = convertView.findViewById(R.id.tv_other_msg_read_count);
            viewHolder.ivMeMsgReadCount = convertView.findViewById(R.id.iv_me_msg_read_count);

            //이모션 읽은 카운트
            viewHolder.tvMeEmotionTime = (TextView) convertView.findViewById(R.id.tv_me_emotion_time);
            viewHolder.layoutEmotionAear = convertView.findViewById(R.id.layout_emotion_root);
            viewHolder.tvMeEmotionReadCount = convertView.findViewById(R.id.tv_me_emotion_read_count);
            viewHolder.ivMeEmotionReadCount = convertView.findViewById(R.id.iv_me_emotion_read_count);

            //상대방 이모션 읽은 카운
            viewHolder.tvOtherEmotionTime = convertView.findViewById(R.id.tv_other_emotion_time);
            viewHolder.layoutOtherEmotionAear = convertView.findViewById(R.id.layout_emotion_other_root);
            viewHolder.tvOtherEmotionReadCount = convertView.findViewById(R.id.tv_other_emotion_read_count);
            viewHolder.ivOtherEmotionReadCount = convertView.findViewById(R.id.iv_other_emotion_read_count);

            //메세지 상단 Date
            viewHolder.tvMsgDate = convertView.findViewById(R.id.tv_date);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        updateTheme(viewHolder);

        String me_xid = MineTalkApp.getUserInfoModel().getUser_xid();
        MessageData item = mDataList.get(position);

        MessageJSonModel msgModel = new MessageJSonModel();
        try {
            JSONObject msgObject = new JSONObject(item.getMessage());
            msgModel = MessageJSonModel.parse(msgObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if(position == 0){
            viewHolder.tvMsgDate.setVisibility(View.VISIBLE);
            viewHolder.tvMsgDate.setText(mDtFullFormat.format(item.getSendDate()));
        }else{
            String preDate = mDtFullFormat.format(mDataList.get(position-1).getSendDate());
            String curDate = mDtFullFormat.format(item.getSendDate());

            if(preDate.equals(curDate)){
                viewHolder.tvMsgDate.setVisibility(View.GONE);
            }else{
                viewHolder.tvMsgDate.setVisibility(View.VISIBLE);
                viewHolder.tvMsgDate.setText(curDate);
            }
        }

        if (me_xid.equals(item.getXid())) {
            viewHolder.layoutMe.setVisibility(View.VISIBLE);
            viewHolder.layoutOther.setVisibility(View.GONE);
            viewHolder.layoutInviteMessage.setVisibility(View.GONE);
            viewHolder.tvMeMsgTime.setText(mDtFormat.format(item.getSendDate()));
            viewHolder.tvMeEmotionTime.setText(mDtFormat.format(item.getSendDate()));

            int readCount = 0;
            if(mThreadController != null ){
                readCount = mThreadController.unReadCountByMessageSeq(mDataList.get(position).getSeq());
            }
            //안읽은 메세지가 있을 경우
            if( readCount != 0 ) {
                viewHolder.tvMeMsgReadCount.setVisibility(View.VISIBLE);
                viewHolder.tvMeMsgReadCount.setText(Integer.toString(readCount));

                viewHolder.tvMeEmotionReadCount.setVisibility(View.VISIBLE);
                viewHolder.tvMeEmotionReadCount.setText(Integer.toString(readCount));

                //1:1 채팅일 경우 안읽음 표시를 이미지로 한다.
                if(mThreadController.getMemberCount() == 2){
                    viewHolder.tvMeMsgReadCount.setVisibility(View.INVISIBLE);
                    viewHolder.ivMeMsgReadCount.setImageResource(MineTalk.isBlackTheme ? R.drawable.icon_no_read_bk : R.drawable.icon_no_read);
                    viewHolder.ivMeMsgReadCount.setVisibility(View.VISIBLE);

                    viewHolder.tvMeEmotionReadCount.setVisibility(View.INVISIBLE);
                    viewHolder.ivMeEmotionReadCount.setImageResource(MineTalk.isBlackTheme ? R.drawable.icon_no_read_bk : R.drawable.icon_no_read);
                    viewHolder.ivMeEmotionReadCount.setVisibility(View.VISIBLE);

                    if(mIsSecurity) {
                        viewHolder.ivMeMsgReadCount.setImageResource(R.drawable.icon_no_read_security);
                        viewHolder.ivMeEmotionReadCount.setImageResource(R.drawable.icon_no_read_security);
                    }
                }
            }else{
                viewHolder.tvMeMsgReadCount.setVisibility(View.INVISIBLE);
                viewHolder.ivMeMsgReadCount.setVisibility(View.INVISIBLE);

                viewHolder.tvMeEmotionReadCount.setVisibility(View.INVISIBLE);
                viewHolder.ivMeEmotionReadCount.setVisibility(View.INVISIBLE);
            }

            if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_TEXT) || msgModel.getDeleteYn().equals("Y")
                || (item.getDeleteFlag() != null && item.getDeleteFlag().equals("Y")) ) {
                viewHolder.layoutMeText.setVisibility(View.VISIBLE);
                viewHolder.layoutMeImage.setVisibility(View.GONE);
                viewHolder.layoutMeVideo.setVisibility(View.GONE);
                viewHolder.layoutMeContact.setVisibility(View.GONE);
                viewHolder.layoutMeFile.setVisibility(View.GONE);

                if (msgModel.getDeleteYn().equals("Y") ||
                        (item.getDeleteFlag() != null && item.getDeleteFlag().equals("Y"))) {

                    //메세지 삭제 이미지
                    Drawable deleteImage = MineTalkApp.getAppContext().getResources().getDrawable( R.drawable.icon_msg_warning_w );
                    int h = deleteImage.getIntrinsicHeight();
                    int w = deleteImage.getIntrinsicWidth();
                    deleteImage.setBounds( 0, 0, w, h );

                    viewHolder.layoutMeTextAear.setVisibility(View.VISIBLE);
                    viewHolder.layoutMeTextBox.setVisibility(View.VISIBLE);
                    viewHolder.tvMeMsgTime.setVisibility(View.VISIBLE);
                    viewHolder.tvMeMessage.setText(context.getResources().getString(R.string.chat_message_deleted));
                    viewHolder.tvMeMessage.setTextColor(MineTalk.isBlackTheme ? Color.parseColor("#80858992") : Color.parseColor("#80ffffff"));
                    viewHolder.tvMeMessage.setCompoundDrawablePadding(10);
                    viewHolder.tvMeMessage.setCompoundDrawables( deleteImage, null, null, null );
                    viewHolder.layoutEmotionAear.setVisibility(View.GONE);
                    //viewHolder.ivMeMsgVoice.setVisibility(View.GONE);

                } else {
                    viewHolder.tvMeMessage.setCompoundDrawables( null, null, null, null );

                    String messge = StringEscapeUtils.unescapeJava(msgModel.getText());
                    if (messge == null || messge.equals("")) {
                        viewHolder.layoutMeTextBox.setVisibility(View.GONE);
                        viewHolder.tvMeMessage.setText("");
                    } else {
                        viewHolder.layoutMeTextBox.setVisibility(View.VISIBLE);
                        viewHolder.tvMeMessage.setText(messge);
                    }

                    if (msgModel.getExtra() != null && !msgModel.getExtra().equals("")) {
                        viewHolder.layoutEmotionAear.setVisibility(View.VISIBLE);
                        viewHolder.ivMeEmoticon.setVisibility(View.VISIBLE);
                        viewHolder.tvMeEmotionTime.setVisibility(View.VISIBLE);

                        viewHolder.layoutMeTextAear.setVisibility(View.GONE);
                        viewHolder.ivMeMsgReadCount.setVisibility(View.GONE);
                        viewHolder.tvMeMsgReadCount.setVisibility(View.GONE);
                        viewHolder.tvMeMsgTime.setVisibility(View.GONE);
                        int resId = CommonUtils.getResourceImage(context, msgModel.getExtra());
                        Glide.with(context).asGif().load(resId).into(viewHolder.ivMeEmoticon);
                    } else {
                        viewHolder.layoutMeTextAear.setVisibility(View.VISIBLE);
                        //viewHolder.ivMeMsgReadCount.setVisibility(View.VISIBLE);
                        //viewHolder.tvMeMsgReadCount.setVisibility(View.VISIBLE);
                        viewHolder.tvMeMsgTime.setVisibility(View.VISIBLE);

                        viewHolder.layoutEmotionAear.setVisibility(View.GONE);
                        viewHolder.ivMeEmoticon.setVisibility(View.GONE);
                        viewHolder.tvMeEmotionTime.setVisibility(View.GONE);
                    }
                }
            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_IMAGE)) {
                viewHolder.layoutMeText.setVisibility(View.GONE);
                viewHolder.layoutMeImage.setVisibility(View.VISIBLE);
                viewHolder.layoutMeVideo.setVisibility(View.GONE);
                viewHolder.layoutMeContact.setVisibility(View.GONE);
                viewHolder.layoutMeFile.setVisibility(View.GONE);

                viewHolder.layoutMeImage.setTag(item);


                if (!msgModel.getWidth().equals("") && msgModel.getWidth() != null) {
                    int imageWidth = Integer.valueOf(msgModel.getWidth());
                    if (imageWidth < 660) {
                        viewHolder.ivMeImage.getLayoutParams().width = imageWidth;
                    } else {
                        viewHolder.ivMeImage.getLayoutParams().width = context.getResources().getDimensionPixelOffset(R.dimen.width_660);
                    }
                } else {
                    viewHolder.ivMeImage.getLayoutParams().width = context.getResources().getDimensionPixelOffset(R.dimen.width_660);
                }

//                synchronized (context) {
//                    RequestOptions requestOptions = new RequestOptions();
//                    requestOptions.skipMemoryCache(true);
//                    requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
//                    requestOptions.placeholder(R.mipmap.ic_launcher);
//                    requestOptions.error(R.mipmap.ic_launcher);
//
//                    Glide.with(context).load(msgModel.getFileUrl()).apply(requestOptions).into(viewHolder.ivMeImage);
//                }

                //Glide.with(context).load(msgModel.getFileUrl()).into(viewHolder.ivMeImage);

                String width = msgModel.getWidth();
                String height = msgModel.getHeight();

                if(!width.isEmpty() && !height.isEmpty()){
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.override((int)(Integer.parseInt(width) * COMPRESS_RATIO), (int)(Integer.parseInt(height) * COMPRESS_RATIO));
                    requestOptions.placeholder(android.R.color.transparent);

                    Glide
                            .with(context)
                            .load(msgModel.getFileUrl())
//                            .apply(new RequestOptions().override((int)(Integer.parseInt(width) * COMPRESS_RATIO), (int)(Integer.parseInt(height) * COMPRESS_RATIO)))
                            .apply(requestOptions)
                            .into(viewHolder.ivMeImage);
                }else{
                    Glide.with(context).load(msgModel.getFileUrl()).into(viewHolder.ivMeImage);
                }



                viewHolder.layoutMeImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MessageJSonModel item = new MessageJSonModel();
                        try {
                            MessageData msd = (MessageData) v.getTag();
                            item = MessageJSonModel.parse(new JSONObject(msd.getMessage()));
                        } catch (JSONException e) {
                            item = null;
                            e.printStackTrace();
                        }

                        if (item == null) return;

                        String contactName = MineTalkApp.getUserInfoModel().getUser_name();
                        PhotoViewerActivity.startActivity(context, item.getFileUrl(), "", contactName,true);

                    }
                });

            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_VIDEO)) {
                viewHolder.layoutMeText.setVisibility(View.GONE);
                viewHolder.layoutMeImage.setVisibility(View.GONE);
                viewHolder.layoutMeVideo.setVisibility(View.VISIBLE);
                viewHolder.layoutMeContact.setVisibility(View.GONE);
                viewHolder.layoutMeFile.setVisibility(View.GONE);

                viewHolder.layoutMeVideo.setTag(item);
                viewHolder.layoutMeVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MessageJSonModel item = new MessageJSonModel();
                        try {
                            MessageData msd = (MessageData) v.getTag();
                            item = MessageJSonModel.parse(new JSONObject(msd.getMessage()));
                        } catch (JSONException e) {
                            item = null;
                            e.printStackTrace();
                        }

                        if (item == null) return;

                        Uri uri = Uri.parse(item.getFileUrl());
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                        intent.setDataAndType(uri, "video/*");
                        context.startActivity(intent);
                    }
                });

            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_CONTACT)) {
                viewHolder.layoutMeText.setVisibility(View.GONE);
                viewHolder.layoutMeImage.setVisibility(View.GONE);
                viewHolder.layoutMeVideo.setVisibility(View.GONE);
                viewHolder.layoutMeContact.setVisibility(View.VISIBLE);
                viewHolder.layoutMeFile.setVisibility(View.GONE);

                viewHolder.tvMeContactName.setText(msgModel.getContactName());

                viewHolder.layoutMeContact.setTag(item);
                viewHolder.layoutMeContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MessageJSonModel item = new MessageJSonModel();
                        try {
                            MessageData msd = (MessageData) v.getTag();
                            item = MessageJSonModel.parse(new JSONObject(msd.getMessage()));
                        } catch (JSONException e) {
                            item = null;
                            e.printStackTrace();
                        }

                        if (item == null) return;

                        Intent intent = new Intent(Intent.ACTION_INSERT, ContactsContract.Contacts.CONTENT_URI);

                        Bundle bundle = new Bundle();
                        bundle.putString(ContactsContract.Intents.Insert.PHONE, item.getContactHp());
                        bundle.putString(ContactsContract.Intents.Insert.NAME, item.getContactName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });

            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_FILE)) {
                viewHolder.layoutMeText.setVisibility(View.GONE);
                viewHolder.layoutMeImage.setVisibility(View.GONE);
                viewHolder.layoutMeVideo.setVisibility(View.GONE);
                viewHolder.layoutMeContact.setVisibility(View.GONE);
                viewHolder.layoutMeFile.setVisibility(View.VISIBLE);

                //String name = chooseFileName(msgModel.getFileUrl());
                String name = msgModel.getExtra();
                viewHolder.tvMeFileName.setText(name);
            }

            viewHolder.layoutMeText.setTag(item);
            viewHolder.layoutMeImage.setTag(item);
            viewHolder.layoutMeVideo.setTag(item);
            viewHolder.layoutMeContact.setTag(item);

            viewHolder.layoutMeText.setOnLongClickListener(msgMeLongPressListener);
            viewHolder.layoutMeImage.setOnLongClickListener(msgMeLongPressListener);
            viewHolder.layoutMeVideo.setOnLongClickListener(msgMeLongPressListener);
            viewHolder.layoutMeContact.setOnLongClickListener(msgMeLongPressListener);


        } else {
            boolean isDeleteMsg = false;
            viewHolder.layoutMe.setVisibility(View.GONE);
            viewHolder.layoutOther.setVisibility(View.VISIBLE);

            int readCount = 0;
            if(mThreadController != null ){
                readCount = mThreadController.unReadCountByMessageSeq(mDataList.get(position).getSeq());
            }

            //안읽은 메세지가 있을 경우
            if( readCount != 0 ) {
                viewHolder.tvOtherMsgReadCount.setVisibility(View.VISIBLE);
                viewHolder.tvOtherMsgReadCount.setText(Integer.toString(readCount));

                viewHolder.tvOtherEmotionReadCount.setVisibility(View.VISIBLE);
                viewHolder.tvOtherEmotionReadCount.setText(Integer.toString(readCount));
            }else{
                viewHolder.tvOtherMsgReadCount.setVisibility(View.INVISIBLE);
                viewHolder.tvOtherEmotionReadCount.setVisibility(View.INVISIBLE);
            }

            String profileUrl = msgModel.getSenderProfileUrl();
            if (profileUrl != null && !profileUrl.equals("")) {
                Glide.with(context).load(profileUrl).into(viewHolder.ivOtherUserImg);
            } else {
                Glide.with(context).load(R.drawable.img_basic).into(viewHolder.ivOtherUserImg);
            }
            viewHolder.layoutUserPrfoile.setTag(item.getXid());
            viewHolder.layoutUserPrfoile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String otherXid = (String) v.getTag();
                    if (mListener != null) {
                        mListener.onProfile(otherXid);
                    }
                }
            });


            String contactName = MineTalkApp.getContactUserName(msgModel.getSenderHp());
            if (contactName == null || contactName.equals("")) {
                viewHolder.tvOtherUserName.setText(item.getNickName());
            } else {
                viewHolder.tvOtherUserName.setText(contactName);
            }

            viewHolder.tvOtherMsgTime.setText(mDtFormat.format(item.getSendDate()));
            viewHolder.tvOtherEmotionTime.setText(mDtFormat.format(item.getSendDate()));

            if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_TEXT) || msgModel.getMsgType().equals(MineTalk.MSG_TYPE_URL)) {
                viewHolder.layoutOtherText.setVisibility(View.VISIBLE);
                viewHolder.layoutOtherImage.setVisibility(View.GONE);
                viewHolder.layoutOtherContact.setVisibility(View.GONE);
                viewHolder.layoutInviteMessage.setVisibility(View.GONE);
                viewHolder.layoutOtherVideo.setVisibility(View.GONE);
                viewHolder.layoutOtherFile.setVisibility(View.GONE);

                if (msgModel.getDeleteYn().equals("Y") ||
                    (item.getDeleteFlag() != null && item.getDeleteFlag().equals("Y")) ) {
                    isDeleteMsg = true;

                    //메세지 삭제 이미지
                    Drawable deleteImage = MineTalkApp.getAppContext().getResources().getDrawable( MineTalk.isBlackTheme ? R.drawable.icon_msg_warning_w : R.drawable.icon_msg_warning );
                    int h = deleteImage.getIntrinsicHeight();
                    int w = deleteImage.getIntrinsicWidth();
                    deleteImage.setBounds( 0, 0, w, h );

                    viewHolder.layoutOtherTextBox.setVisibility(View.VISIBLE);
                    viewHolder.tvButtonTrans.setEnabled(false);
                    viewHolder.tvButtonTrans.setVisibility(View.GONE);
                    viewHolder.tvOtherMessage.setText(context.getResources().getString(R.string.chat_message_deleted));
                    viewHolder.tvOtherMessage.setTextColor(Color.parseColor("#80858992"));
                    viewHolder.tvOtherMessage.setCompoundDrawablePadding(10);
                    viewHolder.tvOtherMessage.setCompoundDrawables( deleteImage, null, null, null );

                    viewHolder.ivOriginMsgVoice.setVisibility(View.GONE);
                    viewHolder.layoutOtherEmotionAear.setVisibility(View.GONE);

                } else {
                    viewHolder.tvOtherMessage.setCompoundDrawables( null, null, null, null );

                    viewHolder.tvButtonTrans.setEnabled(true);
                    viewHolder.tvButtonTrans.setVisibility(View.VISIBLE);

                    String messge = StringEscapeUtils.unescapeJava(msgModel.getText());
                    if (messge == null || messge.equals("")) {
                        viewHolder.layoutOtherTextBox.setVisibility(View.GONE);
                        viewHolder.tvOtherMessage.setText("");
                    } else {
                        viewHolder.layoutOtherTextBox.setVisibility(View.VISIBLE);
                        viewHolder.tvOtherMessage.setText(messge);
                    }

                    if (msgModel.getExtra() != null && !msgModel.getExtra().equals("")) {
                        viewHolder.layoutOtherEmotionAear.setVisibility(View.VISIBLE);
                        //viewHolder.ivOtherEmoticon.setVisibility(View.VISIBLE);

                        viewHolder.layoutOtherTextBox.setVisibility(View.GONE);
                        //viewHolder.tvOtherMsgTime.setVisibility(View.GONE);

                        int resId = CommonUtils.getResourceImage(context, msgModel.getExtra());
                        Glide.with(context).asGif().load(resId).into(viewHolder.ivOtherEmoticon);
                    } else {
                        viewHolder.layoutOtherTextBox.setVisibility(View.VISIBLE);
                        //viewHolder.tvOtherMsgTime.setVisibility(View.VISIBLE);

                        viewHolder.layoutOtherEmotionAear.setVisibility(View.GONE);
                        //viewHolder.ivOtherEmoticon.setVisibility(View.GONE);
                    }
                }


                //번역 설정 유무
                if (item.getTran() == null || item.getTran().equals("")) {
                    viewHolder.layoutTranslationAear.setVisibility(View.GONE);
                    viewHolder.layoutTransVoiceAear.setVisibility(View.GONE);
                    //viewHolder.layoutOriginVoiceAear.setVisibility(View.GONE);

                    //viewHolder.tvButtonTrans.setVisibility(View.VISIBLE);
                    viewHolder.tvButtonTrans.setText(Html.fromHtml(context.getResources().getString(R.string.chat_button_trans_visible)));
                    viewHolder.tvButtonTrans.setTag(item);
                    viewHolder.layoutOriginVoiceAear.setTag(item);


                    //자동 번역 설정
                    if (mIsAutoTrans && !isDeleteMsg) {
                        if (mListener != null) {
                            mListener.onTranslation(item);
                        }
                    }

                    viewHolder.layoutOriginVoiceAear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MessageData mdData = (MessageData) v.getTag();
                            String tMsg = "";
                            String tLangCode = "";
                            try {
                                JSONObject originObject = new JSONObject(mdData.getMessage());
                                tMsg = originObject.getString("text");
                                tLangCode = originObject.getString("langCode");

                                initTTS(tLangCode, StringEscapeUtils.unescapeJava(tMsg));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                tMsg = item.getTran();
                            }
                        }
                    });

                    viewHolder.tvButtonTrans.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mListener != null) {
                                MessageData viewData = (MessageData) v.getTag();
                                mListener.onTranslation(viewData);
                            }
                        }
                    });
                } else {
                    viewHolder.layoutTranslationAear.setVisibility(isDeleteMsg ? View.GONE : View.VISIBLE);
                    viewHolder.layoutTransVoiceAear.setVisibility(isDeleteMsg ? View.GONE :View.VISIBLE);
                    //viewHolder.layoutOriginVoiceAear.setVisibility(View.VISIBLE);
                    viewHolder.tvButtonTrans.setVisibility(View.GONE);

                    String transMsg = "";
                    try {
                        JSONObject transObject = new JSONObject(item.getTran());
                        transMsg = transObject.getString("text");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        transMsg = item.getTran();
                    }
                    viewHolder.tvOtherTransMessage.setText(StringEscapeUtils.unescapeJava(transMsg));

                    viewHolder.ivOtherMsgTransVoice.setTag(item);
                    viewHolder.layoutTransVoiceAear.setTag(item);
                    viewHolder.layoutOriginVoiceAear.setTag(item);

                    viewHolder.layoutOriginVoiceAear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MessageData mdData = (MessageData) v.getTag();
                            String tMsg = "";
                            String tLangCode = "";
                            try {
                                JSONObject originObject = new JSONObject(mdData.getMessage());
                                tMsg = originObject.getString("text");
                                tLangCode = originObject.getString("langCode");

                                initTTS(tLangCode, StringEscapeUtils.unescapeJava(tMsg));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                tMsg = item.getTran();
                            }
                        }
                    });

                    viewHolder.layoutTransVoiceAear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MessageData mdData = (MessageData) v.getTag();
                            String tMsg = "";
                            String tLangCode = "";
                            try {
                                JSONObject transObject = new JSONObject(mdData.getTran());
                                tMsg = transObject.getString("text");
                                tLangCode = transObject.getString("languageCode");

                                initTTS(tLangCode, StringEscapeUtils.unescapeJava(tMsg));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                tMsg = item.getTran();
                            }
                        }
                    });

                }


                viewHolder.ivOtherMsgVoice.setTag(msgModel);
                viewHolder.ivOtherMsgVoice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        MessageJSonModel msgJsonModel = (MessageJSonModel) v.getTag();
                        initTTS(msgJsonModel.getLanguageCode(), StringEscapeUtils.unescapeJava(msgJsonModel.getText()));
//                        String voiceCode = CountryRepository.getInstance().getCountryModel(msgJsonModel.getLanguageCode()).getVoice_code();
//                        Log.e("@@@TAG", "onclick ivOtherMsg : " + voiceCode);


                    }
                });

//                viewHolder.ivOtherMsgTransVoice.setTag(item);
//                viewHolder.ivOtherMsgTransVoice.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Log.e("@@@TAG", "onclick ivOtherMsgTransVoice + " + item.getTran());
//                        MessageData msg = (MessageData)v.getTag();
//                        String targetCode = "";
//                        try {
//                            JSONObject transObject = new JSONObject(item.getTran());
//                            targetCode = transObject.getString("target");
//                            String targetVoiceCode = CountryRepository.getInstance().getCountryModel(targetCode).getVoice_code();
//                            Log.e("@@@TAG", "targetVoiceCode " + targetVoiceCode);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });


            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_IMAGE)) {
                viewHolder.layoutOtherImage.setVisibility(View.VISIBLE);
                viewHolder.layoutOtherText.setVisibility(View.GONE);
                viewHolder.layoutOtherContact.setVisibility(View.GONE);
                viewHolder.layoutInviteMessage.setVisibility(View.GONE);
                viewHolder.layoutOtherVideo.setVisibility(View.GONE);
                viewHolder.layoutOtherFile.setVisibility(View.GONE);

                if (msgModel.getDeleteYn().equals("Y") ||
                        (item.getDeleteFlag() != null && item.getDeleteFlag().equals("Y")) ) {
                    isDeleteMsg = true;

                    //메세지 삭제 이미지
                    Drawable deleteImage = MineTalkApp.getAppContext().getResources().getDrawable( MineTalk.isBlackTheme ? R.drawable.icon_msg_warning_w : R.drawable.icon_msg_warning );
                    int h = deleteImage.getIntrinsicHeight();
                    int w = deleteImage.getIntrinsicWidth();
                    deleteImage.setBounds( 0, 0, w, h );

                    viewHolder.tvButtonTrans.setEnabled(false);
                    viewHolder.tvButtonTrans.setVisibility(View.GONE);
                    viewHolder.layoutOtherText.setVisibility(View.VISIBLE);
                    viewHolder.layoutOtherTextBox.setVisibility(View.VISIBLE);
                    viewHolder.tvOtherMessage.setText(context.getResources().getString(R.string.chat_message_deleted));
                    viewHolder.tvOtherMessage.setTextColor(Color.parseColor("#80858992"));
                    viewHolder.tvOtherMessage.setCompoundDrawablePadding(10);
                    viewHolder.tvOtherMessage.setCompoundDrawables( deleteImage, null, null, null );

                    viewHolder.layoutTranslationAear.setVisibility(View.GONE);
                    viewHolder.ivOriginMsgVoice.setVisibility(View.GONE);
                    viewHolder.layoutOtherImage.setVisibility(View.GONE);

                } else {
                    viewHolder.layoutOtherImage.setVisibility(View.VISIBLE);
                    // Image Size
                    if (!msgModel.getWidth().equals("") && msgModel.getWidth() != null) {
                        int imageWidth = 0;
                        try {
                            imageWidth = Integer.valueOf(msgModel.getWidth());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            imageWidth = context.getResources().getDimensionPixelOffset(R.dimen.width_660);
                        }

                        if (imageWidth < 660) {
                            viewHolder.ivOtherImage.getLayoutParams().width = imageWidth;
                        } else {
                            viewHolder.ivOtherImage.getLayoutParams().width = context.getResources().getDimensionPixelOffset(R.dimen.width_660);
                        }
                    } else {
                        viewHolder.ivOtherImage.getLayoutParams().width = context.getResources().getDimensionPixelOffset(R.dimen.width_660);
                    }

                    if (!msgModel.getFileUrl().equals("")) {
                        //Glide.with(context).load(msgModel.getFileUrl()).into(viewHolder.ivOtherImage);

                        String width = msgModel.getWidth();
                        String height = msgModel.getHeight();

                        if(!width.isEmpty() && !height.isEmpty()){
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.override((int)(Integer.parseInt(width) * COMPRESS_RATIO), (int)(Integer.parseInt(height) * COMPRESS_RATIO));
                            requestOptions.placeholder(android.R.color.transparent);

                            Glide
                                    .with(context)
                                    .load(msgModel.getFileUrl())
                                    .apply(requestOptions)
                                    .into(viewHolder.ivOtherImage);
                        }else{
                            Glide.with(context).load(msgModel.getFileUrl()).into(viewHolder.ivOtherImage);
                        }
                    }
                    ///

                    viewHolder.layoutOtherImage.setTag(item);
//                Glide.with(context).load(msgModel.getFileUrl()).into(viewHolder.ivMeImage);
                    viewHolder.layoutOtherImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MessageJSonModel item = new MessageJSonModel();
                            try {
                                MessageData msd = (MessageData) v.getTag();
                                item = MessageJSonModel.parse(new JSONObject(msd.getMessage()));
                            } catch (JSONException e) {
                                item = null;
                                e.printStackTrace();
                            }

                            if (item == null) return;

                            String contactName = MineTalkApp.getUserNameByPhoneNum(item.getContactName(), item.getContactHp());
                            PhotoViewerActivity.startActivity(context, item.getFileUrl(), "", contactName, true);

                        }
                    });
                }

            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_VIDEO)) {
                viewHolder.layoutOtherVideo.setVisibility(View.VISIBLE);
                viewHolder.layoutOtherImage.setVisibility(View.GONE);
                viewHolder.layoutOtherText.setVisibility(View.GONE);
                viewHolder.layoutOtherContact.setVisibility(View.GONE);
                viewHolder.layoutInviteMessage.setVisibility(View.GONE);
                viewHolder.layoutOtherFile.setVisibility(View.GONE);

                if (msgModel.getDeleteYn().equals("Y") ||
                        (item.getDeleteFlag() != null && item.getDeleteFlag().equals("Y")) ) {
                    isDeleteMsg = true;

                    //메세지 삭제 이미지
                    Drawable deleteImage = MineTalkApp.getAppContext().getResources().getDrawable( MineTalk.isBlackTheme ? R.drawable.icon_msg_warning_w : R.drawable.icon_msg_warning );
                    int h = deleteImage.getIntrinsicHeight();
                    int w = deleteImage.getIntrinsicWidth();
                    deleteImage.setBounds( 0, 0, w, h );

                    viewHolder.tvButtonTrans.setEnabled(false);
                    viewHolder.tvButtonTrans.setVisibility(View.GONE);
                    viewHolder.layoutOtherText.setVisibility(View.VISIBLE);
                    viewHolder.layoutOtherTextBox.setVisibility(View.VISIBLE);
                    viewHolder.tvOtherMessage.setText(context.getResources().getString(R.string.chat_message_deleted));
                    viewHolder.tvOtherMessage.setTextColor(Color.parseColor("#80858992"));
                    viewHolder.tvOtherMessage.setCompoundDrawablePadding(10);
                    viewHolder.tvOtherMessage.setCompoundDrawables( deleteImage, null, null, null );

                    viewHolder.layoutTranslationAear.setVisibility(View.GONE);
                    viewHolder.ivOriginMsgVoice.setVisibility(View.GONE);
                    viewHolder.layoutOtherVideo.setVisibility(View.GONE);

                } else {
                    viewHolder.layoutOtherVideo.setVisibility(View.VISIBLE);
                    viewHolder.layoutOtherVideo.setTag(item);
                    viewHolder.layoutOtherVideo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MessageJSonModel item = new MessageJSonModel();
                            try {
                                MessageData msd = (MessageData) v.getTag();
                                item = MessageJSonModel.parse(new JSONObject(msd.getMessage()));
                            } catch (JSONException e) {
                                item = null;
                                e.printStackTrace();
                            }

                            if (item == null) return;

                            Uri uri = Uri.parse(item.getFileUrl());
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                            intent.setDataAndType(uri, "video/*");
                            context.startActivity(intent);
                        }
                    });
                }

            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_CONTACT)) {
                viewHolder.layoutOtherImage.setVisibility(View.GONE);
                viewHolder.layoutOtherText.setVisibility(View.GONE);
                viewHolder.layoutOtherContact.setVisibility(View.VISIBLE);
                viewHolder.layoutInviteMessage.setVisibility(View.GONE);
                viewHolder.layoutOtherVideo.setVisibility(View.GONE);
                viewHolder.layoutOtherFile.setVisibility(View.GONE);

                viewHolder.tvOtherContactName.setText(msgModel.getContactName());

                viewHolder.layoutOtherContact.setTag(item);
                viewHolder.layoutOtherContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MessageJSonModel item = new MessageJSonModel();
                        try {
                            MessageData msd = (MessageData) v.getTag();
                            item = MessageJSonModel.parse(new JSONObject(msd.getMessage()));
                        } catch (JSONException e) {
                            item = null;
                            e.printStackTrace();
                        }

                        if (item == null) return;

                        Intent intent = new Intent(Intent.ACTION_INSERT, ContactsContract.Contacts.CONTENT_URI);

                        Bundle bundle = new Bundle();
                        bundle.putString(ContactsContract.Intents.Insert.PHONE, item.getContactHp());
                        bundle.putString(ContactsContract.Intents.Insert.NAME, item.getContactName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                    }
                });

            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_INVITE)) {
                viewHolder.layoutOther.setVisibility(View.GONE);
                viewHolder.layoutOtherImage.setVisibility(View.GONE);
                viewHolder.layoutOtherText.setVisibility(View.GONE);
                viewHolder.layoutOtherContact.setVisibility(View.GONE);
                viewHolder.layoutOtherVideo.setVisibility(View.GONE);
                viewHolder.layoutInviteMessage.setVisibility(View.VISIBLE);
                viewHolder.layoutOtherFile.setVisibility(View.GONE);

                String user = "";
                for (int i = 0; i < msgModel.getInviteUserInfo().size(); i++) {
                    if (i != (msgModel.getInviteUserInfo().size() - 1)) {
                        user = user + MineTalkApp.getUserNameByPhoneNum(msgModel.getInviteUserInfo().get(i).getUser_name(), msgModel.getInviteUserInfo().get(i).getUser_hp()) + ",";
                    } else {
                        user = user + MineTalkApp.getUserNameByPhoneNum(msgModel.getInviteUserInfo().get(i).getUser_name(), msgModel.getInviteUserInfo().get(i).getUser_hp());
                    }
                }
                viewHolder.tvInviteMessage.setText(user + context.getString(R.string.invite_user_msg_1));
            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_USER_EXIT)) {
                viewHolder.layoutOther.setVisibility(View.GONE);
                viewHolder.layoutOtherImage.setVisibility(View.GONE);
                viewHolder.layoutOtherText.setVisibility(View.GONE);
                viewHolder.layoutOtherContact.setVisibility(View.GONE);
                viewHolder.layoutOtherVideo.setVisibility(View.GONE);
                viewHolder.layoutInviteMessage.setVisibility(View.VISIBLE);
                viewHolder.layoutOtherFile.setVisibility(View.GONE);

                String user = "";
                for (int i = 0; i < msgModel.getInviteUserInfo().size(); i++) {
                    if (i != (msgModel.getInviteUserInfo().size() - 1)) {
                        user = user + MineTalkApp.getUserNameByPhoneNum(msgModel.getInviteUserInfo().get(i).getUser_name(), msgModel.getInviteUserInfo().get(i).getUser_hp()) + ",";
                    } else {
                        user = user + MineTalkApp.getUserNameByPhoneNum(msgModel.getInviteUserInfo().get(i).getUser_name(), msgModel.getInviteUserInfo().get(i).getUser_hp());
                    }
                }
                viewHolder.tvInviteMessage.setText(user + context.getString(R.string.exit_user_msg_1));
            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_FILE)) {
                viewHolder.layoutOtherImage.setVisibility(View.GONE);
                viewHolder.layoutOtherText.setVisibility(View.GONE);
                viewHolder.layoutOtherContact.setVisibility(View.GONE);
                viewHolder.layoutInviteMessage.setVisibility(View.GONE);
                viewHolder.layoutOtherVideo.setVisibility(View.GONE);
                viewHolder.layoutOtherFile.setVisibility(View.VISIBLE);

                //String name = chooseFileName(msgModel.getFileUrl());
                String name = msgModel.getExtra();
                final String url = msgModel.getFileUrl();
                viewHolder.tvOtherFileName.setText(name);

                viewHolder.tvBtnOtherFileSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(mContext, MineTalkApp.getCurrentActivity().getResources().getString(R.string.file_download_start), Toast.LENGTH_SHORT).show();
                        DownloadFiles(url, name);
                        //File file = new File(new URL(msgModel.getFileUrl()).toURI());
                    }
                });
            }


//            viewHolder.layoutOtherText.setVisibility(View.VISIBLE);
//            viewHolder.layoutOtherImage.setVisibility(View.GONE);
//            viewHolder.layoutOtherContact.setVisibility(View.GONE);
//            viewHolder.layoutOtherVideo.setVisibility(View.GONE);


            viewHolder.layoutOtherText.setTag(item);
            viewHolder.layoutOtherImage.setTag(item);
            viewHolder.layoutOtherContact.setTag(item);
            viewHolder.layoutOtherVideo.setTag(item);

            viewHolder.layoutOtherText.setOnLongClickListener(msgOtherLongPressListener);
            viewHolder.layoutOtherImage.setOnLongClickListener(msgOtherLongPressListener);
            viewHolder.layoutOtherContact.setOnLongClickListener(msgOtherLongPressListener);
            viewHolder.layoutOtherVideo.setOnLongClickListener(msgOtherLongPressListener);
        }

        return convertView;
    }

    public void DownloadFiles(String url, String destName){

        if (mDownloadManager == null) {
            mDownloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        }

        Uri uri = Uri.parse(url);        //data는 파일을 떨궈 주는 uri
        DownloadManager.Request request = new DownloadManager.Request(uri);
        //request.setTitle("파일다운로드");    //다운로드 완료시 noti에 제목

        request.setVisibleInDownloadsUi(true);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        //모바일 네트워크와 와이파이일때 가능하도록
        request.setNotificationVisibility( DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        //다운로드 완료시 noti에 보여주는것

        request.setDestinationInExternalPublicDir(Environment.getExternalStorageDirectory() + "/MineChat/", destName);
        //다운로드 경로, 파일명을 적어준다

        mDownloadReference = mDownloadManager.enqueue(request);

        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED);

        receiverNotificationClicked = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String extraId = DownloadManager.EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS;
                long[] references = intent.getLongArrayExtra(extraId);
                for (long reference : references) {

                }
            }
        };

        mContext.registerReceiver(receiverNotificationClicked, filter);
        IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);

        receiverDownloadComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

                if(mDownloadReference == reference){

                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(reference);
                    Cursor cursor = mDownloadManager.query(query);

                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                    int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);

                    int status = cursor.getInt(columnIndex);
                    int reason = cursor.getInt(columnReason);

                    cursor.close();

                    switch (status){

                        case DownloadManager.STATUS_SUCCESSFUL :

                            Toast.makeText(mContext, MineTalkApp.getCurrentActivity().getResources().getString(R.string.file_download_sucess), Toast.LENGTH_SHORT).show();
                            break;

                        case DownloadManager.STATUS_PAUSED :

                            //Toast.makeText(mContext, "다운로드 중지 : " + reason, Toast.LENGTH_SHORT).show();
                            break;

                        case DownloadManager.STATUS_FAILED :

                            //Toast.makeText(mContext, "다운로드 취소 : " + reason, Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        };

        mContext.registerReceiver(receiverDownloadComplete, intentFilter);
    }

    private void updateTheme(ViewHolder viewHolder) {
        Context context = MineTalkApp.getAppContext();
        if (MineTalk.isBlackTheme) {
            viewHolder.tvMeMessage.setTextColor(context.getResources().getColor(R.color.bk_point_color));
            viewHolder.tvOtherUserName.setTextColor(context.getResources().getColor(R.color.white_color));
            viewHolder.ivOriginMsgVoice.setImageResource(R.drawable.icon_sound_tran_bk);
            viewHolder.ivOtherMsgTransVoice.setImageResource(R.drawable.icon_sound_tran_bk);
            viewHolder.tvTransTitle.setTextColor(context.getResources().getColor(R.color.bk_point_color));

            viewHolder.layoutMeContact.setBackgroundResource(mIsSecurity ? R.drawable.my_text_security_msg_box_bk : R.drawable.my_text_msg_box_bk);
            viewHolder.layoutMeTextAear.setBackgroundResource(mIsSecurity ? R.drawable.my_text_security_msg_box_bk : R.drawable.my_text_msg_box_bk);
            viewHolder.layoutMeFile.setBackgroundResource(mIsSecurity ? R.drawable.my_text_security_msg_box_bk : R.drawable.my_text_msg_box_bk);

            viewHolder.tvOtherMessage.setTextColor(context.getResources().getColor(R.color.white_color));
            viewHolder.tvOtherMessage.setBackgroundResource(R.drawable.other_text_msg_box_bk);

            viewHolder.tvOtherFileName.setTextColor(context.getResources().getColor(R.color.white_color));
            viewHolder.layoutOtherFile.setBackgroundResource(R.drawable.other_text_msg_box_bk);

            viewHolder.tvOtherTransMessage.setTextColor(context.getResources().getColor(R.color.white_color));
            viewHolder.tvOtherTransMessage.setBackgroundResource(R.drawable.other_text_msg_box_bk);

            viewHolder.tvButtonTrans.setTextColor(context.getResources().getColor(R.color.white_color));
            viewHolder.tvButtonTrans.setBackgroundResource(R.drawable.other_text_msg_box_bk);

            viewHolder.tvInviteMessage.setTextColor(context.getResources().getColor(R.color.bk_point_color));

            viewHolder.tvMeMsgReadCount.setTextColor(mIsSecurity ? context.getResources().getColor(R.color.security_chat_color) : context.getResources().getColor(R.color.bk_point_color));
            viewHolder.tvOtherMsgReadCount.setTextColor(mIsSecurity ? context.getResources().getColor(R.color.security_chat_color) : context.getResources().getColor(R.color.bk_point_color));

            //viewHolder.tvOtherMessage.setLinkTextColor(context.getResources().getColor(R.color.bk_point_color));
            viewHolder.tvMeMessage.setLinkTextColor(context.getResources().getColor(R.color.bk_point_color));
            viewHolder.tvMeFileName.setTextColor(context.getResources().getColor(R.color.bk_point_color));
        } else {
            viewHolder.tvMeMessage.setTextColor(context.getResources().getColor(R.color.white_color));
            viewHolder.tvOtherUserName.setTextColor(context.getResources().getColor(R.color.main_text_color));
            viewHolder.ivOriginMsgVoice.setImageResource(R.drawable.icon_sound_tran);
            viewHolder.ivOtherMsgTransVoice.setImageResource(R.drawable.icon_sound_tran);
            viewHolder.tvTransTitle.setTextColor(context.getResources().getColor(R.color.app_point_color));

            viewHolder.layoutMeContact.setBackgroundResource(mIsSecurity ? R.drawable.my_text_security_msg_box : R.drawable.my_text_msg_box);
            viewHolder.layoutMeTextAear.setBackgroundResource(mIsSecurity ? R.drawable.my_text_security_msg_box : R.drawable.my_text_msg_box);
            viewHolder.layoutMeFile.setBackgroundResource(mIsSecurity ? R.drawable.my_text_security_msg_box : R.drawable.my_text_msg_box);

            viewHolder.tvOtherMessage.setTextColor(context.getResources().getColor(R.color.main_text_color));
            viewHolder.tvOtherFileName.setTextColor(context.getResources().getColor(R.color.main_text_color));
            viewHolder.tvOtherFileName.setBackgroundResource(R.drawable.other_text_msg_box);

            viewHolder.tvOtherMessage.setBackgroundResource(R.drawable.other_text_msg_box);
            viewHolder.tvOtherTransMessage.setTextColor(context.getResources().getColor(R.color.main_text_color));
            viewHolder.tvOtherTransMessage.setBackgroundResource(R.drawable.other_text_msg_box);

            viewHolder.tvButtonTrans.setTextColor(context.getResources().getColor(R.color.main_text_color));
            viewHolder.tvButtonTrans.setBackgroundResource(R.drawable.other_text_msg_box);

            viewHolder.tvInviteMessage.setTextColor(context.getResources().getColor(R.color.main_text_color));

            viewHolder.tvMeMsgReadCount.setTextColor(mIsSecurity ? context.getResources().getColor(R.color.security_chat_color) : context.getResources().getColor(R.color.app_point_color));
            viewHolder.tvOtherMsgReadCount.setTextColor(mIsSecurity ? context.getResources().getColor(R.color.security_chat_color) : context.getResources().getColor(R.color.app_point_color));

            //viewHolder.tvOtherMessage.setLinkTextColor(context.getResources().getColor(R.color.bk_point_color));
            viewHolder.tvMeMessage.setLinkTextColor(context.getResources().getColor(R.color.white_color));
        }
    }

    private View.OnLongClickListener msgMeLongPressListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            MessageData data = (MessageData) v.getTag();
            if (mListener != null) {

//                int readCount = 0;
//                if(mThreadController != null ){
//                    readCount = mThreadController.unReadCountByMessageSeq(data.getSeq());
//                }
//
//                mListener.onLongPressMsgItem(MSG_TYPE_ME, data, readCount == mThreadController.getMemberCount()-1);



                long diffInMillies = System.currentTimeMillis() - data.getSendDate().getTime();
                int diffMin = (int)(diffInMillies / (60*1000));
                Log.d("Min Diff : ", Integer.toString(diffMin));
                mListener.onLongPressMsgItem(MSG_TYPE_ME, data, diffMin < 5 && diffMin >= 0);
            }
            return true;
        }
    };

    private View.OnLongClickListener msgOtherLongPressListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            MessageData data = (MessageData) v.getTag();
            if (mListener != null) {
                mListener.onLongPressMsgItem(MSG_TYPE_OTHER, data, false);
            }
            return true;
        }
    };


    class ViewHolder {
        LinearLayout layoutOther;
        RelativeLayout layoutUserPrfoile;
        CornerRoundImageView ivOtherUserImg;
        TextView tvOtherUserName;
        TextView tvOtherMessage;
        TextView tvOtherMsgTime;
        LinearLayout layoutTranslationAear;
        TextView tvOtherTransMessage;
        TextView tvButtonTrans;
        CornerRoundImageView ivOtherImage;

        LinearLayout layoutMeTextAear;
        LinearLayout layoutTransVoiceAear;
        LinearLayout layoutOriginVoiceAear;
        LinearLayout layoutEmotionAear;
        LinearLayout layoutOtherEmotionAear;

        LinearLayout layoutOtherText;
        ImageView ivOtherEmoticon;
        LinearLayout layoutOtherTextBox;
        LinearLayout layoutOtherImage;
        RelativeLayout layoutOtherVideo;
        LinearLayout layoutOtherContact;
        TextView tvMeContactName;

        ImageView ivOtherMsgVoice;
        ImageView ivOtherMsgTransVoice;
        ImageView ivOriginMsgVoice;

        TextView tvTransTitle;

        LinearLayout layoutMe;
        TextView tvMeMessage;
        TextView tvMeMsgTime;

        ImageView ivMeMsgVoice;

        LinearLayout layoutMeText;
        ImageView ivMeEmoticon;
        LinearLayout layoutMeTextBox;
        RelativeLayout layoutMeVideo;
        RelativeLayout layoutMeImage;
        CornerRoundImageView ivMeImage;

        LinearLayout layoutMeContact;
        TextView tvOtherContactName;

        RelativeLayout layoutInviteMessage;
        TextView tvInviteMessage;


        LinearLayout layoutMeFile;
        TextView tvMeFileName;

        LinearLayout layoutOtherFile;
        TextView tvOtherFileName;
        TextView tvBtnOtherFileSave;

        ImageView ivMeMsgReadCount;
        TextView tvMeMsgReadCount;
        TextView tvOtherMsgReadCount;

        TextView tvMeEmotionTime;
        ImageView ivMeEmotionReadCount;
        TextView tvMeEmotionReadCount;

        TextView tvOtherEmotionTime;
        ImageView ivOtherEmotionReadCount;
        TextView tvOtherEmotionReadCount;

        TextView tvMsgDate;
    }

    private String chooseFileName(String path) {
        try {
            String fileName = path.substring(path.lastIndexOf('/') + 1);

            return fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
