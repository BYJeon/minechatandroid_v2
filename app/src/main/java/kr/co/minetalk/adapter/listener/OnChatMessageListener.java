package kr.co.minetalk.adapter.listener;

import kr.co.minetalk.message.model.MessageData;

public interface OnChatMessageListener {
    public void onTranslation(MessageData messageData);
    public void onProfile(String userXid);
    public void onLongPressMsgItem(int type, MessageData messageData, boolean isAllDelete);
}
