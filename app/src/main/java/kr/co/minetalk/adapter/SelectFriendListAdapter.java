package kr.co.minetalk.adapter;

import android.content.res.ColorStateList;
import android.databinding.BindingAdapter;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.adapter.listener.OnFriendBlockAdapterListener;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.LayoutChatListItemBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.databinding.LayoutSelectFriendListItemBinding;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class SelectFriendListAdapter extends RecyclerView.Adapter<SelectFriendListAdapter.SelectViewHolder> implements View.OnClickListener {
    private OnFriendListViewOrderListener mListener = null;
    private List<FriendListModel> items = new ArrayList<>();
    private List<FriendListModel> itemsOriginal = new ArrayList<>();
    private Boolean isCheckView = true;
    private String  mFilter = "";

    @NonNull
    @Override
    public SelectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 0){
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new SelectViewHolder(binding);
        }

        LayoutSelectFriendListItemBinding binding = LayoutSelectFriendListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new SelectFriendListAdapter.SelectViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectViewHolder holder, int position) {
        if(position == 0){
            //FriendListModel item = items.get(position);
            holder.bind(null);
        }else{
            FriendListModel item = items.get(position-1);
            holder.bind(item);
        }

    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size()+1;
    }

    @BindingAdapter("bind:selectList")
    public static void bindItem(RecyclerView recyclerView, List<FriendListModel> items) {
        SelectFriendListAdapter adapter = (SelectFriendListAdapter) recyclerView.getAdapter();

        if (adapter != null) {
            adapter.setData(items);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setData(List<FriendListModel> items) {
        if (items != null) {
            this.items.clear();
            this.items.addAll(items);
            this.itemsOriginal.clear();
            this.itemsOriginal.addAll(items);
            notifyDataSetChanged();
        }
    }

    public void setData(List<FriendListModel> items, boolean isCheckView) {
        this.isCheckView = isCheckView;
        if (items != null) {
            this.items.clear();
            this.items.addAll(items);
            this.itemsOriginal.clear();
            this.itemsOriginal.addAll(items);
            notifyDataSetChanged();
        }
    }

    public ArrayList<FriendListModel> getSelectData() {
        ArrayList<FriendListModel> selectList = new ArrayList<>();
        for(int i = 0 ; i < itemsOriginal.size(); i++) {
            if(itemsOriginal.get(i).isIs_select()) {
                selectList.add(itemsOriginal.get(i));
            }
        }
        return selectList;
    }

    @Override
    public void onClick(View v) {
//        if (v.getId() == R.id.tv_button_block_lift) {
//            String user_xid = v.getTag().toString();
//            if (mListener != null) {
//                mListener.onBlockLift(user_xid);
//            }
//        }
    }

    public void setAdapterListener(OnFriendListViewOrderListener listener) {
        this.mListener = listener;
    }

    public void filter(String str) {
        this.mFilter = str;
        items.clear();
        if(!mFilter.equals("")) {
            for(int i = 0 ; i < itemsOriginal.size() ; i++ ) {
                FriendListModel item = itemsOriginal.get(i);
                if(item.getUser_name().toLowerCase().contains(mFilter.toLowerCase())) {
                    items.add(item);
                }
            }
        } else {
            items.addAll(itemsOriginal);
        }
        notifyDataSetChanged();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class SelectViewHolder extends RecyclerView.ViewHolder {
        LayoutSelectFriendListItemBinding mBinding;
        LayoutListHeaderViewBinding mHeaderBinding;

        public SelectViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            this.mHeaderBinding = binding;
        }

        public SelectViewHolder(LayoutSelectFriendListItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
            this.setIsRecyclable(false);
        }

        void bind(FriendListModel item) {

            if(mBinding == null){ //Header 영역
                mHeaderBinding.btSortList.setText(Preferences.getFriendSelectOrder() ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));

                mHeaderBinding.ivListType.setVisibility(View.VISIBLE);
                boolean listType = Preferences.getSelectFriendListType();

                if(MineTalk.isBlackTheme) { //GridType
                    mHeaderBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
                } else { //ListType
                    mHeaderBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
                }

                mHeaderBinding.ivListType.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Preferences.setSelectFriendListType(!Preferences.getSelectFriendListType());
                        if(mListener != null)
                            mListener.refreshListType(Preferences.getSelectFriendListType());
                    }
                });

                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Preferences.setFriendSelectOrder(!Preferences.getFriendSelectOrder());
                        mHeaderBinding.btSortList.setText(Preferences.getFriendSelectOrder() ?
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                                MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                        if(mListener != null)
                            mListener.refreshOrder(Preferences.getFriendSelectOrder());
                    }
                });

                String countMessage = String.format("%s %d%s", MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_title),
                        items == null ? 0 : items.size(), MineTalkApp.getCurrentActivity().getResources().getString(R.string.human_count));

                mHeaderBinding.tvCount.setText(countMessage);
            }else{
                updateTheme();

                //연락처에서 이름을 가져 온다.
                String contactName = MineTalkApp.getUserNameByPhoneNum(item.getUser_name(), item.getUser_hp());

                if(contactName != null && !contactName.equals(""))
                    mBinding.tvUserName.setText(contactName);
                else
                    mBinding.tvUserName.setText(item.getUser_name());

                if(item.getUser_state_message().equals("")) {
                    mBinding.tvStatus.setVisibility(View.GONE);
                    mBinding.tvStatus.setText(item.getUser_state_message());
                } else {
                    mBinding.tvStatus.setVisibility(View.VISIBLE);
                    mBinding.tvStatus.setText(item.getUser_state_message());
                }

                if (item != null) {
                    String profileImgUrl = item.getUser_profile_image();
                    if (profileImgUrl == null || profileImgUrl.equals("")) {
                        Glide.with(mBinding.getRoot()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                    } else {
                        Glide.with(mBinding.getRoot()).load(item.getUser_profile_image()).into(mBinding.ivUserImage);
                    }
                }

                mBinding.cbSelect.setVisibility(isCheckView ? View.VISIBLE : View.GONE);
                mBinding.cbSelect.setSelected(item.isIs_select());
                mBinding.cbSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBinding.cbSelect.setSelected(!mBinding.cbSelect.isSelected());
                        item.setIs_select(mBinding.cbSelect.isSelected());
                    }
                });
            }
        }

        private void updateTheme(){
            mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

            if(MineTalk.isBlackTheme){
                mBinding.cbSelect.setBackgroundResource(R.drawable.selector_check_box_bk);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//                    mBinding.cbSelect.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(MineTalkApp.getAppContext(), R.color.bk_point_color)));
            }else{
                mBinding.cbSelect.setBackgroundResource(R.drawable.selector_check_box_w);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//                    mBinding.cbSelect.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(MineTalkApp.getAppContext(), R.color.app_point_color)));
            }
        }
    }
}
