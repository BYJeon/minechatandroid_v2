package kr.co.minetalk.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.minetalk.fragment.CashFragment;
import kr.co.minetalk.fragment.TokenFragment;
import kr.co.minetalk.fragment.listener.OnMoreFragmentEventListener;

public class MoreFragmentPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private Fragment mCurrentFragment;

    public MoreFragmentPagerAdapter(FragmentManager fragmentManager, OnMoreFragmentEventListener listener) {
        super(fragmentManager);
        mFragments.clear();

        CashFragment cash = CashFragment.newInstance();
        cash.setOnFragmentListener(listener);

        TokenFragment token = TokenFragment.newInstance();
        token.setOnFragmentListener(listener);

        mFragments.add(token);
        mFragments.add(cash);

        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = (Fragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    @Override
    public float getPageWidth(int position) {
        return (0.9f);
    }
}
