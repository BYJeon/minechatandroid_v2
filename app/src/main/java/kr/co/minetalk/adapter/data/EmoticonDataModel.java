package kr.co.minetalk.adapter.data;

import kr.co.minetalk.api.model.BaseModel;

public class EmoticonDataModel extends BaseModel {

    private String emoticonName;
    private int    emoticonRes;

    public String getEmoticonName() {
        return emoticonName;
    }

    public void setEmoticonName(String emoticonName) {
        this.emoticonName = emoticonName;
    }

    public int getEmoticonRes() {
        return emoticonRes;
    }

    public void setEmoticonRes(int emoticonRes) {
        this.emoticonRes = emoticonRes;
    }
}
