package kr.co.minetalk.adapter;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.adapter.listener.OnFriendBlockAdapterListener;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.databinding.LayoutChatGroupGridItemBinding;
import kr.co.minetalk.databinding.LayoutListHeaderViewBinding;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class ChatGroupGridAdapter extends RecyclerView.Adapter<ChatGroupGridAdapter.ChatViewHolder> implements View.OnClickListener {
    private OnFriendListViewOrderListener mListener = null;
    private List<ThreadData> items;
    private SimpleDateFormat mDtFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat mTimeFormat = new SimpleDateFormat("HH:mm");
    private HashMap<String, String> mContactMap = null;

    private ChatListAdapter.ChatHolderType mHolderType = ChatListAdapter.ChatHolderType.NORMAL;
    private List<ImageView> mProfileImgList = new ArrayList<>();

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == 0){
            LayoutListHeaderViewBinding binding = LayoutListHeaderViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ChatGroupGridAdapter.ChatViewHolder(binding);
        }

        LayoutChatGroupGridItemBinding binding = LayoutChatGroupGridItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ChatGroupGridAdapter.ChatViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        if(position == 0){
            //FriendListModel item = items.get(position);
            holder.bind(null);
        }else{
            ThreadData item = items.get(position-1);
            holder.bind(item);
        }

    }

    @Override
    public int getItemCount() {
        return items == null ? 1 : items.size()+1;
    }

    @BindingAdapter("bind:threadGrid")
    public static void bindItem(RecyclerView recyclerView, List<ThreadData> items) {
        ChatGroupGridAdapter adapter = (ChatGroupGridAdapter) recyclerView.getAdapter();

        if (adapter != null) {
            adapter.setData(items);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public void setContactMap(HashMap<String, String> contactMap){
        this.mContactMap = contactMap;
    }

    public void setData(List<ThreadData> items) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    public void setData(List<ThreadData> items, ChatListAdapter.ChatHolderType type) {
        if (items != null) {
            this.items = items;
            notifyDataSetChanged();
        }

        this.mHolderType = type;
    }

    @Override
    public void onClick(View v) {
//        if (v.getId() == R.id.tv_button_block_lift) {
//            String user_xid = v.getTag().toString();
//            if (mListener != null) {
//                mListener.onBlockLift(user_xid);
//            }
//        }
    }

    public void setAdapterListener(OnFriendListViewOrderListener listener) {
        this.mListener = listener;
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ChatViewHolder extends RecyclerView.ViewHolder {
        LayoutChatGroupGridItemBinding mBinding;
        LayoutListHeaderViewBinding mHeaderBinding;

        public ChatViewHolder(LayoutListHeaderViewBinding binding) {
            super(binding.getRoot());
            this.mHeaderBinding = binding;
        }


        public ChatViewHolder(LayoutChatGroupGridItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bind(ThreadData item) {
            if(mBinding == null) { //Header 영역
                mHeaderBinding.btSortList.setText(Preferences.getChatSettingOrder() ? "가↑" : "가↓");

                mHeaderBinding.btSortList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Preferences.setChatSettingOrder(!Preferences.getChatSettingOrder() );
                        mHeaderBinding.btSortList.setText(Preferences.getChatSettingOrder() ? "가↑" : "가↓");
                        if(mListener != null)
                            mListener.refreshOrder(Preferences.getChatSettingOrder());
                    }
                });

                String countMessage = String.format("단체 채팅 %d건", items == null ? 0 : items.size());
                mHeaderBinding.tvCount.setText(countMessage);

            }else{
                updateTheme();
                mBinding.layoutAdItemRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String threadName = "";
                        if(item.getIsDirect().toUpperCase().equals("Y")) {
                            if(item.getMembers().size() > 0) {
                                String members = "";
                                for(ThreadMember tmember : item.getMembers()) {
                                    if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                        //String contactName = mContactMap.get(tmember.getXid());
                                        //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());
                                        String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                        if(contactName == null || contactName.equals("")) {
                                            members = tmember.getNickName();
                                        } else {
                                            members = contactName;
                                        }
                                        break;
                                    }
                                }
                                threadName = members;
                            }
                        } else {
                            if(item.getMembers().size() > 0) {
                                String members = "";

                                int count = 0;
                                for(ThreadMember tmember : item.getMembers()) {
                                    //String contactName = mContactMap.get(tmember.getXid());
                                    //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());
                                    String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                    if(contactName == null || contactName.equals("")) {
                                        members = members + tmember.getNickName();
                                    } else {
                                        members = members + contactName;
                                    }

                                    if(count != (item.getMembers().size() - 1)) {
                                        members = members + ",";
                                    }
                                    count++;
                                }
                                threadName = members;
                            }
                        }

                        ChatDetailActivity.startActivity(MineTalkApp.getCurrentActivity(), item.getThreadKey(), threadName);
                    }
                });

                if(mHolderType == ChatListAdapter.ChatHolderType.FRIEND_SEARCH){
                    String myChatThreadKey = Preferences.getMyChattingThreadKey();

                    //나와의 채팅
                    if(item.getThreadKey().equals(myChatThreadKey)){
                        mBinding.tvGroupTitle.setText("ME");
                        mBinding.tvGroupTitle.setBackgroundResource(R.drawable.bg_round_00a1e1);
                    }else if(item.getIsDirect().toUpperCase().equals("Y")){ // 1:1 채팅
                        mBinding.tvGroupTitle.setText("1:1");
                        mBinding.tvGroupTitle.setBackgroundResource(R.drawable.bg_chat_round_16c066);
                    }else{ // 단체 채팅
                        mBinding.tvGroupTitle.setText("단체");
                        mBinding.tvGroupTitle.setBackgroundResource(R.drawable.bg_round_f8790d);
                    }
                }

                mBinding.tvMemberCount.setText(Integer.toString(item.getMembers().size()));

                MessageJSonModel msgModel = new MessageJSonModel();
                try {
                    JSONObject msgObject = new JSONObject(item.getLastMessage());
                    msgModel = MessageJSonModel.parse(msgObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(msgModel.getMsgType().equals(MineTalk.MSG_TYPE_TEXT) ||
                        msgModel.getMsgType().equals(MineTalk.MSG_TYPE_URL)) {
                    if(msgModel.getText().equals("")) {
                        mBinding.tvLastMsg.setText(MineTalkApp.getCurrentActivity().getString(R.string.msg_type_emoticon));
                    } else {
                        mBinding.tvLastMsg.setText(StringEscapeUtils.unescapeJava(msgModel.getText()));
                    }
                } else if(msgModel.getMsgType().equals(MineTalk.MSG_TYPE_IMAGE)) {
                    mBinding.tvLastMsg.setText(MineTalkApp.getCurrentActivity().getString(R.string.msg_type_image));
                } else if(msgModel.getMsgType().equals(MineTalk.MSG_TYPE_VIDEO)) {
                    mBinding.tvLastMsg.setText(MineTalkApp.getCurrentActivity().getString(R.string.msg_type_video));
                } else if(msgModel.getMsgType().equals(MineTalk.MSG_TYPE_CONTACT)) {
                    mBinding.tvLastMsg.setText(MineTalkApp.getCurrentActivity().getString(R.string.msg_type_contact));
                }

                int unReadCount = getUnReadCount(item.getLastSeq(), item.getMembers(), MineTalkApp.getUserInfoModel().getUser_xid());
                if(unReadCount > 0) {
                    if(unReadCount > 99) {
                        mBinding.tvUnreadCount.setText("99+");
                    } else {
                        mBinding.tvUnreadCount.setText(String.valueOf(unReadCount));
                    }
                    mBinding.tvUnreadCount.setVisibility(View.VISIBLE);
                } else {
                    mBinding.tvUnreadCount.setVisibility(View.GONE);
                    mBinding.tvUnreadCount.setText("");
                }

                long now = System.currentTimeMillis();
                Date date = new Date(now);
                String curDateTime = mDtFormat.format(date);
                String updateTime = mDtFormat.format(item.getUpdateDate());
                long diffDay = MineTalkApp.calDateBetweenDiff(curDateTime, updateTime, mDtFormat);

                if(diffDay != -1 && diffDay == 0) {
                    mBinding.tvMsgDate.setText(mTimeFormat.format(item.getUpdateDate()));
                }
                else
                    mBinding.tvMsgDate.setText(updateTime);

                // 1:1 대화방
                if(item.getIsDirect().toUpperCase().equals("Y")) {
                    String myChatThreadKey = Preferences.getMyChattingThreadKey();

                    if(item.getThreadKey().equals(myChatThreadKey)) {
                        mBinding.tvUserName.setText(MineTalkApp.getUserInfoModel().getUser_name());

                        String profileImgUrl = MineTalkApp.getUserInfoModel().getUser_profile_image();

                        if (profileImgUrl == null || profileImgUrl.equals("")) {
                            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                        } else {
                            Glide.with(MineTalkApp.getCurrentActivity()).load(profileImgUrl).into(mBinding.ivUserImage);
                        }

                    } else {

                        String chatName = Preferences.getChattingName(item.getThreadKey());
                        if(!chatName.equals("")) {
                            mBinding.tvUserName.setText(chatName);

                            for(ThreadMember tmember : item.getMembers()) {
                                if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                    String userProfile = tmember.getProfileImageUrl();
                                    if(userProfile == null || userProfile.equals(userProfile.equals(""))) {
                                        Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                                    } else {
                                        Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mBinding.ivUserImage);
                                    }
                                    Log.e("@@@TAG","url : " + userProfile);
                                    break;
                                }
                            }
                        } else {
                            if(item.getMembers().size() > 0) {
                                String members = "";
                                for(ThreadMember tmember : item.getMembers()) {
                                    if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                        //String contactName = mContactMap.get(tmember.getXid());
                                        //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());

                                        String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());

                                        String userProfile = tmember.getProfileImageUrl();
                                        if(userProfile == null || userProfile.equals(userProfile.equals(""))) {
                                            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                                        } else {
                                            Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mBinding.ivUserImage);
                                        }
                                        Log.e("@@@TAG","url : " + userProfile);

                                        if(contactName == null || contactName.equals("")) {
                                            members = tmember.getNickName();
                                        } else {
                                            members = contactName;
                                        }
                                        break;
                                    }

                                }
                                mBinding.tvUserName.setText(members);
                            }
                        }
                    }
                } else {
                    mProfileImgList.clear();
                    int memberCnt = item.getJoinMemberCount();
                    if(memberCnt == 2){
                        mBinding.layoutOneProfile.setVisibility(View.VISIBLE);
                        mBinding.layoutTwoProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutThreeProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutFourProfile.setVisibility(View.INVISIBLE);

                        mProfileImgList.add(mBinding.ivUserImage);
                    }else if(memberCnt == 3){ //3명 채팅
                        mBinding.layoutOneProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutTwoProfile.setVisibility(View.VISIBLE);
                        mBinding.layoutThreeProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutFourProfile.setVisibility(View.INVISIBLE);

                        mProfileImgList.add(mBinding.ivUserImageTwo1);
                        mProfileImgList.add(mBinding.ivUserImageTwo2);

                    }else if(memberCnt == 4) { //4명 채팅
                        mBinding.layoutOneProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutTwoProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutThreeProfile.setVisibility(View.VISIBLE);
                        mBinding.layoutFourProfile.setVisibility(View.INVISIBLE);

                        mProfileImgList.add(mBinding.ivUserImageThree1);
                        mProfileImgList.add(mBinding.ivUserImageThree2);
                        mProfileImgList.add(mBinding.ivUserImageThree3);
                    }else if(memberCnt > 4) { //5명 이상 채팅
                        mBinding.layoutOneProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutTwoProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutThreeProfile.setVisibility(View.INVISIBLE);
                        mBinding.layoutFourProfile.setVisibility(View.VISIBLE);

                        mProfileImgList.add(mBinding.ivUserImageFour1);
                        mProfileImgList.add(mBinding.ivUserImageFour2);
                        mProfileImgList.add(mBinding.ivUserImageFour3);
                        mProfileImgList.add(mBinding.ivUserImageFour4);
                    }

                    // 단체방
                    String myChatThreadKey = Preferences.getMyChattingThreadKey();
                    if(item.getThreadKey().equals(myChatThreadKey)) {
                        mBinding.tvUserName.setText(MineTalkApp.getCurrentActivity().getResources().getString(R.string.popup_profile_button_chat_me));
                    } else {

                        String chatName = Preferences.getChattingName(item.getThreadKey());

                        if(!chatName.equals("")) {
                            mBinding.tvUserName.setText(chatName);

                            int nCnt = 0;
                            for(ThreadMember tmember : item.getMembers()) {
                                if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                    String userProfile = tmember.getProfileImageUrl();

                                    if(nCnt < 4) {
                                        if (userProfile == null || userProfile.equals(userProfile.equals(""))) {
                                            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mProfileImgList.get(nCnt));
                                        } else {
                                            Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mProfileImgList.get(nCnt));
                                        }
                                        Log.e("@@@TAG", "url : " + userProfile);
                                    }
                                    //break;
                                    nCnt++;
                                }
                            }

                        } else {
                            if(item.getMembers().size() > 0) {
                                String members = "";
                                int count = 0;
                                for(ThreadMember tmember : item.getMembers()) {
                                    String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                    //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());
                                    //String contactName = mContactMap.get(tmember.getXid());
                                    if(contactName == null || contactName.equals("")) {
                                        members = members + tmember.getNickName();
                                    } else {
                                        members = members + contactName;
                                    }

                                    if(!members.equals("") && count != (item.getMembers().size() - 1)) {
                                        members = members + ",";
                                    }
                                    count++;
                                }

                                mBinding.tvUserName.setText(members);

                                int nCnt = 0;
                                for(ThreadMember tmember : item.getMembers()) {
                                    if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                        String userProfile = tmember.getProfileImageUrl();
                                        if(nCnt < 4) {
                                            if (userProfile == null || userProfile.equals(userProfile.equals(""))) {
                                                Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mProfileImgList.get(nCnt));
                                            } else {
                                                Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mProfileImgList.get(nCnt));
                                            }
                                            Log.e("@@@TAG", "url : " + userProfile);
                                        }
                                        //break;
                                        nCnt++;
                                    }
                                }

                            }
                        }
                    }

                }

//            String chatName = Preferences.getChattingName(item.getThreadKey());
//            if(!chatName.equals("")) {
//                mBinding.tvUserName.setText(chatName);
//
//                for(ThreadMember tmember : item.getMembers()) {
//                    if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
//                        String userProfile = tmember.getProfileImageUrl();
//                        if(userProfile == null || userProfile.equals(userProfile.equals(""))) {
//                            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
//                        } else {
//                            Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mBinding.ivUserImage);
//                        }
//                        Log.e("@@@TAG","url : " + userProfile);
//                        break;
//                    }
//                }
//            } else {
//                if(item.getMembers().size() > 0) {
//                    String members = "";
//                    for(ThreadMember tmember : item.getMembers()) {
//                        if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
//                            String contactName = MineTalkApp.getContactUserName(tmember.getUserHp());
//                            //String contactName = CommonUtils.getContactNameByNum( MineTalkApp.getAppContext(), tmember.getUserHp());
//                            //String contactName = mContactMap == null ? "" : mContactMap.get(tmember.getXid());
//
//                            String userProfile = tmember.getProfileImageUrl();
//                            if(userProfile == null || userProfile.equals(userProfile.equals(""))) {
//                                Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
//                            } else {
//                                Glide.with(MineTalkApp.getCurrentActivity()).load(userProfile).into(mBinding.ivUserImage);
//                            }
//                            Log.e("@@@TAG","url : " + userProfile);
//
//                            if(contactName == null || contactName.equals("")) {
//                                members = tmember.getNickName();
//                            } else {
//                                members = contactName;
//                            }
//                            break;
//                        }
//
//                    }
//                    mBinding.tvUserName.setText(members);
//                }
//            }
            }
        }

        private void updateTheme(){
            mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
        }
    }

    private int getUnReadCount(int lastSeq, List<ThreadMember> members, String userXid) {
        int readSeq = 0;
        for(ThreadMember tmember : members) {
            if(tmember.getXid().equals(userXid)) {
                readSeq = tmember.getReadSeq();
            }
        }

        return lastSeq - readSeq;
    }

}
