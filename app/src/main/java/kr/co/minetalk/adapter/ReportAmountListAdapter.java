package kr.co.minetalk.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.PointAmountListModel;
import kr.co.minetalk.api.model.PointHistoryModel;
import kr.co.minetalk.utils.CommonUtils;

public class ReportAmountListAdapter extends BaseAdapter {


    private ArrayList<PointAmountListModel> mDataList = new ArrayList<>();
    private String mTotalAmount = "";
    private String mPointType = "";

    public void setData(ArrayList<PointAmountListModel> arrayList, String totalAmout, String point_type) {
        this.mDataList.clear();
        if(arrayList.size() > 0) {
            addData(arrayList.get(0));
            addData(arrayList.get(0));
        }
        this.mDataList.addAll(arrayList);

        this.mTotalAmount = totalAmout;
        this.mPointType = point_type;

        notifyDataSetChanged();
    }

    public void addData(PointAmountListModel data) {
        //this.mDataList.clear();
        this.mDataList.add(data);

        notifyDataSetChanged();
    }

    public void clear() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context = parent.getContext();
        ViewHolder viewHolder = new ViewHolder();


        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.report_list_item, parent, false);

            viewHolder.mRootLayout = convertView.findViewById(R.id.layout_root);
            viewHolder.mTvMsg1 = (TextView) convertView.findViewById(R.id.tv_msg_1);
            viewHolder.mTvMsg2 = (TextView) convertView.findViewById(R.id.tv_msg_2);
            viewHolder.mTvRegDate = (TextView) convertView.findViewById(R.id.tv_date);
            viewHolder.mTvRegDateTime = (TextView) convertView.findViewById(R.id.tv_date_time);
            viewHolder.mTvPoint = (TextView) convertView.findViewById(R.id.tv_point);

            viewHolder.mTvDateTitle = convertView.findViewById(R.id.tv_date_title);
            viewHolder.mTvHistoryTitle = convertView.findViewById(R.id.tv_history_title);
            viewHolder.mTvAmountTitle = convertView.findViewById(R.id.tv_amount_title);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PointAmountListModel item = mDataList.get(position);

        String dateInfo = item.getPoint_reg_date();
        int idx = dateInfo.indexOf(" ");
        viewHolder.mTvRegDate.setText(item.getPoint_reg_date().substring(0, idx));
        viewHolder.mTvRegDateTime.setText(item.getPoint_reg_date().substring(idx+1));

        viewHolder.mTvPoint.setText(CommonUtils.comma_won(item.getPoint_amount()));

        //첫 Row는 타이틀
        if(position == 0){
            viewHolder.mTvDateTitle.setVisibility(View.VISIBLE);
            viewHolder.mTvHistoryTitle.setVisibility(View.VISIBLE);
            viewHolder.mTvAmountTitle.setVisibility(View.VISIBLE);

            viewHolder.mTvHistoryTitle.setText(mPointType.equals(MineTalk.POINT_TYPE_DEPOSIT) ? "입금내역" : "출금내역");
            viewHolder.mTvRegDate.setVisibility(View.INVISIBLE);
            viewHolder.mTvRegDateTime.setVisibility(View.INVISIBLE);

            viewHolder.mTvMsg1.setVisibility(View.INVISIBLE);
            viewHolder.mTvMsg2.setVisibility(View.INVISIBLE);

            viewHolder.mTvPoint.setVisibility(View.INVISIBLE);
        }else if( position == 1){ //두번째 Row 는 합산
            viewHolder.mTvRegDate.setVisibility(View.INVISIBLE);
            viewHolder.mTvRegDateTime.setVisibility(View.INVISIBLE);

            viewHolder.mTvDateTitle.setVisibility(View.VISIBLE);
            viewHolder.mTvDateTitle.setText(item.getPoint_from_address());
            viewHolder.mTvDateTitle.setTypeface(null, Typeface.NORMAL);

            viewHolder.mTvDateTitle.setText(mPointType.equals(MineTalk.POINT_TYPE_DEPOSIT) ? "입금 합계" : "출금 합계");

            viewHolder.mTvPoint.setText(CommonUtils.comma_won(mTotalAmount));
        }else{
            viewHolder.mTvMsg1.setVisibility(View.INVISIBLE);
            viewHolder.mTvMsg2.setVisibility(View.INVISIBLE);

            viewHolder.mTvHistoryTitle.setVisibility(View.VISIBLE);
            viewHolder.mTvHistoryTitle.setText(mPointType.equals(MineTalk.POINT_TYPE_DEPOSIT) ? item.getPoint_from_address() : item.getPoint_to_address());
            viewHolder.mTvHistoryTitle.setTypeface(null, Typeface.NORMAL);
        }

        if(position%2 == 0){
            viewHolder.mRootLayout.setBackgroundColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.bk_theme_status : R.color.white_color));
        }else{
            viewHolder.mRootLayout.setBackgroundColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.bk_theme_bg : R.color.main_theme_bg));
        }

        viewHolder.mTvDateTitle.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
        viewHolder.mTvHistoryTitle.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
        viewHolder.mTvAmountTitle.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

        viewHolder.mTvRegDate.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
        viewHolder.mTvRegDateTime.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

        viewHolder.mTvMsg1.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
        viewHolder.mTvMsg2.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

        viewHolder.mTvPoint.setTextColor(MineTalkApp.getAppContext().getResources().getColor(MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

        return convertView;
    }

    class ViewHolder {
        TextView mTvMsg1;
        TextView mTvMsg2;
        TextView mTvRegDate;
        TextView mTvRegDateTime;
        TextView mTvPoint;

        TextView mTvDateTitle;
        TextView mTvHistoryTitle;
        TextView mTvAmountTitle;

        LinearLayout mRootLayout;
    }
}
