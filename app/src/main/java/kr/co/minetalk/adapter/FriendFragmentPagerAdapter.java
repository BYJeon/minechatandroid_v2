package kr.co.minetalk.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.co.minetalk.fragment.ContactFragment;
import kr.co.minetalk.fragment.FavoriteFragment;
import kr.co.minetalk.fragment.FriendFragment;
import kr.co.minetalk.fragment.ProfileFragment;
import kr.co.minetalk.fragment.ShopFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.listener.OnProfileListener;

public class FriendFragmentPagerAdapter extends FragmentPagerAdapter {
    public static final int FRAGMENT_PROFILE  = 0;
    public static final int FRAGMENT_FAVOTITE = 1;
    public static final int FRAGMENT_FRIEND   = 2;
    public static final int FRAGMENT_CONTACT  = 3;

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private Fragment mCurrentFragment;

    public FriendFragmentPagerAdapter(FragmentManager fragmentManager, OnFragmentEventListener listener, OnProfileListener profileListener) {
        super(fragmentManager);


        mFragments.clear();
        ProfileFragment profile = ProfileFragment.newInstance();
        profile.setOnProfileListener(profileListener);

        FavoriteFragment favorite = FavoriteFragment.newInstance();
        favorite.setOnFragmentListener(listener);

        FriendFragment friend = FriendFragment.newInstance();
        friend.setOnFragmentListener(listener);

        ContactFragment contact = ContactFragment.newInstance();
        contact.setOnFragmentListener(listener);


        mFragments.add(profile);
        mFragments.add(favorite);
        mFragments.add(friend);
        mFragments.add(contact);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = (Fragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }
}
