package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.FriendProfileActivity;
import kr.co.minetalk.adapter.FriendGridAdapter;
import kr.co.minetalk.adapter.FriendListAdapter;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.FragmentFriendBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.CustomScrollView;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.FriendListView;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class FriendFragment extends BaseFragment {
    private FriendListAdapter mListAdapter;
    private FriendGridAdapter mGridAdapter;

    public static FriendFragment newInstance() {
        FriendFragment fragment = new FriendFragment();
        return fragment;
    }

    private FragmentFriendBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;
    private boolean isSearchMode = false;
    private String  mSearchKeyword = "";
    private Parcelable recyclerViewState;

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    public void setSearchMode(boolean mode){
        this.isSearchMode = mode;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        //bDescOrder = Preferences.getFriendSettingOrder();
        //mBinding.btSortList.setText(bDescOrder ? "가↑" : "가↓");
        //refreshView(bListType);

        //mListAdapter.notifyDataSetChanged();
        //mGridAdapter.notifyDataSetChanged();

        updateTheme();
        refreshView(bListType);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);

        mListAdapter = new FriendListAdapter();
        mGridAdapter = new FriendGridAdapter();

        mBinding.recyclerView.setAdapter(mListAdapter);
        setUIEventListener();

        bDescOrder = Preferences.getFriendSettingOrder();

        if(!isSearchMode)
            refreshView(Preferences.getFriendListType());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void updateTheme(){
        if(MineTalk.isBlackTheme){
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top_bk);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom_bk);
        }else{
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom);
        }
    }

    private void setUIEventListener() {
        mBinding.btSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                Preferences.setFriendSettingOrder(bDescOrder);
                mBinding.btSortList.setText(bDescOrder ? getResources().getString(R.string.text_asc) : getResources().getString(R.string.text_desc));
                refreshView(bListType);
            }
        });


        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.viewGradient.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);

                mBinding.viewGradient.setVisibility(View.INVISIBLE);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bListType){ // Grid Type
                    mBinding.recyclerView.scrollToPosition(mGridAdapter.getItemCount()-1);
                }else{
                    mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                }

                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void refreshView() {
        try {
            setupFriendListData(this.bListType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //친구의 상태 메세지 변경
    private void updateFriendListData(boolean bGridType) {

        ArrayList<FriendListModel> friends = MineTalkApp.getFriendList();
        ArrayList<FriendListModel> tmp = new ArrayList<>();
        tmp.addAll(friends);
        FriendListModel model;
        String contactName = "";

        for( int i = 0; i < tmp.size(); ++i){
            model = tmp.get(i);
            contactName = MineTalkApp.getContactUserName(model.getUser_hp().replace("-", ""));
            if(contactName != null && contactName.length() > 0)
                model.setUser_name(contactName);
        }

        Collections.sort(tmp, bDescOrder ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);

        if(bGridType){
            for(int i = 0; i < mGridAdapter.getItem().size(); ++i) {
                model = tmp.get(i);
                if(model.getUser_hp().equals(mGridAdapter.getItem().get(i).getUser_hp()) ){
                    mGridAdapter.getItem().get(i).setUser_state_message(model.getUser_state_message());
                }
            }

            mGridAdapter.notifyDataSetChanged();
        }else{
            for(int i = 0; i < mListAdapter.getItem().size(); ++i) {
                model = tmp.get(i);
                if(model.getUser_hp().equals(mListAdapter.getItem().get(i).getUser_hp()) ){
                    mListAdapter.getItem().get(i).setUser_state_message(model.getUser_state_message());
                }
            }

            mListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void refreshView(boolean bGridType) {
        this.bListType = bGridType;
        try {
            setupFriendListData(bGridType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSearchKeyword(final String keyowrd, boolean listType){
        this.bListType = listType;
        this.mSearchKeyword = keyowrd;
        refreshView(listType);
    }

    @Override
    public void refreshListType(boolean listType) {
        this.bListType = listType;

        if (listType) { //GridType
            mLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            mLayoutManager.setSpanCount(1);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }

        mListAdapter.notifyDataSetChanged();
        mGridAdapter.notifyDataSetChanged();

        mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
        mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
    }


    // 친구 리스트 Data Settting
    private void setupFriendListData(boolean bGridType) {

        ArrayList<FriendListModel> friends = MineTalkApp.getFriendList();
        ArrayList<FriendListModel> tmp = new ArrayList<>();
        tmp.addAll(friends);

        String countMessage = String.format("친구 %d명", isSearchMode ? 0 : tmp.size());
        mBinding.tvFriendCount.setText(countMessage);

        if(isSearchMode && mSearchKeyword.length() == 0) return;

        // 친구 검색 모드 일 경우
        if(isSearchMode){
            FriendListModel model;
            for(int i = 0; i < tmp.size(); ++i){
                model = tmp.get(i);
                if(!model.getUser_name().toLowerCase().contains(mSearchKeyword))
                    tmp.remove(i);
            }
        }

        countMessage = String.format("친구 %d명", tmp.size());
        mBinding.tvFriendCount.setText(countMessage);

        FriendListModel model;
        String contactName = "";

        for( int i = 0; i < tmp.size(); ++i){
            model = tmp.get(i);
            contactName = MineTalkApp.getUserNameByPhoneNum(model.getUser_name(), model.getUser_hp().replace("-", ""));
            if(contactName != null && contactName.length() > 0)
                model.setUser_name(contactName);
        }

        Collections.sort(tmp, bDescOrder ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);

        recyclerViewState = mBinding.recyclerView.getLayoutManager().onSaveInstanceState();

        mListAdapter.setListener(mFriendItemViewListener);
        mListAdapter.setItem(tmp);

        mGridAdapter.setListener(mFriendItemViewListener);
        mGridAdapter.setItem(tmp);

        refreshListType(bGridType);

        mBinding.recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
    }

    private OnFriendListViewListener mFriendItemViewListener = new OnFriendListViewListener() {
        @Override
        public void onClickItem(FriendListModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.showProfile(data, FriendProfileActivity.FriendType.FRIEND);
            }
        }

        @Override
        public void onClickFavorite(FriendListModel data) {

        }

        @Override
        public void addFriend(FriendListModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.addFriend(data);
            }
        }

        @Override
        public void showMyProfile(UserInfoBaseModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.showMyProfile();
            }
        }

        @Override
        public void createChatToMe() {

        }

        @Override
        public void editMyProfile() {

        }

        @Override
        public void refreshOrder(boolean bOrder) {
            bDescOrder = bOrder;
            refreshView(bListType);
        }
    };


}