package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.AdsMessageDetailActivity;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.activity.EditChatNameActivity;
import kr.co.minetalk.adapter.ChatListOrgAdapter;
import kr.co.minetalk.api.ListAdMessageApi;
import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.api.model.AdListResponse;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.FragmentChatOrgBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.message.api.AlarmThreadApi;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.OutThreadApi;
import kr.co.minetalk.message.api.ThreadListApi;
import kr.co.minetalk.message.manager.MessageThreadManager;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.repository.ChatRepository;
import kr.co.minetalk.ui.CustomScrollView;
import kr.co.minetalk.ui.listener.OnAdChatItemListener;
import kr.co.minetalk.ui.popup.SimpleChatListOptionPopup;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.AdListItemView;

public class ChatFragmentOrg extends Fragment {
    public static ChatFragmentOrg newInstance() {
        ChatFragmentOrg fragment = new ChatFragmentOrg();
        return fragment;
    }

    private FragmentChatOrgBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;

    private ChatListOrgAdapter mAdapter;

    private ListAdMessageApi mListAdMessageApi;

    private String mCurrentType;
    private HashMap<String, String> mContactMap = new HashMap<>();

    // Paging Variable /////////////////////
    private int mPage               = 1;
    private int mTotalCount         = 0;
    private int mDataCount          = 0;
    private boolean mLockScrollView = false;
    ////////////////////////////////////////

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_org, container, false);
        mBinding.setHandlers(this);

        this.mContactMap.clear();
        mContactMap = FriendManager.getInstance().selectFriendXidToNameMap();

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initApi();
        setUIEventListener();

        mAdapter = new ChatListOrgAdapter();
        mBinding.listView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        setNormalChat();

    }

    private void initApi() {
        mListAdMessageApi = new ListAdMessageApi(getActivity(), new ApiBase.ApiCallBack<AdListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(getActivity());
            }

            @Override
            public void onSuccess(int request_code, AdListResponse adListResponse) {
                ProgressUtil.hideProgress(getActivity());
                if(adListResponse != null) {
                    setAdListData(adListResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(getActivity());
            }

            @Override
            public void onCancellation() {
                ProgressUtil.hideProgress(getActivity());
            }
        });
    }

    private void setNormalChat() {
        mBinding.tvButtonNormal.setSelected(true);
        mBinding.tvButtonAd.setSelected(false);

        mBinding.tvButtonAd.setTextColor(Color.parseColor("#9b9b9b"));
        mBinding.tvButtonNormal.setTextColor(Color.parseColor("#FFFFFF"));

        mBinding.layoutAdRoot.setVisibility(View.GONE);
    }

    private void setAdChat() {
        mBinding.tvButtonNormal.setSelected(false);
        mBinding.tvButtonAd.setSelected(true);

        mBinding.tvButtonNormal.setTextColor(Color.parseColor("#9b9b9b"));
        mBinding.tvButtonAd.setTextColor(Color.parseColor("#FFFFFF"));

        mBinding.layoutAdRoot.setVisibility(View.VISIBLE);

//        mBinding.tvTabLeft.setTextColor(Color.parseColor("#FFFFFF"));
//        mBinding.tvTabRight.setTextColor(Color.parseColor("#002169"));
//        mBinding.tvTabLeft.setSelected(true);
//        mBinding.tvTabRight.setSelected(false);



        mBinding.layoutAdListContainer.removeAllViews();
        initializePagingInfo();
        mBinding.tvTabRight.setTextColor(Color.parseColor("#FFFFFF"));
        mBinding.tvTabLeft.setTextColor(Color.parseColor("#002169"));
        mBinding.tvTabLeft.setSelected(false);
        mBinding.tvTabRight.setSelected(true);

        mCurrentType = MineTalk.AD_LIST_TYPE_R;
        listAdMessageRequest(mCurrentType, mPage);
    }

    private void initializePagingInfo() {
        mTotalCount     = 0;
        mDataCount      = 0;
        mPage           = 1;
        mLockScrollView = false;
    }

    private void setUIEventListener() {
        mBinding.tvTabLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.tvTabRight.setTextColor(Color.parseColor("#FFFFFF"));
                mBinding.tvTabLeft.setTextColor(Color.parseColor("#002169"));
                mBinding.tvTabLeft.setSelected(false);
                mBinding.tvTabRight.setSelected(true);

                mBinding.layoutAdListContainer.removeAllViews();
                initializePagingInfo();

                mCurrentType = MineTalk.AD_LIST_TYPE_R;
                listAdMessageRequest(mCurrentType, mPage);
            }
        });

        mBinding.tvTabRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.tvTabLeft.setTextColor(Color.parseColor("#FFFFFF"));
                mBinding.tvTabRight.setTextColor(Color.parseColor("#002169"));
                mBinding.tvTabLeft.setSelected(true);
                mBinding.tvTabRight.setSelected(false);

                mBinding.layoutAdListContainer.removeAllViews();
                initializePagingInfo();

                mCurrentType = MineTalk.AD_LIST_TYPE_S;
                listAdMessageRequest(mCurrentType, mPage);
            }
        });

        mBinding.btnLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.selectLanguage();
                }
            }
        });

        mBinding.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ThreadData item = (ThreadData) mAdapter.getItem(position);


                String threadName = "";

                if(item.getIsDirect().toUpperCase().equals("Y")) {
                    if(item.getMembers().size() > 0) {
                        String members = "";
                        for(ThreadMember tmember : item.getMembers()) {
                            if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                String contactName = mContactMap.get(tmember.getXid());
                                if(contactName == null || contactName.equals("")) {
                                    members = tmember.getNickName();
                                } else {
                                    members = contactName;
                                }
                                break;
                            }
                        }
                        threadName = members;
                    }
                } else {
                    if(item.getMembers().size() > 0) {
                        String members = "";
//                        for(ThreadMember tmember : item.getMembers()) {
//                            members = members + tmember.getNickName() + ",";
//                        }
                        int count = 0;
                        for(ThreadMember tmember : item.getMembers()) {
                            String contactName = mContactMap.get(tmember.getXid());
                            if(contactName == null || contactName.equals("")) {
                                members = members + tmember.getNickName();
                            } else {
                                members = members + contactName;
                            }

                            if(count != (item.getMembers().size() - 1)) {
                                members = members + ",";
                            }
                            count++;
                        }
                        threadName = members;
                    }
                }

                ChatDetailActivity.startActivity(MineTalkApp.getCurrentActivity(), item.getThreadKey(), threadName);
            }
        });

        mBinding.listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                ThreadData threadData = (ThreadData) mAdapter.getItem(position);
                String userXidMe = MineTalkApp.getUserInfoModel().getUser_xid();
                boolean isAlarm = false;
                String alarmMst = "";

                if(threadData.getMembers().size() > 0) {
                    for(int i = 0 ; i < threadData.getMembers().size() ; i++) {
                        if(userXidMe.equals(threadData.getMembers().get(i).getXid())) {
                            isAlarm = threadData.getMembers().get(i).getAlarm().toUpperCase().equals("Y") ? true : false;
                        }
                    }
                }

                if(isAlarm) {
                    alarmMst = getContext().getResources().getString(R.string.chat_popup_room_alarm_off);
                } else {
                    alarmMst = getContext().getResources().getString(R.string.chat_popup_room_alarm_on);
                }

                String popupTitle = ((TextView)view.findViewById(R.id.tv_user_name)).getText().toString();

                SimpleChatListOptionPopup chatListOptionPopup = new SimpleChatListOptionPopup();
                chatListOptionPopup.setCancelable(true);
                chatListOptionPopup.setTitle(popupTitle);
                chatListOptionPopup.setAlarmMessage(alarmMst);
                chatListOptionPopup.setEventListener(new SimpleChatListOptionPopup.OnOptionPopupListener() {
                    @Override
                    public void onItemClick(int index) {
                        if(index == 0) {
                            ThreadData data = (ThreadData) mAdapter.getItem(position);
                            ChatRepository.getInstance().setThreadKey(data.getThreadKey());
                            String chatName = ((TextView)view.findViewById(R.id.tv_user_name)).getText().toString();
                            EditChatNameActivity.startActivity(getContext(), chatName);

                        } else if(index == 1) {
                            ThreadData data = (ThreadData) mAdapter.getItem(position);
                            boolean alarm = false;
                            if(data.getMembers().size() > 0) {
                                for(int i = 0 ; i < data.getMembers().size() ; i++) {
                                    if(userXidMe.equals(threadData.getMembers().get(i).getXid())) {
                                        alarm = threadData.getMembers().get(i).getAlarm().toUpperCase().equals("Y") ? true : false;
                                    }
                                }

                                alarmRequest(!alarm, data.getThreadKey());
                            }

                        } else if(index == 2) {
                            ThreadData data = (ThreadData) mAdapter.getItem(position);

                            String threadKye = data.getThreadKey();
                            MessageThreadManager.getInstance().removeThread(getContext(), threadKye);
                            String xid = MineTalkApp.getUserInfoModel().getUser_xid();
                            outThreadRequest(xid, threadKye);
                        }
                    }
                });
                chatListOptionPopup.show(getChildFragmentManager(), "popup_chat_option");


                return true;
            }
        });

        mBinding.btnCreateChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.makeChatting();
                }
            }
        });


        mBinding.btnUserFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.searchChat();
                }
            }
        });

        mBinding.tvButtonAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAdChat();
            }
        });

        mBinding.tvButtonNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNormalChat();
            }
        });

        mBinding.scrollView.setScrollViewListener(new CustomScrollView.ScrollViewListener() {
            @Override
            public void onScrollChanged(CustomScrollView scrollView, int x, int y, int oldx, int oldy) {
                View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                if(mTotalCount != 0 && mDataCount < mTotalCount) {
                    if (diff == 0 && mLockScrollView == false) { // 스크롤 bottom
                        mLockScrollView = true;
                        mPage = mPage + 1;

                        listAdMessageRequest(mCurrentType, mPage);
                    }
                }
            }
        });
    }

    private void alarmRequest(boolean enable, String threadKey) {
        ProgressUtil.showProgress(getActivity());

        String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
        AlarmThreadApi alarmThreadApi = new AlarmThreadApi(getContext(), userXid, threadKey, enable);
        alarmThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                ProgressUtil.hideProgress(getActivity());
                refresh();
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                ProgressUtil.hideProgress(getActivity());
            }
        });
    }

    private void chatListRequest(String user_xid) {
//        ProgressUtil.showProgress(MineTalkApp.getCurrentActivity());
        ThreadListApi threadListApi = new ThreadListApi(MineTalkApp.getAppContext(), user_xid);
        threadListApi.request(new BaseHttpRequest.APICallbackListener<List<ThreadData>>() {
            @Override
            public void onSuccess(List<ThreadData> res) {
                int count  = 1;

                ArrayList<ThreadData> threadList = new ArrayList<>();
                for(ThreadData data : res) {
                    threadList.add(data);
                    count++;
                }

                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.setData(threadList);
//                        ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                    }
                });
            }
        });
    }

    private void listAdMessageRequest(String type, int page) {
        mListAdMessageApi.execute(type, page);
    }

    private void setAdListData(AdListResponse response) {
        if(mLockScrollView) {
            mTotalCount = response.getTotal_cnt();
            mDataCount = mDataCount + response.getAd_list().size();
        } else {
            mBinding.layoutAdListContainer.removeAllViews();
            mTotalCount = response.getTotal_cnt();
            mDataCount = mDataCount + response.getAd_list().size();
        }

        for(int i = 0 ; i < response.getAd_list().size() ; i++) {
            AdListItemView itemView = new AdListItemView(getActivity());
            itemView.setData(mCurrentType, response.getAd_list().get(i));
            itemView.setOnEventListener(mAdChatListItemListener);
            mBinding.layoutAdListContainer.addView(itemView);
        }

        mLockScrollView = false;
    }

    private OnAdChatItemListener mAdChatListItemListener = new OnAdChatItemListener() {
        @Override
        public void onItemClick(String type, AdListModel data) {
            if(type.equals(MineTalk.AD_LIST_TYPE_R)) {
                AdsMessageDetailActivity.startActivity(getActivity(), type, data.getAd_idx(), data.getUser_name());
            } else if(type.equals(MineTalk.AD_LIST_TYPE_S)) {

            }
        }
    };


    public void refresh() {
        try {
            chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // 방에서 뒤로가기로 이탈 했을때 했을때
    private void outThreadRequest(String xid, String threadKey) {
        ProgressUtil.showProgress(getActivity());
        OutThreadApi outThreadApi = new OutThreadApi(getContext(), xid, threadKey);
        outThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(getActivity());
                        refresh();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
            }
        });
    }

}
