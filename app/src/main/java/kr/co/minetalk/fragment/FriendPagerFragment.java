package kr.co.minetalk.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.FriendManageActivity;
import kr.co.minetalk.activity.FriendSearchActivity;
import kr.co.minetalk.activity.QRFriendActivity;
import kr.co.minetalk.adapter.FriendFragmentPagerAdapter;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.FragmentFriendPagerBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.listener.OnProfileListener;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class FriendPagerFragment extends Fragment {
    private final int PERMISSON = 1;

    public static final int FRAGMENT_PROPILE = 0;
    public static final int FRAGMENT_FAVORITE = 1;
    public static final int FRAGMENT_FRIEND = 2;
    public static final int FRAGMENT_CONTACT = 3;

    public static FriendPagerFragment newInstance() {
        FriendPagerFragment fragment = new FriendPagerFragment();
        return fragment;
    }

    private FragmentFriendPagerBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;
    private OnProfileListener mProfileListener = null;

    private FriendFragmentPagerAdapter mAdapter;
    private int mViewPagerPosition = 0;

    private TextView tv1;

    public void setOnFragmentListener(OnFragmentEventListener listener, OnProfileListener profileListener) {
        this.mFragmentListener = listener;
        this.mProfileListener = profileListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend_pager, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            updateTheme();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUIEventListener();
        setFriendPagerView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    QRFriendActivity.startActivity(getActivity());
                } else {
                    return;
                }
        }
    }

    private void setUIEventListener() {
        mBinding.btnUserFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendSearchActivity.startActivity(getActivity());
            }
        });

        //QR 코드 친구 찾기
        mBinding.btnQrFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MineTalkApp.getCurrentActivity(), new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        QRFriendActivity.startActivity(getActivity());
                    }
                }
            }
        });

        //친구목록 설정
        mBinding.btnUserSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { FriendManageActivity.startActivity(getActivity());}
        });

        //친구 리스트 ViewType(List,Grid)
        mBinding.ivListType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setFriendListType(!Preferences.getFriendListType());
                boolean listType = Preferences.getFriendListType();

                if(MineTalk.isBlackTheme) { //GridType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
                } else { //ListType
                    mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
                }

                for(int i = 1; i < mAdapter.getCount(); ++i){
                    BaseFragment baseFragment = (BaseFragment) mAdapter.getItem(i);
                    baseFragment.refreshListType(listType);
                }
            }
        });
    }

    private void setFriendPagerView(){
        final ViewPager viewPager = mBinding.viewPager;
        viewPager.setOffscreenPageLimit(4);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter = new FriendFragmentPagerAdapter(getFragmentManager(), mFragmentListener, mProfileListener);
                viewPager.setAdapter(mAdapter);
            }
        });

        mBinding.tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition() == 0){
                    mBinding.ivListType.setVisibility(View.INVISIBLE);
                }else{
                    mBinding.ivListType.setVisibility(View.VISIBLE);
                }
                viewPager.setCurrentItem(tab.getPosition(), false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mBinding.tablayout));

        mBinding.getRoot().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int topPos = (mBinding.tablayout.getHeight() / 2) - (mBinding.ivListType.getHeight() / 2);

                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams)mBinding.ivListType.getLayoutParams();
                if( marginParams.topMargin == 0 )
                    marginParams.setMargins(marginParams.leftMargin, topPos, marginParams.rightMargin, marginParams.rightMargin);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 2){
                    if(mFragmentListener != null){
                        mFragmentListener.onUpdateFriendList();
                    }

//                    if(tv1 != null)
//                        tv1.setTextColor(getResources().getColor(R.color.app_point_color));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //첫 페이지 화면을 친구로 설정 함.
        viewPager.setCurrentItem(2, false);
    }

    public void refreshView() {
        try {
            if(mAdapter != null){
               for( int i = 1; i < mAdapter.getCount()-1; ++i){
                   ((BaseFragment)mAdapter.getItem(i)).refreshView();
               }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private OnFriendListViewListener mFriendItemViewListener = new OnFriendListViewListener() {
        @Override
        public void onClickItem(FriendListModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.showProfile(data);
            }
        }

        @Override
        public void onClickFavorite(FriendListModel data) {

        }

        @Override
        public void addFriend(FriendListModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.addFriend(data);
            }
        }

        @Override
        public void showMyProfile(UserInfoBaseModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.showMyProfile();
            }
        }

        @Override
        public void createChatToMe() {

        }

        @Override
        public void editMyProfile() {

        }

        @Override
        public void refreshOrder(boolean bOrder) {

        }
    };

    private void updateTheme(){
        boolean listType = Preferences.getFriendListType();

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvFriendTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tablayout.setBackgroundResource(R.drawable.bg_tab_layout_bk);
            mBinding.tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tablayout.setTabTextColors(getResources().getColor(R.color.white_color), getResources().getColor(R.color.white_color));

            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);

            mBinding.btnQrFriend.setImageResource(R.drawable.icon_qr_bk);
            mBinding.btnUserFind.setImageResource(R.drawable.icon_search_bk);
            mBinding.btnUserSet.setImageResource(R.drawable.icon_set_bk);
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvFriendTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tablayout.setBackgroundResource(R.drawable.bg_tab_layout);
            mBinding.tablayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.app_point_color));
            mBinding.tablayout.setTabTextColors(getResources().getColor(R.color.main_text_color), getResources().getColor(R.color.main_text_color));

            mBinding.ivListType.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);

            mBinding.btnQrFriend.setImageResource(R.drawable.icon_qr);
            mBinding.btnUserFind.setImageResource(R.drawable.icon_search);
            mBinding.btnUserSet.setImageResource(R.drawable.icon_set);
        }
    }
}
