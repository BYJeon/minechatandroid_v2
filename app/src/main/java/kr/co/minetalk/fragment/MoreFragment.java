package kr.co.minetalk.fragment;

import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import kr.co.minetalk.BuildConfig;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.EditProfileActivity;
import kr.co.minetalk.activity.FriendManageActivity;
import kr.co.minetalk.activity.MyQrActivity;
import kr.co.minetalk.activity.NoticeActivity;
import kr.co.minetalk.adapter.MoreFragmentPagerAdapter;
import kr.co.minetalk.databinding.FragmentMoreBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.fragment.listener.OnMoreFragmentEventListener;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;

public class MoreFragment extends Fragment {

    private final int PERMISSON = 1;

    public static MoreFragment newInstance() {
        MoreFragment fragment = new MoreFragment();
        return fragment;
    }

    private FragmentMoreBinding mBinding;
    private OnFragmentEventListener mFragmentListener;

    private MoreFragmentPagerAdapter mFragmentAdapter;

    private int mCurrentViewPagerIndex = 0;


    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_more, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        refreshView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initApi();
        setUIEventListener();

        mBinding.viewPager.setClipToPadding(false);
        mBinding.viewPager.setPadding((int) CommonUtils.convertDpToPixel(15, getActivity()), 0, (int) CommonUtils.convertDpToPixel(15, getActivity()), 0);
        mBinding.viewPager.setPageMargin((int) CommonUtils.convertDpToPixel(17, getActivity()));

//        mBinding.ivIndigator1.setSelected(true);
//        mBinding.ivIndigator2.setSelected(false);

    }

    private void initApi() {

    }

    private void setUIEventListener() {

        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCurrentViewPagerIndex = position;
//                if (position == 0) {
//                    mBinding.ivIndigator1.setSelected(true);
//                    mBinding.ivIndigator2.setSelected(false);
//                } else if(position == 1) {
//                    mBinding.ivIndigator1.setSelected(false);
//                    mBinding.ivIndigator2.setSelected(true);
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mBinding.switchMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);

                if(mFragmentListener != null) {
                    if(isSelect) {
                        mFragmentListener.changeReceiveMessageFlag("Y");
                    } else {
                        mFragmentListener.changeReceiveMessageFlag("N");
                    }
                }

            }
        });

        mBinding.btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditProfileActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoticeActivity.startActivity(getActivity());
            }
        });

//        mBinding.btnPointSend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PointSendActivity.startActivity(getActivity());
//            }
//        });

        mBinding.btnQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyQrActivity.startActivity(getActivity());
            }
        });

//        mBinding.btnPointReport.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PointReportActivity.startActivity(getActivity());
//            }
//        });

        mBinding.layoutButtonFriendManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendManageActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.logout();
                }
            }
        });

        mBinding.layoutButtonWithDrawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.withDrawal();
                }
            }
        });

        mBinding.ivRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onRefresh();
                }
            }
        });

        mBinding.layoutButtonFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onFaq();
                }
            }
        });

        mBinding.layoutButtonQna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onQna();
                }
            }
        });

        mBinding.layoutButtonRecommendManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onRecommendManager();
                }
            }
        });

        mBinding.layoutButtonTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onPolicy();
                }
            }
        });

    }

    private void setAppVersionInfomation() {
        String version = BuildConfig.VERSION_NAME;
        mBinding.tvAppVersion.setText(version);
    }

    private void setupMessageSwitch() {
        String messageFlag = MineTalkApp.getUserInfoModel().getUser_message_receive_flag();
        if(messageFlag.toUpperCase().equals("Y")) {
            mBinding.switchMessage.setSelected(true);
        } else {
            mBinding.switchMessage.setSelected(false);
        }
    }

    private void setupUserInfomation() {
        String userName = MineTalkApp.getUserInfoModel().getUser_name();
        String userEmail = MineTalkApp.getUserInfoModel().getUser_email();
        String profileImgUrl = MineTalkApp.getUserInfoModel().getUser_profile_image();

        if(profileImgUrl == null || profileImgUrl.equals("")) {
            Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
        } else {
            Glide.with(MineTalkApp.getCurrentActivity()).load(profileImgUrl).into(mBinding.ivUserImage);
        }

        mBinding.tvUserName.setText(userName);
        if(userEmail == null || userEmail.equals("")) {
            String emptyMsg = MineTalkApp.getAppContext().getResources().getString(R.string.more_fragment_email_empty_message);
            mBinding.tvUserEmail.setText(emptyMsg);
        } else {
            mBinding.tvUserEmail.setText(userEmail);
        }

        mFragmentAdapter.notifyDataSetChanged();
    }

    public void refreshView() {
        try {
            mFragmentAdapter = new MoreFragmentPagerAdapter(getChildFragmentManager(), mOnMoreFragmentEventListener);
            mBinding.viewPager.setAdapter(mFragmentAdapter);

            mBinding.viewPager.setCurrentItem(mCurrentViewPagerIndex);
            setupUserInfomation();
            setupMessageSwitch();
            setAppVersionInfomation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    return;
                }
        }
    }

    private OnMoreFragmentEventListener mOnMoreFragmentEventListener = new OnMoreFragmentEventListener() {
        @Override
        public void onRefresh() {
            if(mFragmentListener != null) {
                mFragmentListener.onRefresh();
            }
        }
    };


}
