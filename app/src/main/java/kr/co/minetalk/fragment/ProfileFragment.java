package kr.co.minetalk.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.AdsMessageDetailActivity;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.activity.EditProfileActivity;
import kr.co.minetalk.activity.FriendProfileActivity;
import kr.co.minetalk.activity.FriendSearchActivity;
import kr.co.minetalk.activity.MainActivity;
import kr.co.minetalk.activity.MyQrActivity;
import kr.co.minetalk.activity.PhotoViewerActivity;
import kr.co.minetalk.activity.PointReportActivity;
import kr.co.minetalk.activity.PointReportMoreActivity;
import kr.co.minetalk.activity.base.BindActivity;
import kr.co.minetalk.api.UploadImageApi;
import kr.co.minetalk.api.UserInfoApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.FragmentFriendBinding;
import kr.co.minetalk.databinding.FragmentProfileBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.listener.OnMediaPopupListener;
import kr.co.minetalk.ui.listener.OnProfileListener;
import kr.co.minetalk.ui.popup.BottomMediaPopup;
import kr.co.minetalk.ui.popup.SimplePopup;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.ContactListView;
import kr.co.minetalk.view.FriendListView;
import kr.co.minetalk.view.UserIconView;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class ProfileFragment extends BaseFragment {
    private final int PERMISSON = 1;
    private UploadImageApi mUploadImageApi;
    private UserInfoApi mUserInfoApi;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    private FragmentProfileBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;
    private OnProfileListener mProfileListener = null;

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    public void setOnProfileListener(OnProfileListener listener) {
        this.mProfileListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            setupMyProfile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initApi();
        setUIEventListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void initApi() {
        mUserInfoApi = new UserInfoApi(MineTalkApp.getCurrentActivity(), new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MineTalkApp.getCurrentActivity());
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                if(userInfoBaseModel != null) {
                    String profileImgUrl = userInfoBaseModel.getUser_profile_image();

                    if(profileImgUrl == null || profileImgUrl.equals("")) {
                        Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
                    } else {
                        Glide.with(MineTalkApp.getCurrentActivity()).load(profileImgUrl).into(mBinding.ivUserImage);
                    }

                    MineTalkApp.setUserInfoModel(userInfoBaseModel);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
            }

            @Override
            public void onCancellation() {

            }
        });

        mUploadImageApi = new UploadImageApi(MineTalkApp.getCurrentActivity(), new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MineTalkApp.getCurrentActivity());
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                if(baseModel != null) {
                    if(mUserInfoApi != null)
                        mUserInfoApi.onDestroy();

                    mUserInfoApi.execute();
                }

                if(!mCameraphotonameTemp.equals("")) {
                    try {
                        File tempImageFile = new File(mCameraphotonameTemp);
                        tempImageFile.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mCameraphotonameTemp = "";
                }


            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                showMessageAlert(message);
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void showMessageAlert(String message) {
        SimplePopup simplePopup = new SimplePopup(message, null, getString(R.string.common_ok));
        simplePopup.show(getFragmentManager(), "alert_message");
    }

    private void setUIEventListener() {
        //나와의 채팅
        mBinding.tvButtonChatMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mProfileListener != null) {
                    mProfileListener.onTalkMe(MineTalkApp.getUserInfoModel());
                }
            }
        });

        //내 프로필 수정
        mBinding.tvButtonEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mProfileListener != null) {
                    mProfileListener.onEditProfile();
                }
            }
        });

        //내 포인트
       mBinding.tvButtonPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PointReportMoreActivity.startActivity(getActivity());
            }
        });

        //내 QR
        mBinding.tvButtonQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyQrActivity.startActivity(getActivity());
            }
        });

        //내 사진 클릭
        mBinding.ivUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserInfoBaseModel userInfoMe = MineTalkApp.getUserInfoModel();

                if(userInfoMe != null) {
                    PhotoViewerActivity.startActivity(getActivity(), userInfoMe.getUser_profile_image(), userInfoMe.getUser_name(), StringEscapeUtils.unescapeJava(userInfoMe.getUser_state_message()), "");
                }
            }
        });

        mBinding.ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MineTalkApp.getCurrentActivity(), new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        showMediaBottomPopup();
                    }
                }
            }
        });
    }

    @Override
    public void refreshView() {
        try {
            setupMyProfile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refreshView(boolean bGridType) {

    }

    @Override
    public void refreshListType(boolean listType) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        MineTalkApp.mIsOutOfApp = false;

        if(requestCode == MineTalk.REQUEST_CODE_MEDIA) {
            if(resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                if (uri != null) {
                    String contentType = CommonUtils.getMediaType(MineTalkApp.getCurrentActivity(), uri);
                    String contentPath = CommonUtils.getImageMediaPath(MineTalkApp.getCurrentActivity(), uri);

                    Log.e("@@@TAG", "file path : " + contentPath);

                    try {
                        ExifInterface exif = new ExifInterface(contentPath);
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                        Bitmap originalImage = BitmapFactory.decodeFile(contentPath);
                        Bitmap rotateImage = CommonUtils.rotateBitmap(originalImage, orientation);

                        String saveTempBmImage = Environment.getExternalStorageDirectory() + "/MineChat/" + "tmp_profile_" + System.currentTimeMillis() + ".jpg";
                        if(saveBitmapToFileCache(rotateImage, saveTempBmImage)) {
                            mCameraphotonameTemp  = saveTempBmImage;
                            uploadImageRequest(new File(saveTempBmImage));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        } else if(requestCode == MineTalk.REQUEST_CODE_CAMERA && resultCode == Activity.RESULT_OK) {


            File image = new File(Environment.getExternalStorageDirectory() + "/MineChat/" + mCameraPhotoName);
            Bitmap originalImage = BitmapFactory.decodeFile(image.getPath());

            if(originalImage != null) {
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(image.getPath());

                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Bitmap rotateImage = CommonUtils.rotateBitmap(originalImage, orientation);


                    String saveTempBmImage = Environment.getExternalStorageDirectory() + "/MineChat/" + "tmp_profile_" + mCameraPhotoName;

                    if(saveBitmapToFileCache(rotateImage, saveTempBmImage)) {
                        mCameraphotonameTemp  = saveTempBmImage;
                        uploadImageRequest(new File(saveTempBmImage));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    onChooseMedia();
                    showMediaBottomPopup();
                } else {
                    return;
                }
        }
    }

    private void uploadImageRequest(File image) {
        mUploadImageApi.execute(MineTalkApp.getCurrentActivity(), image);
    }

    private Uri mCameraPhotoUri;
    private String mCameraPhotoName;
    private String mCameraphotonameTemp = "";
    public void requestTakePhotoRequest(int request_code) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); //사진을 찍기 위하여 설정합니다.
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException e) {
            Toast.makeText(MineTalkApp.getCurrentActivity(), getResources().getString(R.string.image_process_failed_msg), Toast.LENGTH_SHORT).show();
        }
        if (photoFile != null) {
            mCameraPhotoUri = FileProvider.getUriForFile(MineTalkApp.getCurrentActivity(),
                    "kr.co.minetalk.provider", photoFile); //FileProvider의 경우 이전 포스트를 참고하세요.
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mCameraPhotoUri); //사진을 찍어 해당 Content uri를 photoUri에 적용시키기 위함

            MineTalkApp.mIsOutOfApp = true;
            startActivityForResult(intent, request_code);
        }
    }

    // Android M에서는 Uri.fromFile 함수를 사용하였으나 7.0부터는 이 함수를 사용할 시 FileUriExposedException이
    // 발생하므로 아래와 같이 함수를 작성합니다. 이전 포스트에 참고한 영문 사이트를 들어가시면 자세한 설명을 볼 수 있습니다.
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        String imageFileName = "PP" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/MineChat/"); //test라는 경로에 이미지를 저장하기 위함
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCameraPhotoName = image.getName();

        return image;
    }

    private boolean saveBitmapToFileCache(Bitmap bitmap, String strFilePath) {

        File fileCacheItem = new File(strFilePath);
        OutputStream out = null;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(out != null)
                    out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private void onChooseMedia() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,null);
        galleryIntent.setType("image/*");
        galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.select_work));

        //앱 내에서 다른 Intent를 불렀을 경우 체트
        MineTalkApp.mIsOutOfApp = true;
        startActivityForResult(chooser,MineTalk.REQUEST_CODE_MEDIA);
    }

    private void showMediaBottomPopup() {
        BottomMediaPopup mediaPopup = new BottomMediaPopup(MineTalkApp.getCurrentActivity());
        mediaPopup.setEventListener(new OnMediaPopupListener() {
            @Override
            public void onCamera() {
                requestTakePhotoRequest(MineTalk.REQUEST_CODE_CAMERA);
            }

            @Override
            public void onGallery() {
                onChooseMedia();
            }
        });
        mediaPopup.show();
    }
    // 내프로필 Data Setting
    private void setupMyProfile() {

        updateTheme();
        //mBinding.layoutMyProfileContainer.removeAllViews();
        UserInfoBaseModel userInfoMe = MineTalkApp.getUserInfoModel();

        if(userInfoMe != null) {

            MessageJSonModel msgModel = new MessageJSonModel();
            try {
                JSONObject msgObject = new JSONObject(userInfoMe.getUser_state_message());
                msgModel = MessageJSonModel.parse(msgObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mBinding.tvName.setText(userInfoMe.getUser_name());
            String profileImgUrl = userInfoMe.getUser_profile_image();
            if(profileImgUrl == null || profileImgUrl.equals("")) {
                Glide.with(getActivity()).load(R.drawable.img_basic).into(mBinding.ivUserImage);
            } else {
                Glide.with(getActivity()).load(profileImgUrl).into(mBinding.ivUserImage);
            }

            mBinding.tvStatusMe.setText(userInfoMe.getUser_state_message());
            //String nationCode = String.format("(+%s)", userInfoMe.getUser_nation_code());
            mBinding.tvPhoneNum.setText(userInfoMe.getUser_hp());
        }
    }

    private void updateTheme(){
        if(MineTalk.isBlackTheme){
            mBinding.tvName.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvStatusMe.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvPhoneNum.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.ivCamera.setImageResource(R.drawable.btn_camera_edit_bk);
        }else{
            mBinding.tvName.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvStatusMe.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvPhoneNum.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.ivCamera.setImageResource(R.drawable.btn_camera_edit);
        }
    }
}
