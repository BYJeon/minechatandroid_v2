package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.AdsMessageDetailActivity;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.adapter.ChatGridAdapter;
import kr.co.minetalk.adapter.ChatListAdapter;
import kr.co.minetalk.api.ListAdMessageApi;
import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.api.model.AdListResponse;
import kr.co.minetalk.api.model.MessageJSonModel;
import kr.co.minetalk.databinding.FragmentChatBinding;
import kr.co.minetalk.databinding.FragmentChatSecurityBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.base.ChatBaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.message.api.AlarmThreadApi;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.OutThreadApi;
import kr.co.minetalk.message.api.ThreadListApi;
import kr.co.minetalk.message.api.ThreadSecurityListApi;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.ui.listener.OnAdChatItemListener;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.AdListItemView;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class ChatSecurityFragment extends ChatBaseFragment {
    public static ChatSecurityFragment newInstance() {
        ChatSecurityFragment fragment = new ChatSecurityFragment();
        return fragment;
    }

    private FragmentChatSecurityBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;

    private GridLayoutManager mGridLayoutManager;

    private ChatListAdapter mListAdapter;
    private ChatGridAdapter mGridAdapter;

    private ListAdMessageApi mListAdMessageApi;

    private String mCurrentType;
    private HashMap<String, String> mContactMap = new HashMap<>();

    private ThreadData mChatMe = null;

    // Paging Variable /////////////////////
    private int mPage = 1;
    private int mTotalCount = 0;
    private int mDataCount = 0;
    private boolean mLockScrollView = false;
    ////////////////////////////////////////

    private Parcelable recyclerViewState;

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    public void setOnContactMap(HashMap<String, String> contactMap) {
        this.mContactMap = contactMap;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_security, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        updateTheme();
        refreshChatList();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initApi();
        setUIEventListener();

        mListAdapter = new ChatListAdapter();

        mGridAdapter = new ChatGridAdapter();

        mGridLayoutManager = new GridLayoutManager(getContext(), 1);
        mBinding.recyclerView.setLayoutManager(mGridLayoutManager);
        mBinding.recyclerView.setAdapter(mListAdapter);

        String countMessage = String.format(getResources().getString(R.string.security_chat_count_msg), "0");
        //mBinding.tvBlockCount.setText(countMessage);
    }

    @Override
    public void refreshView() {
        refresh();
    }

    @Override
    public void refreshView(boolean bGridType) {
        refresh();
    }

    @Override
    public void refreshListType(boolean listType) {
        if (listType) { //GridType
            mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            mGridLayoutManager.setSpanCount(1);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }

        mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
        mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
    }


    @Override
    public void refreshChatList() {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateTheme(){
        if(MineTalk.isBlackTheme){
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top_bk);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom_bk);
        }else{
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom);
        }
    }

    private void initApi() {
        mListAdMessageApi = new ListAdMessageApi(getActivity(), new ApiBase.ApiCallBack<AdListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(getActivity());
            }

            @Override
            public void onSuccess(int request_code, AdListResponse adListResponse) {
                ProgressUtil.hideProgress(getActivity());
                if (adListResponse != null) {
                    //setAdListData(adListResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(getActivity());
            }

            @Override
            public void onCancellation() {
                ProgressUtil.hideProgress(getActivity());
            }
        });
    }


    private void initializePagingInfo() {
        mTotalCount = 0;
        mDataCount = 0;
        mPage = 1;
        mLockScrollView = false;
    }

    private void setUIEventListener() {
        //나와의 채팅
        mBinding.layoutChatme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mChatMe != null) {
                    String threadName = "";
                    if (mChatMe.getMembers().size() > 0) {
                        String members = "";
                        for (ThreadMember tmember : mChatMe.getMembers()) {
                            if (!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                String contactName = mContactMap.get(tmember.getXid());
                                if (contactName == null || contactName.equals("")) {
                                    members = tmember.getNickName();
                                } else {
                                    members = contactName;
                                }
                                break;
                            }
                        }
                        threadName = members;
                    }

                    ChatDetailActivity.startActivity(MineTalkApp.getCurrentActivity(), mChatMe.getThreadKey(), threadName);
                }
            }
        });

        mBinding.ibSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                mBinding.ibSortList.setText(bDescOrder ? "가↑" : "가↓");
                refreshView();
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.viewGradient.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);

                mBinding.viewGradient.setVisibility(View.INVISIBLE);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bListType){ // Grid Type
                    mBinding.recyclerView.scrollToPosition(mGridAdapter.getItemCount()-1);
                }else{
                    mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                }

                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });
    }

    private OnFriendListViewOrderListener mFriendListViewOrderListener = new OnFriendListViewOrderListener() {
        @Override
        public void refreshListType(boolean bGridType) {
            boolean listType = Preferences.getSelectFriendListType();

            if (listType) { //GridType
                mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return position == 0 ? BaseFragment.mRowNum : 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mGridAdapter);
            } else {
                mGridLayoutManager.setSpanCount(1);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mListAdapter);
            }
        }

        @Override
        public void refreshOrder(boolean bOrder) {
            bDescOrder = bOrder;
            refreshView();
        }
    };

    private void alarmRequest(boolean enable, String threadKey) {
        ProgressUtil.showProgress(getActivity());

        String userXid = MineTalkApp.getUserInfoModel().getUser_xid();
        AlarmThreadApi alarmThreadApi = new AlarmThreadApi(getContext(), userXid, threadKey, enable);
        alarmThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                ProgressUtil.hideProgress(getActivity());
                refresh();
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                ProgressUtil.hideProgress(getActivity());
            }
        });
    }

    private void chatListRequest(String user_xid) {
        ThreadSecurityListApi threadListApi = new ThreadSecurityListApi(MineTalkApp.getAppContext(), user_xid);
        threadListApi.request(new BaseHttpRequest.APICallbackListener<List<ThreadData>>() {
            @Override
            public void onSuccess(List<ThreadData> res) {
                String myChatThreadKey = Preferences.getMyChattingThreadKey();
                mChatMe = null;

                ArrayList<ThreadData> threadList = new ArrayList<>();
                int nTotalUnreadCnt = 0;

                for (ThreadData data : res) {
                    int unReadCount = getUnReadCount(data.getLastSeq(), data.getMembers(), MineTalkApp.getUserInfoModel().getUser_xid());
                    //전체 안읽은 메세지 숫자
                    nTotalUnreadCnt += unReadCount;
                    threadList.add(data);
                }

                Log.d("ChatFragment M:", "ChatSecurityFragment");
                //채팅 노티 숫자 업데이트
                if(mFragmentListener != null)
                    mFragmentListener.onUpdateBottomMenuNotification(nTotalUnreadCnt, 1);

                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerViewState = mBinding.recyclerView.getLayoutManager().onSaveInstanceState();

                        // 나와의 대화가 있을 경우
                        if (mChatMe != null) {
                            mBinding.layoutChatmeList.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));

                            mBinding.layoutChatme.setVisibility(View.VISIBLE);
                            mBinding.tvChatmeTitle.setVisibility(View.VISIBLE);
                            mBinding.layoutChatmeList.layoutRoot.setVisibility(View.VISIBLE);
                            mBinding.layoutChatmeList.tvUserName.setText(MineTalkApp.getUserInfoModel().getUser_name());

                            MessageJSonModel msgModel = new MessageJSonModel();
                            try {
                                JSONObject msgObject = new JSONObject(mChatMe.getLastMessage());
                                msgModel = MessageJSonModel.parse(msgObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_TEXT) ||
                                    msgModel.getMsgType().equals(MineTalk.MSG_TYPE_URL)) {
                                if (msgModel.getText().equals("")) {
                                    mBinding.layoutChatmeList.tvStatus.setText(MineTalkApp.getCurrentActivity().getString(R.string.msg_type_emoticon));
                                } else {
                                    mBinding.layoutChatmeList.tvStatus.setText(StringEscapeUtils.unescapeJava(msgModel.getText()));
                                }
                            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_IMAGE)) {
                                mBinding.layoutChatmeList.tvStatus.setText(MineTalkApp.getCurrentActivity().getString(R.string.msg_type_image));
                            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_VIDEO)) {
                                mBinding.layoutChatmeList.tvStatus.setText(MineTalkApp.getCurrentActivity().getString(R.string.msg_type_video));
                            } else if (msgModel.getMsgType().equals(MineTalk.MSG_TYPE_CONTACT)) {
                                mBinding.layoutChatmeList.tvStatus.setText(MineTalkApp.getCurrentActivity().getString(R.string.msg_type_contact));
                            }


                            String profileImgUrl = MineTalkApp.getUserInfoModel().getUser_profile_image();

                            if (profileImgUrl == null || profileImgUrl.equals("")) {
                                Glide.with(MineTalkApp.getCurrentActivity()).load(R.drawable.img_basic).into(mBinding.layoutChatmeList.ivUserImage);
                            } else {
                                Glide.with(MineTalkApp.getCurrentActivity()).load(profileImgUrl).into(mBinding.layoutChatmeList.ivUserImage);
                            }

                            long now = System.currentTimeMillis();
                            Date date = new Date(now);
                            SimpleDateFormat mDtFormat = new SimpleDateFormat("yyyy-MM-dd");
                            String curDateTime = mDtFormat.format(date);
                            String updateTime = mDtFormat.format(mChatMe.getUpdateDate());
                            long diffDay = MineTalkApp.calDateBetweenDiff(curDateTime, updateTime, mDtFormat);

                            if(diffDay != -1 && diffDay == 0) {
                                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
                                mBinding.layoutChatmeList.tvLastDate.setText(timeFormat.format(mChatMe.getUpdateDate()));
                            }
                            else
                                mBinding.layoutChatmeList.tvLastDate.setText(updateTime);

                        } else {
                            mBinding.layoutChatme.setVisibility(View.GONE);
                            mBinding.tvChatmeTitle.setVisibility(View.GONE);
                            mBinding.layoutChatmeList.layoutRoot.setVisibility(View.GONE);
                        }


                        mListAdapter.setContactMap(mContactMap);
                        mGridAdapter.setContactMap(mContactMap);

                        //정렬
                        Collections.sort(threadList, bDescOrder ? MineTalkApp.threadDataCmpDesc : MineTalkApp.threadDataCmpAsc);

                        mListAdapter.setData(threadList, true);
                        mListAdapter.setAdapterListener(mFriendListViewOrderListener);
                        mGridAdapter.setData(threadList, true);
                        mGridAdapter.setAdapterListener(mFriendListViewOrderListener);

                        boolean listType = Preferences.getChatListType();

                        if (listType) { //GridType
                            mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                                @Override
                                public int getSpanSize(int position) {
                                    return position == 0 ? BaseFragment.mRowNum : 1;
                                }
                            });
                            mBinding.recyclerView.setAdapter(mGridAdapter);
                        } else {
                            mGridLayoutManager.setSpanCount(1);
                            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                                @Override
                                public int getSpanSize(int position) {
                                    return 1;
                                }
                            });
                            mBinding.recyclerView.setAdapter(mListAdapter);
                        }

                        String countMessage = String.format(getResources().getString(R.string.security_chat_count_msg), Integer.toString(threadList.size()));
                        mBinding.tvBlockCount.setText(countMessage);

                        mBinding.recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                    }
                });
            }
        });
    }

    private int getUnReadCount(int lastSeq, List<ThreadMember> members, String userXid) {
        int readSeq = 0;
        for(ThreadMember tmember : members) {
            if(tmember.getXid().equals(userXid)) {
                readSeq = tmember.getReadSeq();
            }
        }

        return lastSeq - readSeq;
    }

    private void listAdMessageRequest(String type, int page) {
        mListAdMessageApi.execute(type, page);
    }

    private void setAdListData(AdListResponse response) {
//        if(mLockScrollView) {
//            mTotalCount = response.getTotal_cnt();
//            mDataCount = mDataCount + response.getAd_list().size();
//        } else {
//            mBinding.layoutAdListContainer.removeAllViews();
//            mTotalCount = response.getTotal_cnt();
//            mDataCount = mDataCount + response.getAd_list().size();
//        }

        for (int i = 0; i < response.getAd_list().size(); i++) {
            AdListItemView itemView = new AdListItemView(getActivity());
            //itemView.setData(mCurrentType, response.getAd_list().get(i));
            itemView.setOnEventListener(mAdChatListItemListener);
            //mBinding.layoutAdListContainer.addView(itemView);
        }

        mLockScrollView = false;
    }

    private OnAdChatItemListener mAdChatListItemListener = new OnAdChatItemListener() {
        @Override
        public void onItemClick(String type, AdListModel data) {
            if (type.equals(MineTalk.AD_LIST_TYPE_R)) {
                AdsMessageDetailActivity.startActivity(getActivity(), type, data.getAd_idx(), data.getUser_name());
            } else if (type.equals(MineTalk.AD_LIST_TYPE_S)) {

            }
        }
    };


    public void refresh() {
        try {
            chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // 방에서 뒤로가기로 이탈 했을때 했을때
    private void outThreadRequest(String xid, String threadKey) {
        ProgressUtil.showProgress(getActivity());
        OutThreadApi outThreadApi = new OutThreadApi(getContext(), xid, threadKey);
        outThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(getActivity());
                        refresh();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
            }
        });
    }
}
