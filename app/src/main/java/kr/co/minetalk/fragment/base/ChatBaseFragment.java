package kr.co.minetalk.fragment.base;

public abstract class ChatBaseFragment extends BaseFragment {
    public abstract void refreshChatList();
}
