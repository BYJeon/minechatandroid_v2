package kr.co.minetalk.fragment.base;

import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;

import org.apache.commons.lang3.NotImplementedException;

import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.ui.popup.SimplePopup;

public abstract class BaseFragment extends Fragment {
    protected boolean bListType  = false;
    protected boolean bDescOrder = false;
    public static final int mRowNum = 3;
    protected GridLayoutManager mLayoutManager = null;

    public abstract void refreshView();
    public abstract void refreshListType(boolean listType);
    public abstract void refreshView(boolean bGridType);
}
