package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.AdsMessageDetailActivity;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.adapter.ChatGridAdapter;
import kr.co.minetalk.adapter.ChatGroupGridAdapter;
import kr.co.minetalk.adapter.ChatListAdapter;
import kr.co.minetalk.api.ListAdMessageApi;
import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.api.model.AdListResponse;
import kr.co.minetalk.databinding.FragmentChatBinding;
import kr.co.minetalk.databinding.FragmentChatSearchBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.message.api.AlarmThreadApi;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.OutThreadApi;
import kr.co.minetalk.message.api.ThreadListApi;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.message.model.ThreadMember;
import kr.co.minetalk.ui.listener.OnAdChatItemListener;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.AdListItemView;

public class ChatSearchFragment extends BaseFragment {
    public static ChatSearchFragment newInstance() {
        ChatSearchFragment fragment = new ChatSearchFragment();
        return fragment;
    }

    private FragmentChatSearchBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;

    private GridLayoutManager mGridLayoutManager;
    private ChatListAdapter mListAdapter;
    private ChatGroupGridAdapter mGridAdapter;

    private ListAdMessageApi mListAdMessageApi;

    private String mCurrentType;
    private HashMap<String, String> mContactMap = new HashMap<>();
    private String mSearchKeyword = "";

    private ThreadData mChatMe = null;

    // Paging Variable /////////////////////
    private int mPage               = 1;
    private int mTotalCount         = 0;
    private int mDataCount          = 0;
    private boolean mLockScrollView = false;
    ////////////////////////////////////////

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    public void setOnContactMap(HashMap<String, String> contactMap){
        this.mContactMap = contactMap;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_search, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initApi();
        setUIEventListener();

        mListAdapter = new ChatListAdapter();
        //mListAdapter.setAdapterListener(mFriendBlockAdapterListener);

        mGridAdapter = new ChatGroupGridAdapter();
        //mGridAdapter.setAdapterListener(mFriendBlockAdapterListener);

        mGridLayoutManager = new GridLayoutManager(getContext(), 1);
        mBinding.recyclerView.setLayoutManager(mGridLayoutManager);
        mBinding.recyclerView.setAdapter(mListAdapter);

        String countMessage = String.format("채팅 %d건", 0);
        mBinding.tvBlockCount.setText(countMessage);
    }


    @Override
    public void refreshView() {
        refresh(this.bListType);
    }

    @Override
    public void refreshView(boolean bGridType) {
        refresh(bGridType);
    }

    @Override
    public void refreshListType(boolean listType) {
        this.bListType = listType;
        if (listType) { //GridType
            mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            mGridLayoutManager.setSpanCount(1);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }
    }

    private void initApi() {
        mListAdMessageApi = new ListAdMessageApi(getActivity(), new ApiBase.ApiCallBack<AdListResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(getActivity());
            }

            @Override
            public void onSuccess(int request_code, AdListResponse adListResponse) {
                ProgressUtil.hideProgress(getActivity());
                if(adListResponse != null) {
                    //setAdListData(adListResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(getActivity());
            }

            @Override
            public void onCancellation() {
                ProgressUtil.hideProgress(getActivity());
            }
        });
    }


    private void initializePagingInfo() {
        mTotalCount     = 0;
        mDataCount      = 0;
        mPage           = 1;
        mLockScrollView = false;
    }

    private void setUIEventListener() {
        mBinding.ibSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                mBinding.ibSortList.setText(bDescOrder ? "가↑" : "가↓");
                refreshView();
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });
    }


    public void setSearchKeyword(final String keyowrd, boolean listType){
        this.mSearchKeyword = keyowrd;
        refreshView(listType);
    }

    private void chatListRequest(String user_xid, boolean bGridType) {
        this.bListType = bGridType;

        ThreadListApi threadListApi = new ThreadListApi(MineTalkApp.getAppContext(), user_xid);
        threadListApi.request(new BaseHttpRequest.APICallbackListener<List<ThreadData>>() {
            @Override
            public void onSuccess(List<ThreadData> res) {
                if( mSearchKeyword.length() == 0 || mSearchKeyword.equals("") ) return;
                String myChatThreadKey = Preferences.getMyChattingThreadKey();
                mChatMe = null;


                String threadName = "";
                ArrayList<ThreadData> threadList = new ArrayList<>();
                for(ThreadData data : res) {
                    if(data.getIsDirect().toUpperCase().equals("Y")) {
                        if(data.getMembers().size() > 0) {
                            String members = "";
                            for(ThreadMember tmember : data.getMembers()) {
                                if(!tmember.getXid().equals(MineTalkApp.getUserInfoModel().getUser_xid())) {
                                    //String contactName = mContactMap.get(tmember.getXid());
                                    String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());

                                    if(contactName == null || contactName.equals("")) {
                                        members = tmember.getNickName();
                                    } else {
                                        members = contactName;
                                    }
                                    break;
                                }
                            }
                            threadName = members;
                        }
                    } else {
                        if(data.getMembers().size() > 0) {
                            String members = "";
                            int count = 0;
                            for(ThreadMember tmember : data.getMembers()) {
                                //String contactName = mContactMap.get(tmember.getXid());
                                String contactName = MineTalkApp.getUserNameByPhoneNum(tmember.getNickName(), tmember.getUserHp());
                                if(contactName == null || contactName.equals("")) {
                                    members = members + tmember.getNickName();
                                } else {
                                    members = members + contactName;
                                }

                                if(count != (data.getMembers().size() - 1)) {
                                    members = members + ",";
                                }
                                count++;
                            }
                            threadName = members;
                        }
                    }

                    if(threadName.toLowerCase().contains(mSearchKeyword))
                        threadList.add(data);
                }

                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mListAdapter.setContactMap(mContactMap);
                        mGridAdapter.setContactMap(mContactMap);

                        Collections.sort(threadList, bDescOrder ? MineTalkApp.threadDataCmpDesc : MineTalkApp.threadDataCmpAsc);

                        mListAdapter.setData(threadList, ChatListAdapter.ChatHolderType.FRIEND_SEARCH);
                        mGridAdapter.setData(threadList, ChatListAdapter.ChatHolderType.FRIEND_SEARCH);

                        if (bGridType) { //GridType
                            mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                                @Override
                                public int getSpanSize(int position) {
                                    return position == 0 ? BaseFragment.mRowNum : 1;
                                }
                            });
                            mBinding.recyclerView.setAdapter(mGridAdapter);
                        } else {
                            mGridLayoutManager.setSpanCount(1);
                            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                                @Override
                                public int getSpanSize(int position) {
                                    return 1;
                                }
                            });
                            mBinding.recyclerView.setAdapter(mListAdapter);
                        }

                        String countMessage = String.format("채팅 %d건", threadList.size());
                        mBinding.tvBlockCount.setText(countMessage);
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                    }
                });
            }
        });
    }

    private void listAdMessageRequest(String type, int page) {
        mListAdMessageApi.execute(type, page);
    }

    private void setAdListData(AdListResponse response) {
//        if(mLockScrollView) {
//            mTotalCount = response.getTotal_cnt();
//            mDataCount = mDataCount + response.getAd_list().size();
//        } else {
//            mBinding.layoutAdListContainer.removeAllViews();
//            mTotalCount = response.getTotal_cnt();
//            mDataCount = mDataCount + response.getAd_list().size();
//        }

        for(int i = 0 ; i < response.getAd_list().size() ; i++) {
            AdListItemView itemView = new AdListItemView(getActivity());
            itemView.setData(mCurrentType, response.getAd_list().get(i));
            itemView.setOnEventListener(mAdChatListItemListener);
            //mBinding.layoutAdListContainer.addView(itemView);
        }

        mLockScrollView = false;
    }

    private OnAdChatItemListener mAdChatListItemListener = new OnAdChatItemListener() {
        @Override
        public void onItemClick(String type, AdListModel data) {
            if(type.equals(MineTalk.AD_LIST_TYPE_R)) {
                AdsMessageDetailActivity.startActivity(getActivity(), type, data.getAd_idx(), data.getUser_name());
            } else if(type.equals(MineTalk.AD_LIST_TYPE_S)) {

            }
        }
    };


    public void refresh(boolean bGridType) {
        try {
            chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid(), bGridType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 방에서 뒤로가기로 이탈 했을때 했을때
    private void outThreadRequest(String xid, String threadKey) {
        ProgressUtil.showProgress(getActivity());
        OutThreadApi outThreadApi = new OutThreadApi(getContext(), xid, threadKey);
        outThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(getActivity());
                        refresh(Preferences.getSearchFriendListType());
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
            }
        });
    }
}
