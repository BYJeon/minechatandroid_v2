package kr.co.minetalk.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.ChargeActivity;
import kr.co.minetalk.activity.ExchangeActivity;
import kr.co.minetalk.activity.PayActivity;
import kr.co.minetalk.activity.PointExchangeActivity;
import kr.co.minetalk.activity.PointReportActivity;
import kr.co.minetalk.activity.PointSendActivity;
import kr.co.minetalk.activity.SendPointUserActivity;
import kr.co.minetalk.databinding.FragmentTokenBinding;
import kr.co.minetalk.fragment.listener.OnMoreFragmentEventListener;
import kr.co.minetalk.utils.CommonUtils;

public class TokenFragment extends Fragment {
    private final int PERMISSON = 1;
    public static TokenFragment newInstance() {
        TokenFragment fragment = new TokenFragment();
        return fragment;
    }

    private FragmentTokenBinding mBinding;
    private OnMoreFragmentEventListener mFragmentListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_token, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initApi();
        setUIEventListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        setDefaultData();
    }

    private void initApi() {

    }

    private void setUIEventListener() {

        // 결제하기
        mBinding.tvButtonPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MineTalkApp.getCurrentActivity(), new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        PayActivity.startActivity(getActivity(), PointSendActivity.MODE_TOKEN);
                    }
                }

            }
        });

        // 선물하기
        mBinding.btnTokenSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendPointUserActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.POINT_TYPE_TOKEN);
            }
        });

        // Token 내역
        mBinding.btnTokenReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointReportActivity.startActivity(getActivity(), MineTalk.POINT_TYPE_TOKEN);
            }
        });

        mBinding.ivRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onRefresh();
                }
            }
        });

        mBinding.tvButtonExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ExchangeActivity.startActivity(getActivity());
                PointExchangeActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointReportActivity.startActivity(getActivity(), MineTalk.POINT_TYPE_TOKEN);
            }
        });
    }

    public void setOnFragmentListener(OnMoreFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    public void setDefaultData() {
        mBinding.tvToken.setText(CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getTotal_mine_token()));


        String delayToken = CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getLock_mine_token());
        String delayTokenString = String.format(getActivity().getResources().getString(R.string.more_fragment_delay_token), delayToken);
        mBinding.tvDelayToken.setText(delayTokenString);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PayActivity.startActivity(getActivity(), PointSendActivity.MODE_TOKEN);
                } else {
                    return;
                }
        }
    }
}
