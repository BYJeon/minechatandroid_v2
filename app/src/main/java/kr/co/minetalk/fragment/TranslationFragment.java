package kr.co.minetalk.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.CameraTranslactionActivity;
import kr.co.minetalk.activity.EditProfileActivity;
import kr.co.minetalk.activity.SelectLanguageActivity;
import kr.co.minetalk.activity.VoiceTranslationActivity;
import kr.co.minetalk.databinding.FragmentFriendBinding;
import kr.co.minetalk.databinding.FragmentTranslationBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;

public class TranslationFragment extends Fragment {
    private final int PERMISSON = 1;
    private CountryRepository mCountryRepository;

    public static TranslationFragment newInstance() {
        TranslationFragment fragment = new TranslationFragment();
        return fragment;
    }

    private FragmentTranslationBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_translation, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateTheme();
        setTranslationOption();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCountryRepository = CountryRepository.getInstance();
        setUIEventListener();
    }

    private void getCountryName(final ImageView iv, final TextView tv, final String lanCode, final String defaultLan){
        String languageName = "";
        int resID = -1;
        if(lanCode.equals("")) {
            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                languageName = mCountryRepository.getCountryModel(defaultLan).getCountryName();
            } else {
                languageName = mCountryRepository.getCountryModel(defaultLan).getCountryNameOther();
            }

            resID = CommonUtils.getResourceImage(MineTalkApp.getCurrentActivity(), mCountryRepository.getCountryModel(defaultLan).getFlag_image());
        } else {

            if(Preferences.getLanguage() != null && Preferences.getLanguage().equals("ko")) {
                languageName = mCountryRepository.getCountryModel(lanCode).getCountryName();
            } else {
                languageName = mCountryRepository.getCountryModel(lanCode).getCountryNameOther();
            }

            resID = CommonUtils.getResourceImage(MineTalkApp.getCurrentActivity(), mCountryRepository.getCountryModel(lanCode).getFlag_image());
        }

        tv.setText(languageName);
        iv.setImageResource(resID);
    }

    private void setTranslationOption(){
        String topLanguage = Preferences.getVoiceBottomLanguage();
        getCountryName(mBinding.ivTransSource, mBinding.tvTransSource, topLanguage, "ko");

        String bottomLanguage = Preferences.getVoiceTopLanguage();
        getCountryName(mBinding.ivTransTarget, mBinding.tvTransTarget, bottomLanguage, "en");


        topLanguage = Preferences.getCameraSourceCode();
        getCountryName(mBinding.ivTransImgSource, mBinding.tvTransImgSource, topLanguage, "ko");
        getCountryName(mBinding.ivTransCameraSource, mBinding.tvTransCameraSource, topLanguage, "ko");

        bottomLanguage = Preferences.getCameraTargetCode();
        getCountryName(mBinding.ivTransImgTarget, mBinding.tvTransImgTarget, bottomLanguage, "en");
        getCountryName(mBinding.ivTransCameraTarget, mBinding.tvTransCameraTarget, bottomLanguage, "en");
    }

    private void setUIEventListener() {
        mBinding.layoutButtonVoiceTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VoiceTranslationActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonCameraTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ){
                        ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        CameraTranslactionActivity.startActivity(getActivity());
                    }
                } else {
                    CameraTranslactionActivity.startActivity(getActivity());
                }
            }
        });

        mBinding.layoutButtonImageTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ){
                        ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        CameraTranslactionActivity.startActivity(getActivity(), true);
                    }
                } else {
                    CameraTranslactionActivity.startActivity(getActivity(), true);
                }


            }
        });

        mBinding.btnTransSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectLanguageActivity.startActivity(MineTalkApp.getCurrentActivity());
            }
        });
    }

    private void updateTheme(){

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            //mBinding.layoutButtonVoiceTrans.setBackgroundResource(R.drawable.bg_tran_bk);
            //mBinding.layoutButtonImageTrans.setBackgroundResource(R.drawable.bg_tran_bk);
            //mBinding.layoutButtonCameraTrans.setBackgroundResource(R.drawable.bg_tran_bk);

            mBinding.btnTransSet.setImageResource(R.drawable.icon_set_bk);
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            //mBinding.layoutButtonVoiceTrans.setBackgroundResource(R.drawable.bg_tran);
            //mBinding.layoutButtonImageTrans.setBackgroundResource(R.drawable.bg_tran);
            //mBinding.layoutButtonCameraTrans.setBackgroundResource(R.drawable.bg_tran);

            mBinding.btnTransSet.setImageResource(R.drawable.icon_set);
        }
    }

}
