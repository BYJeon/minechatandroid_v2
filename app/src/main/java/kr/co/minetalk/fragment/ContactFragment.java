package kr.co.minetalk.fragment;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.FriendProfileActivity;
import kr.co.minetalk.adapter.ContactGridViewAdapter;
import kr.co.minetalk.adapter.ContactListViewAdapter;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.FragmentContactBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.CustomScrollView;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.ContactListView;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class ContactFragment extends BaseFragment {

    private ContactListViewAdapter mListAdapter;
    private ContactGridViewAdapter mGridAdapter;
    private ContactListViewAdapter.ViewType mType = ContactListViewAdapter.ViewType.NORMAL;

    public static ContactFragment newInstance() {
        ContactFragment fragment = new ContactFragment();
        return fragment;
    }

    private ArrayList<ContactModel> mContactList = new ArrayList<>();
    private ArrayList<ContactModel> mContactSearchList = new ArrayList<>();
    private Map<String, String> mContactMap = new HashMap<>();

    private boolean bDescOrder = Preferences.getContactSettingOrder();
    private FragmentContactBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;


    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    public void setViewType(ContactListViewAdapter.ViewType type){
        this.mType = type;
    }

    public void setSearchContactList(ArrayList<ContactModel> list){
        this.mContactSearchList = list;
        setupContactListData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            updateTheme();
            mListAdapter.notifyDataSetChanged();
            mGridAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mListAdapter = new ContactListViewAdapter();
        mListAdapter.setFragmentListener(mFragmentListener);
        mListAdapter.setListener(mFriendItemViewListener);

        mListAdapter.setViewType(mType);
        mGridAdapter = new ContactGridViewAdapter();
        mGridAdapter.setFragmentListener(mFragmentListener);
        mGridAdapter.setListener(mFriendItemViewListener);
        mGridAdapter.setViewType(mType);

        mBinding.recyclerView.setAdapter(mListAdapter);
        setUIEventListener();
        setupContactListData();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void updateTheme(){
        if(MineTalk.isBlackTheme){
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top_bk);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom_bk);
        }else{
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom);
        }
    }

    private void setUIEventListener() {
        mBinding.btSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                mBinding.btSortList.setText(bDescOrder ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                setupContactListDataByOrder();
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.viewGradient.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);

                mBinding.viewGradient.setVisibility(View.INVISIBLE);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(mGridAdapter.getItemCount()-1);

                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });
    }


    @Override
    public void refreshView() {
        setupContactListData();
    }

    @Override
    public void refreshView(boolean bGridType) {
        setupContactListData(bGridType);
    }

    @Override
    public void refreshListType(boolean listType) {
        if (listType) { //GridType
            mLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            mLayoutManager.setSpanCount(1);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }

        mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
        mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);


    }

    // 내 연락처 리스트 정렬
    private void setupContactListDataByOrder() {
        Collections.sort(mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList, bDescOrder ? MineTalkApp.contactCmpDesc : MineTalkApp.contactCmpAsc);

        mListAdapter.setItem(mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList);
        mGridAdapter.setItem(mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList);

        refreshListType(mType == ContactListViewAdapter.ViewType.NORMAL ? Preferences.getFriendListType() : Preferences.getSearchFriendListType());
    }

    // 내 연락처 리스트 정렬
    private void setupContactListDataByOrder(boolean order) {
        Collections.sort(mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList, order ? MineTalkApp.contactCmpDesc : MineTalkApp.contactCmpAsc);

        mListAdapter.setItem(mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList);
        mGridAdapter.setItem(mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList);

        refreshListType(mType == ContactListViewAdapter.ViewType.NORMAL ? Preferences.getFriendListType() : Preferences.getSearchFriendListType());
    }

    // 내 연락처 리스트
    private void setupContactListData() {
        mContactList.clear();
        mContactMap.clear();

        ArrayList<ContactModel> contactList = MineTalkApp.getContactList();
        HashMap<String, String> firendListMap = MineTalkApp.getFriendListMap();

        for(int i = 0; i < contactList.size(); ++i) {
            ContactModel model = contactList.get(i);
            if(mContactMap.containsKey(model.getUser_phone())) continue;
            //친구는 연락처 리스트에서 제외
            if(!firendListMap.containsKey(model.getUser_phone())) {
                mContactList.add(model);
                mContactMap.put(model.getUser_phone(), model.getUser_name());
            }
        }

        mListAdapter.setItem(mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList);
        mGridAdapter.setItem(mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList);

        String countMessage = String.format("%s %d%s", MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_text_contact),
                mType ==  ContactListViewAdapter.ViewType.NORMAL ? mContactList.size() : mContactSearchList.size(),
                MineTalkApp.getCurrentActivity().getResources().getString(R.string.human_count));
        mBinding.tvContactCount.setText(countMessage);
    }

    // 내 연락처 리스트
    private void setupContactListData(boolean bGridType) {
        mContactList.clear();
        mContactMap.clear();

        ArrayList<ContactModel> contactList = MineTalkApp.getContactList();
        HashMap<String, String> firendListMap = MineTalkApp.getFriendListMap();

        for(int i = 0; i < contactList.size(); ++i) {
            ContactModel model = contactList.get(i);
            if(mContactMap.containsKey(model.getUser_phone())) continue;
            //친구는 연락처 리스트에서 제외
            if(!firendListMap.containsKey(model.getUser_phone())) {
                mContactList.add(model);
                mContactMap.put(model.getUser_phone(), model.getUser_name());
            }
        }

        String countMessage = String.format("연락처 %d명", 0);
        mContactList.addAll(MineTalkApp.getContactList());
        Collections.sort(mContactList, bDescOrder ? MineTalkApp.contactCmpDesc : MineTalkApp.contactCmpAsc);

        mListAdapter.setItem( mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList);
        mGridAdapter.setItem( mType == ContactListViewAdapter.ViewType.NORMAL ? mContactList : mContactSearchList);

        mListAdapter.setListener(mFriendItemViewListener);
        mGridAdapter.setListener(mFriendItemViewListener);


        refreshListType(bGridType);

        countMessage = String.format("%s %d%s", MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_text_contact),
                mType ==  ContactListViewAdapter.ViewType.NORMAL ? mContactList.size() : mContactSearchList.size(),
                MineTalkApp.getCurrentActivity().getResources().getString(R.string.human_count));
        mBinding.tvContactCount.setText(countMessage);
    }


    private OnFriendListViewListener mFriendItemViewListener = new OnFriendListViewListener() {
        @Override
        public void onClickItem(FriendListModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.showProfile(data, FriendProfileActivity.FriendType.CONTACT);
            }
        }

        @Override
        public void onClickFavorite(FriendListModel data) {

        }

        @Override
        public void addFriend(FriendListModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.addFriend(data);
            }
        }

        @Override
        public void showMyProfile(UserInfoBaseModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.showMyProfile();
            }
        }

        @Override
        public void createChatToMe() {

        }

        @Override
        public void editMyProfile() {

        }

        @Override
        public void refreshOrder(boolean bOrder) {
            setupContactListDataByOrder(bOrder);
        }
    };


}
