package kr.co.minetalk.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.ChargeActivity;
import kr.co.minetalk.activity.PayActivity;
import kr.co.minetalk.activity.PointReportActivity;
import kr.co.minetalk.activity.PointSendActivity;
import kr.co.minetalk.activity.SendPointUserActivity;
import kr.co.minetalk.databinding.FragmentCashBinding;
import kr.co.minetalk.fragment.listener.OnMoreFragmentEventListener;
import kr.co.minetalk.utils.CommonUtils;

public class CashFragment extends Fragment {
    private final int PERMISSON = 1;
    public static CashFragment newInstance() {
        CashFragment fragment = new CashFragment();
        return fragment;
    }

    private FragmentCashBinding mBinding;
    private OnMoreFragmentEventListener mFragmentListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_cash, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initApi();
        setUIEventListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void initApi() {

    }

    @Override
    public void onResume() {
        super.onResume();
        setDefaultData();
    }



    private void setUIEventListener() {
        // 결제하기
        mBinding.tvButtonPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Build.VERSION.SDK_INT >= 23) {
                    if(ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(MineTalkApp.getCurrentActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MineTalkApp.getCurrentActivity(), new String[] {Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSON);
                    } else {
                        PayActivity.startActivity(getActivity(), PointSendActivity.MODE_CASH);
                    }
                }

            }
        });

        // 선물하기
        mBinding.btnCashSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendPointUserActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalk.POINT_TYPE_CASH);
            }
        });

        // 충전하기
        mBinding.tvButtonCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChargeActivity.startActivity(getActivity(), ChargeActivity.POINT_TYPE_CASH);
            }
        });

        // 마인캐시 내역
        mBinding.btnCashReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointReportActivity.startActivity(getActivity(), MineTalk.POINT_TYPE_CASH);
            }
        });

        mBinding.ivRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onRefresh();
                }
            }
        });

        mBinding.layoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointReportActivity.startActivity(getActivity(), MineTalk.POINT_TYPE_CASH);
            }
        });


    }

    public void setOnFragmentListener(OnMoreFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    public void setDefaultData() {
        mBinding.tvCash.setText(CommonUtils.comma_won(MineTalkApp.getUserInfoModel().getUser_mine_cash()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PayActivity.startActivity(getActivity(), PointSendActivity.MODE_CASH);
                } else {
                    return;
                }
        }
    }

}
