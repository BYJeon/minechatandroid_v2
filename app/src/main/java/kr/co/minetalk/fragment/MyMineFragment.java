package kr.co.minetalk.fragment;

import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.BuildConfig;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.BuyCouponActivity;
import kr.co.minetalk.activity.ChatNormalSetActivity;
import kr.co.minetalk.activity.CreateMarketingChatActivity;
import kr.co.minetalk.activity.EditAccountActivity;
import kr.co.minetalk.activity.EditProfileActivity;
import kr.co.minetalk.activity.FriendManageActivity;
import kr.co.minetalk.activity.LimitCheckActivity;
import kr.co.minetalk.activity.MyQrActivity;
import kr.co.minetalk.activity.NextGearActivity;
import kr.co.minetalk.activity.NoticeActivity;
import kr.co.minetalk.activity.PointExchangeActivity;
import kr.co.minetalk.activity.PointReportActivity;
import kr.co.minetalk.activity.PointReportMoreActivity;
import kr.co.minetalk.activity.PointSaveActivity;
import kr.co.minetalk.activity.SaveHistoryActivity;
import kr.co.minetalk.activity.SelectNormalOptionActivity;
import kr.co.minetalk.activity.WalletActivity;
import kr.co.minetalk.adapter.MoreFragmentPagerAdapter;
import kr.co.minetalk.api.MiningInfoApi;
import kr.co.minetalk.api.NextGearYnApi;
import kr.co.minetalk.api.PointInfoApi;
import kr.co.minetalk.api.model.MiningInfoResponse;
import kr.co.minetalk.api.model.NextGearResponse;
import kr.co.minetalk.api.model.PointInfoResponse;
import kr.co.minetalk.databinding.FragmentMymineBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.fragment.listener.OnMoreFragmentEventListener;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;

public class MyMineFragment extends Fragment {

    private final int PERMISSON = 1;

    public static MyMineFragment newInstance() {
        MyMineFragment fragment = new MyMineFragment();
        return fragment;
    }

    private MiningInfoApi mMiningInfoApi;
    private NextGearYnApi mNextGearYnApi;
    private PointInfoApi mPointInfoApi;

    private FragmentMymineBinding mBinding;
    private OnFragmentEventListener mFragmentListener;
    private MoreFragmentPagerAdapter mFragmentAdapter;
    private int mCurrentViewPagerIndex = 0;
    private static final int NEXT_GEAR_NEED_CASH = 697000;
    private long mTotalCash = 0;
    private boolean isNextGearApply = false;
    private boolean isOverBalance = false;
    private String overBalaceMsg = "";

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_mymine, container, false);
        mBinding.setHandlers(this);
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateTheme();

        //사용자 마이닝 정보
        mMiningInfoApi.execute();

        //사용자 Cash 정보 요청
        mPointInfoApi.execute();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        refreshView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initApi();
        setUIEventListener();

        mBinding.viewPager.setClipToPadding(false);
        mBinding.viewPager.setPadding((int) CommonUtils.convertDpToPixel(15, getActivity()), 0, (int) CommonUtils.convertDpToPixel(15, getActivity()), 0);
        mBinding.viewPager.setPageMargin((int) CommonUtils.convertDpToPixel(17, getActivity()));
    }

    private void initApi() {

        mPointInfoApi = new PointInfoApi(getActivity(), new ApiBase.ApiCallBack<PointInfoResponse>() {
            @Override
            public void onPreparation() {
                try {
                    ProgressUtil.showProgress(getActivity());
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(int request_code, PointInfoResponse response) {
                try {
                    ProgressUtil.hideProgress(getActivity());
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }

                if(response != null) {
                    mTotalCash = Long.parseLong(response.getUser_mine_cash());
                    //넥스트 기어 신청 여부
                    mNextGearYnApi.execute();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {

            }

            @Override
            public void onCancellation() {

            }
        });

        mNextGearYnApi = new NextGearYnApi(getActivity(), new ApiBase.ApiCallBack<NextGearResponse>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(getActivity());
            }

            @Override
            public void onSuccess(int request_code, NextGearResponse nextGearResponse) {
                ProgressUtil.hideProgress(getActivity());
                if(nextGearResponse != null) {

                    isOverBalance = nextGearResponse.getNextgear_overbalance().toLowerCase().equals("y");
                    overBalaceMsg = nextGearResponse.getPopup_message();
                    isNextGearApply = nextGearResponse.getNextgear_apply_yn().toLowerCase().equals("y");

                    boolean isEnable = mTotalCash >= NEXT_GEAR_NEED_CASH && nextGearResponse.getNextgear_yn().toLowerCase().equals("y");
                    if(isNextGearApply)
                        isEnable = true;

                    mBinding.btnNextGear.setBackgroundResource( isEnable ? MineTalk.isBlackTheme ?
                            R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066 : MineTalk.isBlackTheme ? R.drawable.button_bg_4dcdb60c : R.drawable.button_bg_4d16c066);

                    mBinding.btnNextGear.setEnabled(isEnable);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(getActivity());
            }

            @Override
            public void onCancellation() {

            }
        });

        mMiningInfoApi = new MiningInfoApi(getActivity(), new ApiBase.ApiCallBack<MiningInfoResponse>() {
            @Override
            public void onPreparation() {
                //ProgressUtil.showProgress(getActivity());
            }

            @Override
            public void onSuccess(int request_code, MiningInfoResponse baseModel) {
                //ProgressUtil.hideProgress(getActivity());
                if(baseModel != null) {

                    String accumulateAmount = CommonUtils.comma_won(baseModel.getAccumulate_amount());
                    String limitAmount = CommonUtils.comma_won(baseModel.getLimit_amount());

                    mBinding.tvDepositAmount.setText(accumulateAmount);
                    mBinding.tvLimitAmount.setText(limitAmount);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                //ProgressUtil.hideProgress(getActivity());
            }

            @Override
            public void onCancellation() {

            }
        });
    }

    private void updateTheme(){
        String miningDate = Preferences.getMiningSaveDate(MineTalkApp.getUserInfoModel().getUser_xid());
        boolean isPossibleMining = false;
        if(miningDate.equals("")){
            isPossibleMining = true;
        }else{
            String curTime = CommonUtils.getKorTime("yyyy-MM-dd");
            long diffDate = CommonUtils.getDateDiff(miningDate, curTime);
            if(diffDate > 0)
                isPossibleMining = true;
        }

        isPossibleMining = true;
        mBinding.btnTodayDeposite.setEnabled(isPossibleMining);

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.bk_point_color));

            //제목 BG
            mBinding.layoutPointTitle.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.layoutAppInfoTitle.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.layoutPrivacyTitle.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.layoutCsTitle.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.layoutBusinessTitle.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));

            //포인트 정보
            mBinding.tvMyPoint.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvMyPoint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_point_bk, 0, 0, 0 );
            mBinding.tvMyPointHistory.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvMyPointHistory.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_history_bk, 0, 0, 0 );
            mBinding.tvExchange.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvExchange.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_exchange_bk, 0, 0, 0 );
            mBinding.tvWallet.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvWallet.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_wallet_bk, 0, 0, 0 );

            mBinding.layoutButtonPointInfo.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonPoint.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonExchange.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonWallet.setBackgroundColor(Color.TRANSPARENT);

            //앱 정보
            mBinding.tvAppVersion.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvAppVersion.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_version_bk, 0, 0, 0 );

            mBinding.tvNoticeTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvNoticeTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_notice_bk, 0, 0, 0 );

            mBinding.tvEventTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvEventTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_event_bk, 0, 0, 0 );

            mBinding.tvUpdateTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvUpdateTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_update_bk, 0, 0, 0 );

            mBinding.tvTermsTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvTermsTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_terms_bk, 0, 0, 0 );

            mBinding.layoutButtonAppInfo.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonNotice.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonTerms.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonEvent.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonUpdate.setBackgroundColor(Color.TRANSPARENT);

            //개인 정보
            mBinding.tvProfileTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvProfileTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_profile_bk, 0, 0, 0 );

            mBinding.tvAccountTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvAccountTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_account_setting_bk, 0, 0, 0 );

            mBinding.tvFriendManageTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvFriendManageTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_friend_bk, 0, 0, 0 );

            mBinding.tvFriendChatTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvFriendChatTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_chat_settings_bk, 0, 0, 0 );

            mBinding.tvRecommendManageTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvRecommendManageTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_recomm_bk, 0, 0, 0 );

            mBinding.tvMycontentsTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvMycontentsTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_contents_bk, 0, 0, 0 );

            mBinding.tvLogoutTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvLogoutTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_logout_bk, 0, 0, 0 );

            mBinding.layoutButtonEditProfile.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonFriendManage.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonChatManage.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonRecommendManage.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonMycontents.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonAccount.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonLogout.setBackgroundColor(Color.TRANSPARENT);

            //고객 센터
            mBinding.tvFaqTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvFaqTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_faq_bk, 0, 0, 0 );
            mBinding.tvQnaTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvQnaTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_qna_bk, 0, 0, 0 );

            mBinding.layoutButtonFaq.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonQna.setBackgroundColor(Color.TRANSPARENT);
            mBinding.layoutButtonMycontents.setBackgroundColor(Color.TRANSPARENT);

            //비즈니스 센터
            mBinding.tvMineAdTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvMineAdTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_ad_bk, 0, 0, 0 );
            mBinding.layoutButtonMineAd.setBackgroundColor(Color.TRANSPARENT);

            mBinding.btnQr.setImageResource(R.drawable.icon_qr_my_bk);
            mBinding.btnUserSet.setImageResource(R.drawable.icon_set_bk);


            mBinding.btnTodayDeposite.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnTodayDeposite.setBackgroundResource(isPossibleMining ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_4dcdb60c);

            mBinding.btnNextGear.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnNextGear.setBackgroundResource(R.drawable.button_bg_4dcdb60c);

            //mBinding.tvMiningInfoTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvDepositTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvDepositAmount.setTextColor(getResources().getColor(R.color.bk_point_color));

            mBinding.tvLimitTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tvLimitAmount.setTextColor(getResources().getColor(R.color.bk_point_color));

            //mBinding.layoutMiningMenu.setBackgroundResource(R.drawable.bg_round_272f39);
            mBinding.btnBuyCoupon.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnBuyCoupon.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnLimitView.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnLimitView.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnHistoryView.setBackgroundResource(R.drawable.button_bg_cdb60c);
            mBinding.btnHistoryView.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutMiningDivierView.setBackgroundColor(getResources().getColor(R.color.main_text_color));
        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvTitle.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.layoutPointTitle.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutAppInfoTitle.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutPrivacyTitle.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutCsTitle.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutBusinessTitle.setBackgroundColor(getResources().getColor(R.color.white_color));

            //포인트 정보
            mBinding.tvMyPoint.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvMyPoint.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_point, 0, 0, 0 );
            mBinding.tvMyPointHistory.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvMyPointHistory.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_history, 0, 0, 0 );
            mBinding.tvExchange.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvExchange.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_ex, 0, 0, 0 );
            mBinding.tvWallet.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvWallet.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_wallet, 0, 0, 0 );

            mBinding.layoutButtonPointInfo.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonPoint.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonExchange.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonWallet.setBackgroundColor(getResources().getColor(R.color.white_color));

            //앱 정보
            mBinding.tvAppVersion.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAppVersion.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_ver, 0, 0, 0 );

            mBinding.tvNoticeTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvNoticeTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_notification, 0, 0, 0 );

            mBinding.tvEventTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvEventTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_event, 0, 0, 0 );

            mBinding.tvUpdateTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvUpdateTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_update, 0, 0, 0 );

            mBinding.tvTermsTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvTermsTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_terms, 0, 0, 0 );

            mBinding.layoutButtonAppInfo.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonNotice.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonTerms.setBackgroundColor(getResources().getColor(R.color.white_color));

            //개인 정보
            mBinding.tvProfileTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvProfileTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_profile, 0, 0, 0 );

            mBinding.tvAccountTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvAccountTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_account_setting, 0, 0, 0 );

            mBinding.tvFriendManageTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvFriendManageTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_fr, 0, 0, 0 );

            mBinding.tvFriendChatTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvFriendChatTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_chat_settings, 0, 0, 0 );

            mBinding.tvRecommendManageTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvRecommendManageTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_re, 0, 0, 0 );

            mBinding.tvMycontentsTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvMycontentsTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_my, 0, 0, 0 );

            mBinding.tvLogoutTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvLogoutTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_logout, 0, 0, 0 );

            mBinding.layoutButtonEditProfile.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonFriendManage.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonChatManage.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.layoutButtonRecommendManage.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonMycontents.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonAccount.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonLogout.setBackgroundColor(getResources().getColor(R.color.white_color));

            //고객 센터
            mBinding.tvFaqTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvFaqTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_faq, 0, 0, 0 );
            mBinding.tvQnaTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvQnaTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_qna, 0, 0, 0 );

            mBinding.layoutButtonFaq.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutButtonQna.setBackgroundColor(getResources().getColor(R.color.white_color));

            //비즈니스 센터
            mBinding.tvMineAdTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvMineAdTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_ad, 0, 0, 0 );
            mBinding.layoutButtonMineAd.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.btnQr.setImageResource(R.drawable.icon_qr_my);
            mBinding.btnUserSet.setImageResource(R.drawable.icon_set);

            mBinding.btnTodayDeposite.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnTodayDeposite.setBackgroundResource(isPossibleMining ? R.drawable.button_bg_16c066 : R.drawable.button_bg_4d16c066);

            mBinding.btnNextGear.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnNextGear.setBackgroundResource(R.drawable.button_bg_4d16c066);

            //mBinding.tvMiningInfoTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            mBinding.tvDepositTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvDepositAmount.setTextColor(getResources().getColor(R.color.main_text_color));

            mBinding.tvLimitTitle.setTextColor(getResources().getColor(R.color.bk_main_text_nor_color));
            mBinding.tvLimitAmount.setTextColor(getResources().getColor(R.color.main_text_color));

            //mBinding.layoutMiningMenu.setBackgroundResource(R.drawable.bg_round_f9f9f9);

            mBinding.btnBuyCoupon.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnBuyCoupon.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnLimitView.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnLimitView.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnHistoryView.setBackgroundResource(R.drawable.button_bg_16c066);
            mBinding.btnHistoryView.setTextColor(getResources().getColor(R.color.white_color));

            mBinding.layoutMiningDivierView.setBackgroundColor(getResources().getColor(R.color.main_theme_bg));
        }
    }

    private void setUIEventListener() {

        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCurrentViewPagerIndex = position;
//                if (position == 0) {
//                    mBinding.ivIndigator1.setSelected(true);
//                    mBinding.ivIndigator2.setSelected(false);
//                } else if(position == 1) {
//                    mBinding.ivIndigator1.setSelected(false);
//                    mBinding.ivIndigator2.setSelected(true);
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mBinding.layoutButtonEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditProfileActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoticeActivity.startActivity(getActivity());
            }
        });


        mBinding.btnQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyQrActivity.startActivity(getActivity());
            }
        });

        mBinding.btnUserSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectNormalOptionActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonFriendManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendManageActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonChatManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatNormalSetActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PointReportActivity.startActivity(getActivity(), MineTalk.POINT_TYPE_TOKEN);
            }
        });

        mBinding.layoutButtonPointInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PointReportMoreActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PointExchangeActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WalletActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onFaq();
                }
            }
        });

        mBinding.layoutButtonQna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onQna();
                }
            }
        });

        mBinding.layoutButtonRecommendManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onRecommendManager();
                }
            }
        });

        mBinding.layoutButtonTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onPolicy();
                }
            }
        });

        mBinding.layoutButtonEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onEvent();
                }
            }
        });

        mBinding.layoutButtonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.onUpdate();
                }
            }
        });

        //로그 아웃
        mBinding.layoutButtonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFragmentListener != null) {
                    mFragmentListener.logout();
                }
            }
        });

        mBinding.layoutButtonMineAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateMarketingChatActivity.startActivity(getActivity());
            }
        });

        mBinding.layoutButtonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditAccountActivity.startActivity(getActivity());
            }
        });

        //넥스트기어
        mBinding.btnNextGear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isOverBalance && !isNextGearApply){
                    if(mFragmentListener != null) {
                        mFragmentListener.showMessagePopup(overBalaceMsg);
                    }
                }else
                    NextGearActivity.startActivity(getActivity(), mTotalCash);

                //NextGearActivity.startActivity(getActivity());
            }
        });

        //오늘적립하기
        mBinding.btnTodayDeposite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PointSaveActivity.startActivity(getActivity());
            }
        });

        //쿠폰 구매
        mBinding.btnBuyCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //BuyCouponActivity.startActivity(getActivity());
                if(mFragmentListener != null) {
                    mFragmentListener.reqBuyCoupon();
                }
            }
        });

        //한도 조회
        mBinding.btnLimitView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LimitCheckActivity.startActivity(getActivity());
                //((MainActivity)MineTalkApp.getmMainContext()).getOnFragmentEventListner().onShowMessagePopupByLimitCheck();
            }
        });

        //적립 이력
        mBinding.btnHistoryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveHistoryActivity.startActivity(getActivity());
                //((MainActivity)MineTalkApp.getmMainContext()).getOnFragmentEventListner().onShowMessagePopupBySaveHistory();
            }
        });
    }

    private void setAppVersionInfomation() {
        String version = BuildConfig.VERSION_NAME;
        mBinding.tvAppVersion.setText(String.format("%s %s", getString(R.string.more_fragment_button_version) , version));
    }

    public void refreshView() {
        try {
            mFragmentAdapter = new MoreFragmentPagerAdapter(getChildFragmentManager(), mOnMoreFragmentEventListener);
            mBinding.viewPager.setAdapter(mFragmentAdapter);

            mBinding.viewPager.setCurrentItem(mCurrentViewPagerIndex);
            //setupUserInfomation();
            //setupMessageSwitch();
            setAppVersionInfomation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSON :
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    return;
                }
        }
    }

    private OnMoreFragmentEventListener mOnMoreFragmentEventListener = new OnMoreFragmentEventListener() {
        @Override
        public void onRefresh() {
            if(mFragmentListener != null) {
                mFragmentListener.onRefresh();
            }
        }
    };



}
