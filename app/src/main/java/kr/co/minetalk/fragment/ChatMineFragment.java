package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.AdsMessageDetailActivity;
import kr.co.minetalk.adapter.ChatAdGridAdapter;
import kr.co.minetalk.adapter.ChatAdListAdapter;
import kr.co.minetalk.adapter.ChatGridAdapter;
import kr.co.minetalk.adapter.ChatListAdapter;
import kr.co.minetalk.api.ListAdMessageApi;
import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.api.model.AdListResponse;
import kr.co.minetalk.databinding.FragmentChatGroupBinding;
import kr.co.minetalk.databinding.FragmentChatMineBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.base.ChatBaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.message.api.AlarmThreadApi;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.OutThreadApi;
import kr.co.minetalk.message.api.ThreadListApi;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.ui.listener.OnAdChatItemListener;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.ProgressUtil;
import kr.co.minetalk.view.AdListItemView;
import kr.co.minetalk.view.listener.OnFriendListViewOrderListener;

public class ChatMineFragment extends ChatBaseFragment {
    public static ChatMineFragment newInstance() {
        ChatMineFragment fragment = new ChatMineFragment();
        return fragment;
    }

    private FragmentChatMineBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;

    private GridLayoutManager mGridLayoutManager;
    private ChatAdListAdapter mListAdapter;
    private ChatAdGridAdapter mGridAdapter;

    private ListAdMessageApi mListAdMessageApi;

    private String mCurrentType;
    private HashMap<String, String> mContactMap = new HashMap<>();

    // Paging Variable /////////////////////
    private int mPage               = 1;
    private int mTotalCount         = 0;
    private int mDataCount          = 0;
    private boolean mLockScrollView = false;
    ////////////////////////////////////////

    private Parcelable recyclerViewState;

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    public void setOnContactMap(HashMap<String, String> contactMap){
        this.mContactMap = contactMap;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_mine, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        updateTheme();
        mListAdapter.notifyDataSetChanged();
        mGridAdapter.notifyDataSetChanged();
        //refreshChatList();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initApi();
        setUIEventListener();

        mListAdapter = new ChatAdListAdapter();
        //mListAdapter.setAdapterListener(mFriendBlockAdapterListener);

        mGridAdapter = new ChatAdGridAdapter();
        //mGridAdapter.setAdapterListener(mFriendBlockAdapterListener);

        mGridLayoutManager = new GridLayoutManager(getContext(), 1);
        mBinding.recyclerView.setLayoutManager(mGridLayoutManager);
        mBinding.recyclerView.setAdapter(mListAdapter);

        String countMessage = String.format("마인 채팅 %d건", 0);
        mBinding.tvBlockCount.setText(countMessage);

        initializePagingInfo();
    }

    @Override
    public void refreshView() {
        refresh();
    }

    @Override
    public void refreshView(boolean bGridType) {
        refresh();
    }

    @Override
    public void refreshListType(boolean listType) {
        //boolean type = Preferences.getChatListType();

        if (listType) { //GridType
            mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            mGridLayoutManager.setSpanCount(1);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }

        mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
        mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
    }

    @Override
    public void refreshChatList() {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initApi() {
        mListAdMessageApi = new ListAdMessageApi(getActivity(), new ApiBase.ApiCallBack<AdListResponse>() {
            @Override
            public void onPreparation() {
                //ProgressUtil.showProgress(getActivity());
            }

            @Override
            public void onSuccess(int request_code, AdListResponse adListResponse) {
                ProgressUtil.hideProgress(getActivity());
                if(adListResponse != null) {
                    setAdListData(adListResponse);
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(getActivity());
            }

            @Override
            public void onCancellation() {
                ProgressUtil.hideProgress(getActivity());
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.viewGradient.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);

                    if(mTotalCount != 0 && mDataCount < mTotalCount) {
                        if (mLockScrollView == false) { // 스크롤 bottom
                            mLockScrollView = true;
                            mPage = mPage + 1;

                            listAdMessageRequest(mCurrentType, mPage);
                        }
                    }
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);

                mBinding.viewGradient.setVisibility(View.INVISIBLE);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bListType){ // Grid Type
                    mBinding.recyclerView.scrollToPosition(mGridAdapter.getItemCount()-1);
                }else{
                    mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                }

                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);

                if(mTotalCount != 0 && mDataCount < mTotalCount) {
                    if (mLockScrollView == false) { // 스크롤 bottom
                        mLockScrollView = true;
                        mPage = mPage + 1;

                        listAdMessageRequest(mCurrentType, mPage);
                    }
                }
            }
        });
    }

//    private void setNormalChat() {
//        mBinding.tvButtonNormal.setSelected(true);
//        mBinding.tvButtonAd.setSelected(false);
//
//        mBinding.tvButtonAd.setTextColor(Color.parseColor("#9b9b9b"));
//        mBinding.tvButtonNormal.setTextColor(Color.parseColor("#FFFFFF"));
//
//        mBinding.layoutAdRoot.setVisibility(View.GONE);
//    }
//
//    private void setAdChat() {
//        mBinding.tvButtonNormal.setSelected(false);
//        mBinding.tvButtonAd.setSelected(true);
//
//        mBinding.tvButtonNormal.setTextColor(Color.parseColor("#9b9b9b"));
//        mBinding.tvButtonAd.setTextColor(Color.parseColor("#FFFFFF"));
//
//        mBinding.layoutAdRoot.setVisibility(View.VISIBLE);
//
////        mBinding.tvTabLeft.setTextColor(Color.parseColor("#FFFFFF"));
////        mBinding.tvTabRight.setTextColor(Color.parseColor("#002169"));
////        mBinding.tvTabLeft.setSelected(true);
////        mBinding.tvTabRight.setSelected(false);
//
//
//
//        mBinding.layoutAdListContainer.removeAllViews();
//        initializePagingInfo();
//        mBinding.tvTabRight.setTextColor(Color.parseColor("#FFFFFF"));
//        mBinding.tvTabLeft.setTextColor(Color.parseColor("#002169"));
//        mBinding.tvTabLeft.setSelected(false);
//        mBinding.tvTabRight.setSelected(true);
//
//        mCurrentType = MineTalk.AD_LIST_TYPE_R;
//        listAdMessageRequest(mCurrentType, mPage);
//    }

    private void initializePagingInfo() {
        mTotalCount     = 0;
        mDataCount      = 0;
        mPage           = 1;
        mLockScrollView = false;
        bDescOrder      = Preferences.getMineChatSettingOrder();
        mBinding.ibSortList.setText(bDescOrder ? "가↑" : "가↓");

        listAdMessageRequest(MineTalk.AD_LIST_TYPE_R, mPage);
    }

    private void setUIEventListener() {
        mBinding.ibSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                mBinding.ibSortList.setText(bDescOrder ? "가↑" : "가↓");
                refreshView();
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });
    }


    private void updateTheme(){
        if(MineTalk.isBlackTheme){
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top_bk);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom_bk);
        }else{
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom);
        }
    }

    private void chatListRequest(String user_xid) {
//        ThreadListApi threadListApi = new ThreadListApi(MineTalkApp.getAppContext(), user_xid);
//        threadListApi.request(new BaseHttpRequest.APICallbackListener<List<ThreadData>>() {
//            @Override
//            public void onSuccess(List<ThreadData> res) {
//
//
//                ArrayList<ThreadData> threadList = new ArrayList<>();
//                for(ThreadData data : res) {
//                    //그룹 채팅만 필터링 한다.
//                    //if(!data.getIsDirect().toUpperCase().equals("Y"))
//                    //    threadList.add(data);
//                }
//
//                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        mListAdapter.setContactMap(mContactMap);
//                        mGridAdapter.setContactMap(mContactMap);
//
//                        //정렬
//                        Collections.sort(threadList, bDescOrder ? MineTalkApp.threadDataCmpDesc : MineTalkApp.threadDataCmpAsc);
//
//                        mListAdapter.setData(threadList);
//                        mGridAdapter.setData(threadList);
//
//
//                        boolean listType = Preferences.getChatListType();
//
//                        if(listType) { //GridType
//                            mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
//                            mBinding.recyclerView.setAdapter(mGridAdapter);
//                        }else{
//                            mGridLayoutManager.setSpanCount(1);
//                            mBinding.recyclerView.setAdapter(mListAdapter);
//                        }
//
//                        String countMessage = String.format("마인 채팅 %d건", threadList.size());
//                        mBinding.tvBlockCount.setText(countMessage);
//                    }
//                });
//
//            }
//
//            @Override
//            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
//                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
//                    }
//                });
//            }
//        });
    }

    private void listAdMessageRequest(String type, int page) {
        mListAdMessageApi.execute(type, page);
    }

    private void setAdListData(AdListResponse response) {
        if(mLockScrollView) {
            mTotalCount = response.getTotal_cnt();
            mDataCount = mDataCount + response.getAd_list().size();
        } else {
            mTotalCount = response.getTotal_cnt();
            mDataCount = mDataCount + response.getAd_list().size();
        }

//        for(int i = 0 ; i < response.getAd_list().size() ; i++) {
//            AdListItemView itemView = new AdListItemView(getActivity());
//            itemView.setData(mCurrentType, response.getAd_list().get(i));
//            itemView.setOnEventListener(mAdChatListItemListener);
//            mBinding.layoutAdListContainer.addView(itemView);
//        }

        recyclerViewState = mBinding.recyclerView.getLayoutManager().onSaveInstanceState();

        ArrayList<AdListModel> adList = response.getAd_list();

        //정렬
        Collections.sort(adList, bDescOrder ? MineTalkApp.adCmpDesc : MineTalkApp.adCmpAsc);

        mListAdapter.setData(adList);
        mListAdapter.setAdapterListener(mFriendListViewOrderListener);
        mGridAdapter.setData(adList);
        mGridAdapter.setAdapterListener(mFriendListViewOrderListener);

        boolean listType = Preferences.getChatListType();

        if (listType) { //GridType
            mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            mGridLayoutManager.setSpanCount(1);
            mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }

        mBinding.recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);


        mLockScrollView = false;
    }

    private OnFriendListViewOrderListener mFriendListViewOrderListener = new OnFriendListViewOrderListener() {
        @Override
        public void refreshListType(boolean bGridType) {
            boolean listType = Preferences.getSelectFriendListType();

            recyclerViewState = mBinding.recyclerView.getLayoutManager().onSaveInstanceState();

            if (listType) { //GridType
                mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return position == 0 ? BaseFragment.mRowNum : 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mGridAdapter);
            } else {
                mGridLayoutManager.setSpanCount(1);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mListAdapter);
            }

            mBinding.recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);

        }

        @Override
        public void refreshOrder(boolean bOrder) {
            bDescOrder = bOrder;
            refresh();
        }
    };

    private OnAdChatItemListener mAdChatListItemListener = new OnAdChatItemListener() {
        @Override
        public void onItemClick(String type, AdListModel data) {
            if(type.equals(MineTalk.AD_LIST_TYPE_R)) {
                AdsMessageDetailActivity.startActivity(getActivity(), type, data.getAd_idx(), data.getUser_name());
            } else if(type.equals(MineTalk.AD_LIST_TYPE_S)) {

            }
        }
    };


    public void refresh() {
        try {
            //chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());

            List<AdListModel> adList = mListAdapter.getData();
            if(adList == null) return;

            recyclerViewState = mBinding.recyclerView.getLayoutManager().onSaveInstanceState();

            //정렬
            Collections.sort(adList, bDescOrder ? MineTalkApp.adCmpDesc : MineTalkApp.adCmpAsc);

            boolean listType = Preferences.getChatListType();

            if (listType) { //GridType
                mGridLayoutManager.setSpanCount(BaseFragment.mRowNum);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return position == 0 ? BaseFragment.mRowNum : 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mGridAdapter);
            } else {
                mGridLayoutManager.setSpanCount(1);
                mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return 1;
                    }
                });
                mBinding.recyclerView.setAdapter(mListAdapter);
            }

            mBinding.recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);


            mLockScrollView = false;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // 방에서 뒤로가기로 이탈 했을때 했을때
    private void outThreadRequest(String xid, String threadKey) {
        ProgressUtil.showProgress(getActivity());
        OutThreadApi outThreadApi = new OutThreadApi(getContext(), xid, threadKey);
        outThreadApi.request(new BaseHttpRequest.APICallbackListener<Void>() {
            @Override
            public void onSuccess(Void res) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProgressUtil.hideProgress(getActivity());
                        refresh();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
            }
        });
    }
}
