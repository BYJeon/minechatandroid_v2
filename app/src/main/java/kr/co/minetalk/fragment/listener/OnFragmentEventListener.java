package kr.co.minetalk.fragment.listener;

import kr.co.minetalk.activity.FriendProfileActivity;
import kr.co.minetalk.api.model.FriendListModel;

public interface OnFragmentEventListener {
    public void reqBuyCoupon();
    public void logout();
    public void withDrawal();
    public void showMessagePopup(String message);
    public void changeReceiveMessageFlag(String flag);
    public void showProfile(FriendListModel data);
    public void showProfile(FriendListModel data, FriendProfileActivity.FriendType type);
    public void selectLanguage();
    public void makeChatting();
    public void searchChat();
    public void addFriend(FriendListModel data);
    public void showMyProfile();
    public void makeChattingToMe();
    public void editProfile();
    public void onRefresh();

    public void onFaq();
    public void onQna();
    public void onRecommendManager();
    public void onPolicy();
    public void onChangeTheme();

    public void onEvent();
    public void onUpdate();

    public void onUpdateFriendList();
    public void onSendSMS(String toName, String toPhoneNo);
    public void onUpdateBottomMenuNotification(int notiCnt, int index);

    public void onShowMessagePopupByLimitCheck();
    public void onShowMessagePopupBySaveHistory();
}
