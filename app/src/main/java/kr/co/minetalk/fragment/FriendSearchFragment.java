package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.Collections;

import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.FriendProfileActivity;
import kr.co.minetalk.activity.MainActivity;
import kr.co.minetalk.adapter.FriendGridAdapter;
import kr.co.minetalk.adapter.FriendListAdapter;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.FragmentFriendSearchBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.CustomScrollView;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.FriendListView;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class FriendSearchFragment extends BaseFragment {
    private FriendListAdapter mListAdapter;
    private FriendGridAdapter mGridAdapter;

    public static FriendSearchFragment newInstance() {
        FriendSearchFragment fragment = new FriendSearchFragment();
        return fragment;
    }

    private boolean bDescOrder = Preferences.getFriendSettingOrder();
    private FragmentFriendSearchBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend_search, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);

        mListAdapter = new FriendListAdapter();
        mGridAdapter = new FriendGridAdapter();

        mBinding.recyclerView.setAdapter(mListAdapter);

        setUIEventListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void setUIEventListener() {
        mBinding.btSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                mBinding.btSortList.setText(bDescOrder ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                refreshView();
            }
        });


        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.viewGradient.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);

                mBinding.viewGradient.setVisibility(View.INVISIBLE);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean bListType = Preferences.getFriendListType();
                if(bListType){ // Grid Type
                    mBinding.recyclerView.scrollToPosition(mGridAdapter.getItemCount()-1);
                }else{
                    mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                }

                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void refreshView() {
        try {
            refreshSearchList(bListType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refreshView(boolean bGridType) {
        try {
            bListType = bGridType;
            refreshSearchList(bGridType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refreshListType(boolean listType) {
        refreshSearchList(listType);
    }

    private void refreshSearchList(boolean bGridType){

        if (bGridType) { //GridType

            Collections.sort(mGridAdapter.getItem(), bDescOrder ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);

            mLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            Collections.sort(mListAdapter.getItem(), bDescOrder ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);

            mLayoutManager.setSpanCount(1);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }

        mListAdapter.notifyDataSetChanged();
        mGridAdapter.notifyDataSetChanged();

        mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
        mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
    }

    // 친구 리스트 Data Settting
    public void setupFriendListData(ArrayList<FriendListModel> items, boolean listType) {

        if(mBinding == null) return;
        ArrayList<FriendListModel> friends = items;

        String countMessage = String.format("친구 %d명", friends.size());
        mBinding.tvFriendCount.setText(countMessage);

        Collections.sort(friends, bDescOrder ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);

        FriendListModel model;
        String contactName = "";


        for( int i = 0; i < friends.size(); ++i){
            model = friends.get(i);
            contactName = MineTalkApp.getUserNameByPhoneNum(model.getUser_name(), model.getUser_hp().replace("-", ""));
            if(contactName != null && contactName.length() > 0)
                model.setUser_name(contactName);
        }

        Collections.sort(friends, bDescOrder ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);

        mListAdapter.setListener(mFriendItemViewListener);
        mListAdapter.setItem(friends);

        mGridAdapter.setListener(mFriendItemViewListener);
        mGridAdapter.setItem(friends);


        mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
        mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);

        refreshListType(listType);
    }

    private OnFriendListViewListener mFriendItemViewListener = new OnFriendListViewListener() {
        @Override
        public void onClickItem(FriendListModel data) {
            OnFragmentEventListener profileListener = ((MainActivity)MineTalkApp.getmMainContext()).getOnFragmentEventListner();
            if(profileListener != null) {
                profileListener.showProfile(data, FriendProfileActivity.FriendType.FRIEND_OTHER);
            }
        }

        @Override
        public void onClickFavorite(FriendListModel data) {

        }

        @Override
        public void addFriend(FriendListModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.addFriend(data);
            }
        }

        @Override
        public void showMyProfile(UserInfoBaseModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.showMyProfile();
            }
        }

        @Override
        public void createChatToMe() {

        }

        @Override
        public void editMyProfile() {

        }

        @Override
        public void refreshOrder(boolean bOrder) {
            bDescOrder = bOrder;
            refreshView();
        }
    };

}