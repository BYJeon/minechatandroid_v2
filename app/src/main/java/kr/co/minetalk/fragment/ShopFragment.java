package kr.co.minetalk.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.Browser;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.URISyntaxException;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.databinding.FragmentShopBinding;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.utils.Sha256Util;

public class ShopFragment extends Fragment {
    public static ShopFragment newInstance() {
        ShopFragment fragment = new ShopFragment();
        return fragment;
    }

    private FragmentShopBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;

    private WebSettings mWebSettings;

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUIEventListener();

        mWebSettings = mBinding.webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setLoadWithOverviewMode(true);
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setSupportZoom(true);
        mWebSettings.setBuiltInZoomControls(false);
        mWebSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings.setAllowFileAccess(false);

        mBinding.webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mBinding.webView.setScrollbarFadingEnabled(true);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mBinding.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            mBinding.webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        // android 5.0부터 앱에서 API수준21이상을 타겟킹하는 경우 아래추가
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBinding.webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(mBinding.webView, true);
        }

        mBinding.webView.setWebViewClient(new CustomWebViewClient());
        mBinding.webView.setWebChromeClient(new CustomWebChromeClient());

        String auth = "";

        try {
            String userType = Preferences.getLoginType();
            String shop_id  = MineTalkApp.getUserInfoModel().getShop_id();

            if(userType.equals(MineTalk.LOGIN_TYPE_GOOGLE) || userType.equals(MineTalk.LOGIN_TYPE_FACEBOOK) || userType.equals(MineTalk.LOGIN_TYPE_KAKAO)){
                String userPassword = "";
                String provider_id = MineTalkApp.getUserInfoModel().getUser_provider_id();
                auth = CommonUtils.makeShopAuthorization(shop_id, provider_id);
            }else{
                String userPassword = Preferences.getUserPassword();
                auth = CommonUtils.makeShopAuthorization(shop_id, Sha256Util.encoding(userPassword));
            }

//            if(userType.equals("A") || userType.equals(MineTalk.LOGIN_TYPE_IP)) {
//                String userPassword = Preferences.getUserPassword();
//                auth = CommonUtils.makeShopAuthorization(shop_id, Sha256Util.encoding(userPassword));
//            } else {
//                String userPassword = "";
//                String provider_id = MineTalkApp.getUserInfoModel().getUser_provider_id();
//                auth = CommonUtils.makeShopAuthorization(shop_id, provider_id);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.e("@@@TAG","user auth : " + auth);
        mBinding.webView.loadUrl(MineTalk.getShopServer() + "?shop_auth=" + auth);
    }

    private void setUIEventListener() {
        mBinding.layoutWebProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    class CustomWebViewClient extends WebViewClient {
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return loadPage(view, url);
//            return super.shouldOverrideUrlLoading(view, url);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            return loadPage(view, url);
//            return super.shouldOverrideUrlLoading(view, request);
        }

        @Nullable
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            return super.shouldInterceptRequest(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mBinding.layoutWebProgress.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mBinding.layoutWebProgress.setVisibility(View.GONE);
        }

        private boolean loadPage(WebView view, String url) {
            Context context = view.getContext();
            if(context == null) {
                context = getActivity();
            }

            Log.e("LOG.. CALL URL ==>", url);

            /* ISP(비씨, 국민) 인증 어플 연동 */
            if (url.startsWith("ispmobile")) {
                Log.e("LOG..ISP URL ==>", url);
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                try {
                    startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException ex) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mobile.vpay.co.kr/jsp/MISP/andown.jsp"));
                    startActivity(intent);
                }
                return true;
            }else if (url.contains("market://") ||
                    url.endsWith(".apk") ||
                    url.contains("http://market.android.com") ||
                    url.contains("vguard") ||
                    url.contains("v3mobile") ||
                    url.contains("ansimclick") ||
                    url.contains("smhyundaiansimclick://") ||
                    url.contains("smshinhanansimclick://") ||
                    url.contains("smshinhancardusim://") ||
                    url.contains("hanaansim://") ||
                    url.contains("citiansimmobilevaccine://") ||
                    url.contains("droidxantivirus") ||
                    url.contains("market://details?id=com.shcard.smartpay") ||
                    url.contains("shinhan-sr-ansimclick://") ||
                    url.contains("lottesmartpay") ||
                    url.contains("com.ahnlab.v3mobileplus") ||
                    url.contains("v3mobile") ||
                    url.contains("mvaccine") ||
                    url.contains("http://m.ahnlab.com/kr/site/download") ||
                    url.contains("com.lotte.lottesmartpay") ||
                    url.contains("com.lcacApp") ||
                    url.contains("lotteappcard://") ||
                    url.contains("eftpay") ||
                    url.contains("cloudpay")) {
                Log.e("LOG.. app URL ==>", url);
                return callApp(context, url);
            }else if (url.startsWith("http://") || url.startsWith("https://")) {
                Log.e("LOG.. http URL ==>", url);
                view.loadUrl(url);
                return true;
            }else if (url.startsWith("smartxpay-transfer://")) {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                try {
                    startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException ex) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=kr.co.uplus.ecredit"));
                    startActivity(intent);
                }
                return true;
            } else if (url.startsWith("smartxpay-transfer://")) {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                try {
                    startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException ex) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=kr.co.uplus.ecredit"));
                    startActivity(intent);
                }
                return true;
            } else if (url.startsWith("lguthepay")) {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException ex) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.lguplus.paynow"));
                    startActivity(intent);
                }
                return true;
            } else {
                Log.e("LOG.. else url ==>", url);
                return callApp(context, url);
            }
        }


        //외부앱호출
        public boolean callApp(Context context, String url) {
            Intent intent = null;
            try {
                Log.e("LOG.. callApp ==>", url);
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                Log.e("LOG.. get Scheme ==>", intent.getScheme());
                Log.e("LOG.. get Scheme ==>", intent.getDataString());

            } catch (URISyntaxException ex) {
                Log.e("LOG.. Browser", "Bad URI " + url + ":" + ex.getMessage());
                return false;
            }
            try {
                //국민앱 통합 17.05.18 변경
                if (url.contains("kb-acp")) {
                    Log.e("LOG..kb-acp ==>", url);
                    String srCode = null;
                    try {
                        Uri uri = Uri.parse(url);
                        srCode = uri.getQueryParameter("srCode");
                        Intent intent2 = new Intent(Intent.ACTION_VIEW);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Uri data = Uri.parse("kb-acp://pay?srCode=" + srCode + "&kb-acp://");
                        intent2.setData(data);
                        startActivity(intent2);
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            Intent intent3 = new Intent(Intent.ACTION_VIEW);
                            intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent3.setData(Uri.parse("market://details?id=com.kbcard.kbkookmincard"));
                            startActivity(intent3);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    return true;
                }
                //crome버전방식 2014.4 추가 (킷캣버전이후)
                if (url.startsWith("intent")) {
                    Log.e("LOG.. intent ==>", url);
                    if (context.getPackageManager().resolveActivity(intent, 0) == null) {
                        String packagename = intent.getPackage();
                        Log.e("LOG.. packagename  ==>", packagename);
                        if (packagename != null) {
                            Uri uri = Uri.parse("market://search?q=pname:" + packagename);
                            intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                            return true;
                        }
                    }else {
                        Log.e("LOG.. intent2 ==>", url);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setComponent(null);

                        try{
                            if(((Activity)context).startActivityIfNeeded(intent,-1)){

                                return true;
                            }
                        }catch(ActivityNotFoundException ex){
                            return false;
                        }
                    }
                    //Uri uri = Uri.parse(intent.getDataString());
                    //intent = new Intent(Intent.ACTION_VIEW, uri);
                    //startActivity(intent);
                    //return true;
                } else { //구방식
                    Log.e("LOG.. callApp url ==>", url);
                    Uri uri = Uri.parse(url);
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                return true;
            } catch (Exception e) {
                Log.e("LOG.. Exception ==>", e.getMessage());
                e.printStackTrace();
                return false;
            }
        }

    }

    class CustomWebChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            return super.onJsAlert(view, url, message, result);
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
            return super.onJsConfirm(view, url, message, result);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            final WebSettings settings = view.getSettings();
            settings.setDomStorageEnabled(true);
            settings.setJavaScriptEnabled(true);
            settings.setAllowFileAccess(true);
            settings.setAllowContentAccess(true);
            view.setWebChromeClient(this);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(view);
            resultMsg.sendToTarget();
            return false;
        }

        @Override
        public void onCloseWindow(WebView window) {
            super.onCloseWindow(window);
        }
    }


    public boolean canHistoryBack() {
        if(mBinding.webView.canGoBack()) {
            mBinding.webView.goBack();
            return true;
        } else {
            return false;
        }
    }
}
