package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.Collections;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.FriendProfileActivity;
import kr.co.minetalk.adapter.FriendGridAdapter;
import kr.co.minetalk.adapter.FriendListAdapter;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.FragmentFavoriteBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.ui.CustomScrollView;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;
import kr.co.minetalk.view.FriendListView;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class FavoriteFragment extends BaseFragment {
    private FriendListAdapter mListAdapter;
    private FriendGridAdapter mGridAdapter;

    public static FavoriteFragment newInstance() {
        FavoriteFragment fragment = new FavoriteFragment();
        return fragment;
    }

    private boolean bDescOrder = false;
    private FragmentFavoriteBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;

    private Parcelable recyclerViewState;

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();

        updateTheme();

        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setupFavoriteListData();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);

        mListAdapter = new FriendListAdapter();
        mListAdapter.setCountTypeStr(MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_text_favorite_friend));
        mGridAdapter = new FriendGridAdapter();
        mGridAdapter.setCountTypeStr(MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_text_favorite_friend));

        mBinding.recyclerView.setAdapter(mListAdapter);

        setUIEventListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void updateTheme(){
        if(MineTalk.isBlackTheme){
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top_bk);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom_bk);
        }else{
            mBinding.btnScrollTop.setImageResource(R.drawable.btn_scroll_top);
            mBinding.btnScrollBottom.setImageResource(R.drawable.btn_scroll_bottom);
        }
    }

    private void setUIEventListener() {
        mBinding.btSortList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDescOrder = !bDescOrder;
                mBinding.btSortList.setText(bDescOrder ?
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_asc) :
                        MineTalkApp.getCurrentActivity().getResources().getString(R.string.text_desc));
                refreshView();
            }
        });


        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                View view = recyclerView.getChildAt(recyclerView.getChildCount() - 1);

                if (!mBinding.recyclerView.canScrollVertically(-1)) {
                    Log.d("onScrollStateChanged", "Top of list");
                    mBinding.viewGradient.setVisibility(View.INVISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                    if( view.getBottom() < mBinding.recyclerView.getHeight() )
                        mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                } else if (!mBinding.recyclerView.canScrollVertically(1)) {
                    Log.d("onScrollStateChanged", "Bottom of list");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("onScrollStateChanged", "idle");
                    mBinding.viewGradient.setVisibility(View.VISIBLE);
                    mBinding.btnScrollTop.setVisibility(View.VISIBLE);
                    mBinding.btnScrollBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.btnScrollTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.recyclerView.scrollToPosition(0);

                mBinding.viewGradient.setVisibility(View.INVISIBLE);
                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });

        mBinding.btnScrollBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean bListType = Preferences.getFriendListType();
                if(bListType){ // Grid Type
                    mBinding.recyclerView.scrollToPosition(mGridAdapter.getItemCount()-1);
                }else{
                    mBinding.recyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
                }

                mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
                mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void refreshView() {
        try {
            setupFavoriteListData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refreshView(boolean bGridType) {
        try {
            setupFavoriteListData(bGridType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refreshListType(boolean listType) {
        if (listType) { //GridType
            mLayoutManager.setSpanCount(BaseFragment.mRowNum);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? BaseFragment.mRowNum : 1;
                }
            });
            mBinding.recyclerView.setAdapter(mGridAdapter);
        } else {
            mLayoutManager.setSpanCount(1);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
            mBinding.recyclerView.setAdapter(mListAdapter);
        }

        mListAdapter.notifyDataSetChanged();
        mGridAdapter.notifyDataSetChanged();

        mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
        mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);
    }

    // 친구 즐겨찾기 Data Setting
    private void setupFavoriteListData() {

        boolean listType = Preferences.getFriendListType();
        ArrayList<FriendListModel> arrayList = FriendManager.getInstance().selectFavorite();

        String countMessage = String.format("%s %d%s", MineTalkApp.getCurrentActivity().getResources().getString(R.string.friend_fragment_text_favorite_friend),
                arrayList.size(), MineTalkApp.getCurrentActivity().getResources().getString(R.string.human_count));
        mBinding.tvFavoriteCount.setText(countMessage);

        FriendListModel model;
        String contactName = "";


        for( int i = 0; i < arrayList.size(); ++i){
            model = arrayList.get(i);
            contactName = MineTalkApp.getUserNameByPhoneNum(model.getUser_name(), model.getUser_hp().replace("-", ""));
            if(contactName != null && contactName.length() > 0)
                model.setUser_name(contactName);
        }

        Collections.sort(arrayList, bDescOrder ? MineTalkApp.cmpDesc : MineTalkApp.cmpAsc);

        recyclerViewState = mBinding.recyclerView.getLayoutManager().onSaveInstanceState();

        mListAdapter.setListener(mFriendItemViewListener);
        mListAdapter.setItem(arrayList);

        mGridAdapter.setListener(mFriendItemViewListener);
        mGridAdapter.setItem(arrayList);


        mBinding.btnScrollTop.setVisibility(View.INVISIBLE);
        mBinding.btnScrollBottom.setVisibility(View.INVISIBLE);

        refreshListType(listType);
        mBinding.recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
    }

    // 친구 즐겨찾기 Data Setting
    private void setupFavoriteListData(boolean bGridType) {
        refreshListType(bGridType);
    }

    private OnFriendListViewListener mFriendItemViewListener = new OnFriendListViewListener() {
        @Override
        public void onClickItem(FriendListModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.showProfile(data, FriendProfileActivity.FriendType.FAVORITE);
            }
        }

        @Override
        public void onClickFavorite(FriendListModel data) {

        }

        @Override
        public void addFriend(FriendListModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.addFriend(data);
            }
        }

        @Override
        public void showMyProfile(UserInfoBaseModel data) {
            if(mFragmentListener != null) {
                mFragmentListener.showMyProfile();
            }
        }

        @Override
        public void createChatToMe() {

        }

        @Override
        public void editMyProfile() {

        }

        @Override
        public void refreshOrder(boolean bOrder) {
            bDescOrder = bOrder;
            refreshView();
        }
    };


}
