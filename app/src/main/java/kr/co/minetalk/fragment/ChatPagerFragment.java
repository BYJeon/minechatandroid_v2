package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.AdsMessageDetailActivity;
import kr.co.minetalk.activity.ChatDetailActivity;
import kr.co.minetalk.activity.ChatNormalSetActivity;
import kr.co.minetalk.activity.ChatSearchActivity;
import kr.co.minetalk.activity.FriendSearchActivity;
import kr.co.minetalk.activity.SelectFriendActivity;
import kr.co.minetalk.adapter.ChatFragmentPagerAdapter;
import kr.co.minetalk.api.ListAdMessageApi;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.FragmentChatPagerBinding;
import kr.co.minetalk.fragment.base.BaseFragment;
import kr.co.minetalk.fragment.base.ChatBaseFragment;
import kr.co.minetalk.fragment.listener.OnFragmentEventListener;
import kr.co.minetalk.utils.Preferences;

public class ChatPagerFragment extends Fragment {
    public static ChatPagerFragment newInstance() {
        ChatPagerFragment fragment = new ChatPagerFragment();
        return fragment;
    }

    private FragmentChatPagerBinding mBinding;
    private OnFragmentEventListener mFragmentListener = null;

    //private ChatListAdapter mAdapter;

    private ListAdMessageApi mListAdMessageApi;

    private String mCurrentType;
    private HashMap<String, String> mContactMap = new HashMap<>();

    // Paging Variable /////////////////////
    private int mPage               = 1;
    private int mTotalCount         = 0;
    private int mDataCount          = 0;
    private boolean mLockScrollView = false;
    ////////////////////////////////////////


    // Pager Variable ////
    private ChatFragmentPagerAdapter mAdapter;

    //TabLayout 상단 TextView
    private TextView mTvSecurityChatTabTitle = null;

    public void setOnFragmentListener(OnFragmentEventListener listener) {
        this.mFragmentListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_pager, container, false);
        mBinding.setHandlers(this);


        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
            updateTheme();
        try {
            //chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.mContactMap.clear();
        mContactMap = FriendManager.getInstance().selectFriendXidToNameMap();

        setUIEventListener();
        setChatPagerView();

        boolean listType = Preferences.getChatListType();
        mBinding.ivListTypeChat.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);


//        TabLayout.Tab tab1 = mBinding.tabLayoutChat.newTab();
//        TabLayout.Tab newTab1 = tab1.setCustomView(R.layout.layout_tab_item);
//        mTvSecurityChatTabTitle = newTab1.getCustomView().findViewById(android.R.id.text1);
//        newTab1.setText(getResources().getString(R.string.chat_security));
//
//        mBinding.tabLayoutChat.addTab(newTab1);
//
//        TabLayout.Tab tab2 = mBinding.tabLayoutChat.newTab();
//        TabLayout.Tab newTab2 = tab2.setCustomView(R.layout.layout_tab_item);
//        mTvSecurityChatTabTitle = newTab2.getCustomView().findViewById(android.R.id.text1);
//        newTab2.setText(getResources().getString(R.string.chat_security));
//
//        mBinding.tabLayoutChat.addTab(newTab2);
//
//        TabLayout.Tab tab3 = mBinding.tabLayoutChat.newTab();
//        TabLayout.Tab newTab3 = tab3.setCustomView(R.layout.layout_tab_item);
//        mTvSecurityChatTabTitle = newTab3.getCustomView().findViewById(android.R.id.text1);
//        newTab3.setText(getResources().getString(R.string.chat_security));
//
//        mBinding.tabLayoutChat.addTab(newTab3);

        // Add Tab
        TabLayout.Tab tab4 = mBinding.tabLayoutChat.newTab();
        TabLayout.Tab newTab = tab4.setCustomView(R.layout.layout_tab_item);
        mTvSecurityChatTabTitle = newTab.getCustomView().findViewById(android.R.id.text1);
        mTvSecurityChatTabTitle.setTextColor(getResources().getColor(R.color.security_chat_color));
        newTab.setText(getResources().getString(R.string.chat_security));

        mBinding.tabLayoutChat.addTab(newTab);
    }

    private void initializePagingInfo() {
        mTotalCount     = 0;
        mDataCount      = 0;
        mPage           = 1;
        mLockScrollView = false;
    }

    private void setUIEventListener() {
        //채팅 리스트 ViewType(List,Grid)
        mBinding.ivListTypeChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setChatListType(!Preferences.getChatListType());
                boolean listType = Preferences.getChatListType();

                if(MineTalk.isBlackTheme) { //GridType
                    mBinding.ivListTypeChat.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);
                } else { //ListType
                    mBinding.ivListTypeChat.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);
                }

                for(int i = 0; i < mAdapter.getCount(); ++i){
                    BaseFragment baseFragment = (BaseFragment) mAdapter.getItem(i);
                    baseFragment.refreshListType(listType);
                }
            }
        });

        mBinding.btnCreateChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mFragmentListener != null) {
                    mFragmentListener.makeChatting();
                }

                //SelectFriendActivity.startActivity(MineTalkApp.getCurrentActivity(), MineTalkApp.getCurrentActivity().getString(R.string.select_friend_title), SelectFriendActivity.TYPE_NEW);
            }
        });

        mBinding.btnUserFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatSearchActivity.startActivity(getActivity());
            }
        });


        //채팅 전체 셋팅
        mBinding.btnUserSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatNormalSetActivity.startActivity(getActivity());
            }
        });
    }

    private void setChatPagerView(){
        final ViewPager viewPager = mBinding.viewPagerChat;
        viewPager.setOffscreenPageLimit(3);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter = new ChatFragmentPagerAdapter(getFragmentManager(), mFragmentListener, mContactMap);
                viewPager.setAdapter(mAdapter);
            }
        });

        mBinding.tabLayoutChat.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(), false);
                //mTvSecurityChatTabTitle.setTextColor(tab.getPosition() == 3 ? MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.security_chat_color));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mBinding.tabLayoutChat));

        mBinding.getRoot().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int topPos = (mBinding.tabLayoutChat.getHeight() / 2) - (mBinding.ivListTypeChat.getHeight() / 2);

                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams)mBinding.ivListTypeChat.getLayoutParams();
                if( marginParams.topMargin == 0 )
                    marginParams.setMargins(marginParams.leftMargin, topPos, marginParams.rightMargin, marginParams.rightMargin);
            }
        });
    }


    public void refreshChatList() {
        try {
            for(int i = 0; i < mAdapter.getCount(); ++i){
                ChatBaseFragment fragment = (ChatBaseFragment) mAdapter.getItem(i);
                fragment.refreshChatList();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateTheme(){
        boolean listType = Preferences.getChatListType();

        //mTvSecurityChatTabTitle.setTextColor(mBinding.viewPagerChat.getCurrentItem() == 3 ? MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.security_chat_color));

        if(MineTalk.isBlackTheme){
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.bk_theme_bg));
            mBinding.tvChatTitle.setTextColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tabLayoutChat.setBackgroundResource(R.drawable.bg_tab_layout_bk);
            mBinding.tabLayoutChat.setSelectedTabIndicatorColor(getResources().getColor(R.color.bk_point_color));
            mBinding.tabLayoutChat.setTabTextColors(getResources().getColor(R.color.white_color), getResources().getColor(R.color.white_color));

            mBinding.ivListTypeChat.setImageResource(listType ? R.drawable.icon_top_grid_on_bk : R.drawable.icon_top_list_on_bk);

            mBinding.btnCreateChat.setImageResource(R.drawable.icon_add_top_chat_bk);
            mBinding.btnUserFind.setImageResource(R.drawable.icon_search_bk);
            mBinding.btnUserSet.setImageResource(R.drawable.icon_set_bk);

        }else{
            mBinding.layoutRoot.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.tvChatTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tabLayoutChat.setBackgroundResource(R.drawable.bg_tab_layout);
            mBinding.tabLayoutChat.setSelectedTabIndicatorColor(getResources().getColor(R.color.app_point_color));
            mBinding.tabLayoutChat.setTabTextColors(getResources().getColor(R.color.main_text_color), getResources().getColor(R.color.main_text_color));

            mBinding.ivListTypeChat.setImageResource(listType ? R.drawable.icon_top_grid_on : R.drawable.icon_top_list_on);

            mBinding.btnCreateChat.setImageResource(R.drawable.icon_add_top_chat);
            mBinding.btnUserFind.setImageResource(R.drawable.icon_search);
            mBinding.btnUserSet.setImageResource(R.drawable.icon_set);
        }
    }
}
