package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;

import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.adapter.SelectFriendAdapter;
import kr.co.minetalk.adapter.SendPointUserAdapter;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.FragmentSelectFriendBinding;
import kr.co.minetalk.repository.ChatRepository;

public class SelectFriendFragment extends Fragment {
    public static SelectFriendFragment newInstance() {
        SelectFriendFragment fragment = new SelectFriendFragment();
        return fragment;
    }

    private FragmentSelectFriendBinding mBinding;
    private SendPointUserAdapter mAdapter;


    private ArrayList<FriendListModel> mFriendList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_friend, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUIEventListener();
        loadFriend();
    }

    private void loadFriend() {
        mFriendList.clear();

        for(int i = 0; i < MineTalkApp.getFriendList().size() ; i++) {
            FriendListModel data = MineTalkApp.getFriendList().get(i);

            FriendListModel item = new FriendListModel();
            item.setUser_xid(data.getUser_xid());
            item.setUser_hp(data.getUser_hp());
            item.setUser_name(data.getUser_name());
            item.setUser_state_message(data.getUser_state_message());
            item.setUser_profile_image(data.getUser_profile_image());

            mFriendList.add(item);
        }

        mAdapter = new SendPointUserAdapter();
        mAdapter.setData(mFriendList);
        mBinding.listView.setAdapter(mAdapter);
    }

    private void setUIEventListener() {
        mBinding.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mAdapter.setSelectIndex(position);
            }
        });
    }

    public String getSelectUserXid() {
        int selectIndex = mAdapter.getSelectIndex();
        if(selectIndex != -1) {
            FriendListModel item = (FriendListModel) mAdapter.getItem(selectIndex);
            return item.getUser_xid();
        }

        return "";
    }
}
