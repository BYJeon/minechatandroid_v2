package kr.co.minetalk.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.adapter.SelectChatListAdapter;
import kr.co.minetalk.databinding.FragmentSelectChatListBinding;
import kr.co.minetalk.databinding.FragmentSelectFriendBinding;
import kr.co.minetalk.message.api.BaseHttpRequest;
import kr.co.minetalk.message.api.ThreadListApi;
import kr.co.minetalk.message.model.ThreadData;

public class SelectChatListFragment extends Fragment {
    public static SelectChatListFragment newInstance() {
        SelectChatListFragment fragment = new SelectChatListFragment();
        return fragment;
    }

    private FragmentSelectChatListBinding mBinding;
    private SelectChatListAdapter mAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_chat_list, container, false);
        mBinding.setHandlers(this);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new SelectChatListAdapter();
        mBinding.listView.setAdapter(mAdapter);

        setUIEventListener();


        chatListRequest(MineTalkApp.getUserInfoModel().getUser_xid());
    }

    private void setUIEventListener() {
        mBinding.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ThreadData threadData = (ThreadData)mAdapter.getItem(position);
                if(threadData != null) {
                    mAdapter.setSelectThread(threadData.getThreadKey());
                }

            }
        });
    }

    private void chatListRequest(String user_xid) {
//        ProgressUtil.showProgress(MineTalkApp.getCurrentActivity());
        ThreadListApi threadListApi = new ThreadListApi(MineTalkApp.getAppContext(), user_xid);
        threadListApi.request(new BaseHttpRequest.APICallbackListener<List<ThreadData>>() {
            @Override
            public void onSuccess(List<ThreadData> res) {
                int count  = 1;

                ArrayList<ThreadData> threadList = new ArrayList<>();
                for(ThreadData data : res) {
                    threadList.add(data);
                    count++;
                }

                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.setData(threadList);
//                        ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody, Throwable error) {
                MineTalkApp.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                    }
                });
            }
        });
    }

    public String getSelectData() {
        return this.mAdapter.getSelectThreadKey();
    }
}
