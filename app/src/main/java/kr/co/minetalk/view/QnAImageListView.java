package kr.co.minetalk.view;

import android.content.Context;
import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.IOException;
import java.io.InputStream;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.LayoutFriendListViewOrgBinding;
import kr.co.minetalk.databinding.LayoutQnaImageViewBinding;
import kr.co.minetalk.view.listener.OnFriendListViewListener;
import kr.co.minetalk.view.listener.OnQnAListViewListener;

public class QnAImageListView extends LinearLayout {


    private Context mContext;
    private LayoutQnaImageViewBinding mBinding;

    private String mFilePath;
    private float  mFileSize;
    private OnQnAListViewListener mListener;


    public QnAImageListView(Context context) {
        super(context);
        initView(context);
    }

    public QnAImageListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public QnAImageListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_qna_image_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {

        mBinding.btnDel.setImageResource(MineTalk.isBlackTheme ? R.drawable.btn_close_s_bk : R.drawable.btn_close_s);
        mBinding.btnDel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onClickItem(mFilePath);
                }
            }
        });

    }


    public void setOnItemListener(OnQnAListViewListener listener) {
        this.mListener = listener;
    }

    public void setData(String path, float fileSize) {
        this.mFilePath = path;
        this.mFileSize = fileSize;

        refreshView();
    }

    public String getData() { return mFilePath; }

    private void refreshView() {
        //임시 파일 수정
        String filter = "tmp_profile_";
        String tmp = mFilePath;
        int index = tmp.indexOf(filter);
        tmp = tmp.substring(index + filter.length());
        mBinding.tvFileName.setText(tmp+" ("+ String.format("%.1f", mFileSize)+"mb)");
    }


}
