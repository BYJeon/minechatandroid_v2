package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.activity.CreateMarketingChatActivity;
import kr.co.minetalk.activity.SelectFriendActivity;
import kr.co.minetalk.activity.SelectSecurityFriendActivity;
import kr.co.minetalk.databinding.LayoutCreateChatViewBinding;

public class CreateChatView extends LinearLayout {

    private Context mContext;
    private LayoutCreateChatViewBinding mBinding;

    public CreateChatView(Context context) {
        super(context);
        initView(context);
    }

    public CreateChatView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CreateChatView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_create_chat_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();

        updateTheme();
    }

    private void setUIEventListener() {
        mBinding.layoutRoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(View.GONE);
            }
        });

        mBinding.btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(View.GONE);
            }
        });

        mBinding.tvButtonNormalChat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(View.GONE);
                SelectFriendActivity.startActivity(mContext, mContext.getString(R.string.select_friend_title), SelectFriendActivity.TYPE_NEW);
            }
        });

        mBinding.tvButtonSecurityChat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(View.GONE);
                SelectSecurityFriendActivity.startActivity(mContext, mContext.getString(R.string.select_friend_title), SelectFriendActivity.TYPE_NEW);
            }
        });
    }

    public void updateTheme(){
        //방 만들기
        Drawable createChat = getResources().getDrawable( MineTalk.isBlackTheme ? R.drawable.icon_chat_add_general_w : R.drawable.icon_chat_add_general );
        int h = createChat.getIntrinsicHeight();
        int w = createChat.getIntrinsicWidth();
        createChat.setBounds( 0, 0, w, h );
        mBinding.tvButtonNormalChat.setCompoundDrawables(null, createChat, null, null);

        if(MineTalk.isBlackTheme){
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));
            mBinding.layoutBody.setBackgroundColor(getResources().getColor(R.color.bk_theme_status));

            mBinding.tvButtonNormalChat.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.white_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before_wh);
        }else{
            mBinding.layoutHeader.setBackgroundColor(getResources().getColor(R.color.white_color));
            mBinding.layoutBody.setBackgroundColor(getResources().getColor(R.color.white_color));

            mBinding.tvButtonNormalChat.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.tvActivityTitle.setTextColor(getResources().getColor(R.color.main_text_color));
            mBinding.btnBack.setImageResource(R.drawable.btn_top_before);
        }
    }
}
