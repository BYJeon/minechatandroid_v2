package kr.co.minetalk.view.image;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatImageView;

import kr.co.minetalk.R;


public class CornerRoundImageView extends AppCompatImageView {

    // ----- Local Value -----
    private Path mMaskPath = null;
    private float[] mRadii = null;
    // -----------------------

    ////////////////////////////////
    // CONSTRUCTOR
    public CornerRoundImageView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public CornerRoundImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public CornerRoundImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    ////////////////////////////////
    // OVERRIDE
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w != oldw || h != oldh) {
            generateMaskPath(w, h);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (canvas.isOpaque()) {
            canvas.saveLayerAlpha(0, 0, canvas.getWidth(), canvas.getHeight(), 255);
        }
        if (mMaskPath != null) {
            canvas.clipPath(mMaskPath);
        }
        super.onDraw(canvas);
    }

    ////////////////////////////////
    // PRIVATE
    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray a;
        if (attrs == null) {
            a = context.obtainStyledAttributes(defStyleAttr > 0 ? defStyleAttr : R.style.DefaultCornerRoundImageStyle, R.styleable.CornerRoundImageStyle);
        } else {
            a = context.obtainStyledAttributes(attrs, R.styleable.CornerRoundImageStyle, defStyleAttr, R.style.DefaultCornerRoundImageStyle);
        }

        mRadii = getTopCornerRadii(
                a.getDimension(R.styleable.CornerRoundImageStyle_roundTopLeft, 0),
                a.getDimension(R.styleable.CornerRoundImageStyle_roundTopRight, 0),
                a.getDimension(R.styleable.CornerRoundImageStyle_roundBottomLeft, 0),
                a.getDimension(R.styleable.CornerRoundImageStyle_roundBottomRight, 0)
        );
        a.recycle();
    }

    private float[] getTopCornerRadii(float topLeft, float topRight, float bottomLeft, float bottomRight) {
        // float lt = topLeft * getResources().getDisplayMetrics().density;
        // float rt = topRight * getResources().getDisplayMetrics().density;
        // return new float[]{lt, lt, rt, rt, 0, 0, 0, 0};
        return new float[]{topLeft, topLeft, topRight, topRight, bottomRight, bottomRight, bottomLeft, bottomLeft};
    }

    private void generateMaskPath(int w, int h) {
        if (mMaskPath == null) {
            mMaskPath = new Path();
        }
        mMaskPath.addRoundRect(new RectF(0, 0, w, h), mRadii, Path.Direction.CW);
    }
    ////////////////////////////////
}
