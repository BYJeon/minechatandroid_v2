package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.R;
import kr.co.minetalk.databinding.LayoutColorBoxBinding;

public class ColorBoxView extends LinearLayout {

    private Context mContext;
    private LayoutColorBoxBinding mBinding;

    public ColorBoxView(Context context) {
        super(context);
        initView(context);
    }

    public ColorBoxView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ColorBoxView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_color_box, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);
    }

    public void setBackColor(String color) {
        mBinding.viewColor.setBackgroundColor(Color.parseColor("#" + color));
    }

    public void setChecked(boolean isCheck) {
        if(isCheck) {
            mBinding.ivChecked.setVisibility(View.VISIBLE);
        } else {
            mBinding.ivChecked.setVisibility(View.INVISIBLE);
        }
    }


}
