package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;

import kr.co.minetalk.R;
import kr.co.minetalk.api.model.AdMessageModel;
import kr.co.minetalk.databinding.LayoutAdMessageReceiveBinding;

public class AdMessageReceiveView extends LinearLayout {

    private Context mContext;
    private LayoutAdMessageReceiveBinding mBinding;

    private AdMessageModel mData;
    private SimpleDateFormat mDtFormat = new SimpleDateFormat("HH:mm");

    public AdMessageReceiveView(Context context) {
        super(context);
        initView(context);
    }

    public AdMessageReceiveView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public AdMessageReceiveView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;
        View v = LayoutInflater.from(context).inflate(R.layout.layout_ad_message_receive, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);
    }

    public void setData(AdMessageModel data) {
        this.mData = data;

        refreshView();
    }

    private void refreshView() {
        if(mData != null) {

            String profileImgUrl = mData.getUser_profile_image();
            if(profileImgUrl == null || profileImgUrl.equals("")) {
                Glide.with(mContext).load(R.drawable.img_basic).into(mBinding.ivOtherUserImage);
            } else {
                Glide.with(mContext).load(mData.getUser_profile_image()).into(mBinding.ivOtherUserImage);
            }

            mBinding.tvTitle.setText(mData.getAd_title());
            mBinding.tvContent.setText(mData.getAd_contents());


            String contentImage = mData.getAd_image_url();
            if(contentImage != null && !contentImage.equals("")) {
                mBinding.ivContentsImage.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(contentImage).into(mBinding.ivContentsImage);
            } else {
                mBinding.ivContentsImage.setVisibility(View.GONE);
            }



//            mBinding.tvOtherMsgTime.setText(mDtFormat.format(mData.getAd_send_date()));
        }
    }




}
