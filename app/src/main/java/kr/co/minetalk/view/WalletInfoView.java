package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.AdMessageModel;
import kr.co.minetalk.api.model.TokenListModel;
import kr.co.minetalk.databinding.LayoutAdMessageReceiveBinding;
import kr.co.minetalk.databinding.LayoutWalletInfoBinding;
import kr.co.minetalk.ui.listener.OnAdChatItemListener;

public class WalletInfoView extends LinearLayout {

    private Context mContext;
    private LayoutWalletInfoBinding mBinding;

    private TokenListModel mData;

    private FAQItemView.ItemType mType = FAQItemView.ItemType.ITEM_TYPE_TITLE;

    public WalletInfoView(Context context) {
        super(context);
        initView(context);
    }

    public WalletInfoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public WalletInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;
        View v = LayoutInflater.from(context).inflate(R.layout.layout_wallet_info, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);
        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.btnWalletCopy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MineTalk.setClipBoardLink(MineTalkApp.getAppContext(), mBinding.tvWalletAddr.getText().toString());
            }
        });
    }


    public void setData(TokenListModel data, FAQItemView.ItemType type) {
        this.mData = data;
        this.mType = type;

        refreshView();
    }

    private void refreshView() {
        if(mData != null) {

            mBinding.tvCoinTitle.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));
            mBinding.tvWalletAddr.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));

            mBinding.btnWalletCopy.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.main_text_color) : getResources().getColor(R.color.white_color));
            mBinding.btnWalletCopy.setBackgroundResource(MineTalk.isBlackTheme ? R.drawable.button_bg_cdb60c : R.drawable.button_bg_16c066);

            //코인명
            mBinding.layoutCoinTypeRoot.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : getResources().getColor(R.color.white_color));
            mBinding.tvCointNameTitle.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.main_text_nor_color) : getResources().getColor(R.color.main_text_color));

            mBinding.tvCointNameTitle.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));
            mBinding.tvCoinTitle.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));

            mBinding.viewWalletName.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_divider_bg) : getResources().getColor(R.color.main_divider_bg));
            //생성일시
            mBinding.layoutRegdateRoot.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : getResources().getColor(R.color.white_color));
            mBinding.tvRegDate.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));
            mBinding.tvRegDateTitle.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));
            mBinding.viewRegDate.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_divider_bg) : getResources().getColor(R.color.main_divider_bg));

            //지갑주소
            mBinding.layoutButtonWalletAddr.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : getResources().getColor(R.color.white_color));
            mBinding.layoutButtonWalletAddrSub.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : getResources().getColor(R.color.white_color));
            mBinding.tvWalletAddrTitle.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));
            mBinding.tvWalletAddr.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));

            mBinding.tvCoinTitle.setText(mData.getUser_token_symble());
            mBinding.tvWalletAddr.setText(mData.getUser_token_address());

            mBinding.tvRegDate.setText(mData.getUser_token_regDate());
        }
    }




}
