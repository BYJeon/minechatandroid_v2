package kr.co.minetalk.view.listener;

import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;

public interface OnQnAListViewListener {
    public void onClickItem(String path);
}
