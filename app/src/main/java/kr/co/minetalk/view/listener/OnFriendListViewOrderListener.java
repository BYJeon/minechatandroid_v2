package kr.co.minetalk.view.listener;

import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;

public interface OnFriendListViewOrderListener {
    public void refreshListType(boolean bGridType);
    public void refreshOrder(boolean bOrder);
}
