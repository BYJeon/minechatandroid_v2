package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.R;
import kr.co.minetalk.databinding.LayoutCheckBarViewBinding;
import kr.co.minetalk.view.listener.OnCheckBarEventListener;

public class CheckBarView extends LinearLayout {

    private Context mContext;
    private LayoutCheckBarViewBinding mBinding;

    private String mValue;

    public CheckBarView(Context context) {
        super(context);
        initView(context);
    }

    public CheckBarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CheckBarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }


    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_check_bar_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);
        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {

    }

    public void setData(String buttonTitle, String value) {
        this.mBinding.tvTitle.setText(buttonTitle);
        this.mValue = value;
    }

    public void setSelected(boolean isSelect) {
        mBinding.ivChecked.setSelected(isSelect);
    }






}
