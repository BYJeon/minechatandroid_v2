package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.R;
import kr.co.minetalk.databinding.LayoutLanguageIconViewBinding;

public class LanguageIconView extends LinearLayout {

    private Context mContext;
    private LayoutLanguageIconViewBinding mBinding;

    public LanguageIconView(Context context) {
        super(context);
        initView(context);
    }

    public LanguageIconView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public LanguageIconView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }
    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_language_icon_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);
    }

    public void setData(int imageRes, String name) {
        mBinding.ivFlag.setImageResource(imageRes);
        mBinding.tvCountryName.setText(name);
    }

    public void select() {
        mBinding.ivChecked.setVisibility(View.VISIBLE);
    }

    public void unSelect() {
        mBinding.ivChecked.setVisibility(View.INVISIBLE);
    }
}
