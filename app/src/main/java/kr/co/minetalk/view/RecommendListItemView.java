package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.R;
import kr.co.minetalk.api.model.RecommendModel;
import kr.co.minetalk.databinding.LayoutRecommendListItemBinding;
import kr.co.minetalk.utils.CommonUtils;

public class RecommendListItemView extends LinearLayout {
    private Context mContext;
    private RecommendModel mData;
    private LayoutRecommendListItemBinding mBinding;

    public RecommendListItemView(Context context) {
        super(context);
        initView(context);
    }

    public RecommendListItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public RecommendListItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;
        View v = LayoutInflater.from(context).inflate(R.layout.layout_recommend_list_item, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);
        setUIEventLisetner();
    }

    private void setUIEventLisetner() {

    }

    public void setData(RecommendModel data) {
        this.mData = data;
        refreshView();
    }

    private void refreshView() {
        if(mData != null) {
            mBinding.tvUserName.setText(mData.getUser_name());
            if(mData.getUse_flag().toUpperCase().equals("Y")) {
                mBinding.tvState.setBackgroundResource(R.drawable.round_box_recom_green);
                mBinding.tvState.setText(mContext.getResources().getString(R.string.recommend_reg_y));
            } else {
                mBinding.tvState.setBackgroundResource(R.drawable.round_box_recom_red);
                mBinding.tvState.setText(mContext.getResources().getString(R.string.recommend_reg_n));
            }

            String reg_date = CommonUtils.formattedDate(mData.getReg_date(), "yyyy-MM-dd HH:mm:ss", "yyyy.MM.dd");
            mBinding.tvRegDate.setText(mContext.getResources().getString(R.string.recommend_reg_date) + reg_date);

            SpannableString content = new SpannableString(mData.getUser_hp());
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            mBinding.tvUserPhone.setText(content);

            mBinding.tvUserCerti.setVisibility(mData.getUser_hp_confirm().equals("Y") ? View.INVISIBLE : View.VISIBLE);
        }
    }

}
