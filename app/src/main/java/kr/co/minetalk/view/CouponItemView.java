package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.CouponModel;
import kr.co.minetalk.api.model.FaqModel;
import kr.co.minetalk.databinding.LayoutCouponViewBinding;
import kr.co.minetalk.databinding.LayoutFaqViewBinding;
import kr.co.minetalk.utils.CommonUtils;

public class CouponItemView extends LinearLayout {

    public enum ItemType {
        ITEM_TYPE_TITLE(1),
        ITEM_TYPE_ODD(2),
        ITEM_TYPE_EVEN(3);

        private int value;

        ItemType(int value) { this.value = value; }

        public int getValue() { return this.value; }
    }

    private Context mContext;

    private LayoutCouponViewBinding mBinding;

    private boolean mIsOpen = false;

    private CouponModel mCouponModel;

    private ItemType mFAQType = ItemType.ITEM_TYPE_ODD;

    public CouponItemView(Context context) {
        super(context);
        initView(context);
    }

    public CouponItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CouponItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context, ItemType type) {
        this.mContext = context;
        this.mFAQType = type;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_coupon_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();

//        mBinding.tvNoticeType.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
//        mBinding.tvNoticeTitle.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
//        mBinding.tvDetailContent.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
//
//        if(type == ItemType.ITEM_TYPE_TITLE){
//            mBinding.tvNoticeType.setTypeface(mBinding.tvNoticeType.getTypeface(),Typeface.BOLD);
//            mBinding.tvNoticeTitle.setTypeface(mBinding.tvNoticeType.getTypeface(),Typeface.BOLD);
//            mBinding.tvNoticeTitle.setGravity(Gravity.CENTER);
//            mBinding.ivArrow.setVisibility(View.INVISIBLE);
//            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
//        }else if(type == ItemType.ITEM_TYPE_ODD){
//            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
//            mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
//
//        }else{
//            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
//            mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
//        }
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_coupon_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {

    }

    public void setData(CouponModel data) {
        this.mCouponModel = data;

        refreshView();
    }

    private void refreshView() {
        if(mCouponModel != null) {
            mBinding.tvDateTime.setText(mCouponModel.getReg_date());
            mBinding.tvCouponTitle.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));
            mBinding.tvCouponTitle.setText(mCouponModel.getCoupon_name());
            mBinding.tvCouponType.setText(mCouponModel.getCoupon_category());

            if(mCouponModel.getCoupon_category().equals("R")){ //요청
                mBinding.tvCouponType.setText(getResources().getString(R.string.mining_request_title));
            }else if(mCouponModel.getCoupon_category().equals("C")){//승인
                mBinding.tvCouponType.setText(getResources().getString(R.string.mining_approve_title));
            }else{
                mBinding.tvCouponType.setText(getResources().getString(R.string.mining_reject_title));
            }

            if(mCouponModel.getCoupon_category().contains("C")){
                mBinding.tvCouponType.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.app_point_color));
                mBinding.tvAmount.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));
            }else{
                mBinding.tvCouponType.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));
                mBinding.tvAmount.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.bk_main_text_nor_color));
            }

            String limitAmount = CommonUtils.comma_won(mCouponModel.getCoupon_amount());
            mBinding.tvAmount.setText(limitAmount);
        }
    }
}
