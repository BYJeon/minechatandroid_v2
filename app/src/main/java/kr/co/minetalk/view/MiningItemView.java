package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.CouponModel;
import kr.co.minetalk.api.model.MiningModel;
import kr.co.minetalk.databinding.LayoutCouponViewBinding;
import kr.co.minetalk.databinding.LayoutMiningViewBinding;
import kr.co.minetalk.utils.CommonUtils;

public class MiningItemView extends LinearLayout {

    public enum ItemType {
        ITEM_TYPE_TITLE(1),
        ITEM_TYPE_ODD(2),
        ITEM_TYPE_EVEN(3);

        private int value;

        ItemType(int value) { this.value = value; }

        public int getValue() { return this.value; }
    }

    private Context mContext;

    private LayoutMiningViewBinding mBinding;

    private boolean mIsOpen = false;

    private MiningModel mMiningModel;

    private ItemType mFAQType = ItemType.ITEM_TYPE_ODD;

    public MiningItemView(Context context) {
        super(context);
        initView(context);
    }

    public MiningItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public MiningItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context, ItemType type) {
        this.mContext = context;
        this.mFAQType = type;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_mining_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_mining_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {

    }

    public void setData(MiningModel data) {
        this.mMiningModel = data;

        refreshView();
    }

    private void refreshView() {
        if(mMiningModel != null) {
            mBinding.tvDateTime.setText(mMiningModel.getReg_date());
            mBinding.tvCouponTitle.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));

            if(!mMiningModel.getEtc_data().equals(""))
                mBinding.tvCouponTitle.setText(mMiningModel.getMining_name() + " " + String.format(getResources().getString(R.string.day_count_title), mMiningModel.getEtc_data()));
            else
                mBinding.tvCouponTitle.setText(mMiningModel.getMining_name());

            mBinding.tvCouponType.setText(mMiningModel.getCategory());
            if(mMiningModel.getCategory().contains("구매")){
                mBinding.tvCouponType.setTextColor(getResources().getColor(R.color.app_point_color));
            }else if(mMiningModel.getCategory().contains("추천")){
                mBinding.tvCouponType.setTextColor(Color.parseColor("#058aff"));
            }

            mBinding.tvAmount.setTextColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color));

            String limitAmount = CommonUtils.comma_won(mMiningModel.getAmount());
            mBinding.tvAmount.setText(limitAmount);
        }
    }
}
