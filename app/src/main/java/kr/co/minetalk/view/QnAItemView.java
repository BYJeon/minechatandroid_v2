package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.QnAModel;
import kr.co.minetalk.databinding.LayoutQnaViewBinding;
import kr.co.minetalk.utils.CommonUtils;

public class QnAItemView extends LinearLayout {
    private Context mContext;

    private LayoutQnaViewBinding mBinding;

    private boolean mIsOpen = false;

    private QnAModel mQnAModel;

    private FAQItemView.ItemType mFAQType = FAQItemView.ItemType.ITEM_TYPE_ODD;

    public QnAItemView(Context context, FAQItemView.ItemType type) {
        super(context);
        initView(context, type);
    }

    public QnAItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public QnAItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_qna_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void initView(Context context, FAQItemView.ItemType type) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_qna_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();

        mBinding.tvNoticeType.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvQnaTitle.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvDetailContent.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvQnaAnswerTitle.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvQnaSendTitle.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );

        mFAQType = type;

        if(type == FAQItemView.ItemType.ITEM_TYPE_TITLE){
            mBinding.tvNoticeType.setTypeface(mBinding.tvNoticeType.getTypeface(), Typeface.NORMAL);
            mBinding.tvNoticeType.setGravity(Gravity.CENTER);
            mBinding.tvQnaTitle.setTypeface(mBinding.tvNoticeType.getTypeface(),Typeface.NORMAL);
            mBinding.tvQnaTitle.setGravity(Gravity.CENTER);

            //mBinding.tvNoticeType.setTextColor(getResources().getColor(R.color.main_text_nor_color));
            //mBinding.tvQnaTitle.setTextColor(getResources().getColor(R.color.main_text_nor_color));

            mBinding.ivArrow.setVisibility(View.INVISIBLE);
            mBinding.layoutDateInfo.setVisibility(View.INVISIBLE);
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#F8F8F8") );

            mBinding.viewTopGap.setVisibility(View.VISIBLE);

        }else if(type == FAQItemView.ItemType.ITEM_TYPE_ODD){
            mBinding.tvQnaTitle.setVisibility(View.INVISIBLE);
            mBinding.viewTopGap.setVisibility(View.GONE);
            //mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : getResources().getColor(R.color.white_color) );
            //mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#f2f2f2") );

            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#FFFFFF") );
            mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#FFFFFF") );
        }else{
            mBinding.tvQnaTitle.setVisibility(View.INVISIBLE);
            mBinding.viewTopGap.setVisibility(View.GONE);
            //mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : getResources().getColor(R.color.white_color) );
            //mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#f2f2f2") );

            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#F8F8F8") );
            mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#F8F8F8") );

        }

        mBinding.viewRowDivder.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_divider_bg) : getResources().getColor(R.color.main_divider_bg));
        mBinding.ivArrow.setImageResource(MineTalk.isBlackTheme ? R.drawable.icon_arrow_drop_down_small_wh : R.drawable.icon_arrow_drop_down_small);
    }

    private void setUIEventListener() {
        mBinding.layoutNoticeHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if( mFAQType == FAQItemView.ItemType.ITEM_TYPE_TITLE ) return;
                if(mIsOpen) {
                    closeDetail();
                    mIsOpen = false;
                } else {
                    openDetail();
                    mIsOpen = true;
                }
            }
        });
    }

    private void openDetail() {
        mBinding.ivArrow.setImageResource(MineTalk.isBlackTheme ? R.drawable.icon_arrow_drop_up_small_wh : R.drawable.icon_arrow_drop_up_samll);
        mBinding.layoutDetail.setVisibility(View.VISIBLE);

    }

    private void closeDetail() {
        mBinding.ivArrow.setImageResource(MineTalk.isBlackTheme ? R.drawable.icon_arrow_drop_down_small_wh : R.drawable.icon_arrow_drop_down_small);
        mBinding.layoutDetail.setVisibility(View.GONE);
    }

    public void setData(QnAModel data) {
        this.mQnAModel = data;

        refreshView();
    }

    private void refreshView() {
        if(mQnAModel != null) {
            mBinding.tvNoticeType.setText(mQnAModel.getQna_type());
            mBinding.tvQnaTitle.setText(mQnAModel.getQna_question());

            String qnaAnswer = mContext.getResources().getString(R.string.qna_question_tag) + "<br><br>" +
                    mQnAModel.getQna_question() + "<br><br> - <br><br>" +
                    mContext.getResources().getString(R.string.qna_answer_tag) + "<br><br>" +
                    mQnAModel.getQna_answer();

            mBinding.tvDetailContent.setText(Html.fromHtml(qnaAnswer));

            String reg_date = String.format(getResources().getString(R.string.qna_qna_send_reg_date), mQnAModel.getReg_date());
            String reg_recv_date = String.format(getResources().getString(R.string.qna_qna_recv_reg_date), mQnAModel.getAnswer_reg_date());
            mBinding.tvRegDate.setText(reg_date);
            mBinding.tvQnaSendTitle.setText(reg_date);
            mBinding.tvQnaAnswerTitle.setText(reg_recv_date);

//            mBinding.tvRegDate.setText(mQnAModel.getReg_date());
        }
    }
}
