package kr.co.minetalk.view;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by kyd0822 on 2017. 8. 28..
 */

public class ReverseTextView extends TextView {

    public ReverseTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.translate(getWidth(), getHeight());
        canvas.scale(-1, -1);
        super.onDraw(canvas);
    }
}
