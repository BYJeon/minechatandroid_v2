package kr.co.minetalk.view.listener;

public interface OnCheckBarEventListener {
    public void onChecked(boolean isCheck, String value, String type);
}
