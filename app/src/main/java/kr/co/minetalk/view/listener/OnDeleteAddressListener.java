package kr.co.minetalk.view.listener;

public interface OnDeleteAddressListener {
    public void onAddressDelete(String idx);
}
