package kr.co.minetalk.view.listener;

import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.view.FriendListView;

public interface OnFriendListViewListener {
    public void onClickItem(FriendListModel data);
    public void showMyProfile(UserInfoBaseModel data);
    public void onClickFavorite(FriendListModel data);
    public void addFriend(FriendListModel data);
    public void createChatToMe();
    public void editMyProfile();
    public void refreshOrder(boolean bOrder);
}
