package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Date;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.databinding.LayoutAdListItemBinding;
import kr.co.minetalk.ui.listener.OnAdChatItemListener;
import kr.co.minetalk.utils.CommonUtils;

public class AdListItemView extends LinearLayout {
    private Context mContext;
    private LayoutAdListItemBinding mBinding;
    private OnAdChatItemListener mListener;

    private String mType;
    private AdListModel mData;

    public AdListItemView(Context context) {
        super(context);
        initView(context);
    }

    public AdListItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public AdListItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;
        View v = LayoutInflater.from(context).inflate(R.layout.layout_ad_list_item, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.layoutAdItemRoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onItemClick(mType, mData);
                }
            }
        });
    }

    public void setData(String type, AdListModel data) {
        this.mData = data;
        this.mType = type;

        refreshView();
    }

    private void refreshView() {
        if(mData != null) {
            if(mType.equals(MineTalk.AD_LIST_TYPE_R)) {
                // 받은 메시지
                mBinding.tvInfo2.setText("");
                mBinding.tvInfo2.setVisibility(View.GONE);

                if(mData.getReceive_yn().equals("Y")) {
                    mBinding.tvInfo1.setText(mData.getAd_token_amount() + mContext.getString(R.string.get_token));
                } else {
                    mBinding.tvInfo1.setTextColor(Color.parseColor("#002169"));
                    mBinding.tvInfo1.setText(mContext.getResources().getString(R.string.ad_unread));
                }

                try {
                    String expiredDateStart = CommonUtils.convertDateFormat(mData.getAd_send_date(), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");
                    String expiredDateEnd   = CommonUtils.convertDateFormat(mData.getAd_end_date(), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");

                    String expiredDate = " (" + mContext.getResources().getString(R.string.ad_experiod) + " : " + expiredDateEnd + ")";
                    String tvInfoStr = mBinding.tvInfo1.getText().toString();

                    boolean isExpired = dateCompaired(getCurrentDate(), expiredDateEnd);
                    if(isExpired) {
                        mBinding.tvInfo1.setTextColor(Color.parseColor("#002169"));
                        mBinding.tvInfo1.setText(tvInfoStr);
                        mBinding.tvInfo2.setTextColor(Color.parseColor("#ff6e6e"));
                        mBinding.tvInfo2.setText(mContext.getString(R.string.date_expired) );
                        mBinding.tvInfo2.setVisibility(View.VISIBLE);

                        if(mData.getReceive_yn().equals("N")) {
                            mBinding.tvInfo1.setVisibility(View.GONE);
                        } else {
                            mBinding.tvInfo1.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mBinding.tvInfo1.setTextColor(Color.parseColor("#002169"));
                        mBinding.tvInfo1.setText(tvInfoStr + expiredDate);

                        mBinding.tvInfo2.setTextColor(Color.parseColor("#9b9b9b"));
                        mBinding.tvInfo2.setText("");
                        mBinding.tvInfo2.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mBinding.tvMsgDate.setText("");
                mBinding.tvMsgDate.setVisibility(View.GONE);
            } else if(mType.equals(MineTalk.AD_LIST_TYPE_S)) {
                // 보낸 메시지
                String receiveMsg = mContext.getString(R.string.person_1) + " " + mData.getAd_token_amount() +
                        mContext.getString(R.string.more_fragment_text_unit_token);
                mBinding.tvInfo1.setText(receiveMsg);

                mBinding.tvInfo2.setVisibility(View.VISIBLE);
                String info2 = mData.getReceive_count() + mContext.getResources().getString(R.string.ad_read_text) + " / " + mData.getAd_send_count() + mContext.getResources().getString(R.string.person_unit);
                mBinding.tvInfo2.setTextColor(Color.parseColor("#9b9b9b"));
                mBinding.tvInfo2.setText(info2);

                mBinding.tvMsgDate.setText(mData.getAd_send_date());
                mBinding.tvMsgDate.setVisibility(View.VISIBLE);
            }

            if(mData.getUser_profile_image() == null || mData.getUser_profile_image().equals("")) {
                Glide.with(mContext).load(R.drawable.img_basic).into(mBinding.ivUserImage);
            } else {
                Glide.with(mContext).load(mData.getUser_profile_image()).into(mBinding.ivUserImage);
            }

            mBinding.tvUserName.setText(mData.getUser_name());
            mBinding.tvLastMsg.setText(mData.getAd_title());

        }
    }

    public void setOnEventListener(OnAdChatItemListener listener) {
        this.mListener = listener;
    }


    /**
     * 현재 날짜 가져오기 (yyyy-MM-dd HH:mm 형식으로 리턴)
     * @return
     */
    public String getCurrentDate() {
        long now = System.currentTimeMillis();
        Date date = new Date(now);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(date).toString();
    }

    /**
     * 날짜 비교 함수
     * 날짜 a가 날짜 b 보다 이후 일 경우 true, 아니면 false
     * @param a
     * @param b
     */
    public boolean dateCompaired(String a, String b) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            Date dateA = sdf.parse(a);
            Date dataB = sdf.parse(b);

            return dateA.after(dataB);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

}
