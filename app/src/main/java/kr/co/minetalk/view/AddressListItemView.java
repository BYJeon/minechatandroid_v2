package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.R;
import kr.co.minetalk.databinding.LayoutAddressListItemViewBinding;
import kr.co.minetalk.view.listener.OnDeleteAddressListener;

public class AddressListItemView extends LinearLayout {
    private Context mContext;
    private LayoutAddressListItemViewBinding mBinding;
    private OnDeleteAddressListener mListener;

    private String mData = "";

    public AddressListItemView(Context context) {
        super(context);
        initView(context);
    }

    public AddressListItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public AddressListItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_address_list_item_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);
        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.ivDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null && !mData.equals("")) {
                    mListener.onAddressDelete(mData);
                }
            }
        });
    }

    public void setOnDeleteListener(OnDeleteAddressListener listener) {
        this.mListener = listener;
    }

    public void setData(String data) {
        this.mData = data;
        mBinding.tvTitle.setText(data);
    }

    public String getValue() {
        return mBinding.tvTitle.getText().toString();
    }



}
