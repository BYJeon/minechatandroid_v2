package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.FaqModel;
import kr.co.minetalk.databinding.LayoutFaqViewBinding;

public class FAQItemView extends LinearLayout {

    public enum ItemType {
        ITEM_TYPE_TITLE(1),
        ITEM_TYPE_ODD(2),
        ITEM_TYPE_EVEN(3);

        private int value;

        ItemType(int value) { this.value = value; }

        public int getValue() { return this.value; }
    }

    private Context mContext;

    private LayoutFaqViewBinding mBinding;

    private boolean mIsOpen = false;

    private FaqModel mFAQModel;

    private ItemType mFAQType = ItemType.ITEM_TYPE_ODD;

    public FAQItemView(Context context) {
        super(context);
        initView(context);
    }

    public FAQItemView(Context context, ItemType type) {
        super(context);
        initView(context, type);
    }

    public FAQItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public FAQItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context, ItemType type) {
        this.mContext = context;
        this.mFAQType = type;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_faq_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();

        mBinding.tvNoticeType.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvNoticeTitle.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvDetailContent.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );

        if(type == ItemType.ITEM_TYPE_TITLE){
            mBinding.tvNoticeType.setTypeface(mBinding.tvNoticeType.getTypeface(),Typeface.BOLD);
            mBinding.tvNoticeTitle.setTypeface(mBinding.tvNoticeType.getTypeface(),Typeface.BOLD);
            mBinding.tvNoticeTitle.setGravity(Gravity.CENTER);
            mBinding.ivArrow.setVisibility(View.INVISIBLE);
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
        }else if(type == ItemType.ITEM_TYPE_ODD){
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
            mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );

        }else{
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
            mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
        }
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_faq_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.layoutNoticeHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if( mFAQType == ItemType.ITEM_TYPE_TITLE ) return;
                if(mIsOpen) {
                    closeDetail();
                    mIsOpen = false;
                } else {
                    openDetail();
                    mIsOpen = true;
                }
            }
        });
    }

    private void openDetail() {
        mBinding.ivArrow.setText("▲");
        mBinding.layoutDetail.setVisibility(View.VISIBLE);

    }

    private void closeDetail() {
        mBinding.ivArrow.setText("▼");
        mBinding.layoutDetail.setVisibility(View.GONE);
    }

    public void setData(FaqModel data) {
        this.mFAQModel = data;

        refreshView();
    }

    private void refreshView() {
        if(mFAQModel != null) {
            mBinding.tvNoticeType.setText(mFAQModel.getFaq_type());
            mBinding.tvNoticeTitle.setText(mFAQModel.getFaq_question());
            mBinding.tvDetailContent.setText(mFAQModel.getFaq_answer());

//            String reg_date = CommonUtils.formattedDate(mFAQModel.getReg_date(), "yyyy-MM-dd HH:mm:ss", "yyyy.MM.dd");
//            mBinding.tvRegDate.setText(reg_date);
            mBinding.tvRegDate.setText("");
        }
    }
}
