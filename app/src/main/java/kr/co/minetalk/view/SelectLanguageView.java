package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.R;
import kr.co.minetalk.databinding.LayoutSelectMyLanguageBinding;
import kr.co.minetalk.repository.CountryRepository;
import kr.co.minetalk.utils.Preferences;

public class SelectLanguageView extends LinearLayout {

    private Context mContext;
    private LayoutSelectMyLanguageBinding mBinding;
    private String mSelectLanguage = "";
    public SelectLanguageView(Context context) {
        super(context);
        initView(context);
    }

    public SelectLanguageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public SelectLanguageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_select_my_language, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    public void init() {

        mBinding.iconKor.setData(R.drawable.icon_korea, CountryRepository.getInstance().getCountryModel("ko").getCountryName());
        mBinding.iconCh.setData(R.drawable.icon_china, CountryRepository.getInstance().getCountryModel("zh-CN").getCountryName());
        mBinding.iconEng.setData(R.drawable.icon_english, CountryRepository.getInstance().getCountryModel("en").getCountryName());
        mBinding.iconJa.setData(R.drawable.icon_japan, CountryRepository.getInstance().getCountryModel("ja").getCountryName());
        mBinding.iconVi.setData(R.drawable.icon_vietnam, CountryRepository.getInstance().getCountryModel("vi").getCountryName());

        String myLanguage = Preferences.getMyChatLanguage();
        mSelectLanguage = Preferences.getMyChatLanguage();
        if(myLanguage.equals("ko")) {
            mBinding.iconKor.select();
            mBinding.iconCh.unSelect();
            mBinding.iconEng.unSelect();
            mBinding.iconJa.unSelect();
            mBinding.iconVi.unSelect();
        } else if(myLanguage.equals("zh-CN")) {
            mBinding.iconKor.unSelect();
            mBinding.iconCh.select();
            mBinding.iconEng.unSelect();
            mBinding.iconJa.unSelect();
            mBinding.iconVi.unSelect();
        } else if(myLanguage.equals("en")) {
            mBinding.iconKor.unSelect();
            mBinding.iconCh.unSelect();
            mBinding.iconEng.select();
            mBinding.iconJa.unSelect();
            mBinding.iconVi.unSelect();
        } else if(myLanguage.equals("ja")) {
            mBinding.iconKor.unSelect();
            mBinding.iconCh.unSelect();
            mBinding.iconEng.unSelect();
            mBinding.iconJa.select();
            mBinding.iconVi.unSelect();
        } else if(myLanguage.equals("vi")) {
            mBinding.iconKor.unSelect();
            mBinding.iconCh.unSelect();
            mBinding.iconEng.unSelect();
            mBinding.iconJa.unSelect();
            mBinding.iconVi.select();
        }

    }

    private void setUIEventListener() {
        mBinding.layoutRoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(View.GONE);
            }
        });

        mBinding.btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(View.GONE);
            }
        });

        mBinding.btnConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setMyChatLanguage(mSelectLanguage);
                setVisibility(View.GONE);
            }
        });



        mBinding.iconKor.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.select();
                mBinding.iconCh.unSelect();
                mBinding.iconEng.unSelect();
                mBinding.iconJa.unSelect();
                mBinding.iconVi.unSelect();

                mSelectLanguage = "ko";
            }
        });

        mBinding.iconCh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.unSelect();
                mBinding.iconCh.select();
                mBinding.iconEng.unSelect();
                mBinding.iconJa.unSelect();
                mBinding.iconVi.unSelect();

                mSelectLanguage = "zh-CN";
            }
        });

        mBinding.iconEng.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.unSelect();
                mBinding.iconCh.unSelect();
                mBinding.iconEng.select();
                mBinding.iconJa.unSelect();
                mBinding.iconVi.unSelect();

                mSelectLanguage = "en";
            }
        });

        mBinding.iconJa.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.unSelect();
                mBinding.iconCh.unSelect();
                mBinding.iconEng.unSelect();
                mBinding.iconJa.select();
                mBinding.iconVi.unSelect();

                mSelectLanguage = "ja";
            }
        });

        mBinding.iconVi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.iconKor.unSelect();
                mBinding.iconCh.unSelect();
                mBinding.iconEng.unSelect();
                mBinding.iconJa.unSelect();
                mBinding.iconVi.select();

                mSelectLanguage = "vi";
            }
        });
    }
}
