package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.R;
import kr.co.minetalk.databinding.LayoutCheckBarButtonBinding;
import kr.co.minetalk.view.listener.OnCheckBarEventListener;

public class CheckBarButtonView extends LinearLayout {

    private Context mContext;
    private LayoutCheckBarButtonBinding mBinding;
    private OnCheckBarEventListener mListener;

    private String mValue;
    private String mType;

    public CheckBarButtonView(Context context) {
        super(context);
        initView(context);
    }

    public CheckBarButtonView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CheckBarButtonView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }


    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_check_bar_button, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);
        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.layoutRoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !mBinding.ivChecked.isSelected();
                mBinding.ivChecked.setSelected(isSelect);

                if(mListener != null) {
                    mListener.onChecked(isSelect, mValue, mType);
                }
            }
        });
    }

    public void setType(String type) {
        this.mType = type;
    }


    public void setCheckBarEventListener(OnCheckBarEventListener listener) {
        this.mListener = listener;
    }

    public void setData(String buttonTitle, String value) {
        this.mBinding.tvTitle.setText(buttonTitle);
        this.mValue = value;
    }

    public void setSelected(boolean isSelect) {
        mBinding.ivChecked.setSelected(isSelect);
    }

    public boolean isSelect() {
        return mBinding.ivChecked.isSelected();
    }

    public String getValue() {
        return mValue;
    }






}
