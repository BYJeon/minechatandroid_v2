package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.NoticeModel;
import kr.co.minetalk.databinding.LayoutNoticeBinding;
import kr.co.minetalk.utils.CommonUtils;

public class NoticeItemView extends LinearLayout {
    private Context mContext;

    private LayoutNoticeBinding mBinding;

    private boolean mIsOpen = false;

    private NoticeModel mNoticeModel;

    public NoticeItemView(Context context, FAQItemView.ItemType type) {
        super(context);
        initView(context, type);
    }

    public NoticeItemView(Context context) {
        super(context);
        initView(context);
    }

    public NoticeItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public NoticeItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_notice, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void initView(Context context, FAQItemView.ItemType type) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_notice, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();

        mBinding.tvNoticeTitle.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvRegDate.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvNoticeTitle.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvDetailContent.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );

        if(type == FAQItemView.ItemType.ITEM_TYPE_EVEN){
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
            mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
        }else{
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
            mBinding.layoutDetail.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
            //mBinding.layoutNoticeHeader.setBackgroundColor(Color.parseColor("#F8F8F8"));
            //mBinding.layoutDetail.setBackgroundColor(Color.parseColor("#F8F8F8"));
        }
    }

    private void setUIEventListener() {
        mBinding.layoutNoticeHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mIsOpen) {
                    closeDetail();
                    mIsOpen = false;
                } else {
                    openDetail();
                    mIsOpen = true;
                }
            }
        });
    }

    private void openDetail() {
        mBinding.ivArrow.setText("▲");
        mBinding.layoutDetail.setVisibility(View.VISIBLE);

    }

    private void closeDetail() {
        mBinding.ivArrow.setText("▼");
        mBinding.layoutDetail.setVisibility(View.GONE);
    }

    public void setData(NoticeModel data) {
        this.mNoticeModel = data;

        refreshView();
    }

    private void refreshView() {
        if(mNoticeModel != null) {
            mBinding.tvNoticeTitle.setText(mNoticeModel.getNotice_title());
            mBinding.tvDetailContent.setText(mNoticeModel.getNotice_contents());

            String reg_date = CommonUtils.formattedDate(mNoticeModel.getReg_date(), "yyyy-MM-dd HH:mm:ss", "yyyy.MM.dd");
            mBinding.tvRegDate.setText(reg_date);
        }
    }
}
