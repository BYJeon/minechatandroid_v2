package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.MineTokenHistoryModel;
import kr.co.minetalk.databinding.LayoutTokenPointViewBinding;

public class PointTokenItemView extends LinearLayout {

    public enum ItemType {
        ITEM_TYPE_TITLE(1),
        ITEM_TYPE_TOTAL(2),
        ITEM_TYPE_ODD(3),
        ITEM_TYPE_EVEN(4);

        private int value;

        ItemType(int value) { this.value = value; }

        public int getValue() { return this.value; }
    }

    private Context mContext;

    private LayoutTokenPointViewBinding mBinding;

    private boolean mIsOpen = false;

    private MineTokenHistoryModel mTokenModel;

    private ItemType mTokenType = ItemType.ITEM_TYPE_ODD;

    public PointTokenItemView(Context context) {
        super(context);
        initView(context);
    }

    public PointTokenItemView(Context context, ItemType type) {
        super(context);
        initView(context, type);
    }

    public PointTokenItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PointTokenItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context, ItemType type) {
        this.mContext = context;
        this.mTokenType = type;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_token_point_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();

        mBinding.tvPointType.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvPointTitle.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvPointAmount.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );

        if(type == ItemType.ITEM_TYPE_TITLE){
            mBinding.tvPointType.setTypeface(mBinding.tvPointType.getTypeface(),Typeface.BOLD);
            mBinding.tvPointTitle.setTypeface(mBinding.tvPointTitle.getTypeface(),Typeface.BOLD);
            mBinding.tvPointAmount.setGravity(Gravity.CENTER);
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
        }else if(type == ItemType.ITEM_TYPE_TOTAL){
            //mBinding.tvPointTitle.setGravity(Gravity.LEFT);
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
            mBinding.tvPointAmount.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
        } else if(type == ItemType.ITEM_TYPE_ODD){
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
            mBinding.tvPointAmount.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
        }else{
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
            mBinding.tvPointAmount.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
        }
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_cash_point_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {

    }



    public void setData(MineTokenHistoryModel data) {
        this.mTokenModel = data;

        refreshView();
    }

    private void refreshView() {
        if(mTokenModel != null) {
            mBinding.tvPointType.setText(mTokenModel.getToken_type());
            if(!mTokenModel.getEnd_date().equals(""))
                mBinding.tvPointTitle.setText("기한: " + mTokenModel.getEnd_date());
            else
                mBinding.tvPointTitle.setText(mTokenModel.getEnd_date());

            mBinding.tvPointAmount.setText(mTokenModel.getPoint_amount());
        }
    }
}
