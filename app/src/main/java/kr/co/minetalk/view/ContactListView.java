package kr.co.minetalk.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.databinding.LayoutContactListViewBinding;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class ContactListView extends LinearLayout {

    private Context mContext;
    private LayoutContactListViewBinding mBinding;
    private OnFriendListViewListener mListener;

    private ContactModel mData;

    public ContactListView(Context context) {
        super(context);
        initView(context);
    }

    public ContactListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ContactListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public void setOnItemListener(OnFriendListViewListener listener) {
        this.mListener = listener;
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_contact_list_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.layoutRoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    FriendListModel data = new FriendListModel();
                    data.setUser_name(mData.getUser_name());
                    data.setUser_hp(mData.getUser_phone());
                    mListener.onClickItem(data);
                }
            }
        });

        mBinding.btnSendSmsInvite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String sendUserName = MineTalkApp.getUserInfoModel().getUser_name();
                String sendUserHp = MineTalkApp.getUserInfoModel().getUser_hp();
                String msg = String.format(mContext.getResources().getString(R.string.invite_sms_msg),
                        sendUserName,
                        sendUserHp);

                sendSMS(mContext, mData.getUser_phone(), msg);
            }
        });
    }

    public void setData(ContactModel data) {
        this.mData = data;
        refreshView();
    }

    private void refreshView() {
        if(mData != null) {
            String userName = mData.getUser_name();
            mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
            mBinding.tvUserName.setText(userName);
            mBinding.tvUserPhone.setText(mData.getUser_phone());
            Glide.with(mContext).load(R.drawable.icon_phone_basic).into(mBinding.ivUserImage);
        }
    }

    private void sendSMS(Context context, String phone, String msg) {
        try {
            Uri smsUri = Uri.parse("sms:" + phone);
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, smsUri);
            sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            sendIntent.putExtra("sms_body", msg);
            context.startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
