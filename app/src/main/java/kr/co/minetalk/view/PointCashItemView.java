package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.CashModel;
import kr.co.minetalk.api.model.FaqModel;
import kr.co.minetalk.databinding.LayoutCashPointViewBinding;

public class PointCashItemView extends LinearLayout {

    public enum ItemType {
        ITEM_TYPE_TITLE(1),
        ITEM_TYPE_TOTAL(2),
        ITEM_TYPE_ODD(3),
        ITEM_TYPE_EVEN(4);

        private int value;

        ItemType(int value) { this.value = value; }

        public int getValue() { return this.value; }
    }

    private Context mContext;

    private LayoutCashPointViewBinding mBinding;

    private boolean mIsOpen = false;

    private CashModel mCashModel;

    private ItemType mFAQType = ItemType.ITEM_TYPE_ODD;

    public PointCashItemView(Context context) {
        super(context);
        initView(context);
    }

    public PointCashItemView(Context context, ItemType type) {
        super(context);
        initView(context, type);
    }

    public PointCashItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PointCashItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context, ItemType type) {
        this.mContext = context;
        this.mFAQType = type;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_cash_point_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();

        mBinding.tvPointType.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvPointTitle.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );
        mBinding.tvPointAmount.setTextColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.white_color) : getResources().getColor(R.color.main_text_color) );

        if(type == ItemType.ITEM_TYPE_TITLE){
            mBinding.tvPointType.setTypeface(mBinding.tvPointType.getTypeface(),Typeface.BOLD);
            mBinding.tvPointTitle.setTypeface(mBinding.tvPointTitle.getTypeface(),Typeface.BOLD);
            mBinding.tvPointAmount.setGravity(Gravity.CENTER);
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
        }else if(type == ItemType.ITEM_TYPE_TOTAL){
            //mBinding.tvPointTitle.setGravity(Gravity.LEFT);
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
            mBinding.tvPointAmount.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
        } else if(type == ItemType.ITEM_TYPE_ODD){
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
            mBinding.tvPointAmount.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : Color.parseColor("#F8F8F8") );
        }else{
            mBinding.layoutNoticeHeader.setBackgroundColor( MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
            mBinding.tvPointAmount.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : Color.parseColor("#FFFFFF") );
        }
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_cash_point_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {

    }



    public void setData(CashModel data) {
        this.mCashModel = data;

        refreshView();
    }

    private void refreshView() {
        if(mCashModel != null) {
            mBinding.tvPointType.setText(mCashModel.getCash_type());
            mBinding.tvPointTitle.setText(mCashModel.getCash_content());
            mBinding.tvPointAmount.setText(mCashModel.getCash_amount());
        }
    }
}
