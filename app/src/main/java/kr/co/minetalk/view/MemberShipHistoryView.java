package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.AdMessageModel;
import kr.co.minetalk.api.model.MemberShipHistoryModel;
import kr.co.minetalk.databinding.LayoutAdMessageReceiveBinding;
import kr.co.minetalk.databinding.MembershipListItemBinding;

public class MemberShipHistoryView extends LinearLayout {

    private Context mContext;
    private MembershipListItemBinding mBinding;

    private MemberShipHistoryModel mData;
    private SimpleDateFormat mDtFormat = new SimpleDateFormat("HH:mm");
    private FAQItemView.ItemType mItemType = FAQItemView.ItemType.ITEM_TYPE_TITLE;

    public MemberShipHistoryView(Context context) {
        super(context);
        initView(context);
    }

    public MemberShipHistoryView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public MemberShipHistoryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;
        View v = LayoutInflater.from(context).inflate(R.layout.membership_list_item, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);
    }

    public void setData(MemberShipHistoryModel data, FAQItemView.ItemType type) {
        this.mData = data;
        this.mItemType = type;

        refreshView();
    }

    private void refreshView() {
        if(mData != null) {

            String regDate = mData.getReg_date();
            String updData = mData.getUpd_date();

            mBinding.tvDate.setText(regDate.substring(0, 10));
            mBinding.tvDateTime.setText(regDate.substring(11));

            if(updData.length()>0){
                mBinding.tvCompleteDate.setText(updData.substring(0, 10));
                mBinding.tvCompleteDateTime.setText(updData.substring(11));
            }

            mBinding.tvPointText.setText(mData.getPoint_text());
            mBinding.tvPointAmount.setText(mData.getPoint_amount());
            mBinding.tvStatus.setText(mData.getStatus_text());

            if(mItemType == FAQItemView.ItemType.ITEM_TYPE_TITLE){
                mBinding.layoutRoot.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_status) : getResources().getColor(R.color.white_color));

                mBinding.tvDateTitle.setVisibility(View.VISIBLE);
                mBinding.tvContentTitle.setVisibility(View.VISIBLE);
                mBinding.tvUpdTitle.setVisibility(View.VISIBLE);
                mBinding.tvStatusTitle.setVisibility(View.VISIBLE);

                mBinding.tvDate.setVisibility(View.INVISIBLE);
                mBinding.tvDateTime.setVisibility(View.INVISIBLE);

                mBinding.tvCompleteDate.setVisibility(View.INVISIBLE);
                mBinding.tvCompleteDateTime.setVisibility(View.INVISIBLE);

                mBinding.tvPointText.setVisibility(View.INVISIBLE);
                mBinding.tvPointAmount.setVisibility(View.INVISIBLE);
                mBinding.tvStatus.setVisibility(View.INVISIBLE);

            }else if(mItemType == FAQItemView.ItemType.ITEM_TYPE_ODD){
                mBinding.layoutRoot.setBackgroundColor(MineTalk.isBlackTheme ? getResources().getColor(R.color.bk_theme_bg) : getResources().getColor(R.color.main_theme_bg));

                mBinding.tvDateTitle.setVisibility(View.INVISIBLE);
                mBinding.tvContentTitle.setVisibility(View.INVISIBLE);
                mBinding.tvUpdTitle.setVisibility(View.INVISIBLE);
                mBinding.tvStatusTitle.setVisibility(View.INVISIBLE);

            }else{
                mBinding.tvDateTitle.setVisibility(View.INVISIBLE);
                mBinding.tvContentTitle.setVisibility(View.INVISIBLE);
                mBinding.tvUpdTitle.setVisibility(View.INVISIBLE);
                mBinding.tvStatusTitle.setVisibility(View.INVISIBLE);
            }

//
//
//            String contentImage = mData.getAd_image_url();
//            if(contentImage != null && !contentImage.equals("")) {
//                mBinding.ivContentsImage.setVisibility(View.VISIBLE);
//                Glide.with(mContext).load(contentImage).into(mBinding.ivContentsImage);
//            } else {
//                mBinding.ivContentsImage.setVisibility(View.GONE);
//            }


        }
    }




}
