package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.HashMap;

import kr.co.minetalk.R;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.databinding.LayoutUserIconViewBinding;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class UserIconView extends LinearLayout {

    private Context mContext;
    private FriendListModel mData;

    private LayoutUserIconViewBinding mBinding;
    private OnFriendListViewListener mListener;

    public UserIconView(Context context) {
        super(context);
        initView(context);
    }

    public UserIconView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public UserIconView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;
        View v = LayoutInflater.from(context).inflate(R.layout.layout_user_icon_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);
        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.layoutRoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onClickItem(mData);
                }
            }
        });
    }

    public void setOnItemListener(OnFriendListViewListener listener) {
        this.mListener = listener;
    }

    public void setData(FriendListModel data) {
        this.mData = data;

        refreshView();
    }

    private void refreshView() {
        if(mData != null) {
            String profileImgUrl = mData.getUser_profile_image();
            if(profileImgUrl == null || profileImgUrl.equals("")) {
                Glide.with(mContext).load(R.drawable.img_basic).into(mBinding.ivUserImage);
            } else {
                Glide.with(mContext).load(mData.getUser_profile_image()).into(mBinding.ivUserImage);
            }


            HashMap<String, String> contactMap = new HashMap<>();
            contactMap = FriendManager.getInstance().selectFriendXidToNameMap();

            String contactName = contactMap.get(mData.getUser_xid());
            if(contactName == null || contactMap.equals("")) {
                mBinding.tvUserName.setText(mData.getUser_name());
            } else {
                mBinding.tvUserName.setText(contactName);
            }

        }
    }
}
