package kr.co.minetalk.view;

import android.content.Context;
import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.IOException;
import java.io.InputStream;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.LayoutFriendListViewOrgBinding;
import kr.co.minetalk.view.listener.OnFriendListViewListener;

public class FriendListView extends LinearLayout {

    public static enum ViewType {
        NORMAL,
        RECOMMEND,
        ME
    }

    private Context mContext;
    private LayoutFriendListViewOrgBinding mBinding;

    private FriendListModel mData;
    private UserInfoBaseModel mUserInfoData;
    private OnFriendListViewListener mListener;

    private ViewType mViewType = ViewType.NORMAL;

    public FriendListView(Context context) {
        super(context);
        initView(context);
    }

    public FriendListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public FriendListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_friend_list_view_org, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.layoutRoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    if(mViewType.equals(ViewType.ME)) {
                        mListener.showMyProfile(mUserInfoData);
                    } else {
                        mListener.onClickItem(mData);
                    }

                }
            }
        });

        mBinding.btnUserAdd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.addFriend(mData);
                }
            }
        });
    }

    public void setViewType(ViewType viewType) {
        this.mViewType = viewType;
    }

    public void setOnItemListener(OnFriendListViewListener listener) {
        this.mListener = listener;
    }

    public void setUserInfoData(UserInfoBaseModel data) {
        this.mUserInfoData = data;

        refreshViewMe();
    }
    public void setData(FriendListModel data) {
        this.mData = data;
        refreshView();
    }

    private void refreshView() {
        if(mViewType == ViewType.NORMAL || mViewType == ViewType.ME) {
            mBinding.btnUserAdd.setVisibility(View.GONE);
        } else {
            mBinding.btnUserAdd.setVisibility(View.VISIBLE);
        }

        if(mData != null) {
            String profileImgUrl = mData.getUser_profile_image();


            if(profileImgUrl == null || profileImgUrl.equals("")) {
                Glide.with(mContext).load(R.drawable.img_basic).into(mBinding.ivUserImage);
            } else {
                Glide.with(mContext).load(mData.getUser_profile_image()).into(mBinding.ivUserImage);
            }

            //연락처에서 이름을 가져 온다.
            //String contactName = MineTalkApp.getContactUserName(mData.getUser_hp());

            //String contactName = CommonUtils.getContactNameByNum( getContext(), mData.getUser_hp());
//            String contactName = MineTalkApp.getContactUserName(mData.getUser_hp());
//            mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
//            if(contactName != null && !contactName.equals(""))
//                mBinding.tvUserName.setText(contactName);
//            else
//                mBinding.tvUserName.setText(mData.getUser_name());

            mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
            mBinding.tvUserName.setText(MineTalkApp.getUserNameByPhoneNum(mData.getUser_name(), mData.getUser_hp()));

            String userXidMe = MineTalkApp.getUserInfoModel().getUser_xid();

            //본인 사용자일 경우
            if(userXidMe.equals(mData.getUser_xid())) {
                UserInfoBaseModel userInfoMe = MineTalkApp.getUserInfoModel();
                mBinding.tvUserName.setText(userInfoMe.getUser_name());
            }


            if(mData.getUser_state_message().equals("")) {
                mBinding.tvStateMessage.setVisibility(View.GONE);
                mBinding.tvStateMessage.setText(mData.getUser_state_message());
            } else {
                mBinding.tvStateMessage.setVisibility(View.VISIBLE);
                mBinding.tvStateMessage.setText(mData.getUser_state_message());
            }

            String nationCode = mData.getUser_nation_code();

            if(nationCode != null && !nationCode.equals("")) {
                try {
                    AssetManager assetManager = mContext.getResources().getAssets();
                    InputStream inputStream = assetManager.open("flag/" + nationCode + ".jpg");
                    mBinding.ivNationFlag.setImageDrawable(Drawable.createFromStream(inputStream, null));
                    inputStream.close();
                    mBinding.ivNationFlag.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                    mBinding.ivNationFlag.setVisibility(View.GONE);
                }
            } else {
                mBinding.ivNationFlag.setVisibility(View.GONE);
            }
        }
    }

    private void refreshViewMe() {
        mBinding.btnUserAdd.setVisibility(View.GONE);
        if(mUserInfoData != null) {
            String profileImgUrl = mUserInfoData.getUser_profile_image();
            if(profileImgUrl == null || profileImgUrl.equals("")) {
                Glide.with(mContext).load(R.drawable.img_basic).into(mBinding.ivUserImage);
            } else {
                Glide.with(mContext).load(profileImgUrl).into(mBinding.ivUserImage);
            }
            mBinding.tvUserName.setTextColor(MineTalkApp.getAppContext().getResources().getColor( MineTalk.isBlackTheme ? R.color.white_color : R.color.main_text_color));
            mBinding.tvUserName.setText(mUserInfoData.getUser_name());

            if(mUserInfoData.getUser_state_message().equals("")) {
                mBinding.tvStateMessage.setVisibility(View.GONE);
                mBinding.tvStateMessage.setText(StringEscapeUtils.unescapeJava(mUserInfoData.getUser_state_message()));
            } else {
                mBinding.tvStateMessage.setVisibility(View.VISIBLE);
                mBinding.tvStateMessage.setText(StringEscapeUtils.unescapeJava(mUserInfoData.getUser_state_message()));
            }
        }
    }

}
