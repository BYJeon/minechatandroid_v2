package kr.co.minetalk.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.co.minetalk.R;
import kr.co.minetalk.databinding.LayoutAdImageViewBinding;
import kr.co.minetalk.databinding.LayoutQnaImageViewBinding;
import kr.co.minetalk.view.listener.OnQnAListViewListener;

public class AdImageListView extends LinearLayout {


    private Context mContext;
    private LayoutAdImageViewBinding mBinding;

    private String mFilePath;
    private OnQnAListViewListener mListener;


    public AdImageListView(Context context) {
        super(context);
        initView(context);
    }

    public AdImageListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public AdImageListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(context).inflate(R.layout.layout_ad_image_view, this, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.setView(this);

        addView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {

        mBinding.btnDel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onClickItem(mFilePath);
                }
            }
        });

    }


    public void setOnItemListener(OnQnAListViewListener listener) {
        this.mListener = listener;
    }

    public void setData(String path) {
        this.mFilePath = path;
        refreshView();
    }

    public String getData() { return mFilePath; }

    private void refreshView() {
        //임시 파일 수정
        String tmp = mFilePath;
        int index = tmp.lastIndexOf("/");
        tmp = tmp.substring(index+1);
        mBinding.tvFileName.setText(tmp);
    }


}
