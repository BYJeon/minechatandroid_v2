package kr.co.minetalk;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.widget.Toast;

import kr.co.minetalk.locker.PageListener;
import kr.co.minetalk.message.model.MessageData;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;

public class MineTalk {

    public static enum ServerType{
        API, CHAT, SHOP
    }

    public static final boolean isDebug = false;
    public static boolean isBlackTheme = false;
    public static PageListener pageListener = null;
    public static MessageData mTransmissionData = null;

    private static final String API_SERVER  = "https://app.minetalk.co.kr";
    private static final String CHAT_SERVER = "https://chat.minetalk.co.kr:8888";
    private static final String SHOP_SERVER = "https://shop.minetalk.co.kr";
    private static final String GAME_BLOCK_SERVER  = "http://game-blockpack.minetalk.co.kr/";
    private static final String GAME_TYCOON_SERVER = "http://game-tycoon.minetalk.co.kr/";
    private static final String GAME_ROCK_PAPER_SCISSORS_SERVER  = "http://game-rps.minetalk.co.kr/?live=Y";
    private static final String GAME_BASKETBALL_SERVER = "http://game-basketball.minetalk.co.kr/?live=Y";

    private static final String API_SERVER_DEV  = "https://tapp.minetalk.co.kr";
    private static final String CHAT_SERVER_DEV = "https://tchat.minetalk.co.kr:8888";
    private static final String SHOP_SERVER_DEV = "https://tshop.minetalk.co.kr";
    private static final String GAME_BLOCK_SERVER_DEV  = "http://game-blockpack.minetalk.co.kr/dev.html";
    private static final String GAME_TYCOON_SERVER_DEV = "http://game-tycoon.minetalk.co.kr/dev.html";
    private static final String GAME_ROCK_PAPER_SCISSORS_DEV  = "http://game-rps.minetalk.co.kr/?live=N";
    private static final String GAME_BASKETBALL_SERVER_DEV = "http://game-basketball.minetalk.co.kr/?live=N";

    private static final String USER_PLATFORM = "android";

    public static final String SMS_TYPE_REGIST    = "regist";      // 회원가입
    public static final String SMS_TYPE_PWINQUIRY = "pwinquiry";   // 비밀번호 찾기
    public static final String SMS_TYPE_SENDPOINT = "sendpoint";   // 포인트 보내기
    public static final String SMS_TYPE_SECESSION = "secession";   // 회원탈퇴
    public static final String SMS_TYPE_EXCHANGE  = "exchange";    // 토큰 이체 요청
    public static final String SMS_TYPE_JOINTYPE  = "jointype";    // 계정 전환 신청
    public static final String SMS_TYPE_NEXTGEAR  = "nextgear";    // NextGear 신청

    public static final String REGIST_TYPE_H = "H"; // 전화번호
    public static final String REGIST_TYPE_S = "S"; // SNS
    public static final String REGIST_TYPE_I = "I"; // 아이디

    public static final String REGIST_TYPE_G = "G"; // 구글
    public static final String REGIST_TYPE_F = "F"; // 페이스북
    public static final String REGIST_TYPE_K = "K"; // 카카오

    public static final String LOGIN_TYPE_APP      = "A";    // App
    public static final String LOGIN_TYPE_IP      = "IP";    // Id, pass
    public static final String LOGIN_TYPE_GOOGLE   = "G";    // 구글
    public static final String LOGIN_TYPE_FACEBOOK = "F";    // 페이스북
    public static final String LOGIN_TYPE_KAKAO    = "K";    // 카카오

    public static final String X_LOGIN_TYPE       = "T";    // REST Api 호출시 login type

    public static final int REQUEST_CODE_REGIST = 2001;

    public static final int REQUEST_CODE_GOOGLE_LOGIN = 9000;
    public static final int REQUEST_CODE_FACEBOOK_LOGIN = 9001;
    public static final int REQUEST_CODE_KAKAO_LOGIN    = 9002;

    public static final int REQUEST_CODE_CHAT_OPTION = 7001;
    public static final int REQUEST_CODE_SELECT_FRIEND = 7002;
    public static final int REQUEST_CODE_SELECT_ROOM_FRIEND = 7003;

    public static final int REQUEST_CODE_STT_TOP    = 8001;
    public static final int REQUEST_CODE_STT_BOTTOM = 8002;

    public static final int REQUEST_CODE_MEDIA        = 9999;
    public static final int REQUEST_CODE_TARGET        = 6001;

    public static final int REQUEST_CODE_PICTURE = 8100;
    public static final int REQUEST_CODE_FILE    = 8110;
    public static final int REQUEST_CODE_CAMERA  = 8200;
    public static final int REQUEST_CODE_MOV     = 8300;
    public static final int REQUEST_CODE_CONTACT = 8400;


    public static final int REQUEST_CODE_STT = 8500;
    public static final int REQUEST_CODE_TRANSMISSION = 8600;
    public static int CONTACT_RANGE = 30;


    public static final String MSG_TYPE_TEXT    = "TEXT";
    public static final String MSG_TYPE_IMAGE   = "IMAGE";
    public static final String MSG_TYPE_VIDEO   = "VIDEO";
    public static final String MSG_TYPE_CONTACT = "CONTACT";
    public static final String MSG_TYPE_URL     = "URL";
    public static final String MSG_TYPE_FILE    = "FILE";

    public static final String MSG_TYPE_INVITE = "INVITE";
    public static final String MSG_TYPE_USER_EXIT = "USER_EXIT";

    public static final String AD_LIST_TYPE_S = "S";    // 보낸 내역
    public static final String AD_LIST_TYPE_R = "R";    // 받은 내역

    public static final String ARG_SNS_USER_NAME  = "arg_sns_user_name";
    public static final String ARG_SNS_USER_EMAIL = "arg_sns_user_email";
    public static final String ARG_SNS_ID         = "arg_sns_id";
    public static final String ARG_SNS_USER_PROFILE_IMG = "arg_sns_userr_profile_img";

    public static final String POINT_TYPE_CASH  = "mine_cash";
    public static final String POINT_TYPE_TOKEN = "mine_token";

    public static final String POINT_TYPE_WITHDRAW  = "point_withdraw"; // 출금내역
    public static final String POINT_TYPE_DEPOSIT  = "point_deposit";   // 입금내역

    public static final String BACKGROUND_TYPE_IMAGE = "image";
    public static final String BACKGROUND_TYPE_COLOR = "color";

    public static final String BROAD_CAST_NEW_MESSAGE = "kr.co.minetalk.new_message";
    public static final String BROAD_CAST_DELETE_MESSAGE = "kr.co.minetalk.delete_message";
    public static final String BROAD_CAST_SECURITY_CHAT_OUT_MESSAGE = "kr.co.minetalk.security_chat_out_message";
    public static final String BROAD_CAST_FINGER_MESSAGE = "kr.co.minetalk.finger_message";
    public static final String BROAD_CAST_LOCK_SCREEN_PAUSE_MESSAGE = "kr.co.minetalk.lock_screen_pause";

    public static final String FINGER_LOCK_RESULT    = "finger_lock_result";
    public static final String FINGER_LOCK_MESSAGE    = "finger_lock_message";
    public static final String FINGER_LOCK_RESTART    = "finger_lock_restart";

    public static  String PUSH_SERVICE_THREAD_KEY = "";
    public static  String PUSH_SERVICE_SECURITY_KEY = "";

    public static  String PUSH_THREAD_KEY = "";
    public static  String PUSH_DELETE_THREAD_KEY = "";
    public static  String PUSH_DELETE_SEQ = "";
    //public static  String PUSH_SECURITY_THREAD_KEY = "";
    public static  String AD_IDX = "";
    public static  String AD_TITLE = "";

    public static String SHARED_MESSAGE = "";
    public static String SHARED_TEXT_SELECTED_MESSAGE = "";

    public static String SHARED_IMAGE_MESSAGE = "";
    public static Uri SHARED_IMG_URI = null;

    public static String getServer(ServerType type) {
        if(type == ServerType.API) {
            return getApiServer();
        } else if(type == ServerType.CHAT) {
            return getChatServer();
        } else if(type == ServerType.SHOP) {
            return getShopServer();
        } else {
            return getApiServer();
        }
    }

    public static String getApiServer() {
        if(MineTalk.isDebug) {
            return API_SERVER_DEV;
        } else {
            return API_SERVER;
        }
    }

    public static String getChatServer() {
        if(MineTalk.isDebug) {
            return CHAT_SERVER_DEV;
        } else {
            return CHAT_SERVER;
        }
    }

    public static String getShopServer() {
        if(MineTalk.isDebug) {
            return SHOP_SERVER_DEV;
        } else {
            return SHOP_SERVER;
        }
    }

    public static String getGameBlockServer() {
        if(MineTalk.isDebug) {
            return GAME_BLOCK_SERVER_DEV;
        } else {
            return GAME_BLOCK_SERVER;
        }
    }

    public static String getGameTycoonServer() {
        if(MineTalk.isDebug) {
            return GAME_TYCOON_SERVER_DEV;
        } else {
            return GAME_TYCOON_SERVER;
        }
    }

    public static String getGameRockPaperScissorsServer() {
        if(MineTalk.isDebug) {
            return GAME_ROCK_PAPER_SCISSORS_DEV;
        } else {
            return GAME_ROCK_PAPER_SCISSORS_SERVER;
        }
    }

    public static String getGameBasketBallServer() {
        if(MineTalk.isDebug) {
            return GAME_BASKETBALL_SERVER_DEV;
        } else {
            return GAME_BASKETBALL_SERVER;
        }
    }

    public static String getUserPlatform() {
        return USER_PLATFORM;
    }

    /**
     * 클립보드에 주소 복사 기능
     * @param context
     * @param link
     */
    public static void setClipBoardLink(Context context , String link){

        ClipboardManager clipboardManager = (ClipboardManager)context.getSystemService(context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("label", link);
        clipboardManager.setPrimaryClip(clipData);
        Toast.makeText(context, "클립보드에 복사되었습니다.", Toast.LENGTH_SHORT).show();

    }

    public static String getClipBoardLink(Context context){
        ClipboardManager clipboard = (ClipboardManager)context.getSystemService(Context.CLIPBOARD_SERVICE);
        String pasteData = "";

        // 클립보드에 데이터가 없거나 텍스트 타입이 아닌 경우
        if (!(clipboard.hasPrimaryClip())){
        }
        else if (!(clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {
        }
        else {
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
            pasteData = item.getText().toString();
        }

        return pasteData;
    }
}
