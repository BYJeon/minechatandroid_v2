package kr.co.minetalk.utils;

import android.app.Application;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.view.ContextThemeWrapper;

import java.util.Locale;

public class LocaleUtils {

    /*
    Locale.ENGLISH // English
    Locale.KOREA // Korea
    Locale.JAPAN // Japan
    Locale.SIMPLIFIED_CHINESE // China (간체)
    Locale.TRADITIONAL_CHINESE // China (번체)

    KO - 한국어
    EN - 영어
    JP - 일본어
    SC - 중국어 간체 (Simplified Chinese)
    TC - 중국어 번체 (Traditional Chinese)
     */

    private static Locale mLocale;

    public static void setLocale(Locale locale) {
        mLocale = locale;
        if (mLocale != null) {
            Locale.setDefault(mLocale);
        }
    }

    public static void updateConfig(ContextThemeWrapper wrapper) {
        if (mLocale != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Configuration configuration = new Configuration();
            configuration.setLocale(mLocale);
            wrapper.applyOverrideConfiguration(configuration);
        }
    }

    public static void updateConfig(Application app, Configuration configuration) {
        if (mLocale != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Configuration config = new Configuration(configuration);
            config.locale = mLocale;
            Resources res = app.getBaseContext().getResources();
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
    }
}
