package kr.co.minetalk.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Preferences {
    private static final String TAG = "Preferences";
    private static final String NAME = "minetalk";

    private static final String KEY_LANGUAGE = "user_language";

    private static final String KEY_LOGIN_AUTHORIZATION = "login_authorization";
    private static final String KEY_AUTHORIZATION = "authorization";
    private static final String KEY_X_LOGIN_TYPE  = "x_login_type";

    private static final String KEY_MEMBER_DEVICE_ID = "key_member_device_id";
    private static final String KEY_PUSH_REGISTRATION_ID = "key_push_registration_id";

    private static final String KEY_NATION_CODE = "key_nation_code";

    private static final String KEY_USER_PASSWORD = "key_user_password";

    private static final String KEY_VOICE_TRANSLATION_TOP = "key_voice_translation_top";
    private static final String KEY_VOICE_TRANSLATION_BOTTOM = "key_voice_translation_bottom";

    private static final String KEY_MY_CHAT_LANGUAGE = "key_my_chat_launguage";

    private static final String KEY_CHAT_TRANSLATION_USE = "key_translation_use";
    private static final String KEY_CHAT_TRANSLATION_LANGUAGE_CODE = "key_chat_trans";

    private static final String KEY_CHAT_BACKGROUND_COLOR = "key_chat_background_color";
    private static final String KEY_CHAT_BACKGROUND_IMAGE = "key_chat_background_image";
    private static final String KEY_CHAT_BACKGROUND_TYPE  = "key_chat_background_type";

    private static final String KEY_CAMERA_TRANSLATION_SOURCE_CODE = "key_camera_translation_source_code";
    private static final String KEY_CAMERA_TRANSLATION_TARGET_CODE = "key_camera_translation_target_code";

    private static final String KEY_MY_CHATTING_THREAD_KEY = "key_my_chatting_name";

    private static final String KEY_AUTHORITY_NOTICE = "key_authority_notice";

    private static final String KEY_CONTACT_USE_NOTICE = "key_contact_use_notice";

    private static final String KEY_REFERER = "key_referer";
    private static final String KEY_SMS_PHONE = "key_sms_phone";

    private static final String KEY_CHAT_LAST_SEQ = "key_last_seq_";

    private static Context mApplicationContext = null;

    private static final String KEY_NOTICE_DATE = "key_notice_date";


    private static final String KEY_FRIEND_LIST_TYPE = "key_friend_list_type";
    private static final String KEY_SELECT_FRIEND_LIST_TYPE = "key_select_friend_list_type";
    private static final String KEY_CHAT_LIST_TYPE = "key_chat_list_type";
    private static final String KEY_BLOCK_FRIEND_LIST_TYPE = "key_block_friend_list_type";
    private static final String KEY_SEARCH_FRIEND_LIST_TYPE = "key_search_friend_list_type";
    private static final String KEY_SEARCH_CHAT_LIST_TYPE = "key_search_friend_chat_type";

    private static final String KEY_THEME_TYPE = "key_theme_type";

    private static final String KEY_BACKUP_SAVE_TIME = "key_backup_save_time";
    private static final String KEY_BACKUP_RESTORE_TIME = "key_backup_restore_time";

    private static final String KEY_AUTO_TRANS_ENABLE = "key_auto_trans_enable";
    private static final String KEY_NOTICE_ENABLE = "key_notice_enable";
    private static final String KEY_CHAT_TIME_ORDER = "key_chat_time_order";
    private static final String KEY_USER_CUSTOM_NUMBER = "key_user_custom_num";

    private static final String KEY_FRIEND_SETTING_ORDER = "key_friend_setting_order";
    private static final String KEY_CONTACT_SETTING_ORDER = "key_contact_setting_order";
    private static final String KEY_FRIEND_SELECT_ORDER = "key_friend_select_order";
    private static final String KEY_FRIEND_BLOCK_ORDER = "key_friend_block_order";

    private static final String KEY_CHAT_SETTING_ORDER = "key_chat_setting_order";
    private static final String KEY_CHAT_GROUP_SETTING_ORDER = "key_chat_group_setting_order";
    private static final String KEY_MINE_CHAT_SETTING_ORDER = "key_mine_chat_setting_order";


    private static final String KEY_PIN_SCREEN_LOCK = "key_pin_screen_lock";
    private static final String KEY_FINGER_SCREEN_LOCK = "key_finger_screen_lock";

    public static void init(Context context) {
        mApplicationContext = context;
    }

    private static SharedPreferences getPreferences() {
        if (mApplicationContext == null) {
            Log.e(TAG, "error preferences not init");
            return null;
        }

        return mApplicationContext.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
    }

    /* preferences에 string을 기록한다 */
    private static void putString(String key, String value) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init putString key : " + key + " value : " + value);
            return;
        }

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(key, value);
        editor.apply();
    }

    private static void putBoolean(String key, boolean value) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init putString key : " + key + " value : " + value);
            return;
        }

        SharedPreferences.Editor editor = pref.edit();

        editor.putBoolean(key, value);
        editor.apply();
    }

    private static String getString(String key) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init getString key : " + key);
            return "";
        }

        return pref.getString(key, "");
    }

    private static boolean getBoolean(String key) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init getString key : " + key);
            return false;
        }

        return pref.getBoolean(key, false);
    }

    private static boolean getBoolean(String key, boolean isDefault) {
        SharedPreferences pref = getPreferences();
        if (pref == null) {
            Log.e(TAG, "error Preferences not init getString key : " + key);
            return false;
        }

        return pref.getBoolean(key, isDefault);
    }


    public static void setLanguage(String language) {
        putString(KEY_LANGUAGE, language);
    }

    public static String getLanguage() {
        String lang = getString(KEY_LANGUAGE);
        return lang.isEmpty() ? "" : lang;
    }

    public static void setLoginAuthorization(String authorization) {
        putString(KEY_LOGIN_AUTHORIZATION, authorization);
    }

    public static String getLoginAuthorization() {
        return getString(KEY_LOGIN_AUTHORIZATION);
    }

    public static void setAuthorization(String authorization) {
        putString(KEY_AUTHORIZATION, authorization);
    }

    public static String getAuthorization() {
        return getString(KEY_AUTHORIZATION);
    }

    public static void setLoginType(String loginType) {
        putString(KEY_X_LOGIN_TYPE, loginType);
    }

    public static String getLoginType() {
        return getString(KEY_X_LOGIN_TYPE);
    }

    public static void setPushToken(String pushToken) {
        putString(KEY_PUSH_REGISTRATION_ID, pushToken);
    }

    public static String getPushToken() {
        return getString(KEY_PUSH_REGISTRATION_ID);
    }

    public static void setDeviceId(String deviceId) {
        putString(KEY_MEMBER_DEVICE_ID, deviceId);
    }

    public static String getDeviceId() {
        return getString(KEY_MEMBER_DEVICE_ID);
    }

    public static void clearLoginInfo() {
        putString(KEY_AUTHORIZATION, "");
        putString(KEY_X_LOGIN_TYPE, "");
    }

    public static void setNationCode(String nation_code) {
        putString(KEY_NATION_CODE, nation_code);
    }

    public static String getNationCode() {
        return getString(KEY_NATION_CODE);
    }

    public static void setUserPassword(String user_password) {
        putString(KEY_USER_PASSWORD, user_password);
    }

    public static String getUserPassword() {
        return getString(KEY_USER_PASSWORD);
    }


    public static void setVoiceTopLanguage(String languageCode) {
        putString(KEY_VOICE_TRANSLATION_TOP, languageCode);
    }

    public static String getVoiceTopLanguage() {
        return getString(KEY_VOICE_TRANSLATION_TOP);
    }

    public static void setVoiceBottomLanguage(String languageCode) {
        putString(KEY_VOICE_TRANSLATION_BOTTOM, languageCode);
    }

    public static String getVoiceBottomLanguage() {
        return getString(KEY_VOICE_TRANSLATION_BOTTOM);
    }

    public static void setMyChatLanguage(String languageCode) {
        putString(KEY_MY_CHAT_LANGUAGE, languageCode);
    }

    public static boolean getChatListType() {
        return getBoolean(KEY_CHAT_LIST_TYPE);
    }

    public static void setChatListType(boolean listType) {
        putBoolean(KEY_CHAT_LIST_TYPE, listType);
    }

    public static boolean getFriendListType() {
        return getBoolean(KEY_FRIEND_LIST_TYPE);
    }

    public static void setFriendListType(boolean listType) {
        putBoolean(KEY_FRIEND_LIST_TYPE, listType);
    }

    public static boolean getFriendSettingOrder() {
        return getBoolean(KEY_FRIEND_SETTING_ORDER);
    }

    public static void setFriendSettingOrder(boolean order) {
        putBoolean(KEY_FRIEND_SETTING_ORDER, order);
    }

    public static boolean getChatSettingOrder() {
        return getBoolean(KEY_CHAT_SETTING_ORDER);
    }

    public static void setChatSettingOrder(boolean order) {
        putBoolean(KEY_CHAT_SETTING_ORDER, order);
    }

    public static boolean getMineChatSettingOrder() {
        return getBoolean(KEY_MINE_CHAT_SETTING_ORDER);
    }

    public static void setMineChatSettingOrder(boolean order) {
        putBoolean(KEY_MINE_CHAT_SETTING_ORDER, order);
    }

    public static boolean getChatGroupSettingOrder() {
        return getBoolean(KEY_CHAT_SETTING_ORDER);
    }

    public static void setChatGroupSettingOrder(boolean order) {
        putBoolean(KEY_CHAT_SETTING_ORDER, order);
    }

    public static boolean getFriendSelectOrder() {
        return getBoolean(KEY_FRIEND_SELECT_ORDER);
    }

    public static void setFriendSelectOrder(boolean order) {
        putBoolean(KEY_FRIEND_SELECT_ORDER, order);
    }

    public static boolean getFriendBlockOrder() {
        return getBoolean(KEY_FRIEND_BLOCK_ORDER);
    }

    public static void setFriendBlockOrder(boolean order) {
        putBoolean(KEY_FRIEND_BLOCK_ORDER, order);
    }

    public static boolean getContactSettingOrder() {
        return getBoolean(KEY_CONTACT_SETTING_ORDER);
    }

    public static void setContactSettingOrder(boolean order) {
        putBoolean(KEY_CONTACT_SETTING_ORDER, order);
    }

    public static boolean getThemeType() {
        return getBoolean(KEY_THEME_TYPE);
    }

    public static void setThemeType(boolean themeType) {
        putBoolean(KEY_THEME_TYPE, themeType);
    }

    public static boolean getBlockFriendListType() {
        return getBoolean(KEY_BLOCK_FRIEND_LIST_TYPE);
    }

    public static boolean getSelectFriendListType() {
        return getBoolean(KEY_SELECT_FRIEND_LIST_TYPE);
    }

    public static void setSelectFriendListType(boolean listType) {
        putBoolean(KEY_SELECT_FRIEND_LIST_TYPE, listType);
    }

    public static void setBlockFriendListType(boolean listType) {
        putBoolean(KEY_BLOCK_FRIEND_LIST_TYPE, listType);
    }

    public static boolean getSearchChatListType() {
        return getBoolean(KEY_SEARCH_CHAT_LIST_TYPE);
    }

    public static void setSearchChatListType(boolean listType) {
        putBoolean(KEY_SEARCH_CHAT_LIST_TYPE, listType);
    }

    public static boolean getSearchFriendListType() {
        return getBoolean(KEY_SEARCH_FRIEND_LIST_TYPE);
    }

    public static void setSearchFriendListType(boolean listType) {
        putBoolean(KEY_SEARCH_FRIEND_LIST_TYPE, listType);
    }

    public static String getMyChatLanguage() {
        return getString(KEY_MY_CHAT_LANGUAGE);
    }

    public static void setChatTranslationUse(boolean isUseFlag) {
        putString(KEY_CHAT_TRANSLATION_USE, isUseFlag ? "Y" : "N");
    }

    public static boolean getChatTranslationUse() {

        String useFlag = getString(KEY_CHAT_TRANSLATION_USE);
        boolean isUsed = true;
        if(useFlag == null || useFlag.equals("")) {
            isUsed = true;
        } else {
            isUsed = useFlag.equals("Y") ? true : false;
        }

        return isUsed;
    }

    public static void setChatTranslationLanguageCode(String code) {
        putString(KEY_CHAT_TRANSLATION_LANGUAGE_CODE, code);
    }

    public static String getChatTranslationLanguageCode() {
        return getString(KEY_CHAT_TRANSLATION_LANGUAGE_CODE);
    }


    public static void setChatBackgroundColor(String color) {
        putString(KEY_CHAT_BACKGROUND_COLOR, color);
    }

    public static String getChatBackgroundColor() {
        return getString(KEY_CHAT_BACKGROUND_COLOR);
    }


    public static void setChatBackgroundType(String type) {
        putString(KEY_CHAT_BACKGROUND_TYPE, type);
    }

    public static String getChatBackgroundType() {
        return getString(KEY_CHAT_BACKGROUND_TYPE);
    }

    public static void setChatBackgroundImage(String imagePath) {
        putString(KEY_CHAT_BACKGROUND_IMAGE, imagePath);
    }

    public static String getChatBackgroundImage() {
        return getString(KEY_CHAT_BACKGROUND_IMAGE);
    }

    public static void setCameraSourceCode(String code) {
        putString(KEY_CAMERA_TRANSLATION_SOURCE_CODE, code);
    }

    public static String getCameraSourceCode() {
        return getString(KEY_CAMERA_TRANSLATION_SOURCE_CODE);
    }

    public static void setCameraTargetCode(String code) {
        putString(KEY_CAMERA_TRANSLATION_TARGET_CODE, code);
    }

    public static String getCameraTargetCode() {
        return getString(KEY_CAMERA_TRANSLATION_TARGET_CODE);
    }

    public static void setMyChattingThreadKey(String threadKey) {
        putString(KEY_MY_CHATTING_THREAD_KEY, threadKey);
    }


    public static boolean getPinScreenLock() {
        return getBoolean(KEY_PIN_SCREEN_LOCK);
    }

    public static void setPinScreenLock(boolean pinScreenLock) {
        putBoolean(KEY_PIN_SCREEN_LOCK, pinScreenLock);
    }

    public static boolean getFingerScreenLock() {
        return getBoolean(KEY_FINGER_SCREEN_LOCK);
    }

    public static void setFingerScreenLock(boolean fingerScreenLock) {
        putBoolean(KEY_FINGER_SCREEN_LOCK, fingerScreenLock);
    }

    public static String getBackUpSaveTime() {
        return getString(KEY_BACKUP_SAVE_TIME);
    }

    public static void setBackUpSaveTime(String time) {
        putString(KEY_BACKUP_SAVE_TIME, time);
    }

    public static String getBackUpRestoreTime() {
        return getString(KEY_BACKUP_RESTORE_TIME);
    }

    public static void setBackUpRestoreTime(String time) {
        putString(KEY_BACKUP_RESTORE_TIME, time);
    }

    public static boolean getAutoTransEnalbe() {
        return getBoolean(KEY_AUTO_TRANS_ENABLE);
    }

    public static void setAutoTransEnable(boolean autoTrans) {
        putBoolean(KEY_AUTO_TRANS_ENABLE, autoTrans);
    }

    public static boolean getNoticeEnable() {
        return getBoolean(KEY_NOTICE_ENABLE, true);
    }

    public static void setNoticeEnable(boolean notice) {
        putBoolean(KEY_NOTICE_ENABLE, notice);
    }

    public static boolean getChatTimeOrder() {
        return getBoolean(KEY_CHAT_TIME_ORDER);
    }

    public static void setChatTimeOrder(boolean timeOrder) {
        putBoolean(KEY_CHAT_TIME_ORDER, timeOrder);
    }

    public static boolean getChatAutoTransEnable(String key) {
        return getBoolean("chatRoomTrans_" + key);
    }

    public static void setChatAutoTransEnable(String key, boolean enable) {
        putBoolean("chatRoomTrans_" + key, enable);
    }

    public static boolean getChatSecurityEnable(String key) {
        return getBoolean("chatRoom_" + key);
    }

    public static void setChatSecurityEnable(String key, boolean timeOrder) {
        putBoolean("chatRoom_" + key, timeOrder);
    }

    public static String getMyChattingThreadKey() {
        return getString(KEY_MY_CHATTING_THREAD_KEY);
    }

    public static void setChattingName(String key, String value) {
        putString("chatName_" + key, value);
    }

    public static String getChattingName(String key) {
        return getString("chatName_" + key);
    }

    public static String getCustomNumber(String xid) {
        return getString(xid);
    }

    public static void setCustomNumber(String xid, String number) {
        putString(xid, number);
    }

    public static String getLocalDeleteSeq(String threadKey) {
        return getString("del_seq_" + threadKey);
    }

    public static void setLocalDeleteSeq(String threadKey, String seq) {
        putString("del_seq_" + threadKey, seq);
    }

    public static String getMiningSaveDate(String xid) {
        return getString("mining_" + xid);
    }

    public static void setMiningSaveDate(String xid, String date) {
        putString("mining_" + xid, date);
    }

    public static void setAuthorityNotice(boolean isNotice) {
        putBoolean(KEY_AUTHORITY_NOTICE, isNotice);
    }

    public static boolean getAuthorityNotice() {
        return getBoolean(KEY_AUTHORITY_NOTICE);
    }

    public static void setContactUseNotice(boolean isNotice) {
        putBoolean(KEY_CONTACT_USE_NOTICE, isNotice);
    }

    public static boolean getContactUseNotice() {
        return getBoolean(KEY_CONTACT_USE_NOTICE);
    }

    public static void setRecommandUserCode(String value) {
        putString(KEY_REFERER, value);
    }

    public static String getREcommandUserCode() {
        return getString(KEY_REFERER);
    }

    public static void setSmsPhone(String value) {
        putString(KEY_SMS_PHONE, value);
    }

    public static String getSmsPhone() {
        return getString(KEY_SMS_PHONE);
    }

    public static void setChatLastSeq(String threadKey, String seq) {
        putString(KEY_CHAT_LAST_SEQ + threadKey, seq);
    }

    public static String getChatLastSeq(String threadKey) {
        return getString(KEY_CHAT_LAST_SEQ + threadKey);
    }

    public static void setNoticeDate(String date) {
        putString(KEY_NOTICE_DATE, date);
    }

    public static String getNoticeDate() {
        return getString(KEY_NOTICE_DATE);
    }
}
