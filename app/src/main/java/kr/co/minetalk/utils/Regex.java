package kr.co.minetalk.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by episode on 2018. 10. 2..
 */

public class Regex {

    // 이메일 정규식
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    // 비밀번호 정규식
//    // 최소 7자리, 소문자 1개 대문자 1개 숫자 1개 특수문자 1
//    public static final Pattern VALID_PASSWOLD_REGEX_ALPHA_NUM = Pattern.compile("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{7,}$");
    // 최소 7자리, 문자, 숫자 1개 특수문자 1
//    public static final Pattern VALID_PASSWOLD_REGEX_ALPHA_NUM = Pattern.compile("^[a-zA-Z0-9!@.#$%^&*?_~]{8,}$"); // 4자리 ~ 16자리까지 가능

    //public static final Pattern VALID_ID_REGEX_ALPHA_NUM = Pattern.compile("^(?=.*?[a-zA-Z])(?=.*?[0-9]).{8,12}$");

    //영문 숫자 특수문자 제외 8~12자리
    public static final Pattern VALID_ID_REGEX_ALPHA_NUM = Pattern.compile("^((?=.*\\d)(?=.*[a-z]).{8,12}$)");
    //public static final Pattern VALID_ID_REGEX_ALPHA_NUM = Pattern.compile("^([a-zA-Z][a-zA-Z0-9]{8,12}$)");

    public static final Pattern VALID_PASSWOLD_REGEX_ALPHA_NUM = Pattern.compile("^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");

//    public static final Pattern VALID_PASSWOLD_REGEX_ALPHA_NUM = Pattern.compile("^(?=.*?[a-zA-Z])(?=.*?[0-9]).{6,}$");


    public static final Pattern VALID_PHONE_NUMBER_REGEX_ALPHA_NUM = Pattern.compile("(^[0-9]*$)");


    // 아이디 정규식 검사
    public static boolean validateID(String idStr) {
        Matcher matcher = VALID_ID_REGEX_ALPHA_NUM.matcher(idStr);
        return matcher.find();
    }

    // 이메일 정규식 검사
    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    // 비밀번호 정규식 검사
    public static boolean validatePassword(String pwStr) {
        Matcher matcher = VALID_PASSWOLD_REGEX_ALPHA_NUM.matcher(pwStr);
        return matcher.matches();
    }

    public static boolean validatePhone(String inputStr) {
        Matcher matcher = VALID_PHONE_NUMBER_REGEX_ALPHA_NUM.matcher(inputStr);
        return matcher.matches();
    }
}
