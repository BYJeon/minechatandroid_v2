package kr.co.minetalk.utils;

import android.os.AsyncTask;

import java.net.URL;
import java.net.URLConnection;

/**
 * Created by episode on 2018. 1. 15..
 */

public class UrlFileSizeAsyncTask extends AsyncTask<String, Void, Integer> {

    private String mUrl = "";
    private OnFileSizeCallback callback = null;

    public UrlFileSizeAsyncTask(String mUrl) {
        this.mUrl = mUrl;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public void setCallback(OnFileSizeCallback callback) {
        this.callback = callback;
    }

    @Override
    protected Integer doInBackground(String... strings) {
        int file_size = 0;

        try {
            URL url = new URL(mUrl);
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();
            file_size = urlConnection.getContentLength();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return file_size;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        if(callback != null) {
            callback.callBackSize(integer.intValue());
        }
    }

    public interface OnFileSizeCallback {
        public void callBackSize(int size);
    }

    public void onDestroy() {

    }

}
