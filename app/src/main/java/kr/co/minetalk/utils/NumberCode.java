package kr.co.minetalk.utils;

import android.content.res.AssetManager;
import android.util.Log;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.api.model.NumberCodeData;

/**
 * Created by episode on 2018. 1. 12..
 */

public class NumberCode {

    private static NumberCode INSTANCE = null;
    public static NumberCode getIntance() {
        if(INSTANCE == null) {
            INSTANCE = new NumberCode();
        }
        return INSTANCE;
    }

    private ArrayList<NumberCodeData> mArrayList = new ArrayList<>();
    private String[] mTitleArray = null;

    private HashMap<String, String> mNationName = new HashMap<>();

    private AssetManager mAssetManager;

    public NumberCode() {
        initData();
    }

    private void initData() {
        mArrayList.clear();
        mNationName.clear();

        mAssetManager = MineTalkApp.getAppContext().getResources().getAssets();
        getDataFromAsset();

        mTitleArray = new String[mArrayList.size()];
        initTitleArray();
    }

    private void initTitleArray() {

        String appLanguage = Preferences.getLanguage();

        for (int i = 0 ; i < mArrayList.size() ; i++) {
            if(appLanguage.equals("ko")) {
                mTitleArray[i] = mArrayList.get(i).getName();
            } else {
                mTitleArray[i] = mArrayList.get(i).getListTitleEng();
            }
        }
    }

    public String[] getmTitleArray() {
        return this.mTitleArray;
    }


    public ArrayList<NumberCodeData> getDataList() {
        return mArrayList;
    }

    public void getDataFromAsset(){

        String languageCode = Preferences.getLanguage();
        InputStream inputStream = null;

        try{
            //asset manager에게서 inputstream 가져오기
            inputStream = mAssetManager.open("nation.txt", AssetManager.ACCESS_BUFFER);

            //문자로 읽어들이기
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            //파일읽기
            String line = "";
            while((line=reader.readLine()) != null){
                String[] arrayData = line.split(",");
                mArrayList.add(new NumberCodeData(arrayData[1], arrayData[0], arrayData[2]));

                if(languageCode != null && languageCode.equals("ko")) {
                    String[] nationName = arrayData[1].split(" ");
                    mNationName.put(arrayData[0].replace("+", ""), nationName[0]);
                } else {
                    String[] nationName = arrayData[2].split(" ");
                    mNationName.put(arrayData[0].replace("+", ""), nationName[0]);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (inputStream != null) {
                try { inputStream.close(); } catch (IOException e) {}
            }
        }
    }

    public String getNationName(String nationCode) {
        for(int i = 0 ; i < mArrayList.size() ; i++) {
            NumberCodeData item = mArrayList.get(i);
            if(item.getNumCode().equals(nationCode)) {
                return item.getName();
            }
        }

        return "";
    }

    public String getNationNameToMap(String nationCode) {
        return mNationName.get(nationCode);
    }
}
