package kr.co.minetalk.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

/**
 * Created by kyd0822 on 2017. 8. 22..
 */

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private Camera mCamera;
    public List<Camera.Size> prSupportedPreviewSizes;
    private Camera.Size prPreviewSize;

    public CameraPreview(Context context) {
        super(context);
        setZOrderMediaOverlay(true);
        Log.v("jyp@@@","CameraPreview(Context context) 생성자호출");
    }



    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.v("jyp@@@", "CameraPreview(Context context, AttributeSet attrs) 생성자호출");
        mCamera = Camera.open();
        prSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();


    }



    public void takePhoto(Camera.PictureCallback handler){

        mCamera.getParameters().setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        mCamera.autoFocus(new Camera.AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean success, Camera camera) {

            }
        });
        Log.e("@@@TAG","take 1");

        if(mCamera != null){
            Log.e("@@@TAG","take 2");
            mCamera.takePicture(null,null,handler);
//            return true;
        }else{
            Log.e("@@@TAG","take 3");
//            return false;
        }
    }

    public void stopCameraPreView() {
        if(mCamera != null) {
            mCamera.stopPreview();
        }
    }

    public void startCameraPreView() {
        if(mCamera != null) {
            mCamera.startPreview();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.e("@@@KYD","surfaceCreated ");
        // Surface가 생성되었으니 프리뷰를 어디에 띄울지 지정해준다. (holder 로 받은 SurfaceHolder에 뿌려준다.
        if(mCamera == null) {
            mCamera = Camera.open();
            prSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
        }

        try {

            Camera.Parameters parameters = mCamera.getParameters();
            if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                parameters.set("orientation", "portrait");
                mCamera.setDisplayOrientation(90);
                parameters.setRotation(90);
            } else {
                parameters.set("orientation", "landscape");
                mCamera.setDisplayOrientation(0);
                parameters.setRotation(0);
            }

            parameters.setFocusMode(parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            mCamera.setParameters(parameters);

            mCamera.setPreviewDisplay(holder);


            mCamera.startPreview();
        } catch (IOException e) {
            Log.d("TAG", "Error setting camera preview: " + e.getMessage());
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {

//        Path clipPath = new Path();
//
////        clipPath.addCircle(getWidth() / 2, getHeight() / 2, getWidth() / 2, Path.Direction.CCW);
//        Rect rect = new Rect();
//        clipPath.addRect(50, 50, 300, 300, Path.Direction.CCW);
//        canvas.clipPath(clipPath);

        super.dispatchDraw(canvas);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.e("@@@KYD","surfaceDestroyed ");
        // 프리뷰 제거시 카메라 사용도 끝났다고 간주하여 리소스를 전부 반환한다
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }


    private Camera.Size getBestPreviewSize(int width, int height)
    {
        Camera.Size result=null;
        Camera.Parameters p = mCamera.getParameters();
        for (Camera.Size size : p.getSupportedPreviewSizes()) {
            if (size.width<=width && size.height<=height) {
                if (result==null) {
                    result=size;
                } else {
                    int resultArea=result.width*result.height;
                    int newArea=size.width*size.height;

                    if (newArea>resultArea) {
                        result=size;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.e("@@@KYD","surfaceChanged ");
        // 프리뷰를 회전시키거나 변경시 처리를 여기서 해준다.
        // 프리뷰 변경시에는 먼저 프리뷰를 멈춘다음 변경해야한다.
        if (holder.getSurface() == null){
            // 프리뷰가 존재하지 않을때
            return;
        }

        // 우선 멈춘다
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            // 프리뷰가 존재조차 하지 않는 경우다
        }

        // 프리뷰 변경, 처리 등을 여기서 해준다.
        Camera.Parameters parameters = mCamera.getParameters();
        List<String> focusModes = parameters.getSupportedFocusModes();
//        parameters.setFocusMode(parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        Camera.Size size = getBestPreviewSize(w, h);
        Log.e("@@@KYD","surfaceChanged  width: "+prPreviewSize.width);
        Log.e("@@@KYD","surfaceChanged  height: "+prPreviewSize.height);


        parameters.setPreviewSize(prPreviewSize.width, prPreviewSize.height);
        parameters.setPictureSize(prPreviewSize.width, prPreviewSize.height);

//        parameters.setPreviewSize(size.width, size.height);
//        parameters.setPictureSize(size.width, size.height);


        // 새로 변경된 설정으로 프리뷰를 재생성한다
        try {
            mCamera.setParameters(parameters);
            mCamera.setPreviewDisplay(holder);

            mCamera.startPreview();

        } catch (Exception e){
            Log.e("@@@KYD", "Error starting camera preview: " + e.getMessage());
            try {
                mCamera.setPreviewDisplay(holder);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            mCamera.startPreview();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.e("@@@KYD","onMeasure()");

        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        Log.e("@@@KYD","width: "+width);
        Log.e("@@@KYD","height: "+height);
        setMeasuredDimension(width, height);

        if (prSupportedPreviewSizes != null) {
            prPreviewSize = getOptimalPreviewSize(prSupportedPreviewSizes, width, height);
            Log.v("jyp@@@","prPreviewSize.width: "+prPreviewSize.width);
            Log.v("jyp@@@","prPreviewSize.height: "+prPreviewSize.height);
        }
    }

    public Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {

        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        return optimalSize;
    }

}
