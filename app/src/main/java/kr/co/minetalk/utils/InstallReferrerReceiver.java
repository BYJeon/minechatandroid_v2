package kr.co.minetalk.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import kr.co.minetalk.MineTalk;
import kr.co.minetalk.activity.LoginActivity;
import kr.co.minetalk.activity.RegistNormalActivity;
import kr.co.minetalk.repository.RegistRepository;

/**
 * Created by kyd0822 on 2017. 10. 25..
 */

public class InstallReferrerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String recommandUserId = "";

        if(intent.getAction().equals("com.android.vending.INSTALL_REFERRER")) {
            Bundle extra = intent.getExtras();
            recommandUserId = extra.getString("referrer");

            if(recommandUserId!=null && recommandUserId.contains("code")) {
                if(recommandUserId.contains("code")) {
                    int index = recommandUserId.indexOf("code");
                    recommandUserId = recommandUserId.substring(index+4);
                    Preferences.setRecommandUserCode(recommandUserId);
                } else {
                    Preferences.setRecommandUserCode("");
                }
            } else {
                Preferences.setRecommandUserCode("");
            }
        }
    }
}
