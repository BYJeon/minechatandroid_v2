package kr.co.minetalk.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import kr.co.minetalk.BuildConfig;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;


public class CommonUtils {

    public static final String TAG = "CommonUtils";
    private static String AFTERCARE_PACKAGE_NAME = "kr.co.minetalk";

    /**
     * 로그 출력 (debug 수준)
     *
     * @param msg
     */
    public static void debug(String msg) {
        debug(AFTERCARE_PACKAGE_NAME, msg);
    }

    public static void debug(String tag, String msg) {
        debug(Log.DEBUG, tag, msg);
    }

    public static void debug(int type, String tag, String msg) {

        if (BuildConfig.isDebug) {
            switch (type) {
                case Log.VERBOSE:
                    Log.v(tag, msg);
                    break;
                case Log.DEBUG:
                    Log.d(tag, msg);
                    break;
                case Log.INFO:
                    Log.i(tag, msg);
                    break;
                case Log.WARN:
                    Log.w(tag, msg);
                    break;
                case Log.ERROR:
                    Log.e(tag, msg);
                    break;
                case Log.ASSERT:
                    Log.wtf(tag, msg);
                    break;
            }
        }
    }

    /**
     * 리스트 객체 Empty 확인
     *
     * @param list List Object
     * @return boolean
     */
    public static boolean isEmptyList(Object list) {
        if (list == null) {
            return true;
        }
        if (list instanceof List &&
                ((List) list).size() > 0) {
            return false;
        }
        return true;
    }

    /**
     * 리스트 객체 Empty 확인
     *
     * @param list List Object
     * @param type Generic Class type
     * @return boolean
     */
    public static boolean isEmptyList(Object list, Class type) {
        if (type == null) {
            return true;
        }
        if (!isEmptyList(list) &&
                type.isInstance(((List) list).get(0))) {
            return false;
        }
        return true;
    }

    /**
     * 키보드를 hide 한다.
     *
     * @param context
     * @param view
     */
    public static void hideKeypad(Context context, View view) {
        InputMethodManager im = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 키보드를 show 한다.
     *
     * @param context
     * @param view
     */
    public static void showKeypad(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
    }

    /**
     * 키보드를 hide -> show, show -> hide 한다.
     *
     * @param context
     */
    public static void toggleKeypad(Context context) {
        InputMethodManager im = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        im.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    /**
     * 현재 사용할 수 있는 네트워크가 있는지 확인
     *
     * @return boolean true : 가능
     */
    public static boolean isAvailableNetwork() {
        Context context = MineTalkApp.getAppContext();

        NetworkInfo ni = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        if (ni == null) {
            return false;
        }

        return true;
    }



    /**
     * PixelsToDp
     *
     * @param context
     * @param px
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    /**
     * DpToPixel
     *
     * @param context
     * @param dp
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static int getImageWidth(ImageView imageView) {
        return imageView.getDrawable().getIntrinsicWidth();
    }

    public static int getImageHeight(ImageView imageView) {
        return imageView.getDrawable().getIntrinsicHeight();
    }


    /**
     * String 날짜 포메터.
     *
     * @param dateStr       String 날짜
     * @param curDateFormat 현재 date format
     * @param newDateFormat 바꿀 date format
     * @return String 날짜
     * @throws ParseException
     */
    public static String convertDateFormat(String dateStr, String curDateFormat, String newDateFormat)
            throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(curDateFormat);
        Date date = sdf.parse(dateStr);
        sdf = new SimpleDateFormat(newDateFormat);
        return sdf.format(date);
    }

    public static String comma_won(String junsu) {
        String result_int = "";
        try {
            int inValues = Integer.parseInt(junsu);
            DecimalFormat Commas = new DecimalFormat("#,###,###,###");
            result_int = (String) Commas.format(inValues);
        }catch (NumberFormatException e){
            e.printStackTrace();
        }
        return result_int;
    }

    public static String getVersionName(Context context) {
        PackageInfo pi = null;
        try {
            pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pi.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 구글 플레이 스토어에 앱을 실행함.
     *
     * @param context
     */
    public static void startStore(Context context) {
        try {
            Intent intent = new Intent();
            intent.setData(Uri.parse("market://details?id=" + AFTERCARE_PACKAGE_NAME));
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    // 언어변경
    public static void setLanguage(String local) {
        Configuration config = new Configuration();

        if (local.equals("en")) {
            Locale.setDefault(Locale.US);
            config.locale = Locale.US;
        } else if (local.equals("ko")) {
            Locale.setDefault(Locale.KOREA);
            config.locale = Locale.KOREA;
        } else {
            Locale locale = new Locale(local);
            Locale.setDefault(locale);
            config.locale = locale;
        }

        MineTalkApp.getAppContext().getResources().updateConfiguration(config, MineTalkApp.getAppContext().getResources().getDisplayMetrics());
    }

    //
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    // 다운로드 폴더 생성 (File)
    public static File getStorageDownload(Context context) {
        if (isExternalStorageWritable()) {
            String download = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
            File f = new File(download + "/" + context.getString(R.string.app_name));
            if (!f.exists()) {

                f.mkdirs();
            }
            return f;
        }
        return null;
    }

    // 다운로드 폴더 생성 (String)
    public static String getStorageDownloadPath(Context context) {
        String path = getStorageDownload(context).getAbsolutePath();
        return path == null ? null : path;
    }

    // 스토리지 권한 확인
    public static boolean grantExternalStoragePermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    // Show View (Child)
    public static void showWithChildView(View v) {
        if (v instanceof ViewGroup) {
            int count = ((ViewGroup) v).getChildCount();
            for (int k = 0; k < count; k++) {
                showWithChildView(((ViewGroup) v).getChildAt(k));
            }
            v.setVisibility(View.VISIBLE);
        } else {
            v.setClickable(true);
            v.setVisibility(View.VISIBLE);
        }
    }

    // Hide View (Child)
    public static void hideWithChildView(View v) {
        if (v instanceof ViewGroup) {
            int count = ((ViewGroup) v).getChildCount();
            for (int k = 0; k < count; k++) {
                hideWithChildView(((ViewGroup) v).getChildAt(k));
            }
            v.setVisibility(View.GONE);
        } else {
            v.setClickable(false);
            v.setVisibility(View.GONE);
        }
    }

    // Create Drawable From View
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    public static String encoderBase64(String str) {
        return Base64.encodeToString(str.getBytes(), Base64.NO_WRAP);
    }

    /**
     * Header Basic 값 만들기
     * email:pass
     * @param user_id
     * @param password
     * @return
     */
    public static String makeAuthorization(String user_id, String password) {
        String authorization = encoderBase64(user_id + ":" + password);
        return authorization;
    }

    public static String makeShopAuthorization(String shop_id, String pw) {
        String authorization = encoderBase64(shop_id + ":" + pw);
        return authorization;
    }

    /**
     * 파일명으로 리스스 찾기
     * @param context
     * @param resName
     * @return
     */
    public static int getResourceImage(Context context, String resName) {
        int resId;
        String packageName = context.getPackageName();
        resId = context.getResources().getIdentifier(resName, "drawable", packageName);
        return resId;
    }



    public static String numberMask(String money) {
        long value = Long.parseLong(money);
        DecimalFormat format = new DecimalFormat("###,###");
        //콤마
        format.format(value);
        String result_int = format.format(value);

        return result_int;
    }



    /**
     * 날짜 포멧 변환
     * @param date 변환할 날짜
     * @param fromFormatString 변환될 포맷
     * @param toFormatString 변환할 포맷
     * @return 변환된 날짜 문자열
     */
    public static String formattedDate(String date, String fromFormatString, String toFormatString) {
        SimpleDateFormat fromFormat = new SimpleDateFormat(fromFormatString, Locale.KOREA);
        SimpleDateFormat toFormat   = new SimpleDateFormat(toFormatString, Locale.KOREA);
        Date fromDate = null;

        try {
            fromDate = fromFormat.parse(date);
        } catch(ParseException e) {
            fromDate = new Date();
        }
        return toFormat.format(fromDate);
    }
    /**
     * * 예시)
     * “yyyy.MM.dd G ‘at’ HH:mm:ss z”	2001.07.04 AD at 12:08:56 PDT
     * “EEE, MMM d, ”yy”	Wed, Jul 4, ’01
     * “h:mm a”	12:08 PM
     * “hh ‘o”clock’ a, zzzz”	12 o’clock PM, Pacific Daylight Time
     * “K:mm a, z”	0:08 PM, PDT
     * “yyyyy.MMMMM.dd GGG hh:mm aaa”	02001.July.04 AD 12:08 PM
     * “EEE, d MMM yyyy HH:mm:ss Z”	Wed, 4 Jul 2001 12:08:56 -0700
     * “yyMMddHHmmssZ”	010704120856-0700
     * “yyyy-
     */

    public static String getImageMediaPath(Context context, Uri uri) {
        int index = 0;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor.moveToFirst()) {
            index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        }
        return cursor.getString(index);
    }

    public static String getImageMediaExternalPath(Context context, Uri uri) {
        int index = 0;
        String[] projection = new String[]{
                MediaStore.Video.Media._ID,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.MIME_TYPE
        };
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);

        if (cursor == null || !cursor.moveToFirst()) {
            Log.e("TAG", "cursor null or cursor is empty");
            return "";
        }

        //String contentUrl = uri.toString() + "/" + cursor.getString(0);
        String contentUrl = cursor.getString(0);

        return contentUrl;
    }

    public static String getMediaType(Context context, Uri uri) {
        return context.getContentResolver().getType(uri);
    }

    public static String getDataFromAsset(Context context, String fileName){
        AssetManager assetManager = context.getResources().getAssets();
        InputStream inputStream = null;

        try{
            //asset manager에게서 inputstream 가져오기
            inputStream = assetManager.open(fileName, AssetManager.ACCESS_BUFFER);

            //문자로 읽어들이기
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            //파일읽기
            String strResult = "";
            String line = "";
            while((line=reader.readLine()) != null){
                strResult += line;
            }

            //읽은내용 출력
            return strResult;
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if (inputStream != null) {
                try { inputStream.close(); } catch (IOException e) {}
            }
        }

        return null;
    }

    /**
     * Bitmap이미지의 가로, 세로 사이즈를 리사이징 한다.
     *
     * @param source 원본 Bitmap 객체
     * @param maxResolution 제한 해상도
     * @return 리사이즈된 이미지 Bitmap 객체
     */
    public static Bitmap resizeBitmapImage(Bitmap source, int maxResolution)
    {
        int width = source.getWidth();
        int height = source.getHeight();
        int newWidth = width;
        int newHeight = height;
        float rate = 0.0f;

        if(width > height) {
            if(maxResolution < width) {
                rate = maxResolution / (float) width;
                newHeight = (int) (height * rate);
                newWidth = maxResolution;
            }
        } else {
            if(maxResolution < height) {
                rate = maxResolution / (float) height;
                newWidth = (int) (width * rate);
                newHeight = maxResolution;
            }
        }
        return Bitmap.createScaledBitmap(source, newWidth, newHeight, true);
    }


    public static Bitmap convertResImageToBitmap(Context context, int resImg) {
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), resImg);
        return bm;
    }

    // File Extension
    public static String getFileExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }


    public static String getContactNameByNum(Context context, String phoneNumber){
        Uri uri;
        String[] projection;
        String displayName = "";
        ContentResolver mContentResolver = context.getContentResolver();

        // If targeting Donut or below, use
        // Contacts.Phones.CONTENT_FILTER_URL and
        // Contacts.Phones.DISPLAY_NAME
        uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        projection = new String[] {ContactsContract.PhoneLookup.DISPLAY_NAME};

        // Query the filter URI
        Cursor cursor = (new CursorLoader(context, uri, projection, null, null, null)).loadInBackground();

        // mContentResolver.query(uri, projection, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst())
                displayName = cursor.getString(0);

            cursor.close();
        }

        return  displayName;
    }

    //이전 개발자 공유 코드
    public static ArrayList<ContactModel> getContactList(Context context) {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        };

        ContentResolver mContentResolver = context.getContentResolver();
        String[] selectionArgs = null;
        String selection = ContactsContract.CommonDataKinds.Phone.TYPE + " = " + ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;
        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor cursor = mContentResolver.query(uri, projection, selection, selectionArgs, sortOrder);
        ArrayList<ContactModel> contactList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                String phoneNumber = cursor.getString(0);
                String name = cursor.getString(1);
                if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
                    continue;
                }
                phoneNumber = phoneNumber.replaceAll("-", "");
                ContactModel item = new ContactModel(name, phoneNumber);
                //item.setUser_name(name);
                //item.setUser_phone(phoneNumber);
                contactList.add(item);
            } while (cursor.moveToNext());
        }
        return contactList;
    }

//    public static int getContactListCount(Context context){
//        //Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//        //ContentResolver mContentResolver = context.getContentResolver();
//        //Cursor cursor = mContentResolver.query(uri, null, null, null, null);
//
//        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//
//        String[] projection = new String[]{
//                ContactsContract.CommonDataKinds.Phone.NUMBER,
//                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
//        };
//        ContentResolver mContentResolver = context.getContentResolver();
//        String[] selectionArgs = null;
//        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
//        Cursor cursor = mContentResolver.query(uri, projection, null, selectionArgs, sortOrder);
//
//        return cursor.getCount();
//    }
//    public static ArrayList<ContactModel> getContactList(Context context) {
//        ArrayList<ContactModel> contactList = new ArrayList<>();
//
//        try {
//            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//            //uri = uri.buildUpon().appendQueryParameter("limit", String.format("%d, %d", 0, MineTalk.CONTACT_RANGE)).build();
//
//            String[] projection = new String[]{
//                    ContactsContract.CommonDataKinds.Phone.NUMBER,
//                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
//            };
//
//            ContentResolver mContentResolver = context.getContentResolver();
//            String[] selectionArgs = null;
//            String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
//            Cursor cursor = mContentResolver.query(uri, projection, null, selectionArgs, sortOrder);
//
//            try {
//                if (cursor.moveToFirst()) {
//                    do {
//                        String phoneNumber = cursor.getString(0);
//                        String name = cursor.getString(1);
//                        if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
//                            continue;
//                        }
//                        phoneNumber = phoneNumber.replaceAll("-", "");
//                        contactList.add(new ContactModel(name, phoneNumber));
//                    } while (cursor.moveToNext());
//                }
//            } finally {
//                if(cursor != null)
//                    cursor.close();
//            }
//        }catch (IllegalArgumentException e){
//            return contactList;
//        }
//
//        return  contactList;
//    }

    public static ArrayList<ContactModel> getContactList(Context context, String para) {

//        ArrayList<ContactModel> contactList = new ArrayList<>();
//
//        try{
//            String[] PROJECTION = new String[] {
//                    ContactsContract.Contacts.DISPLAY_NAME,
//                    ContactsContract.CommonDataKinds.Phone.NUMBER };
//           // ArrayList<ContactBean> listContacts = new ArrayList<>();
//            CursorLoader cursorLoader = new CursorLoader(context,
//                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION,
//                    null, null, "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME
//                    + ")ASC LIMIT " + para);
//
//            Cursor c = cursorLoader.loadInBackground();
//
//            if (c.moveToFirst()) {
//
//                int Number = c
//                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
//                int Name = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
//
//                do {
//
//                    try {
//                        String phoneNumber = c.getString(Number);
//                        String name = c.getString(Name);
//
//                        if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
//                            continue;
//                        }
//                        phoneNumber = phoneNumber.replaceAll("-", "");
//                        contactList.add(new ContactModel(name, phoneNumber));
//
//                    } catch (Exception e) {
//
//                    }
//
//                } while (c.moveToNext());
//
//                c.close();
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }


        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        };
        ContentResolver mContentResolver = context.getContentResolver();
        String[] selectionArgs = null;
        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC LIMIT " + para;
        Cursor cursor = mContentResolver.query(uri, projection, null, selectionArgs, sortOrder);
        ArrayList<ContactModel> contactList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                String phoneNumber = cursor.getString(0);
                String name = cursor.getString(1);
                if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
                    continue;
                }
                phoneNumber = phoneNumber.replaceAll("-", "");
                ContactModel item = new ContactModel(name, phoneNumber);
                //item.setUser_name(name);
                //item.setUser_phone(phoneNumber);
                contactList.add(item);
            } while (cursor.moveToNext());
        }
        return contactList;

//        try {
//            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//
//            String[] projection = new String[]{
//                    ContactsContract.CommonDataKinds.Phone.NUMBER,
//                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
//            };
//
//            ContentResolver mContentResolver = context.getContentResolver();
//            String[] selectionArgs = null;
//            //String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC LIMIT " + para;
//            String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC LIMIT " + para;
//            Cursor cursor = mContentResolver.query(uri, projection, null, selectionArgs, sortOrder);
//
//
//            try {
//                if (cursor.moveToFirst()) {
//                    do {
//                        String phoneNumber = cursor.getString(0);
//                        String name = cursor.getString(1);
//                        if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
//                            continue;
//                        }
//                        phoneNumber = phoneNumber.replaceAll("-", "");
//                        contactList.add(new ContactModel(name, phoneNumber));
//                    } while (cursor.moveToNext());
//                }
//            }finally {
//                if(cursor != null)
//                    cursor.close();
//            }
//        }catch (Exception e){
//            return contactList;
//        }


//        return  contactList;
    }


    public static HashMap<String, String> getContactMap(Context context) {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        };

        ContentResolver mContentResolver = context.getContentResolver();
        String[] selectionArgs = null;
        String selection = ContactsContract.CommonDataKinds.Phone.TYPE + " = " + ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;
        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor cursor = mContentResolver.query(uri, projection, selection, selectionArgs, sortOrder);
        HashMap<String, String> contactMap = new HashMap<>();

        if (cursor.moveToFirst()) {
            do {

                String phoneNumber = cursor.getString(0);
                String name = cursor.getString(1);
                if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(name)) {
                    continue;
                }
                phoneNumber = phoneNumber.replaceAll("-", "");
                contactMap.put(phoneNumber, name);
            } while (cursor.moveToNext());
        }
        return contactMap;

    }

    public static Bitmap resizeBitmapImageFn(Bitmap bmpSource, int maxResolution){

        int iWidth = bmpSource.getWidth();      //비트맵이미지의 넓이
        int iHeight = bmpSource.getHeight();     //비트맵이미지의 높이
        int newWidth = iWidth ;
        int newHeight = iHeight ;
        float rate = 0.0f;



        //이미지의 가로 세로 비율에 맞게 조절

        if(iWidth > iHeight ){

            if(maxResolution < iWidth ){
                rate = maxResolution / (float) iWidth ;
                newHeight = (int) (iHeight * rate);
                newWidth = maxResolution;
            }
        }else{
            if(maxResolution < iHeight ){
                rate = maxResolution / (float) iHeight ;
                newWidth = (int) (iWidth * rate);
                newHeight = maxResolution;
            }
        }

        return Bitmap.createScaledBitmap(
                bmpSource, newWidth, newHeight, true);
    }
    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }


    /** Get Bitmap's Width **/
    public static int getBitmapOfWidth( String fileName ){
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fileName, options);
            return options.outWidth;

        } catch(Exception e) {
            return 0;
        }
    }

    /** Get Bitmap's height **/
    public static int getBitmapOfHeight( String fileName ){
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fileName, options);
            return options.outHeight;
        } catch(Exception e) {
            return 0;
        }
    }


    public static String getPathFromUri(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;


        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static long getDateDiff(String target, String source) {
        long diffDay = -1;
        if (target == null || target.length() == 0) return -1;
        String strFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
        try {
            Date startDate = sdf.parse(target);
            Date endDate = sdf.parse(source);

            //두날짜 사이의 시간 차이(ms)를 하루 동안의 ms(24시*60분*60초*1000밀리초) 로 나눈다.
            diffDay = (startDate.getTime() - endDate.getTime()) / (24 * 60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return Math.abs(diffDay);
    }

    public static String getKorTime(String format) {
        TimeZone tz;
        Date date = new Date();
        DateFormat dt = new SimpleDateFormat(format);
        tz = TimeZone.getTimeZone("Asia/Seoul");
        dt.setTimeZone(tz);

        return dt.format(date);
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
}
