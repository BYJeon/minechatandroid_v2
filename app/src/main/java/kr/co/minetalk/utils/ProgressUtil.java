package kr.co.minetalk.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Window;
import android.widget.ProgressBar;


import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import kr.co.minetalk.R;

public class ProgressUtil {

    private static ProgressUtil Instance;

    private Map<String, CustomProgressbar> progressMap;

    private String LOG_TAG = this.getClass().getSimpleName();

    public static ProgressUtil getInstance() {
        if (Instance == null)
            Instance = new ProgressUtil();
        return Instance;
    }

    private ProgressUtil() {

        progressMap = new HashMap();
    }

    public static void showProgress(Context context) {
        try {
            getInstance().progressOn(context);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    public static void hideProgress(Context context) {
        try {
            getInstance().progressOff(context);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    public static void closeAllProgress() {
        getInstance().closeAll();
    }

    private void progressOn(Context context) {

        CustomProgressbar CustomProgressbar = null;

        if (context == null) {
            closeAll();
            return;
        }

        if (progressMap.containsKey(getTag(context))) {
            CustomProgressbar = progressMap.get(getTag(context));
            CustomProgressbar.retain();
        } else {
            CustomProgressbar = new CustomProgressbar(context);
            progressMap.put(context.getClass().getSimpleName(), CustomProgressbar);
        }

        try {
            CustomProgressbar.show();
        } catch (Exception e) {
            progressMap.remove(getTag(context));
            progressOn(context);
        }

    }

    public synchronized void progressOff(Context context) {

        if (context == null) {
            closeAll();
            return;
        }

        CustomProgressbar CustomProgressbar = progressMap.get(getTag(context));
        if (CustomProgressbar != null) {

            synchronized (progressMap) {

                try {
                    CustomProgressbar.release();

                    if (CustomProgressbar != null && CustomProgressbar.hasClosed() & CustomProgressbar.isShowing()) {
                        CustomProgressbar.cancel();
                        CustomProgressbar.dismiss();
                        CustomProgressbar = null;
                        progressMap.remove(getTag(context));
                    }
                } catch (ConcurrentModificationException exception) {
                    Log.e(LOG_TAG, "ConcurrentModificationException = {}", exception);
                }

            }
        }
    }

    public synchronized void closeAll() {
        Iterator<String> keys = progressMap.keySet().iterator();

        synchronized (progressMap) {

            while (keys.hasNext()) {

                try {
                    String key = keys.next();
                    CustomProgressbar CustomProgressbar = progressMap.get(key);
                    if (CustomProgressbar != null) {
                        CustomProgressbar.cancel();
                        CustomProgressbar.dismiss();
                        CustomProgressbar = null;
                        progressMap.remove(key);
                    }
                } catch (Exception exception) {
                    Log.e(LOG_TAG, "Progress All Close Exception : ", exception);
                }

            }


        }
    }

    public String getTag(Context context) {

        if (context != null) {
            return context.getClass().getSimpleName();
        }

        return "";
    }

    public static class CustomProgressbar extends Dialog {

        private int retainCount = 0;

        public void retain() {
            this.retainCount++;
        }

        public void release() {
            this.retainCount--;
        }

        public boolean hasClosed() {
            if (Math.max(retainCount, 0) == 0) {
                return true;
            }
            return false;
        }

        public CustomProgressbar(Context context) {
            super(context);
            requestWindowFeature(Window.FEATURE_NO_TITLE); // 지저분한(?) 다이얼 로그 제목을 날림
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getWindow().setDimAmount(0);
//            getWindow().setBackgroundDrawableResource(R.color.color_99000000);

            setContentView(R.layout.app_progress);
            retainCount = 0;


            // 외부 클릭하여 종료하지 못하도록
            setCanceledOnTouchOutside(false);


        }

        ProgressBar progressBar;


        // 어플에 포커스가 가면 동작한다
        @Override
        public void onWindowFocusChanged(boolean hasFocus) {
            super.onWindowFocusChanged(hasFocus);

        }
    }

}
