package kr.co.minetalk.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by episode on 2018. 10. 2..
 */

public class Sha256Util {
    public static String encoding(String param) throws Exception {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(param.getBytes());

            byte[] bs = md.digest();
            StringBuffer sb = new StringBuffer();
            for(int i = 0 ; i < bs.length ; i++) {
                sb.append(Integer.toString((bs[i] & 0xff) + 0x100, 16).substring(1));
            }
            result = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
        return result;
    }
}
