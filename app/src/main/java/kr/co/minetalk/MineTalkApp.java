package kr.co.minetalk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.igaworks.IgawCommon;
import com.igaworks.v2.core.application.AbxActivityHelper;
import com.igaworks.v2.core.application.AbxActivityLifecycleCallbacks;
import com.kakao.auth.ApprovalType;
import com.kakao.auth.AuthType;
import com.kakao.auth.IApplicationConfig;
import com.kakao.auth.ISessionConfig;
import com.kakao.auth.KakaoAdapter;
import com.kakao.auth.KakaoSDK;

import io.fabric.sdk.android.Fabric;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import kr.co.minetalk.api.model.AdListModel;
import kr.co.minetalk.api.model.ContactModel;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.database.FriendManager;
import kr.co.minetalk.locker.view.AppLocker;
import kr.co.minetalk.message.manager.MessageThreadManager;
import kr.co.minetalk.message.model.ThreadData;
import kr.co.minetalk.service.BackgroundService;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.Preferences;

public class MineTalkApp extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {
    private static Context mApplicationContext = null;
    private static Activity mCurrentActivity = null;
    private static Context mMainContext = null;

    private volatile static UserInfoBaseModel mUserInfoModel = null;
    private static ArrayList<FriendListModel> mRecommendList = new ArrayList<>();
    private static ArrayList<FriendListModel> mFriendList = new ArrayList<>();
    private static HashMap<String, String> mFriendListMap = new HashMap<>();

    private static ArrayList<Activity> mStack = new ArrayList<>();
    private static HashMap<String, String> mContactMap = new HashMap<>();
    private static HashMap<String, String> nickNameMap = new HashMap<>();

    private static ArrayList<ContactModel> mContactList = new ArrayList<>();

    public static String NOTICE_UPDATE_YN = "";
    public static String NOITICE_YN = "";
    public static String NOITICE_TITLE = "";
    public static String NOITICE_CONTENTS = "";

    private boolean isBackground = false;
    public static boolean mIsOutOfApp = false;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mApplicationContext = this;
        Preferences.init(this);

        FriendManager.init(this);
        MessageThreadManager.init(this);

        //IGAWorks AutoSession
        IgawCommon.autoSessionTracking(MineTalkApp.this);

        //Adbrix
        AbxActivityHelper.initializeSdk(MineTalkApp.this, "8N0YqnVLCEeO73MwFC88Og", "5bUNz4dkAk2gdVEAlVQ3jA");

        if (Build.VERSION.SDK_INT >= 14) {
            registerActivityLifecycleCallbacks(new AbxActivityLifecycleCallbacks());
        }

        IntentFilter screenOffFilter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (isBackground) {
                    isBackground = false;
                }
            }
        }, screenOffFilter);


        Log.e("@@@TAG", "reg : " + Preferences.getPushToken());

        // Register to be notified of activity state changes
        registerActivityLifecycleCallbacks(this);

        // 채팅방 배경화면 초기 설정
        String chatBackType = Preferences.getChatBackgroundType();
        if (chatBackType == null || chatBackType.equals("")) {
            Preferences.setChatBackgroundType(MineTalk.BACKGROUND_TYPE_COLOR);
            Preferences.setChatBackgroundColor("F9F9F9");
        }


        String systemLanguageCode = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            systemLanguageCode = getCurrentLocale().getLanguage().toLowerCase();
        } else {
            systemLanguageCode = getCurrentLocale2().toString().toLowerCase();
        }

        if (systemLanguageCode != null && systemLanguageCode.equals("zh")) {
            systemLanguageCode = "zh-CN";
        }

        if (systemLanguageCode != null && systemLanguageCode.equals("in")) {
            systemLanguageCode = "id";
        }


        if (!systemLanguageCode.equals("ko") &&
                !systemLanguageCode.equals("en") &&
                !systemLanguageCode.equals("zh-CN") &&
                !systemLanguageCode.equals("ja") &&
                !systemLanguageCode.equals("vi") &&
                !systemLanguageCode.equals("id") &&
                !systemLanguageCode.equals("th") &&
                !systemLanguageCode.equals("ms")) {

            systemLanguageCode = "en";
        }


        Preferences.setLanguage(systemLanguageCode);

        Log.e("@@@TAG", "system language : " + systemLanguageCode);


        KakaoSDK.init(new KakaoSDKAdapter());

        // 내언어 초기 설정
        if (Preferences.getMyChatLanguage().equals("")) {
//            Preferences.setMyChatLanguage("ko");
            Preferences.setMyChatLanguage(systemLanguageCode);
        }

        // 채팅방 번역 언어 초기 설정
        if (Preferences.getChatTranslationLanguageCode().equals("")) {
            Preferences.setChatTranslationLanguageCode("en");
        }

        if (Preferences.getCameraSourceCode().equals("")) {
            Preferences.setCameraSourceCode("ko");
        }

        if (Preferences.getCameraTargetCode().equals("")) {
            Preferences.setCameraTargetCode("en");
        }

        //핀번호 입력
        //AppLocker.getInstance().enableAppLock(this, Preferences.getPinScreenLock());
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        if(MineTalk.pageListener != null) {
            MineTalk.pageListener.onActivityCreated(activity);
        }

        String clazzName = activity.getClass().getName();
        Log.d("MineTalkApp", "onActivityCreated " + clazzName);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        //AppLocker.getInstance().addIgnoredActivity(activity.getClass().getCanonicalName());

        if(MineTalk.pageListener != null) {
            MineTalk.pageListener.onActivityStarted(activity);
        }

        String clazzName = activity.getClass().getName();
        Log.d("MineTalkApp", "onActivityStarted " + clazzName);

        //Toast.makeText(getAppContext(), "onActivityStarted " + clazzName, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResumed(Activity activity) {
        //AppLocker.getInstance().removeIgnoredActivity(activity.getClass().getCanonicalName());

        if(MineTalk.pageListener != null) {
            MineTalk.pageListener.onActivityResumed(activity);
        }

        String clazzName = activity.getClass().getName();
        Log.d("MineTalkApp", "onActivityResumed " + clazzName);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        if(MineTalk.pageListener != null) {
            MineTalk.pageListener.onActivityPaused(activity);
        }

        String clazzName = activity.getClass().getName();
        Log.d("MineTalkApp", "onActivityPaused " + clazzName);

    }

    @Override
    public void onActivityStopped(Activity activity) {
        //AppLocker.getInstance().removeIgnoredActivity(activity.getClass().getCanonicalName());

        if(MineTalk.pageListener != null) {
            MineTalk.pageListener.onActivityStopped(activity);
        }

        String name = activity.getClass().getCanonicalName();
        String clazzName = activity.getClass().getName();
        Log.d("MineTalkApp", "onActivityStopped " + clazzName);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        if(MineTalk.pageListener != null) {
            MineTalk.pageListener.onActivitySaveInstanceState(activity);
        }

        String clazzName = activity.getClass().getName();
        String topMostName = MineTalkApp.getCurrentActivity().getClass().getName();

        if(topMostName.toLowerCase().contains("mainactivity")) {
            Intent intent = new Intent(MineTalk.BROAD_CAST_LOCK_SCREEN_PAUSE_MESSAGE);
            intent.putExtra("visible", true);
            //MineTalkApp.getCurrentActivity().sendBroadcast(intent);
        }

        Log.d("MineTalkApp", "onActivitySaveInstanceState " + clazzName);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if(MineTalk.pageListener != null) {
            MineTalk.pageListener.onActivityDestroyed(activity);
        }

        String clazzName = activity.getClass().getName();
        Log.d("MineTalkApp", "onActivityDestroyed " + clazzName);
    }

    public static void initializeContactMap() {
        mContactMap.clear();
        mContactList.clear();

        try {
            mContactMap = CommonUtils.getContactMap(mApplicationContext);
            mContactList.addAll(CommonUtils.getContactList(mApplicationContext));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (!MineTalkApp.mIsOutOfApp && level == TRIM_MEMORY_UI_HIDDEN) {
            isBackground = true;
            AppLocker.getInstance().clearIgnoredActivity();
        }
    }

    public static ArrayList<ContactModel> getContactList() {
        return mContactList;
    }

    public static Context getAppContext() {
        return mApplicationContext;
    }

    public static void setCurrentActivity(Activity activity) {
        mCurrentActivity = activity;
    }

    public static Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    public static void addActivity(Activity activity) {
        mStack.add(activity);
    }

    public static void clearActivity() {
        for (int i = 0; i < mStack.size(); i++) {
            mStack.get(i).finish();
        }
    }

    public static void setUserInfoModel(UserInfoBaseModel userInfoModel) {
        mUserInfoModel = new UserInfoBaseModel();
        mUserInfoModel = userInfoModel;
    }

    public static UserInfoBaseModel getUserInfoModel() {
        return mUserInfoModel;
    }

    public static void setFriendList(ArrayList<FriendListModel> friendList) {
        mFriendList.clear();
        mFriendListMap.clear();
        mFriendList.addAll(friendList);

        for (FriendListModel model : mFriendList) {
            if (!mFriendListMap.containsKey(model.getUser_hp())) {
                mFriendListMap.put(model.getUser_hp(), model.getUser_name());
            }
        }
    }

    public static HashMap<String, String> getFriendListMap() {
        return mFriendListMap;
    }

    public static ArrayList<FriendListModel> getFriendList() {
        return mFriendList;
    }

    public static Comparator<FriendListModel> cmpAsc = new Comparator<FriendListModel>() {

        @Override
        public int compare(FriendListModel o1, FriendListModel o2) {
            return o1.getUser_name().compareTo(o2.getUser_name());
        }
    };


    public static Comparator<FriendListModel> cmpDesc = new Comparator<FriendListModel>() {

        @Override
        public int compare(FriendListModel o1, FriendListModel o2) {
            return o2.getUser_name().compareTo(o1.getUser_name());
        }
    };

    public static Comparator<ContactModel> contactCmpAsc = new Comparator<ContactModel>() {

        @Override
        public int compare(ContactModel o1, ContactModel o2) {
            return o1.getUser_name().compareTo(o2.getUser_name());
        }
    };

    public static Comparator<ContactModel> contactCmpDesc = new Comparator<ContactModel>() {

        @Override
        public int compare(ContactModel o1, ContactModel o2) {
            return o2.getUser_name().compareTo(o1.getUser_name());
        }
    };

    public static Comparator<AdListModel> adCmpAsc = new Comparator<AdListModel>() {

        @Override
        public int compare(AdListModel o1, AdListModel o2) {
            return o1.getUser_name().compareTo(o2.getUser_name());
        }
    };

    public static Comparator<AdListModel> adCmpDesc = new Comparator<AdListModel>() {

        @Override
        public int compare(AdListModel o1, AdListModel o2) {
            return o2.getUser_name().compareTo(o1.getUser_name());
        }
    };

    public static Comparator<ThreadData> threadDataCmpAsc = new Comparator<ThreadData>() {

        @Override
        public int compare(ThreadData o1, ThreadData o2) {
            SimpleDateFormat mDtFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateTarget = mDtFormat.format(o2.getUpdateDate());
            String dateSource = mDtFormat.format(o1.getUpdateDate());

            return dateTarget.compareTo(dateSource);
        }
    };

    public static Comparator<ThreadData> threadDataCmpDesc = new Comparator<ThreadData>() {

        @Override
        public int compare(ThreadData o1, ThreadData o2) {
            SimpleDateFormat mDtFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateTarget = mDtFormat.format(o2.getUpdateDate());
            String dateSource = mDtFormat.format(o1.getUpdateDate());

            return dateSource.compareTo(dateTarget);
        }
    };

    public static void setRecommendList(ArrayList<FriendListModel> arrayList) {
        mRecommendList.clear();
        mRecommendList.addAll(arrayList);
    }

    public static ArrayList<FriendListModel> getRecommendList() {
        return mRecommendList;
    }


    public static void setMainContext(final Context context) {
        mMainContext = context;
    }

    public static Context getmMainContext() {
        return mMainContext;
    }

    @SuppressWarnings("deprecation")
    public String getCurrentLocale2() {

        return Resources.getSystem().getConfiguration().locale.getLanguage();
    }

    @TargetApi(Build.VERSION_CODES.N)
    public Locale getCurrentLocale() {
        getResources();
        return Resources.getSystem().getConfiguration().getLocales().get(0);
    }


    private static class KakaoSDKAdapter extends KakaoAdapter {
        /**
         * Session Config에 대해서는 default값들이 존재한다.
         * 필요한 상황에서만 override해서 사용하면 됨.
         *
         * @return Session의 설정값.
         */

        @Override
        public ISessionConfig getSessionConfig() {
            return new ISessionConfig() {
                @Override
                public AuthType[] getAuthTypes() {
                    return new AuthType[]{AuthType.KAKAO_LOGIN_ALL};
                }

                @Override
                public boolean isUsingWebviewTimer() {
                    return false;
                }

                @Override
                public boolean isSecureMode() {
                    return false;
                }

                @Override
                public ApprovalType getApprovalType() {
                    return ApprovalType.INDIVIDUAL;
                }

                @Override
                public boolean isSaveFormData() {
                    return true;
                }
            };
        }

        @Override
        public IApplicationConfig getApplicationConfig() {

            return new IApplicationConfig() {
                @Override
                public Context getApplicationContext() {
                    return MineTalkApp.getAppContext();
                }
            };
        }
    }

    public static void saveLoginInfoClear() {
        Preferences.setLoginType("");
        Preferences.setNationCode("");
        Preferences.setLoginType("");
        Preferences.setLoginAuthorization("");
        Preferences.setAuthorization("");
        Preferences.setUserPassword("");
    }


    @Override
    public void onTerminate() {
        super.onTerminate();

        FriendManager.getInstance().release();
        MessageThreadManager.getInstance().release();
    }

    public static int getContactListCount() {
        return mContactMap.size();
    }

    public static String getContactUserName(String phoneNumber) {
        return mContactMap.get(phoneNumber);
    }

    public static void setContactUserName(String phoneNumber, String name) {
        mContactMap.put(phoneNumber, name);
    }

    public static void startBackgroundService(Context context, int jobFrag) {
        Intent intent = new Intent(context, BackgroundService.class);
        intent.putExtra(BackgroundService.SERVICE_JOB_FLAG, jobFrag);
        context.startService(intent);
    }

    public static long calDateBetweenDiff(String target, String source, SimpleDateFormat format) {
        try {
            // date1, date2 두 날짜를 parse()를 통해 Date형으로 변환.
            Date FirstDate = format.parse(target);
            Date SecondDate = format.parse(source);

            // Date로 변환된 두 날짜를 계산한 뒤 그 리턴값으로 long type 변수를 초기화 하고 있다.
            // 연산결과 -950400000. long type 으로 return 된다.
            long calDate = FirstDate.getTime() - SecondDate.getTime();

            // Date.getTime() 은 해당날짜를 기준으로1970년 00:00:00 부터 몇 초가 흘렀는지를 반환해준다.
            // 이제 24*60*60*1000(각 시간값에 따른 차이점) 을 나눠주면 일수가 나온다.
            long calDateDays = calDate / (24 * 60 * 60 * 1000);

            calDateDays = Math.abs(calDateDays);
            return calDateDays;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static String getUserNameByPhoneNum(String name, String phoneNo) {
        //사용자 본일일 경우
        if (MineTalkApp.getUserInfoModel().getUser_hp().equals(phoneNo))
            return MineTalkApp.getUserInfoModel().getUser_name();

        String userName = name;
        //연락처에서 이름을 가져 온다.
        String contactName = MineTalkApp.getContactUserName(phoneNo);
        if (contactName != null && !contactName.equals(""))
            userName = contactName;

        //HashMap<String, String> nickNameMap = FriendManager.getInstance().selectFriendNickNameMap();

        //닉네임 저장이 있다면 닉네임으로 출력.
        if(nickNameMap.size() == 0){
            nickNameMap = FriendManager.getInstance().selectFriendNickNameMap();
        }

        if (nickNameMap.containsKey(phoneNo)) {
            userName = nickNameMap.get(phoneNo);
        }

        return userName;
    }
}