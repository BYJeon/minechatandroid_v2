package kr.co.minetalk.ui.listener;

public interface OnMediaPopupListener {

    public void onCamera();
    public void onGallery();
}
