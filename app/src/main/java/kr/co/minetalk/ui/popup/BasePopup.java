package kr.co.minetalk.ui.popup;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import kr.co.minetalk.ui.data.DialogInterface;
import kr.co.minetalk.utils.CommonUtils;


public abstract class BasePopup<T extends BasePopup.BaseDialogListener> extends DialogFragment implements DialogInterface {
    public final static String TAG = BasePopup.class.getSimpleName();
    public final static int PRIORITY_BASE = 0;

    protected T mListener;
    protected int mPriority = PRIORITY_BASE;
    protected int mDialogId = 0;
    protected boolean mMatchParentHeight = false;
    private SparseArray<Object> mKeyedTags;

    abstract protected View getCreatedView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    public interface BaseDialogListener {
        public final static int STATE_OK = 0;
        public final static int STATE_CANCEL = -1;
        public final static int STATE_BACK = -2;
    }

    public void setDialogEventListener(T l) {
        mListener = l;
    }

    public void setMatchParentHeight(boolean value) {
        mMatchParentHeight = value;
    }

    @Override
    public void setPriority(int priority) {
        mPriority = priority;
    }

    @Override
    public int getPriority() {
        return mPriority;
    }

    @Override
    public void setDialogId(int id) {
        mDialogId = id;
    }

    @Override
    public int getDialogId() {
        return mDialogId;
    }

    @Override
    public void dismiss() {
        if (isResumed()) {
            super.dismiss();
        } else {
            // this should only be used for cases where it is okay for the UI state to change unexpectedly on the user.
            super.dismissAllowingStateLoss();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = getCreatedView(inflater, container, savedInstanceState);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return root;
    }

    @Override
    public void onPause() {
        try {
            Dialog dialog = getDialog();

            if (dialog != null) {
                View view = dialog.getCurrentFocus();

                if (view != null) {
                    if (view instanceof EditText) {
                    	CommonUtils.hideKeypad(getActivity(), view);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.6f;

        Activity activity = getActivity();
        if (activity != null) {
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            windowParams.width = metrics.widthPixels;

            if (mMatchParentHeight) {
                windowParams.height = metrics.heightPixels;
            }
        }

        window.setAttributes(windowParams);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        }catch (IllegalStateException e){
            e.printStackTrace();
        }
    }

    public void setTag(int key, Object tag) {
        if (mKeyedTags == null) {
            mKeyedTags = new SparseArray<Object>(2);
        }

        mKeyedTags.put(key, tag);
    }

    public Object getTag(int key) {
        if (mKeyedTags != null)
            return mKeyedTags.get(key);
        return null;
    }


}
