package kr.co.minetalk.ui.popup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.co.minetalk.R;

public class SimpleChatWorkPopup extends BasePopup implements View.OnClickListener {

    public interface OnItemIndexListener {
        public void onItemClick(int index);
    }
    public SimpleChatWorkPopup() {
    }

    private LinearLayout mLayout1;
    private LinearLayout mLayout2;
    private LinearLayout mLayout3;
    private LinearLayout mLayout4;
    private LinearLayout mLayout5;
    private LinearLayout mLayout6;

    private TextView mTvTitle;

//    private LinearLayout mLayout4;
//    private LinearLayout mLayout5;

    private OnOptionPopupListener mListener;

    private boolean isCopy = true;
    private boolean isCopyTrans = true;
    private boolean isDelete = true;
    private boolean isDeleteAll = true;
    private boolean isTransmission = true;
    private boolean isSave = true;


    private String mTitle = "";

    @Override
    protected View getCreatedView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.popup_simple_chat_work, container, false);
        mLayout1 = (LinearLayout) rootView.findViewById(R.id.layout_1);
        mLayout2 = (LinearLayout) rootView.findViewById(R.id.layout_2);
        mLayout3 = (LinearLayout) rootView.findViewById(R.id.layout_3);
        mLayout4 = (LinearLayout) rootView.findViewById(R.id.layout_4);
        mLayout5 = (LinearLayout) rootView.findViewById(R.id.layout_5);
        mLayout6 = (LinearLayout) rootView.findViewById(R.id.layout_6);
        mTvTitle = (TextView) rootView.findViewById(R.id.tv_title);

//        mLayout4 = (LinearLayout) rootView.findViewById(R.id.layout_4);
//        mLayout5 = (LinearLayout) rootView.findViewById(R.id.layout_5);
//
        mLayout1.setOnClickListener(this);
        mLayout2.setOnClickListener(this);
        mLayout3.setOnClickListener(this);
        mLayout4.setOnClickListener(this);
        mLayout5.setOnClickListener(this);
        mLayout6.setOnClickListener(this);
//        mLayout4.setOnClickListener(this);
//        mLayout5.setOnClickListener(this);

        if(!mTitle.equals("")) {
            mTvTitle.setVisibility(View.VISIBLE);
            mTvTitle.setText(mTitle);
        } else {
            mTvTitle.setVisibility(View.GONE);
            mTvTitle.setText("");
        }

        if(isCopy) {
            mLayout1.setVisibility(View.VISIBLE);
        } else {
            mLayout1.setVisibility(View.GONE);
        }

        if(isDelete) {
            mLayout2.setVisibility(View.VISIBLE);
        } else {
            mLayout2.setVisibility(View.GONE);
        }

        if(isDeleteAll) {
            mLayout6.setVisibility(View.VISIBLE);
        } else {
            mLayout6.setVisibility(View.GONE);
        }

        if(isTransmission) {
            mLayout3.setVisibility(View.VISIBLE);
        } else {
            mLayout3.setVisibility(View.GONE);
        }

        if(isSave) {
            mLayout4.setVisibility(View.VISIBLE);
        } else {
            mLayout4.setVisibility(View.GONE);
        }

        if(isCopyTrans) {
            mLayout5.setVisibility(View.VISIBLE);
        } else {
            mLayout5.setVisibility(View.GONE);
        }

        setUIEventListener();

        return rootView;
    }

    public void setEventListener(OnOptionPopupListener listener) {
        this.mListener = listener;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public void setSetting(boolean copy, boolean deleteAll, boolean delete, boolean transmission, boolean isSave, boolean isCopyTrans) {
        this.isCopy = copy;
        this.isDeleteAll = deleteAll;
        this.isDelete = delete;
        this.isTransmission = transmission;
        this.isSave = isSave;
        this.isCopyTrans = isCopyTrans;
    }


    private void setUIEventListener() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_1:
                if(mListener != null) {
                    mListener.onItemClick(0);
                }
                break;
            case R.id.layout_2:
                if(mListener != null) {
                    mListener.onItemClick(1);
                }
                break;
            case R.id.layout_3:
                if(mListener != null) {
                    mListener.onItemClick(2);
                }
                break;
            case R.id.layout_4:
                if(mListener != null) {
                    mListener.onItemClick(3);
                }
                break;
            case R.id.layout_5:
                if(mListener != null) {
                    mListener.onItemClick(4);
                }
                break;
            case R.id.layout_6:
                if(mListener != null) {
                    mListener.onItemClick(5);
                }
                break;
        }

        dismiss();
    }

    public interface OnOptionPopupListener {
        public void onItemClick(int index);
    }
}
