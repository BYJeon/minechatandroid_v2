package kr.co.minetalk.ui.data;

public interface DialogInterface {
	public void setPriority(int priority);

	public int getPriority();

	public void setDialogId(int id);

	public int getDialogId();

	public void setTag(int key, Object tag);

	public Object getTag(int key);
}
