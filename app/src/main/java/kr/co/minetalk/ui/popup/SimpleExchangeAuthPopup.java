package kr.co.minetalk.ui.popup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import kr.co.idevbank.base.api.ApiBase;
import kr.co.minetalk.MineTalk;
import kr.co.minetalk.MineTalkApp;
import kr.co.minetalk.R;
import kr.co.minetalk.api.RequestSmsApi;
import kr.co.minetalk.api.TransferTokenApi;
import kr.co.minetalk.api.model.BaseModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.utils.CommonUtils;
import kr.co.minetalk.utils.NumberCode;
import kr.co.minetalk.utils.ProgressUtil;

public class SimpleExchangeAuthPopup extends BasePopup implements View.OnClickListener {

    public interface OnEventListener {
        public void onComplete();
    }

    private OnEventListener mListener;

    private ImageView ivClose;

    private RelativeLayout layoutNationCode;
    private TextView tvExchangeToken;
    private TextView tvNationCode;
    private EditText etPhoneNumber;
    private EditText etAuthNumber;
    private TextView btnAuth;

    private TextView tvErrorAuthNumber;

    private TextView tvBtnCancel;
    private TextView tvBtnOk;

    private RequestSmsApi mRequestSmsApi;
    private TransferTokenApi mTransferTokenApi;

    private String mNationCode = "82";
    private int mNationCodePositionIndex = 0;

    private String mSmsType = MineTalk.SMS_TYPE_EXCHANGE;

    private String mExchangeToken = "0";

    private String mUserWalletAddress = "";


    public SimpleExchangeAuthPopup() {
    }

    @Override
    protected View getCreatedView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.popup_exchange_phone_auth, container, false);

        ivClose          = (ImageView)      rootView.findViewById(R.id.iv_close);
        layoutNationCode = (RelativeLayout) rootView.findViewById(R.id.layout_nation_code);
        tvExchangeToken  = (TextView)       rootView.findViewById(R.id.tv_transfer_point);
        tvNationCode     = (TextView)       rootView.findViewById(R.id.tv_nation_code);
        etAuthNumber     = (EditText)       rootView.findViewById(R.id.et_auth_number);
        etPhoneNumber    = (EditText)       rootView.findViewById(R.id.et_phone_number);
        btnAuth          = (TextView)       rootView.findViewById(R.id.btn_auth);

        tvErrorAuthNumber = (TextView)      rootView.findViewById(R.id.tv_error_auth);
        tvBtnCancel      = (TextView)       rootView.findViewById(R.id.tv_btn_cancel);
        tvBtnOk          = (TextView)       rootView.findViewById(R.id.tv_btn_ok);

        ivClose.setOnClickListener(this);
        tvBtnOk.setOnClickListener(this);
        tvBtnCancel.setOnClickListener(this);
        btnAuth.setOnClickListener(this);
//        layoutNationCode.setOnClickListener(this);


        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initApi();
        setUIEventListener();

        String exchangeToken = String.format(getResources().getString(R.string.exchange_point_msg), CommonUtils.comma_won(mExchangeToken));
        tvExchangeToken.setText(exchangeToken);


        /**************************
         * Nation Code initialize
         **************************/
        /*try {
            String nationVisibleName = NumberCode.getIntance().getDataList().get(0).getName();
            mNationCode = NumberCode.getIntance().getDataList().get(0).getNumCode().replace("+", "");

            tvNationCode.setText(nationVisibleName);
        } catch (Exception e) {
            e.printStackTrace();
            tvNationCode.setText("한국 " + "+82");
            mNationCode = "82";
        }
        mNationCodePositionIndex = 0;*/
        /**************************/


        String nationCode = MineTalkApp.getUserInfoModel().getUser_nation_code();
        if(nationCode != null && !nationCode.equals("")) {
            String nationName = NumberCode.getIntance().getNationName(nationCode);
            if(nationName != null && !nationName.equals("")) {
                mNationCode = nationCode;
                tvNationCode.setText(nationName + " +" + nationCode);
            } else {
//                tvNationCode.setText("한국 " + "+82");
                tvNationCode.setText(NumberCode.getIntance().getNationNameToMap("82") + "+82");
                mNationCode = "82";
            }
        } else {
//                tvNationCode.setText("한국 " + "+82");
            tvNationCode.setText(NumberCode.getIntance().getNationNameToMap("82") + "+82");
            mNationCode = "82";
        }


        String userPhone = MineTalkApp.getUserInfoModel().getUser_hp();
        etPhoneNumber.setText(userPhone);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close :
                dismiss();
                break;
            case R.id.btn_auth :
                smsRequest();
                break;
            case R.id.tv_btn_cancel:
                dismiss();
                break;
            case R.id.tv_btn_ok:
                if(mListener != null) {
                    String authNumber = etAuthNumber.getText().toString();
                    if(authNumber == null || authNumber.equals("")) {
                        Toast.makeText(MineTalkApp.getCurrentActivity(), getResources().getString(R.string.regist_hint_auth_number), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    transferRequest(authNumber, mExchangeToken, mUserWalletAddress);
                }
                break;
            case R.id.layout_nation_code:
                try {
                    String[] array = NumberCode.getIntance().getmTitleArray();
                    showBottomWheelPopup(array, mNationCodePositionIndex, mNationCodePickerListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void setUIEventListener() {
        etAuthNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String string = s.toString();
                if(string.equals("")) {
                    tvErrorAuthNumber.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if(string.equals("")) {
                    tvErrorAuthNumber.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void initApi() {
        mRequestSmsApi = new RequestSmsApi(MineTalkApp.getCurrentActivity(), new ApiBase.ApiCallBack<BaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MineTalkApp.getCurrentActivity());
            }

            @Override
            public void onSuccess(int request_code, BaseModel baseModel) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                if(baseModel != null) {
                    Toast.makeText(MineTalkApp.getCurrentActivity(), getResources().getString(R.string.common_request_sms_complete_msg), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                Toast.makeText(MineTalkApp.getCurrentActivity(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancellation() {

            }
        });

        mTransferTokenApi = new TransferTokenApi(MineTalkApp.getCurrentActivity(), new ApiBase.ApiCallBack<UserInfoBaseModel>() {
            @Override
            public void onPreparation() {
                ProgressUtil.showProgress(MineTalkApp.getCurrentActivity());
            }

            @Override
            public void onSuccess(int request_code, UserInfoBaseModel userInfoBaseModel) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                if(userInfoBaseModel != null) {
                    MineTalkApp.setUserInfoModel(userInfoBaseModel);
                    mListener.onComplete();
                    dismiss();
                }
            }

            @Override
            public void onFailure(int request_code, String message) {
                ProgressUtil.hideProgress(MineTalkApp.getCurrentActivity());
                if(request_code == 1001) {
                    tvErrorAuthNumber.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(MineTalkApp.getCurrentActivity(), message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancellation() {

            }
        });
    }

    public void setEventListener(OnEventListener listener) {
        this.mListener = listener;
    }

    private void smsRequest() {
        String phoneNumber = etPhoneNumber.getText().toString();
        if(phoneNumber == null || phoneNumber.replace(" ", "").equals("")) {
            Toast.makeText(MineTalkApp.getCurrentActivity(), getResources().getString(R.string.find_password_validation_phone_number), Toast.LENGTH_SHORT).show();
            return;
        }
//        mRequestSmsApi.execute(MineTalk.SMS_TYPE_SENDPOINT, mNationCode, phoneNumber);
        mRequestSmsApi.execute(mSmsType, mNationCode, phoneNumber);
    }

    private void transferRequest(String auth_number, String amount_token, String user_wallet_address) {
        mTransferTokenApi.execute(auth_number, amount_token, user_wallet_address);
    }

    // 전화번호 국가 코드 선택 Listener
    private PopupListenerFactory.BaseInputWheelListener mNationCodePickerListener = new PopupListenerFactory.BaseInputWheelListener() {
        @Override
        public void onClick(int state, int num) {
            if(state == PopupListenerFactory.BaseInputListener.STATE_OK) {
                mNationCodePositionIndex = num;
                try {
                    String viewName = NumberCode.getIntance().getDataList().get(num).getName();
                    mNationCode = NumberCode.getIntance().getDataList().get(num).getNumCode().replace("+", "");

                    tvNationCode.setText(viewName);
                } catch (Exception e) {
                    e.printStackTrace();
                    mNationCode = "82";
                }
            }
        }

        @Override
        public void onClick(int state, int num, int position) {
        }
    };

    public void setUserWalletAddress(String userWalletAddress) {
        this.mUserWalletAddress = userWalletAddress;
    }

    public void setSmsType(String type) {
        this.mSmsType = type;
    }

    public void setExchangeTokenValue(String value) {
        this.mExchangeToken = value;
    }

    protected void showBottomWheelPopup(String[] array, int defaultPosition, PopupListenerFactory.BaseInputWheelListener listener) {
        BottomWheelPopup mBottomWheelPicker = new BottomWheelPopup(MineTalkApp.getCurrentActivity());
        mBottomWheelPicker.setData(array, defaultPosition);
        mBottomWheelPicker.setDialogEventListener(listener);
        mBottomWheelPicker.show();
    }


}
