package kr.co.minetalk.ui.listener;

import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;

public interface OnProfileListener {
    public void onTalk(FriendListModel data);
    public void onFavorite(FriendListModel data, String yn);
    public void onCall(FriendListModel data);
    public void onGift(FriendListModel data);
    public void onBlock(FriendListModel data);
    public void onFriendAdd(FriendListModel data);
    public void onTalkMe(UserInfoBaseModel data);
    public void onEditProfile();
}
