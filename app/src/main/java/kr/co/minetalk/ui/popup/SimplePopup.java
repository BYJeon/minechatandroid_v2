package kr.co.minetalk.ui.popup;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kr.co.minetalk.R;
import kr.co.minetalk.ui.data.PopupListenerFactory;


@SuppressLint("ValidFragment")
public class SimplePopup extends BasePopup<PopupListenerFactory.SimplePopupListener> {

    private RelativeLayout mLogo = null;
    private TextView mCancel, mConfirm = null;
    private RelativeLayout mCancelBtn, mConfirmBtn = null;
    private TextView mMessage, mMessage2, mWarningMessage = null;
    private String mMsg, mDetailMsg, mWarningMsg, mBtnCancelText, mBtnConfirmText = null;

    private boolean mIsTwoButton, mIsBackCancel;
    private boolean mIsReverse = false;

    private SpannableStringBuilder mSp = null;
    private int mColor = 0;

    private boolean mRoomDiningPopup = false;

    public SimplePopup() {
    }

    public SimplePopup(String msg, String detailMsg, String confirmButtonText) {
        super();
        this.mMsg = msg;
        this.mDetailMsg = detailMsg;
        this.mBtnConfirmText = confirmButtonText;
        this.mIsTwoButton = false;
        this.mIsBackCancel = true;
    }

    public SimplePopup(String msg, String detailMsg, String confirmButtonText, boolean isRoomDiningPopup) {
        super();
        this.mMsg = msg;
        this.mDetailMsg = detailMsg;
        this.mBtnConfirmText = confirmButtonText;
        this.mIsTwoButton = false;
        this.mIsBackCancel = true;
        this.mRoomDiningPopup = isRoomDiningPopup;
    }

    public SimplePopup(String msg, String detailMsg, String confirmButtonText, String cancelButtonText) {
        super();
        this.mMsg = msg;
        this.mDetailMsg = detailMsg;
        this.mBtnConfirmText = confirmButtonText;
        this.mBtnCancelText = cancelButtonText;
        this.mIsTwoButton = true;
        this.mIsBackCancel = true;
    }

    public SimplePopup(String msg, String warning, String confirmButtonText, String empty1, String empyt2, String empty3) {
        super();
        this.mMsg = msg;
        this.mWarningMsg = warning;
        this.mBtnConfirmText = confirmButtonText;
        this.mIsTwoButton = false;
        this.mIsBackCancel = true;
    }

    public SimplePopup(String msg, String detailMsg, String warningMsg, String confirmButtonText, String cancelButtonText) {
        super();
        this.mMsg = msg;
        this.mDetailMsg = detailMsg;
        this.mWarningMsg = warningMsg;
        this.mBtnConfirmText = confirmButtonText;
        this.mBtnCancelText = cancelButtonText;
        this.mIsTwoButton = true;
        this.mIsBackCancel = true;
    }

    public SimplePopup(String msg, String detailMsg, String warningMsg, String confirmButtonText, String cancelButtonText, boolean isReverse) {
        super();
        this.mMsg = msg;
        this.mDetailMsg = detailMsg;
        this.mWarningMsg = warningMsg;
        this.mBtnConfirmText = confirmButtonText;
        this.mBtnCancelText = cancelButtonText;
        this.mIsTwoButton = true;
        this.mIsBackCancel = true;
        this.mIsReverse    = isReverse;
    }

    public SimplePopup(String msg, String detailMsg, String confirmButtonText, String cancelButtonText, boolean isBackValue) {
        super();
        this.mMsg = msg;
        this.mDetailMsg = detailMsg;
        this.mBtnConfirmText = confirmButtonText;
        this.mBtnCancelText = cancelButtonText;
        this.mIsTwoButton = true;
        this.mIsBackCancel = isBackValue;

    }


    private OnClickListener mOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.popup_confirm_btn:
                    if (mListener != null) {
                        mListener.onClick(SimplePopup.this, PopupListenerFactory.SimplePopupListener.STATE_OK);
                        dismiss();
                    } else {
                        dismiss();
                    }
                    break;
                case R.id.popup_cancel_btn:

                    if (mListener != null) {
                        mListener.onClick(SimplePopup.this, PopupListenerFactory.SimplePopupListener.STATE_CANCEL);
                        dismiss();
                    } else {
                        dismiss();
                    }
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    public View getCreatedView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.popup_simple, container, false);
        mConfirm = (TextView) rootView.findViewById(R.id.popup_confirm);
        mConfirmBtn = (RelativeLayout) rootView.findViewById(R.id.popup_confirm_btn);
        mMessage = (TextView) rootView.findViewById(R.id.popup_message);
        mMessage2 = (TextView) rootView.findViewById(R.id.popup_detail_message);
        mWarningMessage = rootView.findViewById(R.id.popup_warning_message);
        mWarningMessage.setVisibility(View.GONE);

        if (mBtnConfirmText != null) {
            mConfirm.setText(mBtnConfirmText);
        }

        if (!TextUtils.isEmpty(mMsg)) {
            mMessage.setText(mMsg);
        } else {
            mMessage.setVisibility(View.GONE);
        }
        mConfirmBtn.setOnClickListener(mOnClickListener);

        if (mDetailMsg != null) {
            mMessage2.setText(mDetailMsg);

        } else {
            mMessage2.setVisibility(View.GONE);
        }

        if(mWarningMessage != null){
            mWarningMessage.setText(mWarningMsg);
            mWarningMessage.setVisibility(View.VISIBLE);
        }

        // 버튼이 두 개인 팝업인 경우에만 취소버튼을 활성화 시킨다.
        if (mIsTwoButton) {
            mCancel = (TextView) rootView.findViewById(R.id.popup_cancel);
            mCancelBtn = (RelativeLayout) rootView.findViewById(R.id.popup_cancel_btn);
            mCancelBtn.setVisibility(View.VISIBLE);
            if (mBtnCancelText != null) {
                mCancel.setText(mBtnCancelText);
            }
            mCancelBtn.setOnClickListener(mOnClickListener);

            //Reverse Button
            if(mIsReverse){
                mConfirm.setBackgroundResource(R.drawable.bg_chat_round_c9cfce);
                mCancel.setBackgroundResource(R.drawable.button_bg_16c066);
            }
        }else{
            mCancelBtn = (RelativeLayout) rootView.findViewById(R.id.popup_cancel_btn);
            mCancelBtn.setVisibility(View.GONE);
        }

        if (mColor != 0) {
            mMessage.setTextColor(mColor);
        }

        if (mSp != null) {
            mMessage.setText(mSp);
        }


        return rootView;
    }

    public void setMsgTextColor(int color) {
        this.mColor = color;
    }

    public void setMsgSappanableText(SpannableStringBuilder sp) {
        this.mSp = sp;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if (mListener != null && mIsBackCancel) {
            mListener.onClick(SimplePopup.this, PopupListenerFactory.SimplePopupListener.STATE_CANCEL);
        }
        super.onCancel(dialog);
    }
}