package kr.co.minetalk.ui.popup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import kr.co.minetalk.R;
import kr.co.minetalk.databinding.PopupBottomWheelBinding;
import kr.co.minetalk.ui.data.PopupListenerFactory;
import kr.co.minetalk.ui.widget.adapters.ArrayWheelAdapter;

public class BottomWheelPopup<T extends BasePopup.BaseDialogListener> extends BottomSheetDialog implements View.OnClickListener {
    private Context mContext;
    protected PopupListenerFactory.BaseInputWheelListener mListener;

    private PopupBottomWheelBinding mBinding;

    private String[] mBaseInputArray;

    private int mCurrentItem = 0;

    private int mPosition = -1;
    private boolean mIsEdit = false;

    public BottomWheelPopup(@NonNull Context context) {
        super(context);
        initView(context);
    }


//    public BottomWheelPopup(Context context, String[] inputArray) {
//        mContext = context;
//        mBaseInputArray = inputArray;
//    }
//
//
//    public BottomWheelPopup(Context context, String[] inputArray, int currnetItem) {
//        mContext = context;
//        mBaseInputArray = inputArray;
//        mCurrentItem = currnetItem;
//    }
//
//
//    public BottomWheelPopup(Context context, String[] inputArray, int currnetItem, int position) {
//        mContext = context;
//        mBaseInputArray = inputArray;
//        mCurrentItem = currnetItem;
//        mPosition = position;
//    }
//
//
//    public BottomWheelPopup(Context context, String[] inputArray, int currnetItem, int position, boolean isEdit) {
//        mContext = context;
//        mBaseInputArray = inputArray;
//        mCurrentItem = currnetItem;
//        mPosition = position;
//        mIsEdit = isEdit;
//    }


    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(getContext()).inflate(R.layout.popup_bottom_wheel, null);
        mBinding = DataBindingUtil.bind(v);
        // setCancelable(false);
        setContentView(v);


        mBinding.popupConfirm.setOnClickListener(this);
        mBinding.popupCancel.setOnClickListener(this);

    }

    public void setData(String[] inputArray, int currnetItem) {
        mBaseInputArray = inputArray;
        mCurrentItem = currnetItem;


        ArrayWheelAdapter<String> minsWheelAdapter = new ArrayWheelAdapter<String>(mContext, mBaseInputArray);
        minsWheelAdapter.setItemResource(R.layout.wheel_time_text_item);
        minsWheelAdapter.setItemTextResource(R.id.time_text);
        mBinding.wheelNum.setViewAdapter(minsWheelAdapter);
        mBinding.wheelNum.setEnabled(true);
        mBinding.wheelNum.setCurrentItem(mCurrentItem);

    }

    public void setDialogEventListener(PopupListenerFactory.BaseInputWheelListener l) {
        mListener = l;
    }


    @Override
    public void onClick(View v) {
        if (mListener == null)
            return;

        int id = v.getId();

        if (id == R.id.popup_confirm) {
            if(mPosition >= 0) {
                mListener.onClick(PopupListenerFactory.BaseInputListener.STATE_OK,
                        mBinding.wheelNum.getCurrentItem(), mPosition);
            } else {
                mListener.onClick(PopupListenerFactory.BaseInputListener.STATE_OK,
                        mBinding.wheelNum.getCurrentItem());
            }
        } else {
            if(mPosition >= 0 && !mIsEdit) {
                mListener.onClick(PopupListenerFactory.BaseInputListener.STATE_CANCEL,
                        mBinding.wheelNum.getCurrentItem(), mPosition);
            } else {
                mListener.onClick(PopupListenerFactory.BaseInputListener.STATE_CANCEL,
                        mBinding.wheelNum.getCurrentItem());
            }
        }

        dismiss();
    }
}
