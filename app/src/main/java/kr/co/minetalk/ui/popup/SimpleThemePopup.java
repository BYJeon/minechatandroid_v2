package kr.co.minetalk.ui.popup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import kr.co.minetalk.R;
import kr.co.minetalk.adapter.listener.PopupLanguageAdapter;
import kr.co.minetalk.adapter.listener.PopupThemeAdapter;
import kr.co.minetalk.api.model.CountryInfoModel;
import kr.co.minetalk.repository.CountryRepository;

public class SimpleThemePopup extends BasePopup implements View.OnClickListener {

    public interface OnItemIndexListener {
        public void onItemClick(int index);
    }
    public SimpleThemePopup() {
    }


    private ListView mListView;
    private PopupThemeAdapter mAdapter;

    private OnItemIndexListener mListener;

    @Override
    protected View getCreatedView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.popup_simple_list, container, false);

        mListView = (ListView) rootView.findViewById(R.id.lv_list);
        mAdapter = new PopupThemeAdapter();

        ArrayList<CountryInfoModel> data = new ArrayList<>();


        CountryInfoModel normal = new CountryInfoModel("기본 테마","기본 테마","theme_normal","theme_normal","theme_normal");
        CountryInfoModel black  = new CountryInfoModel("블랙 테마","블랙 테마","theme_black","theme_black","theme_black");

        data.add(normal);
        data.add(black);

        mAdapter.setData(data);
        mListView.setAdapter(mAdapter);

        setUIEventListener();


        return rootView;
    }

    public void setOnItemIndexListener(OnItemIndexListener listener) {
        this.mListener = listener;
    }

    private void setUIEventListener() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mListener != null) {
                    mListener.onItemClick(position);
                }
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
