package kr.co.minetalk.ui.popup;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.bumptech.glide.Glide;

import kr.co.minetalk.R;
import kr.co.minetalk.activity.PhotoViewerActivity;
import kr.co.minetalk.api.model.FriendListModel;
import kr.co.minetalk.api.model.UserInfoBaseModel;
import kr.co.minetalk.databinding.LayoutBottomPopupProfileBinding;
import kr.co.minetalk.ui.listener.OnProfileListener;

public class BottomProfilePopup extends BottomSheetDialog {

    public static enum ViewType {
        NORMAl,
        NEW,
        ME
    }


    private Context mContext;
    private LayoutBottomPopupProfileBinding mBinding;

    private OnProfileListener mListener;

    private FriendListModel mData;                  // 친구 프로필 일 경우 사용
    private UserInfoBaseModel mUserInfoData;        // 내 프로필 일 경우 사용

    private ViewType mViewType = ViewType.NORMAl;

    public BottomProfilePopup(@NonNull Context context) {
        super(context);
        this.mContext = context;
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;
        View v = LayoutInflater.from(getContext()).inflate(R.layout.layout_bottom_popup_profile, null);
        mBinding = DataBindingUtil.bind(v);
        // setCancelable(false);
        setContentView(v);

        setUIEventListener();
    }

    public void setViewType(ViewType viewType) {
        this.mViewType = viewType;
    }


    public void setData(FriendListModel data) {
        this.mData = data;

        if(mViewType == ViewType.NORMAl) {
            mBinding.ivButtonFavorite.setVisibility(View.VISIBLE);
            mBinding.layoutNew.setVisibility(View.INVISIBLE);
            mBinding.layoutMe.setVisibility(View.INVISIBLE);
            mBinding.layoutNormal.setVisibility(View.VISIBLE);
            mBinding.tvUserPhone.setVisibility(View.VISIBLE);
        } else if(mViewType == ViewType.NEW){
            mBinding.ivButtonFavorite.setVisibility(View.INVISIBLE);
            mBinding.layoutMe.setVisibility(View.INVISIBLE);
            mBinding.tvUserPhone.setVisibility(View.INVISIBLE);
            mBinding.layoutNew.setVisibility(View.VISIBLE);
            mBinding.layoutNormal.setVisibility(View.INVISIBLE);
        } else if(mViewType == ViewType.ME) {
            mBinding.layoutNormal.setVisibility(View.INVISIBLE);
            mBinding.layoutNew.setVisibility(View.INVISIBLE);
            mBinding.ivButtonFavorite.setVisibility(View.INVISIBLE);
            mBinding.layoutMe.setVisibility(View.VISIBLE);
        }

        if(mData != null) {
            String profileImgUrl = mData.getUser_profile_image();
            if(profileImgUrl == null || profileImgUrl.equals("")) {
                Glide.with(mContext).load(R.drawable.img_basic).into(mBinding.ivUserImage);
            } else {
                Glide.with(mContext).load(mData.getUser_profile_image()).into(mBinding.ivUserImage);
            }

            mBinding.tvUserName.setText(mData.getUser_name());
            mBinding.tvUserPhone.setText(mData.getUser_hp());

            if(mData.getFavorite_yn().equals("Y")) {
                mBinding.ivButtonFavorite.setSelected(true);
            } else {
                mBinding.ivButtonFavorite.setSelected(false);
            }
        }
    }

    public void setUserInfoData(UserInfoBaseModel userInfoData) {
        this.mUserInfoData = userInfoData;
        if(mUserInfoData != null) {
            mBinding.layoutNormal.setVisibility(View.INVISIBLE);
            mBinding.layoutNew.setVisibility(View.INVISIBLE);
            mBinding.ivButtonFavorite.setVisibility(View.INVISIBLE);
            mBinding.layoutMe.setVisibility(View.VISIBLE);


            String profileImgUrl = mUserInfoData.getUser_profile_image();
            if(profileImgUrl == null || profileImgUrl.equals("")) {
                Glide.with(mContext).load(R.drawable.img_basic).into(mBinding.ivUserImage);
            } else {
                Glide.with(mContext).load(profileImgUrl).into(mBinding.ivUserImage);
            }

            mBinding.tvUserName.setText(mUserInfoData.getUser_name());
            mBinding.tvUserPhone.setText(mUserInfoData.getUser_hp());
        }
    }


    private void setUIEventListener() {
        mBinding.ivButtonFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelect = !v.isSelected();
                v.setSelected(isSelect);
                if(isSelect) {
                    if(mListener != null) {
                        mListener.onFavorite(mData, "Y");
                    }
                } else {
                    if(mListener != null) {
                        mListener.onFavorite(mData, "N");
                    }
                }
                dismiss();
            }
        });

        // 프로필 닫기
        mBinding.ivButtonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        // 친구 추가
        mBinding.tvButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onFriendAdd(mData);
                }
                dismiss();
            }
        });

        // 차단
        mBinding.tvButtonBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    //mListener.onBlock(mData);
                }
                dismiss();
            }
        });

        // 전화 걸기
        mBinding.tvButtonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onCall(mData);
                }
                dismiss();
            }
        });

        // 전화 걸기
        mBinding.tvUserPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mViewType == ViewType.ME) {
//                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mUserInfoData.getUser_hp()));
//                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mData.getUser_hp()));
                    mContext.startActivity(intent);
                }


            }
        });

        // 선물하기
        mBinding.tvButtonPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onGift(mData);
                }
                dismiss();
            }
        });

        // 1:1 채팅
        mBinding.tvButtonChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onTalk(mData);
                }
                dismiss();
            }
        });

        // 내 프로필 수정
        mBinding.tvButtonEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onEditProfile();
                }
                dismiss();
            }
        });

        // 나와의 채팅
        mBinding.tvButtonChatMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onTalkMe(mUserInfoData);
                }
                dismiss();
            }
        });

        mBinding.ivUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mViewType == ViewType.ME ) {
                    PhotoViewerActivity.startActivity(mContext, mUserInfoData.getUser_profile_image(), "");
                } else {
                    PhotoViewerActivity.startActivity(mContext, mData.getUser_profile_image(), "");
                }

            }
        });
    }

    public void setProfileListener(OnProfileListener listener) {
        this.mListener = listener;
    }
}
