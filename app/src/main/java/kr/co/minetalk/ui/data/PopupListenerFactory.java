package kr.co.minetalk.ui.data;


import kr.co.minetalk.ui.popup.BasePopup;

public class PopupListenerFactory {
    public interface SimplePopupListener extends BasePopup.BaseDialogListener {
        public void onClick(DialogInterface f, int state);
    }

    public interface BaseInputListener extends BasePopup.BaseDialogListener {
        public void onClick(DialogInterface f, int state, int num);
        public void onClick(DialogInterface f, int state, int num, int position);
    }

    public interface BaseInputWheelListener extends BasePopup.BaseDialogListener {
        public void onClick(int state, int num);
        public void onClick(int state, int num, int position);
    }
    
}
