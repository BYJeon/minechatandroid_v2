package kr.co.minetalk.ui.popup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import kr.co.minetalk.R;

public class SimpleNoticePopup extends BasePopup implements View.OnClickListener {


    public SimpleNoticePopup() {
    }

    private TextView mTvNoticeContent;
    private TextView mTvBtnConfirm;
    private ImageView mIvClose;
    private CheckBox  mCbWeekVisible;
//    private LinearLayout mLayout4;
//    private LinearLayout mLayout5;

    private OnNoticeListener mListener;
    private String mNoticeContent="";

    @Override
    protected View getCreatedView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.popup_simple_notice, container, false);
        mTvNoticeContent = (TextView) rootView.findViewById(R.id.tv_notice_content);
        mTvBtnConfirm = (TextView) rootView.findViewById(R.id.tv_btn_confirm);
        mIvClose = (ImageView) rootView.findViewById(R.id.iv_close);
        mCbWeekVisible = rootView.findViewById(R.id.cb_week_visible);

        setUIEventListener();

        mTvNoticeContent.setText(mNoticeContent);

        return rootView;
    }

    public void setEventListener(OnNoticeListener listener) {
        this.mListener = listener;
    }

    public void setContents(String str) {
        this.mNoticeContent = str;
    }


    private void setUIEventListener() {
        mTvBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    final Calendar cal = Calendar.getInstance();
                    String today = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DATE));
                    //mListener.onTodayNotVisible(today);
                    if(mCbWeekVisible.isChecked())
                        mListener.onWeekNotVisible(today);
                }
                dismiss();
            }
        });

        mIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    public interface OnNoticeListener {
        public void onTodayNotVisible(String today);
        public void onWeekNotVisible(String today);
    }
}
