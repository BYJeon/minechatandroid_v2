package kr.co.minetalk.ui.popup;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;

import kr.co.minetalk.R;
import kr.co.minetalk.databinding.LayoutBottomPopupMediaBinding;
import kr.co.minetalk.ui.listener.OnMediaPopupListener;

public class BottomMediaPopup extends BottomSheetDialog {
    private Context mContext;
    private LayoutBottomPopupMediaBinding mBinding;

    private OnMediaPopupListener mListener;
    public BottomMediaPopup(@NonNull Context context) {
        super(context);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;

        View v = LayoutInflater.from(getContext()).inflate(R.layout.layout_bottom_popup_media, null);
        mBinding = DataBindingUtil.bind(v);
        // setCancelable(false);
        setContentView(v);

        setUIEventListener();
    }

    private void setUIEventListener() {
        mBinding.tvButtonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onCamera();
                }
                dismiss();
            }
        });

        mBinding.tvButtonGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    mListener.onGallery();
                }
                dismiss();
            }
        });
    }

    public void setEventListener(OnMediaPopupListener listener) {
        this.mListener = listener;
    }
}
