package kr.co.minetalk.ui.listener;

import kr.co.minetalk.api.model.AdListModel;

public interface OnAdChatItemListener {
    public void onItemClick(String type, AdListModel data);
}
