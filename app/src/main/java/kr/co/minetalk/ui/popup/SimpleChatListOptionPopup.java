package kr.co.minetalk.ui.popup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import kr.co.minetalk.R;
import kr.co.minetalk.adapter.listener.PopupLanguageAdapter;
import kr.co.minetalk.repository.CountryRepository;

public class SimpleChatListOptionPopup extends BasePopup implements View.OnClickListener {

    public interface OnItemIndexListener {
        public void onItemClick(int index);
    }
    public SimpleChatListOptionPopup() {
    }

    private LinearLayout mLayout1;
    private LinearLayout mLayout2;
    private LinearLayout mLayout3;
    private TextView mTvTitle;
    private TextView mTvAlarmMsg;
//    private LinearLayout mLayout4;
//    private LinearLayout mLayout5;

    private OnOptionPopupListener mListener;

    private String mAlarmMessage;
    private String mTitle;

    @Override
    protected View getCreatedView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.popup_simple_chat_list_option, container, false);
        mLayout1 = (LinearLayout) rootView.findViewById(R.id.layout_1);
        mLayout2 = (LinearLayout) rootView.findViewById(R.id.layout_2);
        mLayout3 = (LinearLayout) rootView.findViewById(R.id.layout_3);
        mTvTitle = (TextView) rootView.findViewById(R.id.tv_title);
        mTvAlarmMsg = (TextView) rootView.findViewById(R.id.tv_list_2);
//        mLayout4 = (LinearLayout) rootView.findViewById(R.id.layout_4);
//        mLayout5 = (LinearLayout) rootView.findViewById(R.id.layout_5);
//
        mLayout1.setOnClickListener(this);
        mLayout2.setOnClickListener(this);
        mLayout3.setOnClickListener(this);
//        mLayout4.setOnClickListener(this);
//        mLayout5.setOnClickListener(this);

        mTvTitle.setText(mTitle);
        mTvAlarmMsg.setText(mAlarmMessage);
        setUIEventListener();



        return rootView;
    }

    public void setEventListener(OnOptionPopupListener listener) {
        this.mListener = listener;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }
    public void setAlarmMessage(String message) {
        this.mAlarmMessage = message;
    }


    private void setUIEventListener() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_1:
                if(mListener != null) {
                    mListener.onItemClick(0);
                }
                break;
            case R.id.layout_2:
                if(mListener != null) {
                    mListener.onItemClick(1);
                }
                break;
            case R.id.layout_3:
                if(mListener != null) {
                    mListener.onItemClick(2);
                }
                break;
//            case R.id.layout_4:
//                if(mListener != null) {
//                    mListener.onItemClick(3);
//                }
//                break;
//            case R.id.layout_5:
//                if(mListener != null) {
//                    mListener.onItemClick(4);
//                }
//                break;
        }

        dismiss();
    }

    public interface OnOptionPopupListener {
        public void onItemClick(int index);
    }
}
