package kr.co.minetalk.ui.popup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import kr.co.minetalk.R;
import kr.co.minetalk.ui.data.PopupListenerFactory.BaseInputListener;
import kr.co.minetalk.ui.widget.OnWheelClickedListener;
import kr.co.minetalk.ui.widget.WheelView;
import kr.co.minetalk.ui.widget.adapters.ArrayWheelAdapter;


public class BaseWheelPopup extends BasePopup<BaseInputListener> implements OnClickListener {

    private Button mCancelBtn, mConfirmBtn;
    private String mBtnCancelText, mBtnConfirmText;
    private Context mContext;


    private WheelView mBaseWheelView;
    private String[] mBaseInputArray;

    private int mCurrentItem = 0;

    private int mPosition = -1;
    private boolean mIsEdit = false;

    public BaseWheelPopup() {}

    @SuppressLint("ValidFragment")
    public BaseWheelPopup(Context context, String[] inputArray) {
        mContext = context;
        mBaseInputArray = inputArray;
    }

    @SuppressLint("ValidFragment")
    public BaseWheelPopup(Context context, String[] inputArray, int currnetItem) {
        mContext = context;
        mBaseInputArray = inputArray;
        mCurrentItem = currnetItem;
    }

    @SuppressLint("ValidFragment")
    public BaseWheelPopup(Context context, String[] inputArray, int currnetItem, int position) {
        mContext = context;
        mBaseInputArray = inputArray;
        mCurrentItem = currnetItem;
        mPosition = position;
    }

    @SuppressLint("ValidFragment")
    public BaseWheelPopup(Context context, String[] inputArray, int currnetItem, int position, boolean isEdit) {
        mContext = context;
        mBaseInputArray = inputArray;
        mCurrentItem = currnetItem;
        mPosition = position;
        mIsEdit = isEdit;
    }

    @Override
    protected View getCreatedView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.popup_wheel, container, false);

        mConfirmBtn = (Button)rootView.findViewById(R.id.popup_confirm);
        mCancelBtn = (Button)rootView.findViewById(R.id.popup_cancel);

        mBaseWheelView = (WheelView)rootView.findViewById(R.id.wheel_num);

        ArrayWheelAdapter<String> minsWheelAdapter = new ArrayWheelAdapter<String>(mContext, mBaseInputArray);
        minsWheelAdapter.setItemResource(R.layout.wheel_time_text_item);
        minsWheelAdapter.setItemTextResource(R.id.time_text);
        mBaseWheelView.setViewAdapter(minsWheelAdapter);
        mBaseWheelView.setEnabled(true);
        mBaseWheelView.setCurrentItem(mCurrentItem);


        mConfirmBtn
                .setText(TextUtils.isEmpty(mBtnConfirmText) ? mContext.getString(R.string.common_ok) : mBtnConfirmText);

        mCancelBtn.setText(TextUtils.isEmpty(mBtnCancelText) ? mContext.getString(R.string.common_cancel) : mBtnCancelText);

        mBaseWheelView.addClickingListener(mClick);

        mConfirmBtn.setOnClickListener(this);
        mCancelBtn.setOnClickListener(this);

        setMatchParentHeight(true);
        
//        FontUtils.setFontNR(mConfirmBtn);
//        FontUtils.setFontNR(mCancelBtn);

        return rootView;
    }

    OnWheelClickedListener mClick = new OnWheelClickedListener() {
        public void onItemClicked(WheelView wheel, int itemIndex) {
            wheel.setCurrentItem(itemIndex, true);
        }
    };

    public void onDestroyView() {
        finish();

        super.onDestroyView();
    }

    private void finish() {
        mBaseWheelView.removeClickingListener(mClick);
    }

    public int getBaseInput() {
        return mBaseWheelView.getCurrentItem();
    }

    @Override
    public void onClick(View v) {
        if (mListener == null)
            return;

        int id = v.getId();

        if (id == R.id.popup_confirm) {
            if(mPosition >= 0) {
                mListener.onClick(BaseWheelPopup.this, BaseInputListener.STATE_OK,
                        mBaseWheelView.getCurrentItem(), mPosition);
            } else {
                mListener.onClick(BaseWheelPopup.this, BaseInputListener.STATE_OK,
                        mBaseWheelView.getCurrentItem());
            }
        } else {
            if(mPosition >= 0 && !mIsEdit) {
                mListener.onClick(BaseWheelPopup.this, BaseInputListener.STATE_CANCEL,
                        mBaseWheelView.getCurrentItem(), mPosition);
            } else {
                mListener.onClick(BaseWheelPopup.this, BaseInputListener.STATE_CANCEL,
                        mBaseWheelView.getCurrentItem());
            }
        }

        dismiss();
    }
}
