package kr.co.minetalk.ui.popup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import kr.co.minetalk.R;
import kr.co.minetalk.adapter.listener.PopupLanguageAdapter;
import kr.co.minetalk.repository.CountryRepository;

public class SimpleListPopup extends BasePopup implements View.OnClickListener {

    public interface OnItemIndexListener {
        public void onItemClick(int index);
    }
    public SimpleListPopup() {
    }

//    private LinearLayout mLayout1;
//    private LinearLayout mLayout2;
//    private LinearLayout mLayout3;
//    private LinearLayout mLayout4;
//    private LinearLayout mLayout5;

    private ListView mListView;
    private PopupLanguageAdapter mAdapter;

    private OnItemIndexListener mListener;

    @Override
    protected View getCreatedView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.popup_simple_list, container, false);
//        mLayout1 = (LinearLayout) rootView.findViewById(R.id.layout_1);
//        mLayout2 = (LinearLayout) rootView.findViewById(R.id.layout_2);
//        mLayout3 = (LinearLayout) rootView.findViewById(R.id.layout_3);
//        mLayout4 = (LinearLayout) rootView.findViewById(R.id.layout_4);
//        mLayout5 = (LinearLayout) rootView.findViewById(R.id.layout_5);
//
//        mLayout1.setOnClickListener(this);
//        mLayout2.setOnClickListener(this);
//        mLayout3.setOnClickListener(this);
//        mLayout4.setOnClickListener(this);
//        mLayout5.setOnClickListener(this);

        mListView = (ListView) rootView.findViewById(R.id.lv_list);
        mAdapter = new PopupLanguageAdapter();
        mAdapter.setData(CountryRepository.getInstance().getCountryListAll());
        mListView.setAdapter(mAdapter);

        setUIEventListener();


        return rootView;
    }

    public void setOnItemIndexListener(OnItemIndexListener listener) {
        this.mListener = listener;
    }

    private void setUIEventListener() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mListener != null) {
                    mListener.onItemClick(position);
                }
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.layout_1:
//                if(mListener != null) {
//                    mListener.onItemClick(0);
//                }
//                break;
//            case R.id.layout_2:
//                if(mListener != null) {
//                    mListener.onItemClick(1);
//                }
//                break;
//            case R.id.layout_3:
//                if(mListener != null) {
//                    mListener.onItemClick(2);
//                }
//                break;
//            case R.id.layout_4:
//                if(mListener != null) {
//                    mListener.onItemClick(3);
//                }
//                break;
//            case R.id.layout_5:
//                if(mListener != null) {
//                    mListener.onItemClick(4);
//                }
//                break;
//        }

        dismiss();
    }
}
