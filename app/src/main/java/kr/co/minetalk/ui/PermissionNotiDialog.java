package kr.co.minetalk.ui;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import kr.co.minetalk.R;
import kr.co.minetalk.activity.listener.OnPermissionListener;
import kr.co.minetalk.utils.Preferences;


/**
 * Created by episode on 2018. 4. 15..
 */

public class PermissionNotiDialog extends Dialog {
    private Context mContext;
    private Button mBtnConfirm;

    private OnPermissionListener mListener = null;

    public interface OnButtonListener {
        public void clickOk();
    }

    public PermissionNotiDialog(@NonNull Context context, @NonNull OnPermissionListener listener) {
        super(context, R.style.DialogFullTheme);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.mContext = context;
        this.mListener = listener;

        initViews(context);
    }

    private void initViews(Context context) {
        this.mContext = context;
        setContentView(R.layout.popup_app_permission);

        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);

        setUIEventListener();
    }

    private void setUIEventListener() {
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setAuthorityNotice(true);
                if(mListener != null)
                    mListener.onPermissionCheckComplete();
                dismiss();
            }
        });
    }

}
