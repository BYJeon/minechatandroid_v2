package kr.co.idevbank.base.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.util.Pair;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.CertificatePinner;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import kr.co.idevbank.base.R;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kyd0822 on 2017. 6. 27..
 */

public abstract class ApiBase<Result, ApiInterface> {

    private static final String TAG = "API_BASE";

    public static final int REQUEST_CODE_SUCCESS            = 0;
    public static final int REQUEST_CODE_UNEXPECTED         = 1;
    public static final int REQUEST_CODE_FAIL_NETWORK       = 2;
    public static final int REQUEST_CODE_FAIL_SERVER        = 3;
    public static final int REQUEST_CODE_FAIL_COMMUNICATION = 6;
    public static final int REQUEST_CODE_FAIL_PARAMS        = 8;

    protected String mAuthorization = "";
    protected String mX_Login_Type  = "";
    protected String mX_Lang        = "";

//    protected void setFlag(int flag) {
//        this.flag = flag;
//    }
//
//    protected final int FLAG_START_LOGIN_ACTIVITY = 1 ;
//    protected final int FLAG_IGNORE_LOGIN_CACHE = 2;

    private int flag = 0;

//    public final static String API_BASE_URL = "";   // Real Server

    /**
     *
     */
    protected abstract ApiInterface createApiInterface(Retrofit retrofit);

    protected abstract int getVersionCode();

    protected abstract String getApplicationId();

    protected abstract String getBaseUrl();


    protected abstract boolean isFailParams();

    protected abstract Call<ResponseBody> createCall(ApiInterface apiInterface);

    protected abstract Result parseResult(Context context, int request_code, String response_str) throws JSONException;


    /**
     *
     */
    public static final int QUEUE_TYPE_UNKNOWN = 0;
    public static final int QUEUE_TYPE_ONCE_FIRST = 1;
    public static final int QUEUE_TYPE_ONCE_LAST = 2;// overwrite with last request


    /**
     *
     */
    public interface ApiCallBack<Result> {

        void onPreparation();

        void onSuccess(int request_code, Result result);

        void onFailure(int request_code, String message);

        void onCancellation();
    }

    private AsyncTask apiAsyncTask = null;
    private ApiCallBack apiCallBack = null;


    /**
     *
     */
    private int queue_type = QUEUE_TYPE_UNKNOWN;
    private Context context = null;
    private Activity activity = null;
    private int timeout = 50;

    /**
     * @param activity
     */
    public ApiBase(Activity activity,
                   int queue_type,
                   ApiCallBack apiCallBack) {
        this(activity, queue_type, apiCallBack, 50);
    }

    public ApiBase(Activity activity,
                   int queue_type,
                   ApiCallBack apiCallBack,
                   int timeout) {
        this(activity.getApplicationContext(), queue_type, apiCallBack, timeout);
    }

    public ApiBase(Context context,
                   int queue_type,
                   ApiCallBack apiCallBack) {
        this(context, queue_type, apiCallBack, 50);
    }

    public ApiBase(Context context,
                   int queue_type,
                   ApiCallBack apiCallBack,
                   int timeout) {
        this.context = context;
        this.queue_type = queue_type;
        this.apiCallBack = apiCallBack;
        this.timeout = timeout;
    }

    /**
     * @return
     */
    final public boolean isExecuting() {
        return apiAsyncTask != null;
    }


    /**
     * @param context
     * @return
     */
    protected boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }



    final public void onDestroy() {
        if (apiAsyncTask != null) {
            apiAsyncTask.cancel(true);
        }
    }

    /**
     *
     */
    final protected void apiExecute() {
        apiExecute(0);
    }



    /**
     * @param minimum_time_millis
     */
    final protected void apiExecute(long minimum_time_millis) {
        Log.d(getApiName(), "api executed!");

        if (queue_type == QUEUE_TYPE_ONCE_FIRST) {
            if (apiAsyncTask == null) {
                apiAsyncTask = new GeneralApiAsyncTask();
                ((GeneralApiAsyncTask) apiAsyncTask).execute(minimum_time_millis);
            }
        } else if (queue_type == QUEUE_TYPE_ONCE_LAST) {
            if (apiAsyncTask != null)
                apiAsyncTask.cancel(true);
            apiAsyncTask = new GeneralApiAsyncTask();
            ((GeneralApiAsyncTask) apiAsyncTask).execute(minimum_time_millis);
        } else {
            if (apiAsyncTask == null) {
                apiAsyncTask = new GeneralApiAsyncTask();
                ((GeneralApiAsyncTask) apiAsyncTask).execute(minimum_time_millis);
            }
        }
    }

    /**
     * @param response
     * @return
     */
    private Pair<Pair<Integer, String>, String> getRequestCodeFromResponse(Context mContext, Response<ResponseBody> response) {

        String response_str = null;



        /**
         *  Network 연결 여부 체크
         */
        if (!isNetworkConnected(mContext)) {
            return new Pair<>(new Pair<>(REQUEST_CODE_FAIL_NETWORK, ""), response_str);
        }

        /**
         *
         */
        if (response != null) {
            Log.e(getApiName(), "response.code() -> " + response.code());
            Log.e(TAG, "response.message() -> " + response.message());

            if (response.isSuccess()) {
                try {
                    response_str = response.body().string();
                    Log.e("@@@TAG","reponse_str => " + response_str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    response_str = response.errorBody().string();
                    Log.e(getApiName(), "response_str -> " + response_str);
                    try {
                        JSONObject jsonObject = new JSONObject(response_str);
                        Log.e(getApiName(), "message -> " + jsonObject.getString("message"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(getApiName(), "message parsing error");
                    }
                    //TODO : handle this, this should not be success!
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         *  서버 연결 체크
         */
        if (response_str == null || response_str.equals("")) {
            return new Pair<>(new Pair<>(REQUEST_CODE_FAIL_SERVER, ""), response_str);
        }


        /**
         *
         */
        try {
            JSONObject responseObject = new JSONObject(response_str);

            final String message       = responseObject.getString("message");
            final int    response_code = Integer.parseInt(responseObject.getString("code"));

            if (response.code() == 200) {
                //탈퇴회원 인증 시 "0001"값을 사용하고 있음.
                //response_code가 0일떄만 Success로 처리 하고 있어 REQUEST_CODE_SUCCESS 파라미터로 호출함.
                if(response_code == 1 || response_code == 2)
                    return new Pair<>(new Pair<>(REQUEST_CODE_SUCCESS, message), response_str);

                return new Pair<>(new Pair<>(response_code, message), response_str);
            } else if (response.code() == 400) {
                return new Pair<>(new Pair<>(REQUEST_CODE_FAIL_PARAMS, message), response_str);

            } else if (response.code() == 500
                    || response.code() == 404
                    || response.code() == 405
                    || response.code() == 501
                    || response.code() == 403) {
                return new Pair<>(new Pair<>(REQUEST_CODE_FAIL_COMMUNICATION, message), response_str);

            } else {
                return new Pair<>(new Pair<>(REQUEST_CODE_FAIL_COMMUNICATION, message), response_str);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Pair<>(new Pair<>(REQUEST_CODE_UNEXPECTED, response.message()), response_str);
    }


    private class GeneralApiAsyncTask extends AsyncTask<Long, Void, Pair<Pair<Integer, String>, Result>> {

        @Override
        final protected void onPreExecute() {
            super.onPreExecute();
            if (apiCallBack != null) {
                apiCallBack.onPreparation();
            }
        }

        /**
         * @param delays
         * @return
         */
        @Override
        final protected Pair<Pair<Integer, String>, Result> doInBackground(Long... delays) {

            long start_datetime = System.currentTimeMillis();



            if (isFailParams()) {
                return new Pair<>(new Pair<>(REQUEST_CODE_FAIL_PARAMS, ""), null);
            }

            /**
             *
             */
            Response<ResponseBody> response = null;
            try {


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getBaseUrl())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(getClient())
                        .build();
                ApiInterface apiInterface = createApiInterface(retrofit);

                response = createCall(apiInterface).execute();

            } catch (Exception e) {
                e.printStackTrace();
            }

            /**
             * Handle Response
             */
            String request_message = "";
            Pair<Pair<Integer, String>, String> pair = getRequestCodeFromResponse(context, response);
            int request_code = pair.first.first;
            request_message = pair.first.second;
            String response_str = pair.second;

            /**
             *
             */
            if (response_str == null) {
                return new Pair<>(new Pair<>(request_code, request_message), null);
            }

            /**
             *
             */
            long last_time_millis = System.currentTimeMillis() - start_datetime;
            long minimum_time_millis = 0;
            if (delays.length > 0) {
                minimum_time_millis = delays[0].longValue();
            }
            if (last_time_millis < minimum_time_millis) {
                Log.d(getApiName(), "last_time_millis -> " + last_time_millis);
                Log.d(getApiName(), "minimum_time_millis -> " + minimum_time_millis);
                try {
                    Thread.sleep(minimum_time_millis - last_time_millis);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /**
             *
             */
            try {
                Result result = parseResult(context, request_code, response_str);
                Log.e(getApiName(),"response_str => " + response_str);
                return new Pair<>(new Pair<>(request_code, request_message), result);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return new Pair<>(new Pair<>(request_code, request_message), null);
        }

        /**
         * @param pair
         */
        @Override
        final protected void onPostExecute(Pair<Pair<Integer, String>, Result> pair) {
            super.onPostExecute(pair);

            int request_code = pair.first.first;
            String request_message = pair.first.second;
            Result result = pair.second;

            if (apiCallBack != null) {
                if (request_code == REQUEST_CODE_SUCCESS) {
                    apiCallBack.onSuccess(request_code, result);
                } else {
//                    if(request_message == null || request_message.equals("") || request_message.equals("null")) {
//                        request_message = "현재 네트워크 연결이 원할하지 않습니다.\nWi-Fi 및 3G 네트워크를 확인 하시기 바랍니다.";
//                    }
                    apiCallBack.onFailure(request_code, request_message);
                }
            }

            apiAsyncTask = null;
        }

        /**
         *
         */
        @Override
        final protected void onCancelled() {
            super.onCancelled();

            if (apiCallBack != null) {
                apiCallBack.onCancellation();
            }
            apiAsyncTask = null;
        }
    }

    protected void setHeader(String authorization, String x_login_type, String x_lang) {
        this.mAuthorization = authorization;
        this.mX_Login_Type  = x_login_type;
        this.mX_Lang        = x_lang.toLowerCase();

    }

    protected void setHeaderLangueage(String x_lang) {
        this.mX_Lang = x_lang;
    }

    private OkHttpClient getClient() {
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.setReadTimeout(timeout, TimeUnit.SECONDS);
        httpClient.setConnectTimeout(timeout, TimeUnit.SECONDS);

        httpClient.interceptors().add(new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .header("Authorization", "Basic " + mAuthorization)
                        .addHeader("X-Login-Type", mX_Login_Type)
                        .addHeader("X-Lang",  mX_Lang)
                        .addHeader("Accept", "application/json")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        return httpClient;
    }


    protected String getApiName() {
        return this.getClass().getSimpleName();
    }


    public static SSLSocketFactory getPinnedCertSslSocketFactory(Context context) {
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = context.getResources().openRawResource(R.raw.comma);
            Certificate ca = null;
            try {
                ca = cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } catch (CertificateException e) {
                e.printStackTrace();
            } finally {
                caInput.close();
            }

            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            if (ca == null) {
                return null;
            }
            keyStore.setCertificateEntry("ca", ca);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            SSLContext sslContext= SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);

            return sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
